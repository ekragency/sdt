<?php
require "../../vendor/autoload.php";
use QuickBooks\OAuth2\Client;
use QuickBooks\QBConfigs;
use QuickBooks\ManageQuickBooks;
use QuickBooks\Output;

$session_id = session_id();
if (empty($session_id))
{
    session_start();
}

$configs = QBConfigs::getConfigs();

$client_id = $configs['client_id'];
$client_secret = $configs['client_secret'];
$authorizationRequestUrl = $configs['authorizationRequestUrl'];
$scope = $configs['oauth_scope'];
$tokenEndPointUrl = $configs['tokenEndPointUrl'];
$redirect_uri = $configs['oauth_redirect_uri'];
$response_type = 'code';
$state = 'RandomState';
$grant_type= 'authorization_code';
//$certFilePath = './Certificate/all.platform.intuit.com.pem';
$certFilePath = '../../certificates/cacert.pem';


$client = new Client($client_id, $client_secret, $certFilePath);
//ob_start();
try{
    if (!isset($_GET["code"]))
    {
        /*Step 1
        /*Do not use Curl, use header so it can redirect. Curl just download the content it does not redirect*/
        //$json_result = $client->getAuthorizationCode($authorizationRequestUrl, $scope, $redirect_uri, $response_type, $state, $include_granted_scope);
        //unset $_SESSION global variables
        unset($_SESSION['access_token']);
        unset($_SESSION['refresh_token']);
        $authUrl = $client->getAuthorizationURL($authorizationRequestUrl, $scope, $redirect_uri, $response_type, $state);
        header("Location: ".$authUrl);
        exit();
    }
    else
    {
        if(empty($_SESSION['access_token'])){
            $code = $_GET["code"];
            $responseState = $_GET['state'];
            if(strcmp($state, $responseState) != 0){
                throw new Exception("The state is not correct from Intuit Server. Consider your app is hacked.");
            }
            $result = $client->getAccessToken($tokenEndPointUrl,  $code, $redirect_uri, $grant_type);
            //record them in the session variable
            $_SESSION['access_token'] = $result['access_token'];
            $_SESSION['refresh_token'] = $result['refresh_token'];
        }


        QBConfigs::setKeys($_SESSION['access_token'], $_SESSION['refresh_token']);

        $qbManager = new ManageQuickBooks();
    }
} catch(\Exception $e){
    Output::error($e->getCode().": ".$e->getMessage());
}
//$output = ob_get_clean();

?><html>
<head>
    <meta http-equiv="PRAGMA" content="NO-CACHE">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="Generator" content="">
    <link HREF="/css/style.css" REL="styleSheet" TYPE="text/css">
    <link HREF="/css/processes.css" REL="styleSheet" TYPE="text/css">
    <title>Metafraze Tools - Access</title>
</head>

<BODY leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0"
      onLoad=""
>

<TABLE WIDTH="100%" HEIGHT="100%" BORDER=0 CELLPADDING=0 CELLSPACING=0>

    <tr bgcolor="#004581"><td valign=top align=left colspan=2>
            <table border=0 cellpadding=0 cellspacing=0 width="100%">
                <tr><td height=80><span class=adminhead>Metafraze Tools</span></td></tr>
            </table>
        </td></tr>

    <tr bgcolor="#4C85B0"><td valign=top align=left colspan=2>
            <table border=0 cellspacing=0 cellpadding=0 width="100%">
                <tr><td align=left valign=top width=600 class=adminsubhead>
                        &nbsp;&nbsp;&nbsp;Update Quickbooks</td>
                    <td align=right valign=top class=rightadminsubhead>
                        </td></tr>
            </table>
        </td></tr>

    <tr><td valign=top align=left width=160 height="100%" bgcolor=#C1C1C1 class=menu>
            <img src="/images/spacer.gif" border=0 width=160 height=2><br>
            <div class=adminmenu>

                <br>
                <br>
            </div>
        </td>
        <td valign=top align=left width="100%" class=adminbody>

            <table border=0 cellpadding=0 cellspacing=0 width=840>
                <tr><td valign=top align=left width=840 height=28>&nbsp;</td></tr>
            </table>
            <table border=0 cellpadding=0 cellspacing=0 width=840>
                <tr>
                    <th valign=top align=left height=80 style="font-size: 2em;">
                       Generate Invoices
                    </th>
                    <th valign=top align=left style="font-size: 2em;">
                        Generate Bills
                    </th>
                </tr>
                <tr>
                    <td valign=top align=left>
                        <?php
							$invoiceData = $qbManager->findInvoices();
							echo "<div style=\"margin-bottom: 8px;\">";
							echo "<a href=\"/qoauth/invoices.php\">All Invoices</a> ({$invoiceData->totalCount})<br />";
							echo "</div>";
							foreach($invoiceData->clients as $client){
								echo "<div style=\"margin-bottom: 8px;\">";
								echo "<a href=\"/qoauth/invoices.php?client={$client->id}\">{$client->name}</a> ({$client->projects})<br />";
								echo "</div>";
							}
                        ?>
                    </td>
                    <td valign=top align=left>
                        <?php
							$billData = $qbManager->findAllBills();
							echo "<div style=\"margin-bottom: 8px;\">";
							echo "<a href=\"/qoauth/bills.php\">All Bills</a> ({$billData->totalCount})<br />";
							echo "</div>";
							foreach($billData->linguists as $linguist){
								echo "<div style=\"margin-bottom: 8px;\">";
								echo "<a href=\"/qoauth/bills.php?linguist={$linguist->id}\">{$linguist->name}</a> ({$linguist->bills})<br />";
								echo "</div>";
							}
						?>
                    </td>
                </tr>
            </table>

            <br />
            <br />
            <a href="/qoauth/log.php">View Log</a><br /><br />
            <a href="/qoauth/index.php">Re-authenticate with Quickbooks (restart)</a>
            <?php /*$qbManager->listCategoryItems();*/ ?>

            <!-- Content Starts Here -->
            <table border=0 cellpadding=5 cellspacing=0 width=200>
                <tr><td align=left valign=top class=label></td>
                    <td align=left valign=top class=form>

                    </td>
                </tr>
                <tr>
                    <td align=left valign=top class=label></td>
                    <td align=left valign=top class=form></td>
                </tr>
                <tr><td>&nbsp;</td><td valign=top align=left class=form>

                    </td>
                </tr>
            </table>
            <script language="JavaScript">
                <!--
                document.logonform.usr_login.focus();
                //-->
            </script>

            <!-- Content Ends Here -->
        </td></tr>

    <tr><td bgcolor="#004581" valign=top align=left colspan=2 height=25>
            <table border=0 cellpadding=0 cellspacing=0 width=100%>
                <tr><td valign=top class=footer>
                        Copyright &copy; 2020 - Same Day Translations.  All Rights Reserved.
                    </td>
                    <td valign=top class=rightfooter>

                    </td></tr>
            </table>
        </td></tr>

</TABLE>



</body>
</html>

