<?php
require "../../vendor/autoload.php";
use QuickBooks\OAuth2\Client;
use QuickBooks\QBConfigs;
use QuickBooks\ManageQuickBooks;
use QuickBooks\Output;

$session_id = session_id();
if (empty($session_id))
{
    session_start();
}

$configs = QBConfigs::getConfigs();

$client_id = $configs['client_id'];
$client_secret = $configs['client_secret'];
$authorizationRequestUrl = $configs['authorizationRequestUrl'];
$scope = $configs['oauth_scope'];
$tokenEndPointUrl = $configs['tokenEndPointUrl'];
$redirect_uri = $configs['oauth_redirect_uri'];
$response_type = 'code';
$state = 'RandomState';
$grant_type= 'authorization_code';
//$certFilePath = './Certificate/all.platform.intuit.com.pem';
$certFilePath = '../../certificates/cacert.pem';


$client = new Client($client_id, $client_secret, $certFilePath);

try{
    if(empty($_SESSION['access_token'])){
        throw new \Exception("Access token unavailable.");
    }
    QBConfigs::setKeys($_SESSION['access_token'], $_SESSION['refresh_token']);

    $qbManager = new ManageQuickBooks();
    $maxOut = getenv("MAX_OUTPUT");

} catch(\Exception $e){
    Output::error($e->getCode().": ".$e->getMessage());
    exit;
}

?><html>
<head>
    <meta http-equiv="PRAGMA" content="NO-CACHE">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="Generator" content="">
    <link HREF="/css/style.css" REL="styleSheet" TYPE="text/css">
    <link HREF="/css/processes.css" REL="styleSheet" TYPE="text/css">
    <title>Metafraze Tools - Access</title>

</head>

<BODY leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0"
      onLoad=""
>

<TABLE WIDTH="100%" HEIGHT="100%" BORDER=0 CELLPADDING=0 CELLSPACING=0>

    <tr bgcolor="#004581"><td valign=top align=left colspan=2>
            <table border=0 cellpadding=0 cellspacing=0 width="100%">
                <tr><td height=80><span class=adminhead>Metafraze Tools</span></td></tr>
            </table>
        </td></tr>

    <tr bgcolor="#4C85B0"><td valign=top align=left colspan=2>
            <table border=0 cellspacing=0 cellpadding=0 width="100%">
                <tr><td align=left valign=top width=600 class=adminsubhead>
                        &nbsp;&nbsp;&nbsp;Update Quickbooks</td>
                    <td align=right valign=top class=rightadminsubhead>
                        </td></tr>
            </table>
        </td></tr>

    <tr><td valign=top align=left width=160 height="100%" bgcolor=#C1C1C1 class=menu>
            <img src="/images/spacer.gif" border=0 width=160 height=2><br>
            <div class=adminmenu>

                <br>
                <br>
            </div>
        </td>
        <td valign=top align=left width="100%" class=adminbody>

            <table border=0 cellpadding=0 cellspacing=0 width=840>
                <tr><td valign=top align=left width=840 height=28>&nbsp;</td></tr>
            </table>
            <?php
				try {
					Output::info("----------------------------");
					Output::info("Bills started ". date("Y-m-d h:i:s A"));
					Output::info("----------------------------");
					if (!empty($_GET["linguist"])) {
						$qbManager->processBillsForVendor($_GET["linguist"]);
					} else {
						$qbManager->processAllBills($maxOut);
					}
				}
				catch (\Exception $e) {
					Output::error($e->getMessage());
				}

			?>
            <br />
            <br />
            <a href="/qoauth/index.php">Re-authenticate with Quickbooks</a>

            <!-- Content Starts Here -->
            <table border=0 cellpadding=5 cellspacing=0 width=200>
                <tr><td align=left valign=top class=label></td>
                    <td align=left valign=top class=form>

                    </td>
                </tr>
                <tr>
                    <td align=left valign=top class=label></td>
                    <td align=left valign=top class=form></td>
                </tr>
                <tr><td>&nbsp;</td><td valign=top align=left class=form>

                    </td>
                </tr>
            </table>
            <script language="JavaScript">
                <!--
                document.logonform.usr_login.focus();
                //-->
            </script>

            <!-- Content Ends Here -->
        </td></tr>

    <tr><td bgcolor="#004581" valign=top align=left colspan=2 height=25>
            <table border=0 cellpadding=0 cellspacing=0 width=100%>
                <tr><td valign=top class=footer>
                        Copyright &copy; 2020 - Same Day Translations.  All Rights Reserved.
                    </td>
                    <td valign=top class=rightfooter>

                    </td></tr>
            </table>
        </td></tr>

</TABLE>



</body>
</html>

