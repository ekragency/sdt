#!/usr/bin/perl
###########################################################################
# invadm.cgi                Invoice Administration
###########################################################################
# Copyright 2017, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;
use Excel::Writer::XLSX;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/invadm.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($out_path)    = $config->get_out_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;
my ($temp_path)   = $config->get_temp_path;
my ($forms_path)  = $config->get_forms_path;
my ($forms_url)   = $config->get_forms_url;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $debug         = $config->get_debug;
my $doExpBtn      = 0;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;


# Main program
my $usr_id = 0;
my $action = $shared->get_cgi( "action" );
my $hash_vars;

$hash_vars->{srch_invno}   = $shared->get_cgi( "srch_invno" );
$hash_vars->{srch_ling}    = $shared->get_cgi( "srch_ling" );
$hash_vars->{srch_stat}    = $shared->get_cgi( "srch_stat" );


#
# Main Program
#
 
if ( $auth->validate_access( "invadm" ) ) {
	
	$usr_id    = $auth->getCurrentUser;
	
    if ( $action eq "search_any" ) {
        &search_any;
    } elsif ( $action eq "edit_inv_form" ) {
        &edit_inv_form;
    } elsif ( $action eq "edit_inv" ) {
        &edit_inv;
    } elsif ( $action eq "del_invoice" ) {
        &del_invoice;
    } elsif ( $action eq "audit_log" ) {
    	&audit_log;
    } elsif ( $action eq "view_invoice" ) {
        my $invID = $shared->get_cgi( "inv_id" );
        &view_invoice( $invID );
    } elsif ( $action eq "inv_exp_list" ) {
        my $excelURL = &genExpList;
        if ( $excelURL gt "" ) {
            $env->{open_file} = $excelURL;
        }
        &search_any( "Y" );
    } elsif ( $action eq "mark_inv_all" ) {
        &mark_inv_all;
        &search_any( "Y" );	
    } else {
        &search_any( "Y" );
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    $conf->{f_linguist_select} = &selLing;
    $conf->{f_stat_select}     = $shared->pop_lookup_select( "INV_STAT", "", "ORDER BY lku_sort" );
    
    my $expBtn    = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=inv_exp_list>\n"
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Export Pend Invoice\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";
                  
    if ( $doExpBtn ) {
        $conf->{export_xls} = $expBtn;       
    }
 
    
    my $pg_body                = &disp_form( "inv_main" );

    if ( $doExpBtn ) {
        my $js = "<script type=\"text/javascript\">\n";
        $js   .= "ichk = false;\n";
        $js   .= "function checkAll ( frm1 ) {\n";
        $js   .= "    var aa = document.getElementById( 'frm1' );\n"; 
        $js   .= "    if ( ichk == false ) {\n";
        $js   .= "        ichk = true\n";
        $js   .= "    } else {\n";
        $js   .= "        ichk = false\n";
        $js   .= "    }\n";
        $js   .= "    for ( var i = 0; i < aa.elements.length; i++ ) {\n"; 
        $js   .= "        aa.elements[i].checked = ichk;\n";
        $js   .= "    }\n";
        $js   .= "}\n";
        $js   .= "</script>\n";
        
        $env->{javascript}     = $js;
    }
        
    $env->{page_body}          = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my ( $init_srch ) = @_;
	
	my $content = "";
	my $results = "";
	
	my $srch_invno   = $hash_vars->{srch_invno};
	my $srch_ling    = $hash_vars->{srch_ling};
	my $srch_stat    = $hash_vars->{srch_stat};
	
	my $srch_head    = "Pending Invoices";
	
	if ( ( $srch_invno gt "" ) || ( $srch_ling gt "" ) || ( $srch_stat gt "" ) ) {
	    $srch_head   = "<b>Searched:</b>&nbsp;";
	}
	my $srch_par     = 0;
	my $sp_vars      = "";
	
	if ( ( $srch_invno gt "" ) || ( $srch_ling gt "" ) || ( $srch_stat gt "" ) || ( $init_srch eq "Y" ) ) {
	
	    my $SQL  = "SELECT inv_id, inv_no, inv_date, lig_first_name, lig_last_name, inv_status, inv_amount, lig_currency\n"
	             . "FROM invoice\n"
	             . "INNER JOIN linguist ON inv_ling_id = lig_id\n"
	             . "WHERE 1\n";
	             
	    if ( $srch_invno gt "" ) {
    	    $SQL .= "AND inv_no LIKE '$srch_invno%'\n";
    	    $srch_head .= "Invoice No starts with <b>$srch_invno</b>";
		    $srch_par++;
		    $sp_vars       .= "&srch_invno=$srch_invno";
	    }
	    if ( $srch_ling gt "" ) {
		    $SQL .= "AND inv_ling_id = $srch_ling\n";
		    my $ligRec = $shared->get_rec( "linguist", "lig_id", $srch_ling );
		    my $lName  = $ligRec->{lig_first_name} . " " . $ligRec->{lig_last_name};
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Linguist is <b>$lName</b>";
		    } else {
		        $srch_head .= "Linguist is <b>$lName</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_ling=$srch_ling";
	    }
	    if ( $srch_stat gt "" ) {
	        $SQL .= "AND inv_status = '$srch_stat'\n";
	        my $dspStat = $shared->getLookupDesc( "INV_STAT", $srch_stat );
	        if ( $srch_par > 0 ) {
	            $srch_head .= ", Status is <b>$dspStat</b>";
	        } else {
	            $srch_head .= "Status is <b>$dspStat</b>";
	        }
	        $srch_par++;
	        $sp_vars        = "&srch_stat=$srch_stat";
	    }
	    if ( ( $init_srch eq "Y" ) && ( $srch_par == 0 ) ) {
            $SQL .= "AND inv_status = 'PEND'\n";
            $doExpBtn = 1;
	    }
	    
	    $SQL     .= "ORDER BY inv_date";
	    
	    my $doReset = 0;
	    if ( ( $init_srch eq "Y" ) && ( $srch_par == 0 ) ) {
	    	$sp_vars  .= "&srch_init=Y";
	    } else {
	        $doReset = 1;
	    }
	
	    my @rs   = $shared->getResultSet( $SQL );
	    my $row  = 0;
	    my $row_color;
	    my $fndCnt = 0;
	    
	    $content .= "<span class=subhead><b>$srch_head</b></span><br>\n";
	    if ( ( $doReset ) || ( $doExpBtn ) ) {
	        if ( $doExpBtn ) {
	            $content .= "<form id=\"frm1\" method=post action=\"$cgi_url\">\n";
                $content .= "<input type=hidden name=action value=\"mark_inv_all\">\n";
	        }
	        $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	        $content .= "<tr>\n";
	        if ( $doReset ) {
	    	    $content .= "<td valign=top align=left width=50%><a href=\"$cgi_url\" class=nav><b>[ Clear Filter ]</b></a></td>\n";
	    	} else {
	    	    $content .= "<td valign=top align=left width=50%>&nbsp;</td>\n";
	    	}
	    	if ( $doExpBtn ) {
	    	    $content .= "<td valign=top align=right width=50% class=form><b><input type=checkbox name=checkall style='background: #3f0' onclick='checkAll( frm1 )'> Toggle Checked</b>&nbsp;&nbsp;";
	    	    $content .= "<input type=submit value=\"Marked Invoiced\" class=form></td>\n";
	    	} else {
	    	    $content .= "<td valign=top align=right width=50% class=form>&nbsp;</td>\n";
	    	}
	    	$content .= "</tr>\n";
	    	$content .= "</table>\n";
	    }
	    $content .= "<br>\n";
	    
	    $results .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	    $results .= "<tr bgcolor=\"black\">\n";
	    $results .= "<td valign=top align=left class=form>&nbsp;</td>\n";
	    $results .= "<td valign=top align=left class=form><font color=\"white\"><b>Invoice No</b></font></td>\n";
	    $results .= "<td valign=top align=left class=form><font color=\"white\"><b>Invoice Date</b></font></td>\n";
	    $results .= "<td valign=top align=left class=form><font color=\"white\"><b>Linguist</b></font></td>\n";
	    $results .= "<td valign=top align=left class=form><font color=\"white\"><b>Status</b></font></td>\n";
	    $results .= "<td valign=top align=right class=form><font color=\"white\"><b>Amount</b></font></td></tr>\n";
	    foreach my $rec ( @rs ) {
	    	my $id      = $rec->{inv_id};
	    	my $invNo   = $rec->{inv_no};
	    	my $invDt   = $shared->getDateFmt( $rec->{inv_date} );
	    	my $lName   = $rec->{lig_first_name} . " " . $rec->{lig_last_name};
	    	my $invStat = $shared->getLookupDesc( "INV_STAT", $rec->{inv_status} );
	    	my $iCurr   = $rec->{lig_currency};
	    	my $invAmt  = $shared->fmtCurrency( $rec->{inv_amount}, $iCurr );
	  
	    	
	    	if ( $row == 0 ) {
	    		$row_color = $color_bg2;
	    		$row = 1;
	    	} else {
	    		$row_color = $color_bg1;
	    		$row = 0;
	    	}
	    	
	    	$results .= "<tr bgcolor=\"$row_color\">\n";
	    	$results .= "<td valign=center align=left class=form><a href=\"$cgi_url?action=edit_inv_form&inv_id=$id" . $sp_vars . "\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"View\" title=\"View Invoice\"></a>";
	        if ( $doExpBtn ) {
	            $results .= "&nbsp;<input type=checkbox name=mrk_" . $id . " value=\"Y\">";
	        }
	    	$results .= "</td>\n";
	    	$results .= "<td valign=top align=left class=form>$invNo</td>\n";
	    	$results .= "<td valign=top align=left class=form>$invDt</td>\n";
	    	$results .= "<td valign=top align=left class=form>$lName</td>\n";
	    	$results .= "<td valign=top align=left class=form>$invStat</td>\n";
	    	$results .= "<td valign=top align=right class=form>$invAmt</td></tr>\n";
	    	
	    	$fndCnt++;
	    }
	    $results .= "</table>\n";
	    		
	    if ( $doExpBtn ) {
	        $results .= "</form>\n";
	    }
	    		
	    if ( $fndCnt == 0 ) {
	    	$results  = "No Invoices Found.";
	    }
	    
	    $conf->{inv_cnt}        = "$fndCnt";
    	$conf->{search_results} = $content . $results;
	
    } else {
	
	    $conf->{inv_cnt}        = "0";
	    $conf->{search_results} = "<span class=subhead><b>No search criteria specified.</b></span><br>\n"
	    	                    . "<a href=\"$cgi_url\" class=nav><b>[ Reset ]</b></a>\n";
	
    }

	&print_main;
	
}

############################################################
# Form to edit member of database
############################################################
sub edit_inv_form {

    my ( $inv_id ) = @_;
    
    if ( $inv_id eq "" ) {
        $inv_id  = $shared->get_cgi( "inv_id" );
    }
    
    my $inv_rec = $shared->get_rec( "invoice", "inv_id", $inv_id );
    my $invNo   = $inv_rec->{inv_no};
    my $invDt   = $shared->getDateFmt( $inv_rec->{inv_date} );
    my $invStat = $inv_rec->{inv_status};
    my $lig_id  = $inv_rec->{inv_ling_id};
    my $lig_rec = $shared->get_rec( "linguist", "lig_id", $lig_id );
    my $lName   = $lig_rec->{lig_first_name} . " " . $lig_rec->{lig_last_name};
    my $lPmtPP  = $lig_rec->{lig_pmt_paypal};
    my $lPmtPPE = $lig_rec->{lig_pmt_paypal_eml};
    my $lPmtChk = $lig_rec->{lig_pmt_check};
    my $lPmtWir = $lig_rec->{lig_pmt_wire};
    my $lCurr   = $lig_rec->{lig_currency};
    my $lAdd    = $lig_rec->{lig_address};
    my $lCity   = $lig_rec->{lig_city};
    my $lState  = $lig_rec->{lig_state};
    my $lZip    = $lig_rec->{lig_zip};
    my $lCntry  = $lig_rec->{lig_country};
    my $lPhn    = $lig_rec->{lig_phone};
    my $lFax    = $lig_rec->{lig_fax};
    my $lEml    = $lig_rec->{lig_email};
    my $invAmt  = $shared->fmtCurrency( $inv_rec->{inv_amount}, $lCurr );

    
    my $poList  = "<table border=0 cellpadding=2 cellspacing=0 width=600>\n"
                . "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n"
                . "<table border=0 cellpadding=2 cellspacing=0 width=596>\n"
                . "<tr bgcolor=\"black\">\n"
                . "<td valign=top align=left class=form><font color=\"white\"><b>PO Number</b></td>\n"
                . "<td valign=top align=left class=form><font color=\"white\"><b>Date</b></td>\n"
                . "<td valign=top align=left class=form><font color=\"white\"><b>Content Type</b></td>\n"
                . "<td valign=top align=left class=form><font color=\"white\"><b>Client</b></td>\n"
                . "<td valign=top align=left class=form><font color=\"white\"><b>Proj No</b></td>\n"
                . "<td valign=top align=right class=form><font color=\"white\"><b>PO Amount</b></td></tr>\n";

    my $ctcCnt    = 0;
    my $ctcList   = "<table border=0 cellpadding=2 cellspacing=0 width=450>\n";
    if ( $lAdd gt "" ) {
        $ctcList .= "<tr><td valign=top align=left class=label width=60>Address</td>\n";
        $ctcList .= "<td valign=top align=left class=form width=390>$lAdd<br>$lCity, $lState&nbsp;&nbsp;$lZip&nbsp;&nbsp;$lCntry</td></tr>\n";
        $ctcCnt++;
    }
    if ( $lPhn gt "" ) {
        $ctcList .= "<tr><td valign=top align=left class=label width=60>Phone</td>\n";
        $ctcList .= "<td valign=top align=left class=form width=390>$lPhn</td></tr>\n";
        $ctcCnt++;
    }
    if ( $lFax gt "" ) {
        $ctcList .= "<tr><td valign=top align=left class=label width=60>Fax</td>\n";
        $ctcList .= "<td valign=top align=left class=form width=390>$lFax</td></tr>\n";
        $ctcCnt++;
    }
    if ( $lEml gt "" ) {
        $ctcList .= "<tr><td valign=top align=left class=label width=60>E-mail</td>\n";
        $ctcList .= "<td valign=top align=left class=form width=390><a href=\"mailto:$lEml\">$lEml</a></td></tr>\n";
        $ctcCnt++;
    }
    $ctcList .= "</table>\n";
    
    if ( $ctcCnt == 0 ) {
        $ctcList = "No Contact Info Provided.";
    }
    
    my $row     = 0;
    my $row_color;
    my $poTot   = 0;
    
    my $pSQL    = "SELECT pol_pono, pol_add_date, pol_total_amt, pol_content_type, pol_proj_no, cln_name\n"
                . "FROM po_log\n"
                . "INNER JOIN client ON cln_id = pol_cln_id\n"
                . "WHERE pol_inv_id = $inv_id\n"
                . "ORDER BY pol_pono";
    my @prs     = $shared->getResultSet( $pSQL );
    foreach my $p_rec ( @prs ) {
        my $pno  = $p_rec->{pol_pono};
        my $prj  = $p_rec->{pol_proj_no};
        my $cln  = $p_rec->{cln_name};
        my $pdt  = $shared->getDateFmt( $p_rec->{pol_add_date} );
        my $amt  = $shared->fmtCurrency( $p_rec->{pol_total_amt}, $lCurr );
        my $cTp  = $shared->getLookupDesc( "PO_CONTENT_TYPE", $p_rec->{pol_content_type} );
        $poTot  += $p_rec->{pol_total_amt};
        if ( $row == 0 ) {
            $row_color = $color_bg1;
            $row = 1;
        } else {
            $row_color = $color_bg2;
            $row = 0;
        }
   	    $poList .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form>$pno</td>\n"
   	             . "<td valign=top align=left class=form>$pdt</td>\n"
   	             . "<td valign=top align=left class=form>$cTp</td>\n"
   	             . "<td valign=top align=left class=form>$cln</td>\n"
   	             . "<td valign=top align=left class=form>$prj</td>\n"
   	             . "<td valign=top align=right class=form>$amt</td></tr>\n";
   	}
   	my $dspTot   = $shared->fmtCurrency( $poTot, $lCurr );
   	$poList .= "<tr><td valign=top align=left class=form colspan=3><b>Total</b></td>\n";
   	$poList .= "<td valign=top align=right class=form><b>$dspTot</b></td></tr>\n";
   	$poList .= "</table>\n";
   	$poList .= "</td></tr>\n";
   	$poList .= "</table>\n";
    
    my $pmtMethods   = "";
    if ( $lPmtPP eq "Y" ) {
        $pmtMethods .= "Paypal";
        if ( $lPmtPPE gt "" ) {
            $pmtMethods .= " - (" . $lPmtPPE . ")";
        }
        $pmtMethods .= "<br>";
    }
    if ( $lPmtChk eq "Y" ) {
        $pmtMethods .= "Check<br>";
    }
    if ( $lPmtWir eq "Y" ) {
        $pmtMethods .= "Wire<br>";
    }
    $pmtMethods .= "Currency: $lCurr";
    
    
	my $srch_invno   = $hash_vars->{srch_invno};
	my $srch_ling    = $hash_vars->{srch_ling};
	my $srch_stat    = $hash_vars->{srch_stat};
	
    my $inp_srch     = "";
    my $sp_vars      = "";
    if ( $srch_invno gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_invno value=\"$srch_invno\">\n";
    	$sp_vars  .= "&srch_invno=$srch_invno";
    }
    if ( $srch_ling gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_ling value=\"$srch_ling\">\n";
    	$sp_vars  .= "&srch_ling=$srch_ling";
    }
    if ( $srch_stat gt "" ) {
        $inp_srch .= "<input type=hidden name=srch_stat value=\"$srch_stat\">\n";
        $sp_vars  .= "&srch_stat=$srch_stat";
    }
    if ( $inp_srch eq "" ) {
    	$inp_srch .= "<input type=hidden name=srch_init value=\"Y\">\n";
    	$sp_vars  .= "&srch_init=Y";
    }

    $sp_vars =~ s/^\&//;
    
   	$conf->{form_head}            = "Invoice Details";
   	$conf->{form_start}           = "<form method=post name=inv_edit_form action=\"$cgi_url\">\n"
   	                              . "<input type=hidden name=action value=\"edit_inv\">\n"
   	                              . "<input type=hidden name=inv_id value=\"$inv_id\">\n"
   	                              . $inp_srch;
   	                              
    $conf->{f_lig_name}           = $lName;
    $conf->{f_ctc_info}           = $ctcList;
    $conf->{f_inv_no}             = "<a href=\"$cgi_url?action=view_invoice&inv_id=$inv_id\" target=_blank>$invNo</a>";
    $conf->{f_inv_date}           = $invDt;
    $conf->{f_inv_amount}         = $invAmt;
    $conf->{f_pmt_methods}        = $pmtMethods;
    $conf->{f_po_list}            = $poList;
    
    $conf->{f_inv_status}         = "<select name=inv_status class=form>"
                                  . $shared->pop_lookup_select( "INV_STAT", $invStat, "ORDER BY lku_sort" )
                                  . "</select>";
    if ( $invStat eq "PEND" ) {
        $conf->{form_submit}      = "<table border=0 cellpadding=0 cellspacing=0 width=100%><tr><td valign=top align=left class=form>\n"
   	                              . "<input type=submit value=\"Update Invoice\" class=form>&nbsp;\n"
                                  . "<a href=\"$cgi_url?action=del_invoice&inv_id=$inv_id&$sp_vars\" onclick=\"return confirm('Are you sure?')\" class=btn_action_red>Delete Invoice</a>&nbsp;\n"
                                  . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?$sp_vars'\" class=form></td>\n"
                                  . "<td valign=top align=right class=form>\n"
                                  . "<input type=button value=\"Audit Log\" onClick=\"parent.location.href='$cgi_url?action=audit_log&inv_id=$inv_id&$sp_vars'\" class=form></td></tr>"
                                  . "</table>";
    } else {
        $conf->{form_submit}      = "<table border=0 cellpadding=0 cellspacing=0 width=100%><tr><td valign=top align=left class=form>\n"
   	                              . "<input type=submit value=\"Update Invoice\" class=form>&nbsp;\n"
                                  . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?$sp_vars'\" class=form></td>\n"
                                  . "<td valign=top align=right class=form>\n"
                                  . "<input type=button value=\"Audit Log\" onClick=\"parent.location.href='$cgi_url?action=audit_log&inv_id=$inv_id&$sp_vars'\" class=form></td></tr>"
                                  . "</table>";
    }            

   	$conf->{form_end}     = "</form>";
    	
    
    my $pg_body = &disp_form( "inv_edit_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

############################################################
# Update the record to the members database
############################################################
sub edit_inv {

  	my $srch_init = $shared->get_cgi( "srch_init" );

    my $inv_id    = $shared->get_cgi( "inv_id" );
    my $inv_stat  = $shared->get_cgi( "inv_status" );
    
    my $cur_rec   = $shared->get_rec( "invoice", "inv_id", $inv_id );
    my $p_stat    = $cur_rec->{inv_status};
           	
   	my $chg_dtls = "";

  	if ( $inv_stat ne $p_stat ) {
  	    my $pDesc  = $shared->getLookupDesc( "INV_STAT", $p_stat );
  	    my $nDesc  = $shared->getLookupDesc( "INV_STAT", $inv_stat );
   	    $chg_dtls .= "Invoice Status: from $pDesc to $nDesc<br>\n";
   	}
   	
   	if ( $chg_dtls gt "" ) {
   		chomp( $chg_dtls );
   		$chg_dtls =~ s/<br>$//;
   	}
	
   	my $update   = "UPDATE invoice SET inv_status = '$inv_stat' WHERE inv_id = $inv_id";
   	$shared->doSQLUpdate( $update );

   	if ( $chg_dtls gt "" ) {
   		&add_audit( $inv_id, "Invoice Record Updated...<br>\n" . $chg_dtls );
   	}
    	
    &search_any( $srch_init );

}

############################################################
# Delete Invoice from database and reset PO status
############################################################
sub del_invoice {

  	my $srch_init = $shared->get_cgi( "srch_init" );

    my $inv_id      = $shared->get_cgi( "inv_id" );
    
    my $rec         = $shared->get_rec( "invoice", "inv_id", $inv_id );
    my $fileNm      = $rec->{inv_filename};
    my $ling_id     = $rec->{inv_ling_id};
    my $subD        = substr( $ling_id, 0, 1 );
    my $uPath       = $out_path . "/invoice/" . $subD;
    my $fName       = "$uPath/$fileNm";
           	
    my $up1         = "DELETE FROM invoice_audit WHERE ina_inv_id = $inv_id";
    my $up2         = "UPDATE po_log SET pol_invoice_recd = 'N', pol_inv_id = 0 WHERE pol_inv_id = $inv_id";
    my $up3         = "DELETE FROM invoice WHERE inv_id = $inv_id";
    
    $shared->doSQLUpdate( $up1 );
    $shared->doSQLUpdate( $up2 );
    $shared->doSQLUpdate( $up3 );
    
    unlink $fName;
    	
    &search_any( $srch_init );

}

###########################################################                   
# Get Linguist selection list
#----------------------------------------------------------
sub selLing {
    
    my $SQL = "SELECT lig_id, lig_first_name, lig_last_name FROM linguist ORDER BY lig_first_name, lig_last_name";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{lig_id};
        my $desc     = $rec->{lig_first_name} . " " . $rec->{lig_last_name};
        $result .= "<option value=\"$code\">$desc</option>";
    }
    
    return $result;
}

###########################################################                   
# Stream Invoice PDF to browser
#----------------------------------------------------------
sub view_invoice {
    
    my ( $inv_id )  = @_;

    my $rec         = $shared->get_rec( "invoice", "inv_id", $inv_id );
    my $fileNm      = $rec->{inv_filename};
    my $ling_id     = $rec->{inv_ling_id};
    my $subD        = substr( $ling_id, 0, 1 );
    my $uPath       = $out_path . "/invoice/" . $subD;
    my $fName       = "$uPath/$fileNm";
    
    $shared->streamPDF( $fName );

}


###########################################################
# Add Audit Log Record
#----------------------------------------------------------
sub add_audit {
	
	my ( $id, $desc ) = @_;
	
	$desc =~ s/'/\\'/g;
	
	my $insert = "INSERT INTO invoice_audit ( ina_inv_id, ina_usr_id, ina_date, ina_description )\n"
	           . "VALUES ( $id, $usr_id, now(), '$desc' )";
	$shared->doSQLUpdate( $insert );
	
}

###########################################################
# View Audit Log for a specific application
#----------------------------------------------------------
sub audit_log {

    my $inv_id  = $shared->get_cgi( "inv_id" );
    my $inv_rec = $shared->get_rec( "invoice", "inv_id", $inv_id );
    my $lig_id  = $inv_rec->{inv_ling_id};
    my $lig_rec = $shared->get_rec( "linguist", "lig_id", $lig_id );
  
    my $name    = $lig_rec->{lig_first_name} . " " . $lig_rec->{lig_last_name};
    my $invNo   = $inv_rec->{inv_no};
    my $invAmt  = $shared->fmtNumber( $inv_rec->{inv_amount} );
    
    my $SQL     = "SELECT ina_usr_id, usr_name, ina_date, ina_description\n"
                . "FROM invoice_audit\n"
                . "LEFT JOIN user ON ina_usr_id = usr_id\n"
                . "WHERE ina_inv_id = $inv_id\n"
                . "ORDER BY ina_date DESC";
                
	my $content;
    $content .= "<span class=subhead>Linguist: $name<br>\n";
    $content .= "Invoice: $invNo ($invAmt)</span><br><br>\n";
    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
    $content .= "<tr bgcolor=\"black\">\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Log Date</b></font></td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>User</b></font></td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Description</b></font></td></tr>\n";
    
    my @rs    = $shared->getResultSet( $SQL );
    my $row  = 0;
    my $row_color;

	foreach my $rec ( @rs ) {
	   	my $log_date = $shared->getDateFmt( $rec->{ina_date} );
	   	my $uID      = $rec->{ina_usr_id};
	   	my $uName    = "N\A";
	   	if ( $uID > 0 ) {
	   	    $uName   = $rec->{usr_name};
	   	}
	   	my $desc     = $rec->{ina_description};
	    	
    	if ( $row == 0 ) {
    		$row_color = $color_bg2;
    		$row = 1;
    	} else {
    		$row_color = $color_bg1;
    		$row = 0;
    	}
	    	
    	$content .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form>$log_date</td>\n";
	    $content .= "<td valign=top align=left class=form>$uName</td>\n";
	    $content .= "<td valign=top align=left class=form>$desc</td></tr>\n";
	    
    }
    $content .= "</table>\n";

   	$conf->{search_results} = $content;
   	
    my $pg_body = &disp_form( "inv_audit" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
   	
}

############################################################
# Generate Invoice Pending List (Excel)
############################################################
sub genExpList {

    my $rptDate  = $shared->getSQLToday;

    my $result   = "";
        
	my $fName    = "$forms_path/pending_invoices.xlsx";
	my $urlName  = "$forms_url/pending_invoices.xlsx";
	
	my $workbook = Excel::Writer::XLSX->new( $fName );
	$workbook->set_tempdir( $temp_path );
	
	my $c_litegrey    = $workbook->set_custom_color( 40, 204, 204, 204 );
	my $c_litegreen   = $workbook->set_custom_color( 41, 210, 255, 214 );
    my %colFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'bottom', bold => 1, bg_color => $c_litegrey );
    
    my %bodyFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', border => 1, border_color => $c_litegrey );
    my %boldFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, border => 1, border_color => $c_litegrey );
    my %dateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', num_format => 'mm/dd/yyyy', border => 1, border_color => $c_litegrey );
    my %numFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %boldNumFont   = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right' );
    my %currFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %boldCurrFont  = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right', border => 1, border_color => $c_litegrey );
    my %rateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %pctFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
	
	my $fmtCol   = $workbook->add_format( %colFont );
	my $fmtBody  = $workbook->add_format( %bodyFont );
	my $fmtBold  = $workbook->add_format( %boldFont );
	my $fmtDate  = $workbook->add_format( %dateFont );
	my $fmtNum   = $workbook->add_format( %numFont );
	my $fmtBNum  = $workbook->add_format( %boldNumFont );
	my $fmtCurr  = $workbook->add_format( %currFont );
	my $fmtBCurr = $workbook->add_format( %boldCurrFont );
	my $fmtRate  = $workbook->add_format( %rateFont );
	my $fmtPct   = $workbook->add_format( %pctFont );
	
	$fmtCurr->set_num_format( "\$0.00" );
	$fmtBCurr->set_num_format( "\$0.00" );
	$fmtRate->set_num_format( "\$0.0000" );
	$fmtPct->set_num_format( "0.00\%" );
	$fmtNum->set_num_format( "0.00" );

    my $SQL = "SELECT inv_id, inv_no, lig_first_name, lig_last_name, inv_date,\n"
            . "pol_id, pol_pono, pol_add_date, pol_total_amt, pol_content_type,\n"
            . "pol_proj_no, cln_name, lig_currency\n"
            . "FROM invoice\n"
            . "INNER JOIN po_log ON pol_inv_id = inv_id\n"
            . "INNER JOIN linguist ON lig_id = inv_ling_id\n"
            . "INNER JOIN client ON cln_id = pol_cln_id\n"
            . "WHERE inv_status = 'PEND'\n"
            . "ORDER BY inv_id, pol_id";

    my @rs       = $shared->getResultSet( $SQL );
    	             
    my $sheet    = $workbook->add_worksheet( "Pend Invoice" );
            
    my @colHead  = ( "Bill No", "Linguist Name", "Invoice Date", "Category", 
                     "Project Date", "Project Memo", "Amount", "Currency" );
    $sheet->write_row( 0, 0, \@colHead, $fmtCol );
        
    my $row  = 1;
	foreach my $rec ( @rs ) {

  	    my $invNo   = $rec->{inv_no};
	    my $ligName = $rec->{lig_first_name} . " " . $rec->{lig_last_name};
	    my $invDt   = $rec->{inv_date};
	    my $prjDt   = $rec->{pol_add_date};
	    my $pono    = $rec->{pol_pono};
	    my $cType   = $rec->{pol_content_type};
	    my $clnName = $rec->{cln_name};
	    my $prjNo   = $rec->{pol_proj_no};
	    my $poAmt   = ( $rec->{pol_total_amt} ) + 0;
	    my $curCd   = $rec->{lig_currency};

        my $prjMemo = $pono . "    " . $cType . "    " . $clnName . "    " . $prjNo;
                    
        $sheet->write_string( $row, 0, $invNo, $fmtBody );
        $sheet->write_string( $row, 1, $ligName, $fmtBody );
        $sheet->write_date_time( $row, 2, $invDt . "T", $fmtDate );
        $sheet->write_string( $row, 3, " ", $fmtBody );
        $sheet->write_date_time( $row, 4, $prjDt . "T", $fmtDate );
        $sheet->write_string( $row, 5, $prjMemo, $fmtBody );
        $sheet->write_number( $row, 6, $poAmt, $fmtNum );
        $sheet->write_string( $row, 7, $curCd, $fmtBody );
        
        $row++;
    }
    
    $sheet->set_column( 0, 0, 14 );
    $sheet->set_column( 1, 1, 28 );
    $sheet->set_column( 2, 2, 14 );
    $sheet->set_column( 3, 3, 65 );
    $sheet->set_column( 4, 4, 14 );
    $sheet->set_column( 5, 5, 70 );
    $sheet->set_column( 6, 7, 14 );

    $sheet->hide_gridlines( 2 );
    $sheet->freeze_panes( 1, 0 );

    $workbook->close();
    
    system( "chmod 644 $fName" );
        
    $result = $urlName;
    
    return $result;

}

############################################################
# Update all marked invoices (invoiced)
############################################################
sub mark_inv_all {

    my $SQL = "SELECT inv_id, inv_no\n"
	        . "FROM invoice\n"
	        . "WHERE inv_status = 'PEND'\n"
	        . "ORDER BY inv_id";
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        
        my $inv_id = $rec->{inv_id};
        my $inv_no = $rec->{inv_no};
        
        my $fldChk = "mrk_" . $inv_id;
        my $fldVal = $shared->get_cgi( $fldChk );
        if ( $fldVal eq "Y" ) {
            my $update = "UPDATE invoice SET inv_status = 'PROC' WHERE inv_id = $inv_id";
            $shared->doSQLUpdate( $update );
            my $desc   = "Invoice Record Updated...<br>Invoice Status: from Submitted to Invoiced";
            &add_audit( $inv_id, $desc );
        }
    }
}
