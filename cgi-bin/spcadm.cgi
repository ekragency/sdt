#!/usr/bin/perl
###########################################################################
# spcadm.cgi                Language Administration
###########################################################################
# Copyright 2016, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/spcadm.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $exceptList    = "";
my $dupList       = "";
my $debug         = $config->get_debug;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;


# Main program
my $usr_id = 0;
my $action = $shared->get_cgi( "action" );
my $hash_vars;
my $next_action = $shared->get_cgi( "next_act" );

$hash_vars->{srch_desc}    = $shared->get_cgi( "srch_desc" );
$hash_vars->{srch_init}    = $shared->get_cgi( "srch_init" );
$hash_vars->{next_act}     = $next_action;

my $srch_desc    = $hash_vars->{srch_desc};
my $srch_init    = $hash_vars->{srch_init};
	
my $inp_srch     = "";
my $sp_vars      = "";
if ( $srch_desc gt "" ) {
	$inp_srch .= "<input type=hidden name=srch_desc value=\"$srch_desc\">\n";
   	$sp_vars  .= "&srch_desc=$srch_desc";
}
if ( $inp_srch eq "" ) {
  	$inp_srch .= "<input type=hidden name=srch_init value=\"Y\">\n";
   	$sp_vars  .= "&srch_init=Y";
   	$srch_init = "Y";
} else {
    $inp_srch .= "<input type=hidden name=srch_init value=\"N\">\n";
    $sp_vars  .= "&srch_init=N";
    $srch_init = "N";
}


#
# Main Program
#
 
if ( $auth->validate_access( "spcadm" ) ) {
	
	$usr_id    = $auth->getCurrentUser;

    if ( $action eq "search_any" ) {
        &search_any;
    } elsif ( $action eq "edit_spc_form" ) {
        &edit_spc_form;
    } elsif ( $action eq "edit_spc" ) {
        &edit_spc;
    } elsif ( $action eq "add_spc_form" ) {
        &add_spc_form;
    } elsif ( $action eq "add_spc" ) {
        &add_spc;
    } elsif ( $action eq "del_spc" ) {
        &del_spc;
    } else {
        &search_any;
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my $spc_cnt = $shared->getSQLCount( "SELECT count(*) FROM specialty WHERE spc_active = 'Y'" );

    if ( $conf->{spc_cnt} eq "" ) {
        $conf->{spc_cnt}     = $spc_cnt;
    }
    
   	my $abContent = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=add_spc_form>\n"
                  . $inp_srch
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Add New Specialty\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";

 	$conf->{add_button}  = $abContent;
    
    my $pg_body              = &disp_form( "spc_main" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my $content;
	
	my $srch_head    = "<b>Searched:</b>&nbsp;";
	my $srch_par     = 0;
	
	if ( ( $srch_desc gt "" ) || ( $srch_init eq "Y" ) ) {
	
	    my $SQL  = "SELECT spc_id, spc_description\n"
	             . "FROM specialty\n"
	             . "WHERE 1\n";

	    if ( $srch_desc gt "" ) {
	    	my $spc_srch_desc = $srch_desc;
	    	$spc_srch_desc =~ s/'/\\'/g;
		    $SQL .= "AND spc_description LIKE '$spc_srch_desc%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Specialty starts with <b>$srch_desc</b>";
		    } else {
		        $srch_head .= "Specialty starts with <b>$srch_desc</b>";
		    }
		    $srch_par++;
	    }
	    
	    $SQL     .= "ORDER BY spc_description";
	    
	    my $doReset = 0;
	    if ( ( $srch_init eq "Y" ) && ( $srch_par == 0 ) ) {
	        
	    	$srch_head = "All Specialties";
	    } else {
	        $doReset = 1;
	    }
	
	    my @rs   = $shared->getResultSet( $SQL );
	    my $row  = 0;
	    my $row_color;
	    my $fndCnt = 0;
	    
	    $content .= "<span class=subhead><b>$srch_head</b></span><br><br>\n";
	    if ( $doReset ) {
	    	$content .= "<a href=\"$cgi_url\" class=nav><b>[ Clear Filter ]</b></a><br>\n";
	    }
	    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	    $content .= "<tr bgcolor=\"black\">\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Specialty ID</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Specialty</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
	    foreach my $rec ( @rs ) {
	    	my $id      = $rec->{spc_id};
	    	my $desc    = $rec->{spc_description};
	    	
	    	if ( $row == 0 ) {
	    		$row_color = $color_bg2;
	    		$row = 1;
	    	} else {
	    		$row_color = $color_bg1;
	    		$row = 0;
	    	}
	    	
	    	$content .= "<tr bgcolor=\"$row_color\">\n";
	    	$content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=edit_spc_form&spc_id=$id" . $sp_vars . "\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"View\" title=\"View $desc\"></a></td>\n";
	    	$content .= "<td valign=top align=left class=form>$id</td>\n";
	    	$content .= "<td valign=top align=left class=form>$desc</td>\n";
	    	$content .= "<td valign=top align=right class=form>";
    	    $content .= "<a href=\"$cgi_url?action=del_spc&spc_id=$id" . $sp_vars . "\" onClick=\"return confirm('Permanently remove $desc from database?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete $desc\"></a>";
	    	$content .= "</td></tr>\n";
	    	
	    	$fndCnt++;
	    }
	    $content .= "</table>\n";
	    		
	    if ( $fndCnt == 0 ) {
	    	$content  = "";
	    	$java_msg = "No Specialties found meeting search criteria.";
	    } else {
	    	$conf->{spc_cnt} = "$fndCnt";
	    }
	
    	$conf->{search_results} = $content;
	
	
    } else {
	
	    $java_msg = "No Search Criteria Specified.";        
	
    }

	&print_main;
	
}



############################################################
# Form to add member to database
############################################################
sub add_spc_form {

  	my $today = $shared->getSQLToday;
    	
   	$conf->{form_head}       = "Add New Specialty";
   	$conf->{form_start}      = "<form method=post name=adm_spc_form action=\"$cgi_url\">\n"
    	                     . "<input type=hidden name=action value=\"add_spc\">\n"
    	                     . $inp_srch;
   	$conf->{f_spc_desc}      = "<input name=spc_description value=\"\" size=30 maxlength=30 class=form>";
   	$conf->{form_submit}     = "<input type=submit value=\"Add Now\" class=form>&nbsp;\n"
                             . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}        = "</form>";
   	$conf->{form_name}       = "adm_spc_form";
   	$conf->{focus_field}     = "spc_description";  
    	
    my $pg_body = &disp_form( "spc_add_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

############################################################
# Add the record to the loan database
############################################################
sub add_spc {

   	my $spc_desc  = $shared->get_cgi( "spc_description" );
   	
    $spc_desc    =~ s/'/\\'/g;
    	
    my $insert   = "INSERT INTO specialty ( spc_description )\n"
                 . "VALUES ( '$spc_desc' )";
    $shared->doSQLUpdate( $insert );
        	
   	&search_any;

}

############################################################
# Form to edit member of database
############################################################
sub edit_spc_form {

    my ( $spc_id ) = @_;
    
    if ( $spc_id eq "" ) {
        $spc_id  = $shared->get_cgi( "spc_id" );
    }
    
    my $spc_rec = $shared->get_rec( "specialty", "spc_id", $spc_id );

    my $cDesc    = $spc_rec->{spc_description};
    
    $sp_vars =~ s/^\&//;
    
    $conf->{form_id_row}    = "<tr><td align=right valign=top class=label bgcolor=$color_bg2 width=150>ID</td>\n"
                            . "<td valign=top align=left class=form><b>$spc_id</b></td></tr>\n";

    
   	$conf->{form_head}       = "Update Specialty Details";
   	$conf->{form_start}      = "<form method=post name=spc_edit_form action=\"$cgi_url\">\n"
   	                         . "<input type=hidden name=action value=\"edit_spc\">\n"
   	                         . "<input type=hidden name=spc_id value=\"$spc_id\">\n"
   	                         . $inp_srch;
   	$conf->{f_spc_desc}      = "<input name=spc_description value=\"" . $cDesc . "\" size=30 maxlength=30 class=form>";
    	
   	$conf->{form_submit}     = "<input type=submit value=\"Update Specialty\" class=form>&nbsp;\n"
                             . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?$sp_vars'\" class=form>";
   	$conf->{form_end}        = "</form>";
    	
    my $pg_body = &disp_form( "spc_edit_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

############################################################
# Update the record to the members database
############################################################
sub edit_spc {

    my $spc_id    = $shared->get_cgi( "spc_id" );
   	my $spc_desc  = $shared->get_cgi( "spc_description" );

   	$spc_desc    =~ s/'/\\'/g;
   	
   	my $update   = "UPDATE specialty SET spc_description = '$spc_desc'\n"
   	             . "WHERE spc_id = $spc_id";
   	$shared->doSQLUpdate( $update );
    	
    &search_any;

}

###########################################################                   
# Delete Member from Database
#----------------------------------------------------------
sub del_spc {

   	my $spc_id = $shared->get_cgi( "spc_id" );
   	my $ls_cnt = $shared->getSQLCount( "SELECT COUNT(*) FROM linguist_spec WHERE lis_spc_id = $spc_id" );
   	
    if ( $ls_cnt == 0 ) {	
    	
   	    my $update = "DELETE FROM specialty WHERE spc_id = $spc_id";
   	    $shared->doSQLUpdate( $update );
    	
    } else {

        $java_msg = "Unable to Delete Specialty - Remove Associated Linguists First";

    }
    
    &search_any;
}


