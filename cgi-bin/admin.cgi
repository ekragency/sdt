#!/usr/bin/perl
###########################################################################
# ADMIN.CGI                    
# Administration CGI script for Utah Music Teachers Association
# Created 10/13/2004
###########################################################################
# Copyright 2004, P. Eric Huber
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;
use admin;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );
my $admin  = admin->new( $shared, $auth );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($base_url)    = $config->get_base_url;
my ($site_title)  = $config->get_app_name;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;
my ($mailhost)    = $config->get_mailhost;
my ($app_mode)    = $config->get_app_mode;
my ($page_date)   = "N";
my ($debug)       = $config->get_debug;

my ($conf);
$conf->{cgi_url}  = $cgi_url;
$conf->{img_path} = $img_path;

my (%colors)      = $config->get_colors;
my $color_head1   = $colors{ "HEAD1" };
my $color_head2   = $colors{ "HEAD2" };
my $color_head3   = $colors{ "HEAD3" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";

$conf->{color_head1} = $color_head1;
$conf->{color_head2} = $color_head2;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;
$conf->{color_error} = $color_error;
$conf->{cgi_url}     = $cgi_url;
$conf->{admin_email} = $config->get_admin_email;


# Template Variables
my ($env);
$env->{cgi_url}     = $cgi_url;
$env->{img_path}    = $img_path;
$env->{home_url}    = $base_url;
$env->{app_ver}     = $app_ver;
$env->{color_error} = $color_error;
$env->{template}    = "admin.html";
$env->{css}         = "style.css";

# Main program
my $usr_id = $auth->getCurrentUser;
my $action = $shared->get_cgi( "action" );

if ( $usr_id > 0 ) {
	
	$env->{login_stat}      = $auth->getLoginStatus;
	$env->{usr_id}          = $usr_id;
	$conf->{usr_login}      = $auth->getCurrentUserLogin;
	$conf->{usr_pass}       = $auth->getCurrentUserPass;
	$env->{admin_nav_links} = $auth->getUserOptions;
	
    if ( $action eq "chg_pass_form" ) {
    	$auth->chg_pass_form( $cgi_url );
    } elsif ( $action eq "chg_pass" ) {
    	$java_msg = $auth->chg_pass( $cgi_url );
    	&print_main;
    } elsif ( $action eq "logout" ) {
	    $auth->logout( $cgi_url );
    } elsif ( $action eq "user_maint" ) {
    	if ( $auth->validate_access( "admin_user_maint" ) ) {
            &user_maint;
        } else {
        	$shared->do_err( "Access denied to User Maintenance module.", "admin.html" );
        }
    } elsif ( $action eq "add_user_form" ) {
    	if ( $auth->validate_access( "admin_user_maint" ) ) {
            &add_user_form;
        } else {
        	$shared->do_err( "Access denied to User Maintenance module.", "admin.html" );
        }
    } elsif ( $action eq "add_user" ) {
    	if ( $auth->validate_access( "admin_user_maint" ) ) {
            &add_user;
        } else {
        	$shared->do_err( "Access denied to User Maintenance module.", "admin.html" );
        }
    } elsif ( $action eq "edit_user_form" ) {
    	if ( $auth->validate_access( "admin_user_maint" ) ) {
            &edit_user_form( "" );
        } else {
        	$shared->do_err( "Access denied to User Maintenance module.", "admin.html" );
        }
    } elsif ( $action eq "edit_user" ) {
    	if ( $auth->validate_access( "admin_user_maint" ) ) {
            &edit_user;
        } else {
        	$shared->do_err( "Access denied to User Maintenance module.", "admin.html" );
        }
    } elsif ( $action eq "unlock_user" ) {
    	if ( $auth->validate_access( "admin_user_maint" ) ) {
            &unlock_user;
        } else {
        	$shared->do_err( "Access denied to User Maintenance module.", "admin.html" );
        }
    } elsif ( $action eq "add_user_module" ) {
    	if ( $auth->validate_access( "admin_user_maint" ) ) {
            &add_user_module;
        } else {
        	$shared->do_err( "Access denied to User Maintenance module.", "admin.html" );
        }
    } elsif ( $action eq "del_user_module" ) {
    	if ( $auth->validate_access( "admin_user_maint" ) ) {
            &del_user_module;
        } else {
        	$shared->do_err( "Access denied to User Maintenance module.", "admin.html" );
        }
    } elsif ( $action eq "admin_config" ) {
    	if ( $auth->validate_access( "admin_config" ) ) {
            &admin_config;
        } else {
        	$shared->do_err( "Access denied to System Param module.", "admin.html" );
        }
    } elsif ( $action eq "admin_config_update" ) {
    	if ( $auth->validate_access( "admin_config" ) ) {
            &admin_config_update;
        } else {
        	$shared->do_err( "Access denied to System Param module.", "admin.html" );
        }
    } elsif ( $action eq "browse" ) {
	    &doBrowse;
    } elsif ( $action eq "add_form" ) {
	    &doAddForm;
    } elsif ( $action eq "add" ) {
	    &doAdd;
    } elsif ( $action eq "edit_form" ) {
	    &doEditForm;
    } elsif ( $action eq "edit" ) {
	    &doEdit;
    } elsif ( $action eq "drop" ) {
	    &doDrop;
    } elsif ( $action eq "order_up" ) {
	    &doOrderUp;
    } elsif ( $action eq "order_dn" ) {
	    &doOrderDn;
	} elsif ( $action eq "log_maint" ) {
    	if ( $auth->validate_access( "admin_log_maint" ) ) {
            &log_maint;
        } else {
        	$shared->do_err( "Access denied to Log Maintenance module.", "admin.html" );
        }
    } else {
    	&print_main;
    }
} else {
	if ( $action eq "" ) {
		$auth->login( $cgi_url, "Y" );
	} else {
	    $auth->login( $cgi_url, "N" );
	}
}

exit;


###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


###########################################################                   
# Print Main Admin Options screen
#----------------------------------------------------------
sub print_main {

    my $pg_body = "<table border=0 cellpadding=0 cellspacing=0 width=530>\n";
    $pg_body   .= "<tr><td valign=top align=left class=admincontent>Welcome to the Metafraze Tools site.  The options that are available to you\n";
    $pg_body   .= "are listed on the left.  If you don't have an option you expect to have, please\n";
    $pg_body   .= "contact the administrator to get access.<br><br>Thank you...</td></tr>\n";
    $pg_body   .= "</table>\n";
    
    $env->{html_title} = "$site_title - Welcome";
    $env->{page_head}  = "Welcome";
    $env->{page_body}  = $pg_body;

    $shared->page_gen( $env, $java_msg );

}

###########################################################                   
# Browse Database
#----------------------------------------------------------
sub doBrowse {
	
	my $mnt_code = $shared->get_cgi( "mnt_code" );
	
	if ( $admin->browse( $mnt_code, $cgi_url ) ) {
	} else {
		$shared->do_err( "Invalid mnt_code passed.  Contact administrator.", "admin.html" );
    }
}
	    
	
###########################################################                   
# Add Form - maintain database tables.
#----------------------------------------------------------
sub doAddForm {

	my $mnt_code = $shared->get_cgi( "mnt_code" );
	
	if ( $admin->add_form( $mnt_code, $cgi_url ) ) {
	} else {
		$shared->do_err( "Invalid mnt_code passed.  Contact administrator.", "admin.html" );
    }
	
}


###########################################################                   
# Add Form - maintain database tables.
#----------------------------------------------------------
sub doAdd {

	my $mnt_code = $shared->get_cgi( "mnt_code" );
	
	if ( $admin->add_rec( $mnt_code ) ) {
	    &doBrowse;
	} else {
		$shared->do_err( "Invalid mnt_code passed.  Contact administrator.", "admin.html" );
    }
	
}

###########################################################                   
# Edit Form - maintain database tables.
#----------------------------------------------------------
sub doEditForm {

	my $mnt_code = $shared->get_cgi( "mnt_code" );

	if ( $admin->edit_form( $mnt_code, $cgi_url ) ) {
	} else {
		$shared->do_err( "Invalid mnt_code passed.  Contact administrator.", "admin.html" );
    }
	
}

###########################################################                   
# Edit Record - maintain database tables.
#----------------------------------------------------------
sub doEdit {

	my $mnt_code = $shared->get_cgi( "mnt_code" );
	
	if ( $admin->edit_rec( $mnt_code ) ) {
        &doBrowse;
	} else {
		$shared->do_err( "Invalid mnt_code passed.  Contact administrator.", "admin.html" );
    }
	
}

###########################################################                   
# Drop Record - maintain database tables.
#----------------------------------------------------------
sub doDrop {

	my $mnt_code = $shared->get_cgi( "mnt_code" );
	
	if ( $admin->drop_rec( $mnt_code ) ) {
	    &doBrowse;
	} else {
		$shared->do_err( "Invalid mnt_code passed.  Contact administrator.", "admin.html" );
    }
	
}

###########################################################                   
# Swap the order number with the record up from current.
#----------------------------------------------------------
sub doOrderUp {
	
	my $mnt_code  = $shared->get_cgi( "mnt_code" );
	my $cur_order = $shared->get_cgi( "cur_order" );
	my $order_fld = $shared->get_cgi( "order_fld" );
	my $syh_rec   = $shared->get_rec( "sys_head", "syh_code", $mnt_code );
	my $key_field = $syh_rec->{syh_key_field};
	my $table     = $syh_rec->{syh_table};
	my $revOrd    = $syh_rec->{syh_ord_reverse};
	my $key_val   = $shared->get_cgi( $key_field );

    my $SQL;
    if ( $revOrd eq "Y" ) {
        $SQL = "SELECT $key_field, $order_fld FROM $table WHERE $order_fld < $cur_order ORDER BY $order_fld DESC";
    } else {
        $SQL = "SELECT $key_field, $order_fld FROM $table WHERE $order_fld > $cur_order ORDER BY $order_fld";
    }

    if ( $debug ) {
    	open( DEBUG, ">>$src_path/debug.log" );
    	print DEBUG "###OPENING DEBUG FILE - doOrderUp subroutine###\n";
    	print DEBUG "mnt_code:  [$mnt_code]\n";
    	print DEBUG "cur_order: [$cur_order]\n";
    	print DEBUG "order_fld: [$order_fld]\n";
    	print DEBUG "key_field: [$key_field]\n";
    	print DEBUG "table:     [$table]\n";
    	print DEBUG "key_val:   [$key_val]\n";
    	print DEBUG "SQL:       $SQL\n";
    	close( DEBUG );
    }
    	
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
    	my $new_key_val = $rec->{$key_field};
    	my $new_ord_val = $rec->{$order_fld};
    	
    	my $up1 = "UPDATE $table SET $order_fld = $new_ord_val WHERE $key_field = $key_val";
    	my $up2 = "UPDATE $table SET $order_fld = $cur_order WHERE $key_field = $new_key_val";
    	
    	$shared->doSQLUpdate( $up1 );
    	$shared->doSQLUpdate( $up2 );
    	
    	last;
    }
    
    &doBrowse;
    
}

###########################################################                   
# Swap the order number with the record up from current.
#----------------------------------------------------------
sub doOrderDn {
	
	my $mnt_code  = $shared->get_cgi( "mnt_code" );
	my $cur_order = $shared->get_cgi( "cur_order" );
	my $order_fld = $shared->get_cgi( "order_fld" );
	my $syh_rec   = $shared->get_rec( "sys_head", "syh_code", $mnt_code );
	my $key_field = $syh_rec->{syh_key_field};
	my $table     = $syh_rec->{syh_table};
	my $revOrd    = $syh_rec->{syh_ord_reverse};
	my $key_val   = $shared->get_cgi( $key_field );

    my $SQL;
    if ( $revOrd eq "Y" ) {
        $SQL = "SELECT $key_field, $order_fld FROM $table WHERE $order_fld > $cur_order ORDER BY $order_fld";
    } else {
        $SQL = "SELECT $key_field, $order_fld FROM $table WHERE $order_fld < $cur_order ORDER BY $order_fld DESC";
    }
    
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
    	my $new_key_val = $rec->{$key_field};
    	my $new_ord_val = $rec->{$order_fld};
    	
    	my $up1 = "UPDATE $table SET $order_fld = $new_ord_val WHERE $key_field = $key_val";
    	my $up2 = "UPDATE $table SET $order_fld = $cur_order WHERE $key_field = $new_key_val";
    	
    	$shared->doSQLUpdate( $up1 );
    	$shared->doSQLUpdate( $up2 );
    	
    	last;
    }
    
    &doBrowse;
    
}

###########################################################                   
# Maintain Users
#----------------------------------------------------------
sub user_maint {

    my $in_width;
    my $row;
    my $row_color;
    
    my $SQL = "SELECT * FROM user ORDER BY usr_login";
    my @rs  = $shared->getResultSet( $SQL );
    
        
    my $pg_body  = "<form method=post action=\"$cgi_url\">\n";
    $pg_body .= "<input type=hidden name=action value=add_user_form>\n";
    
    
    $pg_body .= "<table border=0 cellpadding=0 cellspacing=0 width=\"640\">\n";
    $pg_body .= "<tr><td>\n";
    $pg_body .= "<div class=blur><div class=shadow><div class=framework>\n";
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=\"640\">\n";
    $pg_body .= "<tr bgcolor=$color_bg1><td valign=top align=left>\n";
    $pg_body .= "<table border=0 cellpadding=3 cellspacing=0 width=\"636\">\n";
    $pg_body .= "<tr bgcolor=$color_head2><td valign=top align=left class=colhead width=80>User ID</td>\n";
    $pg_body .= "<td valign=top align=left class=colhead width=150>Name</td>\n";
    $pg_body .= "<td valign=top align=left class=colhead width=100>Status</td>\n";
    $pg_body .= "<td valign=top class=rightcolhead width=80><input type=submit value=\"New User\" class=form></td></tr>\n";
    
    $row = 0;
    
    foreach my $usr_rec ( @rs ) {
        my $uID       = $usr_rec->{usr_id};
        my $usr_login = $usr_rec->{usr_login};
        my $usr_name  = $usr_rec->{usr_name};
        my $usr_b_cnt = $usr_rec->{usr_bad_login_cnt};
        my $usr_lock  = $usr_rec->{usr_lock_flag};
        my $usr_actv  = $usr_rec->{usr_active};
        
        if ( $row == 0 ) {
            $row = 1;
            $row_color = $color_bg1;
        } else {
            $row = 0;
            $row_color = $color_bg2;
        }
        
        my $act_stat = "ACTIVE";
        if ( $usr_actv eq "N" ) {
        	$act_stat = "INACTIVE";
        }
        
        my $usr_stat = "$act_stat - OK";
        if ( $usr_lock eq "Y" ) {
            $usr_stat = "$act_stat - Access Locked";
        } elsif ( $usr_b_cnt > 0 ) {
            $usr_stat = "$act_stat - $usr_b_cnt Attempt(s)";
        }
        
        $pg_body .= "<tr bgcolor=$row_color><td valign=top align=left class=colbody><b>$usr_login</b></td>";
        $pg_body .= "<td valign=top align=left class=colbody>$usr_name</td>";
        $pg_body .= "<td valign=top align=left class=colbody>$usr_stat</td>";
        $pg_body .= "<td valign=top align=right class=colbody>";
        $pg_body .= "<a href=\"$cgi_url?action=edit_user_form&usr_id=$uID\">[ Edit ]</a></td></tr>\n";
    }
        
    $pg_body .= "</table>\n";
    
    $pg_body .= "</td></tr>\n";    
    $pg_body .= "</table>\n";
    $pg_body .= "</div></div></div>\n";
    $pg_body .= "</tr></td>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</form>\n";
    
    
    $env->{page_head}    = "User Administration";
    $env->{html_title}   = "$site_title - User Administration";
    $env->{page_body}    = $pg_body;
        
    $shared->page_gen( $env, $java_msg );
        
}

###########################################################                   
# Form to Add a new user
#----------------------------------------------------------
sub add_user_form {

    my $in_width;
    
    my $pg_body = "";
    
    $pg_body  = "<form method=post action=\"$cgi_url\">\n";
    $pg_body .= "<input type=hidden name=action value=add_user>\n";
    
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=614>\n";
    $pg_body .= "<tr bgcolor=$color_bg1><td>\n";
    
    $in_width = 610;
    
    $pg_body .= "<table border=0 cellpadding=3 cellspacing=0 width=$in_width>\n";
    $pg_body .= "<tr bgcolor=$color_bg2><td colspan=2 class=body>\n";
    $pg_body .= "Input the following values below to add a new user.  User ID must be a unique.</td></tr>\n";
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>User ID</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=uLogin size=10 class=form></td></tr>\n";
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>Type</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><select id=uType name=uType class=form onchange=JavaScript:showInpLnk()><option value=\"ADM\">Regular User</option><option value=\"LIG\">Translator</option><option value=\"CLN\">Client</option></select></td></tr>\n";
    
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>Name</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=uName size=30 class=form></td></tr>\n";
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>E-Mail</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=uEmail size=40 class=form></td></tr>\n";
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>Initial Password</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=uPassword size=10 class=form></td></tr>\n";
    
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>&nbsp;</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg2 class=form>\n";
    $pg_body .= "<input type=submit value=\"Add New User\" class=form>&nbsp;\n";
    $pg_body .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?action=user_maint'\" class=form></td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</form>\n";   
    
    $env->{page_head}    = "Add User";
    $env->{html_title}   = "$site_title - Add New User";
    $env->{page_body}    = $pg_body;
        
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Add the user to the database
#----------------------------------------------------------
sub add_user {

    my $usr_login = $shared->get_cgi( "uLogin" );
    my $usr_name  = $shared->get_cgi( "uName" );
    my $usr_email = $shared->get_cgi( "uEmail" );
    my $usr_pass  = $shared->get_cgi( "uPassword" );
    my $usr_calCh = $shared->get_cgi( "uCalChapter" );
    my $usr_type  = $shared->get_cgi( "uType" );
    
    my $chkSQL = "SELECT count(*) FROM user WHERE usr_login = '$usr_login'";
    if ( $shared->getSQLCount( $chkSQL ) ) {
        $java_msg = "User Login ID already exists.  Please select another.";
        &add_user_form;
    } else {
        my $insert = "INSERT INTO user ( usr_login, usr_name, usr_bad_login_cnt,\n"
                   . "usr_password, usr_reset_time, usr_email, usr_type )\n"
                   . "VALUES ( '$usr_login', '$usr_name', 0, '$usr_pass', 0, '$usr_email', '$usr_type' )";
        $shared->doSQLUpdate( $insert );
        my $uID    = $shared->getSQLLastInsertID;
        
        $shared->add_log( "USERADD", "Added User: $usr_login ($usr_name)", $usr_id );
        
        my $rmSQL = "SELECT syd_id FROM sys_module WHERE syd_required = 'Y'";
        my @rmRS  = $shared->getResultSet( $rmSQL );
        foreach my $rmRec ( @rmRS ) {
        	my $syd_id = $rmRec->{syd_id};
        	my $insSQL = "INSERT INTO user_module ( usm_usr_id, usm_syd_id )\n"
        	           . "VALUES ( $uID, $syd_id )";
        	$shared->doSQLUpdate( $insSQL );
        }
        &edit_user_form( $uID );
    }
}


###########################################################                   
# Form to Edit user
#----------------------------------------------------------
sub edit_user_form {

    my ( $uID ) = @_;
    
    if ( $uID eq "" ) {
        $uID = $shared->get_cgi( "usr_id" );
    }
    
    my $usr_rec   = $shared->get_rec( "user", "usr_id", $uID );

    my $usr_login = $usr_rec->{usr_login};
    my $usr_name  = $usr_rec->{usr_name};
    my $usr_email = $usr_rec->{usr_email};
    my $usr_b_cnt = $usr_rec->{usr_bad_login_cnt};
    my $usr_exp   = $usr_rec->{usr_reset_time};
    my $usr_lock  = $usr_rec->{usr_lock_flag};
    my $usr_actv  = $usr_rec->{usr_active};
    my $usr_type  = $usr_rec->{usr_type};
    my $lnkID     = $usr_rec->{usr_link_id};

    my $checked   = ( $usr_actv eq "Y" ) ? " checked" : "";
    
    my $in_width = 814;
    
    my $dspType  = "Regular";
    my $inpLink  = "";
    if ( $usr_type eq "LIG" ) {
        $dspType = "Translator";
        $inpLink = "Link to: <select name=usr_link_id class=form><option value=\"0\"></option>" . &getLIGSelect( $lnkID ) . "</select>";
    } elsif ( $usr_type eq "CLN" ) {
        $dspType = "Client";
        $inpLink = "Link to: <select name=usr_link_id class=form><option value=\"0\"></option>" . &getCLNSelect( $lnkID ) . "</select>";
    }
    
    my $pg_body = "";

    $pg_body .= "<table border=0 cellpadding=3 cellspacing=0 width=$in_width>\n";
    $pg_body .= "<tr bgcolor=$color_bg2><td valign=top align=left class=form colspan=2>\n";
    $pg_body .= "Update the values below.  To reset a user's password, type a new password in the\n";
    $pg_body .= "appropriate field below.  <b>Leave the password field blank to keep the existing one</b>.</td></tr>\n";
    $pg_body .= "<tr bgcolor=$color_bg2><td valign=top align=left width=600>\n";

    $pg_body .= "<form method=post action=\"$cgi_url\">\n";
    $pg_body .= "<input type=hidden name=action value=\"edit_user\">\n";
    $pg_body .= "<input type=hidden name=usr_id value=\"$uID\">\n";
    $pg_body .= "<input type=hidden name=usr_type value=\"$usr_type\">\n";
    
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=594>\n";
    $pg_body .= "<tr><td valign=top align=left class=label width=94>User ID</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form width=300><b>$usr_login</b>&nbsp;&nbsp;\n";
    $pg_body .= "<input type=checkbox name=usr_active value=\"Y\" class=form" . $checked . "> Active</td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>User Type</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><b>$dspType</b>&nbsp;&nbsp;\n";
    $pg_body .= $inpLink . "</td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>Name</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=usr_name size=30 value=\"$usr_name\" class=form></td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>E-Mail</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=usr_email size=40 value=\"$usr_email\" class=form></td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>Reset Password</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=usr_pass size=10 class=form></td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>Status</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form>\n";
    
    if ( $usr_lock eq "Y" ) {
        my $unlock_time = localtime( $usr_exp );
        $pg_body .= "<b>LOCKED</b><br>Until $unlock_time<br>";
        $pg_body .= "<a href=\"$cgi_url?action=unlock_user&usr_id=$uID\"><b>[ Unlock Now! ]</b></a>";
    } else {
        $pg_body .= "<b>NOT LOCKED</b> - $usr_b_cnt bad attempts";
    }
    
    $pg_body .= "</td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg2 class=form>\n";
    $pg_body .= "<input type=submit value=\"Update User\" class=form>&nbsp;\n";
    $pg_body .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?action=user_maint'\" class=form></td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</form>\n";    
    
    $pg_body .= "</td><td valign=top align=left width=214>\n";

    
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=214>\n";
    $pg_body .= "<tr><td valign=top align=left bgcolor=\"$color_head2\" class=colhead>Module Rights</td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left bgcolor=\"$color_bg1\" class=label>Assigned Modules:<br>\n";
    
    $pg_body .= "<form method=post action=\"$cgi_url\">\n";
    $pg_body .= "<input type=hidden name=action value=\"del_user_module\">\n";
    $pg_body .= "<input type=hidden name=usr_id value=\"$uID\">\n";
    $pg_body .= "<select name=usm_id size=6 class=form>\n";
    
    my $notIn = "";
    my $mSQL  = "SELECT usm_id, syd_id, syd_label, syd_order\n"
              . "FROM user_module\n"
              . "INNER JOIN sys_module ON usm_syd_id = syd_id\n"
              . "WHERE usm_usr_id = $uID\n"
              . "ORDER BY syd_order";
    my @mrs   = $shared->getResultSet( $mSQL );
    foreach my $mod_rec ( @mrs ) {
    	my $usm_id = $mod_rec->{usm_id};
        my $label  = $mod_rec->{syd_label};
        my $sydID  = $mod_rec->{syd_id};
        $pg_body .= "<option value=\"$usm_id\">$label</option>\n";
        if ( $notIn eq "" ) {
        	$notIn .= "$sydID";
        } else {
        	$notIn .= ", $sydID";
        }
    }
    
    $pg_body .= "</select><br>\n";
    $pg_body .= "<input type=submit value=\"Remove Module\" class=form>\n";
    $pg_body .= "</form>\n";
    $pg_body .= "</td></tr>\n";

    $pg_body .= "<tr><td valign=top align=left bgcolor=\"$color_bg1\" class=label>Add Module:<br>\n";
    
    $pg_body .= "<form method=post action=\"$cgi_url\">\n";
    $pg_body .= "<input type=hidden name=action value=\"add_user_module\">\n";
    $pg_body .= "<input type=hidden name=usr_id value=\"$uID\">\n";
    $pg_body .= "<select name=usr_syd_id class=form>\n";
    
    my $aSQL  = "SELECT syd_id, syd_label, syd_order\n"
              . "FROM sys_module\n"
              . "WHERE syd_sepline = 'N'\n";
    if ( ( $usr_id >= 10 ) && ( $app_mode eq "PROD" ) ) {
        $aSQL .= "AND syd_active = 'Y'\n";
    }
    if ( $notIn gt "" ) {
        $aSQL .= "AND syd_id NOT IN ( $notIn )\n";
    }
    $aSQL    .= "ORDER BY syd_order";
    my @ars   = $shared->getResultSet( $aSQL );
    foreach my $syd_rec ( @ars ) {
        my $id   = $syd_rec->{syd_id};
        my $desc = $syd_rec->{syd_label};
        $pg_body .= "<option value=\"$id\">$desc</option>\n";
    }
    
    $pg_body .= "</select><br>\n";
    $pg_body .= "<input type=submit value=\"Add Module\" class=form>\n";
    $pg_body .= "</form>\n";
    $pg_body .= "</td></tr>\n";
    
    $pg_body .= "</table>\n";
    $pg_body .= "</td></tr>\n";
    $pg_body .= "</table>\n";
    
    
    $env->{page_head}    = "User Properties";
    $env->{html_title}   = "$site_title - User Properties";
    $env->{page_body}    = $pg_body;
        
    $shared->page_gen( $env, $java_msg );

}

###########################################################                   
# Edit a user in the database.
#----------------------------------------------------------
sub edit_user {

    my $uID       = $shared->get_cgi( "usr_id" );
    my $usr_name  = $shared->get_cgi( "usr_name" );
    my $usr_email = $shared->get_cgi( "usr_email" );
    my $usr_pass  = $shared->get_cgi( "usr_pass" );
    my $usr_actv  = $shared->get_cgi( "usr_active" );
    my $usr_type  = $shared->get_cgi( "usr_type" );
    my $lnkID     = $shared->get_cgi( "usr_link_id" );
    
    if ( $usr_actv eq "" ) {
    	$usr_actv = "N";
    }
    
    if ( $lnkID eq "" ) {
        $lnkID = 0;
    }
    
    if ( ( ( $usr_type eq "CLN" ) || ( $usr_type eq "LIG" ) ) && ( $lnkID == 0 ) ) {
        $java_msg = "WARNING: Translators and Clients require a link to their specific profile.";
    }
    
    my $update    = "";
    if ( $usr_pass eq "" ) {
    	$update   = "UPDATE user SET usr_name = '$usr_name',\n"
    	          . "usr_email = '$usr_email', usr_active = '$usr_actv',\n"
    	          . "usr_link_id = $lnkID\n"
    	          . "WHERE usr_id = $uID";
    } else {
    	$update   = "UPDATE user SET usr_name = '$usr_name',\n"
    	          . "usr_email = '$usr_email', usr_password = '$usr_pass',\n"
    	          . "usr_active = '$usr_actv', usr_link_id = $lnkID\n"
    	          . "WHERE usr_id = $uID";
    }
    
    $shared->doSQLUpdate( $update );
    
    &user_maint;
    
}

###########################################################                   
# Unlock user's account
#----------------------------------------------------------
sub unlock_user {

    my $uID    = $shared->get_cgi( "usr_id" );
    my $update = "UPDATE user SET usr_lock_flag = 'N', usr_bad_login_cnt = 0, usr_reset_time = 0\n"
               . "WHERE usr_id = $uID";
    $shared->doSQLUpdate( $update );
    &edit_user_form( $uID );

}

###########################################################                   
# Add Module rights to User
#----------------------------------------------------------
sub add_user_module {

    my $uID        = $shared->get_cgi( "usr_id" );
    my $syd_id     = $shared->get_cgi( "usr_syd_id" );
    if ( $syd_id gt "" ) {
        my $update = "INSERT INTO user_module ( usm_usr_id, usm_syd_id )\n"
                   . "VALUES ( $uID, $syd_id )";
        $shared->doSQLUpdate( $update );
        
        my $uRec = $shared->get_rec( "user", "usr_id", $uID );
        my $mRec = $shared->get_rec( "sys_module", "syd_id", $syd_id );
        my $uLO  = $uRec->{usr_login};
        my $uNm  = $uRec->{usr_name};
        my $mNm  = $mRec->{syd_label};
        
        $shared->add_log( "MODADD", "Added \"$mNm\" to user: $uLO ($uNm)", $usr_id );
        
    }
    &edit_user_form( $uID );
    
}

###########################################################                   
# Delete Branch from User
#----------------------------------------------------------
sub del_user_module {

    my $uID        = $shared->get_cgi( "usr_id" );
    my $usm_id     = $shared->get_cgi( "usm_id" );
    if ( $usm_id gt "" ) {
    	
    	my $SQL = "SELECT syd_required, syd_label FROM user_module\n"
    	        . "INNER JOIN sys_module ON usm_syd_id = syd_id\n"
    	        . "WHERE usm_id = $usm_id";
    	my $rec = $shared->getResultRec( $SQL );
    	my $rqd = $rec->{syd_required};
    	my $mNm = $rec->{syd_label};
    	if ( $rqd eq "N" ) {
            my $update = "DELETE FROM user_module WHERE usm_id = $usm_id";
            $shared->doSQLUpdate( $update );
            
            my $uRec = $shared->get_rec( "user", "usr_id", $uID );
            my $uLO  = $uRec->{usr_login};
            my $uNm  = $uRec->{usr_name};
        
            $shared->add_log( "MODDEL", "Removed \"$mNm\" from user: $uLO ($uNm)", $usr_id );
            
        } else {
        	$java_msg = "Unable to remove required module.";
        }
    }
    &edit_user_form( $uID );
    
}

###########################################################
# Allow administrator to view and maintain log file.
#----------------------------------------------------------
sub log_maint {

    my $l_fld  = $shared->get_cgi( "l_fld" );
    my $l_val  = $shared->get_cgi( "l_val" );
    my $l_head;
    my $where;
    
    if ( $l_fld eq "log_datetime" ) {
    	$l_head = "Filtered on Log Date";
    	$where  = "where date_format( log_time, '%Y%m%d' ) = '$l_val'";
    		
    } elsif ( $l_fld eq "log_type" ) {
    	$l_head = "Filtered on Log Type";
    	$where  = "where $l_fld = '$l_val'";
    	
    } elsif ( $l_fld eq "log_ip_add" ) {
    	$l_head = "Filtered on IP Address";
    	$where  = "where $l_fld = '$l_val'";
    	
    } elsif ( $l_fld eq "log_usr_id" ) {
    	$l_head = "Filtered on User";
    	$where  = "where $l_fld = $l_val";
    	
    } else {
    	$l_head = "Last Week's Activity - All Records";
    	$where  = "where log_time > date_add( now(), interval -7 day )";
    	
    }

    my $query = "SELECT log_id, log_time, date_format( log_time, '%c/%e/%Y %H:%i' ) as log_date_fmt,\n" .
                "log_type, log_ip_add, log_text, log_usr_id, usr_name\n" .
                "FROM audit_log\n" .
                "LEFT JOIN user ON log_usr_id = usr_id\n" .
                "$where\n" .
                "ORDER BY log_time DESC";
    
    my @rsLog   = $shared->getResultSet( $query );
    my $log_cnt = @rsLog;
    
    my $pg_body;

    $pg_body .= "<table border=0 cellpadding=0 cellspacing=0 width=840>\n";
    $pg_body .= "<tr><td>\n";
    $pg_body .= "<div class=blur><div class=shadow><div class=framework>\n";
    
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=840>\n";
    $pg_body .= "<tr bgcolor=\"$color_bg1\"><td valign=top align=left class=bullet><b>$l_head</b></td>\n";
    $pg_body .= "<td valign=top align=right class=bullet>Record Count: $log_cnt</td></tr>\n";
    $pg_body .= "<tr bgcolor=\"$color_bg1\"><td valign=top align=left colspan=2>\n";
    
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=836>\n";
    $pg_body .= "<tr><td valign=top align=left class=colhead width=120>Log Date</td>\n";
    $pg_body .= "<td valign=top align=left class=colhead width=100>IP Address</td>\n";
    $pg_body .= "<td valign=top align=left class=colhead width=516>Message</td>\n";
    $pg_body .= "<td valign=top align=left class=colhead width=100>User</td></tr>\n";
    
    if ( $log_cnt == 0 ) {
        $pg_body .= "<tr><td colspan=4 class=mbody>No log records found.</td></tr>\n";
    } else {

        my $row = 0;
            
        foreach my $log_rec ( @rsLog ) {
            my $log_id       = $log_rec->{log_id};
            my $log_date_val = substr( $log_rec->{log_time}, 0, 4 )
                             . substr( $log_rec->{log_time}, 5, 2 )
                             . substr( $log_rec->{log_time}, 8, 2 );
            my $log_date_fmt = $log_rec->{log_date_fmt};
            my $log_type     = $log_rec->{log_type};
            my $log_ip       = $log_rec->{log_ip_add};
            my $log_msg      = $log_rec->{log_text};
            my $log_usr_id   = $log_rec->{log_usr_id};
            my $log_name     = $log_rec->{usr_name};

            my $row_color;
            if ( $row == 0 ) {
            	$row_color = $color_bg2;
            	$row = 1;
            } else {
            	$row_color = $color_bg1;
            	$row = 0;
            }
            
            $pg_body .= "<tr bgcolor=$row_color><td valign=top align=left class=form><a href=\"$cgi_url?action=log_maint&l_fld=log_time&l_val=$log_date_val\">$log_date_fmt</a></td>\n";
            $pg_body .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=log_maint&l_fld=log_ip_add&l_val=$log_ip\">$log_ip</a></td>\n";
            $pg_body .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=log_maint&l_fld=log_type&l_val=$log_type\">$log_msg</a></td>\n";
            if ( $log_name ne "" ) {
                $pg_body .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=log_maint&l_fld=log_usr_id&l_val=$log_usr_id\">$log_name</a></td>\n";
            } else {
            	$pg_body .= "<td class=form>&nbsp;</td>\n";
            }
            $pg_body .= "</tr>\n";
        
        }
    
    }
    
    $pg_body .= "</table>\n";
    $pg_body .= "</td></tr>\n";
    $pg_body .= "</table>\n";
    
    $pg_body .= "</div></div></div>\n";
    $pg_body .= "</td></tr>\n";
    $pg_body .= "</table>\n";
    
    $env->{html_title}     = "Log Maintenance";
    $env->{page_head}      = "View Audit Log";
    $env->{page_body}      = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Admin Configuration Form
#----------------------------------------------------------
sub admin_config {

    my $pg_body  = "";
    my $date_cnt = 0;
    
    my $SQL1  = "SELECT lku_sort, lku_code, lku_description FROM lookup WHERE lku_category = 'CFG_TAB' ORDER BY lku_sort";
    my @rs1   = $shared->getResultSet( $SQL1 );
    
    $pg_body  = "<form method=post action=\"$cgi_url\">\n";
    $pg_body .= "<input type=hidden name=action value=admin_config_update>\n";

    $pg_body .= "<table border=0 cellpadding=1 cellspacing=0 width=\"714\">\n";
    $pg_body .= "<tr><td valign=top align=left>\n";
    $pg_body .= "<div class=\"tabber\">\n";

    my $tCnt  = 0;
    
    foreach my $hash ( @rs1 ) {
        
        my $tab = $hash->{lku_code};
        my $tLbl = $hash->{lku_description};
        
        if ( $tCnt == 0 ) {
            $pg_body .= "<div class=\"tabbertab tabbertabdefault\">\n";
        } else {
            $pg_body .= "<div class=\"tabbertab\">\n";
        }
	    $pg_body .= "<h2>$tLbl</h2>\n";
	    
	    $pg_body .= &getInpTable( $tab );
	    
	    $pg_body .= "</div>\n";
	    
	    $tCnt++;
	}

    $pg_body .= "</div>\n";
    $pg_body .= "</td></tr>\n";

    $pg_body .= "<td valign=top align=left class=form>\n";
    $pg_body .= "<input type=submit value=\"Update\" class=form>&nbsp;\n";
    $pg_body .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url'\" class=form></td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</form>\n";   

    my $js    = "";
    if ( $shared->getSQLCount( "SELECT COUNT(*) FROM sys_config WHERE cfg_code LIKE 'ADM_%' AND cfg_type = 'D'" ) > 0 ) {    
        $js    = "<link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"$css_path/calendar.css\"/>\n";
        $js   .= "<script type=\"text/javascript\" src=\"$js_path/calendar.js\"></script>\n";
        $js   .= "<script type=\"text/javascript\" src=\"$js_path/calendar-language.js\"></script>\n";
        $js   .= "<script type=\"text/javascript\" src=\"$js_path/calendar-setup.js\"></script>\n";
    }
    $js       .= $shared->getTabberHead;
    if ( $page_date eq "Y" ) {
        $js   .= $shared->getJSCalHead;
    }
    
    my $el = "";
    if ( $tCnt > 0 ) {
        $el  = "<script type=\"text/javascript\">\n";
        $el .= "tabberAutomatic(tabberOptions);\n";
        $el .= "</script>\n";
    }
    
    
    $env->{javascript}   = $js;
    $env->{page_head}    = "System Parameters";
    $env->{html_title}   = "$site_title - System Parameters";
    $env->{page_body}    = $pg_body;
    $env->{end_load}     = $el;
        
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Admin Configuration Update
#----------------------------------------------------------
sub admin_config_update {

    my $pg_body  = "";
    
    my $SQL   = "SELECT * FROM sys_config WHERE cfg_code LIKE 'ADM_%' ORDER BY cfg_code";
    my @rs    = $shared->getResultSet( $SQL );

    foreach my $rec ( @rs ) {
        
        my $code = $rec->{cfg_code};
        my $type = $rec->{cfg_type};
        my $tmp  = $shared->get_cgi( $code );
        my $val;
        my $fld;
        if ( $type eq "I" ) {
            $tmp =~ s/[\.\,\$]//g;
            $val = $tmp + 0;
            $fld = "cfg_int_value";
        } elsif ( $type eq "S" ) {
            $tmp =~ s/\'/\\'/g;
            $val = $tmp;
            $fld = "cfg_string_value";
        } elsif ( $type eq "D" ) {
            $val = $tmp;
            $fld = "cfg_date_value";
        } elsif ( $type eq "T" ) {
            $tmp =~ s/\'/\\'/g;
            $val = $tmp;
            $fld = "cfg_text_value";
        }
        
        my $update = "UPDATE sys_config SET $fld = ";
        if ( $type eq "I" ) {
            $update .= "$val ";
        } elsif ( $type eq "D" ) {
            $update .= "'" . $shared->getSQLDate( $val ) . "' ";
        } else {
            $update .= "'$val' ";
        }
        $update .= "WHERE cfg_code = '$code'";
        $shared->doSQLUpdate( $update );
    
    }    

    $env->{doFade} = "Y";
    $java_msg      = "System Parameters Updated.";
    &admin_config;
    
}

###########################################################
# Return a select list of linguists.
#----------------------------------------------------------
sub getLIGSelect {
    
    my ( $cur_val ) = @_;
    my $retval;
    
    my @rs         = $shared->getResultSet( "SELECT lig_id, lig_last_name, lig_first_name FROM linguist ORDER BY lig_last_name, lig_first_name" );
    
    foreach my $rec ( @rs ) {
        my $code = $rec->{lig_id};
        my $name = $shared->trim( $rec->{lig_last_name} ) . ", " . $shared->trim( $rec->{lig_first_name} );
        if ( $rec->{lig_last_name} eq "" ) {
            $name    = $rec->{lig_first_name};
        }
        if ( $rec->{lig_first_name} eq "" ) {
            $name    = $rec->{lig_last_name};
        }
        if ( $cur_val == $code ) {
            $retval .= "<option selected value=\"$code\">$name</option>";
        } else {
            $retval .= "<option value=\"$code\">$name</option>";
        }
    }
    
    return $retval;
}

###########################################################
# Return a select list of linguists.
#----------------------------------------------------------
sub getCLNSelect {
    
    my ( $cur_val ) = @_;
    my $retval;
    
    my @rs         = $shared->getResultSet( "SELECT cln_id, cln_name FROM client ORDER BY cln_name" );
    
    foreach my $rec ( @rs ) {
        my $code = $rec->{cln_id};
        my $name = $rec->{cln_name};
        $name   =~ s/'/\\'/g;
        if ( $cur_val == $code ) {
            $retval .= "<option selected value=\"$code\">$name</option>";
        } else {
            $retval .= "<option value=\"$code\">$name</option>";
        }
    }
    
    return $retval;
}


###########################################################
# Get input table for tabbed parameters.
#----------------------------------------------------------
sub getInpTable {
    
    my ( $tab ) = @_;
    
    my $dCnt  = 0;
    
    my $SQL   = "SELECT * FROM sys_config WHERE cfg_code LIKE 'ADM_%' AND cfg_tab = '$tab' ORDER BY cfg_order";
    my @rs    = $shared->getResultSet( $SQL );
    
    my $pg_body = "";
    $pg_body .= "<table border=0 cellpadding=3 cellspacing=0 width=702>\n";
    $pg_body .= "<tr bgcolor=$color_bg2><td colspan=2 class=form>\n";
    $pg_body .= "The following parameters may be updated.</td></tr>\n";
    
    foreach my $rec ( @rs ) {
        
        my $code = $rec->{cfg_code};
        my $type = $rec->{cfg_type};
        my $val;
        if ( $type eq "I" ) {
            $val = $rec->{cfg_int_value};
        } elsif ( $type eq "S" ) {
            $val = $rec->{cfg_string_value};
        } elsif ( $type eq "D" ) {
            $val = $shared->getDateFmt( $rec->{cfg_date_value} );
        } elsif ( $type eq "T" ) {
            $val = $rec->{cfg_text_value};
        }
        
        my ( undef, $tmp ) = split( "_", $code );
        my $label = $shared->getLookupDesc( "ADM_CFG", $tmp );
        my $fld   = "ADM_" . $tmp;
        
        $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>$label</td>\n";
        $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form>\n";
        
        if ( $type eq "I" ) {
            $pg_body .= "<input name=\"$fld\" value=\"$val\" size=13 maxlength=11 class=form>\n";
        } elsif ( $type eq "S" ) {
            $pg_body .= "<input name=\"$fld\" value=\"$val\" size=60 maxlength=100 class=form>\n";
        } elsif ( $type eq "D" ) {
   	        $dCnt++;   	        
   	        my $dcN = "jscal_trigger_" . lc( $tab ) . "_" . $dCnt;
   	        
   	        $pg_body .= $shared->getJSCalInput( $fld, $val, $dcN );
            $page_date = "Y";
            
        } elsif ( $type eq "T" ) {
            $pg_body .= "<textarea name=\"$fld\" cols=60 rows=5 wrap class=form>$val</textarea>\n";
        }
        $pg_body .= "</td></tr>\n";
    
    }
    
    $pg_body .= "</table>\n";
    
    return $pg_body;    
    
}
