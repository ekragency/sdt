#!/usr/bin/perl
###########################################################################
# LP.CGI                    
# Linguist Portal Script - for Same Day Translations
# Created 04/14/2017
###########################################################################
# Copyright 2017, P. Eric Huber
###########################################################################
use strict;
use Net::SMTP;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use ligauth;

my $config = new config;
my $shared = new shared;
my $auth   = ligauth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/lp.cgi";
my ($img_path)    = $config->get_img_path;
my ($base_url)    = $config->get_base_url;
my ($site_title)  = $config->get_lp_name;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;
my ($color_bg1)   = "#EEE5DE";
my ($color_bg2)   = "#FFFFFF";
my ($out_path)    = $config->get_out_path;
my ($app_mode)    = $config->get_app_mode;

my ($conf);
$conf->{cgi_url}  = $cgi_url;
$conf->{img_path} = $img_path;

my $java_msg      = "";

$conf->{cgi_url}     = $cgi_url;
$conf->{admin_email} = $config->get_admin_email;


# Template Variables
my ($env);
$env->{cgi_url}     = $cgi_url;
$env->{img_path}    = $img_path;
$env->{home_url}    = $base_url;
$env->{html_title}  = $site_title;
$env->{template}    = "lp.html";
$env->{css}         = "lp-style.css";
$env->{nav_buttons} = "<a href=\"$cgi_url?action=lp_inv_add_form\" class=green-button>New Invoice</a>\n"
                    . "<a href=\"$cgi_url?action=chg_pass_form\" class=grey-button>Password</a>\n"
                    . "<a href=\"$cgi_url?action=logout\" class=grey-button>Logout</a>";

if ( $app_mode eq "TEST" ) {
    $env->{template} = "lp-test.html";
}

# Main program
my $hash_vars;
$hash_vars->{srch_invno}   = $shared->get_cgi( "srch_invno" );


my $usr_id = $auth->getCurrentUser;
my $action = $shared->get_cgi( "action" );
my $uLogin = $auth->getCurrentUserLogin;
my $uCurr  = $auth->getCurrentUserCurrency;

if ( $usr_id > 0 ) {
	
	$env->{login_stat}      = $auth->getLoginStatus;
	$env->{usr_id}          = $usr_id;
	$conf->{usr_login}      = $auth->getCurrentUserLogin;
	$conf->{usr_pass}       = $auth->getCurrentUserPass;
	
    if ( $action eq "chg_pass_form" ) {
    	$auth->chg_pass_form( $cgi_url );
    } elsif ( $action eq "chg_pass" ) {
    	$java_msg = $auth->chg_pass( $cgi_url );
    	&search_any;
    } elsif ( $action eq "logout" ) {
	    $auth->logout( $cgi_url );
	} elsif ( $action eq "search_any" ) {
	    &search_any;
	} elsif ( $action eq "lp_inv_add_form" ) {
	    &lp_inv_add_form;
	} elsif ( $action eq "lp_inv_insert" ) {
	    &lp_inv_insert;
    } elsif ( $action eq "view_invoice" ) {
        my $invID = $shared->get_cgi( "inv_id" );
        &view_invoice( $invID );
    } else {
    	&search_any;
    }
} else {
	if ( $action eq "" ) {
		$auth->login( $cgi_url, "Y" );
    } elsif ( $action eq "reset_pass_form" ) {
        $auth->reset_pass_form( $cgi_url );
    } elsif ( $action eq "reset_pass" ) {
        $auth->setJavaMsg( $auth->reset_pass( $cgi_url ) );
        $auth->login( $cgi_url, "N" );
	} else {
	    $auth->login( $cgi_url, "N" );
	}
}

exit;


###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my $content = "";

    $content .= "<form method=post action=\"$cgi_url\">\n";
    $content .= "<input type=hidden name=action value=\"search_any\">\n";
    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=786>\n";
    $content .= "<tr><td valign=top align=left class=form>\n";
    if ( $conf->{search_results} eq "" ) {
        $content .= "<b>No Invoices Found.</b> Click <a href=\"$cgi_url\"><b>Here</b></a> to reset search.";
    } else {
        $content .= "&nbsp;";
    }
    $content .= "</td>\n";
    $content .= "<td valign=top align=right class=form><input name=srch_invno value=\"\" size=20 class=form placeholder=\"Invoice No\">\n";
    $content .= "<input type=submit value=\"Search\" class=form></td></tr>\n";
    
    if ( $conf->{search_results} gt "" ) {
        
        $content .= "<tr><td valign=top align=left colspan=2>\n";
        $content .= $conf->{search_results};
        $content .= "</td></tr>\n";
    
    }
    
    $content .= "</table>\n";
    
    $conf->{pg_content} = $content; 	
    
    my $pg_body         = &disp_form( "lp_main" );

    $env->{page_head}   = "My Invoices";
    $env->{page_body}   = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my $content;
	
	my $srch_invno   = $hash_vars->{srch_invno};
	my $srch_head    = "";
	my $sp_vars      = "";
	my $srch_par     = 0;

    my $SQL;
    
	if ( $srch_invno gt "" ) {
	
  	    $SQL = "SELECT * FROM invoice WHERE inv_ling_id = $usr_id\n";

	    if ( $srch_invno gt "" ) {
		    $SQL .= "AND inv_no LIKE '$srch_invno%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Invoice No starts with <b>$srch_invno</b>";
		    } else {
		        $srch_head .= "Invoice No starts with <b>$srch_invno</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_invno=$srch_invno";
	    }
	} else {
   	    $SQL = "SELECT * FROM invoice WHERE inv_ling_id = $usr_id\n"
   	         . "AND inv_date > DATE_ADD( now(), INTERVAL -90 DAY )\n";
        $srch_head .= "All Invoices Uploaded (Last 90 days)";
    }


    $SQL .= "ORDER BY inv_date DESC";
        	    
	my @rs   = $shared->getResultSet( $SQL );
	my $row  = 0;
	my $row_color;
	my $err_color = "#FFCCCC";
	my $fndCnt = 0;
	    
	$content .= "<span class=overview>$srch_head</span><br><br>\n";
	$content .= "<table border=0 cellpadding=2 cellspacing=0 width=782>\n";
	$content .= "<tr>\n";
	$content .= "<td valign=top align=left class=colhead2>Invoice No</td>\n";
	$content .= "<td valign=top align=left class=colhead2>Invoice Date</td>\n";
	$content .= "<td valign=top align=left class=colhead2>Status</td>\n";
	$content .= "<td valign=top align=right class=colhead2>Amount</td></tr>\n";
	foreach my $rec ( @rs ) {
	    my $id      = $rec->{inv_id};
	    my $invno   = $rec->{inv_no};
	    my $invdt   = $shared->getDateFmt( $rec->{inv_date} );
	    my $amt     = $shared->fmtCurrency( $rec->{inv_amount}, $uCurr );
	    my $iStat   = $shared->getLookupDesc( "INV_STAT", $rec->{inv_status} );
	    	
	    if ( $row == 0 ) {
	    	$row_color = $color_bg2;
	    	$row = 1;
	    } else {
	    	$row_color = $color_bg1;
	    	$row = 0;
	    }
	    $content .= "<tr bgcolor=\"$row_color\">\n";
        $content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=view_invoice&inv_id=$id\" target=_blank><b>$invno</b></a></td>\n";
	    $content .= "<td valign=top align=left class=form>$invdt</td>\n";
	    $content .= "<td valign=top align=left class=form>$iStat</td>\n";
	    $content .= "<td valign=top align=right class=form>$amt</td></tr>\n";
	    	
	    $fndCnt++;
	}
	$content .= "</table>\n";
	    		
	if ( $fndCnt == 0 ) {
	   	$content  = "";
	}
	
    $conf->{search_results} = $content;

	&print_main;
	
}


###########################################################
# Form to add new term record.
#----------------------------------------------------------
sub lp_inv_add_form {

    my $invDays = $shared->getConfigInt( "ADM_POINVDAYS" );
    
    my $poCnt   = 0;
    my $po_list = "<b>The following POs are available for invoicing...</b><br><table border=0 cellpadding=2 cellspacing=0>\n";
    my $SQL     = "SELECT pol_id, pol_pono, pol_add_date, pol_total_amt\n"
                . "FROM po_log\n"
                . "WHERE pol_ling_id = $usr_id\n"
                . "AND pol_invoice_recd <> 'Y'\n"
                . "AND DATE_ADD( pol_add_date, INTERVAL $invDays DAY ) > CURDATE()\n"
                . "ORDER BY pol_pono";
    my @rs      = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $pid = $rec->{pol_id};
        my $pno = $rec->{pol_pono};
        my $pdt = $shared->getDateFmt( $rec->{pol_add_date} );
        my $amt = $shared->fmtCurrency( $rec->{pol_total_amt}, $uCurr );
        my $fld = "po_" . $pid;
        $po_list .= "<tr><td valign=top align=left class=form><input type=checkbox name=$fld value=\"Y\" class=form>&nbsp;</td>\n"
                  . "<td valign=top align=left class=form>$pno</td>\n"
                  . "<td valign=top align=left class=form>&nbsp;" . $pdt . "&nbsp;</td>\n"
                  . "<td valign=top align=right class=form>$amt</td></tr>\n";
        $poCnt++;
    }
    $po_list .= "</table>\n";
    
    if ( $poCnt == 0 ) {
        $po_list = "<b>There are currently no POs available for invoicing</b>";
        $conf->{f_submit_disable} = " disabled";
    } else {
        $conf->{f_submit_disable} = "";
    }     
    
    $conf->{f_po_list} = $po_list;
    
    my $pg_body = &disp_form( "lp_inv_add_form" );

    $env->{page_head}   = "Add Invoice";
    $env->{page_body}   = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

###########################################################
# Add new invoice record.
#----------------------------------------------------------
sub lp_inv_insert {

    my $invDays = $shared->getConfigInt( "ADM_POINVDAYS" );

    my $poTot   = 0;
    my $invNo   = $shared->get_cgi( "inv_no" );
    my $invAmt  = ( $shared->get_cgi( "inv_amount" ) ) + 0;
    my @poID;
    my $fileNm  = "";
    my $SQL     = "SELECT pol_id, pol_pono, pol_add_date, pol_total_amt\n"
                . "FROM po_log\n"
                . "WHERE pol_ling_id = $usr_id\n"
                . "AND pol_invoice_recd <> 'Y'\n"
                . "AND DATE_ADD( pol_add_date, INTERVAL $invDays DAY ) > CURDATE()\n"                
                . "ORDER BY pol_pono";
    my @rs      = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $pID = $rec->{pol_id};
        my $fld = "po_" . $pID;
        if ( $shared->get_cgi( $fld ) eq "Y" ) {
            my $amt = ( $rec->{pol_total_amt} ) + 0;
            $poTot += $amt;
            push( @poID, $pID );
        }
    }
    
    my $cntPrevInv = $shared->getSQLCount( "SELECT COUNT(*) FROM invoice WHERE inv_ling_id = $usr_id AND inv_no = '$invNo'" );
    
    my $errStat = 0;
    if ( $invNo eq "" ) {
        $java_msg = "Invoice Number is required.";
        $errStat  = 1;
    } elsif ( $cntPrevInv > 0 ) {
        $java_msg = "Invoice Number already exists - Invoice Number must be unique.";
        $errStat  = 1;
    } elsif ( $poTot == 0 ) {
        $java_msg = "You must check at least one PO to be linked to invoice.";
        $errStat  = 1;
    } elsif ( $shared->round( $invAmt, 2 ) != $shared->round( $poTot, 2 ) ) {
        $java_msg = "Invoice Amount must equal the sum of selected POs.";
        $errStat  = 1;
    } elsif ( $invAmt == 0 ) {
        $java_msg = "Invoice Amount cannot be 0.";
        $errStat  = 1;
    }

    if ( $errStat == 0 ) {
        
        my $tIN     = $invNo;
        $tIN       =~ s/[\W\s]+//g;
        $fileNm    = $usr_id . "-" . $tIN . ".pdf";
        
        my $subD    = substr( $usr_id, 0, 1 );
        my $uPath   = "$out_path/invoice/$subD";
        my $fName   = $uPath . "/" . $fileNm;
        my $up_err  = $shared->uploadFile( "inv_upload", $fName, "PDF" );
        if ( $up_err == 2 ) {
            $java_msg = "Invoice file must be PDF format.";
            $errStat  = 1;
        } elsif ( $up_err == 1 ) {
            $java_msg = "Invoice file (PDF) is required to be uploaded.";
            $errStat  = 1;
        }
    }
    
    if ( $errStat == 0 ) {
        my $logMsg = "Linguist Portal User $uLogin uploaded invoice ($invNo) linked to following POs:<br>";
        
        my $insert = "INSERT INTO invoice ( inv_ling_id, inv_no, inv_date, inv_amount, inv_filename )\n"
                   . "VALUES ( $usr_id, '$invNo', now(), $invAmt, '$fileNm' )";
        $shared->doSQLUpdate( $insert );
        my $invID  = $shared->getSQLLastInsertID;
        
        my $ins2   = "INSERT INTO invoice_audit ( ina_inv_id, ina_usr_id, ina_date, ina_description )\n"
                   . "VALUES ( $invID, 0, now(), 'Invoice submitted by Linguist' )";
        $shared->doSQLUpdate( $ins2 ); 
        
        my $pCnt = 0;
        foreach my $pID ( @poID ) {
            my $pRec   = $shared->get_rec( "po_log", "pol_id", $pID );
            my $poNo   = $pRec->{pol_pono};
            if ( $pCnt > 0 ) {
                $logMsg .= ", $pID ($poNo)";
            } else {
                $logMsg .= "$pID ($poNo)";
            }
            my $update = "UPDATE po_log SET pol_inv_id = $invID, pol_invoice_recd = 'Y'\n"
                       . "WHERE pol_id = $pID";
            $shared->doSQLUpdate( $update );
            $pCnt++;
        }
        
        &notify_inv_submit( $invNo, $invAmt );
        
        $shared->add_log( "LPUPINV", $logMsg, 0 );
        
        &search_any;
    } else {
        &lp_inv_add_form;
    }
}

###########################################################                   
# Stream Invoice PDF to browser
#----------------------------------------------------------
sub view_invoice {
    
    my ( $inv_id )  = @_;

    my $rec         = $shared->get_rec( "invoice", "inv_id", $inv_id );
    my $fileNm      = $rec->{inv_filename};
    my $ling_id     = $rec->{inv_ling_id};
    if ( $ling_id == $usr_id ) {
        my $subD        = substr( $ling_id, 0, 1 );
        my $uPath       = $out_path . "/invoice/" . $subD;
        my $fName       = "$uPath/$fileNm";
    
        $shared->streamPDF( $fName );
    } 
        
}

###########################################################                   
# Send Invoice Submission Notification
#----------------------------------------------------------
sub notify_inv_submit {
	
	my ( $invNo, $invAmt ) = @_;

    my $config        = new config;
	
	my $mailhost      = $config->get_mailhost;
	my $mailadmin     = $config->get_help_email;
	my $mailfrom      = "SDT Admin";

    my $lig_rec    = $shared->get_rec( "linguist", "lig_id", $usr_id );
    my $name       = $lig_rec->{lig_first_name} . " " . $lig_rec->{lig_last_name};
    my $mailto     = $lig_rec->{lig_email};
    
	my $email_body = "";
	$email_body   .= "Hello $name\n\n";
	$email_body   .= "You have successfully submitted a new invoice through the Linguist Portal at SameDay\n";
	$email_body   .= "Translations.  If you have any questions regarding your submission please contact us\n";
	$email_body   .= "by replying to this message.\n\n";
	$email_body   .= "Invoice Summary\n";
	$email_body   .= "Invoice Number: $invNo\n";
	$email_body   .= "Invoice Amount: $invAmt\n\n";

    my $smtp = Net::SMTP->new( $mailhost );
    $smtp->mail( $mailadmin );
    $smtp->to( $mailto );
    $smtp->data();
    $smtp->datasend( "To: $mailto\n" );
    $smtp->datasend( "Reply-To: $mailadmin\n" );
    $smtp->datasend( "From: $mailfrom <$mailadmin>\n" );
    $smtp->datasend( "Subject: SDT Linguist Portal - Invoice $invNo Submitted\n" );
    $smtp->datasend( "\n" );
    $smtp->datasend( $email_body );
    $smtp->dataend();
    $smtp->quit;
        
} 
