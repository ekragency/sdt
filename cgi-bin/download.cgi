#!/usr/bin/perl
###########################################################################
# download.cgi                Prompt user to download file.
# Created 08/23/2013
###########################################################################
# Copyright 2013, P. Eric Huber
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;

my $config = new config;
my $shared = new shared;

$| = 1;

my $out_path  = $config->get_out_path;
my $file      = $shared->get_cgi( "file" );

print "Content-type: application/octet-stream; name=\"$file\"\n";
print "Content-Disposition: attachment; filename=\"$file\"\n\n";

open( INPUT, "$out_path/$file" );
while( <INPUT> ) {
    print $_;
}
close( INPUT );

exit;
