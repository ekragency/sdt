#!/usr/bin/perl
###########################################################################
# lkuadm.cgi             Lookups - Administration script
###########################################################################
# Copyright 2019, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/lkuadm.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_head2   = $colors{ "HEAD2" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $exceptList    = "";
my $dupList       = "";
my $debug         = $config->get_debug;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;


# Main program
my $usr_id = 0;
my $action = $shared->get_cgi( "action" );
my $cat    = $shared->get_cgi( "cat" );
my $hash_vars;

$hash_vars->{srch_desc}    = $shared->get_cgi( "srch_desc" );

my $srch_desc    = $hash_vars->{srch_desc};
	
my $inp_srch     = "";
my $sp_vars      = "";
if ( $srch_desc gt "" ) {
	$inp_srch .= "<input type=hidden name=srch_desc value=\"$srch_desc\">\n";
   	$sp_vars  .= "&srch_desc=$srch_desc";
}


#
# Main Program
#
 
if ( $auth->validate_access( "lkuadm" ) ) {
	
	$usr_id    = $auth->getCurrentUser;

    if ( $action eq "search_any" ) {
        &search_any;
    } elsif ( $action eq "add_lku_form" ) {
        &add_lku_form;
    } elsif ( $action eq "add_lku" ) {
        &add_lku;
    } elsif ( $action eq "del_lku" ) {
        &del_lku;
    } else {
        &print_main;
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}

############################################################
# Print the main term mgr admin screen
############################################################
sub print_main {

    my $content              = "";
    my $SQL                  = "SELECT * FROM lookup WHERE lku_category = 'LKU_CAT' ORDER BY lku_sort";
    my @rs                   = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $cat_code         = $rec->{lku_code};
        my $cat_desc         = $rec->{lku_description};
        $content            .= "<a href=\"$cgi_url?action=search_any&cat=$cat_code\" class=bullet>$cat_desc</a><br>\n";
    }
    
    $conf->{link_options}    = $content;
    
    my $pg_body              = &disp_form( "lkuadm_main" );

    $env->{page_head}        = "Lookup Admin";
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub lku_maint {

    my $lku_cnt = $shared->getSQLCount( "SELECT count(*) FROM lookup WHERE lku_active = 'Y' AND lku_category = '$cat'" );

    if ( $conf->{lku_cnt} eq "" ) {
        $conf->{lku_cnt}     = $lku_cnt;
    }
    
    my $cat_desc  = $shared->getLookupDesc( "LKU_CAT", $cat );
    $conf->{cat_desc}        = $cat_desc;
    
   	my $abContent = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=add_lku_form>\n"
                  . "<input type=hidden name=cat value=\"$cat\">\n"
                  . $inp_srch
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Add Value\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";

 	$conf->{add_button}  = $abContent;
    
    my $pg_body              = &disp_form( "lku_main" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my $content;
	
	my $srch_head    = "<b>Searched:</b>&nbsp;";
	my $srch_par     = 0;
	
    my $SQL  = "SELECT lku_id, lku_description\n"
             . "FROM lookup\n"
             . "WHERE lku_category = '$cat'\n"
             . "AND lku_active = 'Y'\n";

    if ( $srch_desc gt "" ) {
    	my $sql_srch_desc = $srch_desc;
    	$sql_srch_desc =~ s/'/\\'/g;
	    $SQL .= "AND lku_description LIKE '$sql_srch_desc%'\n";
	    if ( $srch_par > 0 ) {
	        $srch_head .= ", Description starts with <b>$srch_desc</b>";
	    } else {
	        $srch_head .= "Description starts with <b>$srch_desc</b>";
	    }
	    $srch_par++;
    }
	    
    $SQL     .= "ORDER BY lku_description";
    
    if ( $srch_par == 0 ) {
        $srch_head = "<b>All Values</b>";
	}
	
    my @rs   = $shared->getResultSet( $SQL );
    my $row  = 0;
    my $row_color;
    my $fndCnt = 0;
	    
	my $content_head = "";
	my $content_body = "";
	
    $content_head   .= "<span class=subhead><b>$srch_head</b></span><br><br>\n";
    $content_head   .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
    $content_head   .= "<tr bgcolor=\"black\">\n";
    $content_head   .= "<td valign=top align=left class=form><font color=\"white\"><b>ID</b></font></td>\n";
	$content_head   .= "<td valign=top align=left class=form><font color=\"white\"><b>Value</b></font></td>\n";
	$content_head   .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
	foreach my $rec ( @rs ) {
	   	my $id      = $rec->{lku_id};
	   	my $desc    = $rec->{lku_description};
	    	
    	if ( $row == 0 ) {
    		$row_color = $color_bg2;
    		$row = 1;
    	} else {
    		$row_color = $color_bg1;
    		$row = 0;
    	}
	    	
    	$content_body .= "<tr bgcolor=\"$row_color\">\n";
	   	$content_body .= "<td valign=top align=left class=form>$id</td>\n";
	   	$content_body .= "<td valign=top align=left class=form>$desc</td>\n";
	   	$content_body .= "<td valign=top align=right class=form>";
   	    $content_body .= "<a href=\"$cgi_url?action=del_lku&lku_id=$id&cat=$cat" . $sp_vars . "\" onClick=\"return confirm('Deactivate $desc from list?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete $desc\"></a>";
	    $content_body .= "</td></tr>\n";
	    	
	    $fndCnt++;
    }
    $content_body .= "</table>\n";
	    		
    if ( $fndCnt == 0 ) {
    	$content  = $content_head . "\n"
    	          . "<tr bgcolor=\"$color_bg2\"><td valign=top align=left class=form colspan=3><i>No Results Found</i></td></tr>\n"
    	          . "</table>\n";
    } else {
        $content  = $content_head . "\n" . $content_body;
    	$conf->{dom_cnt} = "$fndCnt";
    }
	
   	$conf->{search_results} = $content;
	
	&lku_maint;
	
}



############################################################
# Form to add member to database
############################################################
sub add_lku_form {

    my $cat_desc  = $shared->getLookupDesc( "LKU_CAT", $cat );

   	$conf->{form_head}       = "$cat_desc - Add New Value";
   	$conf->{form_start}      = "<form method=post name=adm_lku_form action=\"$cgi_url\">\n"
    	                     . "<input type=hidden name=action value=\"add_lku\">\n"
    	                     . "<input type=hidden name=cat value=\"$cat\">\n"
    	                     . $inp_srch;
   	
   	$conf->{f_lku_desc}      = "<input name=lku_desc value=\"\" size=40 maxlength=60 class=form>";
   	
   	$conf->{form_submit}     = "<input type=submit value=\"Add Now\" class=form>&nbsp;\n"
                             . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}        = "</form>";
   	$conf->{form_name}       = "adm_lku_form";
   	$conf->{focus_field}     = "lku_desc";  
    	
    my $pg_body = &disp_form( "lku_add_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

############################################################
# Add the record to the loan database
############################################################
sub add_lku {

   	my $lku_desc  = $shared->get_cgi( "lku_desc" );
   	
   	my $descCnt   = $shared->getSQLCount( "SELECT COUNT(*) FROM lookup WHERE lku_category = '$cat' AND lku_desc = '$lku_desc'" );
   	if ( $descCnt == 0 ) {
    	
   	    $lku_desc    =~ s/'/\\'/g;
    	
        my $insert   = "INSERT INTO lookup ( lku_category, lku_description )\n"
                     . "VALUES ( '$cat', '$lku_desc' )";
        $shared->doSQLUpdate( $insert );
        
    } else {
        $java_msg = "Value already exists - Must be unique.";
    }
        	
   	&search_any;

}

###########################################################                   
# Delete Domain from database
#----------------------------------------------------------
sub del_lku {

   	my $lku_id = $shared->get_cgi( "lku_id" );
    	
   	my $update = "UPDATE lookup SET lku_active = 'N' WHERE lku_id = $lku_id";
   	$shared->doSQLUpdate( $update );
    
    &search_any;
}

