#!/usr/bin/perl
###########################################################################
# TM.CGI                    
# Terminology Manager Script - for Same Day Translations
# Created 06/30/2015
###########################################################################
# Copyright 2015, P. Eric Huber
###########################################################################
use strict;
use Excel::Writer::XLSX;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use tmauth;

my $config = new config;
my $shared = new shared;
my $auth   = tmauth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/tm.cgi";
my ($img_path)    = $config->get_img_path;
my ($base_url)    = $config->get_base_url;
my ($site_title)  = $config->get_tm_name;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;
my ($color_bg1)   = "#EEE5DE";
my ($color_bg2)   = "#FFFFFF";
my ($temp_path)   = $config->get_temp_path;
my ($forms_path)  = $config->get_forms_path;
my ($forms_url)   = $config->get_forms_url;
my ($app_mode)    = $config->get_app_mode;

my ($conf);
$conf->{cgi_url}  = $cgi_url;
$conf->{img_path} = $img_path;

my $java_msg      = "";

$conf->{cgi_url}     = $cgi_url;
$conf->{admin_email} = $config->get_admin_email;


# Template Variables
my ($env);
$env->{cgi_url}     = $cgi_url;
$env->{img_path}    = $img_path;
$env->{home_url}    = $base_url;
$env->{html_title}  = $site_title;
$env->{template}    = "tm.html";
$env->{css}         = "tm-style.css";
$env->{nav_buttons} = "<a href=\"$cgi_url?action=tm_add_form\" class=green-button>New Terms</a>\n"
                    . "<a href=\"$cgi_url?action=chg_pass_form\" class=grey-button>Password</a>\n"
                    . "<a href=\"$cgi_url?action=logout\" class=grey-button>Logout</a>";

if ( $app_mode eq "TEST" ) {
    $env->{template} = "tm-test.html";
}

# Main program
my $hash_vars;
$hash_vars->{srch_src}     = $shared->get_cgi( "srch_src" );
$hash_vars->{srch_tgt}     = $shared->get_cgi( "srch_tgt" );
$hash_vars->{srch_dom}     = $shared->get_cgi( "srch_dom" );
$hash_vars->{srch_cln}     = $shared->get_cgi( "srch_cln" );



my $usr_id = $auth->getCurrentUser;
my $action = $shared->get_cgi( "action" );
my $cln_id = $auth->getCurrentUserCLNID;
my $uType  = $auth->getCurrentUserType;
my $uLogin = $auth->getCurrentUserLogin;

if ( $cln_id > 0 ) {
    $hash_vars->{srch_cln} = $cln_id;
}

if ( $usr_id > 0 ) {
	
	$env->{login_stat}      = $auth->getLoginStatus;
	$env->{usr_id}          = $usr_id;
	$conf->{usr_login}      = $auth->getCurrentUserLogin;
	$conf->{usr_pass}       = $auth->getCurrentUserPass;
	
    if ( $action eq "chg_pass_form" ) {
    	$auth->chg_pass_form( $cgi_url );
    } elsif ( $action eq "chg_pass" ) {
    	$java_msg = $auth->chg_pass( $cgi_url );
    	&print_main;
    } elsif ( $action eq "logout" ) {
	    $auth->logout( $cgi_url );
	} elsif ( $action eq "search_any" ) {
	    &search_any;
	} elsif ( $action eq "tm_add_form" ) {
	    &tm_add_form;
	} elsif ( $action eq "tm_insert" ) {
	    &tm_insert;
	} elsif ( $action eq "tm_edit_form" ) {
	    &tm_edit_form;
	} elsif ( $action eq "tm_update" ) {
	    &tm_update;
	} elsif ( $action eq "tm_drop" ) {
	    &tm_drop;
	} elsif ( $action eq "tm_export" ) {
        my $excelURL = &tm_export;
        $env->{open_file} = $excelURL;
        &search_any;
    } else {
    	&print_main;
    }
} else {
	if ( $action eq "" ) {
		$auth->login( $cgi_url, "Y" );
	} else {
	    $auth->login( $cgi_url, "N" );
	}
}

exit;


###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my $content = "";

    my $selDom  = $shared->pop_lookup_select( "TM_DOMAIN" );
    
    $content .= "<form method=post action=\"$cgi_url\">\n";
    $content .= "<input type=hidden name=action value=\"search_any\">\n";
    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=786>\n";
    $content .= "<tr><td valign=top align=left colspan=2 class=overview>Search Options</td></tr>\n";
    $content .= "<tr><td valign=top align=left class=label width=136>Source Term</td>\n";
    $content .= "<td valign=top align=left class=form width=650><input name=srch_src value=\"\" size=30 class=form></td></tr>\n";
    $content .= "<tr><td valign=top align=left class=label>Target Term</td>\n";
    $content .= "<td valign=top align=left class=form><input name=srch_tgt value=\"\" size=30 class=form></td></tr>\n";
    $content .= "<tr><td valign=top align=left class=label>Domain</td>\n";
    $content .= "<td valign=top align=left class=form><select name=srch_dom class=form><option value=\"\"></option>" . $selDom . "</select></td></tr>\n";
    
    if ( $uType eq "OTH" ) {
        $content .= "<tr><td valign=top align=left class=label>Client</td>\n";
        $content .= "<td valign=top align=left class=form><select name=srch_cln class=form><option value=\"0\"></option>" . &getCLNSelect . "</select></td></tr>\n";
    }
    
    $content .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
    $content .= "<td valign=top align=left class=form><input type=submit value=\"Search Now\" class=form></td></tr>\n";
    
    if ( $conf->{search_results} gt "" ) {
        
        $content .= "<tr><td valign=top align=left colspan=2 class=overview><br>Search Results</td></tr>\n";
        $content .= "<tr><td valign=top align=left colspan=2>\n";
        $content .= $conf->{search_results};
        $content .= "</td></tr>\n";
    
    }
    
    $content .= "</table>\n";
    
    $conf->{pg_content} = $content; 	
    
    my $pg_body         = &disp_form( "tm_main" );

    $env->{page_head}   = "Find Terms";
    $env->{page_body}   = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my $content;
	
	my $srch_src     = $hash_vars->{srch_src};
	my $srch_tgt     = $hash_vars->{srch_tgt};
	my $srch_dom     = $hash_vars->{srch_dom};
	my $srch_cln     = $hash_vars->{srch_cln};
	my $srch_head    = "<b>Searched:</b>&nbsp;";
	my $sp_vars      = "";
	my $srch_par     = 0;

    my $SQL;
    
	if ( ( $srch_src gt "" ) || ( $srch_tgt gt "" ) || ( $srch_dom gt "" ) || ( $srch_cln > 0 ) ) {
	
  	    $SQL = "SELECT tmm_id, tmm_src_value, tmm_tgt_value, tmm_src_lng_id, tmm_tgt_lng_id,\n"
  	         . "tmm_src_context, tmm_tgt_context,\n"
  	         . "src.lng_desc as src_lang, tgt.lng_desc as tgt_lang, tmm_domain, cln_name\n"
             . "FROM tm_main\n"
             . "INNER JOIN language src ON tmm_src_lng_id = src.lng_id\n"
             . "INNER JOIN language tgt ON tmm_tgt_lng_id = tgt.lng_id\n"
             . "LEFT JOIN client ON tmm_cln_id = cln_id\n"
             . "WHERE 1\n";

	    if ( $srch_src gt "" ) {
		    $SQL .= "AND tmm_src_value LIKE '$srch_src%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Source starts with <b>$srch_src</b>";
		    } else {
		        $srch_head .= "Source starts with <b>$srch_src</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_src=$srch_src";
	    }
	    if ( $srch_tgt gt "" ) {
		    $SQL .= "AND tmm_tgt_value LIKE '$srch_tgt%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Target starts with <b>$srch_tgt</b>";
		    } else {
		        $srch_head .= "Target starts with <b>$srch_tgt</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_tgt=$srch_tgt";
	    }

	    if ( $srch_dom gt "" ) {
	        my $domDesc = $shared->getLookupDesc( "TM_DOMAIN", $srch_dom );
		    $SQL .= "AND tmm_domain = '$srch_dom'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Domain is <b>$domDesc</b>";
		    } else {
		        $srch_head .= "Domain is <b>$domDesc</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_dom=$srch_dom";
	    }

	    if ( $srch_cln > 0 ) {
	        my $cName = $shared->getFieldVal( "client", "cln_name", "cln_id", $srch_cln );
		    $SQL .= "AND tmm_cln_id = $srch_cln\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Client is <b>$cName</b>";
		    } else {
		        $srch_head .= "Client is <b>$cName</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_cln=$srch_cln";
	    }
	} else {
   	    $SQL = "SELECT tmm_id, tmm_src_value, tmm_tgt_value, tmm_src_lng_id, tmm_tgt_lng_id,\n"
             . "src.lng_desc as src_lang, tgt.lng_desc as tgt_lang, tmm_domain, cln_name\n"
             . "FROM tm_main\n"
             . "INNER JOIN language src ON tmm_src_lng_id = src.lng_id\n"
             . "INNER JOIN language tgt ON tmm_tgt_lng_id = tgt.lng_id\n"
             . "LEFT JOIN client ON tmm_cln_id = cln_id\n";
        $srch_head .= "All Database Terms";
    }


    $SQL .= "ORDER BY tmm_src_value";
        	    
	my @rs   = $shared->getResultSet( $SQL );
	my $row  = 0;
	my $row_color;
	my $err_color = "#FFCCCC";
	my $fndCnt = 0;
	    
	$content .= "<span class=label>$srch_head</span><br><br>\n";
	$content .= "<table border=0 cellpadding=2 cellspacing=0 width=782>\n";
	$content .= "<tr>\n";
	$content .= "<td valign=top align=left class=colhead2>Source Term</td>\n";
	$content .= "<td valign=top align=left class=colhead2>Target Term</td>\n";
	$content .= "<td valign=top align=left class=colhead2>Domain</td>\n";
	$content .= "<td valign=top align=left class=colhead2>Client</td>\n";
	$content .= "<td valign=top align=right class=colhead2><a href=\"$cgi_url?action=tm_export" . $sp_vars . "\"><img src=\"$img_path/icon_export.png\" border=0 alt=\"Export Results\" title=\"Export Results to Excel\"></a></td></tr>\n";
	foreach my $rec ( @rs ) {
	    my $id      = $rec->{tmm_id};
	    my $src     = $rec->{tmm_src_value};
	    my $tgt     = $rec->{tmm_tgt_value};
	    my $sTxt    = $rec->{tmm_src_context};
	    my $tTxt    = $rec->{tmm_tgt_context};
	    my $sLng    = $rec->{src_lang};
	    my $tLng    = $rec->{tgt_lang};
	    my $domain  = $shared->getLookupDesc( "TM_DOMAIN", $rec->{tmm_domain} );
	    my $cName   = $rec->{cln_name};
	    	
	    if ( $row == 0 ) {
	    	$row_color = $color_bg2;
	    	$row = 1;
	    } else {
	    	$row_color = $color_bg1;
	    	$row = 0;
	    }
	    $content .= "<tr bgcolor=\"$row_color\">\n";
	    if ( $sTxt gt "" ) {
	        $content .= "<td valign=top align=left class=form><span class=help title=\"$sTxt\">$src ($sLng)</span></td>\n";
	    } else {
	        $content .= "<td valign=top align=left class=form>$src ($sLng)</td>\n";
        }
        if ( $tTxt gt "" ) {
            $content .= "<td valign=top align=left class=form><span class=help title=\"$tTxt\">$tgt ($tLng)</span></td>\n";
        } else {
            $content .= "<td valign=top align=left class=form>$tgt ($tLng)</td>\n";
        }
	    $content .= "<td valign=top align=left class=form>$domain</td>\n";
	    $content .= "<td valign=top align=left class=form>$cName</td>\n";
	    $content .= "<td valign=top align=right class=form>";
    	$content .= "<a href=\"$cgi_url?action=tm_edit_form&tmm_id=$id" . $sp_vars . "\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"Edit Terms\" title=\"Edit Terms\"></a>";
	    $content .= "</td></tr>\n";
	    	
	    $fndCnt++;
	}
	$content .= "</table>\n";
	    		
	if ( $fndCnt == 0 ) {
	   	$content  = "";
	   	$java_msg = "No Terms found meeting search criteria.";
	}
	
    $conf->{search_results} = $content;

	&print_main;
	
}

###########################################################
# Return a select list of linguists.
#----------------------------------------------------------
sub getCLNSelect {
    
    my ( $cur_val ) = @_;
    my $retval;

    if ( $cur_val gt "" ) {
        my $hash = $shared->getResultRec( "SELECT * FROM client WHERE cln_id = $cur_val" );
        my $tmA  = $hash->{cln_tm_access};
        my $name = $hash->{cln_name};
        if ( $tmA eq "N" ) {
            $retval .= "<option selected value=\"$cur_val\">$name</option>";
        }
    }
    
    my @rs         = $shared->getResultSet( "SELECT cln_id, cln_name FROM client WHERE cln_active = 'Y' and cln_tm_access = 'Y' ORDER BY cln_name" );
    
    foreach my $rec ( @rs ) {
        my $code = $rec->{cln_id};
        my $name = $rec->{cln_name};
        if ( $cur_val == $code ) {
            $retval .= "<option selected value=\"$code\">$name</option>";
        } else {
            $retval .= "<option value=\"$code\">$name</option>";
        }
    }
    
    return $retval;
}

###########################################################
# Form to add new term record.
#----------------------------------------------------------
sub tm_add_form {

    my $selLangSrc = &getLang( "", "ENU" );
    my $selLangTgt = &getLang;
    
   	$conf->{f_src_lang}    = "<select name=tmm_src_lng_id class=form>" . $selLangSrc . "</select>";
   	$conf->{f_tgt_lang}    = "<select name=tmm_tgt_lng_id class=form>" . $selLangTgt . "</select>";
   	$conf->{f_src_value}   = "<input name=tmm_src_value value=\"\" size=40 class=form>";
   	$conf->{f_tgt_value}   = "<input name=tmm_tgt_value value=\"\" size=40 class=form>";
   	$conf->{f_src_context} = "<textarea name=tmm_src_context cols=40 rows=5 wrap class=form></textarea>";
   	$conf->{f_tgt_context} = "<textarea name=tmm_tgt_context cols=40 rows=5 wrap class=form></textarea>"; 
   	
   	if ( $uType eq "OTH" ) {
   	    $conf->{domain_row} = "<tr><td valign=top align=left class=label>Domain</td>\n"
   	                        . "<td valign=top align=left class=form><select name=tmm_domain class=form>\n"
   	                        . "<option value=\"\"></option>" . $shared->pop_lookup_select( "TM_DOMAIN" ) . "</select></td></tr>\n";
   	    $conf->{client_row} = "<tr><td valign=top align=left class=label>Client</td>\n"
   	                        . "<td valign=top align=left class=form><select name=tmm_cln_id class=form>\n"
   	                        . &getCLNSelect . "</select></td></tr>\n";
   	} else {
   	    $conf->{domain_row} = "";
   	    $conf->{client_row} = "";
   	}
    	
    my $pg_body = &disp_form( "tm_add_form" );

    $env->{page_head}   = "Add Term Pair";
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

###########################################################
# Add new term record.
#----------------------------------------------------------
sub tm_insert {

    my $src_lng_id   = $shared->get_cgi( "tmm_src_lng_id" );
    my $tgt_lng_id   = $shared->get_cgi( "tmm_tgt_lng_id" );
    my $src_val      = $shared->get_cgi( "tmm_src_value" );
    my $tgt_val      = $shared->get_cgi( "tmm_tgt_value" );
    my $src_cntxt    = $shared->get_cgi( "tmm_src_context" );
    my $tgt_cntxt    = $shared->get_cgi( "tmm_tgt_context" );
    my $domain       = $shared->get_cgi( "tmm_domain" );
    my $tmm_cln_id   = $cln_id;
    
    if ( $uType eq "OTH" ) {
        $tmm_cln_id = $shared->get_cgi( "tmm_cln_id" );
    }
    
    $src_val =~ s/'/\\'/g;
    $tgt_val =~ s/'/\\'/g;
    $src_cntxt =~ s/'/\\'/g;
    $tgt_cntxt =~ s/'/\\'/g;
    
    my $insert = "INSERT INTO tm_main ( tmm_src_lng_id, tmm_tgt_lng_id, tmm_src_value, tmm_tgt_value,\n"
               . "tmm_src_context, tmm_tgt_context, tmm_domain, tmm_cln_id ) VALUES ( $src_lng_id,\n"
               . "$tgt_lng_id, '$src_val', '$tgt_val', '$src_cntxt', '$tgt_cntxt',\n"
               . "'$domain', $tmm_cln_id )";
    $shared->doSQLUpdate( $insert );
    
    &print_main;    	
}


###########################################################
# Form to edit terms record.
#----------------------------------------------------------
sub tm_edit_form {

    my $tmm_id = $shared->get_cgi( "tmm_id" );
    my $rec    = $shared->get_rec( "tm_main", "tmm_id", $tmm_id );
    my $sVal   = $rec->{tmm_src_value};
    my $tVal   = $rec->{tmm_tgt_value};
    my $sCntxt = $rec->{tmm_src_context};
    my $tCntxt = $rec->{tmm_tgt_context};
    my $sLng   = $rec->{tmm_src_lng_id};
    my $tLng   = $rec->{tmm_tgt_lng_id};
    my $domain = $rec->{tmm_domain};
    my $cID    = $rec->{tmm_cln_id};
    
    $conf->{tmm_id}        = $tmm_id;
   	$conf->{f_src_lang}    = $shared->getFieldVal( "language", "lng_desc", "lng_id", $sLng );
   	$conf->{f_tgt_lang}    = $shared->getFieldVal( "language", "lng_desc", "lng_id", $tLng );
   	$conf->{f_src_value}   = "<input name=tmm_src_value value=\"$sVal\" size=40 class=form>";
   	$conf->{f_tgt_value}   = "<input name=tmm_tgt_value value=\"$tVal\" size=40 class=form>";
   	$conf->{f_src_context} = "<textarea name=tmm_src_context cols=40 rows=5 wrap class=form>" . $sCntxt . "</textarea>";
   	$conf->{f_tgt_context} = "<textarea name=tmm_tgt_context cols=40 rows=5 wrap class=form>" . $tCntxt . "</textarea>";
   	$conf->{f_domain}      = $shared->getLookupDesc( "TM_DOMAIN", $domain );
   	$conf->{f_client}      = $shared->getFieldVal( "client", "cln_name", "cln_id", $cID );
   	
   	if ( $uType eq "OTH" ) {
   	    $conf->{f_domain}   = "<select name=tmm_domain class=form>\n"
   	                        . "<option value=\"\"></option>" . $shared->pop_lookup_select( "TM_DOMAIN", $domain ) . "</select>";
   	    $conf->{f_client} = "<select name=tmm_cln_id class=form>\n"
   	                        . &getCLNSelect( $cID ) . "</select>";
   	}

	my $srch_src  = $hash_vars->{srch_src};
	my $srch_tgt  = $hash_vars->{srch_tgt};
	my $srch_dom  = $hash_vars->{srch_dom};
	my $srch_cln  = $hash_vars->{srch_cln};
	
    my $inp_srch     = "";
    my $sp_vars      = "";
    if ( $srch_src gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_src value=\"$srch_src\">\n";
    	$sp_vars  .= "&srch_src=$srch_src";
    }
    if ( $srch_tgt gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_tgt value=\"$srch_tgt\">\n";
    	$sp_vars  .= "&srch_tgt=$srch_tgt";
    }
    if ( $srch_dom gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_dom value=\"$srch_dom\">\n";
    	$sp_vars  .= "&srch_dom=$srch_dom";
    }
    if ( $srch_cln gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_cln value=\"$srch_cln\">\n";
    	$sp_vars  .= "&srch_cln=$srch_cln";
    }

    $conf->{inp_srch}     = $inp_srch;
    $conf->{canc_url}     = "$cgi_url?action=search_any" . $sp_vars;
    $conf->{drop_url}     = "$cgi_url?action=tm_drop&tmm_id=$tmm_id" . $sp_vars;

    my $pg_body = &disp_form( "tm_edit_form" );

    $env->{page_head}     = "Edit Term Pair";
    $env->{page_body}     = $pg_body;
    $shared->page_gen( $env, $java_msg );

}

###########################################################
# Update term record.
#----------------------------------------------------------
sub tm_update {

    my $tmm_id       = $shared->get_cgi( "tmm_id" );
    my $src_val      = $shared->get_cgi( "tmm_src_value" );
    my $tgt_val      = $shared->get_cgi( "tmm_tgt_value" );
    my $src_cntxt    = $shared->get_cgi( "tmm_src_context" );
    my $tgt_cntxt    = $shared->get_cgi( "tmm_tgt_context" );
    my $domain       = $shared->get_cgi( "tmm_domain" );
    my $tmm_cln_id   = $shared->get_cgi( "tmm_cln_id" );
    
    $src_val =~ s/'/\\'/g;
    $tgt_val =~ s/'/\\'/g;
    $src_cntxt =~ s/'/\\'/g;
    $tgt_cntxt =~ s/'/\\'/g;
    
    my $update  = "UPDATE tm_main SET tmm_src_value = '$src_val', tmm_tgt_value = '$tgt_val',\n"
                . "tmm_src_context = '$src_cntxt', tmm_tgt_context = '$tgt_cntxt',\n"
                . "tmm_domain = '$domain', tmm_cln_id = $tmm_cln_id WHERE tmm_id = $tmm_id";
    if ( $uType eq "CLN" ) {
        $update = "UPDATE tm_main SET tmm_src_value = '$src_val', tmm_tgt_value = '$tgt_val',\n"
                . "tmm_src_context = '$src_cntxt', tmm_tgt_context = '$tgt_cntxt'\n"
                . "WHERE tmm_id = $tmm_id";
    }

    $shared->doSQLUpdate( $update );
    
    &search_any;    	
}

###########################################################
# Delete term record.
#----------------------------------------------------------
sub tm_drop {

    my $tmm_id       = $shared->get_cgi( "tmm_id" );
    
    my $update  = "DELETE FROM tm_main WHERE tmm_id = $tmm_id";
    
    $shared->doSQLUpdate( $update );
    
    &search_any;    	
}

##########################################################                   
# Get Languages as selection list
#----------------------------------------------------------
sub getLang {

    my ( $cur_val, $dflt ) = @_;
        
    if ( ( $dflt gt "" ) && ( $cur_val eq "" ) ) {
        $cur_val = $dflt;
    }
    
    my $SQL = "SELECT * FROM language WHERE lng_tm_access = 'Y' ORDER BY lng_desc";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{lng_id};
        my $desc     = $rec->{lng_desc};
        if ( $cur_val == $code ) {
            $result .= "<option selected value=\"$code\">$desc</option>";
        } else {
            $result .= "<option value=\"$code\">$desc</option>";
        }
    }
    
    return $result;
}

############################################################
# Generate Terms Excel Export
############################################################
sub tm_export {

    my $rptDate  = $shared->getSQLToday;
    
	my $fName    = "$forms_path/" . $uLogin . "_export.xlsx";
	my $urlName  = "$forms_url/" . $uLogin . "_export.xlsx";
	my $nowDesc  = $shared->getDateDesc( $rptDate, 1, 1 );
	
	my $workbook = Excel::Writer::XLSX->new( $fName );
	$workbook->set_tempdir( $temp_path );
	
	my $c_litegrey    = $workbook->set_custom_color( 40, 204, 204, 204 );
	my $c_litegreen   = $workbook->set_custom_color( 41, 210, 255, 214 );
    my %headFont      = ( font  => 'Arial', size  => 12, color => 'black', valign  => 'bottom', bold  => 1 );
    my %titleFont     = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom' );
    my %titleFontRt   = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom', align => 'right' );
    my %colFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'bottom', bold => 1, bg_color => $c_litegrey );
    
    my %bodyFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', border => 1, border_color => $c_litegrey );
    my %boldFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, border => 1, border_color => $c_litegrey );
    my %dateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', num_format => 'mm/dd/yy' );
	
	my $fmtHead  = $workbook->add_format( %headFont );
	my $fmtTitle = $workbook->add_format( %titleFont );
	my $fmtTtlRt = $workbook->add_format( %titleFontRt );
	my $fmtCol   = $workbook->add_format( %colFont );
	my $fmtBody  = $workbook->add_format( %bodyFont );
	my $fmtBold  = $workbook->add_format( %boldFont );
	my $fmtDate  = $workbook->add_format( %dateFont );

	my $srch_src     = $hash_vars->{srch_src};
	my $srch_tgt     = $hash_vars->{srch_tgt};
	my $srch_dom     = $hash_vars->{srch_dom};
	my $srch_cln     = $hash_vars->{srch_cln};
	my $srch_head    = "Export Condition: ";
	my $srch_par     = 0;
    my $SQL          = "";

    $SQL = "SELECT tmm_id, tmm_src_value, tmm_tgt_value, tmm_src_context, tmm_tgt_context,\n"
         . "tmm_src_lng_id, tmm_tgt_lng_id,\n"
         . "src.lng_desc as src_lang, tgt.lng_desc as tgt_lang, tmm_domain, cln_name\n"
         . "FROM tm_main\n"
         . "INNER JOIN language src ON tmm_src_lng_id = src.lng_id\n"
         . "INNER JOIN language tgt ON tmm_tgt_lng_id = tgt.lng_id\n"
         . "LEFT JOIN client ON tmm_cln_id = cln_id\n"
         . "WHERE 1\n";

    if ( $srch_src gt "" ) {
	    $SQL .= "AND tmm_src_value LIKE '$srch_src%'\n";
	    if ( $srch_par > 0 ) {
	        $srch_head .= ", Source starts with $srch_src";
	    } else {
	        $srch_head .= "Source starts with $srch_src";
	    }
	    $srch_par++;
	}
	if ( $srch_tgt gt "" ) {
	    $SQL .= "AND tmm_tgt_value LIKE '$srch_tgt%'\n";
	    if ( $srch_par > 0 ) {
	        $srch_head .= ", Target starts with $srch_tgt</b>";
	    } else {
	        $srch_head .= "Target starts with $srch_tgt";
	    }
	    $srch_par++;
	}

	if ( $srch_dom gt "" ) {
	    my $domDesc = $shared->getLookupDesc( "TM_DOMAIN", $srch_dom );
	    $SQL .= "AND tmm_domain = '$srch_dom'\n";
	    if ( $srch_par > 0 ) {
	        $srch_head .= ", Domain is $domDesc";
	    } else {
	        $srch_head .= "Domain is $domDesc";
	    }
	    $srch_par++;
	}

	if ( $srch_cln > 0 ) {
	    my $cName = $shared->getFieldVal( "client", "cln_name", "cln_id", $srch_cln );
	    $SQL .= "AND tmm_cln_id = $srch_cln\n";
	    if ( $srch_par > 0 ) {
	        $srch_head .= ", Client is $cName";
	    } else {
	        $srch_head .= "Client is $cName";
	    }
	    $srch_par++;
	}

    $SQL .= "ORDER BY tmm_src_value";
    
    if ( $srch_par == 0 ) {
        $srch_head .= "All Database Terms";
    }
   
    my @rs   = $shared->getResultSet( $SQL );
    	             
    my $sheet    = $workbook->add_worksheet( "Terms Export" );
            
    $sheet->write_string( 0, 0, $srch_head, $fmtHead );
    $sheet->write_string( 0, 7, "As of: $nowDesc", $fmtTtlRt );
        
    my @colHead  = ( "Source Term", "Source Context", "Source Language", "Target Term", "Target Context", "Target Language", "Domain", "Client" );
    $sheet->write_row( 2, 0, \@colHead, $fmtCol );
        
    my $row = 3;
	foreach my $rec ( @rs ) {

	  	my $id      = $rec->{tmm_id};
	   	my $src     = $rec->{tmm_src_value};
	   	my $tgt     = $rec->{tmm_tgt_value};
	   	my $sCntxt  = $rec->{tmm_src_context};
	   	my $tCntxt  = $rec->{tmm_tgt_context};
	   	my $sLng    = $rec->{src_lang};
	   	my $tLng    = $rec->{tgt_lang};
	   	my $domain  = $shared->getLookupDesc( "TM_DOMAIN", $rec->{tmm_domain} );
	   	my $cName   = $rec->{cln_name};
 
        $sheet->write_string( $row, 0, $src, $fmtBody );
        $sheet->write_string( $row, 1, $sCntxt, $fmtBody );
        $sheet->write_string( $row, 2, $sLng, $fmtBody );
        $sheet->write_string( $row, 3, $tgt, $fmtBody );
        $sheet->write_string( $row, 4, $tCntxt, $fmtBody );
        $sheet->write_string( $row, 5, $tLng, $fmtBody );
        $sheet->write_string( $row, 6, $domain, $fmtBody );
        $sheet->write_string( $row, 7, $cName, $fmtBody );

        $row++;
    }

    $sheet->set_column( 0, 0, 40 );
    $sheet->set_column( 1, 1, 60 );
    $sheet->set_column( 2, 2, 25 );
    $sheet->set_column( 3, 3, 40 );
    $sheet->set_column( 4, 4, 60 );
    $sheet->set_column( 5, 5, 25 );
    $sheet->set_column( 6, 6, 15 );
    $sheet->set_column( 7, 7, 40 );

    $sheet->autofilter( 2, 0, $row, 5 );
    $sheet->hide_gridlines( 2 );
    $sheet->freeze_panes( 3, 0 );

    $workbook->close();
    
    system( "chmod 644 $fName" );
    
    return $urlName;

}
