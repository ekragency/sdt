#!/usr/bin/perl
###########################################################################
# ligadm.cgi                Client Administration
###########################################################################
# Copyright 2013, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;
use Excel::Writer::XLSX;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/ligadm.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($temp_path)   = $config->get_temp_path;
my ($forms_path)  = $config->get_forms_path;
my ($forms_url)   = $config->get_forms_url;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $exceptList    = "";
my $dupList       = "";
my $debug         = $config->get_debug;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;


# Main program
my $usr_id = 0;
my $action = $shared->get_cgi( "action" );
my $hash_vars;
my $next_action = $shared->get_cgi( "next_act" );

$hash_vars->{srch_name}    = $shared->get_cgi( "srch_name" );
$hash_vars->{srch_src}     = $shared->get_cgi( "srch_src" );
$hash_vars->{srch_tgt}     = $shared->get_cgi( "srch_tgt" );
$hash_vars->{srch_spc}     = $shared->get_cgi( "srch_spc" );
$hash_vars->{srch_int}     = $shared->get_cgi( "srch_int" );
$hash_vars->{srch_sdu}     = $shared->get_cgi( "srch_sdu" );
$hash_vars->{srch_mtpe}    = $shared->get_cgi( "srch_mtpe" );
$hash_vars->{next_act}     = $next_action;
$hash_vars->{sort_fld}     = $shared->get_cgi( "sort_fld" );
$hash_vars->{sort_flw}     = $shared->get_cgi( "sort_flw" );



#
# Main Program
#
 
if ( $auth->validate_access( "ligadm" ) ) {
	
	$usr_id    = $auth->getCurrentUser;

    if ( $action eq "search_any" ) {
        &search_any;
    } elsif ( $action eq "change_sort" ) {
    	&change_sort;
    } elsif ( $action eq "edit_lig_form" ) {
        &edit_lig_form;
    } elsif ( $action eq "edit_lig" ) {
        &edit_lig;
    } elsif ( $action eq "add_lig_form" ) {
        &add_lig_form;
    } elsif ( $action eq "add_lig" ) {
        &add_lig;
    } elsif ( $action eq "del_lig" ) {
        &del_lig;
    } elsif ( $action eq "add_rate" ) {
    	&add_rate;
    } elsif ( $action eq "del_rate" ) {
    	&del_rate;
    } elsif ( $action eq "audit_log" ) {
    	&audit_log;
    } elsif ( $action eq "lig_rpt" ) {
        my $excelURL = &genLinguistRpt;
        $env->{open_file} = $excelURL;
        &search_any( "Y" );
    } elsif ( $action eq "lig_unda_form" ) {
        &unda_form;
    } elsif ( $action eq "lig_unda_import" ) {
        &unda_import;
    } else {
        &search_any( "Y" );
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my $lig_cnt = $shared->getSQLCount( "SELECT count(*) FROM linguist" );

    if ( $conf->{lig_cnt} eq "" ) {
        $conf->{lig_cnt}     = $lig_cnt;
    }
    
   	my $abContent = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=add_lig_form>\n"
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Add Linguist\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";

 	$conf->{add_button}  = $abContent;
 	$conf->{selLang}     = &getLang;
 	$conf->{selSpec}     = &getSpecList;
 	
    
    my $pg_body              = &disp_form( "lig_main" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my ( $init_srch ) = @_;
	
	my $content;
	
	my $srch_name    = $hash_vars->{srch_name};
	my $srch_src     = $hash_vars->{srch_src};
	my $srch_tgt     = $hash_vars->{srch_tgt};
	my $srch_int     = $hash_vars->{srch_int};
	my $srch_sdu     = $hash_vars->{srch_sdu};
	my $srch_spc     = $hash_vars->{srch_spc};
	my $srch_mtpe    = $hash_vars->{srch_mtpe};
	my $sort_fld     = $hash_vars->{sort_fld};
	my $sort_flw     = $hash_vars->{sort_flw};
	my $srch_head    = "<b>Searched:</b>&nbsp;";
	my $srch_par     = 0;
	my $sp_vars      = "";

	if ( $sort_fld eq "" ) {
		$sort_fld = "name";
	}
	if ( $sort_flw eq "" ) {
		$sort_flw = "A";
	}
	if ( $srch_sdu eq "" ) {
	    $srch_sdu = "N";
	}
	
	if ( ( $srch_name gt "" ) || ( $srch_src gt "" ) || ( $srch_tgt gt "" ) || 
	     ( $srch_spc gt "" ) || ( $srch_mtpe gt "" ) || ( $srch_int gt "" ) || 
	     ( $srch_sdu eq "Y" ) || ( $init_srch eq "Y" ) ) {
	
	    my $SQL;
	    
  	    $SQL = "SELECT DISTINCT lig_id, lig_last_name, lig_first_name, lig_currency, lig_email, lil_trans_rate,\n"
  	         . "lil_edit_rate, lil_hourly_rate, lig_dont_use\n";
  	         
  	    if ( ( ( $srch_src gt "" ) || ( $srch_tgt gt "" ) ) && ( $srch_spc gt "" ) ) {
  	        $SQL .= "FROM linguist_lang\n"
  	              . "INNER JOIN linguist ON lig_id = lil_lig_id\n"
  	              . "INNER JOIN linguist_spec ON ( lis_lig_id = lig_id AND lis_spc_id = $srch_spc )\n";
  	    } elsif ( $srch_spc gt "" ) {
  	        $SQL .= "FROM linguist\n"
  	              . "INNER JOIN linguist_spec ON ( lis_lig_id = lig_id AND lis_spc_id = $srch_spc )\n"
  	              . "LEFT JOIN linguist_lang ON lil_lig_id = lig_id\n";
  	    } elsif ( $srch_spc eq "" ) {
  	        $SQL .= "FROM linguist_lang\n"
                  . "INNER JOIN linguist ON lig_id = lil_lig_id\n";
        }
        
        if ( $srch_sdu eq "Y" ) {
            $SQL .= "WHERE 1\n";
        } else {
            $SQL .= "WHERE lig_dont_use = 'N'\n";
        }

	    if ( $srch_name gt "" ) {
	    	my $lig_srch_name = $srch_name;
	    	$lig_srch_name =~ s/'/\\'/g;
  	        $SQL = "SELECT lig_id, lig_last_name, lig_first_name, lig_currency, lig_email, lil_trans_rate,\n"
  	             . "lil_edit_rate, lil_hourly_rate, lig_dont_use\n"
                 . "FROM linguist\n";
            if ( $srch_spc gt "" ) {
                $SQL .= "INNER JOIN linguist_spec ON ( lis_lig_id = lig_id AND lis_spc_id = $srch_spc )\n";
            }
            $SQL .= "LEFT JOIN linguist_lang ON lil_lig_id = lig_id\n"
                  . "WHERE lig_last_name LIKE '$lig_srch_name%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Last name starts with <b>$srch_name</b>";
		    } else {
		        $srch_head .= "Last name starts with <b>$srch_name</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_name=$srch_name";
	    }
	    if ( $srch_src gt "" ) {
	        my $srcDesc = &getLangDesc( $srch_src );
		    $SQL .= "AND lil_src_lng_id = $srch_src\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Source Language is <b>$srcDesc</b>";
		    } else {
		        $srch_head .= "Source Language is <b>$srcDesc</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_src=$srch_src";
	    }
	    if ( $srch_tgt gt "" ) {
	        my $tgtDesc = &getLangDesc( $srch_tgt );
		    $SQL .= "AND lil_tgt_lng_id = $srch_tgt\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Target Language is <b>$tgtDesc</b>";
		    } else {
		        $srch_head .= "Target Language is <b>$tgtDesc</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_tgt=$srch_tgt";
	    }
	    if ( $srch_spc gt "" ) {
	        my $spcDesc = $shared->getFieldVal( "specialty", "spc_description", "spc_id", $srch_spc );
	        if ( $srch_par > 0 ) {
	            $srch_head .= ", Specialty is <b>$spcDesc</b>";
	        } else {
	            $srch_head .= "Specialty is <b>$spcDesc</b>";
	        }
	        $srch_par++;
	        $sp_vars       .= "&srch_spc=$srch_spc";
	    }
	    if ( $srch_mtpe gt "" ) {
	        $SQL .= "AND lig_does_mtpe = '$srch_mtpe'\n";
	        my $mtpeDesc = "Does NOT do MTPE";
	        if ( $srch_mtpe eq "Y" ) {
	            $mtpeDesc = "Does MTPE";
	        }
	        if ( $srch_par > 0 ) {
	            $srch_head .= ", <b>$mtpeDesc</b>";
	        } else {
	            $srch_head .= "<b>$mtpeDesc</b>";
	        }
	        $srch_par++;
	        $sp_vars        .= "&srch_mtpe=$srch_mtpe";
	    }
	    if ( $srch_int gt "" ) {
	        $SQL .= "AND lig_is_interpreter = 'Y'\n";
	        if ( $srch_par > 0 ) {
	            $srch_head .= ", Interpreters Only";
	        } else {
	            $srch_head .= "Interpreters Only";
	        }
	        $srch_par++;
	        $sp_vars       .= "&srch_int=Y";
	    }
	    if ( $srch_sdu eq "Y" ) {
	        if ( $srch_par > 0 ) {
	            $srch_head .= ", Showing DO NOT USE";
	        } else {
	            $srch_head .= "Showing DO NOT USE";
	        }
	    }

        my $doFlow = 1;
	    if ( $sort_fld eq "tran" ) {
	    	$SQL     .= "ORDER BY lil_trans_rate";
	    	$sp_vars .= "&sort_fld=tran";
	    } elsif ( $sort_fld eq "edit" ) {
	    	$SQL     .= "ORDER BY lil_edit_rate";
	    	$sp_vars .= "&sort_fld=edit";
	    } elsif ( $sort_fld eq "hour" ) {
	    	$SQL     .= "ORDER BY lil_hourly_rate";
	    	$sp_vars .= "&sort_fld=hour";
	    } else {
	        if ( $sort_flw eq "D" ) {
	    	    $SQL .= "ORDER BY lig_last_name DESC, lig_first_name DESC";
    	    	$sp_vars .= "&sort_fld=name&sort_flw=D";
    	    } else {
    	        $SQL .= "ORDER BY lig_last_name, lig_first_name";
    	        $sp_vars .= "&sort_fld=name&sort_flw=A";
    	    }
    	    $doFlow = 0;   
	    }
	    if ( $doFlow ) {
	        if ( $sort_flw eq "D" ) {
	    	    $SQL     .= " DESC";
	    	    $sp_vars .= "&sort_flw=D";
	        } else {
	    	    $sp_vars .= "&sort_flw=A";
	        }
	    }
	    
	    my $doReset = 0;
	    if ( ( $init_srch eq "Y" ) && ( $srch_par == 0 ) ) {
	        
	    	$srch_head = "All Linguists";
	    	$sp_vars  .= "&srch_init=Y";
	    } else {
	        $doReset = 1;
	    }
	
	    my @rs   = $shared->getResultSet( $SQL );
	    my $row  = 0;
	    my $row_color;
	    my $err_color = "#FFCCCC";
	    my $fndCnt = 0;
	    
	    $content .= "<span class=subhead><b>$srch_head</b></span><br><br>\n";
	    if ( $doReset ) {
	    	$content .= "<a href=\"$cgi_url\" class=nav><b>[ Clear Filter ]</b></a><br>\n";
	    }
	    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	    $content .= "<tr bgcolor=\"black\">\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td>\n";
	    $content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=name" . $sp_vars . "\"><font color=\"white\"><b>Name</b></font></a></td>\n";
	    $content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=tran" . $sp_vars . "\"><font color=\"white\"><b>TR Rate</b></font></a></td>\n";
	    $content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=edit" . $sp_vars . "\"><font color=\"white\"><b>RE Rate</b></font></a></td>\n";
	    $content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=hour" . $sp_vars . "\"><font color=\"white\"><b>Hourly Rate</b></font></a></td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>E-mail</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
	    foreach my $rec ( @rs ) {
	    	my $id      = $rec->{lig_id};
	    	my $name    = $shared->trim( $rec->{lig_last_name} ) . ", " . $shared->trim( $rec->{lig_first_name} );
	    	if ( $rec->{lig_last_name} eq "" ) {
	    	    $name   = $rec->{lig_first_name};
	    	}
	    	if ( $rec->{lig_first_name} eq "" ) {
	    	    $name   = $rec->{lig_last_name};
	    	}
	    	my $cur     = $rec->{lig_currency};
	    	my $tRate   = sprintf( "%.4f", $rec->{lil_trans_rate} );
	    	my $eRate   = sprintf( "%.4f", $rec->{lil_edit_rate} );
	    	my $hRate   = sprintf( "%.2f", $rec->{lil_hourly_rate} );
	    	my $eml     = $rec->{lig_email};
	    	my $noUse   = $rec->{lig_dont_use};
	    	
	    	my $dspCur  = "\$";
	    	if ( $cur eq "EUR" ) {
	    	    $dspCur = "&#8364;"
	    	}
	    	
	    	my $dspTR   = "$dspCur $tRate";
	    	if ( $tRate eq "" ) {
	    	    $dspTR  = "N/A";
	    	}
	    	my $dspER   = "$dspCur $eRate";
	    	if ( $eRate eq "" ) {
	    	    $dspER  = "N/A";
	    	}
	    	my $dspHR   = "$dspCur $hRate";
	    	if ( $hRate eq "" ) {
	    	    $dspHR  = "N/A";
	    	}
	    	
	    	if ( $row == 0 ) {
	    		$row_color = $color_bg2;
	    		$row = 1;
	    	} else {
	    		$row_color = $color_bg1;
	    		$row = 0;
	    	}
	    	
	    	if ( $noUse eq "Y" ) {
	    	    $row_color = $err_color;
	    	}
	    	
	    	my $eLink = "N/A";
	    	if ( $eml gt "" ) {
	    	    $eLink = "<a href=\"mailto:$eml\" class=nav>$eml</a>";
	    	}
	    	
	    	$content .= "<tr bgcolor=\"$row_color\">\n";
	    	$content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=edit_lig_form&lig_id=$id" . $sp_vars . "\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"View\" title=\"View $name\"></a></td>\n";
	    	$content .= "<td valign=top align=left class=form>$name</td>\n";
	    	$content .= "<td valign=top align=left class=form>$dspTR</td>\n";
	    	$content .= "<td valign=top align=left class=form>$dspER</td>\n";
	    	$content .= "<td valign=top align=left class=form>$dspHR</td>\n";
	    	$content .= "<td valign=top align=left class=form>$eLink</td>\n";
	    	$content .= "<td valign=top align=right class=form>";
    	    $content .= "<a href=\"$cgi_url?action=del_lig&lig_id=$id" . $sp_vars . "\" onClick=\"return confirm('Permanently remove $name from database?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete $name\"></a>";
	    	$content .= "</td></tr>\n";
	    	
	    	$fndCnt++;
	    }
	    $content .= "</table>\n";
	    		
	    if ( $fndCnt == 0 ) {
	    	$content  = "";
	    	$java_msg = "No Linguists found meeting search criteria.";
	    } else {
	    	$conf->{lig_cnt} = "$fndCnt";
	    }
	
    	$conf->{search_results} = $content;
	
	
    } else {
	
	    $java_msg = "No Search Criteria Specified.";        
	
    }

	&print_main;
	
}

###########################################################                   
# Change sort of result set
#----------------------------------------------------------
sub change_sort {

	my $new_sort = $shared->get_cgi( "new_sort" );
	my $new_flow = "A";
	if ( $new_sort eq $hash_vars->{sort_fld} ) {
		my $cur_flow = $hash_vars->{sort_flw};
		if ( $cur_flow eq "A" ) {
			$new_flow = "D";
		}
	}
	
	$hash_vars->{sort_fld} = $new_sort;
	$hash_vars->{sort_flw} = $new_flow;
	
	if ( ( $hash_vars->{srch_name} eq "" ) && ( $hash_vars->{srch_src} eq "" ) && ( $hash_vars->{srch_tgt} eq "" ) ) {
	    &search_any( "Y" );
	} else {
		&search_any;
	}
	
}

############################################################
# Form to add member to database
############################################################
sub add_lig_form {

  	my $today = $shared->getSQLToday;
    	
   	$conf->{form_head}    = "Add New Linguist";
   	$conf->{form_start}   = "<form method=post name=adm_lig_form action=\"$cgi_url\">\n"
    	                  . "<input type=hidden name=action value=\"add_lig\">\n";
   	$conf->{f_last_name}  = "<input name=lig_last_name value=\"\" size=35 maxlength=40 class=form>";
   	$conf->{f_first_name} = "<input name=lig_first_name value=\"\" size=35 maxlength=40 class=form>";
   	$conf->{f_interpret}  = "<input type=checkbox name=lig_is_interpreter value=\"Y\" class=form>";
   	$conf->{f_login}      = "<input name=lig_login value=\"\" size=40 maxlength=80 class=form>";
   	$conf->{f_passwd}     = "<input name=lig_password value=\"\" size=20 maxlength=20 class=form>";
   	$conf->{f_phone}      = "<input name=lig_phone value=\"\" size=20 maxlength=30 class=form>";
   	$conf->{f_email}      = "<input name=lig_email value=\"\" size=40 maxlength=80 class=form>";
   	$conf->{f_fax}        = "<input name=lig_fax value=\"\" size=20 maxlength=30 class=form>";
   	$conf->{f_address}    = "Address: <input name=lig_address value=\"\" size=40 maxlength=60 class=form><br>\n"
   	                      . "City: <input name=lig_city value=\"\" size=20 maxlength=40 class=form>&nbsp;&nbsp;\n"
   	                      . "State: <input name=lig_state value=\"\" size=3 maxlength=2 class=form>&nbsp;&nbsp;\n"
   	                      . "Zip: <input name=lig_zip value=\"\" size=10 maxlength=20 class=form><br>\n"
   	                      . "Country: <input name=lig_country value=\"\" size=20 maxlength=30 class=form>";
   	$conf->{f_status}     = "<input type=checkbox name=lig_nda value=\"Y\" class=form> NDA&nbsp;&nbsp;\n"
   	                      . "<input type=checkbox name=lig_w9 value=\"Y\" class=form> W9&nbsp;&nbsp;\n"
   	                      . "Currency: <select name=lig_currency class=form><option value=\"USD\">USD</option><option value=\"EUR\">EUR</option><option value=\"GBP\">GBP</option></select>";
   	$conf->{f_pmt_method} = "<input type=checkbox name=lig_pmt_paypal value=\"Y\" class=form> Paypal - E-Mail: "
   	                      . "<input name=lig_pmt_paypal_eml value=\"\" size=30 maxlength=80><br>\n"
   	                      . "<input type=checkbox name=lig_pmt_check value=\"Y\" class=form> Check<br>\n"
   	                      . "<input type=checkbox name=lig_pmt_wire value=\"Y\" class=form> Wire";
   	$conf->{f_work_tool}  = "<input name=lig_work_tool value=\"\" size=20 maxlength=30 class=form>";
   	$conf->{f_does_mtpe}  = "<input type=checkbox name=lig_does_mtpe value=\"Y\" class=form> Does MTPE";
   	 
   	$conf->{f_specialty}  = &getSpecialty( "" );
   	$conf->{f_comments}   = "<textarea name=lig_comment cols=40 rows=7 wrap class=form></textarea>";
   	
   	$conf->{form_submit}  = "<input type=submit value=\"Add Now\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}     = "</form>";
   	$conf->{form_name}    = "adm_lig_form";
   	$conf->{focus_field}  = "lig_first_name";  
    	
    my $pg_body = &disp_form( "lig_add_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

############################################################
# Add the record to the loan database
############################################################
sub add_lig {

   	my $lName     = $shared->get_cgi( "lig_last_name" );
   	my $fName     = $shared->get_cgi( "lig_first_name" );
   	my $interpret = $shared->get_cgi( "lig_is_interpreter" );
   	my $phn       = $shared->get_cgi( "lig_phone" );
   	my $eml       = $shared->get_cgi( "lig_email" );
   	my $fax       = $shared->get_cgi( "lig_fax" );
   	my $addr      = $shared->get_cgi( "lig_address" );
   	my $city      = $shared->get_cgi( "lig_city" );
   	my $state     = $shared->get_cgi( "lig_state" );
   	my $zip       = $shared->get_cgi( "lig_zip" );
   	my $cntry     = $shared->get_cgi( "lig_country" );
   	my $nda       = $shared->get_cgi( "lig_nda" );
   	my $w9        = $shared->get_cgi( "lig_w9" );
   	my $mtpe      = $shared->get_cgi( "lig_does_mtpe" );
   	my $comment   = $shared->get_cgi( "lig_comment" );
   	my $cur       = $shared->get_cgi( "lig_currency" );
   	my $pmtPP     = $shared->get_cgi( "lig_pmt_paypal" );
   	my $pmtCK     = $shared->get_cgi( "lig_pmt_check" );
   	my $pmtWR     = $shared->get_cgi( "lig_pmt_wire" );
   	my $ppEml     = $shared->get_cgi( "lig_pmt_paypal_eml" );
   	my $wkTool    = $shared->get_cgi( "lig_work_tool" );
   	my $lLogin    = $shared->get_cgi( "lig_login" );
   	my $lPass     = $shared->get_cgi( "lig_password" );
   	 
   	if ( $interpret eq "" ) {
   	    $interpret = "N";
   	}
   	
   	if ( $pmtPP eq "" ) {
   	    $pmtPP = "N";
   	}
   	if ( $pmtCK eq "" ) {
   	    $pmtCK = "N";
   	}
   	if ( $pmtWR eq "" ) {
   	    $pmtWR = "N";
   	}
   	if ( $mtpe eq "" ) {
   	    $mtpe = "N";
   	}
   	
   	$lName       =~ s/'/\\'/g;
   	$fName       =~ s/'/\\'/g;
   	$addr        =~ s/'/\\'/g;
   	$city        =~ s/'/\\'/g;
   	$cntry       =~ s/'/\\'/g;
   	$comment     =~ s/'/\\'/g;
   	$wkTool      =~ s/'/\\'/g;
   	
   	my $ligCnt   = $shared->getSQLCount( "SELECT COUNT(*) FROM linguist WHERE lig_last_name = '$lName' AND lig_first_name = '$fName'" );
   	my $loStat   = 0;
   	if ( $lLogin gt "" ) {
       	
       	my $loginCnt = $shared->getSQLCount( "SELECT COUNT(*) FROM linguist WHERE lig_login = '$lLogin'" );
   	    
   	    if ( length( $lPass ) < 6 ) {
   	        $lPass = &gen_pass_strong;
   	    } elsif ( $loginCnt > 0 ) {
   	        $loStat = 2;
   	    }
   	}
   	   
   	
   	if ( ( $ligCnt == 0 ) && ( $loStat == 0 ) ) {
    	
        my $insert   = "INSERT INTO linguist ( lig_last_name, lig_first_name, lig_is_interpreter, lig_phone, lig_email, lig_fax, lig_address,\n"
                     . "lig_city, lig_state, lig_zip, lig_country, lig_nda, lig_w9, lig_does_mtpe, lig_comment, lig_currency,\n"
                     . "lig_pmt_paypal, lig_pmt_paypal_eml, lig_pmt_check, lig_pmt_wire, lig_work_tool,\n"
                     . "lig_login, lig_password )\n"
                     . "VALUES ( '$lName', '$fName', '$interpret', '$phn', '$eml', '$fax', '$addr', '$city', '$state', '$zip',\n"
                     . "'$cntry', '$nda', '$w9', '$mtpe', '$comment', '$cur', '$pmtPP', '$ppEml', '$pmtCK', '$pmtWR', '$wkTool',\n"
                     . "'$lLogin', '$lPass' )";
                     
        $shared->doSQLUpdate( $insert );
    	
  	    my $lid      = $shared->getSQLLastInsertID;
   	
   	    &updateSpecialty( $lid );
   	    
   	    &add_audit( $lid, "New Linquist Record Created: $lName, $fName." );
    	
   	    &edit_lig_form( $lid );
   	} elsif ( $loStat == 1 ) {
   	    $java_msg = "Password must be 6 characters or more";
   	    &search_any( "Y" );
   	} elsif ( $loStat == 2 ) {
   	    $java_msg = "Login ID already being used - select another";
   	    &search_any( "Y" );
   	} else {
   	    $java_msg = "Linguist $fName $lName Already Exists";
   	    &search_any( "Y" );
   	}

}

############################################################
# Form to edit member of database
############################################################
sub edit_lig_form {

    my ( $lig_id ) = @_;
    
    if ( $lig_id eq "" ) {
        $lig_id  = $shared->get_cgi( "lig_id" );
    }
    
    my $rec = $shared->get_rec( "linguist", "lig_id", $lig_id );

   	my $lName     = $rec->{lig_last_name};
   	my $fName     = $rec->{lig_first_name};
   	my $interpret = $rec->{lig_is_interpreter};
   	my $phn       = $rec->{lig_phone};
   	my $eml       = $rec->{lig_email};
   	my $fax       = $rec->{lig_fax};
   	my $addr      = $rec->{lig_address};
   	my $city      = $rec->{lig_city};
   	my $state     = $rec->{lig_state};
   	my $zip       = $rec->{lig_zip};
   	my $cntry     = $rec->{lig_country};
   	my $nda       = $rec->{lig_nda};
   	my $w9        = $rec->{lig_w9};
   	my $mtpe      = $rec->{lig_does_mtpe};
   	my $comment   = $rec->{lig_comment};
   	my $cur       = $rec->{lig_currency};
   	my $pmtPP     = $rec->{lig_pmt_paypal};
   	my $pmtCK     = $rec->{lig_pmt_check};
   	my $pmtWR     = $rec->{lig_pmt_wire};
   	my $ppEml     = $rec->{lig_pmt_paypal_eml};
   	my $wkTool    = $rec->{lig_work_tool};
   	my $noUse     = $rec->{lig_dont_use};
    my $lLogin    = $rec->{lig_login};
    my $lPass     = $rec->{lig_password};
    my $lastLogin = $shared->getDateFmt( $rec->{lig_last_login} );   	
   	$comment     =~ s/\"/&quot;/g;
   	$comment     =~ s/\</&gt;/g;
    
	my $srch_name = $hash_vars->{srch_name};
	my $srch_src  = $hash_vars->{srch_src};
	my $srch_tgt  = $hash_vars->{srch_tgt};
	my $srch_spc  = $hash_vars->{srch_spc};
	my $srch_int  = $hash_vars->{srch_int};
	my $srch_sdu  = $hash_vars->{srch_sdu};
	my $srch_mtpe = $hash_vars->{srch_mtpe};
	
    my $inp_srch     = "";
    my $sp_vars      = "";
    if ( $srch_name gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_name value=\"$srch_name\">\n";
    	$sp_vars  .= "&srch_name=$srch_name";
    }
    if ( $srch_src gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_src value=\"$srch_src\">\n";
    	$sp_vars  .= "&srch_src=$srch_src";
    }
    if ( $srch_tgt gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_tgt value=\"$srch_tgt\">\n";
    	$sp_vars  .= "&srch_tgt=$srch_tgt";
    }
    if ( $srch_spc gt "" ) {
        $inp_srch .= "<input type=hidden name=srch_spc value=\"$srch_spc\">\n";
        $sp_vars  .= "&srch_spc=$srch_spc";
    }
    if ( $srch_int gt "" ) {
        $inp_srch .= "<input type=hidden name=srch_int value=\"$srch_int\">\n";
        $sp_vars  .= "&srch_int=$srch_int";
    }
    if ( $srch_sdu gt "" ) {
        $inp_srch .= "<input type=hidden name=srch_sdu value=\"$srch_sdu\">\n";
        $sp_vars  .= "&srch_sdu=$srch_sdu";
    }
    if ( $srch_mtpe gt "" ) {
        $inp_srch .= "<input type=hidden name=srch_mtpe value=\"$srch_mtpe\">\n";
        $sp_vars  .= "&srch_mtpe=$srch_mtpe";
    }
    if ( $inp_srch eq "" ) {
    	$inp_srch .= "<input type=hidden name=srch_init value=\"Y\">\n";
    	$sp_vars  .= "&srch_init=Y";
    }


    $sp_vars =~ s/^\&//;
    
    $conf->{form_id_row} = "<tr><td align=right valign=top class=label bgcolor=$color_bg2 width=150>Linquist ID</td>\n"
                         . "<td valign=top align=left class=form><b>$lig_id</b></td></tr>\n";

    
   	$conf->{form_head}    = "Update Linguist Details";
   	$conf->{form_start}   = "<form method=post name=lig_edit_form action=\"$cgi_url\">\n"
   	                      . "<input type=hidden name=action value=\"edit_lig\">\n"
   	                      . "<input type=hidden name=lig_id value=\"$lig_id\">\n"
   	                      . $inp_srch;
   	                      
   	                      
   	$conf->{f_last_name}  = "<input name=lig_last_name value=\"$lName\" size=35 maxlength=40 class=form>";
   	$conf->{f_first_name} = "<input name=lig_first_name value=\"$fName\" size=35 maxlength=40 class=form>";
   	my $intChk = "";
   	if ( $interpret eq "Y" ) {
   	    $intChk = " checked";
   	}
   	
   	$conf->{f_login}      = "<input name=lig_login value=\"$lLogin\" size=40 maxlength=80 class=form>";
   	$conf->{f_passwd}     = "<input name=lig_password value=\"\" size=20 maxlength=20 class=form>";
   	$conf->{f_last_login} = $lastLogin;
   	$conf->{f_interpret}  = "<input type=checkbox name=lig_is_interpreter value=\"Y\" class=form" . $intChk . ">";
   	$conf->{f_phone}      = "<input name=lig_phone value=\"$phn\" size=20 maxlength=30 class=form>";
   	$conf->{f_email}      = "<input name=lig_email value=\"$eml\" size=40 maxlength=80 class=form>";
   	$conf->{f_fax}        = "<input name=lig_fax value=\"$fax\" size=20 maxlength=30 class=form>";
   	$conf->{f_address}    = "Address: <input name=lig_address value=\"$addr\" size=40 maxlength=60 class=form><br>\n"
   	                      . "City: <input name=lig_city value=\"$city\" size=20 maxlength=40 class=form>&nbsp;&nbsp;\n"
   	                      . "State: <input name=lig_state value=\"$state\" size=3 maxlength=2 class=form>&nbsp;&nbsp;\n"
   	                      . "Zip: <input name=lig_zip value=\"$zip\" size=10 maxlength=20 class=form><br>\n"
   	                      . "Country: <input name=lig_country value=\"$cntry\" size=20 maxlength=30 class=form>";
   	                      
   	my $ndaChk = "";
   	if ( $nda eq "Y" ) {
   	    $ndaChk = " checked";
   	}
   	my $w9Chk  = "";
   	if ( $w9 eq "Y" ) {
   	    $w9Chk = " checked";
   	}
   	my $mtpeChk = "";
   	if ( $mtpe eq "Y" ) {
   	    $mtpeChk = " checked";
   	}
   	my $nuChk  = "";
   	if ( $noUse eq "Y" ) {
   	    $nuChk = " checked";
   	}
   	my $selCur = "";
   	if ( $cur eq "USD" ) {
   	    $selCur .= "<option value=\"USD\" selected>USD</option>\n";
   	} else {
   	    $selCur .= "<option value=\"USD\">USD</option>\n";
   	}
   	if ( $cur eq "EUR" ) {
   	    $selCur .= "<option value=\"EUR\" selected>EUR</option>\n";
   	} else {
   	    $selCur .= "<option value=\"EUR\">EUR</option>\n";
   	}
   	if ( $cur eq "GBP" ) {
   	    $selCur .= "<option value=\"GBP\" selected>GBP</option>\n";
   	} else {
   	    $selCur .= "<option value=\"GBP\">GBP</option>\n";
   	}

   	$conf->{f_status}     = "<input type=checkbox name=lig_nda value=\"Y\" class=form" . $ndaChk . "> NDA&nbsp;&nbsp;\n"
   	                      . "<input type=checkbox name=lig_w9 value=\"Y\" class=form" . $w9Chk . "> W9&nbsp;&nbsp;\n"
   	                      . "Currency: <select name=lig_currency class=form>" . $selCur . "</select>&nbsp;&nbsp;\n"
   	                      . "<input type=checkbox name=lig_dont_use value=\"Y\" class=form" . $nuChk . "> DO NOT USE\n";
    my $ppChk = "";
    my $ckChk = "";
    my $wrChk = "";
    if ( $pmtPP eq "Y" ) {
        $ppChk = " checked";
    }
    if ( $pmtCK eq "Y" ) {
        $ckChk = " checked";
    }
    if ( $pmtWR eq "Y" ) {
        $wrChk = " checked";
    }
   	$conf->{f_pmt_method} = "<input type=checkbox name=lig_pmt_paypal value=\"Y\" class=form" . $ppChk . "> Paypal - E-Mail: "
   	                      . "<input name=lig_pmt_paypal_eml value=\"$ppEml\" size=30 maxlength=80><br>\n"
   	                      . "<input type=checkbox name=lig_pmt_check value=\"Y\" class=form" . $ckChk . "> Check<br>\n"
   	                      . "<input type=checkbox name=lig_pmt_wire value=\"Y\" class=form" . $wrChk . "> Wire";
   	$conf->{f_work_tool}  = "<input name=lig_work_tool value=\"$wkTool\" size=20 maxlength=30 class=form>";
   	$conf->{f_does_mtpe}  = "<input type=checkbox name=lig_does_mtpe value=\"Y\" class=form" . $mtpeChk . "> Does MTPE";
   	$conf->{f_specialty}  = &getSpecialty( $lig_id );

   	$conf->{f_comments}   = "<textarea name=lig_comment cols=80 rows=7 wrap class=form>$comment</textarea>";
    	
   	$conf->{form_submit}  = "<table border=0 cellpadding=0 cellspacing=0 width=100%><tr><td valign=top align=left class=form>\n"
   	                      . "<input type=submit value=\"Update Linguist\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?$sp_vars'\" class=form></td>"
                          . "<td valign=top align=right class=form>\n"
                          . "<input type=button value=\"Audit Log\" onClick=\"parent.location.href='$cgi_url?action=audit_log&lig_id=$lig_id&$sp_vars'\" class=form></td></tr>\n"
                          . "</table>";
   	$conf->{form_end}     = "</form>";
    	
    my $fndCnt  = 0;
    
    my $selLang = &getLang;
    
    my $content = "";
   	$content .= "<table border=0 cellpadding=2 cellspacing=0 width=1036>\n";
   	
    $content .= "<form method=post name=rate_add_form action=\"$cgi_url\">\n";
    $content .= "<input type=hidden name=action value=\"add_rate\">\n";
    $content .= "<input type=hidden name=lig_id value=\"$lig_id\">\n";
    $content .= $inp_srch;

    $content .= "<tr><td valign=top align=left class=overview colspan=7><font color=\"black\">Language/Rate Information</font></td></tr>\n";
	$content .= "<tr bgcolor=\"black\">\n";
	$content .= "<td valign=top align=left class=form><font color=\"white\"><b>Source Language</b></font></td>\n";
	$content .= "<td valign=top align=left class=form><font color=\"white\"><b>Target Language</b></font></td>\n";
	$content .= "<td valign=top align=right class=form><font color=\"white\"><b>Translate Rate</b></font></td>\n";
	$content .= "<td valign=top align=right class=form><font color=\"white\"><b>Review Rate</b></font></td>\n";
    $content .= "<td valign=top align=right class=form><font color=\"white\"><b>Hourly Rate</b></font></td>\n";
    $content .= "<td valign=top align=right class=form><font color=\"white\"><b>MTPE Rate</b></font></td>\n";
    $content .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
    
  	$content .= "<tr bgcolor=\"$color_bg1\"><td valign=top align=left class=form>\n";
   	$content .= "<select name=lil_src_lng_id class=form>$selLang</select></td>\n";
   	$content .= "<td valign=top align=left class=form>\n";
   	$content .= "<select name=lil_tgt_lng_id class=form>$selLang</select></td>\n";
   	$content .= "<td valign=top align=right class=form>\n";
   	$content .= "<input name=lil_trans_rate value=\"\" size=8 maxlength=8 class=form></td>\n";
   	$content .= "<td valign=top align=right class=form>\n";
   	$content .= "<input name=lil_edit_rate value=\"\" size=8 maxlength=8 class=form></td>\n";
   	$content .= "<td valign=top align=right class=form>\n";
   	$content .= "<input name=lil_hourly_rate value=\"\" size=10 maxlength=10 class=form></td>\n";
   	$content .= "<td valign=top align=right class=form>\n";
   	$content .= "<input name=lil_mtpe_rate value=\"\" size=10 maxlength=10 class=form></td>\n";
   	$content .= "<td valign=top align=right class=form><input type=submit value=\"Add\" class=form></td></tr>\n";

    my $row = 0;
    my $row_color;
    	
	my $SQL = "SELECT lil_id, lil_src_lng_id, lil_tgt_lng_id, lil_trans_rate, lil_edit_rate, lil_hourly_rate, lil_mtpe_rate\n"
	        . "FROM linguist_lang\n"
	        . "WHERE lil_lig_id = $lig_id\n"
	        . "ORDER BY lil_id";
	my @rs  = $shared->getResultSet( $SQL );
	foreach my $rec ( @rs ) {
	 	my $id      = $rec->{lil_id};
	 	my $src     = $rec->{lil_src_lng_id};
	 	my $tgt     = $rec->{lil_tgt_lng_id};
	 	my $tRate   = $rec->{lil_trans_rate};
	 	my $eRate   = $rec->{lil_edit_rate};
	 	my $hRate   = $rec->{lil_hourly_rate};
	 	my $mRate   = $rec->{lil_mtpe_rate};
	    
	    my $srcDesc = &getLangDesc( $src );
	    my $tgtDesc = &getLangDesc( $tgt );
	   	if ( $row == 0 ) {
	   		$row_color = $color_bg2;
	   		$row = 1;
	   	} else {
	   		$row_color = $color_bg1;
	   		$row = 0;
	   	}
	
	   	$content .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form>$srcDesc</td>\n";
	   	$content .= "<td valign=top align=left class=form>$tgtDesc</td>\n";
	   	$content .= "<td valign=top align=right class=form>" . sprintf( "%.4f", $tRate ) . "</td>\n";
	   	$content .= "<td valign=top align=right class=form>" . sprintf( "%.4f", $eRate ) . "</td>\n";
	   	$content .= "<td valign=top align=right class=form>" . sprintf( "%.2f", $hRate ) . "</td>\n";
	   	$content .= "<td valign=top align=right class=form>" . sprintf( "%.4f", $mRate ) . "</td>\n";
   	    $content .= "<td valign=top align=right class=form><a href=\"$cgi_url?action=del_rate&lil_id=$id&lig_id=$lig_id&$sp_vars\" onClick=\"return confirm('Permanently remove rates from database?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete Rates\"></a></tr>\n";
	    	
	  	$fndCnt++;
	}
	
	if ( $fndCnt == 0 ) {
	  	$content  .= "<tr><td valign=top align=left colspan=7 class=form>No Rates found for Linguist</td></tr>\n";
	}
	
   	$content .= "</form>\n";
	
	$content .= "</table>\n";
	
    $conf->{rate_info} = $content;
    	
    my $pg_body = &disp_form( "lig_edit_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

############################################################
# Update the record to the members database
############################################################
sub edit_lig {

  	my $srch_init = $shared->get_cgi( "srch_init" );
  	my $next_act  = $shared->get_cgi( "next_act" );

    my $lig_id    = $shared->get_cgi( "lig_id" );
  	my $lName     = $shared->get_cgi( "lig_last_name" );
   	my $fName     = $shared->get_cgi( "lig_first_name" );
   	my $interpret = $shared->get_cgi( "lig_is_interpreter" );
   	my $phn       = $shared->get_cgi( "lig_phone" );
   	my $eml       = $shared->get_cgi( "lig_email" );
   	my $fax       = $shared->get_cgi( "lig_fax" );
   	my $addr      = $shared->get_cgi( "lig_address" );
   	my $city      = $shared->get_cgi( "lig_city" );
   	my $state     = $shared->get_cgi( "lig_state" );
   	my $zip       = $shared->get_cgi( "lig_zip" );
   	my $cntry     = $shared->get_cgi( "lig_country" );
   	my $nda       = $shared->get_cgi( "lig_nda" );
   	my $w9        = $shared->get_cgi( "lig_w9" );
   	my $mtpe      = $shared->get_cgi( "lig_does_mtpe" );
   	my $comment   = $shared->get_cgi( "lig_comment" );
   	my $cur       = $shared->get_cgi( "lig_currency" );
   	my $pmtPP     = $shared->get_cgi( "lig_pmt_paypal" );
   	my $pmtCK     = $shared->get_cgi( "lig_pmt_check" );
   	my $pmtWR     = $shared->get_cgi( "lig_pmt_wire" );
   	my $ppEml     = $shared->get_cgi( "lig_pmt_paypal_eml" );
   	my $wkTool    = $shared->get_cgi( "lig_work_tool" );
   	my $noUse     = $shared->get_cgi( "lig_dont_use" );
   	my $lLogin    = $shared->get_cgi( "lig_login" );
   	my $lPass     = $shared->get_cgi( "lig_password" );
   	
   	if ( $interpret eq "" ) {
   	    $interpret = "N";
   	}
   	
   	if ( $nda eq "" ) {
   	    $nda = "N";
   	}
   	if ( $w9 eq "" ) {
   	    $w9 = "N";
   	}
   	if ( $mtpe eq "" ) {
   	    $mtpe = "N";
   	}
    if ( $pmtPP eq "" ) {
        $pmtPP = "N";
    }
    if ( $pmtCK eq "" ) {
        $pmtCK = "N";
    }
    if ( $pmtWR eq "" ) {
        $pmtWR = "N";
    }
    if ( $noUse eq "" ) {
        $noUse = "N";
    }   	
    	
   	my $rec       = $shared->get_rec( "linguist", "lig_id", $lig_id );
   	my $p_lName     = $rec->{lig_last_name};
   	my $p_fName     = $rec->{lig_first_name};
   	my $p_interpret = $rec->{lig_is_interpreter};
   	my $p_phn       = $rec->{lig_phone};
   	my $p_eml       = $rec->{lig_email};
   	my $p_fax       = $rec->{lig_fax};
   	my $p_addr      = $rec->{lig_address};
   	my $p_city      = $rec->{lig_city};
   	my $p_state     = $rec->{lig_state};
   	my $p_zip       = $rec->{lig_zip};
   	my $p_cntry     = $rec->{lig_country};
   	my $p_nda       = $rec->{lig_nda};
   	my $p_w9        = $rec->{lig_w9};
   	my $p_mtpe      = $rec->{lig_does_mtpe};
   	my $p_comment   = $rec->{lig_comment};
   	my $p_cur       = $rec->{lig_currency};
   	my $p_pmtPP     = $rec->{lig_pmt_paypal};
   	my $p_pmtCK     = $rec->{lig_pmt_check};
   	my $p_pmtWR     = $rec->{lig_pmt_wire};
   	my $p_ppEml     = $rec->{lig_pmt_paypal_eml};
   	my $p_wkTool    = $rec->{lig_work_tool};
   	my $p_noUse     = $rec->{lig_dont_use};
    my $p_login     = $rec->{lig_login};
       	
   	my $chg_dtls = "";
   	if ( $lName ne $p_lName ) {
   		$chg_dtls .= "Linguist Last Name: from [$p_lName] to [$lName]<br>\n";
   	}
   	if ( $fName ne $p_fName ) {
   		$chg_dtls .= "Linguist First Name: from [$p_fName] to [$fName]<br>\n";
   	}
   	if ( $interpret ne $p_interpret ) {
   	    $chg_dtls .= "Interpreter: from [$p_interpret] to [$interpret]<br>\n";
   	}
   	
   	if ( $lLogin ne $p_login ) {
   	    $chg_dtls .= "Login: from [$p_login] to [$lLogin]<br>\n";
   	}
   	
   	if ( $phn ne $p_phn ) {
   		$chg_dtls .= "Phone: from [$p_phn] to [$phn]<br>\n";
   	}
   	if ( $eml ne $p_eml ) {
   		$chg_dtls .= "E-mail: from [$p_eml] to [$eml]<br>\n";
   	}
   	if ( $fax ne $p_fax ) {
   		$chg_dtls .= "Fax: from [$p_fax] to [$fax]<br>\n";
   	}
   	if ( $addr ne $p_addr ) {
   		$chg_dtls .= "Address: from [$p_addr] to [$addr]<br>\n";
   	}
   	if ( $city ne $p_city ) {
   		$chg_dtls .= "City: from [$p_city] to [$city]<br>\n";
   	}
   	if ( $state ne $p_state ) {
   		$chg_dtls .= "State: from [$p_state] to [$state]<br>\n";
   	}
   	if ( $zip ne $p_zip ) {
   		$chg_dtls .= "Zip: from [$p_zip] to [$zip]<br>\n";
   	}
   	if ( $cntry ne $p_cntry ) {
   		$chg_dtls .= "Country: from [$p_cntry] to [$cntry]<br>\n";
   	}
    if ( $nda ne $p_nda ) {
        $chg_dtls .= "NDA: from [$p_nda] to [$nda]<br>\n";
    }
    if ( $w9 ne $p_w9 ) {
        $chg_dtls .= "W9: from [$p_w9] to [$w9]<br>\n";
    }
    if ( $mtpe ne $p_mtpe ) {
        $chg_dtls .= "Does MTPE: from [$p_mtpe] to [$mtpe]<br>\n";
    }
    if ( $cur ne $p_cur ) {
        $chg_dtls .= "Currency: from [$p_cur] to [$cur]<br>\n";
    }
    if ( $noUse ne $p_noUse ) {
        $chg_dtls .= "DO NOT USE: from [$p_noUse] to [$noUse]<br>\n";
    }
    if ( $pmtPP ne $p_pmtPP ) {
        $chg_dtls .= "Payment Method - Paypal: from [$p_pmtPP] to [$pmtPP]<br>\n";
    }
    if ( $ppEml ne $p_ppEml ) {
        $chg_dtls .= "Paypal E-mail: from [$p_ppEml] to [$ppEml]<br>\n";
    }
    if ( $pmtCK ne $p_pmtCK ) {
        $chg_dtls .= "Payment Method - Check: from [$p_pmtCK] to [$pmtCK]<br>\n";
    }
    if ( $pmtWR ne $p_pmtWR ) {
        $chg_dtls .= "Payment Method - Wire: from [$p_pmtWR] to [$pmtWR]<br>\n";
    }
    if ( $wkTool ne $p_wkTool ) {
        $chg_dtls .= "Work Tool: from [$p_wkTool] to [$wkTool]<br>\n";
    }
    if ( $comment ne $p_comment ) {
        $chg_dtls .= "Comments: from [$p_comment] to [$comment]<br>\n";
    }
    if ( $lPass gt "" ) {
        $chg_dtls .= "Password Changed<br>\n";
    }
    
   	if ( $chg_dtls gt "" ) {
   		chomp( $chg_dtls );
   		$chg_dtls =~ s/<br>$//;
   	}
    	
   	$lName       =~ s/'/\\'/g;
   	$fName       =~ s/'/\\'/g;
   	$addr        =~ s/'/\\'/g;
   	$city        =~ s/'/\\'/g;
   	$cntry       =~ s/'/\\'/g;
   	$comment     =~ s/'/\\'/g;
   	$wkTool      =~ s/'/\\'/g;
   	
   	my $loStat   = 0;
   	if ( $lLogin gt "" ) {
       	
       	my $loginCnt = $shared->getSQLCount( "SELECT COUNT(*) FROM linguist WHERE lig_login = '$lLogin' AND lig_id <> $lig_id" );
   	    
   	    if ( ( $lPass gt "" ) && ( length( $lPass ) < 6 ) ) {
   	        $loStat = 1;
   	    } elsif ( $loginCnt > 0 ) {
   	        $loStat = 2;
   	    }
   	}
   	
   	if ( $loStat == 0 ) {
   	    my $update   = "UPDATE linguist SET lig_last_name = '$lName', lig_first_name = '$fName', lig_is_interpreter = '$interpret', lig_phone = '$phn', lig_email = '$eml',\n"
   	                 . "lig_fax = '$fax', lig_address = '$addr', lig_city = '$city', lig_state = '$state', lig_zip = '$zip',\n"
   	                 . "lig_country = '$cntry', lig_nda = '$nda', lig_w9 = '$w9', lig_does_mtpe = '$mtpe', lig_currency = '$cur', lig_comment = '$comment',\n"
   	                 . "lig_pmt_paypal = '$pmtPP', lig_pmt_check = '$pmtCK', lig_pmt_wire = '$pmtWR', lig_pmt_paypal_eml = '$ppEml',\n"
   	                 . "lig_work_tool = '$wkTool', lig_dont_use = '$noUse', lig_login = '$lLogin'\n"
   	                 . "WHERE lig_id = $lig_id";
       	$shared->doSQLUpdate( $update );
       	
       	if ( $lPass gt "" ) {
       	    my $up2  = "UPDATE linguist SET lig_password = '$lPass' WHERE lig_id = $lig_id";
       	    $shared->doSQLUpdate( $up2 );
       	}

        &updateSpecialty( $lig_id );
        	
   	    if ( $chg_dtls gt "" ) {
   		    &add_audit( $lig_id, "Linguist Record Updated...<br>\n" . $chg_dtls );
   	    }
    	
        $env->{doFade} = "Y";
        $java_msg      = "Linguist Record Updated...";
    } elsif ( $loStat == 1 ) {
        $java_msg      = "Password must be at least 6 characters";
    } elsif ( $loStat == 2 ) {
        $java_msg      = "Login ID must be unique - $lLogin already being used.";
    }
    
    &edit_lig_form( $lig_id );

}

###########################################################                   
# Delete Member from Database
#----------------------------------------------------------
sub del_lig {

   	my $srch_init = $shared->get_cgi( "srch_init" );
   	my $next_act  = $shared->get_cgi( "next_act" );

   	my $lig_id    = $shared->get_cgi( "lig_id" );
   	my $po_cnt    = $shared->getSQLCount( "SELECT COUNT(*) FROM po_log WHERE pol_ling_id = $lig_id" );
   	my $ut_cnt    = $shared->getSQLCount( "SELECT COUNT(*) FROM user WHERE usr_type = 'LIG' AND usr_link_id = $lig_id" );
    	
    if ( $po_cnt == 0 ) {
        if ( $ut_cnt == 0 ) {
   	        my $lig_rec = $shared->get_rec( "linguist", "lig_id", $lig_id );
   	        my $desc    = "Deleting Linguist (" . $lig_rec->{lig_last_name} . ", " . $lig_rec->{lig_first_name} . ")";
   	        &add_audit( $lig_id, $desc );
    	
   	        my $update = "DELETE FROM linguist WHERE lig_id = $lig_id";
   	        $shared->doSQLUpdate( $update );
    	
   	        my $up2    = "DELETE FROM linguist_lang WHERE lil_lig_id = $lig_id";
   	        $shared->doSQLUpdate( $up2 );
   	    } else {
   	        $java_msg = "Unable to Delete Linguist - Remove Linked User First";
   	    }
   	} else {
   	    $java_msg = "Unable to Delete Linguist - Remove Associated POs First";
   	}
    	
    &search_any( $srch_init );
}


###########################################################
# View Audit Log for a specific application
#----------------------------------------------------------
sub audit_log {

    my $lig_id  = $shared->get_cgi( "lig_id" );
    my $lig_rec = $shared->get_rec( "linguist", "lig_id", $lig_id );
    my $name    = $lig_rec->{lig_last_name} . ", " . $lig_rec->{lig_first_name};
    
    my $SQL     = "SELECT usr_name, lia_date, lia_description\n"
                . "FROM linguist_audit\n"
                . "INNER JOIN user ON lia_usr_id = usr_id\n"
                . "WHERE lia_lig_id = $lig_id\n"
                . "ORDER BY lia_date DESC";
                
	my $content;
    $content .= "<span class=subhead>Linguist: $name</span><br><br>\n";
    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
    $content .= "<tr bgcolor=\"black\">\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Log Date</b></font></td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>User</b></font></td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Description</b></font></td></tr>\n";
    
    my @rs    = $shared->getResultSet( $SQL );
    my $row  = 0;
    my $row_color;

	foreach my $rec ( @rs ) {
	   	my $log_date = $shared->getDateFmt( $rec->{lia_date} );
	   	my $uName    = $rec->{usr_name};
	   	my $desc     = $rec->{lia_description};
	    	
    	if ( $row == 0 ) {
    		$row_color = $color_bg2;
    		$row = 1;
    	} else {
    		$row_color = $color_bg1;
    		$row = 0;
    	}
	    	
    	$content .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form>$log_date</td>\n";
	    $content .= "<td valign=top align=left class=form>$uName</td>\n";
	    $content .= "<td valign=top align=left class=form>$desc</td></tr>\n";
	    
    }
    $content .= "</table>\n";

   	$conf->{search_results} = $content;
   	
    my $pg_body = &disp_form( "lig_audit" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
   	
}


###########################################################
# Add Audit Log Record
#----------------------------------------------------------
sub add_audit {
	
	my ( $id, $desc ) = @_;
	
	$desc =~ s/'/\\'/g;
	
	my $insert = "INSERT INTO linguist_audit ( lia_lig_id, lia_usr_id, lia_date, lia_description )\n"
	           . "VALUES ( $id, $usr_id, now(), '$desc' )";
	$shared->doSQLUpdate( $insert );
	
}

############################################################
# Add the rate record
############################################################
sub add_rate {

    my $lig_id = $shared->get_cgi( "lig_id" );
    
   	my $src       = $shared->get_cgi( "lil_src_lng_id" );
   	my $tgt       = $shared->get_cgi( "lil_tgt_lng_id" );
   	my $tRate     = $shared->validNumber( $shared->get_cgi( "lil_trans_rate" ) );
   	my $eRate     = $shared->validNumber( $shared->get_cgi( "lil_edit_rate" ) );
   	my $hRate     = $shared->validNumber( $shared->get_cgi( "lil_hourly_rate" ) );
   	my $mRate     = $shared->validNumber( $shared->get_cgi( "lil_mtpe_rate" ) );
        	
    my $insert   = "INSERT INTO linguist_lang ( lil_lig_id, lil_src_lng_id, lil_tgt_lng_id, lil_trans_rate,\n"
                 . "lil_edit_rate, lil_hourly_rate, lil_mtpe_rate )\n"
                 . "VALUES\n"
                 . "( $lig_id, $src, $tgt, $tRate, $eRate, $hRate, $mRate  )";
    $shared->doSQLUpdate( $insert );
   	
   	my $sCd      = &getLangCode( $src );
   	my $tCd      = &getLangCode( $tgt );
   	
    &add_audit( $lig_id, "Language Set/Rates Added - [$sCd=>$tCd - $tRate/$eRate/$hRate/$mRate]" );
   	        
    &edit_lig_form( $lig_id );

}

############################################################
# Remove the Contact
############################################################
sub del_rate {

    my $lil_id = $shared->get_cgi( "lil_id" );
    
   	my $lig_id  = $shared->get_cgi( "lig_id" );
   	my $lil_rec = $shared->get_rec( "linguist_lang", "li1_id", $lil_id );
   	
   	my $sCd     = &getLangCode( $lil_rec->{lil_src_lng_id} );
   	my $tCd     = &getLangCode( $lil_rec->{lil_tgt_lng_id} );
    	
   	my $desc    = "$sCd=>$tCd - "
   	            . $lil_rec->{lil_trans_rate} . "/" . $lil_rec->{lil_edit_rate} . "/"
   	            . $lil_rec->{lil_hourly_rate} . "/" . $lil_rec->{lil_mtpe_rate};
    	
    my $update    = "DELETE FROM linguist_lang WHERE lil_id = $lil_id";
    $shared->doSQLUpdate( $update );
        
    my $log_desc  = "Language Rates Record Removed - [$desc]";
   	                  
    &add_audit( $lig_id, $log_desc );
    	
    &edit_lig_form( $lig_id );

}

###########################################################                   
# Create specialty checkboxes
#----------------------------------------------------------
sub getSpecialty {
    
    my ( $lig_id ) = @_;
    
    my $SQL = "SELECT spc_id, spc_description, '' AS lis_id\n"
            . "FROM specialty\n"
            . "ORDER BY spc_description";
    if ( $lig_id gt "" ) {
        $SQL = "SELECT spc_id, spc_description, lis_id\n"
             . "FROM specialty\n"
             . "LEFT JOIN linguist_spec ON ( lis_lig_id = $lig_id AND lis_spc_id = spc_id )\n"
             . "ORDER BY spc_description";
    }
    
    my $result = "<table border=0 cellpadding=2 cellspacing=0>\n";
    $result   .= "<tr>\n";
    my $spcCnt = 0;
    my $col    = 0;
    
    my @rs = $shared->getResultSet( $SQL );
    
    foreach my $rec ( @rs ) {
        my $id    = $rec->{spc_id};
        my $desc  = $rec->{spc_description};
        my $mrk   = $rec->{lis_id};
        
        if ( $col == 5 ) {
            $result .= "</tr>\n"
                     . "<tr>\n";
            $col = 0;
        }
        
        my $chkVal = "";
        if ( $mrk gt "" ) {
            $chkVal = " checked";
        }
        my $fldName = "spc-" . $id;
        $result .= "<td valign=top alignleft class=form><input type=checkbox name=\"$fldName\" value=\"Y\" class=form" . $chkVal ."> $desc</td>\n";
        $col++;
        $spcCnt++;
    }
    
    if ( $col == 5 ) {
        $result .= "</tr>\n";
    } elsif ( $col == 4 ) {
        $result .= "<td>&nbsp;</td></tr>\n";
    } elsif ( $col == 3 ) {
        $result .= "<td colspan=2>&nbsp;</td></tr>\n";
    } elsif ( $col == 2 ) {
        $result .= "<td colspan=3>&nbsp;</td></tr>\n";
    } elsif ( $col == 1 ) {
        $result .= "<td colspan=4>&nbsp;</td></tr>\n";
    }
    
    $result .= "</table>\n";
    if ( $spcCnt == 0 ) {
        $result = "";
    }
    
    return $result;
}

###########################################################                   
# Update linguist_spec table from form
#----------------------------------------------------------
sub updateSpecialty {
    
    my ( $lig_id ) = @_;
    
    my $SQL = "SELECT spc_id, spc_description, lis_id\n"
            . "FROM specialty\n"
            . "LEFT JOIN linguist_spec ON ( lis_lig_id = $lig_id AND lis_spc_id = spc_id )\n"
            . "ORDER BY spc_description";
    
    my @rs = $shared->getResultSet( $SQL );
    
    foreach my $rec ( @rs ) {
        my $id    = $rec->{spc_id};
        my $desc  = $rec->{spc_description};
        my $mrk   = $rec->{lis_id};
        
        my $fldName = "spc-" . $id;
        my $fVal    = $shared->get_cgi( $fldName );
        if ( $fVal eq "" ) {
            $fVal = "N";
        }
        
        if ( ( $mrk gt "" ) && ( $fVal eq "N" ) ) {
            $shared->doSQLUpdate( "DELETE FROM linguist_spec WHERE lis_id = $mrk" );
        } elsif ( ( $mrk eq "" ) && ( $fVal eq "Y" ) ) {
            $shared->doSQLUpdate( "INSERT INTO linguist_spec ( lis_lig_id, lis_spc_id ) VALUES ( $lig_id, $id )" );
        }
    }
}




###########################################################                   
# Get Languages as selection list
#----------------------------------------------------------
sub getLang {
    
    my ( $filt ) = @_;
    
    my $SQL = "SELECT * FROM language ORDER BY lng_desc";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{lng_id};
        my $desc     = $rec->{lng_desc};
        $result .= "<option value=\"$code\">$desc</option>";
    }
    
    return $result;
}

###########################################################                   
# Get Specialty selection list
#----------------------------------------------------------
sub getSpecList {
    
    my $SQL = "SELECT * FROM specialty ORDER BY spc_description";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{spc_id};
        my $desc     = $rec->{spc_description};
        $result .= "<option value=\"$code\">$desc</option>";
    }
    
    return $result;
}

###########################################################                   
# Get Linguist Specialty List
#----------------------------------------------------------
sub getLigSpcList {
    
    my ( $lID ) = @_;
    
    my $SQL = "SELECT spc_description FROM linguist_spec\n"
            . "INNER JOIN specialty ON ( spc_id = lis_spc_id )\n"
            . "WHERE lis_lig_id = $lID\n"
            . "ORDER BY spc_description";
    
    my $result = "";
    my $cnt    = 0;
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $desc     = $rec->{spc_description};
        if ( $cnt == 0 ) {
            $result = $desc;
        } else {
            $result .= ", " . $desc;
        }
        $cnt++;
    }
    
    return $result;
}

###########################################################                   
# Get Language Description
#----------------------------------------------------------
sub getLangDesc {
    
    my ( $lID ) = @_;
    
    my $rec = $shared->getResultRec( "SELECT lng_desc FROM language WHERE lng_id = $lID" );
    my $result = $rec->{lng_desc};
    
    return $result;
}
 
###########################################################                   
# Get Language Description
#----------------------------------------------------------
sub getLangCode {
    
    my ( $lID ) = @_;
    
    my $rec = $shared->getResultRec( "SELECT lng_code FROM language WHERE lng_id = $lID" );
    my $result = $rec->{lng_code};
    
    return $result;
}  

############################################################
# Generate Linguist/Lang Pair report Excel Format
############################################################
sub genLinguistRpt {

    my $rptDate  = $shared->getSQLToday;
    
	my $fName    = "$forms_path/sdt_linguist_rpt.xlsx";
	my $urlName  = "$forms_url/sdt_linguist_rpt.xlsx";
	my $nowDesc  = $shared->getDateDesc( $rptDate, 1, 1 );
	
	my $workbook = Excel::Writer::XLSX->new( $fName );
	$workbook->set_tempdir( $temp_path );
	
	my $c_litegrey    = $workbook->set_custom_color( 40, 204, 204, 204 );
	my $c_litegreen   = $workbook->set_custom_color( 41, 210, 255, 214 );
    my %headFont      = ( font  => 'Arial', size  => 14, color => 'black', valign  => 'bottom', bold  => 1 );
    my %subFont       = ( font  => 'Arial', size  => 12, color => 'black', valign  => 'bottom', bold  => 1 );
    my %titleFont     = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom' );
    my %titleFontRt   = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom', align => 'right' );
    my %colFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'bottom', bold => 1, bg_color => $c_litegrey );
    
    my %bodyFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', border => 1, border_color => $c_litegrey );
    my %boldFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, border => 1, border_color => $c_litegrey );
    my %dateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', num_format => 'mm/dd/yy' );
    my %numFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %boldNumFont   = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right' );
    my %currFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %rateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %pctFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
	
	my $fmtHead  = $workbook->add_format( %headFont );
	my $fmtSub   = $workbook->add_format( %subFont );
	my $fmtTitle = $workbook->add_format( %titleFont );
	my $fmtTtlRt = $workbook->add_format( %titleFontRt );
	my $fmtCol   = $workbook->add_format( %colFont );
	my $fmtBody  = $workbook->add_format( %bodyFont );
	my $fmtBold  = $workbook->add_format( %boldFont );
	my $fmtDate  = $workbook->add_format( %dateFont );
	my $fmtNum   = $workbook->add_format( %numFont );
	my $fmtBNum  = $workbook->add_format( %boldNumFont );
	my $fmtCurr  = $workbook->add_format( %currFont );
	my $fmtRate  = $workbook->add_format( %rateFont );
	my $fmtPct   = $workbook->add_format( %pctFont );
	
	$fmtCurr->set_num_format( "\$0.00" );
	$fmtRate->set_num_format( "\$0.0000" );
	$fmtPct->set_num_format( "0.00\%" );
	
	my $ligSQL   = "SELECT * FROM linguist ORDER BY lig_last_name, lig_first_name";
	my $lilSQL   = "SELECT lig_id, lig_last_name, lig_first_name, lil_src_lng_id, lil_tgt_lng_id,\n"
	             . "lil_trans_rate, lil_edit_rate, lil_hourly_rate\n"
	             . "FROM linguist_lang\n"
	             . "INNER JOIN linguist ON lil_lig_id = lig_id\n"
	             . "ORDER BY lig_last_name, lig_first_name, lil_src_lng_id";

    my @ligRS    = $shared->getResultSet( $ligSQL );
    my @lilRS    = $shared->getResultSet( $lilSQL );
    	             
    my $sheet    = $workbook->add_worksheet( "Linguists" );
            
    $sheet->write_string( 0, 0, "SDT Linguists List", $fmtHead );
    $sheet->write_string( 0, 22, "As of: $nowDesc", $fmtTtlRt );
        
    my @colHead  = ( "Linguist ID", "Last Name", "First Name", "Interpreter", "Status", "Phone", "E-mail", "Fax", "Address", "City", "State", "Zip", "Country", "NDA", "W9", "Currency", "Pmt Paypal", "Paypal E-Mail", "Pmt Check", "Pmt Wire", "Work Tool", "Does MTPE", "Specialties", "Comments" );
    $sheet->write_row( 2, 0, \@colHead, $fmtCol );
        
    my $row = 3;
	foreach my $rec ( @ligRS ) {

        my $lID    = $rec->{lig_id};
        my $fName  = $rec->{lig_first_name};
        my $lName  = $rec->{lig_last_name};
        my $interp = $rec->{lig_is_interpreter};
        my $phn    = $rec->{lig_phone};
        my $eml    = $rec->{lig_email};
        my $fax    = $rec->{lig_fax};
        my $addr   = $rec->{lig_address};
        my $city   = $rec->{lig_city};
        my $state  = $rec->{lig_state};
        my $zip    = $rec->{lig_zip};
        my $cntry  = $rec->{lig_country};
        my $comm   = $rec->{lig_comment};
        my $nda    = $rec->{lig_nda};
        my $w9     = $rec->{lig_w9};
        my $curr   = $rec->{lig_currency};
        my $pPPL   = $rec->{lig_pmt_paypal};
        my $pplEml = $rec->{lig_pmt_paypal_eml};
        my $pChk   = $rec->{lig_pmt_check};
        my $pWire  = $rec->{lig_pmt_wire};
        my $wkTool = $rec->{lig_work_tool};
        my $mtpe   = $rec->{lig_does_mtpe};
        my $spcLst = &getLigSpcList( $lID );
        my $noUse  = $rec->{lig_dont_use};
        
        my $dspInt = "No";
        if ( $interp eq "Y" ) {
            $dspInt = "Yes";
        }
        
        my $dspNDA = "No";
        if ( $nda eq "Y" ) {
            $dspNDA = "Yes";
        }
        
        my $dspW9  = "No";
        if ( $w9 eq "Y" ) {
            $dspW9 = "Yes";
        }
        
        my $dspPPL = "No";
        if ( $pPPL eq "Y" ) {
            $dspPPL = "Yes";
        }
        
        my $dspChk = "No";
        if ( $pChk eq "Y" ) {
            $dspChk = "Yes";
        }
        
        my $dspWire = "No";
        if ( $pWire eq "Y" ) {
            $dspWire = "Yes";
        }
        
        my $dspStat  = "OK";
        if ( $noUse eq "Y" ) {
            $dspStat = "Do Not Use";
        }
        
        my $dspMTPE = "No";
        if ( $mtpe eq "Y" ) {
            $dspMTPE = "Yes";
        }
                	        
        $sheet->write_number( $row, 0, $lID, $fmtNum );
        $sheet->write_string( $row, 1, $lName, $fmtBold );
        $sheet->write_string( $row, 2, $fName, $fmtBold );
        $sheet->write_string( $row, 3, $dspInt, $fmtBody );
        $sheet->write_string( $row, 4, $dspStat, $fmtBody );
        $sheet->write_string( $row, 5, $phn, $fmtBody );
        $sheet->write_string( $row, 6, $eml, $fmtBody );
        $sheet->write_string( $row, 7, $fax, $fmtBody );
        $sheet->write_string( $row, 8, $addr, $fmtBody );
        $sheet->write_string( $row, 9, $city, $fmtBody );
        $sheet->write_string( $row, 10, $state, $fmtBody );
        $sheet->write_string( $row, 11, $zip, $fmtBody );
        $sheet->write_string( $row, 12, $cntry, $fmtBody );
        $sheet->write_string( $row, 13, $dspNDA, $fmtBody );
        $sheet->write_string( $row, 14, $dspW9, $fmtBody );
        $sheet->write_string( $row, 15, $curr, $fmtBody );
        $sheet->write_string( $row, 16, $dspPPL, $fmtBody );
        $sheet->write_string( $row, 17, $pplEml, $fmtBody );
        $sheet->write_string( $row, 18, $dspChk, $fmtBody );
        $sheet->write_string( $row, 19, $dspWire, $fmtBody );
        $sheet->write_string( $row, 20, $wkTool, $fmtBody );
        $sheet->write_string( $row, 21, $dspMTPE, $fmtBody );
        $sheet->write_string( $row, 22, $spcLst, $fmtBody );
        $sheet->write_string( $row, 23, $comm, $fmtBody );

        $row++;
    }

    $sheet->set_column( 0, 0, 11 );
    $sheet->set_column( 1, 2, 25 );
    $sheet->set_column( 3, 3, 14 );
    $sheet->set_column( 4, 4, 8 );
    $sheet->set_column( 5, 5, 25 );
    $sheet->set_column( 6, 6, 38 );
    $sheet->set_column( 7, 7, 21 );
    $sheet->set_column( 8, 8, 46 );
    $sheet->set_column( 9, 9, 15 );
    $sheet->set_column( 10, 11, 8 );
    $sheet->set_column( 12, 12, 18 );
    $sheet->set_column( 13, 14, 8 );
    $sheet->set_column( 15, 15, 11 );
    $sheet->set_column( 16, 16, 13 );
    $sheet->set_column( 17, 17, 27 );
    $sheet->set_column( 18, 19, 11 );
    $sheet->set_column( 20, 20, 18 );
    $sheet->set_column( 21, 21, 12 );
    $sheet->set_column( 22, 22, 80 );
    $sheet->set_column( 23, 23, 150 );

    $sheet->autofilter( 2, 0, $row, 21 );
    $sheet->hide_gridlines( 2 );
    $sheet->freeze_panes( 3, 0 );


            
    my $sheet2    = $workbook->add_worksheet( "Language Rates" );
    
    $sheet2->write_string( 0, 0, "SDT Linguist Languate Rates", $fmtHead );
    $sheet2->write_string( 0, 6, "As of: $nowDesc", $fmtTtlRt );
        
    my @colHead  = ( "Linguist ID", "Linguist Name", "Source Language", "Target Language", "Trans Rate", "Review Rate", "Hourly Rate" );
    $sheet2->write_row( 2, 0, \@colHead, $fmtCol );
	$row = 3;
	foreach my $rec ( @lilRS ) {
	
	    my $lID    = $rec->{lig_id};
	    my $lName  = $rec->{lig_last_name} . ", " . $rec->{lig_first_name};
	    my $srcID  = $rec->{lil_src_lng_id};
	    my $tgtID  = $rec->{lil_tgt_lng_id};
	    my $tRate  = $rec->{lil_trans_rate};
	    my $eRate  = $rec->{lil_edit_rate};
	    my $hRate  = $rec->{lil_hourly_rate};

        my $dspSrc = $shared->getFieldVal( "language", "lng_desc", "lng_id", $srcID );
        my $dspTgt = $shared->getFieldVal( "language", "lng_desc", "lng_id", $tgtID );
        
        $sheet2->write_number( $row, 0, $lID, $fmtNum );
        $sheet2->write_string( $row, 1, $lName, $fmtBold );
        $sheet2->write_string( $row, 2, $dspSrc, $fmtBody );
        $sheet2->write_string( $row, 3, $dspTgt, $fmtBody );
        $sheet2->write_number( $row, 4, $tRate, $fmtRate );
        $sheet2->write_number( $row, 5, $eRate, $fmtRate );
        $sheet2->write_number( $row, 6, $hRate, $fmtCurr );

        $row++;
    }

    $sheet2->set_column( 0, 0, 11 );
    $sheet2->set_column( 1, 1, 34 );
    $sheet2->set_column( 2, 3, 18 );
    $sheet2->set_column( 4, 6, 12 );

    $sheet2->autofilter( 2, 0, $row, 6 );
    $sheet2->hide_gridlines( 2 );
    $sheet2->freeze_panes( 3, 0 );

    $workbook->close();
    
    system( "chmod 644 $fName" );
    
    return $urlName;

}


############################################################
# Form to import UNDA file
############################################################
sub unda_form {

   	$conf->{form_head}    = "Process UNDA File";
   	$conf->{form_start}   = "<form method=post name=adm_lig_form action=\"$cgi_url\" enctype=\"multipart/form-data\">\n"
    	                  . "<input type=hidden name=action value=\"lig_unda_import\">\n";
   	$conf->{f_unda_file}  = "<input type=file name=unda_file class=form size=40>";
   	
   	$conf->{form_submit}  = "<input type=submit value=\"Import Now\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}     = "</form>";
   	$conf->{form_name}    = "lig_unda_form";
    	
    my $pg_body = &disp_form( "lig_unda_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

###########################################################                   
# Import UNDA File
#----------------------------------------------------------
sub unda_import {

    my $file     = "$imp_path/last_unda.txt";
	my $error    = $shared->uploadFile( "unda_file", $file, "txt" );
	if ( $error == 0 ) {
		
	    system( "/usr/bin/dos2unix $file" );
		
		open( FILE, "$file" );
		my @FILE = <FILE>;
		close( FILE );
		
		my $row = 0;
		foreach my $line ( @FILE ) {
		    
		    if ( $row > 0 ) {
		        chomp( $line );
		    
		        my @flds = split( "\t", $line );
		    
		        my $eml  = $flds[3];
		        
		        my $SQL  = "SELECT lig_id, lig_nda FROM linguist WHERE lig_email = '$eml'";
		        my @rs   = $shared->getResultSet( $SQL );
		        foreach my $rec ( @rs ) {
		            my $lID = $rec->{lig_id};
		            my $NDA = $rec->{lig_nda};
		            
		            if ( $NDA eq "N" ) {
		                my $up1 = "UPDATE linguist SET lig_nda = 'Y' WHERE lig_id = $lID";
		                my $up2 = "INSERT INTO linguist_audit ( lia_lig_id, lia_usr_id,\n"
		                        . "lia_date, lia_description ) VALUES ( $lID, $usr_id,\n"
		                        . "now(), 'Marked NDA from file import' )";
		                $shared->doSQLUpdate( $up1 );
		                $shared->doSQLUpdate( $up2 );
		            }
		        }
		    }
		    
	        $row++;
		}
		
		$java_msg = "File Processed Successfully";
		
	} else {
		$java_msg = "Import File not specified!";
    }

    &search_any( "Y" );
    
}

###########################################################                   
# Generate Strong Password
#----------------------------------------------------------
sub gen_pass_strong {
    
    my @part1 = ( "Ejs1", "wDo4p", "xQ3L", "n8S", "vT9L", "hSH0", "Zr5f", "meCp7", "bTYk", "dkW6" );
    my @part2 = ( "\@", "\$", "*", ";", "^", "!", "(", ")", "-", "+" );
    my @part3 = ( "W7Da", "bSt5", "Mr8w", "Q2Sf", "Et5w", "L7jq", "N2N5", "Ir6c", "enP3", "N9ox" );
 
    my $r1    = int( rand( 10 ) );
    my $r2    = int( rand( 10 ) );
    my $r3    = int( rand( 10 ) );
   
    my $newPass  = $part1[ $r1 ] . $part2[ $r2 ] . $part3[ $r3 ];
   
    return $newPass;
}
