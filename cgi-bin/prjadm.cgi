#!/usr/bin/perl
###########################################################################
# prjadm.cgi                Client Administration
###########################################################################
# Copyright 2019, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;
use Excel::Writer::XLSX;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/prjadm.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($po_url)      = "$script_path/poadm.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($temp_path)   = $config->get_temp_path;
my ($forms_path)  = $config->get_forms_path;
my ($forms_url)   = $config->get_forms_url;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $exceptList    = "";
my $dupList       = "";
my $debug         = $config->get_debug;
my $doExpBtn      = 0;
my $doInvChk      = 0;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;


# Main program
my $usr_id = 0;
my $action = $shared->get_cgi( "action" );
my $hash_vars;

$hash_vars->{srch_cln}     = $shared->get_cgi( "srch_cln" );
$hash_vars->{srch_prjn}    = $shared->get_cgi( "srch_prjn" );
$hash_vars->{srch_stat}    = $shared->get_cgi( "srch_stat" );
$hash_vars->{sort_fld}     = $shared->get_cgi( "sort_fld" );
$hash_vars->{sort_flw}     = $shared->get_cgi( "sort_flw" );


#
# Main Program
#
 
if ( $auth->validate_access( "prjadm" ) ) {
	
	$usr_id    = $auth->getCurrentUser;

    if ( $action eq "search_any" ) {
        &search_any;
    } elsif ( $action eq "change_sort" ) {
    	&change_sort;
    } elsif ( $action eq "change_sort" ) {
    	&change_sort;
    } elsif ( $action eq "edit_prj_form" ) {
        my $prj_id = $shared->get_cgi( "prj_id" );
        &edit_prj_form( $prj_id );
    } elsif ( $action eq "edit_prj" ) {
        &edit_prj;
    } elsif ( $action eq "add_prj_form" ) {
        &add_prj_form;
    } elsif ( $action eq "add_prj_next" ) {
        &add_prj_next;
    } elsif ( $action eq "add_prj" ) {
        &add_prj;
    } elsif ( $action eq "add_qprj" ) {
        &add_qprj;
    } elsif ( $action eq "add_prj_quote" ) {
        &add_prj_quote;
    } elsif ( $action eq "del_prj" ) {
        &del_prj;
    } elsif ( $action eq "prj_mark_inv_all" ) {
        &prj_mark_inv_all;
        $hash_vars->{srch_stat} = "IP";
        &search_any;
    } elsif ( $action eq "audit_log" ) {
    	&audit_log;
    } elsif ( $action eq "comm_rpt_form" ) {
        &comm_rpt_form;
    } elsif ( $action eq "comm_rpt" ) {
        my $excelURL = &genCommRpt;
        if ( $excelURL gt "" ) {
            $env->{open_file} = $excelURL;
        }
        &search_any( "Y" );
    } elsif ( $action eq "profit_rpt_form" ) {
        &profit_rpt_form;
    } elsif ( $action eq "profit_rpt" ) {
        my $excelURL = &genProfitRpt;
        if ( $excelURL gt "" ) {
            $env->{open_file} = $excelURL;
        }
        &search_any( "Y" );        
    } elsif ( $action eq "prj_exp_list" ) {
        my $excelURL = &genExpList;
        if ( $excelURL gt "" ) {
            $env->{open_file} = $excelURL;
        }
        $hash_vars->{srch_stat} = "IP";
        &search_any;	
    } else {
        &search_any( "Y" );
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my $prj_cnt = $shared->getSQLCount( "SELECT count(*) FROM project" );

    if ( $conf->{prj_cnt} eq "" ) {
        $conf->{prj_cnt}     = $prj_cnt;
    }

   	my $abContent = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=add_prj_form>\n"
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Add New Project\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";
                  
    my $expBtn    = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=prj_exp_list>\n"
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Export Pend Invoice\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";
                  
    if ( $doExpBtn ) {
        $conf->{export_xls} = $expBtn;
    }

 	$conf->{add_button}  = $abContent;
 	$conf->{selCln}      = &getClient;
    
    my $pg_body              = &disp_form( "prj_main" );

    my $js = "<script type=\"text/javascript\">\n";
    $js   .= "function copyClip( cpname, clcname, pno ){\n";
    $js   .= "  clcname.replace(/'/g, \"\\\'\");\n";
    $js   .= "  var part1   = 'Client Project \"';\n";
    $js   .= "  var part2   = part1.concat( cpname );\n";
    $js   .= "  var part3   = '\" \| Client Contact - ';\n";
    $js   .= "  var part4   = part2.concat( part3 );\n";
    $js   .= "  var part5   = part4.concat( clcname );\n";
    $js   .= "  var part6   = ' \| Project #';\n";
    $js   .= "  var part7   = part5.concat( part6 );\n";
    $js   .= "  var invStr  = part7.concat( pno );\n";
    
    $js   .= "  var e1      = document.createElement('textarea');\n";
    $js   .= "  e1.value    = invStr;\n";
    $js   .= "  e1.setAttribute('readonly', '');\n";
    $js   .= "  e1.style.position = 'absolute';\n";
    $js   .= "  e1.style.left = '-9999px';\n";
    $js   .= "  document.body.appendChild(e1);\n";
    $js   .= "  e1.select();\n";
    $js   .= "  document.execCommand('copy');\n";
    $js   .= "  document.body.removeChild(e1);\n";
    $js   .= "};\n";
    if ( $doInvChk ) {
        $js   .= "ichk = false;\n";
        $js   .= "function checkAll ( frm1 ) {\n";
        $js   .= "    var aa = document.getElementById( 'frm1' );\n"; 
        $js   .= "    if ( ichk == false ) {\n";
        $js   .= "        ichk = true\n";
        $js   .= "    } else {\n";
        $js   .= "        ichk = false\n";
        $js   .= "    }\n";
        $js   .= "    for ( var i = 0; i < aa.elements.length; i++ ) {\n"; 
        $js   .= "        aa.elements[i].checked = ichk;\n";
        $js   .= "    }\n";
        $js   .= "}\n";
    }    
    $js   .= "</script>\n";

    $env->{javascript}       = $js;
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my ( $init_srch ) = @_;
	
	my $content_head;
	my $content_body;
	
	my $srch_cln     = $hash_vars->{srch_cln};
	my $srch_prjn    = $hash_vars->{srch_prjn};
	my $srch_stat    = $hash_vars->{srch_stat};
	my $srch_head    = "<b>Searched:</b>&nbsp;";
	my $sort_fld     = $hash_vars->{sort_fld};
	my $sort_flw     = $hash_vars->{sort_flw};
	my $srch_par     = 0;
	my $sp_vars      = "";
    my $pStat        = "";
    my $color_high   = "#FFC621";
    
	if ( $sort_fld eq "" ) {
		$sort_fld = "due";
	}
	if ( $sort_flw eq "" ) {
		$sort_flw = "A";
	}
	
	if ( ( $srch_cln gt "" ) || ( $srch_prjn gt "" ) || ( $srch_stat gt "" ) || ( $init_srch eq "Y" ) ) {
	
	    my $SQL = "SELECT prj_id, prj_no, prj_pm, cln_name, prj_due_date, prj_due_time, prj_owner,\n"
	            . "prj_assigned_to, prj_further_action,\n"
	            . "CASE\n"
	            . "  WHEN prj_rev_usd_amt > 0 THEN prj_rev_usd_amt\n"
	            . "  WHEN prj_rev_eur_amt > 0 THEN prj_rev_eur_amt\n"
	            . "  ELSE 0\n"
	            . "END rev_amt,\n"
	            . "CASE\n"
	            . "  WHEN prj_rev_usd_amt > 0 THEN 'USD'\n"
	            . "  WHEN prj_rev_eur_amt > 0 THEN 'EUR'\n"
	            . "  ELSE 'N/A'\n"
	            . "END rev_cur,\n"
	            . "prj_client_proj_no, clc_contact, prj_delivery_date, prj_delivered_to_client\n"
	            . "FROM project\n"
	            . "INNER JOIN client ON cln_id = prj_cln_id\n"
	            . "LEFT JOIN client_contact ON clc_id = prj_clc_id\n"
	            . "WHERE 1\n";

	    if ( $srch_cln gt "" ) {
	    	my $dsp_client = $shared->getFieldVal( "client", "cln_name", "cln_id", $srch_cln );
		    $SQL .= "AND prj_cln_id = $srch_cln\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Client is <b>$dsp_client</b>";
		    } else {
		        $srch_head .= "Client is <b>$dsp_client</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_cln=$srch_cln";
	    }
	    if ( $srch_prjn gt "" ) {
		    $SQL .= "AND prj_no LIKE '%$srch_prjn%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Project Number contains <b>$srch_prjn</b>";
		    } else {
		        $srch_head .= "Project Number contains <b>$srch_prjn</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_prjn=$srch_prjn";
	    }
	    if ( $srch_stat gt "" ) {
	        my $srch_done;
	        my $head_done;
	        if ( $srch_stat eq "DP" ) {
	            $srch_done = "AND prj_delivery_date IS NULL OR prj_delivery_date = '0000-00-00'\n";
	            $head_done = "Projects Pending Delivery";
	        } elsif ( $srch_stat eq "IP" ) {
	            $srch_done = "AND prj_delivery_date IS NOT NULL AND prj_delivery_date <> '0000-00-00' AND prj_invoiced = 'N'\n";
	            $head_done = "Projects Delivered - Pending Invoicing";
	            $doExpBtn  = 1;
	            $doInvChk  = 1;
	        } elsif ( $srch_stat eq "DI" ) {
	            $srch_done = "AND prj_delivery_date IS NOT NULL AND prj_delivery_date <> '0000-00-00' AND prj_invoiced = 'Y'\n";
	            $head_done = "Projects Delivered and Invoiced";
	        }
	    	$SQL .= $srch_done;
	    	if ( $srch_par > 0 ) {
	    		$srch_head .= ", $head_done";
	    	} else {
	    		$srch_head .= $head_done;
	    	}
	    	$srch_par++;
	    	$sp_vars       .= "&srch_stat=$srch_stat";
	    }
	    if ( ( $init_srch eq "Y" ) && ( $srch_par == 0 ) ) {
	        $SQL .= "AND prj_delivery_date IS NULL OR prj_delivery_date = '0000-00-00'\n";
	    }

        my $doFlow = 1;
	    if ( $sort_fld eq "pno" ) {
	    	$SQL     .= "ORDER BY prj_no";
	    	$sp_vars .= "&sort_fld=pno";
	    } elsif ( $sort_fld eq "due" ) {
	        if ( $sort_flw eq "D" ) {
	            $SQL .= "ORDER BY prj_due_date DESC, prj_due_time";
	        } else {
	            $SQL .= "ORDER BY prj_due_date, prj_due_time";
	        }
	        $sp_vars .= "&sort_fld=due"; 
	    } elsif ( $sort_fld eq "pm" ) {
	        $SQL     .= "ORDER BY prj_pm";
	        $sp_vars .= "&sort_fld=pm";
	    } elsif ( $sort_fld eq "cln" ) {
	        $SQL     .= "ORDER BY cln_name";
	        $sp_vars .= "&sort_fld=cln";
	    } elsif ( $sort_fld eq "own" ) {
	        $SQL     .= "ORDER BY prj_owner";
	        $sp_vars .= "&sort_fld=own";
	    } elsif ( $sort_fld eq "rev" ) {
	        $SQL     .= "ORDER BY\n"
        	         . "CASE\n"
	                 . "  WHEN prj_rev_usd_amt > 0 THEN prj_rev_usd_amt\n"
	                 . "  WHEN prj_rev_eur_amt > 0 THEN prj_rev_eur_amt\n"
	                 . "  ELSE 0\n"
	                 . "END";
	        $sp_vars .= "&sort_fld=rev";
	    } elsif ( $sort_fld eq "ast" ) {
	        $SQL     .= "ORDER BY prj_assigned_to";
	        $sp_vars .= "&sort_fld=ast";
	    } else {
	    	$SQL     .= "ORDER BY prj_id";
	    	$sp_vars .= "&sort_fld=id";
        }
	    if ( $doFlow ) {
	        if ( $sort_flw eq "D" ) {
	    	    $SQL     .= " DESC";
	    	    $sp_vars .= "&sort_flw=D";
	        } else {
	    	    $sp_vars .= "&sort_flw=A";
	        }
	    }
	    
	    my $doReset = 0;
	    if ( ( $init_srch eq "Y" ) && ( $srch_par == 0 ) ) {
	        
	    	$srch_head = "All Projects Pending Delivery";
	    	$sp_vars  .= "&srch_init=Y";
	    } else {
	        $doReset = 1;
	    }
	
	    my @rs   = $shared->getResultSet( $SQL );
	    my $row  = 0;
	    my $row_color;
	    my $fndCnt = 0;
	    
	    $content_head .= "<span class=subhead><b>$srch_head</b></span><br><br>\n";
	    if ( ( $doReset ) || ( $doInvChk ) ) {
	        if ( $doInvChk ) {
	            $content_head .= "<form id=\"frm1\" method=post action=\"$cgi_url\">\n";
                $content_head .= "<input type=hidden name=action value=\"prj_mark_inv_all\">\n";
            }
	        $content_head .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	        $content_head .= "<tr>\n";
	        if ( $doReset ) {
	    	    $content_head .= "<td valign=top align=left width=50%><a href=\"$cgi_url\" class=nav><b>[ Clear Filter ]</b></a></td>\n";
	    	} else {
	    	    $content_head .= "<td valign=top align=left width=50%>&nbsp;</td>\n";
	    	}
	    	if ( $doInvChk ) {
	    	    $content_head .= "<td valign=top align=right width=50% class=form><b><input type=checkbox name=checkall style='background: #3f0' onclick='checkAll( frm1 )'> Toggle Checked</b>&nbsp;&nbsp;";
	    	    $content_head .= "<input type=submit value=\"Marked Invoiced\" class=form></td>\n";
	    	} else {
	    	    $content_head .= "<td valign=top align=right width=50% class=form>&nbsp;</td>\n";
	    	}
	    	$content_head .= "</tr>\n";
	    	$content_head .= "</table>\n";
	    }
	    $content_head .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	    $content_head .= "<tr bgcolor=\"black\">\n";
	    $content_head .= "<td valign=top align=left class=form>&nbsp;</td>\n";
	    if ( $doInvChk ) {
	        $content_head .= "<td valign=top align=left class=form><font color=\"white\"><b>Invoiced</b></td>\n";
	    }
	    $content_head .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=due" . $sp_vars . "\"><font color=\"white\"><b>Due Date/Time</b></font></a></td>\n";
	    $content_head .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=cln" . $sp_vars . "\"><font color=\"white\"><b>Client Name</b></font></a></td>\n";
	    $content_head .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=own" . $sp_vars . "\"><font color=\"white\"><b>Owner</b></font></a></td>\n";
	    $content_head .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=pm" . $sp_vars . "\"><font color=\"white\"><b>Manager</b></font></a></td>\n";
	    $content_head .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=pno" . $sp_vars . "\"><font color=\"white\"><b>Proj No</b></font></a></td>\n";
	    $content_head .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=ast" . $sp_vars . "\"><font color=\"white\"><b>Assigned To</b></font></a></td>\n";
	    $content_head .= "<td valign=top align=right class=form><a href=\"$cgi_url?action=change_sort&new_sort=rev" . $sp_vars . "\"><font color=\"white\"><b>Revenue</b></font></a></td>\n";
	    $content_head .= "<td valign=top align=left class=form colspan=2>&nbsp;</td></tr>\n";
	    foreach my $rec ( @rs ) {
	    	my $id      = $rec->{prj_id};
	    	my $prjno   = $rec->{prj_no};
	    	my $pm      = $rec->{prj_pm};
	    	my $name    = $rec->{cln_name};
	    	my $asgnTo  = $rec->{prj_assigned_to};
	    	my $fAct    = $rec->{prj_further_action};
	    	my $own     = $rec->{prj_owner};
	    	my $revAmt  = $shared->fmtNumber( $rec->{rev_amt} );
	    	my $revCur  = $rec->{rev_cur};
            my $delvDt  = $shared->getDateFmt( $rec->{prj_delivery_date} );
            my $clnPNO  = $rec->{prj_client_proj_no};
            my $clcName = $rec->{clc_contact};
            my $dtcStat = $rec->{prj_delivered_to_client};
            my $dueDt   = $shared->getDateFmt( $rec->{prj_due_date} );
            my $dueTm   = $rec->{prj_due_time};
            
            my $dispDue = $dueDt . " " . $dueTm;

            my $clipBtn     = "&nbsp;";
            if ( $delvDt ne "N/A" ) {
                $clcName   =~ s/'/\\'/g;
                $clipBtn    = "<button onclick=\"copyClip(  '$clnPNO', '$clcName', '$prjno' )\" class=form>Copy Clip</button>";
            }            
	    	
	    	if ( $row == 0 ) {
	    	    if ( $fAct eq "Y" ) {
	    	        $row_color = $color_high;
	    	    } else {
	    		    $row_color = $color_bg2;
	    		}
	    		$row = 1;
	    	} else {
	    	    if ( $fAct eq "Y" ) {
	    	        $row_color = $color_high;
	    	    } else {
	    		    $row_color = $color_bg1;
	    		}
	    		$row = 0;
	    	}
	    	
	    	$content_body .= "<tr bgcolor=\"$row_color\">\n";
	    	$content_body .= "<td valign=center align=left class=form rowspan=2><a href=\"$cgi_url?action=edit_prj_form&prj_id=$id" . $sp_vars . "\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"View\" title=\"View $prjno\"></a></td>\n";
	    	if ( $doInvChk ) {
	    	    $content_body .= "<td valign=center align=center class=form rowspan=2><input type=checkbox name=mrk_" . $id . " value=\"Y\"></td>";
	    	}
	    	$content_body .= "<td valign=top align=left class=form>$dispDue</td>\n";
	    	$content_body .= "<td valign=top align=left class=form><b>$name</b></td>\n";
	    	$content_body .= "<td valign=top align=left class=form>$own</td>\n";
	    	$content_body .= "<td valign=top align=left class=form>$pm</td>\n";
	    	$content_body .= "<td valign=top align=left class=form>$prjno</td>\n";
	    	$content_body .= "<td valign=top align=left class=form>$asgnTo</td>\n";
	    	$content_body .= "<td valign=top align=right class=form>$revAmt ($revCur)</td>\n";
	    	$content_body .= "<td valign=center align=right class=form rowspan=2>$clipBtn</td>\n";
	    	
	    	$content_body .= "<td valign=center align=right class=form rowspan=2>";
    	    $content_body .= "<a href=\"$cgi_url?action=del_prj&prj_id=$id" . $sp_vars . "\" onClick=\"return confirm('Permanently remove $prjno from database?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete $prjno\"></a>";
	    	$content_body .= "</td></tr>\n";
	    	my $colspn = 9;
	    	if ( $doInvChk ) {
	    	    $colspn = 10;
	    	}
	    	$content_body .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form colspan=" . $colspn . "><b>Status:</b> $dtcStat</td></tr>\n";
	    	
	    	$fndCnt++;
	    }
	    $content_body .= "</table>\n";
	    
	    if ( $doInvChk ) {
	        $content_body .= "</form>\n";
	    }
	    		
	    my $content   = $content_head . "\n" . $content_body;
	    
	    if ( $fndCnt == 0 ) {
	    	$content  = $content_head;
	    	$content .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left class=form>&nbsp;</td>\n";
	    	$content .= "<td valign=top align=left class=form colspan=8><i>No Projects Found...</i></td></tr>\n";
	    	$content .= "</table>\n";
	    } else {
	    	$conf->{prj_cnt} = "$fndCnt";
	    }
	
    	$conf->{search_results} = $content;
	
	
    } else {
	
	    $java_msg = "No Search Criteria Specified.";        
	
    }

	&print_main;
	
}

###########################################################                   
# Change sort of result set
#----------------------------------------------------------
sub change_sort {

	my $new_sort = $shared->get_cgi( "new_sort" );
	my $new_flow = "A";
	if ( $new_sort eq $hash_vars->{sort_fld} ) {
		my $cur_flow = $hash_vars->{sort_flw};
		if ( $cur_flow eq "A" ) {
			$new_flow = "D";
		}
	}
	
	$hash_vars->{sort_fld} = $new_sort;
	$hash_vars->{sort_flw} = $new_flow;
	
    if ( ( $hash_vars->{srch_prjn} eq "" ) && ( $hash_vars->{srch_cln} eq "" ) && ( $hash_vars->{srch_stat} eq "" ) ) {
	    &search_any( "Y" );
	} else {
		&search_any;
	}
}


############################################################
# Form to add project to database
############################################################
sub add_prj_form {

   	$conf->{form_head}    = "New Project";
   	$conf->{form_start}   = "<form method=post name=adm_prj_form action=\"$cgi_url\">\n"
    	                  . "<input type=hidden name=action value=\"add_prj_next\">\n";
    $conf->{f_client}     = "<select name=prj_cln_id class=form>" . &getClient . "</select>";
       	
   	$conf->{form_submit}  = "<input type=submit value=\"Next\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}     = "</form>";
    	
    my $pg_body = &disp_form( "prj_add_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

##########################################################                   
# Get client selection list
#----------------------------------------------------------
sub getClient {
    
    my $SQL = "SELECT cln_id, cln_name FROM client\n"
            . "WHERE ( cln_type = 'B' OR ( cln_type = 'P' AND cln_personal_usr_id = $usr_id ) )\n"
            . "AND cln_active = 'Y'\n"
            . "AND cln_other = 'N'\n"
            . "ORDER BY cln_name";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{cln_id};
        my $desc     = $rec->{cln_name};
        $result .= "<option value=\"$code\">$desc</option>";
    }
    
    return $result;
}

############################################################
# Form to add project to database (second step)
############################################################
sub add_prj_next {

    my $cln_id   = $shared->get_cgi( "prj_cln_id" );

    my $cln_rec  = $shared->get_rec( "client", "cln_id", $cln_id );
    my $cln_name = $cln_rec->{cln_name};

    my $projNo              = &getNextProjNo( $cln_id );
    
    my $f_clc_id            = $shared->get_cgi( "prj_clc_id" );
    my $f_due_date          = $shared->get_cgi( "prj_due_date" );
    my $f_due_time          = $shared->get_cgi( "prj_due_time" );
    my $f_clnPNO            = $shared->get_cgi( "prj_client_proj_no" );
    my $f_revUSD            = $shared->get_cgi( "prj_rev_usd_amt" );
    my $f_revEUR            = $shared->get_cgi( "prj_rev_eur_amt" );
          
    $conf->{form_head}      = "New Project (continued)";
   	$conf->{form_start}     = "<form method=post name=adm_prj_form action=\"$cgi_url\">\n"
    	                    . "<input type=hidden name=action value=\"add_prj\">\n"
    	                    . "<input type=hidden name=prj_cln_id value=\"$cln_id\">\n";
    
    $conf->{f_client}       = $cln_name;	                  
    $conf->{f_proj_no}      = "<b>" . $projNo . "</b>";
    $conf->{f_contact}      = "<select name=prj_clc_id class=form>" . &getClientContact( $cln_id, $f_clc_id ) . "</select>";
   	$conf->{f_due_date}     = $shared->getJSCalInput( "prj_due_date", $f_due_date, "jscal_trigger1" ) . "<span class=\"formred\">*</span>&nbsp;&nbsp;Time: <input name=prj_due_time value=\"$f_due_time\" size=8 maxlength=10 class=form><span class=\"formred\">*</span>";
   	$conf->{f_cln_proj_no}  = "<input id=cpn name=prj_client_proj_no value=\"$f_clnPNO\" size=60 maxlength=80 class=form><span class=\"formred\">*</span>";
   	$conf->{f_rev_usd_amt}  = "<input name=prj_rev_usd_amt value=\"$f_revUSD\" size=10 maxlength=16 class=formright><span class=\"formred\">**</span>";
   	$conf->{f_rev_eur_amt}  = "<input name=prj_rev_eur_amt value=\"$f_revEUR\" size=10 maxlength=16 class=formright><span class=\"formred\">**</span>";

      	                  
   	$conf->{form_submit}  = "<input type=submit value=\"Add Now\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}     = "</form>";
    	
    my $pg_body = &disp_form( "prj_next_form" );

    my $js      = $shared->getJSCalHead;
    
    $env->{javascript}       = $js;
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );

}

#########################################################                   
# Get client contact selection list
#----------------------------------------------------------
sub getClientContact {
    
    my ( $cln_id, $clc_id ) = @_;
    
    my $SQL = "SELECT clc_id, clc_contact, clc_email FROM client_contact WHERE clc_cln_id = $cln_id ORDER BY clc_contact";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{clc_id};
        my $desc     = $rec->{clc_contact} . " (" . $rec->{clc_email} . ")";
        if ( $code eq $clc_id ) {
            $result .= "<option value=\"$code\" selected>$desc</option>";
        } else {
            $result .= "<option value=\"$code\">$desc</option>";
        }
    }
    
    return $result;
}


#########################################################                   
# Get next project number for client
#----------------------------------------------------------
sub getNextProjNo {
    
    my ( $cln_id ) = @_;
    
    my @pi  = ( "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T" );
    
    my $cCd = $shared->getFieldVal( "client", "cln_code", "cln_id", $cln_id );
    my $pno = $shared->getSQLToday;
    $pno   =~ s/\-//g;
    $pno    = substr( $pno, 2 );
    
    my $result = "";
    
    foreach my $ind ( @pi ) {
        my $chkNo = $cCd . "-" . $pno . $ind;
        if ( $shared->getSQLCount( "SELECT COUNT(*) FROM project WHERE prj_no = '$chkNo'" ) == 0 ) {
            $result = $chkNo;
            last;
        }
    }
    
    return $result;
    
}
        
############################################################
# Add the record to the project database
############################################################
sub add_prj {

    my $cln_id    = $shared->get_cgi( "prj_cln_id" );
    my $clnCd     = $shared->get_cgi( "prj_cln_code" );
    my $clc_id    = $shared->get_cgi( "prj_clc_id" );
   	my $dueDt     = $shared->getSQLDate( $shared->get_cgi( "prj_due_date" ) );
   	my $dueTm     = $shared->get_cgi( "prj_due_time" );
   	my $clnPNO    = $shared->get_cgi( "prj_client_proj_no" );
   	my $revUSD    = ( $shared->get_cgi( "prj_rev_usd_amt" ) ) + 0;
   	my $revEUR    = ( $shared->get_cgi( "prj_rev_eur_amt" ) ) + 0;

    my $prjNo   = &getNextProjNo( $cln_id );
 
    my $err       = "";
    if ( $dueDt eq "0000-00-00" ) {
        $err = "Due Date required.";
    } elsif ( $dueTm eq "" ) {
        $err = "Due Time required.";
    } elsif ( $clnPNO eq "" ) {
        $err = "Client Project Number required.";
    } elsif ( ( $revUSD == 0 ) && ( $revEUR == 0 ) ) {
        $err = "Project Revenue in USD or EUR required.";
    }
    
    if ( $err eq "" ) {
        $clnPNO   =~ s/'/\\'/g;
        
        my $insert = "INSERT INTO project ( prj_cln_id, prj_no, prj_clc_id, prj_due_date, prj_due_time, prj_client_proj_no, prj_rev_usd_amt, prj_rev_eur_amt )\n"
                   . "VALUES ( $cln_id, '$prjNo', $clc_id, '$dueDt', '$dueTm', '$clnPNO', $revUSD, $revEUR )";
        $shared->doSQLUpdate( $insert );
    
        my $prj_id = $shared->getSQLLastInsertID;
    
        &edit_prj_form( $prj_id );
    } else {
        $java_msg = $err;
        &add_prj_next;
    }
        
}

############################################################
# Add project from quote
############################################################
sub add_prj_quote {

    my $qte_id  = $shared->get_cgi( "qte_id" );
    
    my $qte_rec = $shared->get_rec( "quote", "qte_id", $qte_id );
    my $qNo     = $qte_rec->{qte_prj_no};
    my $cln_id  = $qte_rec->{qte_cln_id};
    my $clc_id  = $qte_rec->{qte_clc_id};
    my $qAmt    = $qte_rec->{qte_tot_amount} + 0;
    
	my $SQL     = "SELECT qtl_id, qtl_type, qtl_desc, qtl_doc_name, qtl_qty, qtl_rate\n"
	            . "FROM quote_line\n"
	            . "WHERE qtl_qte_id = $qte_id\n"
	            . "ORDER BY qtl_type, qtl_id";
	my @rs  = $shared->getResultSet( $SQL );
	
	my $f_desc = "";
	my $f_wc   = 0;
	my $f_rt   = 0;
	
	foreach my $rec ( @rs ) {
	 	my $id      = $rec->{qtl_id};
	 	$f_desc     = $rec->{qtl_doc_name};
	 	$f_wc       = $rec->{qtl_qty};
	 	$f_rt       = $rec->{qtl_rate};
	 	last;
    }
    
    my $prjNo   = &getNextProjNo( $cln_id );
    my $cln_name = $shared->getFieldVal( "client", "cln_name", "cln_id", $cln_id );
    my $clc_name = $shared->getFieldVal( "client_contact", "clc_contact", "clc_id", $clc_id );         

    my $f_due_date          = $shared->get_cgi( "prj_due_date" );
    my $f_due_time          = $shared->get_cgi( "prj_due_time" );
    my $f_clnPNO            = $f_desc;
    my $f_revUSD            = $shared->get_cgi( "prj_rev_usd_amt" );
    my $f_revEUR            = $shared->get_cgi( "prj_rev_eur_amt" );
    
    $conf->{form_head}      = "New Project from Quote";
   	$conf->{form_start}     = "<form method=post name=adm_prj_form action=\"$cgi_url\">\n"
    	                    . "<input type=hidden name=action value=\"add_qprj\">\n"
    	                    . "<input type=hidden name=prj_cln_id value=\"$cln_id\">\n"
    	                    . "<input type=hidden name=qte_id value=\"$qte_id\">\n";
    	                    
    
    $conf->{f_client}       = $cln_name;	                  
    $conf->{f_proj_no}      = "<b>" . $prjNo . "</b>";
    $conf->{f_contact}      = $clc_name;
   	$conf->{f_due_date}     = $shared->getJSCalInput( "prj_due_date", $f_due_date, "jscal_trigger1" ) . "<span class=\"formred\">*</span>&nbsp;&nbsp;Time: <input name=prj_due_time value=\"$f_due_time\" size=8 maxlength=10 class=form><span class=\"formred\">*</span>";
   	$conf->{f_cln_proj_no}  = "<input id=cpn name=prj_client_proj_no value=\"$f_clnPNO\" size=80 class=form><span class=\"formred\">*</span>";
   	$conf->{f_rev_usd_amt}  = "<input name=prj_rev_usd_amt value=\"$qAmt\" size=10 maxlength=16 class=formright><span class=\"formred\">**</span>";
   	$conf->{f_rev_eur_amt}  = "<input name=prj_rev_eur_amt value=\"$f_revEUR\" size=10 maxlength=16 class=formright><span class=\"formred\">**</span>";
   	
   	$conf->{f_quote_extra}  = "<tr><td align=right valign=top class=label bgcolor=\"$color_bg2\">Word Count</td>\n"
                            . "<td valign=top align=left class=form><input name=prj_word_cnt value=\"$f_wc\" size=20 maxlength=30 class=form></td></tr>\n"
                            . "<tr><td align=right valign=top class=label bgcolor=\"$color_bg2\">Rate</td>\n"
                            . "<td valign=top align=left class=form><input name=prj_rate value=\"$f_rt\" size=20 maxlength=30 class=form></td></tr>\n";
      	                  
   	$conf->{form_submit}  = "<input type=submit value=\"Add Now\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}     = "</form>";
    	
    my $pg_body = &disp_form( "prj_next_form" );

    my $js      = $shared->getJSCalHead;
    
    $env->{javascript}       = $js;
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
}

       
############################################################
# Add the record to the project database
############################################################
sub add_qprj {

    my $qte_id  = $shared->get_cgi( "qte_id" );
    
    my $qte_rec = $shared->get_rec( "quote", "qte_id", $qte_id );
    my $qNo     = $qte_rec->{qte_prj_no};
    my $cln_id  = $qte_rec->{qte_cln_id};
    my $clc_id  = $qte_rec->{qte_clc_id};
    my $qAmt    = $qte_rec->{qte_tot_amount} + 0;
    
    my $prjNo   = &getNextProjNo( $cln_id );

    my $notes  = "see quote #" . $qNo;

   	my $dueDt     = $shared->getSQLDate( $shared->get_cgi( "prj_due_date" ) );
   	my $dueTm     = $shared->get_cgi( "prj_due_time" );
   	my $clnPNO    = $shared->get_cgi( "prj_client_proj_no" );
   	my $wrdCnt    = $shared->get_cgi( "prj_word_cnt" );
   	my $pRate     = $shared->get_cgi( "prj_rate" );
   	my $revUSD    = ( $shared->get_cgi( "prj_rev_usd_amt" ) ) + 0;
   	my $revEUR    = ( $shared->get_cgi( "prj_rev_eur_amt" ) ) + 0;
 
    my $err       = "";
    if ( $dueDt eq "0000-00-00" ) {
        $err = "Due Date required.";
    } elsif ( $dueTm eq "" ) {
        $err = "Due Time required.";
    } elsif ( $clnPNO eq "" ) {
        $err = "Client Project Number required.";
    } elsif ( ( $revUSD == 0 ) && ( $revEUR == 0 ) ) {
        $err = "Project Revenue in USD or EUR required.";
    }
    
    if ( $err eq "" ) {
        $clnPNO   =~ s/'/\\'/g;
        my $insert = "INSERT INTO project ( prj_cln_id, prj_qte_id, prj_no, prj_clc_id, prj_due_date, prj_due_time, prj_client_proj_no, prj_word_cnt, prj_rate, prj_rev_usd_amt,\n"
                   . "prj_rev_eur_amt, prj_notes ) VALUES ( $cln_id, $qte_id, '$prjNo', $clc_id, '$dueDt', '$dueTm', '$clnPNO', '$wrdCnt', '$pRate', $revUSD, $revEUR, '$notes' )";
        $shared->doSQLUpdate( $insert );
    
        my $prj_id = $shared->getSQLLastInsertID;
    
        &edit_prj_form( $prj_id );
    } else {
        $java_msg = $err;
        &add_prj_quote;
    }
        
}


############################################################
# Form to edit project
############################################################
sub edit_prj_form {

    my ( $prj_id ) = @_;
    
    my $prjAdm  = $auth->validate_access( "prjapprove" );
    
    my $prj_rec = $shared->get_rec( "project", "prj_id", $prj_id );
    my $cln_id  = $prj_rec->{prj_cln_id};
    my $clc_id  = $prj_rec->{prj_clc_id};
    my $cln_rec = $shared->get_rec( "client", "cln_id", $cln_id );
    my $clc_rec = $shared->get_rec( "client_contact", "clc_id", $clc_id );
    
    my $clnName    = $cln_rec->{cln_name};
    my $clnType    = $cln_rec->{cln_type};
    my $clnPUID    = $cln_rec->{cln_personal_usr_id};
    my $clcName    = $clc_rec->{clc_contact};
    my $clcEmail   = $clc_rec->{clc_email};
    my $prjNo      = $prj_rec->{prj_no};
    my $owner      = $prj_rec->{prj_owner};
    my $pm         = $prj_rec->{prj_pm};
    my $subject    = $prj_rec->{prj_subject};
    my $wordCnt    = $prj_rec->{prj_word_cnt};
    my $rate       = $prj_rec->{prj_rate};
    my $dueDt      = $shared->getDateFmt( $prj_rec->{prj_due_date} );
    my $dueTm      = $prj_rec->{prj_due_time};
    my $assgnTo    = $prj_rec->{prj_assigned_to};
    my $furthAct   = $prj_rec->{prj_further_action};
    my $clnPNO     = $prj_rec->{prj_client_proj_no};
    my $revUSDAmt  = $prj_rec->{prj_rev_usd_amt};
    my $revEURAmt  = $prj_rec->{prj_rev_eur_amt};
    my $delvComm   = $prj_rec->{prj_delivered_to_client};
    my $delvDt     = $shared->getDateFmt( $prj_rec->{prj_delivery_date} );
    my $invoiced   = $prj_rec->{prj_invoiced};
    my $notes      = $prj_rec->{prj_notes};
    
    my $faChk      = "";
    if ( $furthAct eq "Y" ) {
        $faChk     = " checked";
    }
    
   	$delvComm     =~ s/\"/&quot;/g;
   	$delvComm     =~ s/\</&gt;/g;
   	$notes        =~ s/\"/&quot;/g;
   	$notes        =~ s/\</&gt;/g;
    
	my $srch_cln     = $hash_vars->{srch_cln};
	my $srch_prjn    = $hash_vars->{srch_prjn};
	my $srch_stat    = $hash_vars->{srch_stat};
	
    my $inp_srch     = "";
    my $inp_hid      = "";
    my $sp_vars      = "";
    if ( $srch_cln gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_cln value=\"$srch_cln\">\n";
    	$sp_vars  .= "&srch_cln=$srch_cln";
    }
    if ( $srch_prjn gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_prjn value=\"$srch_prjn\">\n";
    	$sp_vars  .= "&srch_prjn=$srch_prjn";
    }
    if ( $srch_stat gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_stat value=\"$srch_stat\">\n";
    	$sp_vars  .= "&srch_stat=$srch_stat";
    }
    if ( $inp_srch eq "" ) {
    	$inp_srch .= "<input type=hidden name=srch_init value=\"Y\">\n";
    	$sp_vars  .= "&srch_init=Y";
    }

    $sp_vars =~ s/^\&//;
    
    $conf->{form_id_row}          = "<tr><td align=right valign=top class=label bgcolor=$color_bg2 width=150>Project ID</td>\n"
                                  . "<td valign=center align=left class=form><b>$prj_id</b></td></tr>\n";
    
   	$conf->{form_head}            = "Update Project Details";

    $conf->{f_proj_no}            = $prjNo;
    $conf->{f_client_name}        = $clnName;
    $conf->{f_contact_name}       = $clcName;
    $conf->{f_contact_email}      = $clcEmail;
    
    if ( ( $prjAdm ) || ( $owner eq "" ) ) {
   	    $conf->{f_owner}          = "<select name=prj_owner class=form><option value=\"\"></option>" . $shared->pop_lookup_select( "PRJOWN", $owner ) . "</select>";
   	} else {
   	    $conf->{f_owner}          = $owner;
   	    $inp_hid                 .= "<input type=hidden name=prj_owner value=\"$owner\">\n";
   	}
   	if ( ( $prjAdm ) || ( $pm eq "" ) ) {
      	$conf->{f_pm}             = "<select name=prj_pm class=form><option value=\"\"></option>" . $shared->pop_lookup_select( "PRJMGR", $pm ) . "</select>";
    } else {
        $conf->{f_pm}             = $pm;
        $inp_hid                 .= "<input type=hidden name=prj_pm value=\"$pm\">\n";
    }
   	$conf->{f_subject}            = "<select name=prj_subject class=form><option value=\"\"></option>" . $shared->pop_lookup_select( "PRJSUBJ", $subject ) . "</select>";
   	$conf->{f_word_cnt}           = "<input name=prj_word_cnt value=\"$wordCnt\" size=20 maxlength=30 class=form>";
   	$conf->{f_rate}               = "<input name=prj_rate value=\"$rate\" size=20 maxlength=30 class=form>";
   	$conf->{f_due_date}           = $shared->getJSCalInput( "prj_due_date", $dueDt, "jscal_trigger1" ) . "&nbsp;&nbsp;Time: <input name=prj_due_time value=\"$dueTm\" size=8 maxlength=10 class=form>";
   	$conf->{f_assigned_to}        = "<input name=prj_assigned_to value=\"$assgnTo\" class=form>";
   	$conf->{f_cln_proj_no}        = "<input id=cpn name=prj_client_proj_no value=\"$clnPNO\" size=60 maxlength=80 class=form>";
   	$conf->{f_rev_usd_amt}        = "<input name=prj_rev_usd_amt value=\"$revUSDAmt\" size=10 maxlength=16 class=formright>";
   	$conf->{f_rev_eur_amt}        = "<input name=prj_rev_eur_amt value=\"$revEURAmt\" size=10 maxlength=16 class=formright>";
   	$conf->{f_delv_to_client}     = "<textarea name=prj_delivered_to_client cols=60 rows=2 wrap class=form>$delvComm</textarea>";
    if ( $delvDt eq "N/A" ) {
   	    $conf->{f_delv_date}      = $shared->getJSCalInput( "prj_delivery_date", $delvDt, "jscal_trigger2" );
       	$conf->{f_further_action} = "<input type=checkbox name=prj_further_action value=\"Y\" class=form" . $faChk . "> Further Action Needed";
   	} else {
   	    if ( $prjAdm ) {
    	    $conf->{f_delv_date}      = $shared->getJSCalInput( "prj_delivery_date", $delvDt, "jscal_trigger2" );
    	} else {
    	    $conf->{f_delv_date}  = $delvDt;
    	}
    }

   	$conf->{form_start}           = "<form method=post name=prj_edit_form action=\"$cgi_url\">\n"
   	                              . "<input type=hidden name=action value=\"edit_prj\">\n"
   	                              . "<input type=hidden name=prj_id value=\"$prj_id\">\n"
   	                              . $inp_hid
   	                              . $inp_srch;
    
    if ( $delvDt ne "N/A" ) {
        my $chkInv = "";
        if ( $invoiced eq "Y" ) {
            $chkInv = " checked";
        }
        $conf->{f_invoiced}           = "<input type=checkbox name=prj_invoiced value=\"Y\" class=form" . $chkInv . "> Invoiced&nbsp;&nbsp;"
                                      . "<button onclick=\"genCopyIS()\" class=form>Gen Copy Inv String</button>";        
   	}
   	        
   	$conf->{f_notes}              = "<textarea name=prj_notes cols=60 rows=5 wrap class=form>$notes</textarea>";
    
    my $po_text = "";
    if ( $delvDt eq "N/A" ) {
        if ( ( $clnType eq "B" ) || ( $clnType eq "P" && $clnPUID == $usr_id ) ) {
            $po_text = "&nbsp;&nbsp;&nbsp;<input type=button value=\"Add New Purchase Order\" onClick=\"parent.location.href='$po_url?action=add_po_form&cln_id=$cln_id&prj_no=$prjNo'\" class=form>";
        }
    }
    
   	$conf->{form_submit}  = "<table border=0 cellpadding=0 cellspacing=0 width=100%><tr><td valign=top align=left class=form>\n"
   	                      . "<input type=submit value=\"Update Project\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?$sp_vars'\" class=form>\n" 
                          . $po_text . "</td>"
                          . "<td valign=top align=right class=form>\n"
                          . "<input type=button value=\"Audit Log\" onClick=\"parent.location.href='$cgi_url?action=audit_log&prj_id=$prj_id&$sp_vars'\" class=form></td></tr>\n"
                          . "</table>";
   	$conf->{form_end}     = "</form>";
   	
   	my $content_head = "<table border=0 cellpadding=2 cellspacing=0 width=836>\n";
    $content_head   .= "<tr><td valign=top align=left class=overview colspan=9><font color=\"black\">Project Purchase Orders</font></td></tr>\n";
	$content_head   .= "<tr bgcolor=\"black\">\n";
	$content_head   .= "<td valign=top align=left class=form><font color=\"white\"><b>PO Number</b></font></td>\n";
	$content_head   .= "<td valign=top align=left class=form><font color=\"white\"><b>Created</b></font></td>\n";
	$content_head   .= "<td valign=top align=left class=form><font color=\"white\"><b>Linguist</b></font></td>\n";
	$content_head   .= "<td valign=top align=left class=form><font color=\"white\"><b>Type</b></font></td>\n";
	$content_head   .= "<td valign=top align=right class=form><font color=\"white\"><b>USD Amt</b></font></td>\n";
	$content_head   .= "<td valign=top align=right class=form><font color=\"white\"><b>GBP Amt</b></font></td>\n";
	$content_head   .= "<td valign=top align=right class=form><font color=\"white\"><b>EUR Amt</b></font></td></tr>\n";

    my $row = 0;
    my $row_color;
    my $fndCnt       = 0;
    my $content_body = "";
    my $totUSD       = 0;
    my $totEUR       = 0;
    my $totGBP       = 0;
    
    my $SQL     = "SELECT pol_id, pol_pono, lig_first_name, lig_last_name, pol_do_trans, pol_do_edit, pol_do_proof, pol_do_mtpe, pol_add_date, pol_total_amt, pol_currency\n"
	            . "FROM po_log\n"
	            . "INNER JOIN linguist ON pol_ling_id = lig_id\n"
	            . "WHERE pol_proj_no = '$prjNo'\n"
	            . "ORDER BY pol_add_date";

	my @rs  = $shared->getResultSet( $SQL );
	foreach my $rec ( @rs ) {
	    my $pid     = $rec->{pol_id};
	    my $pono    = $rec->{pol_pono};
	    my $addDt   = $shared->getDateFmt( $rec->{pol_add_date} );
	    my $lName   = $shared->trim( $rec->{lig_last_name} ) . ", " . $shared->trim( $rec->{lig_first_name} );
	    if ( $rec->{lig_last_name} eq "" ) {
	        $lName  = $rec->{lig_first_name};
	    }
	    if ( $rec->{lig_first_name} eq "" ) {
	        $lName  = $rec->{lig_last_name};
	    }
	    my $doTR    = $rec->{pol_do_trans};
	    my $doED    = $rec->{pol_do_edit};
	    my $doPF    = $rec->{pol_do_proof};
	    my $doMT    = $rec->{pol_do_mtpe};
	    my $totAmt  = $rec->{pol_total_amt};
	    my $curr    = $rec->{pol_currency};
	    my $poUSD   = 0;
	    my $poGBP   = 0;
	    my $poEUR   = 0;
	    if ( $curr eq "USD" ) {
	        $poUSD = $totAmt;
	        $totUSD += $totAmt;
	    } elsif ( $curr eq "GBP" ) {
	        $poGBP = $totAmt;
	        $totGBP += $totAmt;
	    } elsif ( $curr eq "EUR" ) {
	        $poEUR = $totAmt;
	        $totEUR += $totAmt;
	    }
	    my $dspUSD  = $shared->fmtNumber( $poUSD );
	    my $dspGBP  = $shared->fmtNumber( $poGBP );
	    my $dspEUR  = $shared->fmtNumber( $poEUR );
	    	
	    my $pType   = "";
	    if ( $doTR eq "Y" ) {
	        $pType .= "TR ";
	    }
	    if ( $doED eq "Y" ) {
	        $pType .= "RE ";
	    }
	    if ( $doPF eq "Y" ) {
	        $pType .= "PF ";
	    }
	    if ( $doMT eq "Y" ) {
	        $pType .= "MTPE ";
	    }
	    $pType =~ s/[ \t]+$//;
	    	
	    	
	    if ( $row == 0 ) {
	    	$row_color = $color_bg2;
	    	$row = 1;
	    } else {
	    	$row_color = $color_bg1;
	    	$row = 0;
	    }
	    	
	    $content_body .= "<tr bgcolor=\"$row_color\">\n";
	    $content_body .= "<td valign=top align=left class=form>$pono</td>\n";
	    $content_body .= "<td valign=top align=left class=form>$addDt</td>\n";
	    $content_body .= "<td valign=top align=left class=form>$lName</td>\n";
	    $content_body .= "<td valign=top align=left class=form>$pType</td>\n";
	    $content_body .= "<td valign=top align=right class=form>$dspUSD</td>\n";
	    $content_body .= "<td valign=top align=right class=form>$dspGBP</td>\n";
	    $content_body .= "<td valign=top align=right class=form>$dspEUR</td></tr>\n";
	    	
	  	$fndCnt++;
	}
	
	if ( $fndCnt == 0 ) {
	    $content_body   = "<tr><td valign=top align=left colspan=7 class=form><i>No Purchase Orders Found for Project</i></td></tr>\n";
	} else {
	    my $dspTotUSD   = $shared->fmtNumber( $totUSD );
	    my $dspTotGBP   = $shared->fmtNumber( $totGBP );
	    my $dspTotEUR   = $shared->fmtNumber( $totEUR );
	    $content_body  .= "<tr><td valign=top align=left colspan=4 class=form><b>TOTALS</b></td>\n";
	    $content_body  .= "<td valign=top align=right class=form><b>$dspTotUSD</b></td>\n";
	    $content_body  .= "<td valign=top align=right class=form><b>$dspTotGBP</b></td>\n";
	    $content_body  .= "<td valign=top align=right class=form><b>$dspTotEUR</b></td></tr>\n";
	}
	
    $content_body      .= "</table>\n";
	
    $conf->{po_info}    = $content_head . "\n" . $content_body;
    
    $clcName =~ s/'/\\'/g;
    
    my $js = "<script type=\"text/javascript\">\n";
    $js   .= "function genCopyIS(){\n";
    $js   .= "  var cpnText = document.getElementById( \"cpn\" ).value;\n";
    $js   .= "  var part1   = 'Client Project \"';\n";
    $js   .= "  var part2   = part1.concat( cpnText );\n";
    $js   .= "  var part3   = '\" \| Client Contact - $clcName \| Project #$prjNo';\n";
    $js   .= "  var invStr  = part2.concat( part3 );\n";
    $js   .= "  var e1      = document.createElement('textarea');\n";
    $js   .= "  e1.value    = invStr;\n";
    $js   .= "  e1.setAttribute('readonly', '');\n";
    $js   .= "  e1.style.position = 'absolute';\n";
    $js   .= "  e1.style.left = '-9999px';\n";
    $js   .= "  document.body.appendChild(e1);\n";
    $js   .= "  e1.select();\n";
    $js   .= "  document.execCommand('copy');\n";
    $js   .= "  document.body.removeChild(e1);\n";
    $js   .= "};\n";
    $js   .= "</script>\n";
    $js   .= $shared->getJSCalHead;
    
    my $pg_body = &disp_form( "prj_edit_form" );

    $env->{javascript}       = $js;
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}


############################################################
# Update the record to the project database
############################################################
sub edit_prj {

  	my $srch_init = $shared->get_cgi( "srch_init" );

    my $prj_id    = $shared->get_cgi( "prj_id" );
   	my $owner     = $shared->get_cgi( "prj_owner" );
   	my $pm        = $shared->get_cgi( "prj_pm" );
   	my $subject   = $shared->get_cgi( "prj_subject" );
   	my $wordCnt   = $shared->get_cgi( "prj_word_cnt" );
   	my $rate      = $shared->get_cgi( "prj_rate" );
   	my $dueDt     = $shared->getSQLDate( $shared->get_cgi( "prj_due_date" ) );
   	my $dueTm     = $shared->get_cgi( "prj_due_time" );
   	my $assgnTo   = $shared->get_cgi( "prj_assigned_to" );
   	my $furthAct  = $shared->get_cgi( "prj_further_action" );
   	my $clnPNO    = $shared->get_cgi( "prj_client_proj_no" );
   	my $revUSD    = ( $shared->get_cgi( "prj_rev_usd_amt" ) ) + 0;
   	my $revEUR    = ( $shared->get_cgi( "prj_rev_eur_amt" ) ) + 0;
   	my $delvComm  = $shared->get_cgi( "prj_delivered_to_client" );
   	my $delvDt    = $shared->getSQLDate( $shared->get_cgi( "prj_delivery_date" ) );
   	my $invoiced  = $shared->get_cgi( "prj_invoiced" );
   	my $notes     = $shared->get_cgi( "prj_notes" );
   	
   	if ( $invoiced eq "" ) {
   	    $invoiced = "N";
   	}
   	
   	if ( $furthAct eq "" ) {
   	    $furthAct = "N";
   	}
   	
   	my $cur_rec     = $shared->get_rec( "project", "prj_id", $prj_id );
   	my $proj_no     = $cur_rec->{prj_no};
   	my $p_owner     = $cur_rec->{prj_owner};
   	my $p_pm        = $cur_rec->{prj_pm};
   	my $p_subject   = $cur_rec->{prj_subject};
   	my $p_wordCnt   = $cur_rec->{prj_word_cnt};
    my $p_rate      = $cur_rec->{prj_rate};
    my $p_dueDt     = $cur_rec->{prj_due_date};
    my $p_dueTm     = $cur_rec->{prj_due_time};
    my $p_assgnTo   = $cur_rec->{prj_assigned_to};
    my $p_furthAct  = $cur_rec->{prj_further_action};
    my $p_clnPNO    = $cur_rec->{prj_client_proj_no};
    my $p_revUSD    = ( $cur_rec->{prj_rev_usd_amt} ) + 0;
    my $p_revEUR    = ( $cur_rec->{prj_rev_eur_amt} ) + 0;
    my $p_delvComm  = $cur_rec->{prj_delivered_to_client};
    my $p_delvDt    = $cur_rec->{prj_delivery_date};
    my $p_invoiced  = $cur_rec->{prj_invoiced};
    my $p_notes     = $cur_rec->{prj_notes};
    
    my $prjCID      = $cur_rec->{prj_cln_id};
       	
   	my $chg_dtls = "";
   	
   	my $upDDt    = 0;
   	if ( ( $p_delvDt eq "0000-00-00" ) || ( $p_delvDt eq "" ) ) {
   	    $upDDt   = 1;
   	} elsif ( ( $p_delvDt ne "0000-00-00" ) && ( $auth->validate_access( "prjapprove" ) ) ) {
   	    $upDDt   = 1;
   	}
   	
   	my $chkPermClient = 0;
   	if ( ( $p_delvDt eq "0000-00-00" ) || ( $p_delvDt eq "" ) && ( $delvDt ne "0000-00-00" ) ) {
   	    $chkPermClient = 1;
   	}
   	
   	if ( $owner ne $p_owner ) {
   		$chg_dtls .= "Project Owner: from [$p_owner] to [$owner]<br>\n";
   	}

    if ( $pm ne $p_pm ) {
        $chg_dtls .= "Project PM: from [$p_pm] to [$pm]<br>\n";
    }

   	if ( $subject ne $p_subject ) {
   		$chg_dtls .= "Subject: from [$p_subject] to [$subject]<br>\n";
   	}

    if ( $wordCnt ne $p_wordCnt ) {
        $chg_dtls .= "Word Count: from [$p_wordCnt] to [$wordCnt]<br>\n";
    }

   	if ( $rate ne $p_rate ) {
   		$chg_dtls .= "Rate: from [$p_rate] to [$rate]<br>\n";
   	}

    if ( $dueDt ne $p_dueDt ) {
        $chg_dtls .= "Due Date: from [$p_dueDt] to [$dueDt]<br>\n";
    }
    
    if ( $dueTm ne $p_dueTm ) {
        $chg_dtls .= "Due Time: from [$p_dueTm] to [$dueTm]<br>\n";
    }

   	if ( $assgnTo ne $p_assgnTo ) {
   		$chg_dtls .= "Project Assigned To: from [$p_assgnTo] to [$assgnTo]<br>\n";
   	}
   	
   	if ( $furthAct ne $p_furthAct ) {
   	    $chg_dtls .= "Further Action Needed: from [$p_furthAct] to [$furthAct]<br>\n";
   	}

    if ( $clnPNO ne $p_clnPNO ) {
        $chg_dtls .= "Client Project No: from [$p_clnPNO] to [$clnPNO]<br>\n";
    }

   	if ( $revUSD != $p_revUSD ) {
   	    $chg_dtls .= "Project Ravenue in USD: from [$p_revUSD] to [$revUSD]<br>\n";
   	}

   	if ( $revEUR != $p_revEUR ) {
   	    $chg_dtls .= "Project Revenue in EUR: from [$p_revEUR] to [$revEUR]<br>\n";
   	}
   	
   	if ( $delvComm ne $p_delvComm ) {
   	    $chg_dtls .= "Delivered to Client Notes: from [$p_delvComm] to [$delvComm]<br>\n";
   	}
   	
   	if ( $upDDt ) {
   	    if ( $delvDt ne $p_delvDt ) {
   	        $chg_dtls .= "Delivery Date: from [$p_delvDt] to [$delvDt]<br>\n";
   	    }
   	}
   	
   	if ( $invoiced ne $p_invoiced ) {
   	    $chg_dtls .= "Invoiced: from [$p_invoiced] to [$invoiced]<br>\n";
   	}

    if ( $notes ne $p_notes ) {
        $chg_dtls .= "Notes: from [$p_notes] to [$notes]<br>\n";
    }

   	if ( $chg_dtls gt "" ) {
   		chomp( $chg_dtls );
   		$chg_dtls =~ s/<br>$//;
   	}
    	
   	$owner       =~ s/'/\\'/g;
   	$pm          =~ s/'/\\'/g;
   	$subject     =~ s/'/\\'/g;
   	$wordCnt     =~ s/'/\\'/g;
   	$rate        =~ s/'/\\'/g;
   	$dueTm       =~ s/'/\\'/g;
   	$assgnTo     =~ s/'/\\'/g;
   	$clnPNO      =~ s/'/\\'/g;
   	$delvComm    =~ s/'/\\'/g;
   	$notes       =~ s/'/\\'/g;
   	
   	my $err      = "";
   	if ( $dueDt eq "0000-00-00" ) {
   	    $err = "Due Date cannot be blank.";
   	} elsif ( $dueTm eq "" ) {
   	    $err = "Due Time cannot be blank.";
   	} elsif ( $clnPNO eq "" ) {
   	    $err = "Client Project Number cannot be blank.";
   	} elsif ( ( $revUSD == 0 ) && ( $revEUR == 0 ) ) {
   	    $err = "Project Revenue in USD and EUR cannot both be 0.00";
   	}
   	
   	if ( $err eq "" ) {
   	    
   	    my $ddt      = "";
   	    if ( $upDDt ) {
   	        $ddt     = "prj_delivery_date = '$delvDt', ";
   	        
   	        if ( $chkPermClient ) {
   	            my $pcUpdate = "UPDATE client SET cln_active = 'N' WHERE cln_id = $prjCID AND cln_perm = 'N'";
   	            $shared->doSQLUpdate( $pcUpdate );
   	        }
   	    }
   	
   	    my $update   = "UPDATE project SET prj_owner = '$owner', prj_pm = '$pm',\n"
   	                 . "prj_subject = '$subject', prj_word_cnt = '$wordCnt',\n"
   	                 . "prj_rate = '$rate', prj_due_date = '$dueDt', prj_due_time = '$dueTm',\n"
   	                 . "prj_assigned_to = '$assgnTo', prj_further_action = '$furthAct',\n"
   	                 . "prj_client_proj_no = '$clnPNO', prj_rev_usd_amt = $revUSD,\n"
   	                 . "prj_rev_eur_amt = $revEUR, prj_delivered_to_client = '$delvComm',\n"
   	                 . $ddt . "prj_invoiced = '$invoiced', prj_notes = '$notes'\n"
   	                 . "WHERE prj_id = $prj_id";
   	
   	    $shared->doSQLUpdate( $update );
    	
       	if ( $chg_dtls gt "" ) {
   		    &add_audit( $prj_id, "Project $proj_no Record Updated...<br>\n" . $chg_dtls );
   	    }
    	
        $env->{doFade} = "Y";
        $java_msg      = "Project Record Updated...";
    } else {
        $java_msg      = $err;
    }
    
    &edit_prj_form( $prj_id );

}

###########################################################                   
# Delete Project from Database
#----------------------------------------------------------
sub del_prj {

   	my $srch_init = $shared->get_cgi( "srch_init" );

   	my $prj_id = $shared->get_cgi( "prj_id" );
   	my $prjNo  = $shared->getFieldVal( "project", "prj_no", "prj_id", $prj_id );
   	my $po_cnt = $shared->getSQLCount( "SELECT COUNT(*) FROM po_log WHERE pol_proj_no = '$prjNo'" );
   	
    if ( $po_cnt == 0 ) {
   	    my $desc    = "Deleting Project ($prjNo)";
   	    &add_audit( $prj_id, $desc );
    	
   	    my $update = "DELETE FROM project WHERE prj_id = $prj_id";
   	    $shared->doSQLUpdate( $update );
    	
   	} else {
   	    $java_msg = "Unable to Delete Project - Linked PO Found.";
   	}
    
    &search_any( $srch_init );
}


###########################################################
# View Audit Log for a specific application
#----------------------------------------------------------
sub audit_log {

    my $prj_id  = $shared->get_cgi( "prj_id" );
    my $prj_rec = $shared->get_rec( "project", "prj_id", $prj_id );
    my $cln_id  = $prj_rec->{prj_cln_id};
    my $cln_rec = $shared->get_rec( "client", "cln_id", $cln_id );
    my $client  = $cln_rec->{cln_name};
    my $prjNo   = $prj_rec->{prj_no};
    
    my $SQL     = "SELECT usr_name, pra_date, pra_description\n"
                . "FROM project_audit\n"
                . "INNER JOIN user ON pra_usr_id = usr_id\n"
                . "WHERE pra_prj_id = $prj_id\n"
                . "ORDER BY pra_date DESC";
                
	my $content;
    $content .= "<span class=subhead>Client: $client - Proj: $prjNo</span><br><br>\n";
    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
    $content .= "<tr bgcolor=\"black\">\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Log Date</b></font></td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>User</b></font></td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Description</b></font></td></tr>\n";
    
    my @rs    = $shared->getResultSet( $SQL );
    my $row  = 0;
    my $row_color;

	foreach my $rec ( @rs ) {
	   	my $log_date = $shared->getDateFmt( $rec->{pra_date} );
	   	my $uName    = $rec->{usr_name};
	   	my $desc     = $rec->{pra_description};
	    	
    	if ( $row == 0 ) {
    		$row_color = $color_bg2;
    		$row = 1;
    	} else {
    		$row_color = $color_bg1;
    		$row = 0;
    	}
	    	
    	$content .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form>$log_date</td>\n";
	    $content .= "<td valign=top align=left class=form>$uName</td>\n";
	    $content .= "<td valign=top align=left class=form>$desc</td></tr>\n";
	    
    }
    $content .= "</table>\n";

   	$conf->{search_results} = $content;
   	
    my $pg_body = &disp_form( "prj_audit" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
   	
}


###########################################################
# Add Audit Log Record
#----------------------------------------------------------
sub add_audit {
	
	my ( $id, $desc ) = @_;
	
	$desc =~ s/'/\\'/g;
	
	my $insert = "INSERT INTO project_audit ( pra_prj_id, pra_usr_id, pra_date, pra_description )\n"
	           . "VALUES ( $id, $usr_id, now(), '$desc' )";
	$shared->doSQLUpdate( $insert );
	
}

############################################################
# Display form for selecting parameters for commission report
############################################################
sub comm_rpt_form {

    my $begDt             = $shared->getDateFmt( $shared->getDatePrevFirst );
    my $endDt             = $shared->getDateFmt( $shared->getDatePrevLast );
    
   	$conf->{form_head}    = "Project Commission Report";
   	$conf->{form_start}   = "<form method=post name=adm_prj_form action=\"$cgi_url\">\n"
    	                  . "<input type=hidden name=action value=\"comm_rpt\">\n";
    
    $conf->{f_proj_own}   = "<select name=prj_own class=form><option value=\"\"></option>" . $shared->pop_lookup_select( "PRJOWN" ) . "</select>";
    $conf->{f_pm}         = "<select name=prj_pm class=form><option value=\"\"></option>" . $shared->pop_lookup_select( "PRJMGR" ) . "</select>";
    $conf->{f_proj_begin} = $shared->getJSCalInput( "prj_begin", $begDt, "jscal_trigger1" );
    $conf->{f_proj_end}   = $shared->getJSCalInput( "prj_end", $endDt, "jscal_trigger2" );
    
       	
   	$conf->{form_submit}  = "<input type=submit value=\"Generate\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}     = "</form>";
    	
    my $pg_body = &disp_form( "prj_comm_rpt_form" );

    my $js      = $shared->getJSCalHead;

    $env->{javascript}       = $js;
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

############################################################
# Generate Project extract in Excel Format
############################################################
sub genCommRpt {

    my $rptDate  = $shared->getSQLToday;

    my $prjOwn   = $shared->get_cgi( "prj_own" );
    my $prjPM    = $shared->get_cgi( "prj_pm" );
    my $prjBeg   = $shared->getSQLDate( $shared->get_cgi( "prj_begin" ) );
    my $prjEnd   = $shared->getSQLDate( $shared->get_cgi( "prj_end" ) );
        
    my $result   = "";
    if ( ( $prjOwn gt "" ) && ( $prjPM gt "" ) ) {
        $java_msg = "Select Project Owner OR Project Manager - Not Both";
    } elsif ( ( $prjOwn eq "" ) && ( $prjPM eq "" ) ) {
        $java_msg = "You must select Project Owner or Project Manager";
    } else {
        
	    my $fName    = "$forms_path/sdt_comm_rpt.xlsx";
	    my $urlName  = "$forms_url/sdt_comm_rpt.xlsx";
	    my $nowDesc  = $shared->getDateDesc( $rptDate, 1, 1 );
	
	    my $workbook = Excel::Writer::XLSX->new( $fName );
	    $workbook->set_tempdir( $temp_path );
	
	    my $c_litegrey    = $workbook->set_custom_color( 40, 204, 204, 204 );
	    my $c_litegreen   = $workbook->set_custom_color( 41, 210, 255, 214 );
        my %headFont      = ( font  => 'Arial', size  => 14, color => 'black', valign  => 'bottom', bold  => 1 );
        my %subFont       = ( font  => 'Arial', size  => 12, color => 'black', valign  => 'bottom', bold  => 1 );
        my %titleFont     = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom' );
        my %titleFontRt   = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom', align => 'right' );
        my %colFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'bottom', bold => 1, bg_color => $c_litegrey );
    
        my %bodyFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', border => 1, border_color => $c_litegrey );
        my %boldFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, border => 1, border_color => $c_litegrey );
        my %dateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', num_format => 'mm/dd/yyyy', border => 1, border_color => $c_litegrey );
        my %numFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
        my %boldNumFont   = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right' );
        my %currFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
        my %boldCurrFont  = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right', border => 1, border_color => $c_litegrey );
        my %rateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
        my %pctFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
	
	    my $fmtHead  = $workbook->add_format( %headFont );
	    my $fmtSub   = $workbook->add_format( %subFont );
	    my $fmtTitle = $workbook->add_format( %titleFont );
	    my $fmtTtlRt = $workbook->add_format( %titleFontRt );
	    my $fmtCol   = $workbook->add_format( %colFont );
	    my $fmtBody  = $workbook->add_format( %bodyFont );
	    my $fmtBold  = $workbook->add_format( %boldFont );
	    my $fmtDate  = $workbook->add_format( %dateFont );
	    my $fmtNum   = $workbook->add_format( %numFont );
	    my $fmtBNum  = $workbook->add_format( %boldNumFont );
	    my $fmtCurr  = $workbook->add_format( %currFont );
	    my $fmtBCurr = $workbook->add_format( %boldCurrFont );
	    my $fmtRate  = $workbook->add_format( %rateFont );
	    my $fmtPct   = $workbook->add_format( %pctFont );
	
	    $fmtCurr->set_num_format( "\$##,##0.00" );
	    $fmtBCurr->set_num_format( "\$##,##0.00" );
	    $fmtRate->set_num_format( "\$0.0000" );
	    $fmtPct->set_num_format( "0.00\%" );
	    $fmtNum->set_num_format( "0.00" );

	    my $SQL      = "SELECT prj_owner, prj_pm, prj_subject, prj_client_proj_no, prj_rev_usd_amt,\n"
	                 . "prj_rev_eur_amt, prj_delivery_date\n"
	                 . "FROM project\n"
	                 . "WHERE prj_owner = '$prjOwn'\n"
	                 . "AND ( prj_delivery_date >= '$prjBeg' AND prj_delivery_date <= '$prjEnd' )\n"
	                 . "ORDER BY prj_delivery_date";
	    if ( $prjPM gt "" ) {
	        $SQL     = "SELECT prj_owner, prj_pm, prj_subject, prj_client_proj_no, prj_rev_usd_amt,\n"
	                 . "prj_rev_eur_amt, prj_delivery_date\n"
	                 . "FROM project\n"
	                 . "WHERE prj_pm = '$prjPM'\n"
	                 . "AND ( prj_delivery_date >= '$prjBeg' AND prj_delivery_date <= '$prjEnd' )\n"
	                 . "ORDER BY prj_delivery_date";
	    }
	        
        my @rs       = $shared->getResultSet( $SQL );
    	             
        my $sheet    = $workbook->add_worksheet( "Commission Rpt" );
            
        $sheet->write_string( 0, 0, "SDT Project Commission Report", $fmtHead );
        $sheet->write_string( 0, 5, "As of: $nowDesc", $fmtTtlRt );
        
        my @colHead  = ( "Project Owner", "Project Manager", "Product (Subject)", "Client Proj No", "Revenue (USD)", "Revenue (EUR)", "Delivery Date" );
        $sheet->write_row( 2, 0, \@colHead, $fmtCol );
        
        
        my $tAmt = 0;
        my $row  = 3;
	    foreach my $rec ( @rs ) {

            my $own    = $rec->{prj_owner};
            my $pm     = $rec->{prj_pm};
            my $subj   = $rec->{prj_subject};
            my $clnPNO = $rec->{prj_client_proj_no};
            my $revUSD = ( $rec->{prj_rev_usd_amt} ) + 0;
            my $revEUR = ( $rec->{prj_rev_eur_amt} ) + 0;
            my $delvDt = $rec->{prj_delivery_date};
        
            $tAmt += $revUSD;
        
            $sheet->write_string( $row, 0, $own, $fmtBody );
            $sheet->write_string( $row, 1, $pm, $fmtBody );
            $sheet->write_string( $row, 2, $subj, $fmtBody );
            $sheet->write_string( $row, 3, $clnPNO, $fmtBody );
            $sheet->write_number( $row, 4, $revUSD, $fmtCurr );
            $sheet->write_number( $row, 5, $revEUR, $fmtNum );
            $sheet->write_date_time( $row, 6, $delvDt . "T", $fmtDate );
        
            $row++;
        }
    
        my $commPct  = $shared->getConfigInt( "ADM_PRJCOMMRATE" );
        my $commRate = ( $commPct / 100 );
        my $commAmt  = ( $tAmt * $commRate );
        
        $sheet->write_string( $row, 3, "TOTAL AMOUNT:", $fmtBold );
        $sheet->write_formula( $row, 4, "=SUM(E4:E" . $row . ")", $fmtBCurr, $tAmt );
        $row++;
        $sheet->write_string( $row, 3, "TOTAL COMMISSION ($commPct\%):", $fmtBold );
        $sheet->write_formula( $row, 4, "=E" . $row . "*" . $commRate, $fmtBCurr, $commAmt );

        $sheet->set_column( 0, 2, 20 );
        $sheet->set_column( 3, 3, 56 );
        $sheet->set_column( 4, 6, 15 );

        $sheet->hide_gridlines( 2 );
        $sheet->freeze_panes( 3, 0 );

        $workbook->close();
    
        system( "chmod 644 $fName" );
        
        $result = $urlName;
    }
    
    return $result;

}

############################################################
# Display form for selecting parameters for profitability report
############################################################
sub profit_rpt_form {

    my $begDt             = $shared->getDateFmt( $shared->getDatePrevFirst );
    my $endDt             = $shared->getDateFmt( $shared->getDatePrevLast );
    
   	$conf->{form_head}    = "Project Profitability Report";
   	$conf->{form_start}   = "<form method=post name=adm_prj_form action=\"$cgi_url\">\n"
    	                  . "<input type=hidden name=action value=\"profit_rpt\">\n";
    
    $conf->{f_proj_begin} = $shared->getJSCalInput( "prj_begin", $begDt, "jscal_trigger1" );
    $conf->{f_proj_end}   = $shared->getJSCalInput( "prj_end", $endDt, "jscal_trigger2" );
    
       	
   	$conf->{form_submit}  = "<input type=submit value=\"Generate\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}     = "</form>";
    	
    my $pg_body = &disp_form( "prj_profit_rpt_form" );

    my $js      = $shared->getJSCalHead;

    $env->{javascript}       = $js;
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

############################################################
# Generate Project Profitability in Excel Format
############################################################
sub genProfitRpt {

    my $rptDate  = $shared->getSQLToday;

    my $fxEUR    = $shared->get_cgi( "fx_eur" ) + 0;
    my $fxGBP    = $shared->get_cgi( "fx_gbp" ) + 0;
    my $prjBeg   = $shared->getSQLDate( $shared->get_cgi( "prj_begin" ) );
    my $prjEnd   = $shared->getSQLDate( $shared->get_cgi( "prj_end" ) );
        
    my $result   = "";
    if ( ( $fxEUR == 0 ) || ( $fxGBP == 0 ) ) {
        $java_msg = "FX Rates are required";
    } else {
        
	    my $fName    = "$forms_path/sdt_profit_rpt.xlsx";
	    my $urlName  = "$forms_url/sdt_profit_rpt.xlsx";
	    my $nowDesc  = $shared->getDateDesc( $rptDate, 1, 1 );
	
	    my $workbook = Excel::Writer::XLSX->new( $fName );
	    $workbook->set_tempdir( $temp_path );
	
	    my $c_litegrey    = $workbook->set_custom_color( 40, 204, 204, 204 );
	    my $c_litegreen   = $workbook->set_custom_color( 41, 210, 255, 214 );
        my %headFont      = ( font  => 'Arial', size  => 14, color => 'black', valign  => 'bottom', bold  => 1 );
        my %subFont       = ( font  => 'Arial', size  => 12, color => 'black', valign  => 'bottom', bold  => 1 );
        my %titleFont     = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom' );
        my %titleFontRt   = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom', align => 'right' );
        my %colFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'bottom', bold => 1, bg_color => $c_litegrey );
    
        my %bodyFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', border => 1, border_color => $c_litegrey );
        my %boldFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, border => 1, border_color => $c_litegrey );
        my %dateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', num_format => 'mm/dd/yyyy', border => 1, border_color => $c_litegrey );
        my %numFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
        my %boldNumFont   = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right' );
        my %currFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
        my %boldCurrFont  = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right', border => 1, border_color => $c_litegrey );
        my %rateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
        my %pctFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
	
	    my $fmtHead  = $workbook->add_format( %headFont );
	    my $fmtSub   = $workbook->add_format( %subFont );
	    my $fmtTitle = $workbook->add_format( %titleFont );
	    my $fmtTtlRt = $workbook->add_format( %titleFontRt );
	    my $fmtCol   = $workbook->add_format( %colFont );
	    my $fmtBody  = $workbook->add_format( %bodyFont );
	    my $fmtBold  = $workbook->add_format( %boldFont );
	    my $fmtDate  = $workbook->add_format( %dateFont );
	    my $fmtNum   = $workbook->add_format( %numFont );
	    my $fmtBNum  = $workbook->add_format( %boldNumFont );
	    my $fmtCurr  = $workbook->add_format( %currFont );
	    my $fmtBCurr = $workbook->add_format( %boldCurrFont );
	    my $fmtRate  = $workbook->add_format( %rateFont );
	    my $fmtPct   = $workbook->add_format( %pctFont );
	
	    $fmtCurr->set_num_format( "\$##,##0.00" );
	    $fmtBCurr->set_num_format( "\$##,##0.00" );
	    $fmtRate->set_num_format( "\$0.0000" );
	    $fmtPct->set_num_format( "0.00\%" );
	    $fmtNum->set_num_format( "0.00" );

	    my $SQL      = "SELECT prj_no, cln_name, prj_rev_usd_amt, prj_rev_eur_amt, prj_delivery_date\n"
	                 . "FROM project\n"
	                 . "INNER JOIN client ON cln_id = prj_cln_id\n"
	                 . "WHERE prj_delivery_date >= '$prjBeg' AND prj_delivery_date <= '$prjEnd'\n"
	                 . "ORDER BY prj_delivery_date, prj_no";
	        
        my @rs       = $shared->getResultSet( $SQL );
    	             
        my $sheet    = $workbook->add_worksheet( "Profit Rpt" );
            
        $sheet->write_string( 0, 0, "SDT Project Profitability Report", $fmtHead );
        $sheet->write_string( 0, 5, "As of: $nowDesc", $fmtTtlRt );
        
        my @colHead  = ( "Proj No", "Delivery Date", "Client", "Revenue (USD)", "Costs (USD)", "Profit/Loss (USD)" );
        $sheet->write_row( 2, 0, \@colHead, $fmtCol );
        
        
        my $tREV = 0;
        my $tCST = 0;
        my $row  = 3;
	    foreach my $rec ( @rs ) {

            my $prjNo  = $rec->{prj_no};
            my $cName  = $rec->{cln_name};
            my $revUSD = ( $rec->{prj_rev_usd_amt} ) + 0;
            my $revEUR = ( $rec->{prj_rev_eur_amt} ) + 0;
            my $delvDt = $rec->{prj_delivery_date};

            my $revAmt = $revUSD;
            if ( $revEUR > 0 ) {
                $revAmt = ( $revEUR * $fxEUR );
            }
                    
            $tREV += $revAmt;
            
            my $pCost = 0;
            my $SUB   = "SELECT pol_total_amt, pol_currency FROM po_log WHERE pol_proj_no = '$prjNo'";
            my @srs   = $shared->getResultSet( $SUB );
            foreach my $sRec ( @srs ) {
                my $poAmt = $sRec->{pol_total_amt} + 0;
                my $poCur = $sRec->{pol_currency};
                
                my $poUSD = $poAmt;
                if ( $poCur eq "EUR" ) {
                    $poUSD = ( $poAmt * $fxEUR );
                } elsif ( $poCur eq "GBP" ) {
                    $poUSD = ( $poAmt * $fxGBP );
                }
                $pCost += $poUSD;
            }
            
            $tCST += $pCost;
            
            my $plAmt = ( $revAmt - $pCost );
        
            $sheet->write_string( $row, 0, $prjNo, $fmtBody );
            $sheet->write_date_time( $row, 1, $delvDt . "T", $fmtDate );
            $sheet->write_string( $row, 2, $cName, $fmtBody );
            $sheet->write_number( $row, 3, $revAmt, $fmtCurr );
            $sheet->write_number( $row, 4, $pCost, $fmtCurr );
            $sheet->write_number( $row, 5, $plAmt, $fmtCurr );
         
            $row++;
        }
        
        my $tPL = ( $tREV - $tCST );

        $row++;
            
        $sheet->write_string( $row, 0, "TOTALS:", $fmtBold );
        $sheet->write_formula( $row, 3, "=SUM(D4:D" . ( $row - 1 ) . ")", $fmtBCurr, $tREV );
        $sheet->write_formula( $row, 4, "=SUM(E4:E" . ( $row - 1 ) . ")", $fmtBCurr, $tCST );
        $sheet->write_formula( $row, 5, "=D" . ( $row + 1 ) . "-E" . ( $row + 1 ), $fmtBCurr, $tPL );

        $sheet->set_column( 0, 1, 15 );
        $sheet->set_column( 2, 2, 50 );
        $sheet->set_column( 3, 5, 20 );

        $sheet->hide_gridlines( 2 );
        $sheet->freeze_panes( 3, 0 );

        $workbook->close();
    
        system( "chmod 644 $fName" );
        
        $result = $urlName;
    }
    
    return $result;

}

############################################################
# Generate Invoice Pending List (Excel)
############################################################
sub genExpList {

    my $rptDate  = $shared->getSQLToday;

    my $result   = "";
        
	my $fName    = "$forms_path/projects_pending_invoice.xlsx";
	my $urlName  = "$forms_url/projects_pending_invoice.xlsx";
	
	my $workbook = Excel::Writer::XLSX->new( $fName );
	$workbook->set_tempdir( $temp_path );
	
	my $c_litegrey    = $workbook->set_custom_color( 40, 204, 204, 204 );
	my $c_litegreen   = $workbook->set_custom_color( 41, 210, 255, 214 );
    my %colFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'bottom', bold => 1, bg_color => $c_litegrey );
    
    my %bodyFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', border => 1, border_color => $c_litegrey );
    my %boldFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, border => 1, border_color => $c_litegrey );
    my %dateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', num_format => 'mm/dd/yyyy', border => 1, border_color => $c_litegrey );
    my %numFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %boldNumFont   = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right' );
    my %currFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %boldCurrFont  = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right', border => 1, border_color => $c_litegrey );
    my %rateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %pctFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
	
	my $fmtCol   = $workbook->add_format( %colFont );
	my $fmtBody  = $workbook->add_format( %bodyFont );
	my $fmtBold  = $workbook->add_format( %boldFont );
	my $fmtDate  = $workbook->add_format( %dateFont );
	my $fmtNum   = $workbook->add_format( %numFont );
	my $fmtBNum  = $workbook->add_format( %boldNumFont );
	my $fmtCurr  = $workbook->add_format( %currFont );
	my $fmtBCurr = $workbook->add_format( %boldCurrFont );
	my $fmtRate  = $workbook->add_format( %rateFont );
	my $fmtPct   = $workbook->add_format( %pctFont );
	
	$fmtCurr->set_num_format( "\$0.00" );
	$fmtBCurr->set_num_format( "\$0.00" );
	$fmtRate->set_num_format( "\$0.0000" );
	$fmtPct->set_num_format( "0.00\%" );
	$fmtNum->set_num_format( "0.00" );


    my $SQL = "SELECT prj_id, prj_no, prj_pm, cln_name, prj_due_date, prj_due_time, prj_owner,\n"
	        . "prj_assigned_to, prj_rev_usd_amt, prj_rev_eur_amt, prj_subject,\n"
	        . "prj_client_proj_no, clc_contact, prj_delivery_date, prj_delivered_to_client\n"
	        . "FROM project\n"
	        . "INNER JOIN client ON cln_id = prj_cln_id\n"
	        . "LEFT JOIN client_contact ON clc_id = prj_clc_id\n"
	        . "WHERE prj_delivery_date IS NOT NULL\n"
	        . "AND prj_delivery_date <> '0000-00-00'\n"
	        . "AND prj_invoiced = 'N'\n"
	        . "ORDER BY prj_id";

    my @rs       = $shared->getResultSet( $SQL );
    	             
    my $sheet    = $workbook->add_worksheet( "Pend Invoice" );
            
    my @colHead  = ( "Client Name", "Delivery Date", "Product (Subject)", "Invoice String", 
                     "Project Revenue USD", "Project Revenue EUR", "Project Owner",
                     "Project Manager", "Assigned To", "Status/Delivery Updates", "Notes" );
    $sheet->write_row( 0, 0, \@colHead, $fmtCol );
        
    my $row  = 1;
	foreach my $rec ( @rs ) {

  	    my $id      = $rec->{prj_id};
	    my $prjno   = $rec->{prj_no};
	    my $pm      = $rec->{prj_pm};
	    my $subj    = $rec->{prj_subject};
	    my $name    = $rec->{cln_name};
	    my $asgnTo  = $rec->{prj_assigned_to};
	    my $own     = $rec->{prj_owner};
	    my $revUSD  = ( $rec->{prj_rev_usd_amt} ) + 0;
	    my $revEUR  = ( $rec->{prj_rev_eur_amt} ) + 0;
        my $delvDt  = $rec->{prj_delivery_date};
        my $clnPNO  = $rec->{prj_client_proj_no};
        my $clcName = $rec->{clc_contact};
        my $dtcStat = $rec->{prj_delivered_to_client};
        my $notes   = $rec->{prj_notes};

        my $invStr  = "Client Project \"" . $clnPNO . "\" \| Client Contact - " . $clcName . " \| Project #" . $prjno;
                    
        $sheet->write_string( $row, 0, $name, $fmtBody );
        $sheet->write_date_time( $row, 1, $delvDt . "T", $fmtDate );
        $sheet->write_string( $row, 2, $subj, $fmtBody );
        $sheet->write_string( $row, 3, $invStr, $fmtBody );
        $sheet->write_number( $row, 4, $revUSD, $fmtNum );
        $sheet->write_number( $row, 5, $revEUR, $fmtNum );
        $sheet->write_string( $row, 6, $own, $fmtBody );
        $sheet->write_string( $row, 7, $pm, $fmtBody );
        $sheet->write_string( $row, 8, $asgnTo, $fmtBody );
        $sheet->write_string( $row, 9, $dtcStat, $fmtBody );
        $sheet->write_string( $row, 10, $notes, $fmtBody );
        
        $row++;
    }
    
    $sheet->set_column( 0, 0, 30 );
    $sheet->set_column( 1, 1, 14 );
    $sheet->set_column( 2, 2, 19 );
    $sheet->set_column( 3, 3, 100 );
    $sheet->set_column( 4, 5, 20 );
    $sheet->set_column( 6, 8, 18 );
    $sheet->set_column( 9, 10, 65 );

    $sheet->hide_gridlines( 2 );
    $sheet->freeze_panes( 3, 0 );

    $workbook->close();
    
    system( "chmod 644 $fName" );
        
    $result = $urlName;
    
    return $result;

}

############################################################
# Mark projects invoiced
############################################################
sub prj_mark_inv_all {

    my $SQL = "SELECT prj_id, prj_no\n"
	        . "FROM project\n"
	        . "WHERE prj_delivery_date IS NOT NULL\n"
	        . "AND prj_delivery_date <> '0000-00-00'\n"
	        . "AND prj_invoiced = 'N'\n"
	        . "ORDER BY prj_id";
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        
        my $prj_id = $rec->{prj_id};
        my $prjNo  = $rec->{prj_no};
        
        my $fldChk = "mrk_" . $prj_id;
        my $fldVal = $shared->get_cgi( $fldChk );
        if ( $fldVal eq "Y" ) {
            my $update = "UPDATE project SET prj_invoiced = 'Y' WHERE prj_id = $prj_id";
            $shared->doSQLUpdate( $update );
            my $desc   = "Project " . $prjNo . " Record Updated...<br>Invoiced: from [N] to [Y]";
            &add_audit( $prj_id, $desc );
        }
    }
}
            
        
