#!/usr/bin/perl
###########################################################################
# tmadm.cgi             Terminology Manager - Administration script
###########################################################################
# Copyright 2013, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/tmadm.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_head2   = $colors{ "HEAD2" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $exceptList    = "";
my $dupList       = "";
my $debug         = $config->get_debug;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;


# Main program
my $usr_id = 0;
my $action = $shared->get_cgi( "action" );
my $hash_vars;
my $next_action = $shared->get_cgi( "next_act" );

$hash_vars->{srch_code}    = $shared->get_cgi( "srch_code" );
$hash_vars->{srch_desc}    = $shared->get_cgi( "srch_desc" );

my $srch_desc    = $hash_vars->{srch_desc};
my $srch_code    = $hash_vars->{srch_code};
	
my $inp_srch     = "";
my $sp_vars      = "";
if ( $srch_desc gt "" ) {
	$inp_srch .= "<input type=hidden name=srch_desc value=\"$srch_desc\">\n";
   	$sp_vars  .= "&srch_desc=$srch_desc";
}
if ( $srch_code gt "" ) {
   	$inp_srch .= "<input type=hidden name=srch_code value=\"$srch_code\">\n";
   	$sp_vars  .= "&srch_code=$srch_code";
}


#
# Main Program
#
 
if ( $auth->validate_access( "admin_tmadm" ) ) {
	
	$usr_id    = $auth->getCurrentUser;

	if ( $action eq "add_user_form" ) {
	    &add_user_form;
	} elsif ( $action eq "add_user" ) {
	    &add_user;
	} elsif ( $action eq "edit_user_form" ) {
	    &edit_user_form;
	} elsif ( $action eq "edit_user" ) {
	    &edit_user;
    } elsif ( $action eq "del_user" ) {
        &del_user;
    } elsif ( $action eq "user_maint" ) {
        &user_maint;
    } elsif ( $action eq "search_any" ) {
        &search_any;
    } elsif ( $action eq "edit_dom_form" ) {
        &edit_dom_form;
    } elsif ( $action eq "edit_dom" ) {
        &edit_dom;
    } elsif ( $action eq "add_dom_form" ) {
        &add_dom_form;
    } elsif ( $action eq "add_dom" ) {
        &add_dom;
    } elsif ( $action eq "del_dom" ) {
        &del_dom;
    } elsif ( $action eq "import_form" ) {
        &import_form;
    } elsif ( $action eq "import" ) {
        my $status = &import;
        if ( $status eq "ERROR" ) {
            $java_msg = "Error importing file";
            &import_form;
        } else {
            &print_main;
        }
    } elsif ( $action eq "import_undo" ) {
        &import_undo;
    } else {
        &print_main;
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}

############################################################
# Print the main term mgr admin screen
############################################################
sub print_main {

    my $pg_body              = &disp_form( "tmadm_main" );

    $env->{page_head}        = "Terminology Manager Admin";
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub dom_maint {

    my $dom_cnt = $shared->getSQLCount( "SELECT count(*) FROM language" );

    if ( $conf->{dom_cnt} eq "" ) {
        $conf->{dom_cnt}     = $dom_cnt;
    }
    
   	my $abContent = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=add_dom_form>\n"
                  . $inp_srch
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Add Domain\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";

 	$conf->{add_button}  = $abContent;
    
    my $pg_body              = &disp_form( "dom_main" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my $content;
	
	my $srch_head    = "<b>Searched:</b>&nbsp;";
	my $srch_par     = 0;
	
    my $SQL  = "SELECT lku_id, lku_code, lku_description\n"
             . "FROM lookup\n"
             . "WHERE lku_category = 'TM_DOMAIN'\n";

    if ( $srch_desc gt "" ) {
    	my $dom_srch_desc = $srch_desc;
    	$dom_srch_desc =~ s/'/\\'/g;
	    $SQL .= "AND lku_description LIKE '$dom_srch_desc%'\n";
	    if ( $srch_par > 0 ) {
	        $srch_head .= ", Domain starts with <b>$srch_desc</b>";
	    } else {
	        $srch_head .= "Domain starts with <b>$srch_desc</b>";
	    }
	    $srch_par++;
    }
    if ( $srch_code gt "" ) {
	    $SQL .= "AND lku_code = '$srch_code'\n";
	    if ( $srch_par > 0 ) {
	        $srch_head .= ", Domain code is <b>$srch_code</b>";
	    } else {
	        $srch_head .= "Domain code is <b>$srch_code</b>";
	    }
	    $srch_par++;
    }
	    
    $SQL     .= "ORDER BY lku_description";
    
    if ( $srch_par == 0 ) {
        $srch_head = "<b>All Domains</b>";
	}
	
    my @rs   = $shared->getResultSet( $SQL );
    my $row  = 0;
    my $row_color;
    my $fndCnt = 0;
	    
    $content .= "<span class=subhead><b>$srch_head</b></span><br><br>\n";
    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
    $content .= "<tr bgcolor=\"black\">\n";
    $content .= "<td valign=top align=left class=form>&nbsp;</td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Code</b></font></td>\n";
	$content .= "<td valign=top align=left class=form><font color=\"white\"><b>Domain</b></font></td>\n";
	$content .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
	foreach my $rec ( @rs ) {
	   	my $id      = $rec->{lku_id};
	   	my $code    = $rec->{lku_code};
	   	my $desc    = $rec->{lku_description};
	    	
    	if ( $row == 0 ) {
    		$row_color = $color_bg2;
    		$row = 1;
    	} else {
    		$row_color = $color_bg1;
    		$row = 0;
    	}
	    	
    	$content .= "<tr bgcolor=\"$row_color\">\n";
    	$content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=edit_dom_form&dom_id=$id" . $sp_vars . "\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"View\" title=\"View $desc\"></a></td>\n";
	   	$content .= "<td valign=top align=left class=form>$code</td>\n";
	   	$content .= "<td valign=top align=left class=form>$desc</td>\n";
	   	$content .= "<td valign=top align=right class=form>";
   	    $content .= "<a href=\"$cgi_url?action=del_dom&dom_id=$id" . $sp_vars . "\" onClick=\"return confirm('Permanently remove $desc from database?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete $desc\"></a>";
	    $content .= "</td></tr>\n";
	    	
	    $fndCnt++;
    }
    $content .= "</table>\n";
	    		
    if ( $fndCnt == 0 ) {
    	$content  = "";
    	$java_msg = "No Domains found meeting search criteria.";
    } else {
    	$conf->{dom_cnt} = "$fndCnt";
    }
	
   	$conf->{search_results} = $content;
	
	&dom_maint;
	
}



############################################################
# Form to add member to database
############################################################
sub add_dom_form {

  	my $today = $shared->getSQLToday;
    	
   	$conf->{form_head}       = "Add New Domain";
   	$conf->{form_start}      = "<form method=post name=adm_dom_form action=\"$cgi_url\">\n"
    	                     . "<input type=hidden name=action value=\"add_dom\">\n"
    	                     . $inp_srch;
    $conf->{f_dom_code}      = "<input name=dom_code value=\"\" size=5 maxlength=3 class=form>";
   	$conf->{f_dom_desc}      = "<input name=dom_desc value=\"\" size=40 maxlength=60 class=form>";
   	$conf->{form_submit}     = "<input type=submit value=\"Add Now\" class=form>&nbsp;\n"
                             . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}        = "</form>";
   	$conf->{form_name}       = "adm_dom_form";
   	$conf->{focus_field}     = "dom_code";  
    	
    my $pg_body = &disp_form( "dom_add_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

############################################################
# Add the record to the loan database
############################################################
sub add_dom {

    my $dom_code  = $shared->get_cgi( "dom_code" );
   	my $dom_desc  = $shared->get_cgi( "dom_desc" );
   	
   	my $codeCnt   = $shared->getSQLCount( "SELECT COUNT(*) FROM lookup WHERE lku_category = 'TM_DOMAIN' AND lku_code = '$dom_code'" );
   	if ( $codeCnt == 0 ) {
    	
    	$dom_code    =~ s/'/\\'/g;
   	    $dom_desc    =~ s/'/\\'/g;
    	
        my $insert   = "INSERT INTO lookup ( lku_category, lku_code, lku_description )\n"
                     . "VALUES ( 'TM_DOMAIN', '$dom_code', '$dom_desc' )";
        $shared->doSQLUpdate( $insert );
        
    } else {
        $java_msg = "Code already exists - Must be unique.";
    }
        	
   	&search_any;

}

############################################################
# Form to edit member of database
############################################################
sub edit_dom_form {

    my ( $dom_id ) = @_;
    
    if ( $dom_id eq "" ) {
        $dom_id  = $shared->get_cgi( "dom_id" );
    }
    
    my $dom_rec = $shared->get_rec( "lookup", "lku_id", $dom_id );

    my $cCode    = $dom_rec->{lku_code};
    my $cName    = $dom_rec->{lku_description};
    
    $sp_vars =~ s/^\&//;
    
    $conf->{form_id_row}    = "<tr><td align=right valign=top class=label bgcolor=$color_bg2 width=150>ID</td>\n"
                            . "<td valign=top align=left class=form><b>$dom_id</b></td></tr>\n";

    
   	$conf->{form_head}       = "Update Domain";
   	$conf->{form_start}      = "<form method=post name=dom_edit_form action=\"$cgi_url\">\n"
   	                         . "<input type=hidden name=action value=\"edit_dom\">\n"
   	                         . "<input type=hidden name=dom_id value=\"$dom_id\">\n"
   	                         . $inp_srch;
   	$conf->{f_dom_code}      = "<input name=dom_code value=\"" . $cCode . "\" size=5 maxlength=3 class=form>";
   	$conf->{f_dom_desc}      = "<input name=dom_desc value=\"" . $cName . "\" size=40 maxlength=60 class=form>";
    	
   	$conf->{form_submit}     = "<input type=submit value=\"Update\" class=form>&nbsp;\n"
                             . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?action=search_any&$sp_vars'\" class=form>";
   	$conf->{form_end}        = "</form>";
    	
    my $pg_body = &disp_form( "dom_edit_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

############################################################
# Update the record to the members database
############################################################
sub edit_dom {

    my $dom_id    = $shared->get_cgi( "dom_id" );
    my $dom_code  = $shared->get_cgi( "dom_code" );
   	my $dom_desc  = $shared->get_cgi( "dom_desc" );
    
    my $rec       = $shared->get_rec( "lookup", "lku_id", $dom_id );
    my $p_code    = $rec->{lku_code};

   	$dom_desc    =~ s/'/\\'/g;

    if ( ( $p_code ne $dom_code ) && ( $dom_code ne "" ) ) {
        my $up1 = "UPDATE tm_main SET tmm_domain = '$dom_code' WHERE tmm_domain = '$p_code'";
        $shared->doSQLUpdate( $up1 );
    }
    
   	my $update   = "UPDATE lookup SET lku_code = '$dom_code', lku_description = '$dom_desc'\n"
   	             . "WHERE lku_id = $dom_id";
   	$shared->doSQLUpdate( $update );
    	
    &search_any;

}

###########################################################                   
# Delete Domain from database
#----------------------------------------------------------
sub del_dom {

   	my $dom_id = $shared->get_cgi( "dom_id" );
   	my $rec    = $shared->get_rec( "lookup", "lku_id", $dom_id );
   	my $f_code = $rec->{lku_code};
   	my $d_cnt  = $shared->getSQLCount( "SELECT COUNT(*) FROM tm_main WHERE tmm_domain = '$f_code'" );
   	
    if ( $d_cnt == 0 ) {	
    	
   	    my $update = "DELETE FROM lookup WHERE lku_id = $dom_id";
   	    $shared->doSQLUpdate( $update );
    	
    } else {

        $java_msg = "Unable to Delete Domain - Remove Associated Terminology Pairs First";

    }
    
    &search_any;
}


###########################################################                   
# Maintain Users
#----------------------------------------------------------
sub user_maint {

    my $in_width;
    my $row;
    my $row_color;
    
    my $SQL = "SELECT * FROM tm_user ORDER BY tmu_login";
    my @rs  = $shared->getResultSet( $SQL );
    
        
    my $pg_body  = "<form method=post action=\"$cgi_url\">\n";
    $pg_body .= "<input type=hidden name=action value=add_user_form>\n";
    
    
    $pg_body .= "<table border=0 cellpadding=0 cellspacing=0 width=\"640\">\n";
    $pg_body .= "<tr><td>\n";
    $pg_body .= "<div class=blur><div class=shadow><div class=framework>\n";
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=\"640\">\n";
    $pg_body .= "<tr bgcolor=$color_bg1><td valign=top align=left>\n";
    $pg_body .= "<table border=0 cellpadding=3 cellspacing=0 width=\"636\">\n";
    $pg_body .= "<tr bgcolor=$color_head2><td valign=top align=left class=colhead width=80>User ID</td>\n";
    $pg_body .= "<td valign=top align=left class=colhead width=150>Name</td>\n";
    $pg_body .= "<td valign=top align=left class=colhead width=100>Type</td>\n";
    $pg_body .= "<td valign=top class=rightcolhead width=80><input type=submit value=\"New User\" class=form></td></tr>\n";
    
    $row = 0;
    
    foreach my $usr_rec ( @rs ) {
        my $uID       = $usr_rec->{tmu_id};
        my $usr_login = $usr_rec->{tmu_login};
        my $usr_name  = $usr_rec->{tmu_name};
        my $usr_type  = $usr_rec->{tmu_type};
        
        if ( $row == 0 ) {
            $row = 1;
            $row_color = $color_bg1;
        } else {
            $row = 0;
            $row_color = $color_bg2;
        }

        my $dspType = "Client";
        if ( $usr_type eq "OTH" ) {
            $dspType = "Non-Client";
        }
                
        $pg_body .= "<tr bgcolor=$row_color><td valign=top align=left class=colbody><b>$usr_login</b></td>";
        $pg_body .= "<td valign=top align=left class=colbody>$usr_name</td>";
        $pg_body .= "<td valign=top align=left class=colbody>$dspType</td>";
        $pg_body .= "<td valign=top align=right class=colbody>";
        $pg_body .= "<a href=\"$cgi_url?action=edit_user_form&tmu_id=$uID\">[ Edit ]</a></td></tr>\n";
    }
        
    $pg_body .= "</table>\n";
    
    $pg_body .= "</td></tr>\n";    
    $pg_body .= "</table>\n";
    $pg_body .= "</div></div></div>\n";
    $pg_body .= "</tr></td>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</form>\n";
    
    
    $env->{page_head}    = "Terminology Manager User Admin";
    $env->{html_title}   = "$site_title - Terminology Manager User Admin";
    $env->{page_body}    = $pg_body;
        
    $shared->page_gen( $env, $java_msg );
        
}

###########################################################                   
# Form to Add a new user
#----------------------------------------------------------
sub add_user_form {

    my $in_width;
    
    my $pg_body = "";
    
    $pg_body  = "<form method=post action=\"$cgi_url\">\n";
    $pg_body .= "<input type=hidden name=action value=add_user>\n";
    
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=614>\n";
    $pg_body .= "<tr bgcolor=$color_bg1><td>\n";
    
    $in_width = 610;
    
    $pg_body .= "<table border=0 cellpadding=3 cellspacing=0 width=$in_width>\n";
    $pg_body .= "<tr bgcolor=$color_bg2><td colspan=2 class=sublabel>\n";
    $pg_body .= "Input the following values below to add a new user.  User ID must be a unique.</td></tr>\n";
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>User ID</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=tmu_login size=10 class=form></td></tr>\n";
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>Type</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><select id=tmu_type name=tmu_type class=form><option value=\"CLN\">Client</option><option value=\"OTH\">Non-Client</option></select></td></tr>\n";
    
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>Name</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=tmu_name size=30 class=form></td></tr>\n";
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>Initial Password</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=tmu_passwd size=10 class=form></td></tr>\n";
    
    $pg_body .= "<tr><td bgcolor=$color_bg2 valign=top align=left class=label>&nbsp;</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg2 class=form>\n";
    $pg_body .= "<input type=submit value=\"Add New User\" class=form>&nbsp;\n";
    $pg_body .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?action=user_maint'\" class=form></td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</form>\n";   
    
    $env->{page_head}    = "Terminology Manager - Add User";
    $env->{html_title}   = "$site_title - Terminology Manager - Add New User";
    $env->{page_body}    = $pg_body;
        
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Add the user to the database
#----------------------------------------------------------
sub add_user {

    my $usr_login = $shared->get_cgi( "tmu_login" );
    my $usr_name  = $shared->get_cgi( "tmu_name" );
    my $usr_pass  = $shared->get_cgi( "tmu_passwd" );
    my $usr_type  = $shared->get_cgi( "tmu_type" );
    
    my $chkSQL = "SELECT count(*) FROM tm_user WHERE tmu_login = '$usr_login'";
    if ( $shared->getSQLCount( $chkSQL ) ) {
        $java_msg = "Login ID already exists.  Please select another.";
        &add_user_form;
    } else {
        my $insert = "INSERT INTO tm_user ( tmu_login, tmu_name, tmu_passwd, tmu_type )\n"
                   . "VALUES ( '$usr_login', '$usr_name', '$usr_pass', '$usr_type' )";
        $shared->doSQLUpdate( $insert );
        my $uID    = $shared->getSQLLastInsertID;
                
        $shared->add_log( "TMUADD", "Added TM User: $usr_login ($usr_name)", $usr_id );
        
        &edit_user_form( $uID );
    }
}


###########################################################                   
# Form to Edit user
#----------------------------------------------------------
sub edit_user_form {

    my ( $uID ) = @_;
    
    if ( $uID eq "" ) {
        $uID = $shared->get_cgi( "tmu_id" );
    }
    
    my $usr_rec   = $shared->get_rec( "tm_user", "tmu_id", $uID );

    my $usr_login = $usr_rec->{tmu_login};
    my $usr_name  = $usr_rec->{tmu_name};
    my $usr_type  = $usr_rec->{tmu_type};
    my $lnkID     = $usr_rec->{tmu_cln_id};

    my $in_width = 814;
    
    my $dspType  = "Non-Client";
    my $inpLink  = "";
    if ( $usr_type eq "CLN" ) {
        $dspType = "Client";
        $inpLink = "Link to: <select name=usr_link_id class=form><option value=\"0\"></option>" . &getCLNSelect( $lnkID ) . "</select>";
    }
    
    my $pg_body = "";

    $pg_body .= "<table border=0 cellpadding=3 cellspacing=0 width=$in_width>\n";
    $pg_body .= "<tr bgcolor=$color_bg2><td valign=top align=left class=form colspan=2>\n";
    $pg_body .= "Update the values below.  To reset a user's password, type a new password in the\n";
    $pg_body .= "appropriate field below.  <b>Leave the password field blank to keep the existing one</b>.</td></tr>\n";
    $pg_body .= "<tr bgcolor=$color_bg2><td valign=top align=left width=600>\n";

    $pg_body .= "<form method=post action=\"$cgi_url\">\n";
    $pg_body .= "<input type=hidden name=action value=\"edit_user\">\n";
    $pg_body .= "<input type=hidden name=tmu_id value=\"$uID\">\n";
    $pg_body .= "<input type=hidden name=tmu_type value=\"$usr_type\">\n";
    
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=594>\n";
    $pg_body .= "<tr><td valign=top align=left class=label width=94>User ID</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form width=300><b>$usr_login</b></td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>User Type</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><b>$dspType</b>&nbsp;&nbsp;\n";
    $pg_body .= $inpLink . "</td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>Name</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=tmu_name size=30 value=\"$usr_name\" class=form></td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>Reset Password</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg1 class=form><input name=tmu_passwd size=10 class=form></td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
    $pg_body .= "<td valign=top align=left bgcolor=$color_bg2 class=form>\n";
    $pg_body .= "<input type=submit value=\"Update User\" class=form>&nbsp;\n";
    $pg_body .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?action=user_maint'\" class=form>&nbsp;\n";
    $pg_body .= "<input type=button value=\"DELETE\" onClick=\"parent.location.href='$cgi_url?action=del_user&tmu_id=$uID'\" class=form></td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</form>\n";    
    
    $pg_body .= "</td><td valign=top align=left width=214>\n";
    $pg_body .= "";
    $pg_body .= "</td></tr>\n";

    $pg_body .= "</table>\n";
    
    
    $env->{page_head}    = "Terminology Manager User Details";
    $env->{html_title}   = "$site_title - Terminology Manager User Details";
    $env->{page_body}    = $pg_body;
        
    $shared->page_gen( $env, $java_msg );

}

###########################################################                   
# Edit a user in the database.
#----------------------------------------------------------
sub edit_user {

    my $uID       = $shared->get_cgi( "tmu_id" );
    my $usr_name  = $shared->get_cgi( "tmu_name" );
    my $usr_pass  = $shared->get_cgi( "tmu_passwd" );
    my $usr_type  = $shared->get_cgi( "tmu_type" );
    my $lnkID     = $shared->get_cgi( "usr_link_id" );
    
    if ( $lnkID eq "" ) {
        $lnkID = 0;
    }
    
    if ( ( $usr_type eq "CLN" ) && ( $lnkID == 0 ) ) {
        $java_msg = "WARNING: Clients require a link to their specific profile.";
    }
    
    my $update    = "";
    if ( $usr_pass eq "" ) {
    	$update   = "UPDATE tm_user SET tmu_name = '$usr_name',\n"
    	          . "tmu_cln_id = $lnkID\n"
    	          . "WHERE tmu_id = $uID";
    } else {
    	$update   = "UPDATE tm_user SET tmu_name = '$usr_name',\n"
    	          . "tmu_passwd = '$usr_pass',\n"
    	          . "tmu_cln_id = $lnkID\n"
    	          . "WHERE tmu_id = $uID";
    }
    
    $shared->doSQLUpdate( $update );
    
    &user_maint;
    
}

###########################################################                   
# Delete a terminology management user
#----------------------------------------------------------
sub del_user {

    my $uID       = $shared->get_cgi( "tmu_id" );

    my $update    = "DELETE FROM tm_user WHERE tmu_id = $uID";
    $shared->doSQLUpdate( $update );
    
}

############################################################
# Form to import glossary info database
############################################################
sub import_form {

    my $selLangSrc = &getLang( "", "ENU" );
    my $selLangTgt = &getLang;

    $conf->{form_head}    = "Glossary Import";
    $conf->{form_start}   = "<form method=post name=adm_cln_form action=\"$cgi_url\" enctype=\"multipart/form-data\">\n"
    	                  . "<input type=hidden name=action value=\"import\">\n";
    
    $conf->{f_impfile}    = "<input type=file name=imp_file value=\"\" class=form>";
   	$conf->{f_src_lang}   = "<select name=tmm_src_lng_id class=form>" . $selLangSrc . "</select>";
   	$conf->{f_tgt_lang}   = "<select name=tmm_tgt_lng_id class=form>" . $selLangTgt . "</select>";
    $conf->{f_domain}     = "<select name=tmm_domain class=form>\n"
   	                      . "<option value=\"\"></option>" . $shared->pop_lookup_select( "TM_DOMAIN" ) . "</select>";
    $conf->{f_client}     = "<select name=tmm_cln_id class=form>" . &getCLNSelect . "</select>";

    $conf->{form_submit}  = "<input type=submit value=\"Process File\" class=form>&nbsp;"
                          . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url'\" class=form>&nbsp;&nbsp;"
                          . "<input type=button value=\"Undo Last Import\" onClick=\"parent.location.href='$cgi_url?action=import_undo'\" class=form>";
 
    $conf->{form_end}     = "</form>";
    
    my $pg_body = &disp_form( "tmadm_import" );

    $env->{page_body}     = $pg_body;
    $shared->page_gen( $env, $java_msg );

}

###########################################################
# Import glossary file
#----------------------------------------------------------
sub import {
    
    my $srcLng = $shared->get_cgi( "tmm_src_lng_id" );
    my $tgtLng = $shared->get_cgi( "tmm_tgt_lng_id" );
    my $domain = $shared->get_cgi( "tmm_domain" );
    my $cln_id = ( $shared->get_cgi( "tmm_cln_id" ) ) + 0;
    my $sqlNow = $shared->getSQLNow;
    my $retval = "GOOD";
    
    my $newFl  = "$imp_path/last_glossary.txt";
    my $status = $shared->uploadFile( "imp_file", $newFl, "" );
    if ( $status == 0 ) {
        
	    open ( FILE, $newFl ) or $retval = "ERROR";
	    my @FILE = <FILE>;
	    close( FILE );

        if ( $retval eq "GOOD" ) {
            foreach my $line ( @FILE ) {
                $line =~ s/[\n\r]+//g;
                my ( $t_src, $t_tgt, $t_src_cntxt, $t_tgt_cntxt ) = split( "\t", $line );

                $t_src =~ s/'/\\'/g;
                $t_tgt =~ s/'/\\'/g;
                $t_src_cntxt =~ s/'/\\'/g;
                $t_tgt_cntxt =~ s/'/\\'/g;
                
                $t_src_cntxt =~ s/^"//;
                $t_tgt_cntxt =~ s/^"//;
                $t_src_cntxt =~ s/"$//;
                $t_tgt_cntxt =~ s/"$//;
                
                my $insert = "INSERT INTO tm_main ( tmm_cln_id, tmm_domain, tmm_src_lng_id, tmm_tgt_lng_id,\n"
                           . "tmm_src_value, tmm_tgt_value, tmm_src_context, tmm_tgt_context, tmm_import_time )\n"
                           . "VALUES ( $cln_id, '$domain', $srcLng, $tgtLng, '$t_src', '$t_tgt', '$t_src_cntxt',\n"
                           . "'$t_tgt_cntxt', '$sqlNow' )";
                $shared->doSQLUpdate( $insert );

            }
        }
    }
    
    return $retval;        
    
}

###########################################################
# Undo last import
#----------------------------------------------------------
sub import_undo {
    
    my $max_rec = $shared->getResultRec( "SELECT MAX( tmm_import_time ) AS imp_time FROM tm_main" );
    my $tMax    = $max_rec->{imp_time};
    
    my $update  = "DELETE FROM tm_main WHERE tmm_import_time = '$tMax'";
    $shared->doSQLUpdate( $update );
    
    &print_main;
    
}

###########################################################
# Return a select list of linguists.
#----------------------------------------------------------
sub getCLNSelect {
    
    my ( $cur_val ) = @_;
    my $retval;
    
    if ( $cur_val gt "" ) {
        my $hash = $shared->getResultRec( "SELECT * FROM client WHERE cln_id = $cur_val" );
        my $tmA  = $hash->{cln_tm_access};
        my $name = $hash->{cln_name};
        if ( $tmA eq "N" ) {
            $retval .= "<option selected value=\"$cur_val\">$name</option>";
        }
    }
    
    my @rs         = $shared->getResultSet( "SELECT cln_id, cln_name FROM client WHERE cln_active = 'Y' AND cln_tm_access = 'Y' ORDER BY cln_name" );
    
    foreach my $rec ( @rs ) {
        my $code = $rec->{cln_id};
        my $name = $rec->{cln_name};
        $name   =~ s/'/\\'/g;
        if ( $cur_val == $code ) {
            $retval .= "<option selected value=\"$code\">$name</option>";
        } else {
            $retval .= "<option value=\"$code\">$name</option>";
        }
    }
    
    return $retval;
}

###########################################################                   
# Get Languages as selection list
#----------------------------------------------------------
sub getLang {

    my ( $cur_val, $dflt ) = @_;
        
    if ( ( $dflt gt "" ) && ( $cur_val eq "" ) ) {
        $cur_val = $dflt;
    }
    
    my $SQL = "SELECT * FROM language WHERE lng_tm_access = 'Y' ORDER BY lng_desc";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{lng_id};
        my $desc     = $rec->{lng_desc};
        if ( $cur_val == $code ) {
            $result .= "<option selected value=\"$code\">$desc</option>";
        } else {
            $result .= "<option value=\"$code\">$desc</option>";
        }
    }
    
    return $result;
}

