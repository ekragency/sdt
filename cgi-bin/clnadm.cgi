#!/usr/bin/perl
###########################################################################
# clnadm.cgi                Client Administration
###########################################################################
# Copyright 2013, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;
use Excel::Writer::XLSX;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;
use sdt;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );
my $sdt    = sdt->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/clnadm.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($temp_path)   = $config->get_temp_path;
my ($forms_path)  = $config->get_forms_path;
my ($forms_url)   = $config->get_forms_url;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $exceptList    = "";
my $dupList       = "";
my $debug         = $config->get_debug;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;


# Main program
my $usr_id = 0;
my $ct_adm = 0;
my $action = $shared->get_cgi( "action" );
my $hash_vars;
my $next_action = $shared->get_cgi( "next_act" );

$hash_vars->{srch_name}    = $shared->get_cgi( "srch_name" );
$hash_vars->{srch_code}    = $shared->get_cgi( "srch_code" );
$hash_vars->{srch_contact} = $shared->get_cgi( "srch_contact" );
$hash_vars->{next_act}     = $next_action;
$hash_vars->{sic_val}      = $shared->get_cgi( "sic_val" );


#
# Main Program
#
 
if ( $auth->validate_access( "clnadm" ) ) {
	
	$usr_id    = $auth->getCurrentUser;
	$ct_adm    = $auth->validate_access( "clntypeadm" );

    if ( $action eq "search_any" ) {
        &search_any;
    } elsif ( $action eq "change_sort" ) {
    	&change_sort;
    } elsif ( $action eq "edit_cln_form" ) {
        &edit_cln_form;
    } elsif ( $action eq "edit_cln" ) {
        &edit_cln;
    } elsif ( $action eq "add_cln_form" ) {
        &add_cln_form;
    } elsif ( $action eq "add_cln" ) {
        &add_cln;
    } elsif ( $action eq "del_cln" ) {
        &del_cln;
    } elsif ( $action eq "add_contact" ) {
    	&add_contact;
    } elsif ( $action eq "del_contact" ) {
    	&del_contact;
    } elsif ( $action eq "edit_contact_form" ) {
        &edit_contact_form;
    } elsif ( $action eq "edit_contact" ) {
        &edit_contact;
    } elsif ( $action eq "audit_log" ) {
    	&audit_log;
    } elsif ( $action eq "cln_rpt" ) {
        my $excelURL = &genClientRpt;
        $env->{open_file} = $excelURL;
        &search_any( "Y" );
    	
    } else {
        &search_any( "Y" );
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my ( $sicVal ) = @_;
    
    my $cln_cnt = $shared->getSQLCount( "SELECT count(*) FROM client" );

    if ( $conf->{client_cnt} eq "" ) {
        $conf->{client_cnt}     = $cln_cnt;
    }

  	my $sicChk    = "";
  	if ( $sicVal eq "Y" ) {
  	    $sicChk = " checked";
  	}
  	$conf->{show_inactive} = "<input type=checkbox name=sic_val value=\"Y\"" . $sicChk . ">";
  	
   	my $abContent = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=add_cln_form>\n"
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Add New Client\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";

 	$conf->{add_button}  = $abContent;
    
    my $pg_body              = &disp_form( "cln_main" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my ( $init_srch ) = @_;
	
	my $content;
	
	my $srch_name    = $hash_vars->{srch_name};
	my $srch_code    = $hash_vars->{srch_code};
	my $srch_contact = $hash_vars->{srch_contact};
	my $srch_head    = "<b>Searched:</b>&nbsp;";
	my $srch_par     = 0;
	my $sp_vars      = "";
	my $sic_val      = $hash_vars->{sic_val};
	if ( $sic_val eq "" ) {
	    $sic_val = "N";
	}
	
	if ( ( $sic_val gt "" ) || ( $srch_name gt "" ) || ( $srch_code gt "" ) || ( $srch_contact gt "" ) || ( $init_srch eq "Y" ) ) {
	
	    my $SQL;
	    if ( $ct_adm ) {
	        if ( $srch_contact gt "" ) {
    	        $SQL = "SELECT cln_id, cln_code, cln_name, cln_dsc_rate, cln_dsc_rate_2\n"
	                 . "FROM client_contact\n"
	                 . "INNER JOIN client ON clc_cln_id = cln_id\n"
	                 . "WHERE 1\n";
	        } else {
	    	    $SQL = "SELECT cln_id, cln_active, cln_code, cln_name, cln_dsc_rate, cln_dsc_rate_2\n"
	                 . "FROM client\n"
	                 . "WHERE 1\n";
	        }
	    } else {
	        if ( $srch_contact gt "" ) {
    	        $SQL = "SELECT cln_id, cln_code, cln_name, cln_dsc_rate, cln_dsc_rate_2\n"
	                 . "FROM client_contact\n"
	                 . "INNER JOIN client ON clc_cln_id = cln_id\n"
	                 . "WHERE ( cln_type = 'B' OR ( cln_type = 'P' AND cln_personal_usr_id = $usr_id ) )\n";
	        } else {
	    	    $SQL = "SELECT cln_id, cln_active, cln_code, cln_name, cln_dsc_rate, cln_dsc_rate_2\n"
	                 . "FROM client\n"
	                 . "WHERE ( cln_type = 'B' OR ( cln_type = 'P' AND cln_personal_usr_id = $usr_id ) )\n";
	        }	        
	    }
	    if ( $sic_val eq "N" ) {
	        $SQL .= "AND cln_active = 'Y'\n";
	    } else {
	        $srch_head .= "Showing Inactive Clients";
	    }
	    if ( $srch_name gt "" ) {
	    	my $cln_srch_name = $srch_name;
	    	$cln_srch_name =~ s/'/\\'/g;
		    $SQL .= "AND cln_name LIKE '$cln_srch_name%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Name starts with <b>$srch_name</b>";
		    } else {
		        $srch_head .= "Name starts with <b>$srch_name</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_name=$srch_name";
	    }
	    if ( $srch_code gt "" ) {
		    $SQL .= "AND cln_code LIKE '$srch_code%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Client ID starts with <b>$srch_code</b>";
		    } else {
		        $srch_head .= "Client ID starts with <b>$srch_code</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_code=$srch_code";
	    }
	    if ( $srch_contact gt "" ) {
	    	$SQL .= "AND clc_contact LIKE '%$srch_contact%'\n";
	    	if ( $srch_par > 0 ) {
	    		$srch_head .= ", Contact Name contains <b>$srch_contact</b>";
	    	} else {
	    		$srch_head .= "Contact Name contains <b>$srch_contact</b>";
	    	}
	    	$srch_par++;
	    	$sp_vars       .= "&srch_contact=$srch_contact";
	    }
	    if ( ( $init_srch eq "Y" ) && ( $srch_par == 0 ) ) {
	        if ( $ct_adm ) {
	    	    $SQL = "SELECT cln_id, cln_active, cln_code, cln_name, cln_dsc_rate, cln_dsc_rate_2\n"
	                 . "FROM client\n"
	                 . "WHERE 1\n";
	        } else {
	    	    $SQL = "SELECT cln_id, cln_active, cln_code, cln_name, cln_dsc_rate, cln_dsc_rate_2\n"
	                 . "FROM client\n"
	                 . "WHERE ( cln_type = 'B' OR ( cln_type = 'P' AND cln_personal_usr_id = $usr_id ) )\n";
	        }    
	        if ( $sic_val eq "N" ) {
	            $SQL .= "AND cln_active = 'Y'\n";
	        }
	    }
	    
	    $SQL     .= "ORDER BY cln_name";
	    
	    my $doReset = 0;
	    if ( ( $init_srch eq "Y" ) && ( $srch_par == 0 ) ) {
	        
	    	$srch_head = "All Clients";
	    	$sp_vars  .= "&srch_init=Y";
	    } else {
	        $doReset = 1;
	    }
	
	    my @rs   = $shared->getResultSet( $SQL );
	    my $row  = 0;
	    my $row_color;
	    my $fndCnt = 0;
	    
	    $content .= "<span class=subhead><b>$srch_head</b></span><br><br>\n";
	    if ( $doReset ) {
	    	$content .= "<a href=\"$cgi_url\" class=nav><b>[ Clear Filter ]</b></a><br>\n";
	    }
	    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	    $content .= "<tr bgcolor=\"black\">\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Client ID</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Client Name</b></font></td>\n";
	    $content .= "<td valign=top align=right class=form><font color=\"white\"><b>Discount Rate</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
	    foreach my $rec ( @rs ) {
	    	my $id      = $rec->{cln_id};
	    	my $code    = $rec->{cln_code};
	    	my $name    = $rec->{cln_name};
	    	my $dRate   = $rec->{cln_dsc_rate};
	    	my $dRate2  = $rec->{cln_dsc_rate_2};
	    	my $active  = $rec->{cln_active};
	    	
	    	my $dspCls  = "talist";
	    	if ( $active eq "N" ) {
	    	    $dspCls = "tilist";
	    	}
	    	
	    	my $dspDRate = "&nbsp;";
	    	if ( ( $dRate > 0 ) && ( $dRate2 > 0 ) ) {
	    	    $dspDRate = "$dRate\%/$dRate2\%";
	    	} elsif ( $dRate > 0 ) {
	    	    $dspDRate = "$dRate\%";
	    	}
	    	
	    	if ( $row == 0 ) {
	    		$row_color = $color_bg2;
	    		$row = 1;
	    	} else {
	    		$row_color = $color_bg1;
	    		$row = 0;
	    	}
	    	
	    	$content .= "<tr bgcolor=\"$row_color\">\n";
	    	$content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=edit_cln_form&cln_id=$id" . $sp_vars . "\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"View\" title=\"View $name\"></a></td>\n";
	    	$content .= "<td valign=top align=left class=$dspCls>$code</td>\n";
	    	$content .= "<td valign=top align=left class=$dspCls>$name</td>\n";
	    	$content .= "<td valign=top align=right class=$dspCls>$dspDRate</td>\n";
	    	$content .= "<td valign=top align=right class=form>";
    	    $content .= "<a href=\"$cgi_url?action=del_cln&cln_id=$id" . $sp_vars . "\" onClick=\"return confirm('Permanently remove $name from database?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete $name\"></a>";
	    	$content .= "</td></tr>\n";
	    	
	    	$fndCnt++;
	    }
	    $content .= "</table>\n";
	    		
	    if ( $fndCnt == 0 ) {
	    	$content  = "";
	    	$java_msg = "No Clients found meeting search criteria.";
	    } else {
	    	$conf->{client_cnt} = "$fndCnt";
	    }
	
    	$conf->{search_results} = $content;
	
	
    } else {
	
	    $java_msg = "No Search Criteria Specified.";        
	
    }

	&print_main( $sic_val );
	
}



############################################################
# Form to add member to database
############################################################
sub add_cln_form {

  	my $today = $shared->getSQLToday;

    my $pclnSel    = $sdt->getUserSelect( "0" );
    	
   	$conf->{form_head}            = "Add New Client";
   	$conf->{form_start}           = "<form method=post name=adm_cln_form action=\"$cgi_url\">\n"
    	                          . "<input type=hidden name=action value=\"add_cln\">\n";
   	$conf->{f_cln_name}           = "<input name=cln_name value=\"\" size=40 maxlength=60 class=form>&nbsp;"
   	                              . "<input type=checkbox name=cln_perm value=\"Y\" class=form checked> Permanent Client";
   	
    if ( $ct_adm ) {
   	    $conf->{f_cln_type}       = "<select id=cln_type name=cln_type class=form onchange=javascript:getPersonalUser()>" . $shared->pop_lookup_select( "CLN_TYPE", "B" ) . "</select>"
   	                              . "<span id=cln_puid></span>";
   	} else {
   	    $conf->{f_cln_type}       = "Business";
   	    $conf->{form_start}      .= "<input type=hidden name=cln_type value=\"B\">\n";
   	}
   	
   	$conf->{f_cln_dsc_rate}       = "<input name=cln_dsc_rate value=\"\" size=6 maxlength=5 class=form>";
   	$conf->{f_cln_dsc_rate_2}     = "<input name=cln_dsc_rate_2 value=\"\" size=6 maxlength=5 class=form>";
   	$conf->{f_cln_tr_rate}        = "<input name=cln_tr_rate value=\"\" size=7 maxlength=7 class=form>";
   	$conf->{f_cln_ed_rate}        = "<input name=cln_ed_rate value=\"\" size=7 maxlength=7 class=form>";
   	$conf->{f_cln_hr_rate}        = "<input name=cln_hr_rate value=\"\" size=7 maxlength=7 class=form>";
   	$conf->{f_cln_mtpe_rate}      = "<input name=cln_mtpe_rate value=\"\" size=7 maxlength=7 class=form>";
    $conf->{f_cln_tm_access}      = "<input type=checkbox name=cln_tm_access value=\"Y\" class=form>";
   	$conf->{f_cln_comment}        = "<textarea name=cln_comment cols=40 rows=7 wrap class=form></textarea>";
   	
   	$conf->{form_submit}          = "<input type=submit value=\"Add Now\" class=form>&nbsp;\n"
                                  . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}             = "</form>";
   	$conf->{form_name}            = "adm_cln_form";
   	$conf->{focus_field}          = "cln_name";  

    my $js = "<script type=\"text/javascript\">\n";
    $js   .= "function getPersonalUser(){\n";
    $js   .= "  var iTxt = '';\n";
    $js   .= "  var ct   = document.getElementById('cln_type').value;\n";
    $js   .= "  var fnd  = ct.match(/P/);\n";
    $js   .= "  if (fnd) {\n";
    $js   .= "     iTxt  = '&nbsp;<span class=label>User:</span><select name=cln_personal_usr_id class=form>" . $pclnSel . "</select>';\n";
    $js   .= "  }\n";
    $js   .= "  document.getElementById('cln_puid').innerHTML = iTxt;\n";
    $js   .= "}\n";
    $js   .= "</script>\n";
    	
    my $pg_body = &disp_form( "cln_add_form" );

    $env->{javascript}       = $js;
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

############################################################
# Add the record to the loan database
############################################################
sub add_cln {

   	my $cln_name  = $shared->get_cgi( "cln_name" );
   	my $cln_perm  = $shared->get_cgi( "cln_perm" );
   	my $cln_type  = $shared->get_cgi( "cln_type" );
   	my $dRate     = ( $shared->get_cgi( "cln_dsc_rate" ) ) + 0;
   	my $dRate2    = ( $shared->get_cgi( "cln_dsc_rate_2" ) ) + 0;
   	my $tRate     = ( $shared->get_cgi( "cln_tr_rate" ) ) + 0;
   	my $eRate     = ( $shared->get_cgi( "cln_ed_rate" ) ) + 0;
   	my $hRate     = ( $shared->get_cgi( "cln_hr_rate" ) ) + 0;
   	my $mRate     = ( $shared->get_cgi( "cln_mtpe_rate" ) ) + 0;
   	my $tmAccess  = $shared->get_cgi( "cln_tm_access" );
   	my $comment   = $shared->get_cgi( "cln_comment" );
   	
   	if ( $cln_perm eq "" ) {
   	    $cln_perm = "N";
   	}
   	
   	if ( $tmAccess eq "" ) {
   	    $tmAccess = "N";
   	}
   	
   	my $newCode   = $shared->getSQLCount( "SELECT MAX( cln_code ) FROM client WHERE cln_code < '999'" );
   	$newCode++;
    	
   	$cln_name    =~ s/'/\\'/g;
   	$comment     =~ s/'/\\'/g;

    my $clnCnt   = $shared->getSQLCount( "SELECT COUNT(*) FROM client WHERE cln_name = '$cln_name'" );
    if ( $clnCnt == 0 ) {
    	
        my $insert   = "INSERT INTO client ( cln_code, cln_name, cln_type, cln_dsc_rate, cln_dsc_rate_2, cln_tr_rate, cln_ed_rate, cln_hr_rate,\n"
                     . "cln_mtpe_rate, cln_tm_access, cln_comment, cln_perm )\n"
                     . "VALUES ( '$newCode', '$cln_name', '$cln_type', $dRate, $dRate2, $tRate, $eRate,\n"
                     . "$hRate, $mRate, '$tmAccess', '$comment', '$cln_perm' )";
        $shared->doSQLUpdate( $insert );
    	
  	    my $cln_id   = $shared->getSQLLastInsertID;
  	    
  	    my $name_ext = "";
  	    if ( $cln_perm eq "Y" ) {
  	        $name_ext = " [Permanent]";
  	    }
  	    
   	    &add_audit( $cln_id, "New Client Record Created: $cln_name" . $name_ext . "." );
    	
   	    &edit_cln_form( $cln_id );
   	} else {
   	    $java_msg = "Client $cln_name Already Exists";
   	    &search_any( "Y" );
   	}

}

############################################################
# Form to edit member of database
############################################################
sub edit_cln_form {

    my ( $cln_id ) = @_;
    
    if ( $cln_id eq "" ) {
        $cln_id  = $shared->get_cgi( "cln_id" );
    }
    
    my $cln_rec = $shared->get_rec( "client", "cln_id", $cln_id );

    my $clnCode    = $cln_rec->{cln_code};
    my $clnName    = $cln_rec->{cln_name};
    my $clnPerm    = $cln_rec->{cln_perm};
    my $clnType    = $cln_rec->{cln_type};
    my $dRate      = $cln_rec->{cln_dsc_rate};
    my $dRate2     = $cln_rec->{cln_dsc_rate_2};
    my $tRate      = $cln_rec->{cln_tr_rate};
    my $eRate      = $cln_rec->{cln_ed_rate};
    my $hRate      = $cln_rec->{cln_hr_rate};
    my $mRate      = $cln_rec->{cln_mtpe_rate};
    my $tmAccess   = $cln_rec->{cln_tm_access};
   	my $comment    = $cln_rec->{cln_comment};
    my $active     = $cln_rec->{cln_active};
    my $clnOth     = $cln_rec->{cln_other};
    my $clnPUID    = $cln_rec->{cln_personal_usr_id};
    
    my $pclnSel    = $sdt->getUserSelect( $clnPUID );
    
    my $tmChk      = "";
    if ( $tmAccess eq "Y" ) {
        $tmChk     = " checked";
    }
    
    my $permChk    = "";
    if ( $clnPerm eq "Y" ) {
        $permChk   = " checked";
    }
    
   	$comment      =~ s/\"/&quot;/g;
   	$comment      =~ s/\</&gt;/g;
   	
    
	my $srch_name    = $hash_vars->{srch_name};
	my $srch_code    = $hash_vars->{srch_code};
	my $srch_contact = $hash_vars->{srch_contact};
	
    my $inp_srch     = "";
    my $sp_vars      = "";
    if ( $srch_name gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_name value=\"$srch_name\">\n";
    	$sp_vars  .= "&srch_name=$srch_name";
    }
    if ( $srch_code gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_code value=\"$srch_code\">\n";
    	$sp_vars  .= "&srch_code=$srch_code";
    }
    if ( $srch_contact gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_contact value=\"$srch_contact\">\n";
    	$sp_vars  .= "&srch_contact=$srch_contact";
    }
    if ( $inp_srch eq "" ) {
    	$inp_srch .= "<input type=hidden name=srch_init value=\"Y\">\n";
    	$sp_vars  .= "&srch_init=Y";
    }

    my $actChk = "";
    if ( $active eq "Y" ) {
        $actChk = " checked";
    } 

    $sp_vars =~ s/^\&//;
    
    $conf->{form_id_row}          = "<tr><td align=right valign=top class=label bgcolor=$color_bg2 width=150>Client ID</td>\n"
                                  . "<td valign=center align=left class=form><b>$clnCode</b>&nbsp;&nbsp;\n";
    if ( $clnOth eq "N" ) {
        $conf->{form_id_row}     .= "<input type=checkbox name=cln_active value=\"Y\" class=form" . $actChk . "> Active</td></tr>\n";
    } else {
        $conf->{form_id_row}     .= "</td></tr>\n";
    }

    
   	$conf->{form_head}            = "Update Client Details";
   	$conf->{form_start}           = "<form method=post name=cln_edit_form action=\"$cgi_url\">\n"
   	                              . "<input type=hidden name=action value=\"edit_cln\">\n"
   	                              . "<input type=hidden name=cln_id value=\"$cln_id\">\n"
   	                              . $inp_srch;
   	if ( $clnOth eq "N" ) {
   	    $conf->{f_cln_name}       = "<input name=cln_name value=\"" . $cln_rec->{cln_name} . "\" size=40 maxlength=60 class=form>&nbsp;"
   	                              . "<input type=checkbox name=cln_perm value=\"Y\" class=form" . $permChk . "> Permanent Client";
   	    if ( $ct_adm ) {
   	        $conf->{f_cln_type}       = "<select id=cln_type name=cln_type class=form onchange=javascript:getPersonalUser()>" . $shared->pop_lookup_select( "CLN_TYPE", $clnType ) . "</select>";
   	        if ( $clnType eq "P" ) {
   	            $conf->{f_cln_type}  .= "<span id=cln_puid>&nbsp;<span class=label>User:</span><select name=cln_personal_usr_id class=form>" . $pclnSel . "</select></span>";
            } else {
                $conf->{f_cln_type}  .= "<span id=cln_puid></span>";
            }
   	    } else {
   	        my $dspType   = "Business";
   	        my $dspPUser  = "";
   	        if ( $clnType eq "P" ) {
   	            $dspType  = "Personal";
   	            $dspPUser = $shared->getUserName( $clnPUID );
     	        $conf->{f_cln_type}   = $dspType . "&nbsp;(" . $dspPUser . ")";
   	        } else {
   	            $conf->{f_cln_type}   = $dspType;
   	        }
   	        $conf->{form_start}  .= "<input type=hidden name=cln_type value=\"$clnType\">\n"
   	                              . "<input type=hidden name=cln_personal_usr_id value=\"$clnPUID\">\n";
   	    }
   	} else {
   	    $conf->{f_cln_name}       = $cln_rec->{cln_name};
   	    $conf->{f_cln_type}       = "Generic Client";
   	}
   	
   	$conf->{f_cln_dsc_rate}       = "<input name=cln_dsc_rate value=\"$dRate\" size=6 maxlength=5 class=form>";
   	$conf->{f_cln_dsc_rate_2}     = "<input name=cln_dsc_rate_2 value=\"$dRate2\" size=6 maxlength=5 class=form>";
   	$conf->{f_cln_tr_rate}        = "<input name=cln_tr_rate value=\"$tRate\" size=7 maxlength=7 class=form>";
   	$conf->{f_cln_ed_rate}        = "<input name=cln_ed_rate value=\"$eRate\" size=7 maxlength=7 class=form>";
   	$conf->{f_cln_hr_rate}        = "<input name=cln_hr_rate value=\"$hRate\" size=7 maxlength=7 class=form>";
   	$conf->{f_cln_mtpe_rate}      = "<input name=cln_mtpe_rate value=\"$mRate\" size=7 maxlength=7 class=form>";
   	$conf->{f_cln_tm_access}      = "<input type=checkbox name=cln_tm_access value=\"Y\" class=form" . $tmChk . ">";
   	$conf->{f_cln_comment}        = "<textarea name=cln_comment cols=80 rows=7 wrap class=form>$comment</textarea>";

    	
   	$conf->{form_submit}  = "<table border=0 cellpadding=0 cellspacing=0 width=100%><tr><td valign=top align=left class=form>\n"
   	                      . "<input type=submit value=\"Update Client\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?$sp_vars'\" class=form></td>"
                          . "<td valign=top align=right class=form>\n"
                          . "<input type=button value=\"Audit Log\" onClick=\"parent.location.href='$cgi_url?action=audit_log&cln_id=$cln_id&$sp_vars'\" class=form></td></tr>\n"
                          . "</table>";
   	$conf->{form_end}     = "</form>";
    	
    
    my $fndCnt  = 0;
    
    my $content = "";
    
    if ( $clnOth eq "N" ) {
        
   	    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=836>\n";
   	
        $content .= "<form method=post name=contact_add_form action=\"$cgi_url\">\n";
        $content .= "<input type=hidden name=action value=\"add_contact\">\n";
        $content .= "<input type=hidden name=cln_id value=\"$cln_id\">\n";
        $content .= $inp_srch;

        $content .= "<tr><td valign=top align=left class=overview colspan=9><font color=\"black\">Contact Information</font></td></tr>\n";
	    $content .= "<tr bgcolor=\"black\">\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Contact</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Address</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Phone</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>E-mail</b></font></td>\n";
        $content .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
  	    $content .= "<tr bgcolor=\"$color_bg1\"><td valign=top align=left class=form>\n";
   	    $content .= "<input name=clc_contact value=\"\" size=20 maxlength=40 class=form></td>\n";
   	    $content .= "<td valign=top align=left class=form>\n";
   	    $content .= "<span class=label>Address:</span>&nbsp;<input name=clc_address value=\"\" size=40 maxlength=60 class=form><br>\n";
   	    $content .= "<span class=label>City:</span>&nbsp;<input name=clc_city value=\"\" size=20 maxlength=40 class=form>&nbsp;&nbsp;\n";
   	    $content .= "<span class=label>State:</span>&nbsp;<input name=clc_state value=\"\" size=5 maxlength=2 class=form>&nbsp;&nbsp;\n";
   	    $content .= "<span class=label>Zip:</span>&nbsp;<input name=clc_zip value=\"\" size=10 maxlength=20 class=form><br>\n";
   	    $content .= "<span class=label>Country:</span>&nbsp;<input name=clc_country value=\"\" size=20 maxlength=20 class=form></td>\n";
   	    $content .= "<td valign=top align=left class=form>\n";
   	    $content .= "<input name=clc_phone value=\"\" size=15 maxlength=30 class=form></td>\n";
   	    $content .= "<td valign=top align=left class=form>\n";
   	    $content .= "<input name=clc_email value=\"\" size=20 maxlength=80 class=form></td>\n";
   	    $content .= "<td valign=top align=right class=form><input type=submit value=\"Add\" class=form></td></tr>\n";

        my $row = 0;
        my $row_color;
        my $anyNoUp = 0;
    	
	    my $SQL = "SELECT clc_id, clc_contact, clc_address, clc_city, clc_state,\n"
	            . "clc_zip, clc_country, clc_phone, clc_email\n"
	            . "FROM client_contact\n"
	            . "WHERE clc_cln_id = $cln_id\n"
	            . "ORDER BY clc_contact";
	    my @rs  = $shared->getResultSet( $SQL );
	    foreach my $rec ( @rs ) {
	    	my $id      = $rec->{clc_id};
	 	    my $contact = $rec->{clc_contact};
	 	    my $addr    = $rec->{clc_address};
	 	    my $city    = $rec->{clc_city};
	 	    my $state   = $rec->{clc_state};
	 	    my $zip     = $rec->{clc_zip};
	 	    my $cntry   = $rec->{clc_country};
	 	    my $phone   = $rec->{clc_phone};
	 	    my $email   = $rec->{clc_email};
	   	
	      	my $eLink   = "N/A";
	   	    if ( $email gt "" ) {
	   	        $eLink  = "<a href=\"mailto:$email\" class=nav>$email</a>";
	   	    }
	   	
	   	    if ( $row == 0 ) {
	   		    $row_color = $color_bg2;
	   		    $row = 1;
	   	    } else {
	   		    $row_color = $color_bg1;
	   		    $row = 0;
	   	    }
	
	   	    $content .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form>$contact</td>\n";
	   	    $content .= "<td valign=top align=left class=form>$addr<br>$city, $state&nbsp;&nbsp;$zip<br>$cntry</td>\n";
	   	    $content .= "<td valign=top align=left class=form>$phone</td>\n";
	   	    $content .= "<td valign=top align=left class=form>$eLink</td>\n";
   	        $content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=edit_contact_form&cln_id=$cln_id&clc_id=$id&$sp_vars\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"Edit Contact\" title=\"Edit $contact\"></a>\n";
   	        $content .= "&nbsp;&nbsp;<a href=\"$cgi_url?action=del_contact&clc_id=$id&cln_id=$cln_id&$sp_vars\" onClick=\"return confirm('Permanently remove Contact from database?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete $contact\"></a></tr>\n";
	    	
	  	    $fndCnt++;
	    }
	
	    if ( $fndCnt == 0 ) {
	  	    $content  .= "<tr><td valign=top align=left colspan=5 class=form>No Contacts found for Client</td></tr>\n";
	    }
	
   	    $content .= "</form>\n";
	
	    $content .= "</table>\n";
	}
	
    $conf->{contact_info} = $content;
    
    my $js = "<script type=\"text/javascript\">\n";
    $js   .= "function getPersonalUser(){\n";
    $js   .= "  var iTxt = '';\n";
    $js   .= "  var ct   = document.getElementById('cln_type').value;\n";
    $js   .= "  var fnd  = ct.match(/P/);\n";
    $js   .= "  if (fnd) {\n";
    $js   .= "     iTxt  = '&nbsp;<span class=label>User:</span><select name=cln_personal_usr_id class=form>" . $pclnSel . "</select>';\n";
    $js   .= "  }\n";
    $js   .= "  document.getElementById('cln_puid').innerHTML = iTxt;\n";
    $js   .= "}\n";
    $js   .= "</script>\n";
    	
    my $pg_body = &disp_form( "cln_edit_form" );

    $env->{javascript}       = $js;
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

############################################################
# Update the record to the members database
############################################################
sub edit_cln {

  	my $srch_init = $shared->get_cgi( "srch_init" );
  	my $next_act  = $shared->get_cgi( "next_act" );

    my $cln_id    = $shared->get_cgi( "cln_id" );
   	my $cln_name  = $shared->get_cgi( "cln_name" );
   	my $cln_perm  = $shared->get_cgi( "cln_perm" );
   	my $cln_type  = $shared->get_cgi( "cln_type" );
   	my $cln_puid  = ( $shared->get_cgi( "cln_personal_usr_id" ) ) + 0;
   	my $dRate     = ( $shared->get_cgi( "cln_dsc_rate" ) ) + 0;
   	my $dRate2    = ( $shared->get_cgi( "cln_dsc_rate_2" ) ) + 0;
   	my $tRate     = ( $shared->get_cgi( "cln_tr_rate" ) ) + 0;
   	my $eRate     = ( $shared->get_cgi( "cln_ed_rate" ) ) + 0;
   	my $hRate     = ( $shared->get_cgi( "cln_hr_rate" ) ) + 0;
   	my $mRate     = ( $shared->get_cgi( "cln_mtpe_rate" ) ) + 0;
   	my $tmAccess  = $shared->get_cgi( "cln_tm_access" );
   	my $comment   = $shared->get_cgi( "cln_comment" );
   	my $active    = $shared->get_cgi( "cln_active" );
   	
   	if ( $active eq "" ) {
   	    $active = "N";
   	}
   	if ( $cln_perm eq "" ) {
   	    $cln_perm = "N";
   	}
   	
   	if ( $tmAccess eq "" ) {
   	    $tmAccess = "N";
   	}
    	
   	my $cur_rec     = $shared->get_rec( "client", "cln_id", $cln_id );
   	my $p_cln_name  = $cur_rec->{cln_name};
   	my $p_cln_perm  = $cur_rec->{cln_perm};
   	my $p_cln_type  = $cur_rec->{cln_type};
   	my $p_dRate     = $cur_rec->{cln_dsc_rate};
   	my $p_dRate2    = $cur_rec->{cln_dsc_rate_2};
    my $p_tRate     = $cur_rec->{cln_tr_rate};
    my $p_eRate     = $cur_rec->{cln_ed_rate};
    my $p_hRate     = $cur_rec->{cln_hr_rate};
    my $p_mRate     = $cur_rec->{cln_mtpe_rate};
    my $p_tmAccess  = $cur_rec->{cln_tm_access};
    my $p_comment   = $cur_rec->{cln_comment};
    my $p_active    = $cur_rec->{cln_active};
    my $clnOth      = $cur_rec->{cln_other};
       	
   	my $chg_dtls = "";
   	
   	if ( $clnOth eq "N" ) {
   	    if ( $active ne $p_active ) {
   	        $chg_dtls .= "Status: from [Active = $p_active] to [Active = $active]<br>\n";
   	    }
   	
   	    if ( $cln_name ne $p_cln_name ) {
   		    $chg_dtls .= "Client Name: from [$p_cln_name] to [$cln_name]<br>\n";
   	    }
   	    
   	    if ( $cln_perm ne $p_cln_perm ) {
   	        $chg_dtls .= "Perm Client: from [$p_cln_perm] to [$cln_perm]<br>\n";
   	    }
   	
   	    if ( $cln_type ne $p_cln_type ) {
   	        $chg_dtls .= "Client Type: from [$p_cln_type] to [$cln_type]<br>\n";
   	    }
   	}
   	
   	if ( $dRate != $p_dRate ) {
   	    $chg_dtls .= "Discount Rate - Tier 1: from [$p_dRate\%] to [$dRate\%]<br>\n";
   	}

   	if ( $dRate2 != $p_dRate2 ) {
   	    $chg_dtls .= "Discount Rate - Tier 2: from [$p_dRate2\%] to [$dRate2\%]<br>\n";
   	}
   	
   	if ( $tRate != $p_tRate ) {
   	    $chg_dtls .= "Client Translation Rate: from [$p_tRate] to [$tRate]<br>\n";
   	}
   	
   	if ( $eRate != $p_eRate ) {
   	    $chg_dtls .= "Client Review Rate: from [$p_eRate] to [$eRate]<br>\n";
   	}

   	if ( $hRate != $p_hRate ) {
   	    $chg_dtls .= "Client Hourly Rate: from [$p_hRate] to [$hRate]<br>\n";
   	}
   	
   	if ( $mRate != $p_mRate ) {
   	    $chg_dtls .= "Client MTPE Rate: from [$p_mRate] to [$mRate]<br>\n";
   	}
   	
   	if ( $tmAccess ne $p_tmAccess ) {
   	    $chg_dtls .= "Term Mgr Access: from ($p_tmAccess] to [$tmAccess]<br>\n";
   	}
   	
    if ( $comment ne $p_comment ) {
        $chg_dtls .= "Comment: from [$p_comment] to [$comment]<br>\n";
    }

   	if ( $chg_dtls gt "" ) {
   		chomp( $chg_dtls );
   		$chg_dtls =~ s/<br>$//;
   	}
    	
   	$cln_name    =~ s/'/\\'/g;
   	$comment     =~ s/'/\\'/g;
   	
   	my $update   = "UPDATE client SET cln_active = '$active', cln_name = '$cln_name', cln_perm = '$cln_perm',\n"
   	             . "cln_type = '$cln_type', cln_personal_usr_id = $cln_puid, cln_dsc_rate = $dRate,\n"
   	             . "cln_dsc_rate_2 = $dRate2, cln_tr_rate = $tRate, cln_ed_rate = $eRate,\n"
   	             . "cln_hr_rate = $hRate, cln_mtpe_rate = $mRate, cln_tm_access = '$tmAccess', cln_comment = '$comment'\n"
   	             . "WHERE cln_id = $cln_id";
   	if ( $clnOth eq "Y" ) {
   	    $update   = "UPDATE client SET cln_dsc_rate = $dRate,\n"
   	              . "cln_dsc_rate_2 = $dRate2, cln_tr_rate = $tRate, cln_ed_rate = $eRate,\n"
   	              . "cln_hr_rate = $hRate, cln_mtpe_rate = $mRate, cln_tm_access = '$tmAccess', cln_comment = '$comment'\n"
   	              . "WHERE cln_id = $cln_id";
   	}
   	
   	$shared->doSQLUpdate( $update );
    	
   	if ( $chg_dtls gt "" ) {
   		&add_audit( $cln_id, "Client Record Updated...<br>\n" . $chg_dtls );
   	}
    	
    $env->{doFade} = "Y";
    $java_msg      = "Client Record Updated...";
    &edit_cln_form( $cln_id );

}

###########################################################                   
# Delete Member from Database
#----------------------------------------------------------
sub del_cln {

   	my $srch_init = $shared->get_cgi( "srch_init" );
   	my $next_act  = $shared->get_cgi( "next_act" );

   	my $cln_id = $shared->get_cgi( "cln_id" );
   	my $clnOth = $shared->getFieldVal( "client", "cln_other", "cln_id", $cln_id );
   	my $po_cnt = $shared->getSQLCount( "SELECT COUNT(*) FROM po_log WHERE pol_cln_id = $cln_id" );
   	my $ut_cnt = $shared->getSQLCount( "SELECT COUNT(*) FROM user WHERE usr_type = 'CLN' AND usr_link_id = $cln_id" );
   	
   	if ( $clnOth eq "N" ) {
        if ( $po_cnt == 0 ) {
            if ( $ut_cnt == 0 ) {	
   	            my $cln_rec = $shared->get_rec( "client", "cln_id", $cln_id );
   	            my $desc    = "Deleting Client (" . $cln_rec->{cln_code} . " - " . $cln_rec->{cln_name} . ")";
   	            &add_audit( $cln_id, $desc );
    	
   	            my $update = "DELETE FROM client WHERE cln_id = $cln_id";
   	            $shared->doSQLUpdate( $update );
    	
   	            my $up2    = "DELETE FROM client_contact WHERE clc_cln_id = $cln_id";
   	            $shared->doSQLUpdate( $up2 );
   	        } else {
   	            $java_msg = "Unable to Delete Client - Removed Linked User First";
   	        }
        } else {
            $java_msg = "Unable to Delete Client - Remove Associated POs First";
        }
    } else {
        $java_msg = "Unable to Delete System Required Record.";
    }
    
    &search_any( $srch_init );
}


###########################################################
# View Audit Log for a specific application
#----------------------------------------------------------
sub audit_log {

    my $cln_id  = $shared->get_cgi( "cln_id" );
    my $cln_rec = $shared->get_rec( "client", "cln_id", $cln_id );
    my $name    = $cln_rec->{cln_name};
    
    my $SQL     = "SELECT usr_name, cla_date, cla_description\n"
                . "FROM client_audit\n"
                . "INNER JOIN user ON cla_usr_id = usr_id\n"
                . "WHERE cla_cln_id = $cln_id\n"
                . "ORDER BY cla_date DESC";
                
	my $content;
    $content .= "<span class=subhead>Client: $name</span><br><br>\n";
    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
    $content .= "<tr bgcolor=\"black\">\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Log Date</b></font></td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>User</b></font></td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Description</b></font></td></tr>\n";
    
    my @rs    = $shared->getResultSet( $SQL );
    my $row  = 0;
    my $row_color;

	foreach my $rec ( @rs ) {
	   	my $log_date = $shared->getDateFmt( $rec->{cla_date} );
	   	my $uName    = $rec->{usr_name};
	   	my $desc     = $rec->{cla_description};
	    	
    	if ( $row == 0 ) {
    		$row_color = $color_bg2;
    		$row = 1;
    	} else {
    		$row_color = $color_bg1;
    		$row = 0;
    	}
	    	
    	$content .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form>$log_date</td>\n";
	    $content .= "<td valign=top align=left class=form>$uName</td>\n";
	    $content .= "<td valign=top align=left class=form>$desc</td></tr>\n";
	    
    }
    $content .= "</table>\n";

   	$conf->{search_results} = $content;
   	
    my $pg_body = &disp_form( "cln_audit" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
   	
}


###########################################################
# Add Audit Log Record
#----------------------------------------------------------
sub add_audit {
	
	my ( $id, $desc ) = @_;
	
	$desc =~ s/'/\\'/g;
	
	my $insert = "INSERT INTO client_audit ( cla_cln_id, cla_usr_id, cla_date, cla_description )\n"
	           . "VALUES ( $id, $usr_id, now(), '$desc' )";
	$shared->doSQLUpdate( $insert );
	
}

############################################################
# Add the contact
############################################################
sub add_contact {

    my $cln_id = $shared->get_cgi( "cln_id" );
    
   	my $contact   = $shared->get_cgi( "clc_contact" );
   	my $addr      = $shared->get_cgi( "clc_address" );
   	my $city      = $shared->get_cgi( "clc_city" );
   	my $state     = $shared->get_cgi( "clc_state" );
   	my $zip       = $shared->get_cgi( "clc_zip" );
   	my $cntry     = $shared->get_cgi( "clc_country" );
   	my $phone     = $shared->get_cgi( "clc_phone" );
   	my $email     = $shared->get_cgi( "clc_email" );
   	
   	my $audit_contact = $contact;

    $contact =~ s/'/\\'/g;
    $addr    =~ s/'/\\'/g;
    $city    =~ s/'/\\'/g;
        	
    my $insert   = "INSERT INTO client_contact ( clc_cln_id, clc_contact, clc_address, clc_city,\n"
                 . "clc_state, clc_zip, clc_country, clc_phone, clc_email )\n"
                 . "VALUES\n"
                 . "( $cln_id, '$contact', '$addr', '$city', '$state', '$zip', '$cntry', '$phone', '$email' )";
    $shared->doSQLUpdate( $insert );
   	        
    &add_audit( $cln_id, "Contact Added - [" . $audit_contact . "]" );
   	        
    &edit_cln_form( $cln_id );

}

############################################################
# Remove the Contact
############################################################
sub del_contact {

    my $cln_id = $shared->get_cgi( "cln_id" );
    
   	my $clc_id    = $shared->get_cgi( "clc_id" );
   	my $clc_rec   = $shared->get_rec( "client_ccontact", "clc_id", $clc_id );
    	
    my $qteCnt    = $shared->getSQLCount( "SELECT COUNT(*) FROM quote WHERE qte_clc_id = $clc_id" );
    my $prjCnt    = $shared->getSQLCount( "SELECT COUNT(*) FROM project WHERE prj_clc_id = $clc_id" );
    my $delOk     = 1;
    if ( $qteCnt > 0 ) {
        $java_msg = "Unable to delete contact record - it is currently being used in Quote.";
        $delOk    = 0;
    }
    if ( $prjCnt > 0 ) {
        $java_msg = "Unable to delete contact record - it is currently being used in project.";
        $delOk    = 0;
    }
    if ( $delOk ) {
   	    my $contact   = $clc_rec->{clc_contact};
    	
        my $update    = "DELETE FROM client_contact WHERE clc_id = $clc_id";
        $shared->doSQLUpdate( $update );
        
        my $log_desc  = "Contact Record Removed - [$contact]";
   	                  
        &add_audit( $cln_id, $log_desc );
    }
    
    &edit_cln_form( $cln_id );

}

############################################################
# Form to edit contact record
############################################################
sub edit_contact_form {

    my $cln_id = $shared->get_cgi( "cln_id" );
    my $clc_id = $shared->get_cgi( "clc_id" );
    
    my $cln_rec = $shared->get_rec( "client", "cln_id", $cln_id );
    my $clc_rec = $shared->get_rec( "client_contact", "clc_id", $clc_id );
    
    my $clnName    = $cln_rec->{cln_name};
    my $contact    = $clc_rec->{clc_contact};
    my $addr       = $clc_rec->{clc_address};
    my $city       = $clc_rec->{clc_city};
    my $state      = $clc_rec->{clc_state};
    my $zip        = $clc_rec->{clc_zip};
    my $country    = $clc_rec->{clc_country};
    my $phone      = $clc_rec->{clc_phone};
    my $email      = $clc_rec->{clc_email};

	my $srch_name    = $hash_vars->{srch_name};
	my $srch_code    = $hash_vars->{srch_code};
	my $srch_contact = $hash_vars->{srch_contact};
	
    my $inp_srch     = "";
    my $sp_vars      = "";
    if ( $srch_name gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_name value=\"$srch_name\">\n";
    	$sp_vars  .= "&srch_name=$srch_name";
    }
    if ( $srch_code gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_code value=\"$srch_code\">\n";
    	$sp_vars  .= "&srch_code=$srch_code";
    }
    if ( $srch_contact gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_contact value=\"$srch_contact\">\n";
    	$sp_vars  .= "&srch_contact=$srch_contact";
    }
    if ( $inp_srch eq "" ) {
    	$inp_srch .= "<input type=hidden name=srch_init value=\"Y\">\n";
    	$sp_vars  .= "&srch_init=Y";
    }
    $sp_vars =~ s/^\&//;
    
   	$conf->{form_head}            = "Update Contact Details";
   	$conf->{form_start}           = "<form method=post name=cln_clc_form action=\"$cgi_url\">\n"
   	                              . "<input type=hidden name=action value=\"edit_contact\">\n"
   	                              . "<input type=hidden name=cln_id value=\"$cln_id\">\n"
   	                              . "<input type=hidden name=clc_id value=\"$clc_id\">\n"
   	                              . $inp_srch;

   	$conf->{f_cln_name}           = $clnName;
   	$conf->{f_contact}            = "<input name=clc_contact value=\"$contact\" size=40 maxlength=80 class=form>";
   	$conf->{f_address}            = "<input name=clc_address value=\"$addr\" size=40 maxlength=80 class=form>";
   	$conf->{f_city}               = "<input name=clc_city value=\"$city\" size=30 maxlength=60 class=form>";
   	$conf->{f_state}              = "<input name=clc_state value=\"$state\" size=5 maxlength=5 class=form>";
   	$conf->{f_zip}                = "<input name=clc_zip value=\"$zip\" size=15 maxlength=20 class=form>";
   	$conf->{f_country}            = "<input name=clc_country value=\"$country\" size=15 maxlength=20 class=form>";
   	$conf->{f_phone}              = "<input name=clc_phone value=\"$phone\" size=20 maxlength=40 class=form>";
   	$conf->{f_email}              = "<input name=clc_email value=\"$email\" size=40 maxlength=80 class=form>";
    	
   	$conf->{form_submit}  = "<input type=submit value=\"Update Contact\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?action=edit_cln_form&cln_id=$cln_id&" . $sp_vars . "'\" class=form>";
   	$conf->{form_end}     = "</form>";
    
    my $pg_body = &disp_form( "cln_clc_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

############################################################
# Update contact record
############################################################
sub edit_contact {

    my $cln_id    = $shared->get_cgi( "cln_id" );
    my $clc_id    = $shared->get_cgi( "clc_id" );

    my $contact   = $shared->get_cgi( "clc_contact" );
    my $addr      = $shared->get_cgi( "clc_address" );
    my $city      = $shared->get_cgi( "clc_city" );
    my $state     = $shared->get_cgi( "clc_state" );
    my $zip       = $shared->get_cgi( "clc_zip" );
    my $country   = $shared->get_cgi( "clc_country" );
    my $phone     = $shared->get_cgi( "clc_phone" );
    my $email     = $shared->get_cgi( "clc_email" );
    
   	$contact     =~ s/'/\\'/g;
   	$addr        =~ s/'/\\'/g;
   	$city        =~ s/'/\\'/g;
   	
   	my $update   = "UPDATE client_contact SET clc_contact = '$contact', clc_address = '$addr',\n"
   	             . "clc_city = '$city', clc_state = '$state', clc_zip = '$zip', clc_country = '$country',\n"
   	             . "clc_phone = '$phone', clc_email = '$email'\n"
   	             . "WHERE clc_id = $clc_id";
   	
   	$shared->doSQLUpdate( $update );
    	
    &edit_cln_form( $cln_id );

}


############################################################
# Generate Client/Contact extract in Excel Format
############################################################
sub genClientRpt {

    my $rptDate  = $shared->getSQLToday;
    
	my $fName    = "$forms_path/sdt_client_rpt.xlsx";
	my $urlName  = "$forms_url/sdt_client_rpt.xlsx";
	my $nowDesc  = $shared->getDateDesc( $rptDate, 1, 1 );
	
	my $workbook = Excel::Writer::XLSX->new( $fName );
	$workbook->set_tempdir( $temp_path );
	
	my $c_litegrey    = $workbook->set_custom_color( 40, 204, 204, 204 );
	my $c_litegreen   = $workbook->set_custom_color( 41, 210, 255, 214 );
    my %headFont      = ( font  => 'Arial', size  => 14, color => 'black', valign  => 'bottom', bold  => 1 );
    my %subFont       = ( font  => 'Arial', size  => 12, color => 'black', valign  => 'bottom', bold  => 1 );
    my %titleFont     = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom' );
    my %titleFontRt   = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom', align => 'right' );
    my %colFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'bottom', bold => 1, bg_color => $c_litegrey );
    
    my %bodyFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', border => 1, border_color => $c_litegrey );
    my %boldFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, border => 1, border_color => $c_litegrey );
    my %dateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', num_format => 'mm/dd/yy' );
    my %numFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %boldNumFont   = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right' );
    my %currFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %rateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %pctFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
	
	my $fmtHead  = $workbook->add_format( %headFont );
	my $fmtSub   = $workbook->add_format( %subFont );
	my $fmtTitle = $workbook->add_format( %titleFont );
	my $fmtTtlRt = $workbook->add_format( %titleFontRt );
	my $fmtCol   = $workbook->add_format( %colFont );
	my $fmtBody  = $workbook->add_format( %bodyFont );
	my $fmtBold  = $workbook->add_format( %boldFont );
	my $fmtDate  = $workbook->add_format( %dateFont );
	my $fmtNum   = $workbook->add_format( %numFont );
	my $fmtBNum  = $workbook->add_format( %boldNumFont );
	my $fmtCurr  = $workbook->add_format( %currFont );
	my $fmtRate  = $workbook->add_format( %rateFont );
	my $fmtPct   = $workbook->add_format( %pctFont );
	
	$fmtCurr->set_num_format( "\$0.00" );
	$fmtRate->set_num_format( "\$0.0000" );
	$fmtPct->set_num_format( "0.00\%" );
	
	my $clnSQL   = "SELECT * FROM client WHERE cln_other = 'N' ORDER BY cln_name";
	my $ctcSQL   = "SELECT cln_code, cln_name, clc_contact, clc_address, clc_city, clc_state, clc_zip,\n"
	             . "clc_country, clc_phone, clc_email\n"
	             . "FROM client_contact\n"
	             . "INNER JOIN client ON clc_cln_id = cln_id\n"
	             . "ORDER BY cln_name, clc_contact";

    my @clnRS    = $shared->getResultSet( $clnSQL );
    my @ctcRS    = $shared->getResultSet( $ctcSQL );
    	             
    my $sheet    = $workbook->add_worksheet( "Clients" );
            
    $sheet->write_string( 0, 0, "SDT Client List", $fmtHead );
    $sheet->write_string( 0, 9, "As of: $nowDesc", $fmtTtlRt );
        
    my @colHead  = ( "Client ID", "Name", "Type", "Status", "Tier 1 Disc Rate", "Tier 2 Disc Rate", "Translation Rate", "Review Rate", "Hourly Rate", "MTPE Rate", "Comments" );
    $sheet->write_row( 2, 0, \@colHead, $fmtCol );
        
    my $row = 3;
	foreach my $rec ( @clnRS ) {

        my $cID    = $rec->{cln_code};
        my $cName  = $rec->{cln_name};
        my $cAct   = $rec->{cln_active};
        my $cType  = $rec->{cln_type};
        my $cDRt1  = ( $rec->{cln_dsc_rate} ) / 100;
        my $cDRt2  = ( $rec->{cln_dsc_rate_2} ) / 100;
        my $tRate  = $rec->{cln_tr_rate};
        my $eRate  = $rec->{cln_ed_rate};
        my $hRate  = $rec->{cln_hr_rate};
        my $mRate  = $rec->{cln_mtpe_rate};
        my $comm   = $rec->{cln_comment};
        
        my $status = "Active";
        if ( $cAct eq "N" ) {
            $status = "Inactive";
        }
        
        my $dspType = $shared->getLookupDesc( "CLN_TYPE", $cType );
        	        
        $sheet->write_string( $row, 0, $cID, $fmtBody );
        $sheet->write_string( $row, 1, $cName, $fmtBold );
        $sheet->write_string( $row, 2, $dspType, $fmtBody );
        $sheet->write_string( $row, 3, $status, $fmtBody );
        $sheet->write_number( $row, 4, $cDRt1, $fmtPct );
        $sheet->write_number( $row, 5, $cDRt2, $fmtPct );
        $sheet->write_number( $row, 6, $tRate, $fmtRate );
        $sheet->write_number( $row, 7, $eRate, $fmtRate );
        $sheet->write_number( $row, 8, $hRate, $fmtCurr );
        $sheet->write_number( $row, 9, $mRate, $fmtRate );
        $sheet->write_string( $row, 10, $comm, $fmtBody );            

        $row++;
    }

    $sheet->set_column( 0, 0, 10 );
    $sheet->set_column( 1, 1, 36 );
    $sheet->set_column( 2, 3, 8 );
    $sheet->set_column( 4, 5, 15 );
    $sheet->set_column( 6, 6, 16 );
    $sheet->set_column( 7, 7, 10 );
    $sheet->set_column( 8, 8, 12 );
    $sheet->set_column( 9, 9, 12 );
    $sheet->set_column( 10, 10, 64 );

    $sheet->autofilter( 2, 0, $row, 9 );
    $sheet->hide_gridlines( 2 );
    $sheet->freeze_panes( 3, 0 );


            
    my $sheet2    = $workbook->add_worksheet( "Contacts" );
    
    $sheet2->write_string( 0, 0, "SDT Client Contacts", $fmtHead );
    $sheet2->write_string( 0, 9, "As of: $nowDesc", $fmtTtlRt );
        
    my @colHead  = ( "Client ID", "Client Name", "Contact Name", "Address", "City", "State", "Zip", "Country", "Phone", "E-mail" );
    $sheet2->write_row( 2, 0, \@colHead, $fmtCol );
	$row = 3;
	foreach my $rec ( @ctcRS ) {
	
	    my $cID    = $rec->{cln_code};
	    my $cName  = $rec->{cln_name};
	    my $ctcNm  = $rec->{clc_contact};
	    my $addr   = $rec->{clc_address};
	    my $city   = $rec->{clc_city};
	    my $state  = $rec->{clc_state};
	    my $zip    = $rec->{clc_zip};
	    my $cntry  = $rec->{clc_country};
	    my $phn    = $rec->{clc_phone};
	    my $eml    = $rec->{clc_email};        

        $sheet2->write_string( $row, 0, $cID, $fmtBody );
        $sheet2->write_string( $row, 1, $cName, $fmtBold );
        $sheet2->write_string( $row, 2, $ctcNm, $fmtBold );
        $sheet2->write_string( $row, 3, $addr, $fmtBody );
        $sheet2->write_string( $row, 4, $city, $fmtBody );
        $sheet2->write_string( $row, 5, $state, $fmtBody );
        $sheet2->write_string( $row, 6, $zip, $fmtBody );
        $sheet2->write_string( $row, 7, $cntry, $fmtBody );
        $sheet2->write_string( $row, 8, $phn, $fmtBody );
        $sheet2->write_string( $row, 9, $eml, $fmtBody );            

        $row++;
    }

    $sheet2->set_column( 0, 0, 10 );
    $sheet2->set_column( 1, 1, 36 );
    $sheet2->set_column( 2, 2, 27 );
    $sheet2->set_column( 3, 3, 30 );
    $sheet2->set_column( 4, 4, 14 );
    $sheet2->set_column( 5, 7, 11 );
    $sheet2->set_column( 8, 8, 21 );
    $sheet2->set_column( 9, 9, 36 );

    $sheet2->autofilter( 2, 0, $row, 9 );
    $sheet2->hide_gridlines( 2 );
    $sheet2->freeze_panes( 3, 0 );

    $workbook->close();
    
    system( "chmod 644 $fName" );
    
    return $urlName;

}

