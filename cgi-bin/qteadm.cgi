#!/usr/bin/perl
###########################################################################
# qteadm.cgi             Quote Administration
###########################################################################
# Copyright 2013, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;
use sdt;
use qtepdf;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );
my $sdt    = sdt->new( $shared );
my $qtepdf = qtepdf->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/qteadm.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($prj_url)     = "$script_path/prjadm.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $exceptList    = "";
my $dupList       = "";
my $debug         = $config->get_debug;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;


# Main program
my $usr_id = 0;
my $action = $shared->get_cgi( "action" );
my $hash_vars;
my $next_action = $shared->get_cgi( "next_act" );

$hash_vars->{srch_cln}     = $shared->get_cgi( "srch_cln" );
$hash_vars->{srch_pjno}    = $shared->get_cgi( "srch_pjno" );
$hash_vars->{srch_stat}    = $shared->get_cgi( "srch_stat" );
$hash_vars->{next_act}     = $next_action;

my $srch_cln     = $hash_vars->{srch_cln};
my $srch_pjno    = $hash_vars->{srch_pjno};
my $srch_stat    = $hash_vars->{srch_stat};
	
my $inp_srch     = "";
my $sp_vars      = "";
if ( $srch_cln gt "" ) {
  	$inp_srch .= "<input type=hidden name=srch_cln value=\"$srch_cln\">\n";
   	$sp_vars  .= "&srch_cln=$srch_cln";
}
if ( $srch_pjno gt "" ) {
   	$inp_srch .= "<input type=hidden name=srch_pjno value=\"$srch_pjno\">\n";
   	$sp_vars  .= "&srch_pjno=$srch_pjno";
}
if ( $srch_stat gt "" ) {
   	$inp_srch .= "<input type=hidden name=srch_stat value=\"$srch_stat\">\n";
   	$sp_vars  .= "&srch_stat=$srch_stat";
}
if ( $inp_srch eq "" ) {
	$inp_srch .= "<input type=hidden name=srch_init value=\"Y\">\n";
   	$sp_vars  .= "&srch_init=Y";
}
$sp_vars =~ s/^\&//;


#
# Main Program
#
 
if ( $auth->validate_access( "qteadm" ) ) {
	
	$usr_id    = $auth->getCurrentUser;

    if ( $action eq "search_any" ) {
        &search_any;
    } elsif ( $action eq "change_sort" ) {
    	&change_sort;
    } elsif ( $action eq "edit_qte_form" ) {
        &edit_qte_form;
    } elsif ( $action eq "edit_qte" ) {
        &edit_qte;
    } elsif ( $action eq "add_qte_form" ) {
        &add_qte_form;
    } elsif ( $action eq "add_qte" ) {
        &add_qte;
    } elsif ( $action eq "del_qte" ) {
        &del_qte;
    } elsif ( $action eq "add_item_form" ) {
    	&add_item_form;
    } elsif ( $action eq "add_item" ) {
        &add_item;
    } elsif ( $action eq "edit_item_form" ) {
        &edit_item_form;
    } elsif ( $action eq "edit_item" ) {
        &edit_item;
    } elsif ( $action eq "del_item" ) {
    	&del_item;
    } elsif ( $action eq "audit_log" ) {
    	&audit_log;
    } elsif ( $action eq "quote_pdf" ) {
        my $qte_id = $shared->get_cgi( "qte_id" );
        my $pdfURL = $qtepdf->genQuote( $qte_id );
        $shared->disp_pdf( $pdfURL );
    } else {
        &search_any( "Y" );
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my $qte_cnt = $shared->getSQLCount( "SELECT count(*) FROM quote" );

    if ( $conf->{quote_cnt} eq "" ) {
        $conf->{quote_cnt}     = $qte_cnt;
    }
    
   	my $abContent = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=add_qte_form>\n"
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Add New Quote\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";

 	$conf->{add_button}  = $abContent;
 	$conf->{selClient}   = &select_client( "Y" );
 	$conf->{selStat}     = $shared->pop_lookup_select( "QUOTE_STATUS", "", "ORDER BY lku_sort" );
    
    my $pg_body              = &disp_form( "qte_main" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my ( $init_srch ) = @_;
	
	my $content;
	
	my $srch_cln     = $hash_vars->{srch_cln};
	my $srch_pjno    = $hash_vars->{srch_pjno};
	my $srch_stat    = $hash_vars->{srch_stat};
	my $srch_head    = "<b>Searched:</b>&nbsp;";
	my $srch_par     = 0;
	
	if ( ( $srch_cln gt "" ) || ( $srch_pjno gt "" ) || ( $srch_stat gt "" ) || ( $init_srch eq "Y" ) ) {
	
        my $SQL = "SELECT qte_id, qte_prj_no, qte_status, qte_cln_id, qte_tot_amount, cln_name, cln_other, qte_oth_name\n"
                . "FROM quote\n"
                . "INNER JOIN client on qte_cln_id = cln_id\n"
                . "WHERE 1\n";
        
	    if ( $srch_cln gt "" ) {
	    	my $cln_srch_name = $shared->getFieldVal( "client", "cln_name", "cln_id", $srch_cln );
		    $SQL .= "AND qte_cln_id = $srch_cln\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Client is <b>$cln_srch_name</b>";
		    } else {
		        $srch_head .= "Client is <b>$cln_srch_name</b>";
		    }
		    $srch_par++;
	    }
	    if ( $srch_pjno gt "" ) {
		    $SQL .= "AND qte_prj_no LIKE '$srch_pjno%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Quote Proj No starts with <b>$srch_pjno</b>";
		    } else {
		        $srch_head .= "Quote Proj No starts with <b>$srch_pjno</b>";
		    }
		    $srch_par++;
	    }
	    if ( $srch_stat gt "" ) {
	    	$SQL .= "AND qte_status = '$srch_stat'\n";
	    	if ( $srch_par > 0 ) {
	    		$srch_head .= ", Status is <b>$srch_stat</b>";
	    	} else {
	    		$srch_head .= "Status is <b>$srch_stat</b>";
	    	}
	    	$srch_par++;
	    }
	    if ( $srch_par == 0 ) {
	        $SQL .= "AND qte_status IN ( 'PENDING', 'ACCEPTED', 'DELAYED' )\n";
	    }
	    
	    $SQL     .= "ORDER BY qte_prj_no DESC";
	    
	    my $doReset = 0;
	    if ( ( $init_srch eq "Y" ) && ( $srch_par == 0 ) ) {
	        
	    	$srch_head = "All Pending, Accepted and Delayed Quotes";
	    } else {
	        $doReset = 1;
	    }
	
	    my @rs   = $shared->getResultSet( $SQL );
	    my $row  = 0;
	    my $row_color;
	    my $fndCnt = 0;
	    
	    $content .= "<span class=subhead><b>$srch_head</b></span><br><br>\n";
	    if ( $doReset ) {
	    	$content .= "<a href=\"$cgi_url\" class=nav><b>[ Clear Filter ]</b></a><br>\n";
	    }
	    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	    $content .= "<tr bgcolor=\"black\">\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Quote Proj No</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Status</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Client Name</b></font></td>\n";
	    $content .= "<td valign=top align=right class=form><font color=\"white\"><b>Quote Amount</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
	    foreach my $rec ( @rs ) {
	    	my $id      = $rec->{qte_id};
	    	my $pjno    = $rec->{qte_prj_no};
	    	my $qStat   = $rec->{qte_status};
	    	my $name    = $rec->{cln_name};
	    	my $clnOth  = $rec->{cln_other};
	    	my $qamt    = $rec->{qte_tot_amount};
	    	
	    	if ( $clnOth eq "Y" ) {
	    	    $name   = "Other - " . $rec->{qte_oth_name};
	    	}
	    	
	    	my $dspQAmt = $shared->fmtCurrency( $qamt );
	    	
	    	if ( $row == 0 ) {
	    		$row_color = $color_bg2;
	    		$row = 1;
	    	} else {
	    		$row_color = $color_bg1;
	    		$row = 0;
	    	}
	    	
	    	$content .= "<tr bgcolor=\"$row_color\">\n";
	    	$content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=edit_qte_form&qte_id=$id&" . $sp_vars . "\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"View\" title=\"View Quote\"></a></td>\n";
	    	$content .= "<td valign=top align=left class=form>$pjno</td>\n";
	    	$content .= "<td valign=top align=left class=form>$qStat</td>\n";
	    	$content .= "<td valign=top align=left class=form>$name</td>\n";
	    	$content .= "<td valign=top align=right class=form>$dspQAmt</td>\n";
	    	$content .= "<td valign=top align=right class=form>";
    	    $content .= "<a href=\"$cgi_url?action=del_qte&qte_id=$id&" . $sp_vars . "\" onClick=\"return confirm('Permanently remove Quote from database?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete $pjno\"></a>";
	    	$content .= "</td></tr>\n";
	    	
	    	$fndCnt++;
	    }
	    $content .= "</table>\n";
	    		
	    if ( $fndCnt == 0 ) {
	    	$content  = "";
	    	$java_msg = "No Quotes found meeting search criteria.";
	    } else {
	    	$conf->{quote_cnt} = "$fndCnt";
	    }
	
    	$conf->{search_results} = $content;
	
	
    } else {
	
	    $java_msg = "No Search Criteria Specified.";        
	
    }

	&print_main;
	
}



############################################################
# Form to add member to database
############################################################
sub add_qte_form {

  	my $today = $shared->getSQLToday;
    	
   	$conf->{form_head}            = "Add New Quote";
   	$conf->{form_start}           = "<form method=post name=adm_qte_form action=\"$cgi_url\">\n"
    	                          . "<input type=hidden name=action value=\"add_qte\">\n";
    	                          
    
   	$conf->{f_client}             = "<select name=qte_cln_id class=form>" . &select_client( "N" ) . "</select>";
   	$conf->{f_brand}              = "<select name=qte_brand class=form>" . $shared->pop_lookup_select( "QUOTE_BRAND", "", "ORDER BY lku_sort" ) . "</select>";
   	$conf->{f_pjno}               = "<input name=qte_prj_no value=\"" . &genNewQPN . "\" size=10 maxlength=20 class=form>";
   	
   	$conf->{form_submit}          = "<input type=submit value=\"Add Now\" class=form>&nbsp;\n"
                                  . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}             = "</form>";
   	$conf->{form_name}            = "adm_qte_form";
   	$conf->{focus_field}          = "qte_cln_id";  

    	
    my $pg_body = &disp_form( "qte_add_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

############################################################
# Add the record to the quote database
############################################################
sub add_qte {

   	my $cln_id    = $shared->get_cgi( "qte_cln_id" );
   	my $pjno      = $shared->get_cgi( "qte_prj_no" );
   	my $brand     = $shared->get_cgi( "qte_brand" );
   	
   	my $pnCnt     = $shared->getSQLCount( "SELECT COUNT(*) FROM quote WHERE qte_prj_no = '$pjno'" );
    	
    if ( $pnCnt == 0 ) {
    	
        my $insert   = "INSERT INTO quote ( qte_prj_no, qte_cln_id, qte_add_date, qte_add_usr_id, qte_brand )\n"
                     . "VALUES ( '$pjno', $cln_id, now(), $usr_id, '$brand' )";
        $shared->doSQLUpdate( $insert );
    	
    	my $cln_rec  = $shared->get_rec( "client", "cln_id", $cln_id );
    	
    	my $cln_name = $cln_rec->{cln_name};
    	
  	    my $qte_id   = $shared->getSQLLastInsertID;
   	    &add_audit( $qte_id, "New Quote Record Created: $pjno ($cln_name)." );
    	
   	    &edit_qte_form( $qte_id );
   	} else {
   	    $java_msg = "Quote Proj No Already Exists";
   	    &search_any( "Y" );
   	}

}

############################################################
# Form to edit quote record
############################################################
sub edit_qte_form {

    my ( $qte_id ) = @_;
    
    if ( $qte_id eq "" ) {
        $qte_id  = $shared->get_cgi( "qte_id" );
    }
    
    my $qte_rec = $shared->get_rec( "quote", "qte_id", $qte_id );

    my $cln_id     = $qte_rec->{qte_cln_id};
    my $clc_id     = $qte_rec->{qte_clc_id};
    my $pjno       = $qte_rec->{qte_prj_no};
    my $qDate      = $qte_rec->{qte_add_date};
    my $qAmt       = $qte_rec->{qte_tot_amount};
    my $qDelivery  = $qte_rec->{qte_delivery};
    my $qUID       = $qte_rec->{qte_add_usr_id};
    my $qStat      = $qte_rec->{qte_status};
    my $qBrand     = $qte_rec->{qte_brand};
    my $qComment   = $qte_rec->{qte_comment};
    my $qOthName   = $qte_rec->{qte_oth_name};
    my $qOthCtc    = $qte_rec->{qte_oth_contact};
    my $qOthAdd1   = $qte_rec->{qte_oth_addr_1};
    my $qOthAdd2   = $qte_rec->{qte_oth_addr_2};
    my $qOthPhn    = $qte_rec->{qte_oth_phone};
    my $nDate      = $shared->getDateFmt( $qte_rec->{qte_nudge_date} );

    my $cln_rec    = $shared->get_rec( "client", "cln_id", $cln_id );
    my $clnOth     = $cln_rec->{cln_other};
    my $clnName    = $cln_rec->{cln_name};
    
    my $prjCnt     = $shared->getSQLCount( "SELECT COUNT(*) FROM project WHERE prj_qte_id = $qte_id" );
    if ( $prjCnt == 1 ) {
        my $prjNo          = $shared->getFieldVal( "project", "prj_no", "prj_qte_id", $qte_id );
        $conf->{f_prj_row} = "<tr><td align=right valign=top class=label bgcolor=$color_bg2>Project #</td>\n"
                           . "<td valign=top align=left class=form><i>$prjNo</i></td></tr>";
 
    }

    my $usrName    = $shared->getFieldVal( "user", "usr_name", "usr_id", $qUID );
    my $dspQDate   = $shared->getDateFmt( $qDate );
    my $dspQAmt    = $shared->fmtCurrency( $qAmt );
    
    
    $conf->{form_id_row}          = "<tr><td align=right valign=top class=label bgcolor=$color_bg2 width=150>Quote Proj No</td>\n"
                                  . "<td valign=top align=left class=form><b>$pjno</b></td></tr>\n";

    
   	$conf->{form_head}            = "Update Quote Details";
   	$conf->{form_start}           = "<form method=post name=qte_edit_form action=\"$cgi_url\">\n"
   	                              . "<input type=hidden name=action value=\"edit_qte\">\n"
   	                              . "<input type=hidden name=qte_id value=\"$qte_id\">\n"
   	                              . $inp_srch;
   	                              
   	if ( $clnOth eq "N" ) {
   	    $conf->{f_cln_name}       = $clnName;
   	    $conf->{f_contact}        = "<select name=qte_clc_id class=form><option value=\"0\"></option>" . &select_contact( $cln_id, $clc_id ) . "</select>";
   	} else {
   	    $conf->{f_cln_name}       = "<input name=qte_oth_name value=\"$qOthName\" size=40 maxlength=80 class=form><br>\n"
   	                              . "<input name=qte_oth_addr_1 value=\"$qOthAdd1\" size=40 maxlength=80 placeholder=\"Address Line 1\" class=form><br>\n"
   	                              . "<input name=qte_oth_addr_2 value=\"$qOthAdd2\" size=40 maxlength=80 placeholder=\"Address Line 2\" class=form>";
   	    $conf->{f_contact}        = "<input name=qte_oth_contact value=\"$qOthCtc\" size=40 maxlength=80 class=form><br>\n"
   	                              . "<input name=qte_oth_phone value=\"$qOthPhn\" size=15 maxlength=20 placeholder=\"Contact Phone\" class=form>";
   	}
    $conf->{f_delivery}           = "<input name=qte_delivery value=\"$qDelivery\" size=20 maxlength=30 class=form>";
    $conf->{f_comment}            = "<textarea name=qte_comment rows=4 cols=40 wrap class=form>$qComment</textarea>";
    $conf->{f_quote_date}         = $dspQDate;
    $conf->{f_status}             = "<select name=qte_status class=form>" . $shared->pop_lookup_select( "QUOTE_STATUS", $qStat, "ORDER BY lku_sort" ) . "</select>";
    if ( $qStat eq "DELAYED" ) {
        $conf->{f_status}        .= "&nbsp;&nbsp;Follow-up Date: " . $shared->getJSCalInput( "qte_nudge_date", $nDate, "jscal_trigger" );
    }
    $conf->{f_brand}              = "<select name=qte_brand class=form>" . $shared->pop_lookup_select( "QUOTE_BRAND", $qBrand, "ORDER BY lku_sort" ) . "</select>";                                
    $conf->{f_quote_user}         = $usrName;
    $conf->{f_quote_amt}          = $dspQAmt;


    my $actList           = "<table border=0 cellpadding=0 cellspacing=0 width=100%><tr><td valign=top align=left class=form>\n"
   	                      . "<input type=submit value=\"Update Quote\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?$sp_vars'\" class=form></td>"
                          . "<td valign=top align=right class=form>\n";
    if ( ( $cln_id > 0 ) && ( $clc_id > 0 ) && ( $qStat eq "ACCEPTED" ) && ( $prjCnt == 0 ) ) {
        $actList         .= "<input type=button value=\"Create Project\" onClick=\"parent.location.href='$prj_url?action=add_prj_quote&qte_id=$qte_id'\" class=form>&nbsp;\n";
    }
    $actList             .= "<input type=button value=\"Generate PDF\" onClick=\"window.open( '$cgi_url?action=quote_pdf&qte_id=$qte_id', '_blank' )\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Audit Log\" onClick=\"parent.location.href='$cgi_url?action=audit_log&qte_id=$qte_id&$sp_vars'\" class=form></td></tr>\n"
                          . "</table>";
    $conf->{form_submit}  = $actList;
   	$conf->{form_end}     = "</form>";
  
    my $fndCnt  = 0;
      	
    my $content = "";
   	$content .= "<table border=0 cellpadding=2 cellspacing=0 width=836>\n";
   	
   	if ( $qStat ne "ACCEPTED" ) {
        $content .= "<form method=post name=contact_add_form action=\"$cgi_url\">\n";
        $content .= "<input type=hidden name=action value=\"add_item_form\">\n";
        $content .= "<input type=hidden name=qte_id value=\"$qte_id\">\n";
        $content .= $inp_srch;
    }
    
    $content .= "<tr><td valign=top align=left class=overview colspan=4><font color=\"black\">Quote Items</font></td></tr>\n";
	$content .= "<tr bgcolor=\"black\">\n";
	$content .= "<td valign=top align=left class=form width=100><font color=\"white\"><b>Type</b></font></td>\n";
	$content .= "<td valign=top align=left class=form width=500><font color=\"white\"><b>Description</b></font></td>\n";
	$content .= "<td valign=top align=right class=form width=150><font color=\"white\"><b>Amount</b></font></td>\n";
	if ( $qStat ne "ACCEPTED" ) {
   	    $content .= "<td valign=top align=right class=form width=86><input type=submit value=\"Add Item\" class=form></td></tr>\n";
   	} else {
   	    $content .= "<td valign=top align=right class=form width=86>&nbsp;</td></tr>\n";
   	}

    my $row = 0;
    my $row_color;
    my $anyNoUp = 0;
    	
	my $SQL = "SELECT qtl_id, qtl_type, qtl_desc, qtl_rate, qtl_amount\n"
	        . "FROM quote_line\n"
	        . "WHERE qtl_qte_id = $qte_id\n"
	        . "ORDER BY qtl_type, qtl_id";
	my @rs  = $shared->getResultSet( $SQL );
	foreach my $rec ( @rs ) {
	 	my $id      = $rec->{qtl_id};
	 	my $type    = $rec->{qtl_type};
	 	my $desc    = $rec->{qtl_desc};
	 	my $qtlPct  = $shared->fmtNumber( $rec->{qtl_rate}, 1 );
	 	my $qtlAmt  = $shared->fmtCurrency( $rec->{qtl_amount} );
	 	my $dspCls  = "form";
	 	
	 	my $dspAmt  = $qtlAmt;
	   	
	   	if ( $row == 0 ) {
	   		$row_color = $color_bg2;
	   		$row = 1;
	   	} else {
	   		$row_color = $color_bg1;
	   		$row = 0;
	   	}
	   	
	   	my $dspType = "";
	   	if ( $type eq "A-TR" ) {
	   	    $dspType = "Translate";
	   	} elsif ( $type eq "B-ED" ) {
	   	    $dspType = "Review";
	   	} elsif ( $type eq "C-PF" ) {
	   	    $dspType = "Proof Read";
	   	} elsif ( $type eq "D-MTPE" ) {
	   	    $dspType = "MTPE";
	   	} elsif ( $type eq "D-OTH" ) {
	   	    $dspType = "Other";
	   	} elsif ( $type eq "E-DSC" ) {
	   	    $dspType = "Discount";
	   	    $dspCls  = "formred";
	   	} elsif ( $type eq "F-PCT" ) {
	   	    $dspType = "Discount";
	   	    $dspCls  = "formred";
	   	    $dspAmt  = "(" . $qtlPct . "\%)&nbsp;" . $qtlAmt;
	   	}
	
	   	$content .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form>$dspType</td>\n";
	   	$content .= "<td valign=top align=left class=form>$desc</td>\n";
	   	$content .= "<td valign=top align=right class=$dspCls>$dspAmt</td>\n";
   	    $content .= "<td valign=top align=right class=form>\n";
        $content .= "<a href=\"$cgi_url?action=edit_item_form&qtl_id=$id&qte_id=$qte_id&$sp_vars\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"Edit Line Item\" title=\"Edit Line Item\"></a>";
   	    if ( $qStat ne "ACCEPTED" ) {
   	        $content .= "&nbsp;&nbsp;<a href=\"$cgi_url?action=del_item&qtl_id=$id&qte_id=$qte_id&$sp_vars\" onClick=\"return confirm('Permanently remove Item from Quote?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete Item\"></a>";
   	    }
   	    $content .= "</td></tr>\n";
	    	
	  	$fndCnt++;
	}
	
	if ( $fndCnt == 0 ) {
	  	$content  .= "<tr><td valign=top align=left colspan=4 class=form>No Items found for Quote</td></tr>\n";
	}
	
   	$content .= "</form>\n";
	
	$content .= "</table>\n";
	
    $conf->{item_info} = $content;
    	
    my $pg_body = &disp_form( "qte_edit_form" );

    if ( $qStat eq "DELAYED" ) {
        $env->{javascript}   = $shared->getJSCalHead;
    }
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

############################################################
# Update the record to the members database
############################################################
sub edit_qte {

  	my $srch_init = $shared->get_cgi( "srch_init" );
  	my $next_act  = $shared->get_cgi( "next_act" );

    my $qte_id    = $shared->get_cgi( "qte_id" );
   	my $clc_id    = ( $shared->get_cgi( "qte_clc_id" ) ) + 0;
   	my $qStat     = $shared->get_cgi( "qte_status" );
   	my $delivery  = $shared->get_cgi( "qte_delivery" );
    my $comment   = $shared->get_cgi( "qte_comment" );
    my $othName   = $shared->get_cgi( "qte_oth_name" );
    my $othCtc    = $shared->get_cgi( "qte_oth_contact" );
    my $othAdd1   = $shared->get_cgi( "qte_oth_addr_1" );
    my $othAdd2   = $shared->get_cgi( "qte_oth_addr_2" );
    my $othPhn    = $shared->get_cgi( "qte_oth_phone" );
    my $qBrand    = $shared->get_cgi( "qte_brand" );
    my $nDate     = $shared->getSQLDate( $shared->get_cgi( "qte_nudge_date" ) );
    
    if ( $qStat ne "DELAYED" ) {
        $nDate    = "0000-00-00";
    }
    
   	my $cur_rec     = $shared->get_rec( "quote", "qte_id", $qte_id );
   	my $p_clc_id    = $cur_rec->{qte_clc_id};
   	my $p_qStat     = $cur_rec->{qte_status};
   	my $p_qBrand    = $cur_rec->{qte_brand};
   	my $p_delivery  = $cur_rec->{qte_delivery};
    my $p_comment   = $cur_rec->{qte_comment};
    my $p_OthName   = $cur_rec->{qte_oth_name};
    my $p_OthCtc    = $cur_rec->{qte_oth_contact};
    my $p_OthAdd1   = $cur_rec->{qte_oth_addr_1};
    my $p_OthAdd2   = $cur_rec->{qte_oth_addr_2};
    my $p_OthPhn    = $cur_rec->{qte_oth_phone};
    my $p_nDate     = $cur_rec->{qte_nudge_date};
    my $cln_id      = $cur_rec->{qte_cln_id};
    
    my $clnOth      = $shared->getFieldVal( "client", "cln_other", "cln_id", $cln_id );
    
       	
   	my $chg_dtls = "";
   	
   	if ( $clnOth eq "N" ) {
   	    if ( $clc_id ne $p_clc_id ) {
       	    my $p_contact = $shared->getFieldVal( "client_contact", "clc_contact", "clc_id", $p_clc_id );
   	        my $contact   = $shared->getFieldVal( "client_contact", "clc_contact", "clc_id", $clc_id );
   		    $chg_dtls    .= "Client Contact: from [$p_contact] to [$contact]<br>\n";
   	    }
   	}
   	
   	if ( $qStat ne $p_qStat ) {
   	    $chg_dtls .= "Status: from [$p_qStat] to [$qStat]<br>\n";
   	}
   	
   	if ( $nDate ne $p_nDate ) {
   	    my $prvDate = $shared->getDateFmt( $p_nDate );
   	    my $newDate = $shared->getDateFmt( $nDate );
   	    $chg_dtls .= "Follow-up Date: from [$prvDate] to [$newDate]<br>\n";
   	}
   	
   	if ( $delivery ne $p_delivery ) {
   	    $chg_dtls .= "Delivery Date: from [$p_delivery] to [$delivery]<br>\n";
   	}
   	
   	if ( $qBrand ne $p_qBrand ) {
   	    my $dspPrevBrand = $shared->getLookupDesc( "QUOTE_BRAND", $p_qBrand );
   	    my $dspNewBrand  = $shared->getLookupDesc( "QUOTE_BRAND", $qBrand );
   	    $chg_dtls .= "Brand: from [$dspPrevBrand] to [$dspNewBrand]<br>\n";
   	}

    if ( $comment ne $p_comment ) {
        $p_comment =~ s/'/\\'/g;
        $comment   =~ s/'/\\'/g;
        $chg_dtls .= "Comments: from [$p_comment] to [$comment]<br>\n";
    }
    
    if ( $clnOth eq "Y" ) {
      	if ( $othName ne $p_OthName ) {
    	    $chg_dtls .= "Other Name: from [$p_OthName] to [$othName]<br>\n";
   	    }
      	if ( $othAdd1 ne $p_OthAdd1 ) {
    	    $chg_dtls .= "Address Line 1: from [$p_OthAdd1] to [$othAdd1]<br>\n";
   	    }
      	if ( $othAdd2 ne $p_OthAdd2 ) {
    	    $chg_dtls .= "Address Line 2: from [$p_OthAdd2] to [$othAdd2]<br>\n";
   	    }
      	if ( $othCtc ne $p_OthCtc ) {
    	    $chg_dtls .= "Contact: from [$p_OthCtc] to [$othCtc]<br>\n";
   	    }
   	    if ( $othPhn ne $p_OthPhn ) {
   	        $chg_dtls .= "Contact Phn: from [$p_OthPhn] to [$othPhn]<br>\n";
   	    }
   	}
        
   	if ( $chg_dtls gt "" ) {
   		chomp( $chg_dtls );
   		$chg_dtls =~ s/<br>$//;
   	}
    	
    $othName =~ s/'/\\'/g;
    $othCtc  =~ s/'/\\'/g;
    $othAdd1 =~ s/'/\\'/g;
    $othAdd2 =~ s/'/\\'/g;
    $othPhn  =~ s/'/\\'/g;
    
   	my $update   = "UPDATE quote SET qte_clc_id = $clc_id, qte_delivery = '$delivery', qte_status = '$qStat',\n"
   	             . "qte_brand = '$qBrand', qte_nudge_date = '$nDate',\n"
   	             . "qte_comment = '$comment', qte_oth_name = '$othName', qte_oth_contact = '$othCtc',\n"
   	             . "qte_oth_addr_1 = '$othAdd1', qte_oth_addr_2 = '$othAdd2', qte_oth_phone = '$othPhn'\n"
   	             . "WHERE qte_id = $qte_id";
   	$shared->doSQLUpdate( $update );
   	
    $sdt->updateQuote( $qte_id );
   	    	
   	if ( $chg_dtls gt "" ) {
   		&add_audit( $qte_id, "Quote Record Updated...<br>\n" . $chg_dtls );
   	}
    	
    $env->{doFade} = "Y";
    $java_msg      = "Quote Updated...";
    &edit_qte_form( $qte_id );

}

###########################################################                   
# Delete Quote from Database
#----------------------------------------------------------
sub del_qte {

   	my $srch_init = $shared->get_cgi( "srch_init" );
   	my $next_act  = $shared->get_cgi( "next_act" );

   	my $qte_id = $shared->get_cgi( "qte_id" );
   	
    my $qte_rec = $shared->get_rec( "quote", "qte_id", $qte_id );
    my $clnID   = $qte_rec->{qte_cln_id};
    my $clnName = $shared->getFieldVal( "client", "cln_name", "cln_id", $clnID );
    
   	my $desc    = "Deleting Quote (" . $qte_rec->{qte_prj_no} . " - $clnName)";
   	&add_audit( $qte_id, $desc );
    	
   	my $update = "DELETE FROM quote WHERE qte_id = $qte_id";
   	$shared->doSQLUpdate( $update );
    	
   	my $up2    = "DELETE FROM quote_line WHERE qtl_qte_id = $qte_id";
   	$shared->doSQLUpdate( $up2 );
    
    &search_any( $srch_init );
}


###########################################################
# View Audit Log for a specific application
#----------------------------------------------------------
sub audit_log {

    my $qte_id  = $shared->get_cgi( "qte_id" );
    my $qte_rec = $shared->get_rec( "quote", "qte_id", $qte_id );
    my $cln_rec = $shared->get_rec( "client", "cln_id", $qte_rec->{qte_cln_id} );
    my $name    = $cln_rec->{cln_name};
    my $pjno    = $qte_rec->{qte_prj_no};
    
    my $SQL     = "SELECT usr_name, qta_date, qta_description\n"
                . "FROM quote_audit\n"
                . "INNER JOIN user ON qta_usr_id = usr_id\n"
                . "WHERE qta_qte_id = $qte_id\n"
                . "ORDER BY qta_date DESC";
                
	my $content;
    $content .= "<span class=subhead>Client: $name</span><br>Quote Proj No: $pjno<br><br>\n";
    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
    $content .= "<tr bgcolor=\"black\">\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Log Date</b></font></td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>User</b></font></td>\n";
    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Description</b></font></td></tr>\n";
    
    my @rs    = $shared->getResultSet( $SQL );
    my $row  = 0;
    my $row_color;

	foreach my $rec ( @rs ) {
	   	my $log_date = $shared->getDateFmt( $rec->{qta_date} );
	   	my $uName    = $rec->{usr_name};
	   	my $desc     = $rec->{qta_description};
	    	
    	if ( $row == 0 ) {
    		$row_color = $color_bg2;
    		$row = 1;
    	} else {
    		$row_color = $color_bg1;
    		$row = 0;
    	}
	    	
    	$content .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form>$log_date</td>\n";
	    $content .= "<td valign=top align=left class=form>$uName</td>\n";
	    $content .= "<td valign=top align=left class=form>$desc</td></tr>\n";
	    
    }
    $content .= "</table>\n";

   	$conf->{search_results} = $content;
   	
    my $pg_body = &disp_form( "qte_audit" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
   	
}


###########################################################
# Add Audit Log Record
#----------------------------------------------------------
sub add_audit {
	
	my ( $id, $desc ) = @_;
	
	$desc =~ s/'/\\'/g;
	
	my $insert = "INSERT INTO quote_audit ( qta_qte_id, qta_usr_id, qta_date, qta_description )\n"
	           . "VALUES ( $id, $usr_id, now(), '$desc' )";
	$shared->doSQLUpdate( $insert );
	
}

############################################################
# Form to add the quote item
############################################################
sub add_item_form {

    my $qte_id = $shared->get_cgi( "qte_id" );

    my $qte_rec = $shared->get_rec( "quote", "qte_id", $qte_id );
    my $cln_id  = $qte_rec->{qte_cln_id};
    my $pjno    = $qte_rec->{qte_prj_no};
    
    my $cln_rec = $shared->get_rec( "client", "cln_id", $cln_id );
    my $clnOth  = $cln_rec->{cln_other};
    my $cName   = $cln_rec->{cln_name};
    
    if ( $clnOth eq "Y" ) {
        $cName  = "Other: " . $qte_rec->{qte_oth_name};
    }
    
    $conf->{form_head}    = "New Quote Line Item";
    $conf->{form_start}   = "<form method=post name=adm_qtl_form action=\"$cgi_url\" enctype=\"multipart/form-data\">\n"
    	                  . "<input type=hidden name=action value=\"add_item\">\n"
    	                  . "<input type=hidden name=qte_id value=\"$qte_id\">\n"
    	                  . $inp_srch . "\n";
   	$conf->{f_client}     = $cName;
   	$conf->{f_pjno}       = $pjno;
   	$conf->{f_qtl_type}   = "<select id=qType name=qType class=form onchange=JavaScript:showInp()><option value=\"A\">Import Analysis</option><option value=\"O\">Other Fee</option><option value=\"D\">Discount (Amt)</option><option value=\"P\">Discount (Pct)</option></select>";
    $conf->{f_details}    = "<p id=inpDtls></p>";
    
    my $inpAnal           = &getInpAnalysis;
    my $inpOth            = &getInpOther;
    my $inpPct            = &getInpPct;
    
    $conf->{form_submit}  = "<input type=submit value=\"Add Item Now\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}     = "</form>";
    	
    my $pg_body = &disp_form( "qtl_add_form" );
        
    my $js = "<script type=\"text/javascript\">\n";
    $js   .= "function showInp(){\n";
    $js   .= "  var iTxt   = '$inpAnal';\n";
    $js   .= "  var qType  = document.getElementById('qType').value;\n";
    $js   .= "  var fndO   = qType.match(/O/);\n";
    $js   .= "  var fndD   = qType.match(/D/);\n";
    $js   .= "  var fndP   = qType.match(/P/);\n";
    $js   .= "  if (fndO) {\n";
    $js   .= "     iTxt  = '$inpOth';\n";
    $js   .= "  }\n";
    $js   .= "  if (fndD) {\n";
    $js   .= "     iTxt  = '$inpOth';\n";
    $js   .= "  }\n";
    $js   .= "  if (fndP) {\n";
    $js   .= "     iTxt  = '$inpPct';\n";
    $js   .= "  }\n";
    $js   .= "  document.getElementById('inpDtls').innerHTML = iTxt;\n";
    $js   .= "}\n";
    $js   .= "</script>\n";
    
    $env->{javascript} = $js;
    $env->{onload}     = "showInp()";

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );

}

############################################################
# Generate input fields for analysis item type
############################################################
sub getInpAnalysis {

    my $content = "";
    $content .= "<table border=0 cellpadding=2 cellspacing=0>";
    $content .= "<tr><td valign=top align=right class=form>Source Language</td>";
    $content .= "<td valign=top align=left class=form><select name=qtl_src_lng class=form>" . &getLang . "</select></td></tr>";
    $content .= "<tr><td valign=top align=right class=form>Target Language</td>";
    $content .= "<td valign=top align=left class=form><select name=qtl_tgt_lng class=form>" . &getLang . "</select></td></tr>";
    $content .= "<tr><td valign=top align=right class=form>Analysis File</td>";
    $content .= "<td valign=top align=left class=form><input type=file name=qtl_file value=\"\" class=form></td></tr>";
    $content .= "<tr><td valign=top align=right class=form>Actions</td>";
    $content .= "<td valign=top align=left class=form><input type=checkbox name=qtl_act_tr value=\"1\" class=form> Translate&nbsp;&nbsp;";
    $content .= "<input type=checkbox name=qtl_act_ed value=\"1\" class=form> Review&nbsp;&nbsp;";
    $content .= "<input type=checkbox name=qtl_act_pf value=\"1\" class=form> Proof Read&nbsp;&nbsp;";
    $content .= "<input type=checkbox name=qtl_act_mtpe value=\"1\" class=form> MTPE</td></tr>";
    $content .= "<tr><td valign=top align=right class=form>Document Name</td>";
    $content .= "<td valign=top align=left class=form><input name=qtl_desc size=40 maxlength=80 class=form></td></tr>";
    $content .= "</table>";
    
    return $content;
}

############################################################
# Generate input fields for analysis item type
############################################################
sub getInpOther {

    my $content = "";
    $content .= "<table border=0 cellpadding=2 cellspacing=0>";
    $content .= "<tr><td valign=top align=right class=form>Description</td>";
    $content .= "<td valign=top align=left class=form><textarea name=qtl_desc cols=40 rows=3 wrap class=form></textarea></td></tr>";
    $content .= "<tr><td valign=top align=right class=form>Amount</td>";
    $content .= "<td valign=top align=left class=form><input name=qtl_amount value=\"\" size=10 maxlength=16 class=form></td></tr>";
    $content .= "</table>";
    
    return $content;
}

############################################################
# Generate input fields for percent
############################################################
sub getInpPct {

    my $content = "";
    $content .= "<table border=0 cellpadding=2 cellspacing=0>";
    $content .= "<tr><td valign=top align=right class=form>Description</td>";
    $content .= "<td valign=top align=left class=form><textarea name=qtl_desc cols=40 rows=3 wrap class=form></textarea></td></tr>";
    $content .= "<tr><td valign=top align=right class=form>Percent</td>";
    $content .= "<td valign=top align=left class=form><input name=qtl_percent value=\"\" size=8 maxlength=16 class=form>\%</td></tr>";
    $content .= "</table>";
    
    return $content;
}

############################################################
# Add the quote item
############################################################
sub add_item {

    my $qte_id = $shared->get_cgi( "qte_id" );
    my $qType  = $shared->get_cgi( "qType" );
    my $src    = $shared->get_cgi( "qtl_src_lng" );
    my $tgt    = $shared->get_cgi( "qtl_tgt_lng" );
    my $iFile  = $shared->get_cgi( "qtl_file" );
    my $actTR  = $shared->get_cgi( "qtl_act_tr" );
    my $actED  = $shared->get_cgi( "qtl_act_ed" );
    my $actPF  = $shared->get_cgi( "qtl_act_pf" );
    my $actMT  = $shared->get_cgi( "qtl_act_mtpe" );
    my $desc   = $shared->get_cgi( "qtl_desc" );
    my $amt    = $shared->validNumber( $shared->get_cgi( "qtl_amount" ) );
    my $pct    = $shared->validNumber( $shared->get_cgi( "qtl_percent" ) );
    
    $desc =~ s/'/\\'/g;
    my $docNm  = $desc;
    
    if ( $qType eq "A" ) {
    
        my $cln_id = $shared->getFieldVal( "quote", "qte_cln_id", "qte_id", $qte_id );
        my $cRec   = $shared->get_rec( "client", "cln_id", $cln_id );
        my $tRate  = $cRec->{cln_tr_rate};
        my $eRate  = $cRec->{cln_ed_rate};
        my $hRate  = $cRec->{cln_hr_rate};
        my $mRate  = $cRec->{cln_mtpe_rate};
        
        my $sLang  = $shared->getFieldVal( "language", "lng_desc", "lng_code", $src );
        my $tLang  = $shared->getFieldVal( "language", "lng_desc", "lng_code", $tgt );
        
        my $fExt   = "";
        if ( $iFile =~ /.*[\.](.*)$/ ) {
            $fExt = $1;
        }

        my $newFl  = "$imp_path/last_quote_analysis.txt";
        my $status = $shared->uploadFile( "qtl_file", $newFl, "" );
        if ( $status == 0 ) {
        
            my $hashDtls;
            if ( $fExt eq "txt" ) {
                $hashDtls = $sdt->parseTXTFile( $newFl );
            } else {
                $hashDtls->{return_code} eq "ERROR";
            }
            
            if ( $hashDtls->{return_code} eq "SUCCESS" ) {
        
                if ( $actTR eq "1" ) {
                    my $SQL = "SELECT COUNT(*) FROM language WHERE lng_code = '$src' AND lng_quote_tgt_lng_list LIKE '%" . $tgt . "%'";
                    if ( $shared->getSQLCount( $SQL ) > 0 ) {
                        my $rec = $shared->getResultRec( "SELECT * FROM language WHERE lng_code = '$src' AND lng_quote_tgt_lng_list LIKE '%" . $tgt . "%'" );
                        $tRate  = $rec->{lng_quote_tgt_trans_rate};
                    }
                    
                    my $tr_amt = $sdt->calcAnalysis( $hashDtls, "T", $tRate, $hRate );
                    my $wCnt   = ( $hashDtls->{cnt_wtot} ) + 0;
                    my $dspD   = "Translation of $desc from $sLang into $tLang";
                    
                    my $insert = "INSERT INTO quote_line ( qtl_qte_id, qtl_type, qtl_desc, qtl_doc_name, qtl_qty, qtl_rate, qtl_amount )\n"
                               . "VALUES ( $qte_id, 'A-TR', '$dspD', '$docNm', $wCnt, $tRate, $tr_amt )";
                    $shared->doSQLUpdate( $insert );
                }    
                
                if ( $actED eq "1" ) {
                    my $ed_amt = $sdt->calcAnalysis( $hashDtls, "E", $eRate, $hRate );
                    my $wCnt   = ( $hashDtls->{cnt_wtot} ) + 0;
                    my $dspD   = "Review of $desc from $sLang into $tLang";
                    my $insert = "INSERT INTO quote_line ( qtl_qte_id, qtl_type, qtl_desc, qtl_doc_name, qtl_qty, qtl_rate, qtl_amount )\n"
                               . "VALUES ( $qte_id, 'B-ED', '$dspD', '$docNm', $wCnt, $eRate, $ed_amt )";
                    $shared->doSQLUpdate( $insert );
                }
                
                if ( $actPF eq "1" ) {
                    my $pf_amt = $sdt->calcAnalysis( $hashDtls, "P", $hRate, $hRate );
                    my $wCnt   = ( $hashDtls->{cnt_wtot} ) + 0;
                    my $dspD   = "Proof reading of $desc from $sLang into $tLang";                    
                    my $insert = "INSERT INTO quote_line ( qtl_qte_id, qtl_type, qtl_desc, qtl_doc_name, qtl_qty, qtl_rate, qtl_amount )\n"
                               . "VALUES ( $qte_id, 'C-PF', '$dspD', '$docNm', $wCnt, $hRate, $pf_amt )";
                    $shared->doSQLUpdate( $insert );
                }
                
                if ( $actMT eq "1" ) {
                    my $mt_amt = $sdt->calcAnalysis( $hashDtls, "M", $mRate, $hRate );
                    my $wCnt   = ( $hashDtls->{cnt_wtot} ) + 0;
                    my $dspD   = "MTPE for $desc from $sLang into $tLang";
                    my $insert = "INSERT INTO quote_line ( qtl_qte_id, qtl_type, qtl_desc, qtl_doc_name, qtl_qty, qtl_rate, qtl_amount )\n"
                               . "VALUES ( $qte_id, 'D-MTPE', '$dspD', '$docNm', $wCnt, $mRate, $mt_amt )";
                    $shared->doSQLUpdate( $insert );
                }
                
                
            } else {
                $java_msg = "Problem parsing analysis file. File must be tab-delimited text (txt extension).";
            }
        } else {
            $java_msg = "Problem importing analysis file.  Contact administrator.";
        }
    } elsif ( $qType eq "O" ) {
        
        my $insert = "INSERT INTO quote_line ( qtl_qte_id, qtl_type, qtl_desc, qtl_amount )\n"
                   . "VALUES ( $qte_id, 'D-OTH', '$desc', $amt )";
        $shared->doSQLUpdate( $insert );
        
        
    } elsif ( $qType eq "D" ) {

        my $insert = "INSERT INTO quote_line ( qtl_qte_id, qtl_type, qtl_desc, qtl_amount )\n"
                   . "VALUES ( $qte_id, 'E-DSC', '$desc', $amt )";
        $shared->doSQLUpdate( $insert );
    
    } elsif ( $qType eq "P" ) {

        my $insert = "INSERT INTO quote_line ( qtl_qte_id, qtl_type, qtl_desc, qtl_rate )\n"
                   . "VALUES ( $qte_id, 'F-PCT', '$desc', $pct )";
        $shared->doSQLUpdate( $insert );
        
    }
       
    
    $sdt->updateQuote( $qte_id );
   	        
    &edit_qte_form( $qte_id );

}

############################################################
# Form to edit the quote item
############################################################
sub edit_item_form {

    my $qte_id = $shared->get_cgi( "qte_id" );
    my $qtl_id = $shared->get_cgi( "qtl_id" );
    
    my $qte_rec = $shared->get_rec( "quote", "qte_id", $qte_id );    
    my $cln_id  = $qte_rec->{qte_cln_id};
    my $pjno    = $qte_rec->{qte_prj_no};
    my $qStat   = $qte_rec->{qte_status};

    my $cln_rec = $shared->get_rec( "client", "cln_id", $cln_id );
    my $clnOth  = $cln_rec->{cln_other};
    my $cName   = $cln_rec->{cln_name};
    
    if ( $clnOth eq "Y" ) {
        $cName  = "Other: " . $qte_rec->{qte_oth_name};
    }

    my $qtl_rec = $shared->get_rec( "quote_line", "qtl_id", $qtl_id );
    my $iDesc   = $qtl_rec->{qtl_desc};
    my $iDocNm  = $qtl_rec->{qtl_doc_name};
    my $iType   = $qtl_rec->{qtl_type};
    my $iQty    = $qtl_rec->{qtl_qty};
    my $iRate   = $shared->fmtNumber( $qtl_rec->{qtl_rate}, 1 );
    my $iAmt    = $qtl_rec->{qtl_amount};
    
    if ( $qStat eq "ACCEPTED" ) {
        $conf->{form_head}    = "View Quote Line Item";
        $conf->{form_start}   = "";
        $conf->{f_desc}       = $iDesc;
    } else {
        $conf->{form_head}    = "Edit Quote Line Item";
        $conf->{form_start}   = "<form method=post name=adm_qtl_form action=\"$cgi_url\">\n"
        	                  . "<input type=hidden name=action value=\"edit_item\">\n"
    	                      . "<input type=hidden name=qte_id value=\"$qte_id\">\n"
    	                      . "<input type=hidden name=qtl_id value=\"$qtl_id\">\n"
    	                      . $inp_srch . "\n";
        $conf->{f_desc}       = "<textarea name=qtl_desc cols=60 rows=3 wrap class=form>$iDesc</textarea><br>\n"
                              . "Document Name: <input name=qtl_doc_name value=\"$iDocNm\" size=60 maxlength=100 class=form>";
    }
   	$conf->{f_client}     = $cName;
   	$conf->{f_pjno}       = $pjno;
    $conf->{lbl_amount}   = "Item Amount";
        
    if ( $qStat eq "ACCEPTED" ) {
        if ( ( $iType eq "A-TR" ) || ( $iType eq "B-ED" ) || ( $iType eq "C-PF" ) || ( $iType eq "D-MTPE" ) ) {
            $conf->{f_amount} = "Word/Char Cnt: <b>$iQty</b><br>Rate: <b>\$$iRate</b><br>Total Amt: <b>\$$iAmt</b>";
        } elsif ( $iType eq "E-DSC" ) {
            $conf->{f_amount} = $shared->fmtCurrency( $iAmt );
        } elsif ( $iType eq "F-PCT" ) {
            $conf->{f_amount} = $shared->fmtNumber( $iRate, 1 ) . "\%";
        }
    } else {
        if ( ( $iType eq "A-TR" ) || ( $iType eq "B-ED" ) || ( $iType eq "C-PF" ) || ( $iType eq "D-MTPE" ) ) {
            $conf->{f_amount} = "Word/Char Cnt: <b>$iQty</b><br>Rate: <b>\$$iRate</b><br>Total Amt: <b>\$$iAmt</b>";
        } elsif ( $iType eq "E-DSC" ) {
            $conf->{f_amount} = "<input name=qtl_amount value=\"$iAmt\" size=12 maxlength=16 class=form>";
        } elsif ( $iType eq "F-PCT" ) {
            $conf->{f_amount} = "<input name=qtl_rate value=\"$iRate\" size=8 maxlength=16 class=form>\%";
        }
    }        
    
    if ( $iType eq "A-TR" ) {
        $conf->{f_type} = "Translation";
    } elsif ( $iType eq "B-ED" ) {
        $conf->{f_type} = "Review";
    } elsif ( $iType eq "C-PF" ) {
        $conf->{f_type} = "Proof Reading";
    } elsif ( $iType eq "D-MTPE" ) {
        $conf->{f_type} = "MTPE";
    } elsif ( $iType eq "D-OTH" ) {
        $conf->{f_type} = "Other Fee";
    } elsif ( $iType eq "E-DSC" ) {
        $conf->{f_type} = "Discount (Amt)";
    } elsif ( $iType eq "F-PCT" ) {
        $conf->{f_type} = "Discount (Pct)";
    }
    
    if ( $qStat eq "ACCEPTED" ) {
        $conf->{form_submit}  = "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	    $conf->{form_end}     = "";
   	} else {
        $conf->{form_submit}  = "<input type=submit value=\"Update\" class=form>&nbsp;\n"
                              . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	    $conf->{form_end}     = "</form>";
   	}
    	
    my $pg_body = &disp_form( "qtl_edit_form" );
        
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );

}

############################################################
# Edit the quote item
############################################################
sub edit_item {

    my $qte_id = $shared->get_cgi( "qte_id" );
    my $qtl_id = $shared->get_cgi( "qtl_id" );
    
    my $qtl_rec = $shared->get_rec( "quote_line", "qtl_id", $qtl_id );
    my $iType   = $qtl_rec->{qtl_type};
    my $iDesc   = $shared->get_cgi( "qtl_desc" );
    my $iDocNm  = $shared->get_cgi( "qtl_doc_name" );
    my $iAmt    = ( $shared->get_cgi( "qtl_amount" ) + 0 );
    my $iPct    = ( $shared->get_cgi( "qtl_rate" ) + 0 );    
    
    $iDesc     =~ s/'/\\'/g;
    $iDocNm    =~ s/'/\\'/g;
    
    my $update  = "UPDATE quote_line SET qtl_desc = '$iDesc', qtl_doc_name = '$iDocNm', qtl_amount = $iAmt WHERE qtl_id = $qtl_id";
    if ( ( $iType eq "A-TR" ) || ( $iType eq "B-ED" ) || ( $iType eq "C-PF" ) || ( $iType eq "D-MTPE" ) ) {
        $update = "UPDATE quote_line SET qtl_desc = '$iDesc', qtl_doc_name = '$iDocNm' WHERE qtl_id = $qtl_id";
    } elsif ( $iType eq "F-PCT" ) {
        $update = "UPDATE quote_line SET qtl_desc = '$iDesc', qtl_doc_name = '$iDocNm', qtl_rate = $iPct WHERE qtl_id = $qtl_id";
    }
        
    $shared->doSQLUpdate( $update );
    
    $sdt->updateQuote( $qte_id );
       	        
    &edit_qte_form( $qte_id );

}

############################################################
# Remove item from quote
############################################################
sub del_item {

    my $qte_id = $shared->get_cgi( "qte_id" );
    
   	my $qtl_id    = $shared->get_cgi( "qtl_id" );
    	
    my $update    = "DELETE FROM quote_line WHERE qtl_id = $qtl_id";
    $shared->doSQLUpdate( $update );
    
    $sdt->updateQuote( $qte_id );
    
    &edit_qte_form( $qte_id );

}

###########################################################                   
# Create client selection list
#----------------------------------------------------------
sub select_client {

    my ( $selAll ) = @_;
    
    my $result = "";
    
    my $SQL = "SELECT cln_id, cln_name\n"
            . "FROM client\n"
            . "WHERE ( cln_type = 'B' OR ( cln_type = 'P' AND cln_personal_usr_id = $usr_id ) )\n"
            . "ORDER BY cln_name";
    if ( $selAll eq "N" ) {
        $SQL = "SELECT cln_id, cln_name\n"
             . "FROM client\n"
             . "WHERE ( cln_type = 'B' OR ( cln_type = 'P' AND cln_personal_usr_id = $usr_id ) )\n"
             . "AND cln_active = 'Y'\n"
             . "ORDER BY cln_name";
    }
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $id   = $rec->{cln_id};
        my $desc = $rec->{cln_name};
        $result .= "<option value=\"$id\">$desc</option>\n";
    }
    
    return $result;
}

###########################################################                   
# Create contact selection list
#----------------------------------------------------------
sub select_contact {

    my ( $cln_id, $dflt ) = @_;
    
    my $result = "";
    
    my $SQL = "SELECT clc_id, clc_contact FROM client_contact WHERE clc_cln_id = $cln_id ORDER BY clc_contact";
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $id   = $rec->{clc_id};
        my $desc = $rec->{clc_contact};
        if ( $id == $dflt ) {
            $result .= "<option selected value=\"$id\">$desc</option>\n";
        } else {
            $result .= "<option value=\"$id\">$desc</option>\n";
        }
    }
    
    return $result;
}



###########################################################                   
# generate new Quote Proj No
#----------------------------------------------------------
sub genNewQPN {

    my $next = $shared->getConfigInt( "ADM_QTPRJNO" );
    
    my $prefix = "Q";
    my $result = "";
    
    my $good   = "N";
    while ( $good eq "N" ) {
        my $test = $prefix . sprintf( "%05s", $next );
        my $cnt  = $shared->getSQLCount( "SELECT COUNT(*) FROM quote WHERE qte_prj_no = '$test'" );
        if ( $cnt == 0 ) {
            $good = "Y";
            $result = $test;
        }
        $next++;
    }
    
    $shared->setConfig( "ADM_QTPRJNO", $next );
    
    return $result;
}

##########################################################                   
# Get Languages as selection list
#----------------------------------------------------------
sub getLang {
    
    my $SQL = "SELECT * FROM language ORDER BY lng_desc";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{lng_code};
        my $desc     = $rec->{lng_desc};
        $result .= "<option value=\"$code\">$desc</option>";
    }
    
    return $result;
}
