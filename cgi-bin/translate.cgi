#!/usr/bin/perl
###########################################################################
# translate.cgi                Translation Module
###########################################################################
# Copyright 2013, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/translate.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($out_path)    = $config->get_out_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_bg4     = $colors{ "BG4" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $debug         = $config->get_debug;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;
$conf->{color_bg4}   = $color_bg4;

my $srch_bg          = $shared->get_cgi( "srch_bg" );
my $srch_bid         = $shared->get_cgi( "srch_bid" );
my $srch_slid        = $shared->get_cgi( "srch_slid" );
my $srch_tlid        = $shared->get_cgi( "srch_tlid" );
my $srch_cln         = $shared->get_cgi( "srch_cln" );
my $srch_stat        = $shared->get_cgi( "srch_stat" );

my $inp_srch     = "";
my $sp_vars      = "";
if ( $srch_bid gt "" ) {
	$inp_srch .= "<input type=hidden name=srch_bid value=\"$srch_bid\">\n";
   	$sp_vars  .= "&srch_bid=$srch_bid";
}
if ( $srch_slid > 0 ) {
   	$inp_srch .= "<input type=hidden name=srch_slid value=\"$srch_slid\">\n";
   	$sp_vars  .= "&srch_slid=$srch_slid";
}
if ( $srch_tlid > 0 ) {
   	$inp_srch .= "<input type=hidden name=srch_tlid value=\"$srch_tlid\">\n";
   	$sp_vars  .= "&srch_tlid=$srch_tlid";
}
if ( $srch_cln gt "" ) {
    $inp_srch .= "<input type=hidden name=srch_cln value=\"$srch_cln\">\n";
    $sp_vars  .= "&srch_cln=$srch_cln";
}
if ( $srch_stat gt "" ) {
    $inp_srch  .= "<input type=hidden name=srch_stat value=\"$srch_stat\">\n";
    $sp_vars   .= "&srch_stat=$srch_stat";
}
if ( $srch_bg gt "" ) {
    $inp_srch  .= "<input type=hidden name=srch_bg value=\"$srch_bg\">\n";
    $sp_vars   .= "&srch_pg=$srch_bg";
}


# Main program
my $usr_id = 0;
my $uType  = "ADM";
my $uLink  = 0;
my $action = $shared->get_cgi( "action" );

if ( $auth->validate_access( "translate" ) ) {

    my ($action)    = $shared->get_cgi( "action" );
	$usr_id         = $auth->getCurrentUser;

    my $uRec        = $shared->get_rec( "user", "usr_id", $usr_id );
    $uType          = $uRec->{usr_type};
    $uLink          = $uRec->{usr_link_id};
    $conf->{menu}   = &getMenu( $uType, $action );
    
    if ( ( $action eq "import_form" ) && ( ( $uType eq "ADM" ) || ( $uType eq "CLN" ) ) ) {
        &import_form( $uType, $uLink );
    } elsif ( ( $action eq "import" ) && ( ( $uType eq "ADM" ) || ( $uType eq "CLN" ) ) )  {
        &import( $uType, $uLink );
    } elsif ( $action eq "view_batch" ) {
        &view_batch( $uType, $uLink );
    } elsif ( ( $action eq "asgn_batch_form" ) && ( $uType eq "ADM" ) ) {
        &asgn_batch_form( $uType, $uLink );
    } elsif ( ( $action eq "asgn_batch" ) && ( $uType eq "ADM" ) ) {
        &asgn_batch( $uType, $uLink );
    } elsif ( ( $action eq "rsv_batch" ) && ( $uType eq "LIG" ) ) {
        &rsv_batch( $uType, $uLink );
    } elsif ( $action eq "update_batch" ) {
        &update_batch( $uType, $uLink );
        my $content = &getBatchList( $uType, $uLink );
        &print_main( $content );
    } elsif ( $action eq "cpy_batch_form" ) {
        &cpy_batch_form( $uType, $uLink );
    } elsif ( $action eq "cpy_batch" ) {
        &cpy_batch( $uType, $uLink );
    } elsif ( $action eq "del_batch" ) {
        &del_batch( $uType, $uLink );
        my $content = &getBatchList( $uType, $uLink );
        &print_main( $content );
    } elsif ( $action eq "exp_batch" ) {
        &exp_batch( $uType );
        &view_batch( $uType, $uLink );
    } else {
        my $content = &getBatchList( $uType, $uLink );
        &print_main( $content );
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my ( $content )   = @_;
    
    $conf->{content}  = $content;
    
    my $pg_body              = &disp_form( "tra_main" );
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Import Form
#----------------------------------------------------------
sub import_form {

    my ( $uType, $uLink ) = @_;
    
    my $content = "";
    
    $content .= "<h2>Import File for Translation</h2>\n";
	$content .= "<form method=post action=\"$cgi_url\" enctype=\"multipart/form-data\">\n";
	$content .= "<input type=hidden name=action value=\"import\">\n";
	
	if ( $uType eq "CLN" ) {
	    $content .= "<input type=hidden name=src_cln_id value=\"$uLink\">\n";
	}
	
    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=820>\n";
    $content .= "<tr><td valign=top align=left class=form colspan=2>Instructions: Import file must be a text file\n";
    $content .= "with one phrase or sentence per line.  Phrase or sentence must be 200 characters or less (including spaces\n";
    $content .= "and punctuation).  The system will validate the file upon import and give you a success or failure\n";
    $content .= "indication.</td></tr>\n";
    
    my $cFld  = "";
    if ( $uType eq "CLN" ) {
        $cFld = $shared->getFieldVal( "client", "cln_name", "cln_id", $uLink );
    } elsif ( $uType eq "ADM" ) {
        $cFld = "<select name=src_cln_id class=form>" . &getCLNSelect . "</select>";
    }
    
    my $bgName = "Translation Import #" . $shared->getDateStamp( "Y" );
    my $tgtLst = &getLangChkBoxList;
    
    $content .= "<tr><td valign=top align=left class=label>Client</td>\n";
    $content .= "<td valign=top align=left class=form>$cFld</td></tr>\n";
    $content .= "<tr><td valign=top align=left class=label>Batch Group Name</td>\n";
    $content .= "<td valign=top align=left class=form><input name=bg_name value=\"$bgName\" size=60 maxlength=80 class=form></td></tr>\n";
    $content .= "<tr><td valign=top align=left class=label>Source Language</td>\n";
    $content .= "<td valign=top align=left class=form><select name=src_lng_id class=form>" . &getLang . "</select></td></tr>\n";
    $content .= "<tr><td valign=top align=left class=label>Translate To</td>\n";
    $content .= "<td valign=top align=left class=form>" . $tgtLst . "</td></tr>\n";
    $content .= "<tr><td valign=top align=left class=label>File to Import</td>\n";
    $content .= "<td valign=top align=left class=form><input type=file name=imp_file class=form size=40></td></tr>\n";
    $content .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
    $content .= "<td valign=top align=left class=form><input type=submit class=form value=\"Import Now\"></td></tr>\n";
    $content .= "</table>\n";
    $content .= "</form>\n";
    
    $conf->{content} = $content;
    
    my $pg_body    = &disp_form( "tra_main" );
    $env->{page_body}            = $pg_body;
    $shared->page_gen( $env, $java_msg );

}

###########################################################                   
# Import Data
#----------------------------------------------------------
sub import {

    my ( $uType, $uLink ) = @_;
    
	my $file     = "$imp_path/last_tra_file.txt";
	my $error    = $shared->uploadFile( "imp_file", $file, "" );
	if ( $error == 0 ) {
		
		system( "/usr/bin/dos2unix $file" );
		
		open( FILE, "$file" );
		my @FILE = <FILE>;
		close( FILE );
		
		my $cln_id = $shared->get_cgi( "src_cln_id" );
		my $bgName = $shared->get_cgi( "bg_name" );
		my $src_id = $shared->get_cgi( "src_lng_id" );
		
		my @tgtLst = &getTgtList;
		my $clg_id = 0;
		my $batCnt = 0;
		
		my $cntRows   = @FILE;
		
		if ( $cntRows > 1 ) {

            my $trunc_cnt = 0;
            my $lnCnt     = 0;
            my $wordCnt   = 0;
            my @lines;

            foreach my $line ( @FILE ) {
		
                chomp( $line );
	            $line =~ s/\"//g;
		    
	            if ( length( $line ) > 200 ) {
	                $trunc_cnt++;
	            }
		    
	            push( @lines, $line );
	            $wordCnt += &getWordCnt( $line );
            }

            if ( $wordCnt > 0 ) {
		        $bgName   =~ s/'/\\'/g;
		        my $bgIns  = "INSERT INTO client_batch_group ( clg_cln_id, clg_name, clg_add_date ) VALUES ( $cln_id, '$bgName', now() )";
		        $shared->doSQLUpdate( $bgIns );
		        $clg_id    = $shared->getSQLLastInsertID;

    		    foreach my $tgt_id ( @tgtLst ) {
		        
		            my $batIns = "INSERT INTO client_batch ( clb_cln_id, clb_clg_id, clb_src_lng_id, clb_tgt_lng_id, clb_add_date, clb_word_cnt )\n"
		                       . "VALUES ( $cln_id, $clg_id, $src_id, $tgt_id, now(), $wordCnt )";
		            $shared->doSQLUpdate( $batIns );
		            my $clb_id = $shared->getSQLLastInsertID;
		
        		    if ( $clb_id > 0 ) {    

		                foreach my $line ( @lines ) {
		                    $line =~ s/'/\\'/g;
                            my $insert = "INSERT INTO client_translation ( clt_clg_id, clt_clb_id, clt_cln_id, clt_src_lng_id, clt_tgt_lng_id, clt_src_text, clt_add_date )\n"
		                               . "VALUES ( $clg_id, $clb_id, $cln_id, $src_id, $tgt_id, '$line', now() )";
		                    $shared->doSQLUpdate( $insert );
		                }
		            
		                $batCnt++;
		            
                    }
                }
            
                if ( $batCnt == 0 ) {
                    my $update = "DELETE FROM client_batch_group WHERE clg_id = $clg_id";
                    $shared->doSQLUpdate( $update );
                    $java_msg  = "ERROR: No Target Languages Selected";
                } elsif ( $trunc_cnt > 0 ) {
                    $java_msg  = "WARNING: $trunc_cnt source lines truncated (greater than 200 characters)";
                } else {
                    $java_msg  = "SUCCESS: $batCnt Batches Created";
                }
            } else {
                $java_msg = "ERROR: Total Word Count is 0 - check Import File.";
            }
            
        } else {
            
            $java_msg = "ERROR: Import File contains no data.";
            
        }
        
	} else {
	    
		$java_msg = "ERROR: Problen loading import file!";
		
	}
        
    my $content = &getBatchList( $uType, $uLink );
    &print_main( $content );
}


###########################################################                   
# Get Batch List
#----------------------------------------------------------
sub getBatchList {

    my ( $uType, $uLink ) = @_;
    
    my $result = "";
 
    if ( $uType eq "ADM" ) {
        $result .= &genList( "ALLPEND", 0 );
    } elsif ( $uType eq "CLN" ) {
        $result .= &genList( "ALLCLN", $uLink );
    } elsif ( $uType eq "LIG" ) {
        $result .= &genList( "MYPEND", $uLink );
    }
           
    return $result;
    
}

###########################################################                   
# Generate the Batch List
#----------------------------------------------------------
sub genList {
    
    my ( $listID, $uLink ) = @_;
    
    my $head   = "";
    my $hdRow  = "";
    my $cSpan  = 7;
    my $SQL    = "";
    
    if ( $listID eq "ALLPEND" ) {
        $head  = "All Pending Batches";
        $hdRow = "<tr bgcolor=\"black\">\n"
	           . "<td valign=top align=center class=form><font color=\"white\"><b>Batch ID</b></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Client Name</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Source Language</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Target Language</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Linguist</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Add Date</b></font></td>\n"
	           . "<td valign=top align=center class=form><font color=\"white\"><b>Status/Action</b></font></td></tr>\n";

        my $where = "";
        my $newHd = "";
        if ( $srch_bid gt "" ) {
            my $tmpBID = $srch_bid + 0;
            $newHd = "Searched: Batch ID is $srch_bid";
            $where = "AND clb_id = $tmpBID\n";
        }
        if ( $srch_bg gt "" ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Batch Grp contains $srch_bg";
            } else {
                $newHd .= "Search: Batch Grp contains $srch_bg";
            }
            $where .= "AND clg_name LIKE '%$srch_bg%'\n";
        }
        if ( $srch_slid > 0 ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Src Lang is " . &getLangDesc( $srch_slid );
            } else {
                $newHd .= "Searched: Src Lang is " . &getLangDesc( $srch_slid );
            }
            $where .= "AND clb_src_lng_id = $srch_slid\n";
        }
        if ( $srch_tlid > 0 ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Tgt Lang is " . &getLangDesc( $srch_tlid );
            } else {
                $newHd .= "Searched: Tgt Lang is " . &getLangDesc( $srch_tlid );
            }
            $where .= "AND clb_tgt_lng_id = $srch_tlid\n";
        }
        if ( $srch_cln gt "" ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Client Name contains \"$srch_cln\"";
            } else {
                $newHd .= "Searched: Client Name contains \"$srch_cln\"";
            }
            $where .= "AND cln_name LIKE '%$srch_cln%'\n";
        }
        if ( $srch_stat gt "" ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Status is $srch_stat";
            } else {
                $newHd .= "Searched: Status is $srch_stat";
            }
            if ( $srch_stat eq "PEND" ) {
                $where .= "AND clb_done_date = '0000-00-00'\n";
            } else {
                $where .= "AND clb_done_date <> '0000-00-00'\n";
            }
        }
        
        if ( $newHd gt "" ) {
            $head = $newHd;
        } else {
            $where = "AND clb_done_date = '0000-00-00'\n";
        }    
            
        $SQL   = "SELECT clb_id, cln_name, clb_src_lng_id, clb_tgt_lng_id, clb_add_date, clb_lig_id, lig_last_name, lig_first_name, clb_done_date\n"
               . "FROM client_batch\n"
               . "INNER JOIN client ON clb_cln_id = cln_id\n"
               . "INNER JOIN client_batch_group ON clb_clg_id = clg_id\n"
               . "LEFT JOIN linguist ON clb_lig_id = lig_id\n"
               . "WHERE 1\n"
               . $where
               . "ORDER BY cln_name, clb_add_date";
               
    } elsif ( $listID eq "ALLCLN" ) {
        $head  = "Batches for " . $shared->getFieldVal( "client", "cln_name", "cln_id", $uLink );
        $hdRow = "<tr bgcolor=\"black\">\n"
	           . "<td valign=top align=center class=form><font color=\"white\"><b>Batch ID</b></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Source Language</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Target Language</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Add Date</b></font></td>\n"
	           . "<td valign=top align=center class=form><font color=\"white\"><b>Status/Action</b></font></td></tr>\n";
	    $cSpan = 5;

        my $where = "";
        my $newHd = "";
        if ( $srch_bid gt "" ) {
            my $tmpBID = $srch_bid + 0;
            $newHd = "<br>Searched: Batch ID is $srch_bid";
            $where = "AND clb_id = $tmpBID\n";
        }
        if ( $srch_bg gt "" ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Batch Grp contains $srch_bg";
            } else {
                $newHd .= "<br>Search: Batch Grp contains $srch_bg";
            }
            $where .= "AND clg_name LIKE '%$srch_bg%'\n";
        }
        if ( $srch_slid > 0 ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Src Lang is " . &getLangDesc( $srch_slid );
            } else {
                $newHd .= "<br>Searched: Src Lang is " . &getLangDesc( $srch_slid );
            }
            $where .= "AND clb_src_lng_id = $srch_slid\n";
        }
        if ( $srch_tlid > 0 ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Tgt Lang is " . &getLangDesc( $srch_tlid );
            } else {
                $newHd .= "<br>Searched: Tgt Lang is " . &getLangDesc( $srch_tlid );
            }
            $where .= "AND clb_tgt_lng_id = $srch_tlid\n";
        }
        if ( $srch_stat gt "" ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Status is $srch_stat";
            } else {
                $newHd .= "<br>Searched: Status is $srch_stat";
            }
            if ( $srch_stat eq "PEND" ) {
                $where .= "AND clb_done_date = '0000-00-00'\n";
            } else {
                $where .= "AND clb_done_date <> '0000-00-00'\n";
            }
        }
        
        if ( $newHd gt "" ) {
            $head .= $newHd;
        }    

        $SQL   = "SELECT clb_id, clb_src_lng_id, clb_tgt_lng_id, clb_add_date, clb_done_date\n"
               . "FROM client_batch\n"
               . "INNER JOIN client_batch_group ON clg_id = clb_clg_id\n"
               . "WHERE clb_cln_id = $uLink\n"
               . $where
               . "ORDER BY clb_add_date";
        
    } elsif ( $listID eq "MYPEND" ) {
        $head  = "My Pending Batches";
        $hdRow = "<tr bgcolor=\"black\">\n"
	           . "<td valign=top align=center class=form><font color=\"white\"><b>Batch ID</b></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Client Name</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Source Language</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Target Language</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Add Date</b></font></td>\n"
	           . "<td valign=top align=center class=form><font color=\"white\"><b>Status/Action</b></font></td></tr>\n";
	    $cSpan = 6;

        my $where = "";
        my $newHd = "";
        if ( $srch_bid gt "" ) {
            my $tmpBID = $srch_bid + 0;
            $newHd = "<br>Searched: Batch ID is $srch_bid";
            $where = "AND clb_id = $tmpBID\n";
        }
        if ( $srch_slid > 0 ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Src Lang is " . &getLangDesc( $srch_slid );
            } else {
                $newHd .= "<br>Searched: Src Lang is " . &getLangDesc( $srch_slid );
            }
            $where .= "AND clb_src_lng_id = $srch_slid\n";
        }
        if ( $srch_tlid > 0 ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Tgt Lang is " . &getLangDesc( $srch_tlid );
            } else {
                $newHd .= "<br>Searched: Tgt Lang is " . &getLangDesc( $srch_tlid );
            }
            $where .= "AND clb_tgt_lng_id = $srch_tlid\n";
        }
        if ( $srch_stat gt "" ) {
            if ( $newHd gt "" ) {
                $newHd .= ", Status is $srch_stat";
            } else {
                $newHd .= "<br>Searched: Status is $srch_stat";
            }
            if ( $srch_stat eq "PEND" ) {
                $where .= "AND clb_done_date = '0000-00-00'\n";
            } else {
                $where .= "AND clb_done_date <> '0000-00-00'\n";
            }
        } else {
            $where .= "AND clb_done_date = '0000-00-00'\n";
        }
        
        if ( $newHd gt "" ) {
            $head = $newHd;
        }    

        $SQL   = "SELECT clb_id, cln_name, clb_src_lng_id, clb_tgt_lng_id, clb_add_date, clb_done_date\n"
               . "FROM client_batch\n"
               . "INNER JOIN client ON clb_cln_id = cln_id\n"
               . "WHERE clb_lig_id = $uLink\n"
               . $where
               . "ORDER BY cln_name, clb_add_date";

    } elsif ( $listID eq "READY" ) {
        $head  = "Batches Available for Reservation";
        $hdRow = "<tr bgcolor=\"black\">\n"
	           . "<td valign=top align=center class=form><font color=\"white\"><b>Batch ID</b></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Client Name</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Source Language</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Target Language</b></font></td>\n"
	           . "<td valign=top align=left class=form><font color=\"white\"><b>Add Date</b></font></td>\n"
	           . "<td valign=top align=center class=form><font color=\"white\"><b>Status/Action</b></font></td></tr>\n";
	    $cSpan = 6;
        
        my $wList = "";
        my $LPSQL = "SELECT DISTINCT lil_src_lng_id, lil_tgt_lng_id FROM linguist_lang WHERE lil_lig_id = $uLink";
        my @lprs  = $shared->getResultSet( $LPSQL );
        foreach my $rec ( @lprs ) {
            my $sID = $rec->{lil_src_lng_id};
            my $tID = $rec->{lil_tgt_lng_id};
            if ( $wList eq "" ) {
                $wList .= "( clb_src_lng_id = $sID AND clb_tgt_lng_id = $tID )";
            } else {
                $wList .= " OR ( clb_src_lng_id = $sID AND clb_tgt_lng_id = $tID )";
            }
        }
        $SQL   = "SELECT clb_id, cln_name, clb_src_lng_id, clb_tgt_lng_id, clb_add_date\n"
               . "FROM client_batch\n"
               . "INNER JOIN client ON clb_cln_id = cln_id\n"
               . "WHERE clb_lig_id = 0\n"
               . "AND ( $wList )\n"
               . "ORDER BY cln_name, clb_add_date";
    }

    my $result = "<span class=subtitle>$head</span><br>\n";
    $result   .= "<table border=0 cellpadding=2 cellspacing=0 width=845>\n";
    $result   .= "<tr><td valign=top align=left bgcolor=\"$color_bg2\">\n";
    $result   .= "<table border=0 cellpadding=2 cellspacing=0 width=841>\n";
    $result   .= $hdRow;
    
    my $row  = 0;
    my $row_color;
    my $fndCnt = 0;
    
    my @rs     = $shared->getResultSet( $SQL );
	foreach my $rec ( @rs ) {
	   	my $id      = $rec->{clb_id};
	   	my $name    = $rec->{cln_name};
	   	my $sLng    = &getLangDesc( $rec->{clb_src_lng_id} );
	   	my $tLng    = &getLangDesc( $rec->{clb_tgt_lng_id} );
	   	my $aDt     = $shared->getDateFmt( $rec->{clb_add_date} );
	   	my $lID     = $rec->{clb_lig_id};
	   	my $lName   = $shared->trim( $rec->{lig_first_name} ) . " " . $shared->trim( $rec->{lig_last_name} );
	   	my $dDt     = $rec->{clb_done_date};
	   	
	   	my $sAct    = "<a href=\"$cgi_url?action=view_batch&clb_id=$id" . $sp_vars . "\" class=nav>[ View ]</a>";
	   	if ( ( $listID eq "ALLPEND" ) && ( $lID == 0 ) ) {
	   	    $sAct  .= "&nbsp;<a href=\"$cgi_url?action=asgn_batch_form&clb_id=$id" . $sp_vars . "\" class=nav>[ Assign ]</a>";
	   	} elsif ( $listID eq "READY" ) {
	   	    $sAct   = "<a href=\"$cgi_url?action=rsv_batch&clb_id=$id" . $sp_vars . "\" class=nav>[ Reserve and Work ]</a>";
	   	} 
	   	
	   	if ( $row == 0 ) {
	   		$row_color = $color_bg2;
	   		$row = 1;
	   	} else {
	   		$row_color = $color_bg1;
	   		$row = 0;
	   	}

        $result .= "<tr bgcolor=\"$row_color\">\n";
	    $result .= "<td valign=top align=center class=form>$id</td>\n";
	    if ( $listID eq "ALLPEND" || $listID eq "MYPEND" ) {
	        $result .= "<td valign=top align=left class=form>$name</td>\n";
	    }
	    $result .= "<td valign=top align=left class=form>$sLng</td>\n";
	    $result .= "<td valign=top align=left class=form>$tLng</td>\n";
	    if ( $listID eq "ALLPEND" ) {
	        $result .= "<td valign=top align=left class=form>$lName</td>\n";
	    }
	    $result .= "<td valign=top align=left class=form>$aDt</td>\n";
	    $result .= "<td valign=top align=center class=form>$sAct</td></tr>\n";
	    $fndCnt++;
	}

    if ( $fndCnt == 0 ) {
        $result .= "<tr><td valign=top align=left class=form colspan=$cSpan><b>No Batches Found</b></td></tr>\n";
    }
    
    $result .= "</table>\n";
    $result .= "</td></tr>\n";
    $result .= "</table>\n";
    
    return $result;

}

##########################################################                   
# Reserve Batch
#----------------------------------------------------------
sub rsv_batch {
    
    my ( $uType, $uLink ) = @_;
    
    my $bID     = $shared->get_cgi( "clb_id" );
    my $bRec    = $shared->get_rec( "client_batch", "clb_id", $bID );
    my $lID     = $bRec->{clb_lig_id};
    my $msg     = "";
    if ( $lID > 0 ) {
        $msg    = "Unable to Reserve Batch - Already Assigned";
    }
    if ( $uType ne "LIG" ) {
        $msg    = "Access will not allow reservation";
    }
    if ( $msg eq "" ) {
        my $update  = "UPDATE client_batch SET clb_lig_id = $uLink WHERE clb_id = $bID";
        $shared->doSQLUpdate( $update );
        &view_batch( $uType, $uLink );
    } else {
        $java_msg = $msg;
        my $content = &getBatchList( $uType, $uLink );
        &print_main( $content );
    }
}
        


##########################################################                   
# View the Batch
#----------------------------------------------------------
sub view_batch {
    
    my ( $uType, $uLink ) = @_;
    
    my $bID     = $shared->get_cgi( "clb_id" );
    my $bRec    = $shared->get_rec( "client_batch", "clb_id", $bID );
 	my $sLng    = &getLangDesc( $bRec->{clb_src_lng_id} );
	my $tLng    = &getLangDesc( $bRec->{clb_tgt_lng_id} );
	my $dDt     = $bRec->{clb_done_date};
	my $lID     = $bRec->{clb_lig_id};
	my $cID     = $bRec->{clb_cln_id};
	my $gID     = $bRec->{clb_clg_id};
	my $gName   = $shared->getFieldVal( "client_batch_group", "clg_name", "clg_id", $gID );
	my $lName   = &getLingName( $lID );
	my $cName   = &getCLNName( $cID );
	my $wordCnt = $bRec->{clb_word_cnt};
	my $llID    = $bRec->{clb_lil_id};
	my $tRate   = $shared->getFieldVal( "linguist_lang", "lil_trans_rate", "lil_id", $llID );
	my $cur     = $shared->getFieldVal( "linguist", "lig_currency", "lig_id", $lID );
	
	my $head    = "View Batch";
	my $edit    = 0;
	my $expt    = 0;
	my $delt    = 0;
	my $copy    = 0;
	if ( $uType eq "ADM" ) {
	    $edit   = 1;
	    if ( $dDt ne "0000-00-00" ) {
	        $expt   = 1;
	    }
	    $delt   = 1;
	    if ( $lName eq "" ) {
	        $lName = "<i>Unassigned</i>";
	    }
	    $copy   = 1;
	} elsif ( $uType eq "CLN" ) {
	    if ( $dDt ne "0000-00-00" ) {
	        $expt = 1;
	    }
	    $copy   = 1;
	} elsif ( $uType eq "LIG" ) {
	    $edit   = 1;
	}
	
    my $content = "<span class=subtitle>$head</span><br>\n";
    $content   .= "<table border=1 cellpadding=2 cellspacing=0 width=835>\n";
    $content   .= "<tr><td valign=top align=left bgcolor=\"$color_bg2\">\n";
    $content   .= "<table border=0 cellpadding=2 cellspacing=0 width=831>\n";
    $content   .= "<tr><td valign=top align=right class=label>Batch Group:</td>\n";
    $content   .= "<td valign=top align=left class=form colspan=3><b>$gName</b></td></tr>\n";
    $content   .= "<tr><td valign=top align=right class=label>Batch #:</td>\n";
    $content   .= "<td valign=top align=left class=form>$bID - ($wordCnt words)</td>\n";
    $content   .= "<td valign=top align=right class=label>Client Name:</td>\n";
    $content   .= "<td valign=top align=left class=form>$cName</td></tr>\n";
    if ( ( $uType eq "ADM" ) || ( $uType eq "LIG" ) ) {
        $content   .= "<tr><td valign=top align=right class=label>Assigned Linguist:</td>\n";
        $content   .= "<td valign=top align=left class=form>$lName</td>\n";
        $content   .= "<td valign=top align=right class=label>Linguist Rate:</td>\n";
        $content   .= "<td valign=top align=left class=form>$tRate / word ($cur)</td></tr>\n";
    }
    $content   .= "<tr><td valign=top align=right class=label>Actions:</td>\n";
    $content   .= "<td valgn=top align=left class=form colspan=3>\n";
    
    $content   .= "<a href=\"$cgi_url?action=default" . $sp_vars . "\" class=nav>[ Return to List ]</a>&nbsp;";
    
    if ( $expt ) {
        $content .= "<a href=\"$cgi_url?action=exp_batch&clb_id=$bID" . $sp_vars . "\" class=nav>[ Export Batch ]</a>&nbsp;";
    }
    if ( $copy ) {
        $content .= "<a href=\"$cgi_url?action=cpy_batch_form&clb_id=$bID\" class=nav>[ Copy Batch ]</a>&nbsp;";
    }
    if ( $delt ) {
        $content .= "<a href=\"$cgi_url?action=del_batch&clb_id=$bID" . $sp_vars . "\" class=nav onClick=\"return confirm('Delete Batch from Database?')\">[ Delete Batch ]</a>&nbsp;";
    }
    
    $content   .= "</td></tr>\n";
    $content   .= "</table>\n";
    $content   .= "</td></tr>\n";
    $content   .= "</table>\n";
    
    $content   .= "<br>\n";
    if ( $edit ) {
        $content .= "<form method=post action=$cgi_url>\n";
        $content .= "<input type=hidden name=action value=\"update_batch\">\n";
        $content .= "<input type=hidden name=clb_id value=\"$bID\">\n";
        $content .= $inp_srch;
    }

    $content   .= "<table border=0 cellpadding=2 cellspacing=0 width=835>\n";
    $content   .= "<tr><td valign=top align=left bgcolor=\"$color_bg1\">\n";
    $content   .= "<table border=0 cellpadding=2 cellspacing=0 width=831>\n";
    $content   .= "<tr><td valign=top align=left class=label><b>$sLng</b></td>\n";
    $content   .= "<td valign=top align=left class=label><b>$tLng</b></td></tr>\n";

    my $row  = 0;
    my $row_color;
    
    my $SQL = "SELECT * FROM client_translation WHERE clt_clb_id = $bID ORDER BY clt_id";
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $srcT = $rec->{clt_src_text};
        my $tgtT = $rec->{clt_tgt_text};
        my $id   = $rec->{clt_id};

    	if ( $row == 0 ) {
    		$row_color = $color_bg2;
    		$row = 1;
    	} else {
    		$row_color = $color_bg1;
    		$row = 0;
    	}
        
        $content .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=label>$srcT</td>\n";
        $content .= "<td valign=top align=left class=form>";
        my $fld   = "tgt_" . $id;
        
        if ( $edit ) {
            $content .= "<textarea name=$fld class=form rows=3 cols=80 wrap>$tgtT</textarea>";
        } else {
            $content .= "<b>$tgtT</b>";
        }
        $content .= "</td></tr>\n";
    }        
    
    if ( $edit ) {
        $content .= "<tr><td valign=top align=center colspan=2 class=form><input type=submit value=\"Update Now\" class=form></td></tr>\n";
    }
    
    $content .= "</table>\n";
    $content .= "</td></tr>\n";
    $content .= "</table>\n";
    
    if ( $edit ) {
        $content .= "</form>\n";
    }
    
    $conf->{content}  = $content;
    
    my $pg_body       = &disp_form( "tra_main" );
    $env->{page_body} = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}    

##########################################################                   
# Update the Batch
#----------------------------------------------------------
sub update_batch {
    
    my ( $uType, $uLink ) = @_;
    
    my $bID     = $shared->get_cgi( "clb_id" );
    my $bRec    = $shared->get_rec( "client_batch", "clb_id", $bID );
    my $asgnTo  = $bRec->{clb_lig_id};
    my $emptCnt = 0;
    
    my $SQL     = "SELECT clt_id FROM client_translation WHERE clt_clb_id = $bID ORDER BY clt_id";
    my @rs      = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $id  = $rec->{clt_id};
        my $fld = "tgt_" . $id;
        my $val = $shared->get_cgi( $fld );
        my $prc = ", clt_proc_date = now()";
        if ( $val eq "" ) {
            $emptCnt++;
            $prc = ", clt_proc_date = '0000-00-00'";
        }
        $val    =~ s/'/\\'/g;
        my $update = "UPDATE client_translation SET clt_tgt_text = '$val', clt_proc_usr_id = $usr_id"
                   . $prc . " WHERE clt_id = $id";
        $shared->doSQLUpdate( $update );
    }
    
    my $up2 = "";
    if ( $emptCnt == 0 ) {
        $up2 = "UPDATE client_batch SET clb_done_date = now() WHERE clb_id = $bID";
    } else {
        $up2 = "UPDATE client_batch SET clb_done_date = '0000-00-00' WHERE clb_id = $bID";
    }
    $shared->doSQLUpdate( $up2 );
}

##########################################################                   
# Delete the batch
#----------------------------------------------------------
sub del_batch {
    
    my ( $uType, $uLink ) = @_;
    
    my $bID     = $shared->get_cgi( "clb_id" );
    if ( $uType eq "ADM" ) {
    
        my $update1 = "DELETE FROM client_translation WHERE clt_clb_id = $bID";
        $shared->doSQLUpdate( $update1 );
        my $update2 = "DELETE FROM client_batch WHERE clb_id = $bID";
        $shared->doSQLUpdate( $update2 );
    } else {
        $java_msg = "Unable to delete batch - access denied to function";
    }
}

##########################################################                   
# Export Batch
#----------------------------------------------------------
sub exp_batch {
    
    my ( $uType, $uLink ) = @_;
    
    if ( ( $uType eq "ADM" ) || ( $uType eq "CLN" ) ) {
        
        my $bID     = $shared->get_cgi( "clb_id" );
        my $bRec    = $shared->get_rec( "client_batch", "clb_id", $bID );
        my $sLng    = &getLangDesc( $bRec->{clb_src_lng_id} );
        my $tLng    = &getLangDesc( $bRec->{clb_tgt_lng_id} );
        my $oFile   = "sdt-batch-" . $bID . ".csv";
        
        my $error   = "";
        open( OUTPUT, ">$out_path/$oFile" );
        
        if ( $error eq "" ) {
           print OUTPUT "\"$sLng\",\"$tLng\"\n";
        
            my $SQL     = "SELECT * FROM client_translation WHERE clt_clb_id = $bID ORDER BY clt_id";
            my @rs      = $shared->getResultSet( $SQL );
            foreach my $rec ( @rs ) {
                my $src = $rec->{clt_src_text};
                my $tgt = $rec->{clt_tgt_text};
                $src   =~ s/\"/\\'/g;
                $tgt   =~ s/\"/\\'/g;
                print OUTPUT "\"$src\",\"$tgt\"\n";
            }
        
            close( OUTPUT );
            $env->{download} = $oFile;
            
        } else {
            $java_msg = "Error generating export file.  Contact Administrator.";
        }
    } else {
        $java_msg = "Access to export function is denied.";
    }

}


##########################################################                   
# Assign Batch Form
#----------------------------------------------------------
sub asgn_batch_form {
    
    my ( $uType, $uLink ) = @_;
    
    my $bID     = $shared->get_cgi( "clb_id" );
    my $bRec    = $shared->get_rec( "client_batch", "clb_id", $bID );
    my $slID    = $bRec->{clb_src_lng_id};
    my $tlID    = $bRec->{clb_tgt_lng_id};
 	my $sLng    = &getLangDesc( $slID );
	my $tLng    = &getLangDesc( $tlID );
	my $cID     = $bRec->{clb_cln_id};
	my $cName   = &getCLNName( $cID );
	
	my $head    = "Assign Batch to Linguist";
	
    my $content = "<span class=subtitle>$head</span><br>\n";
    $content   .= "<form method=post action=$cgi_url>\n";
    $content   .= "<input type=hidden name=action value=\"asgn_batch\">\n";
    $content   .= "<input type=hidden name=clb_id value=\"$bID\">\n";
    $content   .= $inp_srch;
    $content   .= "<table border=1 cellpadding=2 cellspacing=0 width=835>\n";
    $content   .= "<tr><td valign=top align=left bgcolor=\"$color_bg2\">\n";
    $content   .= "<table border=0 cellpadding=2 cellspacing=0 width=831>\n";
    $content   .= "<tr><td valign=top align=right class=label>Batch #:</td>\n";
    $content   .= "<td valign=top align=left class=form>$bID</td>\n";
    $content   .= "<td valign=top align=right class=label>Client Name:</td>\n";
    $content   .= "<td valign=top align=left class=form>$cName</td></tr>\n";
    $content   .= "<tr><td valign=top align=right class=label>Source Lang:</td>\n";
    $content   .= "<td valign=top align=left class=form>$sLng</td>\n";
    $content   .= "<td valign=top align=right class=label>Target Lang:</td>\n";
    $content   .= "<td valign=top align=left class=form>$tLng</td></tr>\n";
    $content   .= "<tr><td valign=top align=right class=label>Assign to:</td>\n";
    $content   .= "<td valign=top align=left class=form colspan=3><select name=clb_lig_id class=form>\n";
    $content   .= &getLangLing( $slID, $tlID );
    $content   .= "</select></td></tr>\n";
    $content   .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
    $content   .= "<td valign=top align=left class=form colspan=3><input type=submit value=\"Assign Now\" class=form>&nbsp;\n";
    $content   .= "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form></td></tr>";
    $content   .= "</table>\n";
    $content   .= "</td></tr>\n";
    $content   .= "</table>\n";
    $content   .= "</form>\n";
    
    $conf->{content}  = $content;
    
    my $pg_body       = &disp_form( "tra_main" );
    $env->{page_body} = $pg_body;
    $shared->page_gen( $env, $java_msg );

}

##########################################################                   
# Assign Batch
#----------------------------------------------------------
sub asgn_batch {
    
    my ( $uType, $uLink ) = @_;
    
    my $bID     = $shared->get_cgi( "clb_id" );
    my $bRec    = $shared->get_rec( "client_batch", "clb_id", $bID );
    my $tmp     = $shared->get_cgi( "clb_lig_id" );
    
    my ( $lID, $llID ) = split( "\-", $tmp );
    
    my $update  = "UPDATE client_batch SET clb_lig_id = $lID, clb_lil_id = $llID WHERE clb_id = $bID";
    $shared->doSQLUpdate( $update );

    my $content = &getBatchList( $uType, $uLink );
    &print_main( $content );

}

##########################################################                   
# Copy Batch Form
#----------------------------------------------------------
sub cpy_batch_form {
    
    my ( $uType, $uLink ) = @_;
    
    my $bID     = $shared->get_cgi( "clb_id" );
    my $bRec    = $shared->get_rec( "client_batch", "clb_id", $bID );
    my $slID    = $bRec->{clb_src_lng_id};
    my $tlID    = $bRec->{clb_tgt_lng_id};
 	my $sLng    = &getLangDesc( $slID );
	my $cID     = $bRec->{clb_cln_id};
	my $cName   = &getCLNName( $cID );
	
	my $head    = "Copy Batch to new Target Language";
	
    my $content = "<span class=subtitle>$head</span><br>\n";
    $content   .= "<form method=post action=$cgi_url>\n";
    $content   .= "<input type=hidden name=action value=\"cpy_batch\">\n";
    $content   .= "<input type=hidden name=clb_id value=\"$bID\">\n";
    $content   .= "<table border=1 cellpadding=2 cellspacing=0 width=835>\n";
    $content   .= "<tr><td valign=top align=left bgcolor=\"$color_bg2\">\n";
    $content   .= "<table border=0 cellpadding=2 cellspacing=0 width=831>\n";
    $content   .= "<tr><td valign=top align=right class=label>Batch #:</td>\n";
    $content   .= "<td valign=top align=left class=form>$bID</td>\n";
    if ( $uType eq "ADM" ) {
        $content   .= "<td valign=top align=right class=label>Client Name:</td>\n";
        $content   .= "<td valign=top align=left class=form>$cName</td></tr>\n";
    } else {
        $content   .= "<td valign=top align=left class=label colspan=2>&nbsp;</td></tr>\n";
    }
    
    $content   .= "<tr><td valign=top align=right class=label>Source Lang:</td>\n";
    $content   .= "<td valign=top align=left class=form>$sLng</td>\n";
    $content   .= "<td valign=top align=right class=label>Target Lang:</td>\n";
    
    $content   .= "<td valign=top align=left class=form><select name=clb_tgt_lng_id class=form>" . &getLang( "WHERE lng_id <> $tlID" ) . "</select></td></tr>\n";
    $content   .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
    $content   .= "<td valign=top align=left class=form colspan=3><input type=submit value=\"Copy Now\" class=form>&nbsp;\n";
    $content   .= "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form></td></tr>";
    $content   .= "</table>\n";
    $content   .= "</td></tr>\n";
    $content   .= "</table>\n";
    $content   .= "</form>\n";
    
    $conf->{content}  = $content;
    
    my $pg_body       = &disp_form( "tra_main" );
    $env->{page_body} = $pg_body;
    $shared->page_gen( $env, $java_msg );

}

##########################################################                   
# Copy the Batch to new Target Language
#----------------------------------------------------------
sub cpy_batch {
    
    my ( $uType, $uLink ) = @_;
    
    my $bID     = $shared->get_cgi( "clb_id" );
    my $tlID    = $shared->get_cgi( "clb_tgt_lng_id" );
    my $bRec    = $shared->get_rec( "client_batch", "clb_id", $bID );
    my $slID    = $bRec->{clb_src_lng_id};
    my $cID     = $bRec->{clb_cln_id};
    my $gID     = $bRec->{clb_clg_id};
    
    my $insert  = "INSERT INTO client_batch ( clb_clg_id, clb_cln_id, clb_src_lng_id, clb_tgt_lng_id, clb_add_date )\n"
                . "VALUES ( $gID, $cID, $slID, $tlID, now() )";
    $shared->doSQLUpdate( $insert );
    my $nbID    = $shared->getSQLLastInsertID;
    
    my $wordCnt = 0;
    my $SQL     = "SELECT * FROM client_translation WHERE clt_clb_id = $bID ORDER BY clt_id";
    my @rs      = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $srcTxt = $rec->{clt_src_text};
        $srcTxt   =~ s/'/\\'/g;
     
        $wordCnt  += &getWordCnt( $srcTxt );   
        my $tIns   = "INSERT INTO client_translation ( clt_clg_id, clt_clb_id, clt_cln_id, clt_src_lng_id, clt_tgt_lng_id,\n"
                   . "clt_src_text, clt_add_date ) VALUES ( $gID, $nbID, $cID, $slID, $tlID, '$srcTxt', now() )";
        $shared->doSQLUpdate( $tIns );
    }
    
    if ( $wordCnt > 0 ) {
        my $update  = "UPDATE client_batch SET clb_word_cnt = $wordCnt WHERE clb_id = $nbID";
        $shared->doSQLUpdate( $update );
    }

    my $content = &getBatchList( $uType, $uLink );
    &print_main( $content );

}


##########################################################                   
# Get Languages as selection list
#----------------------------------------------------------
sub getLang {
    
    my ( $filt ) = @_;
    
    my $SQL = "SELECT * FROM language ORDER BY lng_desc";
    if ( $filt gt "" ) {
        $SQL = "SELECT * FROM language $filt ORDER BY lng_desc";
    }
        
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{lng_id};
        my $desc     = $rec->{lng_desc};
        $result .= "<option value=\"$code\">$desc</option>";
    }
    
    return $result;
}

##########################################################                   
# Get Languages as a checkbox grid (3 columns)
#----------------------------------------------------------
sub getLangChkBoxList {
    
    my ( $filt ) = @_;
    
    my $SQL = "SELECT * FROM language ORDER BY lng_desc";
    if ( $filt gt "" ) {
        $SQL = "SELECT * FROM language $filt ORDER BY lng_desc";
    }
    
    my $col    = 1;
    my $result = "<table border=0 cellpadding=2 cellspacing=0>\n";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{lng_id};
        my $desc     = $rec->{lng_desc};
        my $fld      = "lng-" . $code;
        
        if ( $col == 1 ) {
            $result .= "<tr>";
        }
        $result .= "<td valign=top align=left class=form><input type=checkbox name=$fld value=\"Y\" class=form> $desc</td>";
        if ( $col == 3 ) {
            $result .= "</tr>\n";
            $col = 0;
        }
        $col++;
    }
    
    if ( $col == 2 ) {
        $result .= "<td valign=top align=left class=form colspan=2>&nbsp;</td></tr>\n";
    } elsif ( $col == 3 ) {
        $result .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
    }
    
    $result .= "</table>\n";
    
    return $result;
}


##########################################################                   
# Get Languages as a checkbox grid (3 columns)
#----------------------------------------------------------
sub getTgtList {
    
    my ( $filt ) = @_;
    
    my $SQL = "SELECT * FROM language ORDER BY lng_desc";
    if ( $filt gt "" ) {
        $SQL = "SELECT * FROM language $filt ORDER BY lng_desc";
    }
    
    my @result;
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{lng_id};
        my $fld      = "lng-" . $code;

        if ( $shared->get_cgi( $fld ) eq "Y" ) {
            push( @result, $code );
        }
    }
    
    return @result;
}

###########################################################                   
# Get Language Name
#----------------------------------------------------------
sub getLangDesc {
    
    my ( $lID ) = @_;
    
    my $result = $shared->getFieldVal( "language", "lng_desc", "lng_id", $lID );
    
    return $result;
}

###########################################################                   
# Get Linguist Name
#----------------------------------------------------------
sub getLingName {
    
    my ( $lid ) = @_;
    
    my $result  = "";
    if ( $lid > 0 ) {
        my $rec = $shared->getResultRec( "SELECT lig_first_name, lig_last_name FROM linguist WHERE lig_id = $lid" );
        $result = $shared->trim( $rec->{lig_last_name} ) . ", " . $shared->trim( $rec->{lig_first_name} );
        if ( $rec->{lig_last_name} eq "" ) {
            $result = $rec->{lig_first_name};
        }
        if ( $rec->{lig_first_name} eq "" ) {
            $result = $rec->{lig_last_name};
        }
    }
    
    return $result;
}

###########################################################                   
# Get Linguist Name
#----------------------------------------------------------
sub getCLNName {
    
    my ( $cid ) = @_;
    
    my $result  = "";
    if ( $cid > 0 ) {
        my $rec = $shared->getResultRec( "SELECT cln_name FROM client WHERE cln_id = $cid" );
        $result = $rec->{cln_name};
    }
    
    return $result;
}


##########################################################                   
# Get Client List
#----------------------------------------------------------
sub getCLNSelect {
    
    my $SQL = "SELECT * FROM client ORDER BY cln_name";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{cln_id};
        my $desc     = $rec->{cln_name};
        $result .= "<option value=\"$code\">$desc</option>";
    }
    
    return $result;
}

###########################################################                   
# Get Names from translator table as selection list
#----------------------------------------------------------
sub getLangLing {
    
    my ( $src, $tgt ) = @_;
    
    my $SQL = "SELECT * FROM linguist_lang\n"
            . "INNER JOIN linguist ON lil_lig_id = lig_id\n"
            . "WHERE lil_src_lng_id = $src AND lil_tgt_lng_id = $tgt\n"
            . "AND lig_dont_use = 'N'\n"
            . "ORDER BY lig_last_name, lig_first_name";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $name     = $shared->trim( $rec->{lig_last_name} ) . ", " . $shared->trim( $rec->{lig_first_name} );
        if ( $rec->{lig_last_name} eq "" ) {
            $name    = $rec->{lig_first_name};
        }
        if ( $rec->{lig_first_name} eq "" ) {
            $name    = $rec->{lig_last_name};
        }
        my $tid   = $rec->{lig_id};
        my $lid   = $rec->{lil_id};
        my $tRt   = $rec->{lil_trans_rate};
        $result .= "<option value=\"$tid" . "-" . $lid . "\">$name ($tRt)</option>";
    }
    
    return $result;
}

##########################################################                   
# Get Word Count
#----------------------------------------------------------
sub getWordCnt {
    my ( $str ) = @_;
    
    return 1 + ( $str =~ tr{ }{ } );
    
}


##########################################################                   
# Get Module Menu
#----------------------------------------------------------
sub getMenu {
    
    my ( $uType, $action ) = @_;
    
    my $result = "";
    my $dspSearch = 0;
    if ( ( $action eq "" ) || ( $action eq "update_batch" ) || ( $action eq "del_batch" ) ||
         ( $action eq "srch_batch" ) || ( $action eq "default" ) ) {
        $dspSearch = 1;
    }
    
    $result .= "<a href=\"$cgi_url\" class=adminlink>Batch List</a><br>\n";
    
    if ( $dspSearch ) {
        $result .= &getBatchSearch( $uType );
    }
    
    if ( ( $uType eq "ADM" ) || ( $uType eq "CLN" ) ) {
        $result .= "<a href=\"$cgi_url?action=import_form\" class=adminlink>Import File</a><br>\n";
    }
    
    return $result;
}

##########################################################                   
# Get Search Box
#----------------------------------------------------------
sub getBatchSearch {
    
    my ( $uType ) = @_;
    
    my $result = "";

    $result .= "<form method=post action=$cgi_url>\n";
    $result .= "<input type=hidden name=action value=\"srch_batch\">\n";
    $result .= "<table width=160 cellpadding=2 cellspacing=0 border=0 style=\"align:right;\">\n";
    $result .= "<tr><td align=left valign=top class=form>\n";
    $result .= "Batch Search Options:</td></tr>\n";
    if ( ( $uType eq "ADM" ) || ( $uType eq "CLN" ) ) {
        $result .= "<tr><td align=left valign=top class=label>Batch Group<br>\n";
        $result .= "<input name=srch_bg value=\"\" size=15 class=form></td></tr>\n";
    }
    $result .= "<tr><td align=left valign=top class=label>Batch ID<br>\n";
    $result .= "<input name=srch_bid value=\"\" size=10 class=form></td></tr>\n";
    $result .= "<tr><td align=left valign=top class=label>Src Lang<br>\n";
    $result .= "<select name=srch_slid class=form><option value=\"0\"></option>" . &getLang . "</select></td></tr>\n";
    $result .= "<tr><td align=left valign=top class=label>Tgt Lang<br>\n";
    $result .= "<select name=srch_tlid class=form><option value=\"0\"></option>" . &getLang . "</select></td></tr>\n";
    if ( $uType eq "ADM" ) {
        $result .= "<tr><td aligh=left valign=top class=label>Client<br>\n";
        $result .= "<input name=srch_cln value=\"\" size=15 class=form></td></tr>\n";
    }
    $result .= "<tr><td align=left valign=top class=label>Status<br>\n";
    $result .= "<select name=srch_stat class=form><option value=\"\"></option><option value=\"PEND\">Pending</option><option value=\"DONE\">Completed</a></select></td></tr>\n";
    $result .= "<tr><td align=left valign=top class=form>\n";
    $result .= "<input type=submit value=\"Search Now!\" class=form></td></tr>\n";
    $result .= "</table>\n";
    $result .= "</form><br>\n";
    
    return $result;
}
