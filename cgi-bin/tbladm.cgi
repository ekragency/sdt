#!/usr/bin/perl
###########################################################
# tbladm.cgi
# Utah Federation of Music Clubs - Tables Maintenance
#
# Author: Eric Huber
#   Date: 9/28/2011
#
# Modification History: (Recent at Top)
# -------------------------------------
# 09/28/2011 - PEH - Initial Programming
#
###########################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;
use sitetables;


my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );
my $sitetables = sitetables->new( $shared );

#     
# Variables
#



# HTML template
my ($src_path)    = $config->get_src_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/tbladm.cgi";
my ($img_path)    = $config->get_img_path;
my ($admin_email) = $config->get_admin_email;
my ($base_url)    = $config->get_base_url;
my ($js_path)     = $config->get_js_path;
my ($app_ver)     = $config->get_app_ver;
my $java_msg      = "";


my ($conf);
$conf->{cgi_url}     = $cgi_url;

my (%colors)         = $config->get_colors;
my $color_head       = $colors{ "HEAD1" };
my $color_head2      = $colors{ "HEAD2" };
my $color_bg1        = $colors{ "BG1" };
my $color_bg2        = $colors{ "BG2" };
my $color_bg3        = $colors{ "BG3" };

$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;
$conf->{img_path}    = $img_path;
$conf->{admin_email} = $admin_email;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_head}      = $color_head;
$env->{page_head}       = "Tables Maintenance";
$env->{template}        = "admin.html";
$env->{css}             = "style.css";
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{app_ver}         = $app_ver;


#
# Main Program
#

if ( $auth->validate_access( "imgadm" ) ) {

    my ($action)    = $shared->get_cgi( "action" );

    if ( $action eq "edit_tbl_form" ) {
        &edit_tbl_form;
    } elsif ( $action eq "edit_tbl" ) {
        &edit_tbl;
    } elsif ( $action eq "add_tbl_form" ) {
        &add_tbl_form;
    } elsif ( $action eq "add_tbl" ) {
        &add_tbl;
    } elsif ( $action eq "del_tbl" ) {
        &del_tbl;
    } elsif ( $action eq "view_data" ) {
        &view_data;
    } elsif ( $action eq "edit_data_form" ) {
        &edit_data_form;
    } elsif ( $action eq "edit_data" ) {
        &edit_data;
    } elsif ( $action eq "add_data_form" ) {
        &add_data_form;
    } elsif ( $action eq "add_data" ) {
        &add_data;
    } elsif ( $action eq "del_data" ) {
        &del_data;
    } else {
        &print_main;
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my $content     = "";

    if ( $conf->{search_results} eq "" ) {
    	
	    my $srch_head = "All Tables";
	    	
        my $SQL     = "SELECT * FROM table_def\n"
                    . "ORDER BY tbl_description";
	
	    my @rs   = $shared->getResultSet( $SQL );
	    my $row  = 0;
	    my $row_color;
	    my $fndCnt = 0;
	    
	    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=490>\n";
	    $content .= "<tr><td valign=top align=left class=colhead2 colspan=3>$srch_head</td></tr>\n";
	    foreach my $rec ( @rs ) {
	        my $id   = $rec->{tbl_id};
	        my $code = $rec->{tbl_code};
	        my $desc = $rec->{tbl_description};
	        my $col1 = $rec->{tbl_col_head1};
	        
    	    if ( $row == 0 ) {
    		    $row_color = $color_bg2;
    		    $row = 1;
    	    } else {
    		    $row_color = $color_bg1;
    		    $row = 0;
    	    }
	    	
    	    $content .= "<tr bgcolor=\"$row_color\">\n";
	        $content .= "<td valign=top align=left class=form>$code</td>\n";
	        $content .= "<td valign=top align=left class=form>$desc</td>\n";
	        $content .= "<td valign=top align=right class=form><a href=\"$cgi_url?action=edit_tbl_form&tbl_id=$id\"><img src=\"$img_path/b_props.png\" border=0 alt=\"Table Properties\" title=\"Table Properties\"></a>&nbsp;&nbsp;";
	        
	        if ( $col1 gt "" ) {
	            $content .= "<a href=\"$cgi_url?action=view_data&tbl_id=$id\"><img src=\"$img_path/b_data.png\" border=0 alt=\"Table Data\" title=\"Table Data\"></a>&nbsp;&nbsp;";
	        }
	        
	        $content .= "&nbsp;&nbsp;<a href=\"$cgi_url?action=del_tbl&tbl_id=$id\" onClick=\"return confirm('Permanently remove table from database?')\"><img src=\"$img_path/b_del.png\" border=0 alt=\"Delete\" title=\"Delete Table\"></a></tr>\n";
	    	
	        $fndCnt++;
	    }
	    $content .= "</table>\n";
	    		
	    if ( $fndCnt == 0 ) {
	   	    $content  = "";
    	}
    	
    	$conf->{search_results} = $content;
    	$conf->{tbl_cnt}        = $fndCnt;
    }

    my $pg_body              = &disp_form( "adm_tbl_main" );
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}



############################################################
# Form to add table to database
############################################################
sub add_tbl_form {

    $conf->{form_head}           = "Add New Table";
    $conf->{form_action}         = "add_tbl";
    $conf->{form_submit}         = "Add Table Now";
    
    $conf->{select_style}        = $shared->pop_lookup_select( "TABLE_STYLE" );
    $conf->{select_tbl_align}    = $shared->pop_lookup_select( "TABLE_ALIGN" );
    $conf->{select_col1_align}   = $shared->pop_lookup_select( "TABLE_ALIGN" );
    $conf->{select_col2_align}   = $shared->pop_lookup_select( "TABLE_ALIGN" );
    $conf->{select_col3_align}   = $shared->pop_lookup_select( "TABLE_ALIGN" );
    $conf->{select_col4_align}   = $shared->pop_lookup_select( "TABLE_ALIGN" );
    $conf->{select_col5_align}   = $shared->pop_lookup_select( "TABLE_ALIGN" );
    $conf->{select_col6_align}   = $shared->pop_lookup_select( "TABLE_ALIGN" );
    
    my $pg_body                  = &disp_form( "adm_tbl_form" );
    
    $env->{page_body}            = $pg_body;
    $shared->page_gen( $env, "" );

}

############################################################
# Add the record to the table_def database
############################################################
sub add_tbl {

    my $tbl_code      = $shared->get_cgi( "tbl_code" );
    my $tbl_desc      = $shared->get_cgi( "tbl_description" );
    my $tbl_style     = $shared->get_cgi( "tbl_style" );
    my $tbl_align     = $shared->get_cgi( "tbl_align" );
    my $tbl_width     = ( $shared->get_cgi( "tbl_width" ) ) + 0;
    my $tbl_col1_hd   = $shared->get_cgi( "tbl_col_head1" );
    my $tbl_col1_al   = $shared->get_cgi( "tbl_col_align1" );
    my $tbl_col2_hd   = $shared->get_cgi( "tbl_col_head2" );
    my $tbl_col2_al   = $shared->get_cgi( "tbl_col_align2" );
    my $tbl_col3_hd   = $shared->get_cgi( "tbl_col_head3" );
    my $tbl_col3_al   = $shared->get_cgi( "tbl_col_align3" );
    my $tbl_col4_hd   = $shared->get_cgi( "tbl_col_head4" );
    my $tbl_col4_al   = $shared->get_cgi( "tbl_col_align4" );
    my $tbl_col5_hd   = $shared->get_cgi( "tbl_col_head5" );
    my $tbl_col5_al   = $shared->get_cgi( "tbl_col_align5" );
    my $tbl_col6_hd   = $shared->get_cgi( "tbl_col_head6" );
    my $tbl_col6_al   = $shared->get_cgi( "tbl_col_align6" );
    
    
    $tbl_desc     =~ s/'/\\'/g;
    $tbl_col1_hd  =~ s/'/\\'/g;
    $tbl_col2_hd  =~ s/'/\\'/g;
    $tbl_col3_hd  =~ s/'/\\'/g;
    $tbl_col4_hd  =~ s/'/\\'/g;
    $tbl_col5_hd  =~ s/'/\\'/g;
    $tbl_col6_hd  =~ s/'/\\'/g;
    
    my $insert = "INSERT INTO table_def ( tbl_code, tbl_description, tbl_style, tbl_align, tbl_width,\n"
               . "tbl_col_head1, tbl_col_head2, tbl_col_head3, tbl_col_head4, tbl_col_head5, tbl_col_head6,\n"
               . "tbl_col_align1, tbl_col_align2, tbl_col_align3, tbl_col_align4, tbl_col_align5, tbl_col_align6 )\n"
               . "VALUES ( '$tbl_code', '$tbl_desc', '$tbl_style', '$tbl_align', $tbl_width,\n"
               . "'$tbl_col1_hd', '$tbl_col2_hd', '$tbl_col3_hd', '$tbl_col4_hd', '$tbl_col5_hd', '$tbl_col6_hd',\n"
               . "'$tbl_col1_al', '$tbl_col2_al', '$tbl_col3_al', '$tbl_col4_al', '$tbl_col5_al', '$tbl_col6_al' )";
               
    $shared->doSQLUpdate( $insert );
    
    &print_main;

}

############################################################
# Form to edit member of database
############################################################
sub edit_tbl_form {

    my $tbl_id = $shared->get_cgi( "tbl_id" );
    if ( $tbl_id gt "" ) {
    	
    	my $tbl_rec     = $shared->get_rec( "table_def", "tbl_id", $tbl_id );
    	my $tbl_code    = $tbl_rec->{tbl_code};
    	my $tbl_desc    = $tbl_rec->{tbl_description};
    	my $tbl_style   = $tbl_rec->{tbl_style};
    	my $tbl_align   = $tbl_rec->{tbl_align};
    	my $tbl_width   = $tbl_rec->{tbl_width};
    	my $tbl_col1_hd = $tbl_rec->{tbl_col_head1};
    	my $tbl_col2_hd = $tbl_rec->{tbl_col_head2};
    	my $tbl_col3_hd = $tbl_rec->{tbl_col_head3};
    	my $tbl_col4_hd = $tbl_rec->{tbl_col_head4};
    	my $tbl_col5_hd = $tbl_rec->{tbl_col_head5};
    	my $tbl_col6_hd = $tbl_rec->{tbl_col_head6};
    	my $tbl_col1_al = $tbl_rec->{tbl_col_align1};
    	my $tbl_col2_al = $tbl_rec->{tbl_col_align2};
    	my $tbl_col3_al = $tbl_rec->{tbl_col_align3};
    	my $tbl_col4_al = $tbl_rec->{tbl_col_align4};
    	my $tbl_col5_al = $tbl_rec->{tbl_col_align5};
    	my $tbl_col6_al = $tbl_rec->{tbl_col_align6};
    	
    	$tbl_desc =~ s/\"/&quot;/g;
    	
    	$conf->{form_tbl_id}         = "<input type=hidden name=tbl_id value=\"$tbl_id\">\n";
    	$conf->{tbl_id_row}          = "<tr><td valign=top align=left class=label bgcolor=\"$color_bg3\">Table ID</td>\n"
    	                             . "<td valign=top align=left class=form><b>$tbl_id</b></td></tr>\n";
    	                             
    	$conf->{tbl_code}            = $tbl_code;
    	$conf->{tbl_description}     = $tbl_desc;
    	$conf->{tbl_width}           = $tbl_width;
    	$conf->{tbl_col_head1}       = $tbl_col1_hd;
    	$conf->{tbl_col_head2}       = $tbl_col2_hd;
    	$conf->{tbl_col_head3}       = $tbl_col3_hd;
    	$conf->{tbl_col_head4}       = $tbl_col4_hd;
    	$conf->{tbl_col_head5}       = $tbl_col5_hd;
    	$conf->{tbl_col_head6}       = $tbl_col6_hd;

        $conf->{select_style}        = $shared->pop_lookup_select( "TABLE_STYLE", $tbl_style );
        $conf->{select_tbl_align}    = $shared->pop_lookup_select( "TABLE_ALIGN", $tbl_align );
        $conf->{select_col1_align}   = $shared->pop_lookup_select( "TABLE_ALIGN", $tbl_col1_al );
        $conf->{select_col2_align}   = $shared->pop_lookup_select( "TABLE_ALIGN", $tbl_col2_al );
        $conf->{select_col3_align}   = $shared->pop_lookup_select( "TABLE_ALIGN", $tbl_col3_al );
        $conf->{select_col4_align}   = $shared->pop_lookup_select( "TABLE_ALIGN", $tbl_col4_al );
        $conf->{select_col5_align}   = $shared->pop_lookup_select( "TABLE_ALIGN", $tbl_col5_al );
        $conf->{select_col6_align}   = $shared->pop_lookup_select( "TABLE_ALIGN", $tbl_col6_al );
    	
        $conf->{form_head}           = "Edit Table Properties";
        $conf->{form_action}         = "edit_tbl";
        $conf->{form_submit}         = "Update Now";


        my $pg_body                  = &disp_form( "adm_tbl_form" );
        
        $env->{page_body}            = $pg_body;
        $shared->page_gen( $env, "" );
        
    } else {
    	$java_msg = "Table ID not passed.  Invalid Access.";
    	&print_main;
    }
    
}

############################################################
# Update the record to the calendar database
############################################################
sub edit_tbl {

    my $tbl_id        = $shared->get_cgi( "tbl_id" );
    my $tbl_code      = $shared->get_cgi( "tbl_code" );
    my $tbl_desc      = $shared->get_cgi( "tbl_description" );
    my $tbl_style     = $shared->get_cgi( "tbl_style" );
    my $tbl_align     = $shared->get_cgi( "tbl_align" );
    my $tbl_width     = ( $shared->get_cgi( "tbl_width" ) ) + 0;
    my $tbl_col1_hd   = $shared->get_cgi( "tbl_col_head1" );
    my $tbl_col1_al   = $shared->get_cgi( "tbl_col_align1" );
    my $tbl_col2_hd   = $shared->get_cgi( "tbl_col_head2" );
    my $tbl_col2_al   = $shared->get_cgi( "tbl_col_align2" );
    my $tbl_col3_hd   = $shared->get_cgi( "tbl_col_head3" );
    my $tbl_col3_al   = $shared->get_cgi( "tbl_col_align3" );
    my $tbl_col4_hd   = $shared->get_cgi( "tbl_col_head4" );
    my $tbl_col4_al   = $shared->get_cgi( "tbl_col_align4" );
    my $tbl_col5_hd   = $shared->get_cgi( "tbl_col_head5" );
    my $tbl_col5_al   = $shared->get_cgi( "tbl_col_align5" );
    my $tbl_col6_hd   = $shared->get_cgi( "tbl_col_head6" );
    my $tbl_col6_al   = $shared->get_cgi( "tbl_col_align6" );
    
    
    $tbl_desc     =~ s/'/\\'/g;
    $tbl_col1_hd  =~ s/'/\\'/g;
    $tbl_col2_hd  =~ s/'/\\'/g;
    $tbl_col3_hd  =~ s/'/\\'/g;
    $tbl_col4_hd  =~ s/'/\\'/g;
    $tbl_col5_hd  =~ s/'/\\'/g;
    $tbl_col6_hd  =~ s/'/\\'/g;
    
 
    my $update = "UPDATE table_def SET tbl_code = '$tbl_code', tbl_description = '$tbl_desc',\n"
               . "tbl_style = '$tbl_style', tbl_align = '$tbl_align', tbl_width = $tbl_width,\n"
               . "tbl_col_head1 = '$tbl_col1_hd',\n"
               . "tbl_col_head2 = '$tbl_col2_hd',\n"
               . "tbl_col_head3 = '$tbl_col3_hd',\n"
               . "tbl_col_head4 = '$tbl_col4_hd',\n"
               . "tbl_col_head5 = '$tbl_col5_hd',\n"
               . "tbl_col_head6 = '$tbl_col6_hd',\n"
               . "tbl_col_align1 = '$tbl_col1_al',\n"
               . "tbl_col_align2 = '$tbl_col2_al',\n"
               . "tbl_col_align3 = '$tbl_col3_al',\n"
               . "tbl_col_align4 = '$tbl_col4_al',\n"
               . "tbl_col_align5 = '$tbl_col5_al',\n"
               . "tbl_col_align6 = '$tbl_col6_al'\n"
               . "WHERE tbl_id = $tbl_id";
               
    $shared->doSQLUpdate( $update );
    
    &print_main;

}

###########################################################                   
# Delete Table from Database and remove data records
#----------------------------------------------------------
sub del_tbl {

    my $tbl_id = $shared->get_cgi( "tbl_id" );
    if ( $tbl_id gt "" ) {
    	my $up1 = "DELETE FROM table_data WHERE tbd_tbl_id = $tbl_id";
    	$shared->doSQLUpdate( $up1 );
    	
    	my $up2 = "DELETE FROM table_def WHERE tbl_id = $tbl_id";
    	$shared->doSQLUpdate( $up2 );
    	
    }
    
    &print_main;
}

############################################################
# Print the main admin member maintenance screen
############################################################
sub view_data {

    my $tbl_id      = $shared->get_cgi( "tbl_id" );
    
    my $content     = "";
    
    my $tbl_rec     = $shared->get_rec( "table_def", "tbl_id", $tbl_id );
    my $data_head   = $tbl_rec->{tbl_description};
    my @colProp     = $sitetables->getColumnProps( $tbl_id );

    my $SQL         = "SELECT * FROM table_data\n"
                    . "WHERE tbd_tbl_id = $tbl_id\n"
                    . "ORDER BY tbd_sort, tbd_col1";
	
	my @rs   = $shared->getResultSet( $SQL );
	my $row  = 0;
	my $row_color;
	my $fndCnt = 0;
	
	my $colCnt = @colProp;
	$colCnt++;
	    
	$content  .= "<table border=0 cellpadding=2 cellspacing=0 width=780>\n";
	$content  .= "<tr><td valign=top align=left class=colhead2 colspan=$colCnt>$data_head</td></tr>\n";
	$content  .= "<tr class=colhead>";
	foreach my $col ( @colProp ) {
	    my ( $fld, $head, $align ) = split( "\t", $col );
	    if ( $align eq "R" ) {
	        $content .= "<td valign=top align=right>$head</td>";
	    } elsif ( $align eq "C" ) {
	        $content .= "<td valign=top align=center>$head</td>";
	    } else {
	        $content .= "<td valign=top align=left>$head</td>";
	    }
	}
	$content  .= "<td valign=top align=right>&nbsp;</td></tr>\n";
	
	foreach my $rec ( @rs ) {
	    my $id   = $rec->{tbd_id};
	    
   	    if ( $row == 0 ) {
    	    $row_color = $color_bg2;
    	    $row = 1;
    	} else {
    	    $row_color = $color_bg1;
    		$row = 0;
    	}
	    	
    	$content .= "<tr class=mbody bgcolor=\"$row_color\">\n";

	    foreach my $col ( @colProp ) {
	        my ( $fld, $head, $align ) = split( "\t", $col );

            my $value = $rec->{$fld};
            if ( length( $value ) > 40 ) {
                $value = substr( $value, 0, 40 ) . "...";
            }
            
    	    if ( $align eq "R" ) {
	            $content .= "<td valign=top align=right>$value</td>";
	        } elsif ( $align eq "C" ) {
	            $content .= "<td valign=top align=center>$value</td>";
	        } else {
	            $content .= "<td valign=top align=left>$value</td>";
	        }
	    }
	    
        $content .= "<td valign=top align=right class=form>";
	    $content .= "<a href=\"$cgi_url?action=edit_data_form&tbl_id=$tbl_id&tbd_id=$id\"><img src=\"$img_path/b_data.png\" border=0 alt=\"Edit Row\" title=\"Edit Row\"></a>&nbsp;";
        $content .= "&nbsp;&nbsp;<a href=\"$cgi_url?action=del_data&tbl_id=$tbl_id&tbd_id=$id\" onClick=\"return confirm('Permanently remove row from table?')\"><img src=\"$img_path/b_del.png\" border=0 alt=\"Delete\" title=\"Delete Row\"></a></tr>\n";
        
        $fndCnt++;
    }
	        
	$content .= "</table>\n";
	    		
	if ( $fndCnt == 0 ) {
	   	$content  = "";
    }
    	
    $conf->{search_results} = $content;
    $conf->{row_cnt}        = $fndCnt;
    $conf->{tbl_id}         = $tbl_id;

    my $pg_body              = &disp_form( "adm_data_main" );
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}



############################################################
# Form to add table to database
############################################################
sub add_data_form {

    my $tbl_id   = $shared->get_cgi( "tbl_id" );
    
    my $tbl_rec  = $shared->get_rec( "table_def", "tbl_id", $tbl_id );
    my $tbl_desc = $tbl_rec->{tbl_description};
    
    $conf->{form_head}           = "Add New Row";
    $conf->{data_head}           = $tbl_desc;
    $conf->{form_action}         = "add_data";
    $conf->{form_submit}         = "Add Row Now";
    $conf->{tbl_id}              = $tbl_id;
    
    my @colProps                 = $sitetables->getColumnProps( $tbl_id );

    my $content                  = "";
    foreach my $col ( @colProps ) {
        my ( $fld, $head, $align ) = split( "\t", $col );
        
        $content .= "<tr><td valign=top align=left class=label bgcolor=\"$color_bg3\" width=150>$head</td>\n";
        $content .= "<td valign=top align=left  class=form width=418><textarea name=$fld rows=3 cols=50 wrap></textarea></td></tr>\n";
        
    }
    
    $conf->{tbd_rows}            = $content;
    
    my $pg_body                  = &disp_form( "adm_data_form" );
    
    $env->{page_body}            = $pg_body;
    $shared->page_gen( $env, "" );

}

############################################################
# Add the record to the table_def database
############################################################
sub add_data {

    my $tbl_id        = $shared->get_cgi( "tbl_id" );
    my $tbd_sort      = ( $shared->get_cgi( "tbd_sort" ) ) + 0;

    my @colProps      = $sitetables->getColumnProps( $tbl_id );

    my $SQLFlds       = "";
    my $SQLVals       = "";
    
    foreach my $col ( @colProps ) {
        my ( $fld, $head, $align ) = split( "\t", $col );
        
        $SQLFlds .= ", $fld";
        my $val   = $shared->get_cgi( $fld );
        $val     =~ s/'/\\'/g;
        $SQLVals .= ", '$val'"
        
    }
    
    my $insert = "INSERT INTO table_data ( tbd_tbl_id, tbd_sort" . $SQLFlds . " )\n"
               . "VALUES ( $tbl_id, $tbd_sort" . $SQLVals . " )";
               
    $shared->doSQLUpdate( $insert );
    
    &view_data;

}

############################################################
# Form to edit member of database
############################################################
sub edit_data_form {

    my $tbl_id = $shared->get_cgi( "tbl_id" );
    my $tbd_id = $shared->get_cgi( "tbd_id" );
    
    if ( $tbd_id gt "" ) {
    	
    	my $tbl_rec         = $shared->get_rec( "table_def", "tbl_id", $tbl_id );
    	my $tbd_rec         = $shared->get_rec( "table_data", "tbd_id", $tbd_id );
        my $tbl_desc        = $tbl_rec->{tbl_description};
    
        $conf->{data_head}  = $tbl_desc;
        $conf->{tbl_id}     = $tbl_id;
        $conf->{tbd_sort}   = $tbd_rec->{tbd_sort};
    
        my @colProps        = $sitetables->getColumnProps( $tbl_id );

        my $content         = "";
        foreach my $col ( @colProps ) {
            my ( $fld, $head, $align ) = split( "\t", $col );
            
            my $value       = $tbd_rec->{$fld};
            $value         =~ s/\"/&quot;/g;
        
            $content       .= "<tr><td valign=top align=left class=label bgcolor=\"$color_bg3\" width=150>$head</td>\n";
            $content       .= "<td valign=top align=left  class=form width=418><textarea name=$fld rows=3 cols=50 wrap>$value</textarea></td></tr>\n";
        
        }
    
        $conf->{tbd_rows}   = $content;
    	
    	$conf->{form_tbd_id}         = "<input type=hidden name=tbd_id value=\"$tbd_id\">\n";
    	$conf->{tbd_id_row}          = "<tr><td valign=top align=left class=label bgcolor=\"$color_bg3\">Row ID</td>\n"
    	                             . "<td valign=top align=left class=form><b>$tbd_id</b></td></tr>\n";
    	                             
        $conf->{form_head}           = "Edit Row";
        $conf->{form_action}         = "edit_data";
        $conf->{form_submit}         = "Update Now";


        my $pg_body                  = &disp_form( "adm_data_form" );
        
        $env->{page_body}            = $pg_body;
        $shared->page_gen( $env, "" );
        
    } else {
    	$java_msg = "Row ID not passed.  Invalid Access.";
    	&view_data;
    }
    
}

############################################################
# Update the record to the calendar database
############################################################
sub edit_data {

    my $tbl_id        = $shared->get_cgi( "tbl_id" );
    my $tbd_id        = $shared->get_cgi( "tbd_id" );
    my $tbd_sort      = ( $shared->get_cgi( "tbd_sort" ) ) + 0;

    my @colProps      = $sitetables->getColumnProps( $tbl_id );

    my $SQLUps        = "";
    
    foreach my $col ( @colProps ) {
        my ( $fld, $head, $align ) = split( "\t", $col );
        
        my $val   = $shared->get_cgi( $fld );
        $val     =~ s/'/\\'/g;
        $SQLUps .= ", $fld = '$val'"
        
    }
    
    my $update = "UPDATE table_data SET tbd_sort = $tbd_sort" . $SQLUps . " WHERE tbd_id = $tbd_id";
               
    $shared->doSQLUpdate( $update );
    
    &view_data;

}

###########################################################                   
# Delete row from table
#----------------------------------------------------------
sub del_data {

    my $tbd_id = $shared->get_cgi( "tbd_id" );
    if ( $tbd_id gt "" ) {
    	my $up1 = "DELETE FROM table_data WHERE tbd_id = $tbd_id";
    	$shared->doSQLUpdate( $up1 );
    	
    }
    
    &view_data;
}


