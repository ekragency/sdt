#!/usr/bin/perl
###########################################################################
# lngadm.cgi                Language Administration
###########################################################################
# Copyright 2013, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/lngadm.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $exceptList    = "";
my $dupList       = "";
my $debug         = $config->get_debug;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;


# Main program
my $usr_id = 0;
my $action = $shared->get_cgi( "action" );
my $hash_vars;
my $next_action = $shared->get_cgi( "next_act" );

$hash_vars->{srch_code}    = $shared->get_cgi( "srch_code" );
$hash_vars->{srch_name}    = $shared->get_cgi( "srch_name" );
$hash_vars->{srch_init}    = $shared->get_cgi( "srch_init" );
$hash_vars->{next_act}     = $next_action;

my $srch_name    = $hash_vars->{srch_name};
my $srch_code    = $hash_vars->{srch_code};
my $srch_init    = $hash_vars->{srch_init};
	
my $inp_srch     = "";
my $sp_vars      = "";
if ( $srch_name gt "" ) {
	$inp_srch .= "<input type=hidden name=srch_name value=\"$srch_name\">\n";
   	$sp_vars  .= "&srch_name=$srch_name";
}
if ( $srch_code gt "" ) {
   	$inp_srch .= "<input type=hidden name=srch_code value=\"$srch_code\">\n";
   	$sp_vars  .= "&srch_code=$srch_code";
}
if ( $inp_srch eq "" ) {
  	$inp_srch .= "<input type=hidden name=srch_init value=\"Y\">\n";
   	$sp_vars  .= "&srch_init=Y";
   	$srch_init = "Y";
} else {
    $inp_srch .= "<input type=hidden name=srch_init value=\"N\">\n";
    $sp_vars  .= "&srch_init=N";
    $srch_init = "N";
}


#
# Main Program
#
 
if ( $auth->validate_access( "lngadm" ) ) {
	
	$usr_id    = $auth->getCurrentUser;

    if ( $action eq "search_any" ) {
        &search_any;
    } elsif ( $action eq "edit_lng_form" ) {
        &edit_lng_form;
    } elsif ( $action eq "edit_lng" ) {
        &edit_lng;
    } elsif ( $action eq "add_lng_form" ) {
        &add_lng_form;
    } elsif ( $action eq "add_lng" ) {
        &add_lng;
    } elsif ( $action eq "del_lng" ) {
        &del_lng;
    } else {
        &search_any;
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my $lng_cnt = $shared->getSQLCount( "SELECT count(*) FROM language" );

    if ( $conf->{lng_cnt} eq "" ) {
        $conf->{lng_cnt}     = $lng_cnt;
    }
    
   	my $abContent = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=add_lng_form>\n"
                  . $inp_srch
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Add New Language\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";

 	$conf->{add_button}  = $abContent;
    
    my $pg_body              = &disp_form( "lng_main" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for member
#----------------------------------------------------------
sub search_any {
	
	my $content;
	
	my $srch_head    = "<b>Searched:</b>&nbsp;";
	my $srch_par     = 0;
	
	if ( ( $srch_name gt "" ) || ( $srch_code gt "" ) || ( $srch_init eq "Y" ) ) {
	
	    my $SQL  = "SELECT lng_id, lng_code, lng_desc\n"
	             . "FROM language\n"
	             . "WHERE 1\n";

	    if ( $srch_name gt "" ) {
	    	my $lng_srch_name = $srch_name;
	    	$lng_srch_name =~ s/'/\\'/g;
		    $SQL .= "AND lng_desc LIKE '$lng_srch_name%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Language starts with <b>$srch_name</b>";
		    } else {
		        $srch_head .= "Language starts with <b>$srch_name</b>";
		    }
		    $srch_par++;
	    }
	    if ( $srch_code gt "" ) {
		    $SQL .= "AND lng_code LIKE '$srch_code%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Language code starts with <b>$srch_code</b>";
		    } else {
		        $srch_head .= "Language code starts with <b>$srch_code</b>";
		    }
		    $srch_par++;
	    }
	    
	    $SQL     .= "ORDER BY lng_desc";
	    
	    my $doReset = 0;
	    if ( ( $srch_init eq "Y" ) && ( $srch_par == 0 ) ) {
	        
	    	$srch_head = "All Languages";
	    } else {
	        $doReset = 1;
	    }
	
	    my @rs   = $shared->getResultSet( $SQL );
	    my $row  = 0;
	    my $row_color;
	    my $fndCnt = 0;
	    
	    $content .= "<span class=subhead><b>$srch_head</b></span><br><br>\n";
	    if ( $doReset ) {
	    	$content .= "<a href=\"$cgi_url\" class=nav><b>[ Clear Filter ]</b></a><br>\n";
	    }
	    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	    $content .= "<tr bgcolor=\"black\">\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Language Code</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>Language Name</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
	    foreach my $rec ( @rs ) {
	    	my $id      = $rec->{lng_id};
	    	my $code    = $rec->{lng_code};
	    	my $name    = $rec->{lng_desc};
	    	
	    	if ( $row == 0 ) {
	    		$row_color = $color_bg2;
	    		$row = 1;
	    	} else {
	    		$row_color = $color_bg1;
	    		$row = 0;
	    	}
	    	
	    	$content .= "<tr bgcolor=\"$row_color\">\n";
	    	$content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=edit_lng_form&lng_id=$id" . $sp_vars . "\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"View\" title=\"View $name\"></a></td>\n";
	    	$content .= "<td valign=top align=left class=form>$code</td>\n";
	    	$content .= "<td valign=top align=left class=form>$name</td>\n";
	    	$content .= "<td valign=top align=right class=form>";
    	    $content .= "<a href=\"$cgi_url?action=del_lng&lng_id=$id" . $sp_vars . "\" onClick=\"return confirm('Permanently remove $name from database?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete $name\"></a>";
	    	$content .= "</td></tr>\n";
	    	
	    	$fndCnt++;
	    }
	    $content .= "</table>\n";
	    		
	    if ( $fndCnt == 0 ) {
	    	$content  = "";
	    	$java_msg = "No Languages found meeting search criteria.";
	    } else {
	    	$conf->{lng_cnt} = "$fndCnt";
	    }
	
    	$conf->{search_results} = $content;
	
	
    } else {
	
	    $java_msg = "No Search Criteria Specified.";        
	
    }

	&print_main;
	
}



############################################################
# Form to add member to database
############################################################
sub add_lng_form {

  	my $today = $shared->getSQLToday;
    	
   	$conf->{form_head}       = "Add New Language";
   	$conf->{form_start}      = "<form method=post name=adm_lng_form action=\"$cgi_url\">\n"
    	                     . "<input type=hidden name=action value=\"add_lng\">\n"
    	                     . $inp_srch;
    $conf->{f_lng_code}      = "<input name=lng_code value=\"\" size=20 maxlength=15 class=form>";
   	$conf->{f_lng_desc}      = "<input name=lng_desc value=\"\" size=40 maxlength=60 class=form>";
    $conf->{f_lng_tm_access} = "<input type=checkbox name=lng_tm_access value=\"Y\" class=form>";
   	$conf->{form_submit}     = "<input type=submit value=\"Add Now\" class=form>&nbsp;\n"
                             . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}        = "</form>";
   	$conf->{form_name}       = "adm_lng_form";
   	$conf->{focus_field}     = "lng_code";  
    	
    my $pg_body = &disp_form( "lng_add_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

############################################################
# Add the record to the loan database
############################################################
sub add_lng {

    my $lng_code  = $shared->get_cgi( "lng_code" );
   	my $lng_desc  = $shared->get_cgi( "lng_desc" );
   	my $tmAccess  = $shared->get_cgi( "lng_tm_access" );
   	
   	if ( $tmAccess eq "" ) {
   	    $tmAccess = "N";
   	}
   	
   	my $codeCnt   = $shared->getSQLCount( "SELECT COUNT(*) FROM language WHERE lng_code = '$lng_code'" );
   	if ( $codeCnt == 0 ) {
    	
    	$lng_code    =~ s/'/\\'/g;
   	    $lng_desc    =~ s/'/\\'/g;
    	
        my $insert   = "INSERT INTO language ( lng_code, lng_desc, lng_tm_access )\n"
                     . "VALUES ( '$lng_code', '$lng_desc', '$tmAccess' )";
        $shared->doSQLUpdate( $insert );
        
    } else {
        $java_msg = "Code already exists - Must be unique.";
    }
        	
   	&search_any;

}

############################################################
# Form to edit member of database
############################################################
sub edit_lng_form {

    my ( $lng_id ) = @_;
    
    if ( $lng_id eq "" ) {
        $lng_id  = $shared->get_cgi( "lng_id" );
    }
    
    my $lng_rec = $shared->get_rec( "language", "lng_id", $lng_id );

    my $cCode    = $lng_rec->{lng_code};
    my $cName    = $lng_rec->{lng_desc};
    my $tmAccess = $lng_rec->{lng_tm_access};
    my $tList    = $lng_rec->{lng_quote_tgt_lng_list};
    my $tRate    = $lng_rec->{lng_quote_tgt_trans_rate};
    
    my $tmChk    = "";
    if ( $tmAccess eq "Y" ) {
        $tmChk = " checked";
    }
    
    $sp_vars =~ s/^\&//;
    
    $conf->{form_id_row}    = "<tr><td align=right valign=top class=label bgcolor=$color_bg2 width=150>ID</td>\n"
                            . "<td valign=top align=left class=form><b>$lng_id</b></td></tr>\n";

    
   	$conf->{form_head}       = "Update Language Details";
   	$conf->{form_start}      = "<form method=post name=lng_edit_form action=\"$cgi_url\">\n"
   	                         . "<input type=hidden name=action value=\"edit_lng\">\n"
   	                         . "<input type=hidden name=lng_id value=\"$lng_id\">\n"
   	                         . $inp_srch;
   	$conf->{f_lng_code}      = "<input name=lng_code value=\"" . $cCode . "\" size=20 maxlength=15 class=form>";
   	$conf->{f_lng_desc}      = "<input name=lng_desc value=\"" . $cName . "\" size=40 maxlength=60 class=form>";
   	$conf->{f_lng_tm_access} = "<input type=checkbox name=lng_tm_access value=\"Y\" class=form" . $tmChk . ">";
   	$conf->{f_lng_tlist}     = "<input name=lng_quote_tgt_lng_list value=\"$tList\" size=30 maxlength-30 class=form>";
   	$conf->{f_lng_trate}     = "<input name=lng_quote_tgt_trans_rate value=\"$tRate\" size=15 maxlength=16 class=form>";
    	
   	$conf->{form_submit}     = "<input type=submit value=\"Update Language\" class=form>&nbsp;\n"
                             . "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$cgi_url?$sp_vars'\" class=form>";
   	$conf->{form_end}        = "</form>";
    	
    my $pg_body = &disp_form( "lng_edit_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

############################################################
# Update the record to the members database
############################################################
sub edit_lng {

    my $lng_id    = $shared->get_cgi( "lng_id" );
    my $lng_code  = $shared->get_cgi( "lng_code" );
   	my $lng_desc  = $shared->get_cgi( "lng_desc" );
   	my $tmAccess  = $shared->get_cgi( "lng_tm_access" );
   	my $lng_tlist = $shared->get_cgi( "lng_quote_tgt_lng_list" );
   	my $lng_trate = $shared->validNumber( $shared->get_cgi( "lng_quote_tgt_trans_rate" ) );
    	
    if ( $tmAccess eq "" ) {
        $tmAccess = "N";
    }
    
    $lng_code    =~ s/'/\\'/g;
   	$lng_desc    =~ s/'/\\'/g;
   	$lng_tlist   =~ s/'/\\'/g;
   	
   	my $update   = "UPDATE language SET lng_code = '$lng_code', lng_desc = '$lng_desc', lng_quote_tgt_lng_list = '$lng_tlist',\n"
   	             . "lng_quote_tgt_trans_rate = $lng_trate, lng_tm_access = '$tmAccess'\n"
   	             . "WHERE lng_id = $lng_id";
   	$shared->doSQLUpdate( $update );
    	
    &search_any;

}

###########################################################                   
# Delete Member from Database
#----------------------------------------------------------
sub del_lng {

   	my $lng_id = $shared->get_cgi( "lng_id" );
   	my $ll_cnt = $shared->getSQLCount( "SELECT COUNT(*) FROM linguist_lang WHERE lil_src_lng_id = $lng_id OR lil_tgt_lng_id = $lng_id" );
   	
    if ( $ll_cnt == 0 ) {	
    	
   	    my $update = "DELETE FROM language WHERE lng_id = $lng_id";
   	    $shared->doSQLUpdate( $update );
    	
    } else {

        $java_msg = "Unable to Delete Language - Remove Associated Linguists First";

    }
    
    &search_any;
}

###########################################################                   
# Create language selection list
#----------------------------------------------------------
sub select_lang {

    my ( $dflt, $excl ) = @_;
    
    my $result = "";
    
    my $SQL = "SELECT lng_id, lng_desc FROM language WHERE lng_id <> $excl ORDER BY lng_desc";
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $id   = $rec->{lng_id};
        my $desc = $rec->{lng_desc};
        if ( $id == $dflt ) {
            $result .= "<option selected value=\"$id\">$desc</option>\n";
        } else {
            $result .= "<option value=\"$id\">$desc</option>\n";
        }
    }
    
    return $result;
}


