#!/usr/bin/perl
###########################################################################
# poadm.cgi                PO Administration
###########################################################################
# Copyright 2013, P Eric Huber (Moonlight Design, LLC)
###########################################################################
use strict;
use Excel::Writer::XLSX;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;
use polpdf;
use sdt;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );
my $polpdf = polpdf->new( $shared );
my $sdt    = sdt->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($log_path)    = $config->get_log_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/poadm.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($temp_path)   = $config->get_temp_path;
my ($forms_path)  = $config->get_forms_path;
my ($forms_url)   = $config->get_forms_url;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($css_path)    = $config->get_css_path;
my ($out_path)    = $config->get_out_path;

my (%colors)      = $config->get_colors;
my $color_head    = $colors{ "HEAD1" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $exceptList    = "";
my $dupList       = "";
my $debug         = $config->get_debug;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;
$env->{template}        = "admin.html";

my ($conf);
$conf->{cgi_url}     = $cgi_url;
$conf->{img_path}    = $img_path;
$conf->{color_head}  = $color_head;
$conf->{color_bg1}   = $color_bg1;
$conf->{color_bg2}   = $color_bg2;
$conf->{color_bg3}   = $color_bg3;


# Main program
my $usr_id = 0;
my $action = $shared->get_cgi( "action" );
my $hash_vars;
my $next_action = $shared->get_cgi( "next_act" );

$hash_vars->{srch_pono}    = $shared->get_cgi( "srch_pono" );
$hash_vars->{srch_ling}    = $shared->get_cgi( "srch_ling" );
$hash_vars->{srch_cln}     = $shared->get_cgi( "srch_cln" );
$hash_vars->{srch_proj}    = $shared->get_cgi( "srch_proj" );
$hash_vars->{srch_inv}     = $shared->get_cgi( "srch_inv" );
$hash_vars->{next_act}     = $next_action;
$hash_vars->{sort_fld}     = $shared->get_cgi( "sort_fld" );
$hash_vars->{sort_flw}     = $shared->get_cgi( "sort_flw" );




#
# Main Program
#
 
if ( $auth->validate_access( "poadm" ) ) {
	
	$usr_id    = $auth->getCurrentUser;

    if ( $action eq "search_any" ) {
        &search_any;
    } elsif ( $action eq "change_sort" ) {
    	&change_sort;
    } elsif ( $action eq "view_po" ) {
        my $pid = $shared->get_cgi( "pol_id" );
        &view_po( $pid );
    } elsif ( $action eq "view_invoice" ) {
        my $invID = $shared->get_cgi( "inv_id" );
        &view_invoice( $invID );
    } elsif ( $action eq "add_po_form" ) {
        &add_po_form( "" );
    } elsif ( $action eq "clone_po_form" ) {
        my $pid = $shared->get_cgi( "pol_id" );
        &add_po_form( $pid );
    } elsif ( $action eq "add_po_next" ) {
        &add_po_next;
    } elsif ( $action eq "add_po" ) {
        &add_po;
    } elsif ( $action eq "del_po" ) {
        &del_po;
    } elsif ( $action eq "update_po" ) {
        &update_po;
    } elsif ( $action eq "po_pdf" ) {
        my $pid = $shared->get_cgi( "pol_id" );
        my $pdfURL = $polpdf->genPO( $pid );
        $shared->disp_pdf( $pdfURL );
    } elsif ( $action eq "add_po_fee" ) {
        &add_po_fee;
    } elsif ( $action eq "del_po_fee" ) {
        &del_po_fee;
    } elsif ( $action eq "prj_rpt_form" ) {
        &prj_rpt_form;
    } elsif ( $action eq "prj_rpt" ) {
        my $excelURL = &genProjRpt;
        $env->{open_file} = $excelURL;
        &search_any( "Y" );
    } else {
        &search_any( "Y" );
    }
} else {
	$shared->do_err( "Access to this module is denied." );
}
 
exit;

###########################################################                   
# Display specified form.
#----------------------------------------------------------
sub disp_form { # Display specified form
    my $form_name = $_[0];
    my ($content);  
 
               
    open ( FILE, "<$src_path/$form_name" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


############################################################
# Print the main admin member maintenance screen
############################################################
sub print_main {

    my $cntSQL = "SELECT COUNT(*) FROM po_log\n"
    	       . "INNER JOIN linguist ON pol_ling_id = lig_id\n"
	           . "INNER JOIN client ON pol_cln_id = cln_id";

    my $po_cnt = $shared->getSQLCount( $cntSQL );

    if ( $conf->{po_cnt} eq "" ) {
        $conf->{po_cnt}     = $po_cnt;
    }
    
   	my $abContent = "<form method=post action=$cgi_url>\n"
                  . "<input type=hidden name=action value=add_po_form>\n"
                  . "<table width=100\% cellpadding=2 cellspacing=0 border=0>\n"
                  . "<tr bgcolor=\"$color_bg1\"><td valign=top align=center class=form>\n"
                  . "<br>\n"
                  . "<input type=submit value=\"Add New PO\" class=addbutton>\n"
                  . "<br><br>\n"
                  . "</td></tr>\n"
                  . "</table>\n"
                  . "</form>\n";

 	$conf->{add_button}  = $abContent;
 	$conf->{selLing}     = &getLing;
 	$conf->{selCln}      = &getClient;
    
    my $pg_body              = &disp_form( "po_main" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    
}

###########################################################                   
# Search for PO
#----------------------------------------------------------
sub search_any {
	
	my ( $init_srch ) = @_;
	
	my $content;
	
	my $srch_pono    = $hash_vars->{srch_pono};
	my $srch_ling    = $hash_vars->{srch_ling};
	my $srch_cln     = $hash_vars->{srch_cln};
	my $srch_proj    = $hash_vars->{srch_proj};
	my $srch_inv     = $hash_vars->{srch_inv};
	my $sort_fld     = $hash_vars->{sort_fld};
	my $sort_flw     = $hash_vars->{sort_flw};
	my $srch_head    = "<b>Searched:</b>&nbsp;";
	my $srch_par     = 0;
	my $sp_vars      = "";

	if ( $sort_fld eq "" ) {
		$sort_fld = "date";
	}
	if ( $sort_flw eq "" ) {
		$sort_flw = "D";
	}
	
	if ( ( $srch_pono gt "" ) || ( $srch_ling gt "" ) || ( $srch_cln gt "" ) || ( $srch_proj gt "" ) || ( $srch_inv gt "" ) || ( $init_srch eq "Y" ) ) {
	
	    my $SQL = "SELECT pol_id, pol_pono, lig_first_name, lig_last_name, cln_name, pol_do_trans, pol_do_edit, pol_do_proof, pol_do_mtpe, pol_add_date, pol_total_amt\n"
	            . "FROM po_log\n"
	            . "INNER JOIN linguist ON pol_ling_id = lig_id\n"
	            . "INNER JOIN client ON pol_cln_id = cln_id\n"
	            . "WHERE 1\n";

	    if ( $srch_pono gt "" ) {
		    $SQL .= "AND pol_pono LIKE '$srch_pono%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", PO Number contains <b>$srch_pono</b>";
		    } else {
		        $srch_head .= "PO Number contains <b>$srch_pono</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_pono=$srch_pono";
	    }
	    if ( $srch_ling gt "" ) {
	        my $lingName = &getLingName( $srch_ling );
		    $SQL .= "AND pol_ling_id = $srch_ling\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Linguist is <b>$lingName</b>";
		    } else {
		        $srch_head .= "Linguist is <b>$lingName</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_ling=$srch_ling";
	    }
	    if ( $srch_cln gt "" ) {
	        my $clnName = &getClnName( $srch_cln );
	    	$SQL .= "AND pol_cln_id = $srch_cln\n";
	    	if ( $srch_par > 0 ) {
	    		$srch_head .= ", Client is <b>$clnName</b>";
	    	} else {
	    		$srch_head .= "Client is <b>$clnName</b>";
	    	}
	    	$srch_par++;
	    	$sp_vars       .= "&srch_cln=$srch_cln";
	    }
	    if ( $srch_proj gt "" ) {
		    $SQL .= "AND pol_proj_no LIKE '$srch_proj%'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Project No starts with <b>$srch_proj</b>";
		    } else {
		        $srch_head .= "Project No starts with <b>$srch_proj</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_proj=$srch_proj";
	    }

	    if ( $srch_inv gt "" ) {
		    $SQL .= "AND pol_invoice_recd = '$srch_inv'\n";
		    my $srch_desc = "Invoice Received";
		    if ( $srch_inv eq "N" ) {
		        $srch_desc = "Invoice Pending";
		    }
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", <b>$srch_desc</b>";
		    } else {
		        $srch_head .= "<b>$srch_desc</b>";
		    }
		    $srch_par++;
		    $sp_vars       .= "&srch_inv=$srch_inv";
	    }
	    
	    if ( ( $init_srch eq "Y" ) && ( $srch_par == 0 ) ) {
	    	$SQL .= "AND pol_add_date > DATE_ADD( now(), INTERVAL -90 DAY )\n"
	    }

        my $doFlow = 1;
	    if ( $sort_fld eq "pono" ) {
	    	$SQL     .= "ORDER BY pol_pono";
	    	$sp_vars .= "&sort_fld=pono";
	    } elsif ( $sort_fld eq "lig" ) {
	        if ( $sort_flw eq "D" ) {
	    	    $SQL .= "ORDER BY lig_last_name DESC, lig_first_name DESC";
	    	    $sp_vars .= "&sort_fld=lig&sort_flw=D";
	    	} else {
	    	    $SQL .= "ORDER BY lig_last_name, lig_first_name";
	    	    $sp_vars .= "&sort_fld=lig&sort_flw=A";
	    	}
	    	$doFlow   = 0;
	    } elsif ( $sort_fld eq "cln" ) {
	        $SQL     .= "ORDER BY cln_name";
	        $sp_vars .= "&sort_fld=cln";
	    } elsif ( $sort_fld eq "amt" ) {
	        $SQL     .= "ORDER BY pol_total_amt";
	        $sp_vars .= "&sort_fld=amt";
	    } else {
	    	$SQL     .= "ORDER BY pol_add_date";
	    	$sp_vars .= "&sort_fld=date";
        }
	    if ( $doFlow ) {
	        if ( $sort_flw eq "D" ) {
	    	    $SQL     .= " DESC";
	    	    $sp_vars .= "&sort_flw=D";
	        } else {
	    	    $sp_vars .= "&sort_flw=A";
	        }
	    }
	    
	    my $doReset = 0;
	    if ( ( $init_srch eq "Y" ) && ( $srch_par == 0 ) ) {
	        
	    	$srch_head = "POs Created - Last 90 Days";
	    	$sp_vars  .= "&srch_init=Y";
	    } else {
	        $doReset = 1;
	    }
	
	    my @rs   = $shared->getResultSet( $SQL );
	    my $row  = 0;
	    my $row_color;
	    my $fndCnt = 0;
	    
	    $content .= "<span class=subhead><b>$srch_head</b></span><br><br>\n";
	    if ( $doReset ) {
	    	$content .= "<a href=\"$cgi_url\" class=nav><b>[ Clear Filter ]</b></a><br>\n";
	    }
	    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	    $content .= "<tr bgcolor=\"black\">\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td>\n";
	    $content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=pono" . $sp_vars . "\"><font color=\"white\"><b>PO Number</b></font></a></td>\n";
	    $content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=date" . $sp_vars . "\"><font color=\"white\"><b>Created</b></font></a></td>\n";
	    $content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=lig" . $sp_vars . "\"><font color=\"white\"><b>Linguist</b></font></a></td>\n";
	    $content .= "<td valign=top align=left class=form><font color=\"white\"><b>PO Type(s)</b></font></td>\n";
	    $content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=change_sort&new_sort=cln" . $sp_vars . "\"><font color=\"white\"><b>Client</b></font></a></td>\n";
	    $content .= "<td valign=top align=right class=form><a href=\"$cgi_url?action=change_sort&new_sort=amt" . $sp_vars . "\"><font color=\"white\"><b>Total Amt</b></font></a></td>\n";
	    $content .= "<td valign=top align=left class=form>&nbsp;</td></tr>\n";
	    foreach my $rec ( @rs ) {
	    	my $pid     = $rec->{pol_id};
	    	my $pono    = $rec->{pol_pono};
	    	my $addDt   = $shared->getDateFmt( $rec->{pol_add_date} );
	    	my $lName   = $shared->trim( $rec->{lig_last_name} ) . ", " . $shared->trim( $rec->{lig_first_name} );
	    	if ( $rec->{lig_last_name} eq "" ) {
	    	    $lName  = $rec->{lig_first_name};
	    	}
	    	if ( $rec->{lig_first_name} eq "" ) {
	    	    $lName  = $rec->{lig_last_name};
	    	}
	    	my $doTR    = $rec->{pol_do_trans};
	    	my $doED    = $rec->{pol_do_edit};
	    	my $doPF    = $rec->{pol_do_proof};
	    	my $doMT    = $rec->{pol_do_mtpe};
	    	my $cName   = $rec->{cln_name};
	    	my $totAmt  = $rec->{pol_total_amt};
	    	
	    	my $pType   = "";
	    	if ( $doTR eq "Y" ) {
	    	    $pType .= "TR ";
	    	}
	    	if ( $doED eq "Y" ) {
	    	    $pType .= "RE ";
	    	}
	    	if ( $doPF eq "Y" ) {
	    	    $pType .= "PF ";
	    	}
	    	if ( $doMT eq "Y" ) {
	    	    $pType .= "MTPE ";
	    	}
	    	$pType =~ s/[ \t]+$//;
	    	
	    	my $dspAmt = $shared->fmtNumber( $totAmt );
	    	
	    	if ( $row == 0 ) {
	    		$row_color = $color_bg2;
	    		$row = 1;
	    	} else {
	    		$row_color = $color_bg1;
	    		$row = 0;
	    	}
	    	
	    	$content .= "<tr bgcolor=\"$row_color\">\n";
	    	$content .= "<td valign=top align=left class=form><a href=\"$cgi_url?action=view_po&pol_id=$pid" . $sp_vars . "\"><img src=\"$img_path/icon_edit.gif\" border=0 alt=\"View\" title=\"View $pono\"></a></td>\n";
	    	$content .= "<td valign=top align=left class=form>$pono</td>\n";
	    	$content .= "<td valign=top align=left class=form>$addDt</td>\n";
	    	$content .= "<td valign=top align=left class=form>$lName</td>\n";
	    	$content .= "<td valign=top align=left class=form>$pType</td>\n";
	    	$content .= "<td valign=top align=left class=form>$cName</td>\n";
	    	$content .= "<td valign=top align=right class=form>$dspAmt</td>\n";
	    	$content .= "<td valign=top align=right class=form>";
    	    $content .= "<a href=\"$cgi_url?action=del_po&pol_id=$pid" . $sp_vars . "\" onClick=\"return confirm('Permanently remove $pono from database?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete $pono\"></a>";
	    	$content .= "</td></tr>\n";
	    	
	    	$fndCnt++;
	    }
	    $content .= "</table>\n";
	    		
	    if ( $fndCnt == 0 ) {
	    	$content  = "";
	    	if ( $init_srch eq "Y" ) {
	    	    $java_msg = "No POs Found.";
	    	} else {
	    	    $java_msg = "No POs found meeting search criteria.";
	    	}
	    } else {
	    	$conf->{po_cnt} = "$fndCnt";
	    }
	
    	$conf->{search_results} = $content;
	
	
    } else {
	
	    $java_msg = "No Search Criteria Specified.";        
	
    }

	&print_main;
	
}

###########################################################                   
# Change sort of result set
#----------------------------------------------------------
sub change_sort {

	my $new_sort = $shared->get_cgi( "new_sort" );
	my $new_flow = "A";
	if ( $new_sort eq $hash_vars->{sort_fld} ) {
		my $cur_flow = $hash_vars->{sort_flw};
		if ( $cur_flow eq "A" ) {
			$new_flow = "D";
		}
	}
	
	$hash_vars->{sort_fld} = $new_sort;
	$hash_vars->{sort_flw} = $new_flow;
	
    if ( ( $hash_vars->{srch_pono} eq "" ) && ( $hash_vars->{srch_ling} eq "" ) && ( $hash_vars->{srch_cln} eq "" ) &&
         ( $hash_vars->{srch_proj} eq "" ) && ( $hash_vars->{srch_inv} eq "" ) ) {
	    &search_any( "Y" );
	} else {
		&search_any;
	}
	
}



############################################################
# Form to add member to database
############################################################
sub add_po_form {
 
    my ( $pid ) = @_;
   
    $conf->{form_head}    = "New Purchase Order";
    $conf->{form_start}   = "<form method=post name=adm_cln_form action=\"$cgi_url\">\n"
                                 . "<input type=hidden name=action value=\"add_po_next\">\n";

    my $prj_cln_id        = $shared->get_cgi( "cln_id" );
    my $prj_no            = $shared->get_cgi( "prj_no" );

    if ( $pid gt "" ) {
        $conf->{form_start} .= "<input type=hidden name=clone_pid value=\"$pid\">\n";
        $prj_cln_id          = $shared->getFieldVal( "po_log", "pol_cln_id", "pol_id", $pid );
    }
   
    if ( $prj_no gt "" ) {
        $conf->{form_start} .= "<input type=hidden name=prj_no value=\"$prj_no\">\n";
    }
   
    $conf->{f_src}        = "<select name=src_lng_id class=form>" . &getLang . "</select>";
    $conf->{f_tgt}        = "<select name=tgt_lng_id class=form>" . &getLang . "</select>";
    $conf->{f_client}     = "<select name=pol_cln_id class=form>" . &getClientSelect( $prj_cln_id ) . "</seleect>";
              
    $conf->{form_submit}  = "<input type=submit value=\"Next\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
    $conf->{form_end}     = "</form>";

    my $pg_body = &disp_form( "po_add_form" );
 
    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
              
}

############################################################
# Form to add member to database
############################################################
sub add_po_next {

    my $src               = $shared->get_cgi( "src_lng_id" );
    my $tgt               = $shared->get_cgi( "tgt_lng_id" );
    my $clnID             = $shared->get_cgi( "pol_cln_id" );
    my $prj_no            = $shared->get_cgi( "prj_no" );
    
    my $selLing           = &getLangLing( $src, $tgt );
    my $mdRate            = $shared->getConfigInt( "ADM_POMDRATE" );
    my $clonePID          = $shared->get_cgi( "clone_pid" );
    
    if ( $selLing gt "" ) {
    
        my $pono          = &genNewPONo;
        my $pg_body       = "";
        
        if ( $clonePID gt "" ) {

            my $poRec   = $shared->get_rec( "po_log", "pol_id", $clonePID );
            my $oPrjNo  = $poRec->{pol_proj_no};
            my $oClnID  = $poRec->{pol_cln_id};
            my $oDueDt  = $poRec->{pol_due_time};
            my $oNotes  = $poRec->{pol_notes};
            my $oNotEd  = $poRec->{pol_not_edit_text};
            my $oClnNm  = $shared->getFieldVal( "client", "cln_name", "cln_id", $oClnID );
            my $oCType  = $poRec->{pol_content_type};

            $oNotes    =~ s/\"/&quot;/g;
            my $neChk   = "";
            if ( $oNotEd eq "Y" ) {
                $neChk  = " checked";
            }
            
   	        $conf->{form_head}    = "Duplicate Purchase Order (continued)";
   	        $conf->{form_start}   = "<form method=post name=adm_cln_form action=\"$cgi_url\">\n"
    	                         . "<input type=hidden name=action value=\"add_po\">\n"
    	                         . "<input type=hidden name=pol_pono value=\"$pono\">\n"
    	                         . "<input type=hidden name=pol_src_lng_id value=\"$src\">\n"
    	                         . "<input type=hidden name=pol_tgt_lng_id value=\"$tgt\">\n"
    	                         . "<input type=hidden name=clone_pid value=\"$clonePID\">\n"
    	                         . "<input type=hidden name=pol_cln_id value=\"$oClnID\">\n"
    	                         . "<input type=hidden name=pol_proj_no value=\"$oPrjNo\">\n";
    	    
   	        $conf->{f_pono}       = $pono;
   	        $conf->{f_ling}       = "<select id=pol_ling_id name=pol_ling_id class=form onchange=JavaScript:SetTypes()>$selLing</select>";
      	    $conf->{f_client}     = $oClnNm;
      	    $conf->{f_proj_no}    = $oPrjNo;
   	        $conf->{f_type}       = "<input id=pol_do_trans type=checkbox name=pol_do_trans value=\"Y\" class=form> Translate&nbsp;&nbsp;"
   	                              . "<input id=pol_do_edit type=checkbox name=pol_do_edit value=\"Y\" class=form> Review&nbsp;&nbsp;"
   	                              . "<input id=pol_do_proof type=checkbox name=pol_do_proof value=\"Y\" class=form> Proof-read&nbsp;&nbsp;"
   	                              . "<input id=pol_do_mtpe type=checkbox name=pol_do_mtpe value=\"Y\" class=form> MTPE";
   	        $conf->{f_due_date}   = "<input name=pol_due_time value=\"" . $oDueDt . "\" size=20 maxlength=30 class=form>";
   	        $conf->{f_notes}      = "<textarea name=pol_notes cols=50 rows=5 wrap class=form>" . $oNotes . "</textarea><br>\n"
   	                              . "<input type=checkbox name=pol_noedit value=\"Y\" class=form" . $neChk . "> Add \"Not Reviewed\" text to Instructions";
   	        $conf->{f_cont_type}  = "<select name=pol_content_type class=form>" . $shared->pop_lookup_select( "PO_CONTENT_TYPE", $oCType, "" ) . "</select>";
   	
   	        $conf->{form_submit}  = "<input type=submit value=\"Generate Now\" class=form>&nbsp;\n"
                                  . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	        $conf->{form_end}     = "</form>";
    	
            $pg_body = &disp_form( "po_clone_form" );
            
        } else {

    	    my $cln_rec           = $shared->get_rec( "client", "cln_id", $clnID );
    	    my $cln_dsc_rt1       = $cln_rec->{cln_dsc_rate};
    	    my $cln_dsc_rt2       = $cln_rec->{cln_dsc_rate_2};
    	    my $cln_name          = $cln_rec->{cln_name};
    	    my $cln_disp          = "<b>$cln_name</b><p id=\"dscInput\" style=\"display:inline\">";
      	    if ( ( $cln_dsc_rt1 > 0 ) || ( $cln_dsc_rt2 > 0 ) ) {
      	        $cln_disp        .= "<br>";
      	        if ( $cln_dsc_rt1 > 0 ) {
      	            $cln_disp    .= "<input type=radio name=pol_dsc value=\"$cln_dsc_rt1\" checked> Apply " . $cln_dsc_rt1 . "\% Discount&nbsp;&nbsp;&nbsp;";
      	        }
      	        if ( $cln_dsc_rt2 > 0 ) {
      	            $cln_disp    .= "<input type=radio name=pol_dsc value=\"$cln_dsc_rt2\"> Apply " . $cln_dsc_rt2 . "\% Discount&nbsp;&nbsp;&nbsp;";
      	        }
      	        $cln_disp        .= "<input type=radio name=pol_dsc value=\"0\"> No Discount\n";
      	    }
      	    $cln_disp .= "</p>";
      	    my $selProj           = &getProjects( $clnID, $prj_no );
            
   	        $conf->{form_head}    = "New Purchase Order (continued)";
   	        $conf->{form_start}   = "<form method=post name=adm_cln_form action=\"$cgi_url\" onsubmit=\"chkMode(event)\" enctype=\"multipart/form-data\">\n"
    	                         . "<input type=hidden name=action value=\"add_po\">\n"
    	                         . "<input type=hidden name=pol_pono value=\"$pono\">\n"
    	                         . "<input type=hidden name=pol_src_lng_id value=\"$src\">\n"
    	                         . "<input type=hidden name=pol_tgt_lng_id value=\"$tgt\">\n"
    	                         . "<input type=hidden name=pol_cln_id value=\"$clnID\">\n";
    	                         
    	                         
   	        $conf->{f_pono}       = $pono;
   	        $conf->{f_ling}       = "<select id=pol_ling_id name=pol_ling_id class=form onchange=JavaScript:SetTypes()>$selLing</select>";
      	    $conf->{f_client}     = $cln_disp;
      	    $conf->{f_proj_no}    = "<select name=pol_proj_no class=form><option value=\"\"></option>" . $selProj . "</select>";
   	        $conf->{f_type}       = "<input id=pol_do_trans type=checkbox name=pol_do_trans value=\"Y\" class=form> Translate&nbsp;&nbsp;"
   	                              . "<input id=pol_do_edit type=checkbox name=pol_do_edit value=\"Y\" class=form> Review&nbsp;&nbsp;"
   	                              . "<input id=pol_do_proof type=checkbox name=pol_do_proof value=\"Y\" class=form> Proof-read&nbsp;&nbsp;"
   	                              . "<input id=pol_do_mtpe type=checkbox name=pol_do_mtpe value=\"Y\" class=form> MTPE";
   	        $conf->{f_due_date}   = "<input name=pol_due_time value=\"\" size=20 maxlength=30 class=form>";
   	        $conf->{f_mode}       = "<table border=0 cellpadding=2 cellspacing=0 width=600>\n"
   	                              . "<tr><td valign=top align=left class=form width=200><input type=radio id=pol_mode_f name=pol_mode class=form value=\"F\" checked> <b>Analysis File</b><br>\n"
   	                              . "<input type=file id_pol_imp_file name=imp_file value=\"\" class=form></td>"
   	                              . "<td valign=top align=left class=form width=200><input type=radio id=pol_mode_m name=pol_mode value=\"M\" class=form> <b>Minimum Rate</b><br>\n"
   	                              . "(no analysis file)</td>"
   	                              . "<td valign=top align=left class=form width=200><input type=radio id=pol_mode_d name=pol_mode value=\"D\" class=form> <b>Discounted Minimum Rate</b><br>\n"
   	                              . "(no analysis file - calculated at $mdRate \% minimum rate)</td></tr>\n"
   	                              . "</table>\n";
   	        $conf->{f_notes}      = "<textarea name=pol_notes cols=50 rows=5 wrap class=form></textarea><br>\n"
   	                              . "<input type=checkbox name=pol_noedit value=\"Y\" class=form> Add \"Not Reviewed\" text to Instructions";
   	        $conf->{f_cont_type}  = "<select name=pol_content_type class=form>" . $shared->pop_lookup_select( "PO_CONTENT_TYPE", "", "" ) . "</select>";
   	
   	        $conf->{form_submit}  = "<input type=submit value=\"Generate Now\" class=form>&nbsp;\n"
                                  . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	        $conf->{form_end}     = "</form>";
    	
            $pg_body = &disp_form( "po_next_form" );
        }
        
        my $js = "<script type=\"text/javascript\">\n";
        $js   .= "function doInit(){\n";
        $js   .= "  SetTypes();\n";
        if ( $clonePID eq "" ) {
            $js   .= "  clientAction();\n";
        }
        $js   .= "}\n";
        $js   .= "function SetTypes(){\n";
        $js   .= "  var lID   = document.getElementById('pol_ling_id').value;\n";
        $js   .= "  var tfnd  = lID.match(/t/);\n";
        $js   .= "  var efnd  = lID.match(/e/);\n";
        $js   .= "  var hfnd  = lID.match(/h/);\n";
        $js   .= "  var mfnd  = lID.match(/m/);\n";
        $js   .= "  if (tfnd) {\n";
        $js   .= "     document.getElementById('pol_do_trans').disabled=false;\n";
        $js   .= "  } else {\n";
        $js   .= "     document.getElementById('pol_do_trans').checked=false;\n";
        $js   .= "     document.getElementById('pol_do_trans').disabled=true;\n";
        $js   .= "  }\n";
        $js   .= "  if (efnd) {\n";
        $js   .= "     document.getElementById('pol_do_edit').disabled=false;\n";
        $js   .= "  } else {\n";
        $js   .= "     document.getElementById('pol_do_edit').checked=false;\n";
        $js   .= "     document.getElementById('pol_do_edit').disabled=true;\n";
        $js   .= "  }\n";
        $js   .= "  if (hfnd) {\n";
        $js   .= "     document.getElementById('pol_do_proof').disabled=false;\n";
        $js   .= "     document.getElementById('pol_mode_m').disabled=false;\n";
        $js   .= "     document.getElementById('pol_mode_d').disabled=false;\n";
        $js   .= "  } else {\n";
        $js   .= "     document.getElementById('pol_do_proof').checked=false;\n";
        $js   .= "     document.getElementById('pol_do_proof').disabled=true;\n";
        $js   .= "     document.getElementById('pol_mode_f').checked=true;\n";
        $js   .= "     document.getElementById('pol_mode_m').disabled=true;\n";
        $js   .= "     document.getElementById('pol_mode_d').disabled=true;\n";
        $js   .= "  }\n";
        $js   .= "  if (mfnd) {\n";
        $js   .= "     document.getElementById('pol_do_mtpe').disabled=false;\n";
        $js   .= "  } else {\n";
        $js   .= "     document.getElementById('pol_do_mtpe').checked=false;\n";
        $js   .= "     document.getElementById('pol_do_mtpe').disabled=true;\n";
        $js   .= "  }\n";
        $js   .= "}\n";
        $js   .= "function chkMode(evt){\n";
        $js   .= "  var myForm    = document.adm_cln_form;\n";
        $js   .= "  var condition = true;\n";
        $js   .= "  if ( myForm.pol_mode.value == \"F\" ) {\n";
        $js   .= "    if ( myForm.imp_file.value == \"\" ) {\n";
        $js   .= "      alert( \"Analysis File is missing\" );\n";
        $js   .= "      condition = false;\n";
        $js   .= "    }\n";
        $js   .= "  }\n";
        $js   .= "  if( !condition ) {\n";
        $js   .= "    if ( evt.preventDefault ) { event.preventDefault(); }\n";    
        $js   .= "    else if ( evt.returnValue ) { evt.returnValue = false; }\n";    
        $js   .= "    else { return false; }\n";
        $js   .= "  }\n";        
        $js   .= "}\n";
        $js   .= "</script>\n";

        $env->{onload}           = "JavaScript:doInit()";
        $env->{javascript}       = $js;
        $env->{page_body}        = $pg_body;
        $shared->page_gen( $env, $java_msg );
    
    } else {
    
        $java_msg = "No Linguist Found for Source/Target Language.";
        &search_any( "Y" );
    }
}

############################################################
# Add the record to the PO database
############################################################
sub add_po {

    my $clonePID = $shared->get_cgi( "clone_pid" );
    
    my $iFile  = lc( $shared->get_cgi( "imp_file" ) );
    my $poMode = $shared->get_cgi( "pol_mode" );

    my $po_no = $shared->get_cgi( "pol_pono" );
    my $src   = $shared->get_cgi( "pol_src_lng_id" );
    my $tgt   = $shared->get_cgi( "pol_tgt_lng_id" );
    my $ltmp  = $shared->get_cgi( "pol_ling_id" );
    my $cID   = $shared->get_cgi( "pol_cln_id" );
    my $doTR  = $shared->get_cgi( "pol_do_trans" );
    my $doED  = $shared->get_cgi( "pol_do_edit" );
    my $doPF  = $shared->get_cgi( "pol_do_proof" );
    my $doMT  = $shared->get_cgi( "pol_do_mtpe" );
    my $dRt   = ( $shared->get_cgi( "pol_dsc" ) ) + 0;
    my $notes = $shared->get_cgi( "pol_notes" );
    my $cType = $shared->get_cgi( "pol_content_type" );
    my $notEd = $shared->get_cgi( "pol_noedit" );
    my $pDue  = $shared->get_cgi( "pol_due_time" );
    my $pCode = $shared->get_cgi( "pol_proj_no" );
    my $doDsc = "N";
    
    if ( $dRt > 0 ) {
        $doDsc = "Y";
    }
    
    if ( $notEd eq "" ) {
        $notEd = "N";
    }
            
    my $lID   = "0";
    my $lpID  = "0";
    if ( $ltmp =~ /([^\-]+)\-([^tehm]+).*/ ) {
        $lID  = $1;
        $lpID = $2;
    }
            
    $notes =~ s/'/\\'/g;
            
    if ( $doTR eq "" ) {
        $doTR = "N";
    }
    if ( $doED eq "" ) {
        $doED = "N";
    }
    if ( $doPF eq "" ) {
        $doPF = "N";
    }
    if ( $doMT eq "" ) {
        $doMT = "N";
    }

    my $pid = "";
    
    if ( $clonePID gt "" ) {
        
        $pid = $shared->clone_rec( "po_log", "pol_id", $clonePID );
        $shared->doSQLUpdate( "UPDATE po_log SET pol_add_date = now(), pol_invoice_recd = 'N', pol_inv_id = 0 WHERE pol_id = $pid" );
        
    } else {
        
        my $fExt   = "";
        if ( $iFile =~ /.*[\.](.*)$/ ) {
            $fExt = $1;
        }
        my $newFl  = "$imp_path/last_analysis.txt";
        my $status = $shared->uploadFile( "imp_file", $newFl, "" );
        if ( ( $status == 0 ) || ( $poMode ne "F" ) ) {
        
            if ( $fExt eq "txt" ) {
                $pid = &parseTXTFile( $newFl );
            } elsif ( $poMode ne "F" ) {
                $pid = &addMinPO( $poMode );
            } else {
                $java_msg = "Invalid file extension (Analysis file must be tab-delimited text - .txt)";
                &search_any( "Y" );
            } 
            
        } else {

            $java_msg = "Problem importing file [Status: $status].  Contact Administrator.";
            &search_any( "Y" );
        }

    }
        
    if ( $pid gt "" ) {
            
            
        my $lSQL  = "SELECT * FROM linguist_lang\n"
                  . "INNER JOIN linguist ON lig_id = lil_lig_id\n"
                  . "WHERE lil_lig_id = $lID\n"
                  . "AND lil_id = $lpID";
                      
        my $rec   = $shared->getResultRec( $lSQL );
        my $tRate = $rec->{lil_trans_rate};
        my $eRate = $rec->{lil_edit_rate};
        my $pRate = $rec->{lil_hourly_rate};
        my $mRate = $rec->{lil_mtpe_rate};
        my $cur   = $rec->{lig_currency};
            
        my $update = "UPDATE po_log SET pol_pono = '$po_no', pol_ling_id = $lID, pol_lil_id = $lpID, pol_cln_id = $cID,\n"
                   . "pol_do_trans = '$doTR', pol_do_edit = '$doED', pol_do_proof = '$doPF', pol_do_mtpe = '$doMT', pol_do_disc = '$doDsc',\n"
                   . "pol_trans_rate = $tRate, pol_edit_rate = $eRate, pol_hourly_rate = $pRate, pol_mtpe_rate = $mRate, pol_disc_rate = $dRt,\n"
                   . "pol_currency = '$cur', pol_src_lng_id = $src, pol_tgt_lng_id = $tgt, pol_proj_no = '$pCode', pol_due_time = '$pDue',\n"
                   . "pol_notes = '$notes', pol_not_edit_text = '$notEd', pol_content_type = '$cType' WHERE pol_id = $pid";
        $shared->doSQLUpdate( $update );
          
        &calcPO( $pid );
            
        &view_po( $pid );
        
    } else {            
            
        $java_msg = "Problem adding PO - empty pol_id. Contact Administrator.";
        &search_any( "Y" );
	
	}
}


###########################################################                   
# Parse TXT File (Trados v2014).
#----------------------------------------------------------
sub parseTXTFile {

    my ( $fileIn ) = @_;

    my $retval   = "";
    
    my $hashDtls = $sdt->parseTXTFile( $fileIn );
    
    if ( $hashDtls->{return_code} eq "SUCCESS" ) {
   	    my $p_ctxcnt = $hashDtls->{cnt_ctx};
 	    my $p_repcnt = $hashDtls->{cnt_rep};
        my $p_xfrcnt = $hashDtls->{cnt_xfr};
        my $p_100cnt = $hashDtls->{cnt_100};
   	    my $p_95cnt  = $hashDtls->{cnt_95};
   	    my $p_85cnt  = $hashDtls->{cnt_85};
   	    my $p_75cnt  = $hashDtls->{cnt_75};
        my $p_50cnt  = $hashDtls->{cnt_50};
   	    my $p_newcnt = $hashDtls->{cnt_none};
   	    my $p_abase  = $hashDtls->{cnt_abas};
   	    my $p_alrn   = $hashDtls->{cnt_alrn};
   	    my $totCnt   = $hashDtls->{cnt_wtot};

        my $wt_ctx   = $shared->getConfigInt( "ADM_ANALCTXMATCHPCT" );
        my $wt_rep   = $shared->getConfigInt( "ADM_ANALREPPCT" );
        my $wt_xfr   = $shared->getConfigInt( "ADM_ANALXFILREPPCT" );
        my $wt_100   = $shared->getConfigInt( "ADM_ANAL100PCT" );
        my $wt_95    = $shared->getConfigInt( "ADM_ANAL95PCT" );
        my $wt_85    = $shared->getConfigInt( "ADM_ANAL85PCT" );
        my $wt_75    = $shared->getConfigInt( "ADM_ANAL75PCT" );
        my $wt_50    = $shared->getConfigInt( "ADM_ANAL50PCT" );
        my $wt_new   = $shared->getConfigInt( "ADM_ANALNEWPCT" );
        my $wt_abs   = $shared->getConfigInt( "ADM_ANALAMTBASEPCT" );
        my $wt_aln   = $shared->getConfigInt( "ADM_ANALAMTLEARNPCT" );
            	
    	if ( $totCnt > 0 ) {
    	    my $insert = "INSERT INTO po_log ( pol_add_date, pol_add_usr_id, pol_ctx_wcnt, pol_rep_wcnt, pol_xfr_wcnt,\n"
    	               . "pol_100_wcnt, pol_95_wcnt, pol_85_wcnt, pol_75_wcnt, pol_50_wcnt, pol_no_wcnt, pol_abase_wcnt,\n"
    	               . "pol_alrn_wcnt, pol_total_wcnt, pol_ctx_weight, pol_rep_weight, pol_xfr_weight, pol_100_weight,\n"
    	               . "pol_95_weight, pol_85_weight, pol_75_weight, pol_50_weight, pol_none_weight, pol_abase_weight,\n"
    	               . "pol_alrn_weight )\n"
    	               . "VALUES ( now(), $usr_id, $p_ctxcnt, $p_repcnt, $p_xfrcnt, $p_100cnt, $p_95cnt, $p_85cnt,\n"
    	               . "$p_75cnt, $p_50cnt, $p_newcnt, $p_abase, $p_alrn, $totCnt, '$wt_ctx', '$wt_rep', '$wt_xfr',\n"
    	               . "'$wt_100', '$wt_95', '$wt_85', '$wt_75', '$wt_50', '$wt_new', '$wt_abs', '$wt_aln' )";
            $shared->doSQLUpdate( $insert );
            $retval    = "" . ( $shared->getSQLLastInsertID ) . "";
    	}
    	
    }
        
    if ( $debug ) {
        print DEBUG "retval: [$retval]\n";
        close ( DEBUG );
    }
    
    return $retval;
}	    



###########################################################                   
# Add Minimum Rate PO
#----------------------------------------------------------
sub addMinPO {

    my ( $poMode ) = @_;
    
    my $mdRate = $shared->getConfigInt( "ADM_POMDRATE" );
    
    my $insert;
    if ( $poMode eq "M" ) {
        $insert = "INSERT INTO po_log ( pol_add_date, pol_add_usr_id, pol_min_rate )\n"
  	            . "VALUES ( now(), $usr_id, 'Y' )";
  	} else {
        $insert = "INSERT INTO po_log ( pol_add_date, pol_add_usr_id, pol_dscmin_rate, pol_dscmin_rate_value )\n"
   	            . "VALUES ( now(), $usr_id, 'Y', $mdRate )";
   	}

    $shared->doSQLUpdate( $insert );
    my $retval = "" . ( $shared->getSQLLastInsertID ) . "";
    
    return $retval;
}	    

###########################################################                   
# Calculate PO
#----------------------------------------------------------
sub calcPO {
    
    my ( $pid )  = @_;
    
    my $wph         = $shared->getConfigInt( "ADM_WPH" );
    
    my $SQL         = "SELECT * FROM po_log WHERE pol_id = $pid";
    my $rec         = $shared->getResultRec( $SQL );
    my $doTR        = $rec->{pol_do_trans};
    my $doED        = $rec->{pol_do_edit};
    my $doPF        = $rec->{pol_do_proof};
    my $doMT        = $rec->{pol_do_mtpe};
    my $doDsc       = $rec->{pol_do_disc};
    my $tRate       = $rec->{pol_trans_rate};
    my $eRate       = $rec->{pol_edit_rate};
    my $hRate       = $rec->{pol_hourly_rate};
    my $mRate       = $rec->{pol_mtpe_rate};
    my $dRate       = $rec->{pol_disc_rate};
    my $cur         = $rec->{pol_currency};
    
    my $aHash;
    $aHash->{cnt_ctx}  = $rec->{pol_ctx_wcnt};
 	$aHash->{cnt_rep}  = $rec->{pol_rep_wcnt};
 	$aHash->{cnt_xfr}  = $rec->{pol_xfr_wcnt};
   	$aHash->{cnt_100}  = $rec->{pol_100_wcnt};
   	$aHash->{cnt_95}   = $rec->{pol_95_wcnt};
   	$aHash->{cnt_85}   = $rec->{pol_85_wcnt};
   	$aHash->{cnt_75}   = $rec->{pol_75_wcnt};
   	$aHash->{cnt_50}   = $rec->{pol_50_wcnt};
   	$aHash->{cnt_none} = $rec->{pol_no_wcnt};
   	$aHash->{cnt_abas} = $rec->{pol_abase_wcnt};
   	$aHash->{cnt_alrn} = $rec->{pol_alrn_wcnt};
   	$aHash->{cnt_wtot} = $rec->{pol_total_wcnt};
    $aHash->{wht_ctx}  = $rec->{pol_ctx_weight};
 	$aHash->{wht_rep}  = $rec->{pol_rep_weight};
 	$aHash->{wht_xfr}  = $rec->{pol_xfr_weight};
   	$aHash->{wht_100}  = $rec->{pol_100_weight};
   	$aHash->{wht_95}   = $rec->{pol_95_weight};
   	$aHash->{wht_85}   = $rec->{pol_85_weight};
   	$aHash->{wht_75}   = $rec->{pol_75_weight};
   	$aHash->{wht_50}   = $rec->{pol_50_weight};
   	$aHash->{wht_none} = $rec->{pol_none_weight};
   	$aHash->{wht_abas} = $rec->{pol_abase_weight};
   	$aHash->{wht_alrn} = $rec->{pol_alrn_weight};
   	

    my $poTOT       = 0;
    my $trAmt       = 0;
    my $edAmt       = 0;
    my $pfAmt       = 0;
    my $mtAmt       = 0;
    my $dscAmt      = 0;
    my $doMinDsc    = $rec->{pol_dscmin_rate};
    my $mdRate      = $rec->{pol_dscmin_rate_value};
    
    if ( $doMinDsc eq "Y" ) {
        $hRate      = ( $hRate * ( $mdRate / 100 ) );
    }
    
    if ( $doTR eq "Y" ) {
        $trAmt      = $sdt->calcAnalysis( $aHash, "T", $tRate, $hRate );        
        $poTOT += $trAmt;
    }
    
    if ( $doED eq "Y" ) {
        $edAmt      = $sdt->calcAnalysis( $aHash, "E", $eRate, $hRate );
        $poTOT += $edAmt;
    }
    
    if ( $doPF eq "Y" ) {
        $pfAmt      = $sdt->calcAnalysis( $aHash, "P", $hRate, $hRate );
        $poTOT += $pfAmt;
    }
    
    if ( $doMT eq "Y" ) {
        $mtAmt      = $sdt->calcAnalysis( $aHash, "M", $mRate, $hRate );
        $poTOT += $mtAmt;
    }
    
    my $feeAmt = $shared->getSQLCount( "SELECT SUM( pof_amt ) FROM po_fee WHERE pof_pol_id = $pid" );
    $poTOT    += $feeAmt;
    
    if ( $doDsc eq "Y" ) {
        $dscAmt    = $shared->round( $poTOT * ( $dRate / 100 ), 2 );
        $poTOT    -= $dscAmt;
    }
    
    my $update = "UPDATE po_log SET pol_trans_amt = $trAmt, pol_edit_amt = $edAmt,\n"
               . "pol_proof_amt = $pfAmt, pol_mtpe_amt = $mtAmt, pol_disc_amt = $dscAmt, pol_total_amt = $poTOT WHERE pol_id = $pid";
    $shared->doSQLUpdate( $update );
}    


###########################################################                   
# Display PO
#----------------------------------------------------------
sub view_po {
    
    my ( $pid )  = @_;

    my $wph         = $shared->getConfigInt( "ADM_WPH" );

	my $srch_pono = $hash_vars->{srch_pono};
	my $srch_ling = $hash_vars->{srch_ling};
	my $srch_cln  = $hash_vars->{srch_cln};
	my $srch_proj = $hash_vars->{srch_proj};
	my $srch_inv  = $hash_vars->{srch_inv};
	my $sort_fld  = $hash_vars->{sort_fld};
	my $sort_flw  = $hash_vars->{sort_flw};
	
    my $inp_srch     = "";
    my $sp_vars      = "";
    if ( $srch_pono gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_pono value=\"$srch_pono\">\n";
    	$sp_vars  .= "&srch_pono=$srch_pono";
    }
    if ( $srch_ling gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_ling value=\"$srch_ling\">\n";
    	$sp_vars  .= "&srch_ling=$srch_ling";
    }
    if ( $srch_cln gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_cln value=\"$srch_cln\">\n";
    	$sp_vars  .= "&srch_cln=$srch_cln";
    }
    if ( $srch_proj gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_proj value=\"$srch_proj\">\n";
    	$sp_vars  .= "&srch_proj=$srch_proj";
    }
    if ( $srch_inv gt "" ) {
    	$inp_srch .= "<input type=hidden name=srch_inv value=\"$srch_inv\">\n";
    	$sp_vars  .= "&srch_inv=$srch_inv";
    }
    if ( $inp_srch eq "" ) {
    	$inp_srch .= "<input type=hidden name=srch_init value=\"Y\">\n";
    	$sp_vars  .= "&srch_init=Y";
    }
    if ( $sort_fld gt "" ) {
        $inp_srch .= "<input type=hidden name=sort_fld value=\"$sort_fld\">\n";
        $sp_vars  .= "&sort_fld=$sort_fld";
    }
    if ( $sort_flw gt "" ) {
        $inp_srch .= "<input type=hidden name=sort_flw value=\"$sort_flw\">\n";
        $sp_vars  .= "&sort_flw=$sort_flw";
    }
    


    $sp_vars =~ s/^\&//;
    
    my $SQL         = "SELECT * FROM po_log WHERE pol_id = $pid";
    my $rec         = $shared->getResultRec( $SQL );
    my $poDt        = $rec->{pol_add_date};
    my $dspPODt     = $shared->getDateFmt( $poDt );
    my $poNo        = $rec->{pol_pono};
    my $lName       = &getLingName( $rec->{pol_ling_id} );
    my $lEml        = &getLingEml( $rec->{pol_ling_id} );
    my $cName       = &getClnName( $rec->{pol_cln_id} );
    my $notes       = $rec->{pol_notes};
    my $notEd       = $rec->{pol_not_edit_text};
    my $cType       = $rec->{pol_content_type};
    my $doTR        = $rec->{pol_do_trans};
    my $doED        = $rec->{pol_do_edit};
    my $doPF        = $rec->{pol_do_proof};
    my $doMT        = $rec->{pol_do_mtpe};
    my $doDsc       = $rec->{pol_do_disc};
    my $tRate       = $rec->{pol_trans_rate};
    my $eRate       = $rec->{pol_edit_rate};
    my $hRate       = $rec->{pol_hourly_rate};
    my $mRate       = $rec->{pol_mtpe_rate};
    my $dRate       = $rec->{pol_disc_rate};
    my $cur         = $rec->{pol_currency};
    my $pCtxCnt     = $rec->{pol_ctx_wcnt};
    my $pRepCnt     = $rec->{pol_rep_wcnt};
    my $pXFRCnt     = $rec->{pol_xfr_wcnt};
    my $p100Cnt     = $rec->{pol_100_wcnt};
    my $p95Cnt      = $rec->{pol_95_wcnt};
    my $p85Cnt      = $rec->{pol_85_wcnt};
    my $p75Cnt      = $rec->{pol_75_wcnt};
    my $p50Cnt      = $rec->{pol_50_wcnt};
    my $pNoCnt      = $rec->{pol_no_wcnt};
    my $pABase      = $rec->{pol_abase_wcnt};
    my $pALrn       = $rec->{pol_alrn_wcnt};
    my $wTot        = $rec->{pol_total_wcnt};
    my $wt_ctx      = $rec->{pol_ctx_weight};
    my $wt_rep      = $rec->{pol_rep_weight};
    my $wt_xfr      = $rec->{pol_xfr_weight};
    my $wt_100      = $rec->{pol_100_weight};
    my $wt_95       = $rec->{pol_95_weight};
    my $wt_85       = $rec->{pol_85_weight};
    my $wt_75       = $rec->{pol_75_weight};
    my $wt_50       = $rec->{pol_50_weight};
    my $wt_none     = $rec->{pol_none_weight};
    my $wt_abase    = $rec->{pol_abase_weight};
    my $wt_alrn     = $rec->{pol_alrn_weight};    
    my $dscAmt      = $rec->{pol_disc_amt};
    my $poTOT       = $rec->{pol_total_amt};
    my $projNo      = $rec->{pol_proj_no};
    my $comment     = $rec->{pol_int_notes};
    my $invRecd     = $rec->{pol_invoice_recd};
    my $dspUser     = $shared->getUserName( $rec->{pol_add_usr_id} );
    my $doMinDsc    = $rec->{pol_dscmin_rate};
    my $mdRate      = $rec->{pol_dscmin_rate_value};
    my $invID       = ( $rec->{pol_inv_id} ) + 0;
    
    if ( $doMinDsc eq "Y" ) {
        $hRate      = ( $hRate * ( $mdRate / 100 ) );
    }
    
    my $delim       = "<br>";
    $notes         =~ s/\r\n|\r|\n/$delim/g;
    
   	$comment     =~ s/\"/&quot;/g;
   	$comment     =~ s/\</&gt;/g;
   	
   	my $invChk    = "";
   	my $invDis    = "";
   	my $invInfo   = "";
   	if ( $invRecd eq "Y" ) {
   	    $invChk   = " checked";
   	}
   	if ( $invID > 0 ) {
   	    my $invRec = $shared->get_rec( "invoice", "inv_id", $invID );
   	    my $invNo  = $invRec->{inv_no};
   	    my $invDt  = $shared->getDateFmt( $invRec->{inv_date} );
   	    my $invSt  = $shared->getLookupDesc( "INV_STAT", $invRec->{inv_status} );
   	    $invInfo   = "<br><table border=0 cellpadding=2 cellspacing=0>\n"
   	               . "<tr><td valign=top align=left class=form>Invoice No</td>\n"
   	               . "<td valign=top align=left class=form><a href=\"$cgi_url?action=view_invoice&inv_id=$invID\" target=_blank><b>$invNo</b></a></td></tr>\n"
   	               . "<tr><td valign=top align=left class=form>Invoice Date</td>\n"
   	               . "<td valign=top align=left class=form><b>$invDt</b></td></tr>\n"
   	               . "<tr><td valign=top align=left class=form>Pmt Status</td>\n"
   	               . "<td valign=top align=left class=form><b>$invSt</b></td></tr>\n"
   	               . "</table>\n"; 
   	    $invDis    = " disabled";
   	}
   	
   	my $eLink   = "N/A";
   	if ( $lEml gt "" ) {
   	    $eLink  = "<a href=\"mailto:$lEml\" class=nav>$lEml</a>";
   	}

    my $pg_body = "<form action=\"$cgi_url\" method=get target=\"_blank\">\n";
    $pg_body   .= "<input type=hidden name=action value=\"po_pdf\">\n";
    $pg_body   .= "<input type=hidden name=pol_id value=\"$pid\">\n";

    $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
    $pg_body   .= "<tr><td valign=top align=left class=label>PO #:</td>\n";
    $pg_body   .= "<td valign=top align=left class=subhead>$poNo</td>\n";
    $pg_body   .= "<td valign=top align=right class=form><input type=submit value=\"Generate PDF\" class=form></td></tr>\n";
    $pg_body   .= "<tr><td valign=top align=left class=label>Created:</td>\n";
    $pg_body   .= "<td valign=top align=left class=subhead>$dspPODt (by $dspUser)</td>\n";
    $pg_body   .= "<td valign=top align=right class=form><div id=btn1><a href=\"$cgi_url?action=clone_po_form&pol_id=$pid\">Clone PO</a></div></tdL</tr>\n";
    $pg_body   .= "<tr><td valign=top align=left class=label>Linguist:</td>\n";
    $pg_body   .= "<td valign=top align=left class=callout colspan=2>$lName ($eLink)</td></tr>\n";
    $pg_body   .= "<tr><td valign=top align=left class=label>Client:</td>\n";
    $pg_body   .= "<td valign=top align=left class=callout colspan=2>$cName</td></tr>\n";
    $pg_body   .= "<tr><td valign=top align=left class=label>Project No:</td>\n";
    $pg_body   .= "<td valign=top align=left class=callout colspan=2>$projNo</td></tr>\n";
    $pg_body   .= "<tr><td valign=top align=left colspan=3 class=form><hr></td></tr>\n";
    $pg_body   .= "<tr><td valign=top align=left class=label>Instructions:</td>\n";
    $pg_body   .= "<td valign=top align=left class=form colspan=2>$notes</td></tr>\n";
    $pg_body   .= "</table><br><br>\n";
    
    if ( $doTR eq "Y" ) {
        my $amtctx  = $shared->round( $pCtxCnt * ( $wt_ctx / 100 ) * $tRate, 2 );
        my $amtrep  = $shared->round( $pRepCnt * ( $wt_rep / 100 ) * $tRate, 2 );
        my $amtxfr  = $shared->round( $pXFRCnt * ( $wt_xfr / 100 ) * $tRate, 2 );
        my $amt100  = $shared->round( $p100Cnt * ( $wt_100 / 100 ) * $tRate, 2 );
        my $amt95   = $shared->round( $p95Cnt * ( $wt_95 / 100 ) * $tRate, 2 );
        my $amt85   = $shared->round( $p85Cnt * ( $wt_85 / 100 ) * $tRate, 2 );
        my $amt75   = $shared->round( $p75Cnt * ( $wt_75 / 100 ) * $tRate, 2 );
        my $amt50   = $shared->round( $p50Cnt * ( $wt_50 / 100 ) * $tRate, 2 );
        my $amtNo   = $shared->round( $pNoCnt * ( $wt_none / 100 ) * $tRate, 2 );
        my $amtabs  = $shared->round( $pABase * ( $wt_abase / 100 ) * $tRate, 2 );
        my $amtaln  = $shared->round( $pALrn * ( $wt_alrn / 100 ) * $tRate, 2 );
        my $amtTot  = ( $amtctx + $amtrep + $amtxfr + $amt100 + $amt95 + $amt85 + $amt75 + $amt50 + $amtNo + $amtabs + $amtaln );

        $pg_body   .= "<span class=subhead>Translation Rate: $tRate ($cur)</span><br>\n";
	    $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
	    $pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
	    $pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
        $pg_body   .= "<tr><td valign=top align=left class=colhead>Match Types</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Words</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Weight</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Amount</td></tr>\n";

        my $row = 0;
        my $row_color;

        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>Context Match</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$pCtxCnt</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_ctx . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amtctx ) . "</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=left class=label><b>Repetitions</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$pRepCnt</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_rep . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amtrep ) . "</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>Cross-file Repetitions</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$pXFRCnt</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_xfr . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amtxfr ) . "</td></tr>\n";
    
        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=left class=label><b>100\%/Context Match</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$p100Cnt</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_100 . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amt100 ) . "</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>95\% - 99\%</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$p95Cnt</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_95 . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amt95 ) . "</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=left class=label><b>85\% - 94\%</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$p85Cnt</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_85 . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amt85 ) . "</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>75\% - 84\%</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$p75Cnt</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_75 . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amt75 ) . "</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=left class=label><b>50\% - 74\%</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$p50Cnt</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_50 . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amt50 ) . "</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>No Match</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$pNoCnt</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_none . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amtNo ) . "</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=left class=label><b>AdaptiveMT Baseline</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$pABase</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_abase . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amtabs ) . "</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>AdaptiveMT with Learnings</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>$pALrn</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $wt_alrn . "\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amtaln ) . "</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=left class=label><b>TRANSLATION SUB-TOTAL</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form><b>$wTot</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
        $pg_body  .= "<td valign=top align=right class=form><b>" . $shared->fmtNumber( $amtTot ) . "</b></td></tr>\n";
    
        if ( $amtTot < $hRate ) {
            my $extLabel = "";
            if ( $doMinDsc eq "Y" ) {
                $extLabel = " (Discounted $mdRate \%)";
            }
            $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
            $pg_body  .= "<td valign=top align=left class=label><b>MINIMUM RATE" . $extLabel . "</b></td>\n";
            $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
            $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
            $pg_body  .= "<td valign=top align=right class=form><b>" . $shared->fmtNumber( $hRate ) . "</b></td></tr>\n";
        }
            
        $pg_body .= "</table>\n";
        $pg_body .= "</td></tr>\n";
        $pg_body .= "</table><br>\n";
    }
    
    if ( $doED eq "Y" ) {
        my $edAmt   = $shared->round( $wTot * $eRate, 2 );

        $pg_body   .= "<span class=subhead>Review Rate: $eRate ($cur)</span><br>\n";
	    $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
	    $pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
	    $pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
        $pg_body   .= "<tr><td valign=top align=left class=colhead>Word Count</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Weight</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Amount</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=left class=form>$wTot</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>100\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $edAmt ) . "</td></tr>\n";
    
        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>REVIEW SUB-TOTAL</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
        $pg_body  .= "<td valign=top align=right class=form><b>" . $shared->fmtNumber( $edAmt ) . "</b></td></tr>\n";

        if ( $edAmt < $hRate ) {
            my $extLabel = "";
            if ( $doMinDsc eq "Y" ) {
                $extLabel = " (Discounted $mdRate \%)";
            }
            $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
            $pg_body  .= "<td valign=top align=left class=label><b>MINIMUM RATE" . $extLabel . "</b></td>\n";
            $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
            $pg_body  .= "<td valign=top align=right class=form><b>" . $shared->fmtNumber( $hRate ) . "</b></td></tr>\n";
        }
    
        $pg_body .= "</table>\n";
        $pg_body .= "</td></tr>\n";
        $pg_body .= "</table><br>\n";
    }
    
    if ( $doPF eq "Y" ) {
        my $hours = 0;
        if ( ( $wTot / $wph ) == int( $wTot / $wph ) ) {
            $hours = int( $wTot / $wph );
        } else {
            $hours = int( ( $wTot / $wph ) + 1 );
        }
        
        my $pfAmt   = $shared->round( $hours * $hRate, 2 );

        $pg_body   .= "<span class=subhead>Proof Reading Rate: $hRate ($cur)</span><br>\n";
	    $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
	    $pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
	    $pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
        $pg_body   .= "<tr><td valign=top align=left class=colhead>Calculated Hours</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Weight</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Amount</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=right class=form>$hours</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>100\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $pfAmt ) . "</td></tr>\n";
    
        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>PROOFREAD SUB-TOTAL</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
        $pg_body  .= "<td valign=top align=right class=form><b>" . $shared->fmtNumber( $pfAmt ) . "</b></td></tr>\n";
    
        $pg_body .= "</table>\n";
        $pg_body .= "</td></tr>\n";
        $pg_body .= "</table><br>\n";
    }
    
    if ( $doMT eq "Y" ) {
        my $mtAmt   = $shared->round( $wTot * $mRate, 2 );

        $pg_body   .= "<span class=subhead>MTPE Rate: $mRate ($cur)</span><br>\n";
	    $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
	    $pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
	    $pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
        $pg_body   .= "<tr><td valign=top align=left class=colhead>Word Count</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Weight</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Amount</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=left class=form>$wTot</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>100\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $mtAmt ) . "</td></tr>\n";
    
        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>MTPE SUB-TOTAL</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
        $pg_body  .= "<td valign=top align=right class=form><b>" . $shared->fmtNumber( $mtAmt ) . "</b></td></tr>\n";

        if ( $mtAmt < $hRate ) {
            my $extLabel = "";
            if ( $doMinDsc eq "Y" ) {
                $extLabel = " (Discounted $mdRate \%)";
            }
            $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
            $pg_body  .= "<td valign=top align=left class=label><b>MINIMUM RATE" . $extLabel . "</b></td>\n";
            $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
            $pg_body  .= "<td valign=top align=right class=form><b>" . $shared->fmtNumber( $hRate ) . "</b></td></tr>\n";
        }
    
        $pg_body .= "</table>\n";
        $pg_body .= "</td></tr>\n";
        $pg_body .= "</table><br>\n";
    }
            

    $pg_body   .= "</form>\n";

    $pg_body   .= "<form method=post action=\"$cgi_url\">\n";
    $pg_body   .= "<input type=hidden name=action value=\"add_po_fee\">\n";
    $pg_body   .= "<input type=hidden name=pol_id value=\"$pid\">\n";
    $pg_body   .= $inp_srch;
    
    $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
    $pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
    $pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
    $pg_body   .= "<tr><td valign=top align=left class=colhead colspan=3>Other Fees</td></tr>\n";
    $pg_body   .= "<tr bgcolor=\"$color_bg1\"><td valign=top align=left class=form><b>Service</b></td>\n";
    $pg_body   .= "<td valign=top align=right class=form><b>Amount</b></td>\n";
    $pg_body   .= "<td valign=top align=right class=form>&nbsp;</td></tr>\n";
    $pg_body   .= "<tr><td valign=top align=left class=form><input name=pof_service value=\"\" size=40 maxlength=40 class=form></td>\n";
    $pg_body   .= "<td valign=top align=right class=form><input name=pof_amt value=\"\" size=10 maxlength=10 class=form></td>\n";
    $pg_body   .= "<td valign=top align=right class=form><input type=submit value=\"Add Fee\" class=form></td></tr>\n";

    my $row = 0;
    my $row_color;
    
   	my $feeTot  = 0;
   	my $feeSQL  = "SELECT * FROM po_fee WHERE pof_pol_id = $pid ORDER BY pof_service";
   	my @feers   = $shared->getResultSet( $feeSQL );
   	foreach my $fee_rec ( @feers ) {
   	    my $fID  = $fee_rec->{pof_id};
   	    my $fSvc = $fee_rec->{pof_service};
   	    my $fAmt = $fee_rec->{pof_amt};

	   	if ( $row == 0 ) {
	   		$row_color = $color_bg2;
	   		$row = 1;
	   	} else {
	   		$row_color = $color_bg1;
	   		$row = 0;
	   	}
   	    
	   	$pg_body .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=form>$fSvc</td>\n";
	   	$pg_body .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $fAmt ) . "</td>\n";
   	    $pg_body .= "<td valign=top align=right class=form><a href=\"$cgi_url?action=del_po_fee&pof_id=$fID&pol_id=$pid&$sp_vars\" onClick=\"return confirm('Remove Fee from PO?')\"><img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete\" title=\"Delete Fee\"></a></tr>\n";
   	    
   	    $feeTot += $fAmt;
   	}
    
    $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
    $pg_body  .= "<td valign=top align=left class=label><b>FEE SUB-TOTAL</b></td>\n";
    $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
    $pg_body  .= "<td valign=top align=right class=form><b>" . $shared->fmtNumber( $feeTot ) . "</b></td></tr>\n";
    $pg_body   .= "</table>\n";
    $pg_body   .= "</td></tr>\n";
    $pg_body   .= "</table>\n";
    $pg_body   .= "</form>\n";
    
    if ( $doDsc eq "Y" ) {    
        $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
        $pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
        $pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
        $pg_body   .= "<tr><td valign=top align=left class=colhead colspan=3>Client Discount</td></tr>\n";
        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>$cName - $dRate\%</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
        $pg_body  .= "<td valign=top align=right class=form><b>(" . $shared->fmtNumber( $dscAmt ) . ")</b></td></tr>\n";
        $pg_body   .= "</table>\n";
        $pg_body   .= "</td></tr>\n";
        $pg_body   .= "</table><br>\n";
    }

    $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
    $pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
    $pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
    $pg_body   .= "<tr><td valign=top align=left class=colhead>PO TOTAL</td>\n";
    $pg_body   .= "<td valign=top align=right class=colhead>" . $shared->fmtNumber( $poTOT ) . "</td></tr>\n";

    $pg_body   .= "</table>\n";
    $pg_body   .= "</td></tr>\n";
    $pg_body   .= "</table><br>\n";
    
    my $lig_rec = $shared->get_rec( "linguist", "lig_id", $rec->{pol_ling_id} );
    my $pmtMeth = "Pmt Method: ";
    my $pmCnt   = 0;
    if ( $lig_rec->{lig_pmt_paypal} eq "Y" ) {
        $pmtMeth .= "Paypal (<a href=\"mailto:" . $lig_rec->{lig_pmt_paypal_eml} . "\" class=nav>" . $lig_rec->{lig_pmt_paypal_eml} . "</a>)";
        $pmCnt++;
    }
    if ( $lig_rec->{lig_pmt_check} eq "Y" ) {
        if ( $pmCnt > 0 ) {
            $pmtMeth .= ", Check";
        } else {
            $pmtMeth .= "Check";
        }
        $pmCnt++;
    }
    if ( $lig_rec->{lig_pmt_wire} eq "Y" ) {
        if ( $pmCnt > 0 ) {
            $pmtMeth .= ", Wire";
        } else {
            $pmtMeth .= "Wire";
        }
        $pmCnt++;
    }
    if ( $pmCnt == 0 ) {
        $pmtMeth .= "None";
    }
       
    $pg_body   .= "<form method=post action=\"$cgi_url\">\n";
    $pg_body   .= "<input type=hidden name=action value=\"update_po\">\n";
    $pg_body   .= "<input type=hidden name=pol_id value=\"$pid\">\n";
    $pg_body   .= $inp_srch;
    
    $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
    $pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
    $pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
    $pg_body   .= "<tr><td valign=top align=left class=subhead colspan=2>Internal Use</td></tr>\n";
#    $pg_body   .= "<tr><td valign=top align=left class=label>Content Type:</td>\n";
#    $pg_body   .= "<td valign=top align=left class=form bgcolor=\"$color_bg1\">\n";
#    $pg_body   .= "<select name=pol_content_type class=form><option value=\"\"></option>" . $shared->pop_lookup_select( "PO_CONTENT_TYPE", $cType, "" ) . "</select></td></tr>\n";
    $pg_body   .= "<tr><td valign=top align=left class=label>Notes:</td>\n";
    $pg_body   .= "<td valign=top align=left class=form bgcolor=\"$color_bg1\">\n";
    $pg_body   .= "<textarea name=pol_int_notes cols=50 rows=5 wrap class=form>$comment</textarea></td></tr>\n";
    $pg_body   .= "<tr><td valign=top align=left class=label>Invoice Status:</td>\n";
    $pg_body   .= "<td valign=top align=left class=form bgcolor=\"$color_bg1\">\n";
    $pg_body   .= "<input type=checkbox name=pol_invoice_recd value=\"Y\"" . $invChk . $invDis . "> Invoice Received - <i>$pmtMeth</i>";
    $pg_body   .= $invInfo . "</td></tr>\n";
    $pg_body   .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
    $pg_body   .= "<td valign=top align=left class=form><input type=submit value=\"Update\" class=form>&nbsp;\n";
    $pg_body   .= "<input type=button value=\"Return\" onClick=\"parent.location.href='$cgi_url?$sp_vars'\" class=form></td></tr>\n";
    $pg_body   .= "</table>\n";
    $pg_body   .= "</td></tr>\n";
    $pg_body   .= "</table>\n";
    $pg_body   .= "</form>\n";    

    $env->{html_title} = "$site_title - PO Details";
    $env->{page_head}  = "PO Details";
    $env->{page_body}  = $pg_body;
    $env->{template}   = "admin.html";

    $shared->page_gen( $env, $java_msg );
    
} 

###########################################################                   
# Stream Invoice PDF to browser
#----------------------------------------------------------
sub view_invoice {
    
    my ( $inv_id )  = @_;

    my $rec         = $shared->get_rec( "invoice", "inv_id", $inv_id );
    my $fileNm      = $rec->{inv_filename};
    my $ling_id     = $rec->{inv_ling_id};
    my $subD        = substr( $ling_id, 0, 1 );
    my $uPath       = $out_path . "/invoice/" . $subD;
    my $fName       = "$uPath/$fileNm";
    
    $shared->streamPDF( $fName );

}

###########################################################                   
# Delete Member from Database
#----------------------------------------------------------
sub del_po {

   	my $srch_init = $shared->get_cgi( "srch_init" );

   	my $pid       = $shared->get_cgi( "pol_id" );
    	
   	my $update = "DELETE FROM po_log WHERE pol_id = $pid";
   	$shared->doSQLUpdate( $update );
    	
    &search_any( $srch_init );
}


###########################################################                   
# Update the PO
#----------------------------------------------------------
sub update_po {

   	my $srch_init = $shared->get_cgi( "srch_init" );

   	my $pid       = $shared->get_cgi( "pol_id" );
    my $comment   = $shared->get_cgi( "pol_int_notes" );
#    my $cType     = $shared->get_cgi( "pol_content_type" );
    my $invRcd    = $shared->get_cgi( "pol_invoice_recd" );
    
   	$comment     =~ s/'/\\'/g;
   	
   	if ( $invRcd eq "" ) {
   	    $invRcd = "N";
   	}

   	my $update = "UPDATE po_log SET pol_int_notes = '$comment', pol_invoice_recd = '$invRcd' WHERE pol_id = $pid";
   	$shared->doSQLUpdate( $update );
    	
    &search_any( $srch_init );
}

###########################################################                   
# Get Names from translator table as selection list
#----------------------------------------------------------
sub getLing {
    
    my ( $filt ) = @_;
    
    my $SQL = "SELECT * FROM linguist\n";
    $SQL   .= "ORDER BY lig_last_name, lig_first_name";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $name     = $shared->trim( $rec->{lig_last_name} ) . ", " . $shared->trim( $rec->{lig_first_name} );
        if ( $rec->{lig_last_name} eq "" ) {
            $name    = $rec->{lig_first_name};
        }
        if ( $rec->{lig_first_name} eq "" ) {
            $name    = $rec->{lig_last_name};
        }
        my $tid   = $rec->{lig_id};
        $result .= "<option value=\"$tid\">$name</option>";
    }
    
    return $result;
}

###########################################################                   
# Get Names from translator table as selection list
#----------------------------------------------------------
sub getLangLing {
    
    my ( $src, $tgt ) = @_;
    
    my $SQL = "SELECT * FROM linguist_lang\n"
            . "INNER JOIN linguist ON lil_lig_id = lig_id\n"
            . "WHERE lil_src_lng_id = $src AND lil_tgt_lng_id = $tgt\n"
            . "AND lig_dont_use = 'N'\n"
            . "ORDER BY lig_last_name, lig_first_name";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $name     = $shared->trim( $rec->{lig_last_name} ) . ", " . $shared->trim( $rec->{lig_first_name} );
        if ( $rec->{lig_last_name} eq "" ) {
            $name    = $rec->{lig_first_name};
        }
        if ( $rec->{lig_first_name} eq "" ) {
            $name    = $rec->{lig_last_name};
        }
        my $tid   = $rec->{lig_id};
        my $lid   = $rec->{lil_id};
        my $tRt   = $rec->{lil_trans_rate};
        my $eRt   = $rec->{lil_edit_rate};
        my $hRt   = $rec->{lil_hourly_rate};
        my $mRt   = $rec->{lil_mtpe_rate};
        my $rCds  = "";
        if ( $tRt > 0 ) {
            $rCds .= "t";
        }
        if ( $eRt > 0 ) {
            $rCds .= "e";
        }
        if ( $hRt > 0 ) {
            $rCds .= "h";
        }
        if ( $mRt > 0 ) {
            $rCds .= "m";
        }
        $result .= "<option value=\"$tid" . "-" . $lid . $rCds . "\">$name ($tRt/$eRt/$hRt/$mRt)</option>";
    }
    
    return $result;
}

###########################################################                   
# Get Names from translator table as selection list
#----------------------------------------------------------
sub getClient {
    
    my ( $filt ) = @_;
    
    my $SQL = "SELECT * FROM client\n"
            . "WHERE ( cln_type = 'B' OR ( cln_type = 'P' AND cln_personal_usr_id = $usr_id ) )\n"
            . "AND cln_other = 'N'\n"
            . "ORDER BY cln_name";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $name     = $rec->{cln_name};
        my $cid      = $rec->{cln_id};
        $result .= "<Option value=\"$cid\">$name</option>";
    }
    
    return $result;
}

###########################################################                   
# Get Names from translator table as selection list
#----------------------------------------------------------
sub getClientSelect {
    
    my ( $dflt_id ) = @_;
    
    my $SQL = "SELECT * FROM client\n"
            . "WHERE ( cln_type = 'B' OR ( cln_type = 'P' AND cln_personal_usr_id = $usr_id ) )\n"
            . "AND cln_other = 'N'\n"
            . "AND cln_active = 'Y'\n"
            . "ORDER BY cln_name";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $name     = $rec->{cln_name};
        my $cid      = $rec->{cln_id};
        if ( $cid eq $dflt_id ) {
            $result .= "<option selected value=\"$cid\">$name</opion>";
        } else {
            $result .= "<option value=\"$cid\">$name</option>";
        }
    }
    
    return $result;
}

###########################################################                   
# Get projects for a client as a selection list
#----------------------------------------------------------
sub getProjects {
    
    my ( $cln_id, $dflt_prj_no ) = @_;
    
    my $SQL = "SELECT prj_no FROM project\n"
            . "WHERE prj_cln_id = $cln_id\n"
            . "AND ( prj_delivery_date IS NULL OR prj_delivery_date = '0000-00-00' )\n"
            . "ORDER BY prj_no";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $prjNo     = $rec->{prj_no};
        if ( $prjNo eq $dflt_prj_no ) {
            $result .= "<option selected value=\"$prjNo\">$prjNo</option>";
        } else {
            $result .= "<option value=\"$prjNo\">$prjNo</option>";
        }
    }
    
    return $result;
}


###########################################################                   
# Get Linguist Name
#----------------------------------------------------------
sub getLingName {
    
    my ( $lid ) = @_;
    
    my $rec = $shared->getResultRec( "SELECT lig_first_name, lig_last_name FROM linguist WHERE lig_id = $lid" );
    my $result = $shared->trim( $rec->{lig_last_name} ) . ", " . $shared->trim( $rec->{lig_first_name} );
    if ( $rec->{lig_last_name} eq "" ) {
        $result = $rec->{lig_first_name};
    }
    if ( $rec->{lig_first_name} eq "" ) {
        $result = $rec->{lig_last_name};
    }
    
    return $result;
}

###########################################################                   
# Get Linguist Email
#----------------------------------------------------------
sub getLingEml {
    
    my ( $lid ) = @_;
    
    my $rec = $shared->getResultRec( "SELECT lig_email FROM linguist WHERE lig_id = $lid" );
    my $result = $rec->{lig_email};
    
    return $result;
}

###########################################################                   
# Get Client Name
#----------------------------------------------------------
sub getClnName {
    
    my ( $cid ) = @_;
    
    my $rec = $shared->getResultRec( "SELECT cln_name FROM client WHERE cln_id = $cid" );
    my $result = $rec->{cln_name};
    
    return $result;
}

###########################################################                   
# Get Client ID from Code
#----------------------------------------------------------
sub getCLNID {
    
    my ( $code ) = @_;
    
    my $rec = $shared->getResultRec( "SELECT cln_id FROM client WHERE cln_code = '$code'" );
    my $result = $rec->{cln_id};
    
    return $result;
}

###########################################################                   
# generate new PO number
#----------------------------------------------------------
sub genNewPONo {

    my $yr   = $shared->getCurrentYear;
    my $next = $shared->getConfigInt( "ADM_NEXTPO" );
    
    my $prefix = "SDT" . substr( "$yr", 2, 2 ) . "-";
    my $result = "";
    
    my $good   = "N";
    while ( $good eq "N" ) {
        my $test = $prefix . sprintf( "%05s", $next );
        my $cnt  = $shared->getSQLCount( "SELECT COUNT(*) FROM po_log WHERE pol_pono = '$test'" );
        if ( $cnt == 0 ) {
            $good = "Y";
            $result = $test;
        }
        $next++;
    }
    
    $shared->setConfig( "ADM_NEXTPO", $next );
    
    return $result;
}

##########################################################                   
# Get Languages as selection list
#----------------------------------------------------------
sub getLang {
    
    my ( $filt ) = @_;
    
    my $SQL = "SELECT * FROM language ORDER BY lng_desc";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $code     = $rec->{lng_id};
        my $desc     = $rec->{lng_desc};
        $result .= "<option value=\"$code\">$desc</option>";
    }
    
    return $result;
}

############################################################
# Add the fee
############################################################
sub add_po_fee {

    my $pid = $shared->get_cgi( "pol_id" );
    
   	my $svc = $shared->get_cgi( "pof_service" );
   	my $amt = ( $shared->get_cgi( "pof_amt" ) ) + 0;

    $svc   =~ s/'/\\'/g;
        	
    my $insert   = "INSERT INTO po_fee ( pof_pol_id, pof_service, pof_amt )\n"
                 . "VALUES\n"
                 . "( $pid, '$svc', $amt )";
    $shared->doSQLUpdate( $insert );
   	        
   	&calcPO( $pid );
   	        
    &view_po( $pid );

}

############################################################
# Remove the fee
############################################################
sub del_po_fee {

    my $fID   = $shared->get_cgi( "pof_id" );
    my $pid   = $shared->get_cgi( "pol_id" );
    
    my $update    = "DELETE FROM po_fee WHERE pof_id = $fID";
    $shared->doSQLUpdate( $update );
        
    &calcPO( $pid );
        	
    &view_po( $pid );

}


############################################################
# Display form for selecting project for report
############################################################
sub prj_rpt_form {

   	$conf->{form_head}    = "Project Report";
   	$conf->{form_start}   = "<form method=post name=adm_cln_form action=\"$cgi_url\">\n"
    	                  . "<input type=hidden name=action value=\"prj_rpt\">\n";
    my $list = "";
    my $SQL  = "SELECT DISTINCT pol_proj_no FROM po_log ORDER BY pol_proj_no";
    my @rs   = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $prjNo = $rec->{pol_proj_no};
        $list .= "<option value=\"$prjNo\">$prjNo</option>";
    }
    
    $conf->{f_proj_no}    = "<select name=pol_proj_no class=form>" . $list . "</select>";
       	
   	$conf->{form_submit}  = "<input type=submit value=\"Generate\" class=form>&nbsp;\n"
                          . "<input type=button value=\"Cancel\" onClick='history.go(-1)' class=form>";
   	$conf->{form_end}     = "</form>";
    	
    my $pg_body = &disp_form( "po_prj_rpt_form" );

    $env->{page_body}        = $pg_body;
    $shared->page_gen( $env, $java_msg );
    	
}

############################################################
# Generate Project extract in Excel Format
############################################################
sub genProjRpt {

    my $rptDate  = $shared->getSQLToday;

    my $prjNo    = $shared->get_cgi( "pol_proj_no" );
        
	my $fName    = "$forms_path/sdt_proj_rpt.xlsx";
	my $urlName  = "$forms_url/sdt_proj_rpt.xlsx";
	my $nowDesc  = $shared->getDateDesc( $rptDate, 1, 1 );
	
	my $workbook = Excel::Writer::XLSX->new( $fName );
	$workbook->set_tempdir( $temp_path );
	
	my $c_litegrey    = $workbook->set_custom_color( 40, 204, 204, 204 );
	my $c_litegreen   = $workbook->set_custom_color( 41, 210, 255, 214 );
    my %headFont      = ( font  => 'Arial', size  => 14, color => 'black', valign  => 'bottom', bold  => 1 );
    my %subFont       = ( font  => 'Arial', size  => 12, color => 'black', valign  => 'bottom', bold  => 1 );
    my %titleFont     = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom' );
    my %titleFontRt   = ( font  => 'Arial', size  => 10, color => 'black', valign  => 'bottom', align => 'right' );
    my %colFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'bottom', bold => 1, bg_color => $c_litegrey );
    
    my %bodyFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', border => 1, border_color => $c_litegrey );
    my %boldFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, border => 1, border_color => $c_litegrey );
    my %dateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', num_format => 'mm/dd/yy' );
    my %numFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %boldNumFont   = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right' );
    my %currFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %boldCurrFont  = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', bold => 1, align => 'right', border => 1, border_color => $c_litegrey );
    my %rateFont      = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
    my %pctFont       = ( font  => 'Arial', size  => 8, color => 'black', valign  => 'top', align => 'right', border => 1, border_color => $c_litegrey );
	
	my $fmtHead  = $workbook->add_format( %headFont );
	my $fmtSub   = $workbook->add_format( %subFont );
	my $fmtTitle = $workbook->add_format( %titleFont );
	my $fmtTtlRt = $workbook->add_format( %titleFontRt );
	my $fmtCol   = $workbook->add_format( %colFont );
	my $fmtBody  = $workbook->add_format( %bodyFont );
	my $fmtBold  = $workbook->add_format( %boldFont );
	my $fmtDate  = $workbook->add_format( %dateFont );
	my $fmtNum   = $workbook->add_format( %numFont );
	my $fmtBNum  = $workbook->add_format( %boldNumFont );
	my $fmtCurr  = $workbook->add_format( %currFont );
	my $fmtBCurr = $workbook->add_format( %boldCurrFont );
	my $fmtRate  = $workbook->add_format( %rateFont );
	my $fmtPct   = $workbook->add_format( %pctFont );
	
	$fmtCurr->set_num_format( "\$0.00" );
	$fmtBCurr->set_num_format( "\$0.00" );
	$fmtRate->set_num_format( "\$0.0000" );
	$fmtPct->set_num_format( "0.00\%" );

	my $SQL      = "SELECT pol_pono, cln_code, cln_name, lig_last_name, lig_first_name, pol_total_amt\n"
	             . "FROM po_log\n"
	             . "INNER JOIN client ON pol_cln_id = cln_id\n"
	             . "INNER JOIN linguist ON pol_ling_id = lig_id\n"
	             . "WHERE pol_proj_no = '$prjNo' ORDER BY pol_pono";
    my @rs       = $shared->getResultSet( $SQL );
    	             
    my $sheet    = $workbook->add_worksheet( "Project POs" );
            
    $sheet->write_string( 0, 0, "SDT Project - $prjNo", $fmtHead );
    $sheet->write_string( 0, 4, "As of: $nowDesc", $fmtTtlRt );
        
    my @colHead  = ( "PO Number", "Client ID", "Name", "Linguist", "PO Amount" );
    $sheet->write_row( 2, 0, \@colHead, $fmtCol );
        
        
    my $tAmt = 0;
    my $row  = 3;
	foreach my $rec ( @rs ) {

        my $pono   = $rec->{pol_pono};
        my $cID    = $rec->{cln_code};
        my $cName  = $rec->{cln_name};
        my $lName  = $rec->{lig_first_name} . " " . $rec->{lig_last_name};
        my $pAmt   = ( $rec->{pol_total_amt} ) + 0;
        
        $tAmt += $pAmt;
        
        $sheet->write_string( $row, 0, $pono, $fmtBold );
        $sheet->write_string( $row, 1, $cID, $fmtBody );
        $sheet->write_string( $row, 2, $cName, $fmtBody );
        $sheet->write_string( $row, 3, $lName, $fmtBody );
        $sheet->write_number( $row, 4, $pAmt, $fmtCurr );

        $row++;
    }
    
    $sheet->write_string( $row, 3, "TOTAL AMOUNT:", $fmtBold );
    $sheet->write_formula( $row, 4, "=SUM(E4:E" . $row . ")", $fmtBCurr, $tAmt );

    $sheet->set_column( 0, 0, 20 );
    $sheet->set_column( 1, 1, 10 );
    $sheet->set_column( 2, 2, 36 );
    $sheet->set_column( 3, 3, 46 );
    $sheet->set_column( 4, 4, 20 );

    $sheet->hide_gridlines( 2 );
    $sheet->freeze_panes( 3, 0 );

    $workbook->close();
    
    system( "chmod 644 $fName" );
    
    return $urlName;

}
