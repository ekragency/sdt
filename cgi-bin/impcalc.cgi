#!/usr/bin/perl
###########################################################################
# impcalc.cgi                  Metafraze Tools module - Import analysis file and
#                              calculate rates.
#                              Moonlight Design, LLC
# Created 07/24/2013           http://www.dbdev.biz
###########################################################################
# Copyright 2004, P. Eric Huber
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use auth;

my $config = new config;
my $shared = new shared;
my $auth   = auth->new( $shared );

$| = 1;

# HTML template
my ($src_path)    = $config->get_src_path;
my ($script_path) = $config->get_script_path;
my ($cgi_url)     = "$script_path/impcalc.cgi";
my ($adm_url)     = "$script_path/admin.cgi";
my ($img_path)    = $config->get_img_path;
my ($imp_path)    = $config->get_imp_path;
my ($base_url)    = $cgi_url;
my ($site_title)  = $config->get_app_name;
my ($app_ver)     = $config->get_app_ver;
my ($js_path)     = $config->get_js_path;
my ($log_path)    = $config->get_log_path;

my (%colors)      = $config->get_colors;
my $color_head1   = $colors{ "HEAD1" };
my $color_head2   = $colors{ "HEAD2" };
my $color_head3   = $colors{ "HEAD3" };
my $color_bg1     = $colors{ "BG1" };
my $color_bg2     = $colors{ "BG2" };
my $color_bg3     = $colors{ "BG3" };
my $color_error   = $colors{ "ERROR" };
my $java_msg      = "";
my $access_level  = 0;

# Template Variables
my ($env);
$env->{cgi_url}         = $cgi_url;
$env->{img_path}        = $img_path;
$env->{home_url}        = $base_url;
$env->{color_error}     = $color_error;
$env->{login_stat}      = $auth->getLoginStatus;
$env->{admin_nav_links} = $auth->getUserOptions;
$env->{css}             = "style.css";
$env->{app_ver}         = $app_ver;


# Main program
my $action = $shared->get_cgi( "action" );

if ( $auth->validate_access( "impcalc" ) ) {
	
    if ( $action eq "process" ) {
	    &process_file;
    } elsif ( $action eq "disp_po" ) {
        my $pid = $shared->get_cgi( "pid" );
        &dispSummary( $pid );
    } else {
	    &print_main;
    }
} else {
	if ( $action eq "" ) {
		$auth->login( $adm_url, "Y" );
	} else {
	    $auth->login( $adm_url, "N" );
	}
}
    

exit;


###########################################################                   
# Print Main Admin Options screen
#----------------------------------------------------------
sub print_main {

    my $curUser = $auth->getCurrentUserLogin;
    
    my $trSelect = &getNames( "TR" );
    my $edSelect = &getNames( "ED" );
    my $pfSelect = &getNames( "PF" );
    
    my $pg_body = "";
    $pg_body .= "<form method=post name=invoice_upload action=\"$cgi_url\" enctype=\"multipart/form-data\">\n";
    $pg_body .= "<input type=hidden name=action value=\"process\">\n";
    
    $pg_body .= "<table border=0 cellpadding=0 cellspacing=0 width=\"640\">\n";
    $pg_body .= "<tr><td>\n";
    $pg_body .= "<div class=blur><div class=shadow><div class=framework>\n";
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=\"640\">\n";
    $pg_body .= "<tr bgcolor=$color_bg1><td valign=top align=left>\n";
    $pg_body .= "<table border=0 cellpadding=3 cellspacing=0 width=\"636\">\n";
    $pg_body .= "<tr bgcolor=$color_head2><td valign=top align=left class=colhead colspan=2>Import Analysis Log</td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label width=160><b>PO Number:</b></td>\n";
    $pg_body .= "<td valign=top align=left class=form width=476><input name=po_no value=\"\" class=form size=15 maxlength=20></td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label width=160><b>Translator:</b></td>\n";
    $pg_body .= "<td valign=top align=left class=form width=476><select name=tr_rec_id class=form>" . $trSelect . "</select></td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label width=160><b>Editor:</b></td>\n";
    $pg_body .= "<td valign=top align=left class=form width=476><select name=ed_rec_id class=form><option value=\"\"></option>" . $edSelect . "</select></td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label width=160><b>Proof-reader:</b></td>\n";
    $pg_body .= "<td valign=top align=left class=form width=476><select name=pf_rec_id class=form><option value=\"\"></option>" . $pfSelect . "</select></td></tr>\n";
    
    $pg_body .= "<tr><td valign=top align=left class=label width=160><b>Analysis Log File:</b></td>\n";
    $pg_body .= "<td valign=top align=left class=form width=476><input type=file name=imp_file value=\"\" class=form></td></tr>\n";
    $pg_body .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
    $pg_body .= "<td valign=top align=left class=form><input type=submit value=\"Import Now!\" class=form></td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</div></div></div>\n";
    $pg_body .= "</td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</form>\n";
    
    $pg_body .= "</form>\n";

	my $SQL = "SELECT pol_add_date, pol_pono, pol_id\n"
	        . "FROM po_log\n"
            . "ORDER  BY pol_add_date DESC limit 100";
        
    my @rs    = $shared->getResultSet( $SQL );
    my $stSel = "";
    foreach my $rec ( @rs ) {
    	my $po_no = $rec->{pol_pono};
    	my $po_dt = $rec->{pol_add_date};
    	my $pid   = $rec->{pol_id};
    	
    	my $dspDate = $shared->getDateFmt( $po_dt );
    	my $desc    = "PO #$po_no ($dspDate)";
    	$stSel     .= "<option value=\"$cgi_url?action=disp_po&pid=$pid\">$desc</option>\n";
    }
    
    $pg_body .= "<form name=\"polist\" action=\"$cgi_url\">\n";
    $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=650>\n";
    $pg_body .= "<tr><td valign=top align=left width=160 class=label>\n";
    $pg_body .= "<b>Select Existing PO:</b></td>\n";
    $pg_body .= "<td valign=top align=left class=form width=490>";
    $pg_body .= "<select name=\"poselect\" class=form onChange=\"POFilter()\"><option selected value=\"\">[Select PO]</option>\n";
    $pg_body .= $stSel;
    $pg_body .= "</select></td></tr>\n";
    $pg_body .= "</table>\n";
    $pg_body .= "</form>\n";

    my $js = "<script language = \"JavaScript\">\n";
    $js   .= "<!-- For Filter \n";
    $js   .= "function POFilter() {\n";
    $js   .= "    var URL = document.polist.poselect.options[document.polist.poselect.selectedIndex].value\n";
    $js   .= "    parent.location.href = URL\n";
    $js   .= "}\n";
    $js   .= "-->\n";
    $js   .= "</script>\n";
    
    $env->{javascript} = $js;
    $env->{html_title} = "$site_title - Analysis Import and PO Summary";
    $env->{page_head}  = "Analysis Import/Summary";
    $env->{page_body}  = $pg_body;
    $env->{template}   = "admin.html";

    $shared->page_gen( $env, $java_msg );

}

###########################################################                   
# Process the file
#----------------------------------------------------------
sub process_file {

    my $newFl  = "$imp_path/last_analysis.txt";
    my $status = $shared->uploadFile( "imp_file", $newFl, "" );
    if ( $status == 0 ) {
        
        my $pid = &parseFile( $newFl );
        if ( $pid gt "" ) {
            
            my $po_no = $shared->get_cgi( "po_no" );
            my $trID  = $shared->get_cgi( "tr_rec_id" );
            my $edID  = $shared->get_cgi( "ed_rec_id" );
            my $pfID  = $shared->get_cgi( "pf_rec_id" );
            
            my $tRate = &getRate( $trID, "rate" );
            my $eRate = &getRate( $edID, "editrate" );
            my $pRate = &getRate( $pfID, "hourly" );
            
            my $tCur  = &getCurrency( $trID, "rate" );
            my $eCur  = &getCurrency( $edID, "editrate" );
            my $pCur  = &getCurrency( $pfID, "hourly" );
            
            my $update = "UPDATE po_log SET pol_pono = '$po_no', pol_trans_id = '$trID', pol_edit_id = '$edID',\n"
                       . "pol_proof_id = '$pfID', pol_trans_rate = $tRate, pol_edit_rate = $eRate, pol_hourly_rate = $pRate,\n"
                       . "pol_cur_trans = '$tCur', pol_cur_edit = '$eCur', pol_cur_proof = '$pCur' WHERE pol_id = $pid";
            $shared->doSQLUpdate( $update );
            
            &dispSummary( $pid );
            
        } else {
            
            $java_msg = "Problem parsing file. Contact Administrator.";
            &print_main;
            
        }
        
    } else {
        $java_msg = "Problem importing file [Status: $status].  Contact Administrator.";
        &print_main;
    }
	
}


###########################################################                   
# Parse File.
#----------------------------------------------------------
sub parseFile {

    my ( $fileIn ) = @_;
    
    my $retval   = "";
    my $p_repcnt = 0;
    my $p_100cnt = 0;
    my $p_95cnt  = 0;
    my $p_85cnt  = 0;
    my $p_75cnt  = 0;
    my $p_50cnt  = 0;
    my $p_nocnt  = 0;
    my $totCnt   = 0;
    my $debug    = $config->get_debug;
    if ( $debug ) {
        open ( DEBUG, ">$log_path/debug.log" ) or $retval = "ERROR";
    }
    
	open ( FILE, $fileIn ) or $retval = "ERROR";
	my @FILE = <FILE>;
	close( FILE );

    if ( $retval eq "" ) {
        
        my $procLines = 0;
        
        foreach my $line ( @FILE ) {
    	
	        $line = $shared->trim( $line );
	        $line =~ s/[\n\r]+//g;
	        $line =~ s/\,//g;
    	    
 	        if ( $procLines == 1 ) {
 	            
 	            if ( $debug ) {
 	                print DEBUG "Checking Line [$line]\n";
 	            }
            
                if ( $line =~ /^Repetitions\s+\d+\s+(\d+)/ ) {
                    $p_repcnt += $1;
                    $totCnt   += $p_repcnt;
                    if ( $debug ) {
                        print DEBUG "Matched - Repetitions: [cnt: $p_repcnt]\n";
                    }
                } elsif ( $line =~ /^100\%\s+\d+\s+(\d+)/ ) {
                    $p_100cnt += $1;
                    $totCnt   += $p_100cnt;
                } elsif ( $line =~ /^95\% \- 99\%\s+\d+\s+(\d+)/ ) {
                    $p_95cnt += $1;
                    $totCnt  += $p_95cnt;
                } elsif ( $line =~ /^85\% \- 94\%\s+\d+\s+(\d+)/ ) {
                    $p_85cnt += $1;
                    $totCnt  += $p_85cnt;
                } elsif ( $line =~ /^75\% \- 84\%\s+\d+\s+(\d+)/ ) {
                    $p_75cnt += $1;
                    $totCnt  += $p_75cnt;
                } elsif ( $line =~ /^50\% \- 74\%\s+\d+\s+(\d+)/ ) {
                    $p_50cnt += $1;
                    $totCnt  += $p_50cnt;
                } elsif ( $line =~ /^No Match\s+\d+\s+(\d+)/ ) {
                    $p_nocnt += $1;
                    $totCnt  += $p_nocnt;
                    $procLines = 0;
                }
                

  	        } else {
    	        
  	            if ( $line =~ /^Analyse Total \(.+/ ) {
   	                $procLines = 1;
   	            }
   	        }
   	    }
    	
    	if ( $debug ) {
    	    print DEBUG "Total Cnt: $totCnt\n";
    	}
    	
    	if ( $totCnt > 0 ) {
    	    my $insert = "INSERT INTO po_log ( pol_add_date, pol_rep_wcnt, pol_100_wcnt, pol_95_wcnt,\n"
    	               . "pol_85_wcnt, pol_75_wcnt, pol_50_wcnt, pol_no_wcnt )\n"
    	               . "VALUES ( now(), $p_repcnt, $p_100cnt, $p_95cnt, $p_85cnt, $p_75cnt, $p_50cnt, $p_nocnt )";
            $shared->doSQLUpdate( $insert );
            $retval    = "" . ( $shared->getSQLLastInsertID ) . "";
    	}
    	
    }
    
    if ( $debug ) {
        print DEBUG "retval: [$retval]\n";
        close ( DEBUG );
    }
    
    if ( $retval eq "ERROR" ) {
        $retval = "";
    }
    
    return $retval;
}	    

###########################################################                   
# Display summary of PO
#----------------------------------------------------------
sub dispSummary {
    
    my ( $pid )  = @_;
    
    my $SQL         = "SELECT * FROM po_log WHERE pol_id = $pid";
    my $rec         = $shared->getResultRec( $SQL );
    my $poDt       = $rec->{pol_add_date};
    my $dspPODt    = $shared->getDateFmt( $poDt );
    my $poNo       = $rec->{pol_pono};
    my $tRate      = $rec->{pol_trans_rate};
    my $tCur       = $rec->{pol_cur_trans};
    my $eCur       = $rec->{pol_cur_edit};
    my $hCur       = $rec->{pol_cur_proof};
    my $eRate      = $rec->{pol_edit_rate};
    my $hRate      = $rec->{pol_hourly_rate};
    my $minAmt     = $rec->{pol_min_amt};
    my $pRepCnt    = $rec->{pol_rep_wcnt};
    my $p100Cnt    = $rec->{pol_100_wcnt};
    my $p95Cnt     = $rec->{pol_95_wcnt};
    my $p85Cnt     = $rec->{pol_85_wcnt};
    my $p75Cnt     = $rec->{pol_75_wcnt};
    my $p50Cnt     = $rec->{pol_50_wcnt};
    my $pNoCnt     = $rec->{pol_no_wcnt};
    my $amt95      = $shared->round( $p95Cnt * .3 * $tRate, 2 );
    my $amt85      = $shared->round( $p85Cnt * .3 * $tRate, 2 );
    my $amt75      = $shared->round( $p75Cnt * .6 * $tRate, 2 );
    my $amt50      = $shared->round( $p50Cnt * $tRate, 2 );
    my $amtNo      = $shared->round( $pNoCnt * $tRate, 2 );
    my $wTot       = ( $pRepCnt + $p100Cnt + $p95Cnt + $p85Cnt + $p75Cnt + $p50Cnt + $pNoCnt );
    my $amtTot     = ( $amt95 + $amt85 + $amt75 + $amt50 + $amtNo );
    
    my $pg_body = "<span class=subhead>PO Details - $dspPODt (#$poNo)</span><br>\n";
    $pg_body   .= "<a href=\"$cgi_url\" class=nav>[ Return to Main ]</a><br><br>\n";
    $pg_body   .= "<span class=subhead>Translation Rate: $tRate ($tCur)</span><br>\n";
	$pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
	$pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
	$pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
    $pg_body   .= "<tr><td valign=top align=left class=colhead>Match Types</td>\n";
    $pg_body   .= "<td valign=top align=right class=colhead>Words</td>\n";
    $pg_body   .= "<td valign=top align=right class=colhead>Weight</td>\n";
    $pg_body   .= "<td valign=top align=right class=colhead>Cost</td></tr>\n";

    my $row = 0;
    my $row_color;

    $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
    $pg_body  .= "<td valign=top align=left class=label><b>Repetitions</b></td>\n";
    $pg_body  .= "<td valign=top align=right class=form>$pRepCnt</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>0\%</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>0.00</td></tr>\n";
    
    $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
    $pg_body  .= "<td valign=top align=left class=label><b>100\%</b></td>\n";
    $pg_body  .= "<td valign=top align=right class=form>$p100Cnt</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>0\%</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>0.00</td></tr>\n";

    $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
    $pg_body  .= "<td valign=top align=left class=label><b>95\% - 99\%</b></td>\n";
    $pg_body  .= "<td valign=top align=right class=form>$p95Cnt</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>30\%</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amt95 ) . "</td></tr>\n";

    $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
    $pg_body  .= "<td valign=top align=left class=label><b>85\% - 94\%</b></td>\n";
    $pg_body  .= "<td valign=top align=right class=form>$p85Cnt</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>30\%</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amt85 ) . "</td></tr>\n";

    $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
    $pg_body  .= "<td valign=top align=left class=label><b>75\% - 84\%</b></td>\n";
    $pg_body  .= "<td valign=top align=right class=form>$p75Cnt</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>60\%</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amt75 ) . "</td></tr>\n";

    $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
    $pg_body  .= "<td valign=top align=left class=label><b>50\% - 74\%</b></td>\n";
    $pg_body  .= "<td valign=top align=right class=form>$p50Cnt</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>100\%</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amt50 ) . "</td></tr>\n";

    $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
    $pg_body  .= "<td valign=top align=left class=label><b>No Match</b></td>\n";
    $pg_body  .= "<td valign=top align=right class=form>$pNoCnt</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>100\%</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amtNo ) . "</td></tr>\n";

    $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
    $pg_body  .= "<td valign=top align=left class=label><b>TRANSLATION SUB-TOTAL</b></td>\n";
    $pg_body  .= "<td valign=top align=right class=form>$wTot</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
    $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $amtTot ) . "</td></tr>\n";
    
    $pg_body .= "</table>\n";
    $pg_body .= "</td></tr>\n";
    $pg_body .= "</table><br>\n";

    if ( $eRate > 0 ) {
        my $edAmt   = ( $wTot * $eRate );

        $pg_body   .= "<span class=subhead>Editing Rate: $eRate ($eCur)</span><br>\n";
	    $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
	    $pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
	    $pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
        $pg_body   .= "<tr><td valign=top align=left class=colhead>Word Count</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Weight</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Cost</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=right class=form>$wTot</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>100\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $edAmt ) . "</td></tr>\n";
    
        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>EDITING SUB-TOTAL</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $edAmt ) . "</td></tr>\n";
    
        $pg_body .= "</table>\n";
        $pg_body .= "</td></tr>\n";
        $pg_body .= "</table><br>\n";
    }
    
    if ( $hRate > 0 ) {
        my $hours = 0;
        if ( ( $wTot / 8000 ) == int( $wTot / 8000 ) ) {
            $hours = int( $wTot / 8000 );
        } else {
            $hours = int( ( $wTot / 8000 ) + 1 );
        }
        
        my $pfAmt   = ( $hours * $hRate );

        $pg_body   .= "<span class=subhead>Proof Reading Rate: $hRate ($hCur)</span><br>\n";
	    $pg_body   .= "<table border=0 cellpadding=2 cellspacing=0 width=600>\n";
	    $pg_body   .= "<tr bgcolor=\"$color_bg2\"><td valign=top align=left>\n";
	    $pg_body   .= "<table border=0 cellpadding=3 cellspacing=0 width=596>\n";
        $pg_body   .= "<tr><td valign=top align=left class=colhead>Calculated Hours</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Weight</td>\n";
        $pg_body   .= "<td valign=top align=right class=colhead>Cost</td></tr>\n";

        $pg_body  .= "<tr bgcolor=\"$color_bg2\">";
        $pg_body  .= "<td valign=top align=right class=form>$hours</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>100\%</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $pfAmt ) . "</td></tr>\n";
    
        $pg_body  .= "<tr bgcolor=\"$color_bg1\">";
        $pg_body  .= "<td valign=top align=left class=label><b>PROOFREAD SUB-TOTAL</b></td>\n";
        $pg_body  .= "<td valign=top align=right class=form>&nbsp;</td>\n";
        $pg_body  .= "<td valign=top align=right class=form>" . $shared->fmtNumber( $pfAmt ) . "</td></tr>\n";
    
        $pg_body .= "</table>\n";
        $pg_body .= "</td></tr>\n";
        $pg_body .= "</table><br>\n";
    }        

   	        
    $env->{html_title} = "$site_title - PO #$poNo";
    $env->{page_head}  = "PO #$poNo";
    $env->{page_body}  = $pg_body;
    $env->{template}   = "admin.html";

    $shared->page_gen( $env, $java_msg );
    
}    



sub cleanNumber {
    
    my ( $result ) = @_;
    
    my $result =~ s/\,//;

    return $result;
    
}

###########################################################                   
# Get Names from translator table as selection list
#----------------------------------------------------------
sub getNames {
    
    my ( $filt ) = @_;
    
    my $SQL = "SELECT * FROM ts_translators\n";
            
    if ( $filt eq "TR" ) {
        $SQL .= "WHERE rate <> ''\n";
    } elsif ( $filt eq "ED" ) {
        $SQL .= "WHERE editrate <> ''\n";
    } elsif ( $filt eq "PF" ) {
        $SQL .= "WHERE hourly <> ''\n";
    }
    
    $SQL .= "ORDER BY lastname, firstname";
    
    my $result = "";
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $name = $shared->trim( $rec->{lastname} ) . ", " . $shared->trim( $rec->{firstname} );
        my $rate = $rec->{rate};
        if ( $filt eq "ED" ) {
            $rate = $rec->{editrate};
        } elsif ( $filt eq "PF" ) {
            $rate = $rec->{hourly};
        }
        my $tid   = $rec->{id};
        $result .= "<Option value=\"$tid\">$name - $rate</option>";
    }
    
    return $result;
}

###########################################################                   
# Get Rate amount from translator table
#----------------------------------------------------------
sub getRate {

    my ( $tid, $fld ) = @_;
    
    my $rec = $shared->get_rec( "ts_translators", "id", $tid );
    
    my $raw = $rec->{$fld};
    my $result = 0;
    if ( $raw =~ /([\d\.]+)\s\(/ ) {
        $result = $1;
        $result += 0;
    }
    
    return $result;
}

###########################################################                   
# Get currency code from translator table
#----------------------------------------------------------
sub getCurrency {

    my ( $tid, $fld ) = @_;
    
    my $rec = $shared->get_rec( "ts_translators", "id", $tid );
    
    my $raw = $rec->{$fld};
    my $result = "";
    if ( $raw =~ /\(([^\)]+)\)/ ) {
        $result = $1;
    }
    
    return $result;
}
