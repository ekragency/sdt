#!/usr/bin/perl
###########################################################################
# temp_load3.pl                   
# Temporary conversion script
# Created 07/30/2013
###########################################################################
# Copyright 2013, P. Eric Huber
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;

my $config   = new config;
my $shared   = new shared;

&do_fix_ling_lang;
&do_fix_po_lang;

exit;

######################################################
# Main load/update logic
######################################################
sub do_fix_ling_lang {

    my $SQL = "SELECT * FROM linguist_lang";

    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $id       = $rec->{lil_id};
        my $srcCd    = $rec->{lil_src_lng_code};
        my $tgtCd    = $rec->{lil_tgt_lng_code};
        my $srcID    = &getLngID( $srcCd );
        my $tgtID    = &getLngID( $tgtCd );
        
        my $update   = "UPDATE linguist_lang SET lil_src_lng_id = $srcID, lil_tgt_lng_id = $tgtID WHERE lil_id = $id";
        $shared->doSQLUpdate( $update );
    }
}

######################################################
# Main load/update logic
######################################################
sub do_fix_po_lang {

    my $SQL = "SELECT * FROM po_log";

    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $id       = $rec->{pol_id};
        my $srcCd    = $rec->{pol_src_lng_code};
        my $tgtCd    = $rec->{pol_tgt_lng_code};
        my $srcID    = &getLngID( $srcCd );
        my $tgtID    = &getLngID( $tgtCd );
        
        my $update   = "UPDATE po_log SET pol_src_lng_id = $srcID, pol_tgt_lng_id = $tgtID WHERE pol_id = $id";
        $shared->doSQLUpdate( $update );
    }
}

###########################################################                   
# Get Rate amount from translator table
#----------------------------------------------------------
sub getLngID {

    my ( $code ) = @_;
    
    my $result = 0;
    if ( $code gt "" ) {
        my $rec = $shared->get_rec( "language", "lng_code", $code );
        $result = $rec->{lng_id};
    }
    
    if ( $result eq "" ) {
        $result = 0;
    }
    
    return $result;
}
