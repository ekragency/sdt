#!/usr/bin/perl
##################################################
# deploy_sdt - Version 1.0 - Deploy test code
#                            to production.
##################################################
# Copyright (c) 2013     - Moonlight Design, LLC
##################################################

use strict;
use Date::Calc qw( :all );

# Configuration Settings
#-----------------------
my $debug    = 0;
my $src_path = "/home/ehuber/HOME/sdt";
my $prod_cfg = "/mnt/d/projects/sdt/files/prod/config.pm";
my $tgt_path = "/mnt/d/projects/sdt";
my $src_lib  = "use lib \\( \"/mnt/d/projects/sdttest/files/lib";
my $tgt_lib  = "use lib \( \"/mnt/d/projects/sdt/files/lib";

&up_bin;
&up_cgi;
&up_lib;
&up_src;
&up_img;
&up_css;
&up_css_img;
&up_js;
&up_prod;


exit;

sub up_bin {
    
    my $result = 1;
    
    print "Deploying bin...\n";    
    opendir( DIR, "$src_path/bin" );
    my @DIR = readdir( DIR );
    closedir( DIR );
    foreach my $file ( @DIR ) {
        if ( $file =~ /^\./ || $file eq "CVS" ) {
            if ( $debug ) { print "skipping $file\n"; }
        } else {
            open ( INPUT, "$src_path/bin/$file" ) or $result = 0;
            if ( $result ) {
    	        open ( OUTPUT, ">$tgt_path/files/bin/$file" ) or $result = 0;
    	        if ( $result ) {
                    while ( <INPUT> ) {
                        s/$src_lib/$tgt_lib/g;
                        print OUTPUT $_;
                    }
                }
                close OUTPUT;
            }
            close INPUT;
            
            system( "/bin/chmod 755 $tgt_path/files/bin/$file" );
        }
    }
}

sub up_cgi {
    
    my $result = 1;
    
    print "Deploying cgi-bin...\n";    
    opendir( DIR, "$src_path/cgi-bin" );
    my @DIR = readdir( DIR );
    closedir( DIR );
    foreach my $file ( @DIR ) {
        if ( $file =~ /^\./ || $file eq "CVS" ) {
            if ( $debug ) { print "skipping $file\n"; }
        } else {
            open ( INPUT, "$src_path/cgi-bin/$file" ) or $result = 0;
            if ( $result ) {
    	        open ( OUTPUT, ">$tgt_path/cgi-bin/$file" ) or $result = 0;
    	        if ( $result ) {
                    while ( <INPUT> ) {
                        s/$src_lib/$tgt_lib/g;
                        print OUTPUT $_;
                    }
                }
                close OUTPUT;
            }
            close INPUT;
            
            system( "/bin/chmod 755 $tgt_path/cgi-bin/$file" );
        }
    }
}

sub up_lib {
    
    my $result = 1;
    
    print "Deploying lib...\n";    
    opendir( DIR, "$src_path/lib" );
    my @DIR = readdir( DIR );
    closedir( DIR );
    foreach my $file ( @DIR ) {
        if ( $file =~ /^\./ || $file eq "CVS" || $file eq "config.pm" ) {
            if ( $debug ) { print "skipping $file\n"; }
        } else {
            open ( INPUT, "$src_path/lib/$file" ) or $result = 0;
            if ( $result ) {
    	        open ( OUTPUT, ">$tgt_path/files/lib/$file" ) or $result = 0;
    	        if ( $result ) {
                    while ( <INPUT> ) {
                        s/$src_lib/$tgt_lib/g;
                        print OUTPUT $_;
                    }
                }
                close OUTPUT;
            }
            close INPUT;
            
            system( "/bin/chmod 644 $tgt_path/files/lib/$file" );
        }
    }
}

sub up_src {
    
    my $result = 1;
    
    print "Deploying src...\n";    
    opendir( DIR, "$src_path/src" );
    my @DIR = readdir( DIR );
    closedir( DIR );
    foreach my $file ( @DIR ) {
        if ( $file =~ /^\./ || $file eq "CVS" ) {
            if ( $debug ) { print "skipping $file\n"; }
        } else {
            my $cmd = "/bin/cp $src_path/src/$file $tgt_path/files/src/$file";
            system( $cmd );
            system( "/bin/chmod 644 $tgt_path/files/src/$file" );
        }
    }
}

sub up_img {
    
    my $result = 1;
    
    print "Deploying image...\n";    
    opendir( DIR, "$src_path/images" );
    my @DIR = readdir( DIR );
    closedir( DIR );
    foreach my $file ( @DIR ) {
        if ( $file =~ /^\./ || $file eq "CVS" ) {
            if ( $debug ) { print "skipping $file\n"; }
        } else {
            my $cmd = "/bin/cp $src_path/images/$file $tgt_path/htdocs/images/$file";
            system( $cmd );
            system( "/bin/chmod 644 $tgt_path/htdocs/images/$file" );
        }
    }
}	

sub up_css {
    
    my $result = 1;
    
    print "Deploying css...\n";    
    opendir( DIR, "$src_path/css" );
    my @DIR = readdir( DIR );
    closedir( DIR );
    foreach my $file ( @DIR ) {
        if ( $file =~ /^\./ || $file eq "CVS" || $file eq "img" ) {
            if ( $debug ) { print "skipping $file\n"; }
        } else {
            my $cmd = "/bin/cp $src_path/css/$file $tgt_path/htdocs/css/$file";
            system( $cmd );
            system( "/bin/chmod 644 $tgt_path/htdocs/css/$file" );
        }
    }
}	

sub up_css_img {
    
    my $result = 1;
    
    print "Deploying css/img...\n";    
    opendir( DIR, "$src_path/css/img" );
    my @DIR = readdir( DIR );
    closedir( DIR );
    foreach my $file ( @DIR ) {
        if ( $file =~ /^\./ || $file eq "CVS" ) {
            if ( $debug ) { print "skipping $file\n"; }
        } else {
            my $cmd = "/bin/cp $src_path/css/img/$file $tgt_path/htdocs/css/img/$file";
            system( $cmd );
            system( "/bin/chmod 644 $tgt_path/htdocs/css/img/$file" );
        }
    }
}	

sub up_js {
    
    my $result = 1;
    
    print "Deploying javascript...\n";    
    opendir( DIR, "$src_path/javascript" );
    my @DIR = readdir( DIR );
    closedir( DIR );
    foreach my $file ( @DIR ) {
        if ( $file =~ /^\./ || $file eq "CVS" ) {
            if ( $debug ) { print "skipping $file\n"; }
        } else {
            my $cmd = "/bin/cp $src_path/javascript/$file $tgt_path/htdocs/javascript/$file";
            system( $cmd );
            system( "/bin/chmod 644 $tgt_path/htdocs/javascript/$file" );
        }
    }
}

sub up_prod {
    
    my $result = 1;
    
    print "Updating production config...\n";    
	system( "/bin/cp $prod_cfg $tgt_path/files/lib" );
	
}
