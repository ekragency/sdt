#!/usr/bin/perl
###########################################################################
# temp_load2.pl                   
# Temporary conversion script
# Created 07/30/2013
###########################################################################
# Copyright 2013, P. Eric Huber
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;

my $config   = new config;
my $shared   = new shared;

my $startC   = $ARGV[0];

&do_load;

exit;

######################################################
# Main load/update logic
######################################################
sub do_load {

    my $SQL = "SELECT * FROM ts_translators";

    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $id       = $rec->{id};
        my $lName    = $rec->{lastname};
        my $fName    = $rec->{firstname};
        my $eml      = $rec->{email};
        my $phn      = $rec->{phone};
        my $fax      = $rec->{fax};
        my $addr     = $rec->{address};
        my $city     = $rec->{city};
        my $state    = $rec->{state};
        my $cntry    = $rec->{country};
        my $src      = $rec->{source};
        my $tgt      = $rec->{target};
        my $tRate    = &getRate( $rec->{rate} );
        my $eRate    = &getRate( $rec->{editrate} );
        my $hourly   = &getRate( $rec->{hourly} );
        my $comment  = $rec->{comments};
        my $nda      = $rec->{nda};
        my $w9       = $rec->{w9};
        my $cur      = &getCurrency( $rec->{rate} );
        if ( $cur eq "" ) {
            $cur     = &getCurrency( $rec->{editrate} );
            if ( $cur eq "" ) {
                $cur = &getCurrency( $rec->{hourly} );
                if ( $cur eq "" ) {
                    $cur = "USD";
                }
            }
        }

        $lName     =~ s/'/\\'/g;
        $fName     =~ s/'/\\'/g;
        $addr      =~ s/'/\\'/g;
        $city      =~ s/'/\\'/g;
        $cntry     =~ s/'/\\'/g;
        $comment   =~ s/'/\\'/g;
        
        if ( $nda eq "on" ) {
            $nda = "Y";
        } else {
            $nda = "N";
        }
        
        if ( $w9 eq "on" ) {
            $w9 = "Y";
        } else {
            $w9 = "N";
        }
        
        my $insert   = "INSERT INTO linguist ( lig_last_name, lig_first_name, lig_phone, lig_email,\n"
                     . "lig_fax, lig_address, lig_city, lig_state, lig_country, lig_comment,\n"
                     . "lig_nda, lig_w9, lig_currency ) VALUES\n"
                     . "( '$lName', '$fName', '$phn', '$eml', '$fax', '$addr', '$city', '$state', '$cntry', '$comment',\n"
                     . "'$nda', '$w9', '$cur' )";
        $shared->doSQLUpdate( $insert );
        
        my $lid      = $shared->getSQLLastInsertID;
        
        my $ins2     = "INSERT INTO linguist_lang ( lil_lig_id, lil_src_lng_code, lil_tgt_lng_code,\n"
                     . "lil_trans_rate, lil_edit_rate, lil_hourly_rate ) VALUES\n"
                     . "( $lid, '$src', '$tgt', $tRate, $eRate, $hourly )";
        
        $shared->doSQLUpdate( $ins2 );
    }    
}


###########################################################                   
# Get Rate amount from translator table
#----------------------------------------------------------
sub getRate {

    my ( $fld ) = @_;
    
    my $result = 0;
    if ( $fld =~ /([\d\.]+)\s\(/ ) {
        $result = $1;
        $result += 0;
    }
    
    return $result;
}

###########################################################                   
# Get currency code from translator table
#----------------------------------------------------------
sub getCurrency {

    my ( $fld ) = @_;
    
    my $result = "";
    if ( $fld =~ /\(([^\)]+)\)/ ) {
        $result = $1;
    }
    
    return $result;
}
