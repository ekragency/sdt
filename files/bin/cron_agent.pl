#!/usr/bin/perl
###########################################################################
# cron_agent.pl                   
# Agent Script to run nightly processes
# Created 08/14/2013
###########################################################################
# Copyright 2013, P. Eric Huber
###########################################################################
use strict;
use Net::SMTP;
use MIME::Lite;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;

my $config   = new config;
my $shared   = new shared;

my $pdf_path = $config->get_pdf_path;
my $debug    = 0;

&do_clean_pdfs;
&do_quote_init_nudge;
&do_quote_delay_nudge;

exit;

######################################################
# Clean up PDF files
######################################################
sub do_clean_pdfs {

    my @DIR;
    opendir( DIR, "$pdf_path" ) || die ( "Unable to open directory $pdf_path" );
    @DIR = readdir( DIR );
    closedir( DIR );
    foreach my $pdffile ( @DIR ) {
        if ( $pdffile =~ /^\./ ) {
            if ( $debug ) { print "skipping $pdffile\n"; }
        } else {
            my $cmd = "/bin/rm -f $pdf_path/$pdffile";
            if ( $debug ) { print "Removing $pdffile...\n"; }
            system( "$cmd" );
        }
    }
}

######################################################
# Do Quote Initial Nudges
######################################################
sub do_quote_init_nudge {
    
    my $nStart  = $shared->getConfigDate( "ADM_QTNUDGEASOF" );
    my $nDays   = $shared->getConfigInt( "ADM_QTNUDGE" );
    
    my $wStart  = "AND qte_add_date >= '$nStart'\n";
    if ( $nStart eq "0000-00-00" ) {
        $wStart = "";
    }
    
    my $SQL = "SELECT * FROM quote\n"
            . "WHERE qte_status = 'PENDING'\n"
            . "AND qte_sent_init_nudge = 'N'\n"
            . $wStart
            . "AND ( DATEDIFF( CURDATE(), qte_add_date ) >= $nDays )\n"
            . "ORDER BY qte_add_usr_id, qte_prj_no";
            
    my $qLine    = "";
    my $holdUser = 0;
    
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $qID   = $rec->{qte_id};
        my $q_uid = $rec->{qte_add_usr_id};
        my $q_pno = $rec->{qte_prj_no};
        my $q_cid = $rec->{qte_cln_id};
        my $q_amt = $rec->{qte_tot_amount};
    
        if ( $q_uid != $holdUser ) {
            if ( $holdUser > 0 ) {
                &sendMsg( $holdUser, $qLine, "INIT" );
            }
            $qLine    = "";
            $holdUser = $q_uid;
        }
        
        my $cName  = $shared->getFieldVal( "client", "cln_name", "cln_id", $q_cid );
        my $fmtAmt = $shared->fmtCurrency( $q_amt );
        
        $qLine    .= "Quote #" . $q_pno . " - " . $cName . " - " . $fmtAmt . "\n";
        
        my $update = "UPDATE quote SET qte_sent_init_nudge = 'Y' WHERE qte_id = $qID";
        $shared->doSQLUpdate( $update );
        
    }
    
    if ( $holdUser > 0 ) { 
        &sendMsg( $holdUser, $qLine, "INIT" );
    }      
    
}


######################################################
# Do Quote Initial Nudges
######################################################
sub do_quote_delay_nudge {
    
    my $SQL = "SELECT * FROM quote\n"
            . "WHERE qte_status = 'DELAYED'\n"
            . "AND ( qte_nudge_date <> '0000-00-00' AND qte_nudge_date <= CURDATE() )\n"
            . "ORDER BY qte_add_usr_id, qte_prj_no";
            
    my $qLine    = "";
    my $holdUser = 0;
    
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $qID   = $rec->{qte_id};
        my $q_uid = $rec->{qte_add_usr_id};
        my $q_pno = $rec->{qte_prj_no};
        my $q_cid = $rec->{qte_cln_id};
        my $q_amt = $rec->{qte_tot_amount};
        
        if ( $q_uid != $holdUser ) {
            if ( $holdUser > 0 ) {
                &sendMsg( $holdUser, $qLine, "DELAYED" );
            }
            $qLine    = "";
            $holdUser = $q_uid;
        }
        
        my $cName  = $shared->getFieldVal( "client", "cln_name", "cln_id", $q_cid );
        my $fmtAmt = $shared->fmtCurrency( $q_amt );
        
        $qLine    .= "Quote #" . $q_pno . " - " . $cName . " - " . $fmtAmt . "\n";
        
        my $update = "UPDATE quote SET qte_nudge_date = '0000-00-00' WHERE qte_id = $qID";
        $shared->doSQLUpdate( $update );
        
    }
    
    if ( $holdUser > 0 ) { 
        &sendMsg( $holdUser, $qLine, "DELAYED" );
    }      
    
}

###########################################################                   
# Send Initial Nudge Message
#----------------------------------------------------------
sub sendMsg {

    my ( $uID, $qLine, $type ) = @_;
    
    my $config       = new config;
	
	my $help_email   = $config->get_help_email;
	my $mailhost     = $config->get_mailhost;
	my $mailfrom     = "SDT Admin";
	my $from         = "$mailfrom <$help_email>";
	
	my $usr_rec      = $shared->get_rec( "user", "usr_id", $uID );
   
    my $email        = $usr_rec->{usr_email};
    my $subj         = "SDT Quote(s) Followup"; 	
    my $rMsg         = $shared->getConfigText( "ADM_QTNUDGEINIT" );
    if ( $type eq "DELAYED" ) {
        $rMsg        = $shared->getConfigText( "ADM_QTNUDGEDELAY" );
    }
    $rMsg           .= "\n\n" . $qLine . "\n\n";
    
	if ( $email gt "" ) {

        my $smtp = Net::SMTP->new( $mailhost );
        $smtp->mail( $from );
        $smtp->to( $email );
        $smtp->data();
        $smtp->datasend( "To: $email\n" );
        $smtp->datasend( "From: $from\n" );
        $smtp->datasend( "Subject: $subj\n" );
        $smtp->datasend( "\n" );
        $smtp->datasend( $rMsg );
        $smtp->dataend();
        $smtp->quit;
	
    }
}
