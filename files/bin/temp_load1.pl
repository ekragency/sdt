#!/usr/bin/perl
###########################################################################
# temp_load1.pl                   
# Temporary loading script
# Created 07/27/2013
###########################################################################
# Copyright 2013, P. Eric Huber
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;

my $config   = new config;
my $shared   = new shared;

my $startC   = $ARGV[0];

&do_load;

exit;

######################################################
# Main load/update logic
######################################################
sub do_load {

    my $SQL = "SELECT * FROM client";

    $startC += 0;    
    my $code = 101;
    if ( $startC > 0 ) {
        $code = $startC;
    }
    
    my @rs = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $clnID = $rec->{cln_id};
        my $clnCD = $rec->{cln_code};
        my $clnNM = $rec->{cln_name};
        
        if ( $clnCD eq "" ) {
            my $update1 = "UPDATE client SET cln_code = '$code' WHERE cln_id = $clnID";
            $shared->doSQLUpdate( $update1 );
            $code++;
        }
        
        my $SQL2 = "SELECT * FROM client_contact WHERE comp_name = '$clnNM'";
        my @rs2  = $shared->getResultSet( $SQL2 );
        foreach my $cRec ( @rs2 ) {
            my $clcID   = $cRec->{clc_id};
            my $update2 = "UPDATE client_contact SET clc_cln_id = $clnID WHERE clc_id = $clcID";
            $shared->doSQLUpdate( $update2 );
        }
    }    
}
