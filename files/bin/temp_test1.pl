#!/usr/bin/perl
###########################################################################
# temp_test1.pl                   
# Temporary Script to test functionality
# Created 07/30/2013
###########################################################################
# Copyright 2013, P. Eric Huber
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;

my $config   = new config;
my $shared   = new shared;

&do_test;

exit;

######################################################
# Main load/update logic
######################################################
sub do_test {

    my $text  = $shared->getConfigText( "ADM_POINSTEXT" );
    
    print "ORIGINAL VALUE\n";
    print "--------------\n";
    print $text;
    print "\n\n";
    
    my $delim = "</:/>";
    $text    =~ s/\r\n|\r|\n/$delim/g;
    print "AFTER REGEX\n";
    print "-----------\n";
    print $text;
    
    print "\n\n";
    
    my @lines = split( $delim, $text );
    foreach my $line ( @lines ) {
        print "[$line]\n";
    }
    
} 
