#!/usr/bin/perl
###########################################################################
# temp_test2.pl                   
# Temporary Script to test clone_rec functionality
# Created 07/30/2013
###########################################################################
# Copyright 2013, P. Eric Huber
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;

my $config   = new config;
my $shared   = new shared;

&do_test;

exit;

######################################################
# Main load/update logic
######################################################
sub do_test {

    print "TESTING clone_rec\n";
    print "-----------------\n";
    print "Source ID: 656\n";
    print "\n\n";
    
    my $new_id = $shared->clone_rec( "po_log", "pol_id", 656 );
    
    print "New ID: $new_id\n";
    print "\n\n";
        
} 
