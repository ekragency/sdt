#!/usr/bin/perl
###########################################################################
# fix_quote_line.pl                   
# Fix quote line table and populate new field (qtl_doc_name) with document
# name embedded in qtl_desc field.
# Created 07/30/2013
###########################################################################
# Copyright 2013, P. Eric Huber
###########################################################################
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;

my $config   = new config;
my $shared   = new shared;

&do_fix;

exit;

######################################################
# Main load/update logic
######################################################
sub do_fix {

    my $SQL = "SELECT qtl_id, qtl_desc, qtl_type FROM quote_line\n"
            . "WHERE qtl_type IN ( 'A-TR', 'B-ED', 'C-PF', 'D-MTPE' )";
    my @rs  = $shared->getResultSet( $SQL );
   
    my $upCnt = 0;
     
    foreach my $rec ( @rs ) {
        
        my $qtlID  = $rec->{qtl_id};
        my $desc   = $rec->{qtl_desc};
        my $type   = $rec->{qtl_type};
        
        my $docNm  = "";
        if ( $type eq "A-TR" ) {
            if ( $desc =~ /^Translation of (.+) from .+/ ) {
                $docNm = $1;
            }
        } elsif ( $type eq "B-ED" ) {
            if ( $desc =~ /^Editing of (.+) from .+/ ) {
                $docNm = $1;
            }
        } elsif ( $type eq "C-PF" ) {
            if ( $desc =~ /^Proof reading of (.+) from .+/ ) {
                $docNm = $1;
            }
        } elsif ( $type eq "D-MTPE" ) {
            if ( $desc =~ /^MTPE for (.+) from .+/ ) {
                $docNm = $1;
            }
        }
        
        if ( $docNm gt "" ) {
            
            $docNm =~ s/'/\\'/g;
            
            my $update = "UPDATE quote_line SET qtl_doc_name = '$docNm' WHERE qtl_id = $qtlID";
            $shared->doSQLUpdate( $update );
            
            $upCnt++;
            
        }
    }
    
    print "$upCnt records updated.\n\n";
    
} 
