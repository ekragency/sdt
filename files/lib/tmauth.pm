#!/usr/bin/perl
########################################
# This package provides shared database
# and CGI routines and other functions.
#
# Created: 06/30/2015 - Eric Huber
###########################################################################
# Copyright 2004, P. Eric Huber
###########################################################################
use config;
use shared;

my $shared;
my $src_path;
my $color_head1;
my $color_head2;
my $color_bg1;
my $color_bg2;
my $color_bg3;
my $img_path;
my $script_path;
my $site_title;
my $grace_logins;
my $lock_time;
my $tmu_id = 0;
my $app_mode;
my $java_msg = "";
my $page_dtls;

my $debug = 0;

package tmauth;

# -------------------------------------------------------------------------------------
# The constructor   
# -------------------------------------------------------------------------------------
sub new {
    
    my ( $class, $sh_in ) = @_;
    
    my $self = {};
    
    my $config    = new config;
    $src_path     = $config->get_src_path;
    my $app_ver   = $config->get_app_ver;
    $img_path     = $config->get_img_path;
    $script_path  = $config->get_script_path;
    $site_title   = $config->get_tm_name;
    $debug        = $config->get_debug;
    $app_mode     = $config->get_app_mode;
    $page_dtls->{template}    = "tm.html";
    $page_dtls->{app_ver}     = $app_ver;
    $page_dtls->{css}         = "tm-style.css";
    if ( $app_mode eq "TEST" ) {
        $page_dtls->{template} = "tm-test.html";
    }
    
    bless ( $self, $class );

    $shared  = $sh_in;
    
    $tmu_id = $self->authenticate;
    
    return $self;
}
# -------------------------------------------------------------------------------------

sub authenticate {     # Purpose: return either 0 or 1 given password

    my ($self)     = @_;
    my $ret_val    = 0;
    my $cgi_id     = $shared->get_cgi( "tmu_login" );
    my $cgi_pw     = $shared->get_cgi( "tmu_passwd" );
    my $action     = $shared->get_cgi( "action" );
    my $config     = new config;
    my $usrid_cky  = $config->get_tmid_cookie;
    my $usrpw_cky  = $config->get_tmpw_cookie;
    my $loginnow   = 0;
    my $foundLogin = 0;
    
    if ( $action eq "logout" ) {
    	
    	$shared->store_cookie( $usrpw_cky, "delete\|-1d" );
    	
    } else {
    
        if ( $cgi_pw eq "" ) {
        	$cgi_id = $shared->get_cookie( $usrid_cky );
    	    $cgi_pw = $shared->get_cookie( $usrpw_cky ); 
        } else {
            $shared->store_cookie( $usrid_cky, $cgi_id );
    	    $shared->store_cookie( $usrpw_cky, $cgi_pw );
    	    $loginnow = 1;
        }
        
        my $SQL = "SELECT tmu_id, tmu_passwd\n"
                . "FROM tm_user\n"
                . "WHERE tmu_login = '$cgi_id'\n";
        my @rs  = $shared->getResultSet( $SQL );
        foreach my $usr_rec ( @rs ) {
        	$foundLogin = 1;
        	my $db_id = $usr_rec->{tmu_id};
        	my $db_pw = $usr_rec->{tmu_passwd};

            if ( ( $db_pw eq $cgi_pw ) && ( $cgi_pw gt "" ) ) {
                $ret_val = $db_id;
                if ( $loginnow ) {
                  	$shared->add_log( "TMACCESS", "Successful Term Mgr Login for user - ($cgi_id).", 0 );
                }
            } else {
                if ( $loginnow ) {
                    $java_msg = "Login Failed!  Invalid Password Specified.";
                    $shared->add_log( "TMDNYPASS", "Invalid Term Mgr Password Specified - ($cgi_id).", 0 );
                }
            }
            last;
        }
        
        if ( ( $foundLogin == 0 ) && ( $loginnow ) ) {
        	$java_msg = "Login Failed!  Invalid User Specified.";
        	$shared->add_log( "TMDNYUSER", "Invalid Term Mgr User Specified - ($cgi_id).", 0 );
        }
        
    }
        
    return $ret_val;
}


###########################################################                   
# Return current usr_id
#----------------------------------------------------------
sub getCurrentUser {
	my ( $self ) = @_;
	return $tmu_id;
}

###########################################################                   
# Return name of current user
#----------------------------------------------------------
sub getCurrentUserName {
	my ( $self ) = @_;
	my $usr_name = "";
	if ( $tmu_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "tm_user", "tmu_id", $tmu_id );
	    $usr_name   = $usr_rec->{tmu_name};
	}
	return $usr_name;
}

###########################################################                   
# Return name of current user
#----------------------------------------------------------
sub getCurrentUserLogin {
	my ( $self ) = @_;
	my $usr_login = "";
	if ( $tmu_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "tm_user", "tmu_id", $tmu_id );
	    $usr_login   = $usr_rec->{tmu_login};
	}
	return $usr_login;
}

###########################################################                   
# Return name of current user
#----------------------------------------------------------
sub getCurrentUserPass {
	my ( $self ) = @_;
	my $usr_pass = "";
	if ( $tmu_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "tm_user", "tmu_id", $tmu_id );
	    $usr_pass   = $usr_rec->{tmu_passwd};
	}
	return $usr_pass;
}

###########################################################                   
# Return client ID of current user (if type = client)
#----------------------------------------------------------
sub getCurrentUserCLNID {
	my ( $self ) = @_;
	my $cln_id = 0;
	if ( $tmu_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "tm_user", "tmu_id", $tmu_id );
	    $cln_id  = $usr_rec->{tmu_cln_id};
	}
	return $cln_id;
}

###########################################################                   
# Return type of current user (if type = client)
#----------------------------------------------------------
sub getCurrentUserType {
	my ( $self ) = @_;
	my $result = "";
	if ( $tmu_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "tm_user", "tmu_id", $tmu_id );
	    $result  = $usr_rec->{tmu_type};
	}
	return $result;
}

###########################################################                   
# User Login
#----------------------------------------------------------
sub login {

    my ( $self, $usr_url, $noMsg ) = @_;
    
   	my $pg_content = "";
    $pg_content   .= "<form name=logonform method=post action=\"$usr_url\">\n";
    $pg_content   .= "<input type=hidden name=action value=\"login\">\n";
    $pg_content   .= "<table border=0 cellpadding=5 cellspacing=0 width=200>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>User ID:</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input name=tmu_login value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>Password:</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input type=password name=tmu_passwd value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td>&nbsp;</td><td valign=top align=left class=form><input type=submit value=\" Login Now \" class=form></td></tr>\n";
    $pg_content   .= "</table>\n";
    $pg_content   .= "</form>\n";
    $pg_content   .= "<script language=\"JavaScript\">\n";
    $pg_content   .= "<!--\n";
    $pg_content   .= "document.logonform.tmu_login.focus();\n";
    $pg_content   .= "//-->\n";
    $pg_content   .= "</script>\n";
    
        
    $page_dtls->{page_head}    = "System Access";
    $page_dtls->{html_title}   = "$site_title - Access";
    $page_dtls->{nav_buttons}  = "&nbsp;";
    $page_dtls->{page_body}    = $pg_content;
    
    if ( $noMsg eq "Y" ) {
    	$java_msg = "";
    }
        
    $shared->page_gen( $page_dtls, $java_msg );
    
}
    	


###########################################################                   
# Logout Page
#----------------------------------------------------------
sub logout {

    my ( $self, $usr_url ) = @_;

    my $pg_content;    
    $pg_content  = "<center>\n";
    $pg_content .= "<form method=post action=\"$usr_url\">\n";
    $pg_content .= "<table border=0 cellpadding=5 cellspacing=0 width=\"90%\">\n";
    $pg_content .= "<tr><td valign=top align=left class=form>\n";
    $pg_content .= "You are now logged out of $site_title</td></tr>\n";
    $pg_content .= "<tr><td valign=top align=center class=form>\n";
    $pg_content .= "<input type=submit value=\" Login Again \" class=form></td></tr>\n";
    $pg_content .= "</table>\n";
    $pg_content .= "</form>\n";
    $pg_content .= "</center>\n";
    
    $page_dtls->{page_head}    = "System Access";
    $page_dtls->{html_title}   = "$site_title - Access";
    $page_dtls->{page_body}    = $pg_content;
        
    $shared->page_gen( $page_dtls );
    
}


###########################################################                   
# Change Password Form
#----------------------------------------------------------
sub chg_pass_form {

    my ( $self, $usr_url ) = @_;
    
   	my $pg_content = "";
    $pg_content   .= "<form method=post action=\"$usr_url\">\n";
    $pg_content   .= "<input type=hidden name=action value=\"chg_pass\">\n";
    $pg_content   .= "<table border=0 cellpadding=5 cellspacing=0 width=300>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>New Password:</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input type=password name=new_pass value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>Confirm (Retype):</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input type=password name=new_conf value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td>&nbsp;</td><td valign=top align=left class=form><input type=submit value=\" Change Now \" class=form>&nbsp;\n";
    $pg_content   .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$usr_url'\" class=form></td></tr>\n";
    $pg_content   .= "</table>\n";
    $pg_content   .= "</form>\n";
        
    $page_dtls->{page_head}    = "Change Password";
    $page_dtls->{html_title}   = "$site_title - Change Password";
    $page_dtls->{page_body}    = $pg_content;
        
    $shared->page_gen( $page_dtls, $java_msg );
    
}

###########################################################                   
# Change Password Form
#----------------------------------------------------------
sub chg_pass {

    my ( $self, $usr_url ) = @_;
    
    my $result = "";
    
    my $new_pass   = $shared->get_cgi( "new_pass" );
    my $new_conf   = $shared->get_cgi( "new_conf" );
    my $config     = new config;
    my $usrpw_cky  = $config->get_tmpw_cookie;
    
    if ( $new_pass gt "" ) {
    	if ( $new_pass eq $new_conf ) {
    		
    		my $update = "UPDATE tm_user SET tmu_passwd = '$new_pass' WHERE tmu_id = $tmu_id";
    		$shared->doSQLUpdate( $update );
    		
    	    $shared->store_cookie( $usrpw_cky, $new_pass );
    	    
    	    $result = "Password Changed";
    		
        } else {
        	$result = "New password and confirmation do not match.  Please retype your new password exactly the same way in the confirmation field.";
        	$self->chg_pass_form( $usr_url );
        }
    } else {
    	$result = "Password cannot be blank.";
    	$self->chg_pass_form( $usr_url );
    }
    
    return $result;
}

###########################################################                   
# Get Login Status Box
#----------------------------------------------------------
sub getLoginStatus {
	
	my ( $self ) = @_;
	
	my $result = "";
	my $uname  = $self->getCurrentUserName;
	if ( $uname gt "" ) {
	    $result = "Logged in as: <b>$uname</b>";
	}
	return $result;
}


1;
