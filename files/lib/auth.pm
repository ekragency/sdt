#!/usr/bin/perl
########################################
# This package provides shared database
# and CGI routines and other functions.
#
# Created: 10/13/2004 - Eric Huber
###########################################################################
# Copyright 2004, P. Eric Huber
###########################################################################
use config;
use shared;

my $shared;
my $src_path;
my $color_head1;
my $color_head2;
my $color_bg1;
my $color_bg2;
my $color_bg3;
my $img_path;
my $script_path;
my $site_title;
my $grace_logins;
my $lock_time;
my $usr_id = 0;
my $java_msg = "";
my $page_dtls;

my $debug = 0;

package auth;

# -------------------------------------------------------------------------------------
# The constructor   
# -------------------------------------------------------------------------------------
sub new {
    
    my ( $class, $sh_in ) = @_;
    
    my $self = {};
    
    my $config    = new config;
    $src_path     = $config->get_src_path;
    my $app_ver   = $config->get_app_ver;
    my (%colors)  = $config->get_colors;
    $color_head1  = $colors{ "HEAD1" };
    $color_head2  = $colors{ "HEAD2" };
    $color_bg1    = $colors{ "BG1" };
    $color_bg2    = $colors{ "BG2" };
    $color_bg3    = $colors{ "BG3" };
    $img_path     = $config->get_img_path;
    $script_path  = $config->get_script_path;
    $site_title   = $config->get_app_name;
    $grace_logins = $config->get_grace_logins;
    $lock_time    = $config->get_lock_time;
    $debug        = $config->get_debug;
    $page_dtls->{template}    = "admin.html";
    $page_dtls->{color_head1} = $color_head1;
    $page_dtls->{color_head2} = $color_head2;
    $page_dtls->{color_bg1}   = $color_bg1;
    $page_dtls->{color_bg2}   = $color_bg2;
    $page_dtls->{color_bg3}   = $color_bg3;
    $page_dtls->{color_error} = $color_error;
    $page_dtls->{app_ver}     = $app_ver;
    $page_dtls->{css}         = "style.css";

    
    bless ( $self, $class );

    $shared  = $sh_in;
    
    $usr_id = $self->authenticate;
    
    return $self;
}
# -------------------------------------------------------------------------------------

sub authenticate {     # Purpose: return either 0 or 1 given password

    my ($self)     = @_;
    my $ret_val    = 0;
    my $cgi_id     = $shared->get_cgi( "usr_login" );
    my $cgi_pw     = $shared->get_cgi( "usr_password" );
    my $action     = $shared->get_cgi( "action" );
    my $config     = new config;
    my $usrid_cky  = $config->get_usrid_cookie;
    my $usrpw_cky  = $config->get_usrpw_cookie;
    my $now        = time;
    my $lock_stat  = 0;
    my $loginnow   = 0;
    my $foundLogin = 0;
    
    if ( $debug ) {
    	open( DEBUG, ">>$src_path/debug.log" );
    	print DEBUG "###OPENING DEBUG FILE - User Authenticate Subroutine###\n";
    }
    
    if ( $action eq "logout" ) {
    	
    	if ( $debug ) {
    		print DEBUG "action is logout - removing $usrpw_cky cookie value.\n";
        }
    	$shared->store_cookie( $usrpw_cky, "delete\|-1d" );
    	
    } else {
    
        if ( $debug ) {
        	print DEBUG "$usrid_cky value: [$cgi_id]\n";
        	print DEBUG "$usrpw_cky value: [$cgi_pw]\n";
        }
        
        if ( $cgi_pw eq "" ) {
        	$cgi_id = $shared->get_cookie( $usrid_cky );
    	    $cgi_pw = $shared->get_cookie( $usrpw_cky ); 
     	    if ( $debug ) {
     	    	print DEBUG "getting user_id from $usrid_cky cookie: [$cgi_id]\n";
        		print DEBUG "getting user_pw from $usrpw_cky cookie: [$cgi_pw]\n";
            }
        } else {
        	if ( $debug ) {
        		print DEBUG "setting $usrid_cky cookie to [$cgi_id]\n";
        		print DEBUG "setting $usrpw_cky cookie to [$cgi_pw]\n";
            }
            $shared->store_cookie( $usrid_cky, $cgi_id );
    	    $shared->store_cookie( $usrpw_cky, $cgi_pw );
    	    $loginnow = 1;
        }
        
        my $SQL = "SELECT usr_id, usr_password, usr_bad_login_cnt,\n"
                . "usr_reset_time, usr_lock_flag\n"
                . "FROM user\n"
                . "WHERE usr_login = '$cgi_id'\n";
        my @rs  = $shared->getResultSet( $SQL );
        foreach my $usr_rec ( @rs ) {
        	$foundLogin = 1;
        	my $db_id = $usr_rec->{usr_id};
        	my $db_pw = $usr_rec->{usr_password};
        	my $db_lc = $usr_rec->{usr_bad_login_cnt};
        	my $db_rt = $usr_rec->{usr_reset_time};
        	my $db_lk = $usr_rec->{usr_lock_flag};
        	
        	if ( $db_lk eq "Y" ) {
        		if ( $db_rt > $now ) {
        			$lock_stat = 1;
        		}
        	}

            if ( $lock_stat == 0 ) {
                if ( ( $db_pw eq $cgi_pw ) && ( $cgi_pw gt "" ) ) {
                    if ( $db_lc > 0 ) {
                        $self->reset_user_lock( $db_id );
                    }
                    $ret_val = $db_id;
                    if ( $debug ) {
                    	print DEBUG "User $cgi_id succesfully logged in.\n";
                    }
                    if ( $loginnow ) {
                    	$shared->add_log( "LOGINOK", "Successful Login for user.", $db_id );
                    }
                } else {
                    $self->incr_user_lock_cnt( $db_id );
                }
            } else {
            	$shared->add_log( "LOGINLOCK", "Access has been temporarily locked.", $db_id );
                $shared->do_err( "Your login access has been temporarily locked.  This is due to an exceeded number of invalid login attempts." );
            }
            last;
        }
        
        if ( ( $foundLogin == 0 ) && ( $loginnow ) ) {
        	$java_msg = "Login Failed!  Invalid User Specified.";
        	$shared->add_log( "LOGINBAD", "Invalid User Specified - ($cgi_id).", 0 );
        }
        
        if ( $debug ) {
            close( DEBUG );
        }
    }
        
    return $ret_val;
}


###########################################################
# Increment the bad_cnt counter on the user record and
# lock the account if the count exceeds the threshold.
#----------------------------------------------------------
sub incr_user_lock_cnt {
      
    my ( $self, $db_id ) = @_;
    
    my $usr_rec = $shared->get_rec( "user", "usr_id", $db_id );
    my $cur_cnt = $usr_rec->{usr_bad_login_cnt};
    my $usr_lck = $usr_rec->{usr_lock_flag};
    my $new_cnt = $cur_cnt + 1;
    
    if ( ( $new_cnt >= $grace_logins ) && ( $usr_lck ne "Y" ) ) {
    	my $resetT = time + $lock_time;
    	my $update = "UPDATE user SET usr_lock_flag = 'Y',\n"
    	           . "usr_bad_login_cnt = $new_cnt,\n"
    	           . "usr_reset_time = $resetT\n"
    	           . "WHERE usr_id = $db_id";
    	$shared->doSQLUpdate( $update );
    	$java_msg  = "Login Failed - Account is Locked.";
    	$shared->add_log( "LOGINLCK", "Failed Login - LOCK: Account is Locked.", $db_id );

    } else {
    	my $update = "UPDATE user SET usr_bad_login_cnt = $new_cnt\n"
    	           . "WHERE usr_id = $db_id";
    	$shared->doSQLUpdate( $update );
    	$java_msg  = "Login Failed - Warning, only $grace_logins allowed before access is locked out.";
    	$shared->add_log( "LOGINWARN", "Failed Login - WARNING: $new_cnt attempts made.", $db_id );
    }
}

###########################################################
# Reset the user lock (if lock time has expired)
#----------------------------------------------------------
sub reset_user_lock {
            
    my ( $self, $db_id ) = @_;

    my $update = "UPDATE user SET usr_lock_flag = 'N', usr_reset_time = 0,\n"
               . "usr_bad_login_cnt = 0\n"
               . "WHERE usr_id = $db_id";
    $shared->doSQLUpdate( $update );
    
}

###########################################################                   
# Return current usr_id
#----------------------------------------------------------
sub getCurrentUser {
	my ( $self ) = @_;
	return $usr_id;
}

###########################################################                   
# Return name of current user
#----------------------------------------------------------
sub getCurrentUserName {
	my ( $self ) = @_;
	my $usr_name = "";
	if ( $usr_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "user", "usr_id", $usr_id );
	    $usr_name   = $usr_rec->{usr_name};
	}
	return $usr_name;
}

###########################################################                   
# Return name of current user
#----------------------------------------------------------
sub getCurrentUserLogin {
	my ( $self ) = @_;
	my $usr_login = "";
	if ( $usr_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "user", "usr_id", $usr_id );
	    $usr_login   = $usr_rec->{usr_login};
	}
	return $usr_login;
}

###########################################################                   
# Return name of current user
#----------------------------------------------------------
sub getCurrentUserPass {
	my ( $self ) = @_;
	my $usr_pass = "";
	if ( $usr_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "user", "usr_id", $usr_id );
	    $usr_pass   = $usr_rec->{usr_password};
	}
	return $usr_pass;
}


###########################################################                   
# Return chapter access for current user
#----------------------------------------------------------
sub getChapterAccess {
	my ( $self ) = @_;
	my $chapter = "";
	if ( $usr_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "user", "usr_id", $usr_id );
	    $chapter   = $usr_rec->{usr_cal_chapter};
	    if ( $chapter eq "" ) {
	    	$chapter = "ALL";
	    }
	}
	return $chapter;
}


###########################################################                   
# User Login
#----------------------------------------------------------
sub login {

    my ( $self, $usr_url, $noMsg ) = @_;
    
   	my $pg_content = "";
    $pg_content   .= "<form name=logonform method=post action=\"$usr_url\">\n";
    $pg_content   .= "<input type=hidden name=action value=\"login\">\n";
    $pg_content   .= "<table border=0 cellpadding=5 cellspacing=0 width=200>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>User ID:</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input name=usr_login value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>Password:</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input type=password name=usr_password value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td>&nbsp;</td><td valign=top align=left class=form><input type=submit value=\" Login Now \" class=form></td></tr>\n";
    $pg_content   .= "</table>\n";
    $pg_content   .= "</form>\n";
    $pg_content   .= "<script language=\"JavaScript\">\n";
    $pg_content   .= "<!--\n";
    $pg_content   .= "document.logonform.usr_login.focus();\n";
    $pg_content   .= "//-->\n";
    $pg_content   .= "</script>\n";
    
        
    $page_dtls->{page_head}    = "System Access";
    $page_dtls->{html_title}   = "$site_title - Access";
    $page_dtls->{page_body}    = $pg_content;
    
    if ( $noMsg eq "Y" ) {
    	$java_msg = "";
    }
        
    $shared->page_gen( $page_dtls, $java_msg );
    
}
    	


###########################################################                   
# Logout Page
#----------------------------------------------------------
sub logout {

    my ( $self, $usr_url ) = @_;

    my $pg_content;    
    $pg_content  = "<center>\n";
    $pg_content .= "<form method=post action=\"$usr_url\">\n";
    $pg_content .= "<table border=0 cellpadding=5 cellspacing=0 width=\"90%\">\n";
    $pg_content .= "<tr><td valign=top align=left class=form>\n";
    $pg_content .= "You are now logged out of $site_title</td></tr>\n";
    $pg_content .= "<tr><td valign=top align=center class=form>\n";
    $pg_content .= "<input type=submit value=\" Login Again \" class=form></td></tr>\n";
    $pg_content .= "</table>\n";
    $pg_content .= "</form>\n";
    $pg_content .= "</center>\n";
    
    $page_dtls->{page_head}    = "System Access";
    $page_dtls->{html_title}   = "$site_title - Access";
    $page_dtls->{page_body}    = $pg_content;
        
    $shared->page_gen( $page_dtls );
    
}


###########################################################                   
# Change Password Form
#----------------------------------------------------------
sub chg_pass_form {

    my ( $self, $usr_url ) = @_;
    
   	my $pg_content = "";
    $pg_content   .= "<form method=post action=\"$usr_url\">\n";
    $pg_content   .= "<input type=hidden name=action value=\"chg_pass\">\n";
    $pg_content   .= "<table border=0 cellpadding=5 cellspacing=0 width=300>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>New Password:</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input type=password name=new_pass value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>Confirm (Retype):</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input type=password name=new_conf value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td>&nbsp;</td><td valign=top align=left class=form><input type=submit value=\" Change Now \" class=form>&nbsp;\n";
    $pg_content   .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$usr_url'\" class=form></td></tr>\n";
    $pg_content   .= "</table>\n";
    $pg_content   .= "</form>\n";
        
    $page_dtls->{page_head}    = "Change Password";
    $page_dtls->{html_title}   = "$site_title - Change Password";
    $page_dtls->{page_body}    = $pg_content;
        
    $shared->page_gen( $page_dtls, $java_msg );
    
}

###########################################################                   
# Change Password Form
#----------------------------------------------------------
sub chg_pass {

    my ( $self, $usr_url ) = @_;
    
    my $result = "";
    
    my $new_pass   = $shared->get_cgi( "new_pass" );
    my $new_conf   = $shared->get_cgi( "new_conf" );
    my $config     = new config;
    my $usrpw_cky  = $config->get_usrpw_cookie;
    
    if ( $new_pass gt "" ) {
    	if ( $new_pass eq $new_conf ) {
    		
    		my $update = "UPDATE user SET usr_password = '$new_pass' WHERE usr_id = $usr_id";
    		$shared->doSQLUpdate( $update );
    		
    	    $shared->store_cookie( $usrpw_cky, $new_pass );
    	    
    	    $result = "Password Changed";
    		
        } else {
        	$java_msg = "New password and confirmation do not match.  Please retype your new password exactly the same way in the confirmation field.";
        	$self->chg_pass_form( $usr_url );
        }
    } else {
    	$java_msg = "Password cannot be blank.";
    	$self->chg_pass_form( $usr_url );
    }
    
    return $result;
}

###########################################################                   
# Get Login Status Box
#----------------------------------------------------------
sub getLoginStatus {
	
	my ( $self ) = @_;
	
	my $result = "";
	my $uname  = $self->getCurrentUserName;
	if ( $uname gt "" ) {
	    $result   .= "<table border=0 cellpadding=1 cellspacing=0>\n";
	    $result   .= "<tr><td valign=top align=center class=footer>";
	    $result   .= "Logged in as: <b>$uname</b></td></tr>\n";
	    $result   .= "</table>\n";
	}
	return $result;
}

###########################################################                   
# Get User Option Links
#----------------------------------------------------------
sub getUserOptions {

    my ( $self ) = @_;
	my $result   = "";

    if ( $usr_id gt "" ) {
	
        my $query = "SELECT syd_id, syd_script, syd_label, syd_order,\n"
                  . "syd_ext_url, syd_blank_target, syd_sepline\n"
                  . "FROM sys_module\n"
                  . "LEFT JOIN user_module ON ( usm_syd_id = syd_id AND usm_usr_id = $usr_id )\n"
                  . "WHERE syd_display = 'Y'\n"
                  . "AND ( usm_id IS NOT NULL OR syd_sepline = 'Y' )\n"
                  . "ORDER BY syd_order";
              
        my @rs = $shared->getResultSet( $query );

        foreach my $syd_rec ( @rs ) {
    	    my $script = "$script_path/" . $syd_rec->{syd_script};
    	    my $label  = $syd_rec->{syd_label};
	        my $syd_id = $syd_rec->{syd_id};
	        my $extURL = $syd_rec->{syd_ext_url};
	        my $blank  = $syd_rec->{syd_blank_target};
	        my $isSep  = $syd_rec->{syd_sepline};
	    
    	    if ( $isSep eq "Y" ) {
    	        $result .= "<hr class=navsep>\n";
    	    } else {
    	        if ( $extURL gt "" ) {
    	            $result .= "<a href=\"" . $extURL . "\"";
	                if ( $blank eq "Y" ) {
	            	    $result .= " target=_blank";
	                }
	                $result .= " class=adminlink>$label</a><br>\n";
	            } else {
  	                $result .= "<a href=\"" . $script . "\" class=adminlink>$label</a><br>\n";
	            }
	        }
	    }
	    
	    $result .= "<a href=\"$script_path/admin.cgi?action=logout\" class=adminlink>Logout</a><br>\n";
	             
	}
	
	return $result;
	
}

###########################################################                   
# Validate current user has access to module
#----------------------------------------------------------
sub validate_access {

    my ( $self, $syd_code ) = @_;
	my $result   = 0;

    if ( ( $usr_id gt "" ) && ( $syd_code gt "" ) ) {
	
	    my $syd_id = $shared->getFieldVal( "sys_module", "syd_id", "syd_code", $syd_code );
	    
        my $count  = $shared->getSQLCount( "SELECT count(*) from user_module WHERE usm_usr_id = $usr_id AND usm_syd_id = $syd_id" );
        if ( $count > 0 ) {
        	$result = 1;
        }
	}
	
	return $result;
}

1;
