#!/usr/bin/perl
########################################
# This package provides shared database
# and CGI routines and other functions.
#
# Created: 03/10/2004 - Eric Huber
########################################
use Net::SMTP;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use photo;
use member;
use sitetables;

my $shared;
my $photo;
my $member;
my $sitetables;
my $src_path;
my $js_path;
my $cgi_url;
my $script_path;
my $forms_path;
my $forms_url;
my $color_head1;
my $color_head2;
my $color_bg1;
my $color_bg2;
my $color_bg3;
my $color_bg4;
my $img_path;
my $photo_url;
my $site_title;
my $js_cargo = "";

my $debug = 0;
my $page_dtls;

package pages;

# -------------------------------------------------------------------------------------
# The constructor   
# -------------------------------------------------------------------------------------
sub new {
    
    my ( $class, $sh_in, $photo_in, $member_in, $st_in ) = @_;
    
    my $self = {};
    
    my $config    = new config;
    $src_path     = $config->get_src_path;
    $js_path      = $config->get_js_path;
    $forms_path   = $config->get_forms_path;
    $forms_url    = $config->get_forms_url;
    $script_path  = $config->get_script_path;
    $cgi_url      = "$script_path/site.cgi";
    my (%colors)  = $config->get_colors;
    $color_head1  = $colors{ "HEAD1" };
    $color_head2  = $colors{ "HEAD2" };
    $color_bg1    = $colors{ "BG1" };
    $color_bg2    = $colors{ "BG2" };
    $color_bg3    = $colors{ "BG3" };
    $color_bg4    = $colors{ "BG4" };
    $img_path     = $config->get_img_path;
    $photo_url    = $config->get_photo_url;
    $site_title   = $config->get_app_name;
    $debug        = $config->get_debug;
    
    bless ( $self, $class );

    $shared                       = $sh_in;
    $photo                        = $photo_in;
    $member                       = $member_in;
    $sitetables                   = $st_in;
    $page_dtls->{template}        = "default.html";

    return $self;
}
# -------------------------------------------------------------------------------------



############################################################
# Display Page from database
#-----------------------------------------------------------
sub disp_page {

    my ( $self, $pag_code, $java_msg ) = @_;
    my ( $page_dtls );

	if ( $pag_code gt "" ) {
	    
    	my $pag_rec   = $shared->get_rec( "pages", "pag_code", $pag_code );
		my $pag_title = $pag_rec->{pag_title};
		my $pag_head  = $pag_rec->{pag_heading};
		my $pag_src   = $pag_rec->{pag_src_file};
		my $zone1     = $pag_rec->{pag_zone_1};
		my $zone2     = $pag_rec->{pag_zone_2};
		my $zone3     = $pag_rec->{pag_zone_3};
		my $img1      = $pag_rec->{pag_image_1};
		my $img2      = $pag_rec->{pag_image_2};
		my $img3      = $pag_rec->{pag_image_3};
		my $pag_colHd = $pag_rec->{pag_col_head};
		my $pag_colBd = $pag_rec->{pag_col_body};
		my $pag_colW  = $pag_rec->{pag_col_width};
		my $pag_colAW = ( $pag_colW - 4 );
		
		my $aTask     = $shared->get_cgi( "task" );
		
		if ( ( $pag_colHd gt "" ) && ( ( $aTask eq "" ) && ( $pag_code ne "competition" ) ) ) {
		    $page_dtls->{template} = "default_column.html";
		}
		
		if ( ( $pag_title gt "" ) || ( $pag_head gt "" ) || ( $pag_src gt "" ) ) {
			$page_dtls->{html_title}     = $pag_title;
			$page_dtls->{page_head}      = $pag_head;
			$page_dtls->{page_code}      = $pag_code;
			
			if ( $page_dtls->{template} eq "default_column.html" ) {
			    $page_dtls->{column_head}      = $pag_colHd;
			    $page_dtls->{column_body}      = $self->getColBody( $pag_colBd );
			    $page_dtls->{column_width}     = $pag_colW;
			    $page_dtls->{column_adj_width} = $pag_colAW;
			    
			}
			
			if ( $pag_code eq "home" ) {
			    $page_dtls->{column_extra}      = $self->getHomeStats;
			}
			
			if ( $pag_code eq "access" ) {
			    $page_dtls->{submenu_links} = $member->getSubMenu;
			    $page_dtls->{page_body}     = $member->getContent;
			    $java_msg                   = $member->getAlertMsg;
			}
			
			if ( $pag_code eq "leadership" ) {
			    $page_dtls->{submenu_links} = $self->getLdrSubMenu;
			    $page_dtls->{page_body}     = $self->getLdrContent;
			}
			
			if ( $pag_code eq "calendar" ) {
                my $pf_str = "<a href=\"$cgi_url?action=state_calendar_pdf\" target=_blank><img src=\"$img_path/printer.gif\" border=0 alt=\"Printer Friendly Version\" title=\"Printer Friendly Version\" style=\"float:right\"></a>";
	            $page_dtls->{print_friendly}  = $pf_str;
			    $page_dtls->{template}        = "default_print.html";
			    $page_dtls->{content_heading} = $self->getEventHead;
		        $page_dtls->{page_body}       = $self->getEventList( "STATE" );
			}
			          			    
			if ( $page_dtls->{page_body} eq "" ) {
			    $page_dtls->{page_body}      = $self->getPageFromSource( $pag_src, $pag_code, $zone1, $zone2, $zone3, $img1, $img2, $img3, $season );
			}
			
			if ( $js_cargo gt "" ) {
			    $page_dtls->{javascript} = $js_cargo;
			}
			
			if ( ( $page_dtls->{template} eq "default.html" ) && ( $pag_head eq "" ) ) {
			    $page_dtls->{template} eq "default_nohead.html";
			}
			
			$shared->page_gen( $page_dtls, $java_msg );
		} else {
			$shared->do_err( "Invalid Page Code.  Contact the administrator for assistance.", "" );
		}
	} else {
		$shared->do_err( "Page Code parameter is required and was not passed.  Contact the administrator for assistance.", "" );
    }
}

###########################################################                   
# Get Page content from source file
#----------------------------------------------------------
sub getPageFromSource {
	
	my ( $self, $src_file, $pag_code, $zone1, $zone2, $zone3, $img1, $img2, $img3, $season ) = @_;
	
	my $result = "";
	
	my $conf;
	$conf->{cgi_url}        = $cgi_url;
	$conf->{color_head1}    = $color_head1;
	$conf->{color_head2}    = $color_head2;
	$conf->{color_bg1}      = $color_bg1;
	$conf->{color_bg2}      = $color_bg2;
	$conf->{color_bg3}      = $color_bg3;
	$conf->{color_bg4}      = $color_bg4;
	$conf->{img_path}       = $img_path;
	$conf->{page_url}       = "$cgi_url?action=view_page&page=";
	$conf->{script_path}    = $script_path;
	$conf->{forms_url}      = $forms_url;
	$conf->{js_path}        = $js_path;
    if ( $pag_code eq "about_join" ) {
	    $conf->{join_link}  = $self->getJoinLink;
	}

    $zone1       =~ s/{{photo\:(.*?)}}/$photo->display_photo( $1, $cgi_url )/mge;
    $zone1       =~ s/{{bodmem\:(.*?)}}/$self->display_bodmem( $1 )/mge;
    $zone1       =~ s/{{doclink\:(.*?)}}/$self->display_doclink( $1 )/mge;
    $zone1       =~ s/{{imglink\:([^\:]+)\:(.*?)}}/$self->display_imglink( $1, $2 )/mge;
    $zone1       =~ s/{{table\:(.*?)}}/$self->display_table( $1 )/mge;
    $zone1       =~ s/{{(.*?)}}/$conf->{$1}/mg;
    
    $zone2       =~ s/{{photo\:(.*?)}}/$photo->display_photo( $1, $cgi_url )/mge;
    $zone2       =~ s/{{bodmem\:(.*?)}}/$self->display_bodmem( $1 )/mge;
    $zone2       =~ s/{{doclink\:(.*?)}}/$self->display_doclink( $1 )/mge;
    $zone2       =~ s/{{imglink\:([^\:]+)\:(.*?)}}/$self->display_imglink( $1, $2 )/mge;
    $zone2       =~ s/{{table\:(.*?)}}/$self->display_table( $1 )/mge;
    $zone2       =~ s/{{(.*?)}}/$conf->{$1}/mg;

    $zone3       =~ s/{{photo\:(.*?)}}/$photo->display_photo( $1, $cgi_url )/mge;
    $zone3       =~ s/{{bodmem\:(.*?)}}/$self->display_bodmem( $1 )/mge;
    $zone3       =~ s/{{doclink\:(.*?)}}/$self->display_doclink( $1 )/mge;
    $zone3       =~ s/{{imglink\:([^\:]+)\:(.*?)}}/$self->display_imglink( $1, $2 )/mge;
    $zone3       =~ s/{{table\:(.*?)}}/$self->display_table( $1 )/mge;
    $zone3       =~ s/{{(.*?)}}/$conf->{$1}/mg;

	$conf->{zone_1}         = $zone1;
	$conf->{zone_2}         = $zone2;
	$conf->{zone_3}         = $zone3;
	$conf->{img1}           = $img1;
	$conf->{img2}           = $img2;
	$conf->{img3}           = $img3;

	
	if ( $pag_code eq "home" ) {
	    $conf->{home_ss}    = $self->genSlideShow( $season );
    } elsif ( $pag_code eq "teacherloc" ) {
        $conf->{svc_list}   = $shared->pop_lookup_select( "SERVICE", "" );
        $conf->{results}    = $self->getFindTeacher;
	} elsif ( $pag_code eq "news" ) {
	    $conf->{news_list}  = $self->getDocList( "NEWS" );
    } elsif ( $pag_code eq "areas" ) {
        $conf->{area_list}  = $self->getChapterList;
	} elsif ( $pag_code eq "competition" ) {
	    $conf->{comp_list}  = $self->getCompList;
	} elsif ( $pag_code eq "contact" ) {
	    $conf->{page_info}  = $self->getContactData( $zone3 );
	}
	
    open ( FILE, "$src_path/$src_file") or $result = "<span class=\"errbody\">Couldn't open $src_path/$src_file: $!</span>";
    if ( $result eq "" ) {
        while (<FILE>) {
            s/{{slideshow\:\:(.*?)}}/$self->genSlideShow( $1 )/ge;
            s/{{(.*?)}}/$conf->{$1}/g;
            $result .= $_;
        }
        close FILE;
    }
    return $result;
}


###########################################################                   
# Get column body content
#----------------------------------------------------------
sub getColBody {
	
	my ( $self, $content ) = @_;
	
	my $result = "";
	
	my $conf;
	$conf->{cgi_url}        = $cgi_url;
	$conf->{color_head1}    = $color_head1;
	$conf->{color_head2}    = $color_head2;
	$conf->{color_bg1}      = $color_bg1;
	$conf->{color_bg2}      = $color_bg2;
	$conf->{color_bg3}      = $color_bg3;
	$conf->{img_path}       = $img_path;
	$conf->{page_url}       = "$cgi_url?action=view_page&page=";
	$conf->{script_path}    = $script_path;
	$conf->{forms_url}      = $forms_url;
	$conf->{js_path}        = $js_path;

    $content =~ s/{{form\:(.*?)}}/$self->getFormDown( $1 )/mge;
    $content =~ s/{{photo\:(.*?)}}/$photo->display_photo( $1, $cgi_url )/mge;
    $content =~ s/{{(.*?)}}/$conf->{$1}/mg;
    
    return $content;
}

###########################################################                   
# Get Download Form List
#----------------------------------------------------------
sub getFormDown {
	
	my ( $self, $param ) = @_;
	
	my $result      = "";
	
	my $SQL = "SELECT * FROM document\n"
	        . "WHERE doc_category = '$param'\n"
	        . "AND ( doc_expire_date = '0000-00-00' OR doc_expire_date > curdate() )\n"
	        . "ORDER BY doc_name";
	my @rs  = $shared->getResultSet( $SQL );
	my $cnt = 0;
	foreach my $doc_rec ( @rs ) {
	    my $doc_id = $doc_rec->{doc_id};
	    my $name   = $doc_rec->{doc_name};
	    my $type   = $doc_rec->{doc_type};
	    
       	my $type_img = "word.gif";
       	if ( $type eq "PDF" ) {
       		$type_img = "pdf.gif";
       	}
       	$result .= "<a href=\"$forms_url/form" . $doc_id . "." . lc( $type ) . "\" target=_blank><img src=\"$img_path/$type_img\" border=0 alt=\"\">&nbsp;<b>$name</b></a><br>\n";
       	$cnt++;
    }
    
    if ( $cnt == 0 ) {
   	    $result = "<span class=mbody><i>No Forms Available.</i></span><br>\n";
    }
       	
    return $result;
}

###########################################################                   
# Display Document Link
#----------------------------------------------------------
sub display_doclink {
    
    my ( $self, $doc_id ) = @_;
    
    my $result = "<a href=\"" . $self->getDocLink( $doc_id ) . "\" target=_blank><b>Click Here</b></a>";
    
    return $result;
    
}

###########################################################                   
# Display Image Link
#----------------------------------------------------------
sub display_imglink {
    
    my ( $self, $img_file, $url ) = @_;
    
    my $result = "<a href=\"http://$url\" target=_blank><img src=\"$img_path/links/$img_file\" border=1 alt=\"\" title=\"\" width=120></a>&nbsp;&nbsp;&nbsp;";
    
    return $result;
    
}

###########################################################                   
# Get Download Form List
#----------------------------------------------------------
sub getDocLink {
	
	my ( $self, $doc_id ) = @_;
	
	my $result      = "";
	
	my $doc_rec = $shared->get_rec( "document", "doc_id", $doc_id );
	
	my $type   = $doc_rec->{doc_type};
	    
    $result .= "$forms_url/form" . $doc_id . "." . lc( $type );
    
    return $result;
}




###########################################################                   
# Get document list
#----------------------------------------------------------
sub getDocList {
	
	my ( $self, $param ) = @_;
	
	my $result      = "";
	
	my $where_text  = "doc_category != 'NEWS'";
	if ( $param eq "NEWS" ) {
		$where_text = "doc_category = 'NEWS'";
	} elsif ( $param eq "CONF_HO" ) {
	    $where_text = "doc_category = 'CONF_HO'";
	}
	
	my $SQL = "SELECT * FROM document\n"
	        . "WHERE $where_text\n"
	        . "AND ( doc_expire_date = '0000-00-00' OR doc_expire_date > curdate() )\n"
	        . "ORDER BY doc_category, doc_name";
	my @rs  = $shared->getResultSet( $SQL );
	
	my $hold_cat = "";
	my $cnt = 0;
	foreach my $doc_rec ( @rs ) {
	    my $doc_id = $doc_rec->{doc_id};
	    my $cat    = $doc_rec->{doc_category};
	    my $type   = $doc_rec->{doc_type};
	    my $name   = $doc_rec->{doc_name};
	    
	    if ( ( $hold_cat ne $cat ) && ( $param ne "NEWS" ) && ( $param ne "CONF_HO" ) ) {
	    	my $dspCat = $shared->getLookupDesc( "DOC_CATEGORY", $cat );
	    	if ( $cnt > 0 ) {
	    		$result .= "<br>\n";
	    	}
	    	$result   .= "<span class=sublabel><b>$dspCat</b></span><br>\n";
	    	$hold_cat  = $cat;
	    }
	    
       	my $type_img = "word.gif";
       	if ( $type eq "PDF" ) {
       		$type_img = "pdf.gif";
       	}
       	$result .= "<a href=\"$forms_url/form" . $doc_id . "." . lc( $type ) . "\" target=_blank><img src=\"$img_path/$type_img\" border=0 alt=\"\">&nbsp;<b>$name</b></a><br>\n";
       	
       	$cnt++;
    }
    
    if ( $cnt == 0 ) {
    	if ( $param eq "NEWS" ) {
    		$result = "<i>No Newsletters Posted Yet.</i>\n";
        } elsif ( $param eq "CONF_HO" ) {
            $result = "<i>No Handouts Posted Yet.</i>\n";
    	} else {
    	    $result = "<i>No Forms Available.</i>\n";
    	}
    }
       	
    return $result;
}

###########################################################                   
# Get event list
#----------------------------------------------------------
sub getEventList {
	
	my ( $self, $param ) = @_;
	
	my $result      = "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";

    my $curYear     = $shared->getCurrentYear + 0;
    my $startDate   = $curYear . "-08-01";
    if ( ! $shared->isDatePast( $startDate ) ) {
        $curYear--;
        $startDate  = $curYear . "-08-01";
    }
    
	my $where_text  = "cal_chapter = ''";
	if ( $param ne "STATE" ) {
		$where_text = "cal_chapter = '$param'";
	}
	my $SQL = "SELECT * FROM calendar\n"
	        . "WHERE $where_text\n"
	        . "AND cal_date >= '$startDate'\n"
	        . "ORDER BY cal_date";
	my @rs  = $shared->getResultSet( $SQL );
	
	my $cnt = 0;
	my $holdMonth = "";
	foreach my $cal_rec ( @rs ) {
	    my $evtDate  = $cal_rec->{cal_date};
	    my $cal_date = $shared->getDateDesc( $evtDate, 1 );
	    my $cal_time = $cal_rec->{cal_time};
	    my $loc      = $cal_rec->{cal_location};
	    my $desc     = $cal_rec->{cal_description};
	    my $comment  = $cal_rec->{cal_comments};
	    
	    my ( $dMon, $dDay, $dYr ) = split( " ", $cal_date );
	    $dDay       =~ s/\,//g;
	    my $dspDate  = substr( $dMon, 0, 3 ) . " " . $dDay;
	    
	    my $pastEvt  = $shared->isDatePast( $evtDate );
	    
	    my $chkMon   = substr( $dMon, 0, 3 ) . $dYr;
	    if ( $pastEvt ) {
	        $chkMon  = "000";
	    }
	    
	    if ( $holdMonth ne $chkMon ) {
	        if ( $cnt > 0 ) {
	            $result .= "<tr><td valign=top align=left colspan=3><br /><br /></td></tr>\n";
	        }
	        $result  .= "<tr><td valign=top align=left colspan=3 class=colhead>\n";
	        if ( $pastEvt ) {
	            $result .= "Past Events";
	        } else {
	            $result .= "$dMon $dYr";
	        }
	        $result   .= "</td></tr>\n";
	        $result   .= "<tr><td valign=top align=left><div class=\"mbody bg3\">Date/Time</div></td>\n"
	                   . "<td valign=top align=left><div class=\"mbody bg3\">Event Description/Information</div></td>\n"
	                   . "<td valign=top align=left><div class=\"mbody bg3\">Location</div></td></tr>\n";
	                   
	        $holdMonth = $chkMon;
	    }

        if ( $loc eq "" ) {
        	$loc = "N/A";
        }
        
        if ( $pastEvt ) {
            $result .= "<tr><td valign=top align=left class=mbody><i>$dspDate<br>$cal_time</i></td>\n";
            $result .= "<td valign=top align=left class=mbody><span class=bullet><i>$desc</i></span><br><i>$comment</i></td>\n";
            $result .= "<td valign=top align=left class=mbody><i>$loc</i></td></tr>\n";
        } else {
            $result .= "<tr><td valign=top align=left class=mbody><b>$dspDate</b><br>$cal_time</td>\n";
            $result .= "<td valign=top align=left class=mbody><span class=sublabel><b>$desc</b></span><br>$comment</td>\n";
            $result .= "<td valign=top align=left class=mbody>$loc</td></tr>\n";
        }
            
        
        $cnt++;
    }
    
    if ( $cnt == 0 ) {
    	$result = "<tr><td valign=top align=left class=bullet colspan=3><b>No Calendar Events Found.</b></td></tr>";
    }
       	
    $result .= "</table>\n";
    
    return $result;
}

###########################################################                   
# Get chapter list
#----------------------------------------------------------
sub getChapterList {
	
	my ( $self ) = @_;
	
	my $result   = "<div id=arealist>\n"
	             . "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	
	my $SQL = "SELECT chp_code, chp_chapter_desc, chp_website, chp_link_label, chp_link_blank_target, chp_link_url, mbr_last, mbr_first,\n"
	        . "mbr_phone, mbr_email FROM chapter\n"
	        . "LEFT JOIN member ON ( chp_code = mbr_chapter AND mbr_chap_stat = 'PRES' )\n"
	        . "ORDER BY chp_chapter_desc";
	my @rs  = $shared->getResultSet( $SQL );
	
	my $cnt = 0;
	foreach my $chp_rec ( @rs ) {
		my $chp_code = $chp_rec->{chp_code};
	    my $chapter  = $chp_rec->{chp_chapter_desc};
	    my $website  = $chp_rec->{chp_website};
	    my $lnkLbl   = $chp_rec->{chp_link_label};
	    my $lnkTgt   = $chp_rec->{chp_link_blank_target};
	    my $lnkURL   = $chp_rec->{chp_link_url};
	    my $presname = $shared->trim( $chp_rec->{mbr_first} . " " . $chp_rec->{mbr_last} );
	    my $phone    = $chp_rec->{mbr_phone};
	    if ( $presname eq "" ) {
	    	$presname = "<i>None</i>";
	    }

        my $eml   = "";
   	    my $email = $chp_rec->{mbr_email};
	    if ( $email gt "" ) {
	    	$eml .= "<br><a href=\"mailto:$email\"><span class=form><b>$email</b></span></a>";
	    }
	   

        if ( $cnt > 0 ) {
        	$result .= "<tr><td valign=top class=form colspan=3><br><hr class=\"sep\"></td></tr>\n";
        }
        $result .= "<tr><td valign=top align=left class=bullet><b>$chapter</b></td>\n";
        $result .= "<td valign=top align=left class=form>\n";
        $result .= "<a href=\"$cgi_url?action=area_leaders&chp_code=$chp_code\"><b>[ Leadership ]</b></a><br>\n";
        $result .= "<a href=\"$cgi_url?action=area_calendar&chp_code=$chp_code\"><b>[ Calendar ]</b></a><br>\n";
        if ( $website gt "" ) {
            $result .= "<a href=\"$website\" target=_blank><b>[ Website ]</b></a><br>\n";
        }
        if ( $lnkLbl gt "" ) {
            my $lTgt = "";
            if ( $lnkTgt eq "Y" ) {
                $lTgt = " target=_blank";
            }
            $result .= "<a href=\"$lnkURL\"" . $lTgt . "><b>[ $lnkLbl ]</b></a><br>\n";
        }
        $result .= "</td>\n";
        $result .= "<td valign=top align=left class=form><span class=label><b>Area Chair</b></span><br>$presname<br>$phone" . $eml . "</td></tr>\n";
        
        $cnt++;
    }
    
    if ( $cnt == 0 ) {
    	$result .= "<tr><td valign=top align=left class=body colspan=3><b>No Areas Found.</b></td></tr>";
    }
    
    $result .= "</table>\n";
    $result .= "</div>\n";
       	
    return $result;
}


###########################################################                   
# Get Photo List of Board Members
#----------------------------------------------------------
sub getPhotoList {
	
	my ( $self, $filt ) = @_;
	
	my $content = "<table border=0 cellpadding=4 cellspacing=0 width=100%>\n";
	
	my $fndCnt = 0;
	
	if ( $filt eq "EXEC" ) {
	
	    my $SQL = "SELECT brd_order, brd_description AS 'desc', brd_subdesc AS 'subdesc', mbr_first, mbr_last, mbr_cert, mbr_phone, mbr_email,\n"
	            . "mbr_photo, mbr_id\n"
	            . "FROM board\n"
	            . "LEFT JOIN member ON ( brd_mbr_id = mbr_id AND mbr_paid_thru_date > curdate() )\n"
	            . "WHERE brd_is_exec = 'Y'\n"
	            . "ORDER BY brd_order";
	         
	     my $hash   = &procPLQuery( $SQL, 0 );
	     $fndCnt   += $hash->{fndCnt};
	     $content  .= $hash->{content};
	     
    } else {
	    my $SQL1 = "SELECT brd_order, brd_description AS 'desc', brd_subdesc AS 'subdesc', mbr_first, mbr_last, mbr_cert, mbr_phone, mbr_email,\n"
	             . "mbr_photo, mbr_id\n"
	             . "FROM board\n"
	             . "LEFT JOIN member ON ( brd_mbr_id = mbr_id AND mbr_paid_thru_date > curdate() )\n"
	             . "ORDER BY brd_order";
	          
	     my $res1   = &procPLQuery( $SQL1, 0 );
	     $fndCnt   += $res1->{fndCnt};
	     $content  .= $res1->{content};
	     my $colInd = $res1->{colInd};
	     
     
	}

    if ( $fndCnt == 0 ) {
	    $content .= "<tr><td valign=top align=left class=mbody colspan=2><b>No Board Titles Found.</b></td></tr>";
    }
    
    $content .= "</table>\n";
	
	return $content;
	
}


###########################################################                   
# Process PhotoList Query
#----------------------------------------------------------
sub procPLQuery {
    
    my ( $SQL, $colInd ) = @_;
    
    my $content = "";
	my @rs      = $shared->getResultSet( $SQL );
	my $fndCnt  = 0;
	my $lts     = time;
	
    foreach my $mbr_rec ( @rs ) {
    	my $lstN = $mbr_rec->{mbr_last};
    	my $dspN = "<b>" . $mbr_rec->{mbr_last};
    	if ( $mbr_rec->{mbr_first} gt "" ) {
    	    $dspN = "<b>" . $mbr_rec->{mbr_first} . " " . $mbr_rec->{mbr_last};
    	}
    	my $cert = $mbr_rec->{mbr_cert};
    	if ( $cert gt "" ) {
    		$dspN .= ", $cert";
    	}
    	$dspN   .= "</b>";

    	
    	my $phn  = $mbr_rec->{mbr_phone};
    	if ( $phn gt "" ) {
    		$dspN .= "<br>$phn";
    	}

        my $eml = $mbr_rec->{mbr_email};
    	if ( $eml gt "" ) {
    		$dspN .= "<br><a href=\"mailto:$eml\"><b>$eml</b></a>";
    	}
    	
        my $title = $mbr_rec->{desc};
        if ( $mbr_rec->{subdesc} gt "" ) {
            $title .= "<br>" . $mbr_rec->{subdesc};
        }
        
        if ( $lstN eq "" ) {
        	$dspN = "<i>Unassigned</i>";
        }
        
        if ( $colInd == 0 ) {
            $content .= "<tr>\n";
        }
        
	    $content .= "<td valign=top align=left class=tabcell width=50%><div style=\"float:left;padding-right:10px;padding-bottom:20px\">\n";
        if ( ( $mbr_rec->{mbr_photo} eq "N" ) || ( $lstN eq "" ) ) {
            $content   .= "<img src=\"$img_path/member_filler.jpg\" border=\"1\" width=\"80\" height=\"100\">\n";
        } else {
            $content   .= "<img src=\"$photo_url/member_" . $mbr_rec->{mbr_id} . ".jpg?$lts\" border=\"1\" width=\"80\" height=\"100\">\n";
        }
	    
	    $content .= "</div>\n";
	    $content .= "<span class=sublabel><b>$title</b></span><br>$dspN</td>\n";
	    
	    if ( $colInd == 1 ) {
	        $content .= "</tr>\n";
	        $colInd   = 0;
	    } else {
	        $colInd   = 1;
	    }
	    
	    	
	    $fndCnt++;
	}
	    		
	my $result;
	$result->{fndCnt}  = $fndCnt;
	$result->{content} = $content;
	$result->{colInd}  = $colInd;
	
    
    return $result;
}



###########################################################                   
# Get Regular List
#----------------------------------------------------------
sub getRegList {
	
	my ( $self, $filt ) = @_;
	
	my $content = "<table border=0 cellpadding=4 cellspacing=0 width=100%>\n";
	
	my $fndCnt = 0;
	
	if ( $filt eq "COMPCTC" ) {
	
	     my $SQL2 = "SELECT cmp_id, cmp_name AS 'desc', mbr_first, mbr_last, mbr_cert, mbr_phone, mbr_email,\n"
	              . "mbr_photo, mbr_id\n"
	              . "FROM competition\n"
	              . "LEFT JOIN member ON ( cmp_chair_mbr_id = mbr_id AND mbr_paid_thru_date > curdate() )\n"
	              . "WHERE cmp_ufmc_hosted = 'Y'\n"
	              . "ORDER BY cmp_id";
	     my $res2   = &procRegQuery( $SQL2, 0 );
	     $fndCnt   += $res2->{fndCnt};
	     $content  .= $res2->{content};
	     
	}



    if ( $fndCnt == 0 ) {
	    $content .= "<tr><td valign=top align=left class=mbody colspan=2><b>None Found.</b></td></tr>";
    }
    
    $content .= "</table>\n";
	
	return $content;
	
}


###########################################################                   
# Process Regular List Query
#----------------------------------------------------------
sub procRegQuery {
    
    my ( $SQL, $colInd ) = @_;
    
    my $content = "";
	my @rs      = $shared->getResultSet( $SQL );
	my $fndCnt  = 0;
		
    foreach my $mbr_rec ( @rs ) {
    	my $lstN = $mbr_rec->{mbr_last};
    	my $dspN = "<b>" . $mbr_rec->{mbr_last};
    	if ( $mbr_rec->{mbr_first} gt "" ) {
    	    $dspN = "<b>" . $mbr_rec->{mbr_first} . " " . $mbr_rec->{mbr_last};
    	}
    	my $cert = $mbr_rec->{mbr_cert};
    	if ( $cert gt "" ) {
    		$dspN .= ", $cert";
    	}
    	$dspN   .= "</b>";

    	
    	my $phn  = $mbr_rec->{mbr_phone};
    	if ( $phn gt "" ) {
    		$dspN .= "<br>$phn";
    	}

        my $eml = $mbr_rec->{mbr_email};
    	if ( $eml gt "" ) {
    		$dspN .= "<br><a href=\"mailto:$eml\"><b>$eml</b></a>";
    	}
    	
        my $title = $mbr_rec->{desc};
        
        if ( $lstN eq "" ) {
        	$dspN = "<i>Unassigned</i>";
        }
        
        if ( $colInd == 0 ) {
            $content .= "<tr>\n";
        }
        
	    $content .= "<td valign=top align=left class=form><span class=sublabel><b>$title</b></span><br />$dspN<br /><br /></td>\n";
	    
	    if ( $colInd == 1 ) {
	        $content .= "</tr>\n";
	        $colInd   = 0;
	    } else {
	        $colInd   = 1;
	    }
	    
	    $fndCnt++;
	}
	    		
	my $result;
	$result->{fndCnt}  = $fndCnt;
	$result->{content} = $content;
	$result->{colInd}  = $colInd;
	
    
    return $result;
}



###########################################################                   
# Get director list
#----------------------------------------------------------
sub getBoardList {
	
	my ( $self, $filt ) = @_;
	
	my $content = "<table border=0 cellpadding=4 cellspacing=0 width=100%>\n";
	
	my $SQL;
	
	if ( $filt eq "" ) {
	    $SQL = "SELECT brd_order, brd_description, brd_subdesc, mbr_first, mbr_last, mbr_cert, mbr_phone, mbr_email\n"
	         . "FROM board\n"
	         . "LEFT JOIN member ON ( brd_mbr_id = mbr_id AND mbr_paid_thru_date > curdate() )\n"
	         . "ORDER BY brd_order";
	} elsif ( $filt eq "COMPEVAL" ) {
	    $SQL = "SELECT brd_order, brd_description, brd_subdesc, mbr_first, mbr_last, mbr_cert, mbr_phone, mbr_email\n"
	         . "FROM board\n"
	         . "LEFT JOIN member ON ( brd_mbr_id = mbr_id AND mbr_paid_thru_date > curdate() )\n"
	         . "WHERE brd_is_compeval_chair = 'Y'\n"
	         . "ORDER BY brd_order";
	}
	    
	
	my @rs   = $shared->getResultSet( $SQL );
	my $fndCnt = 0;
	my $eCnt   = 0;    
    foreach my $mbr_rec ( @rs ) {
    	my $lstN = $mbr_rec->{mbr_last};
    	my $dspN = "<b>" . $mbr_rec->{mbr_last};
    	if ( $mbr_rec->{mbr_first} gt "" ) {
    	    $dspN = "<b>" . $mbr_rec->{mbr_first} . " " . $mbr_rec->{mbr_last};
    	}
    	my $cert = $mbr_rec->{mbr_cert};
    	if ( $cert gt "" ) {
    		$dspN .= ", $cert";
    	}
    	$dspN   .= "</b>";
    	
    	my $phn  = $mbr_rec->{mbr_phone};
    	if ( $phn gt "" ) {
    		$dspN .= "<br>$phn";
    	}

        my $eml = $mbr_rec->{mbr_email};
    	if ( $eml gt "" ) {
    		$dspN .= "<br><a href=\"mailto:$eml\"><b>$eml</b></a>";
    	}
    	
        my $title = $mbr_rec->{brd_description};
        if ( $mbr_rec->{brd_subdesc} gt "" ) {
            $title .= "<br>" . $mbr_rec->{brd_subdesc};
        }
        
        if ( $lstN eq "" ) {
        	$dspN = "<i>Unassigned</i>";
        }
        
        if ( $eCnt == 0 ) {
            $content .= "<tr>\n";
        }
        
	    $content .= "<td valign=top align=left class=form><span class=sublabel><b>$title</b></span><br />$dspN<br /><br /></td>\n";
	    
	    if ( $eCnt == 1 ) {
	        $content .= "</tr>\n";
	        $eCnt     = 0;
	    } else {
	        $eCnt     = 1;
	    }
	    	
	    $fndCnt++;
	}
	    		
	if ( $eCnt > 0 ) {
	    $content .= "<td valign=top align=left>&nbsp;</td></tr>\n";
	}
	
    if ( $fndCnt == 0 ) {
	    $content .= "<tr><td valign=top align=left class=mbody colspan=2><b>No State Titles Found.</b></td></tr>";
    }
    
    $content .= "</table>\n";
    
    return $content;
}



###########################################################                   
# Get certified members.
#----------------------------------------------------------
sub getCertList {
	
	my ( $self ) = @_;
	
	my $content = "";
	my $SQL = "SELECT mbr_id, mbr_first, mbr_last, mbr_cert, mbr_phone, mbr_email,\n"
	        . "mbr_address, mbr_city, mbr_state, mbr_zip, mbr_chapter, mbr_chapter2, mbr_chapter3\n"
	        . "FROM member\n"
	        . "WHERE mbr_cert = 'NCTM'\n"
	        . "AND mbr_paid_thru_date > curdate()\n"
	        . "ORDER BY mbr_last, mbr_first";
	
	my @rs   = $shared->getResultSet( $SQL );
	my $fndCnt = 0;
	    
    foreach my $mbr_rec ( @rs ) {
    	my $id   = $mbr_rec->{mbr_id};
    	my $dspN = "<b>" . $mbr_rec->{mbr_last};
    	if ( $mbr_rec->{mbr_first} gt "" ) {
    	    $dspN .= ", " . $mbr_rec->{mbr_first};
    	}
    	my $cert = $mbr_rec->{mbr_cert};
    	if ( $cert gt "" ) {
    		$dspN .= " ($cert)";
    	}
    	$dspN   .= "</b>";
    	my $eml  = $mbr_rec->{mbr_email};
    	my $phn  = $mbr_rec->{mbr_phone};
    	my $addr = $mbr_rec->{mbr_address};
    	my $city = $mbr_rec->{mbr_city};
    	my $st   = $mbr_rec->{mbr_state};
    	my $zip  = $mbr_rec->{mbr_zip};
    	my $chp1 = $mbr_rec->{mbr_chapter};
    	my $chp2 = $mbr_rec->{mbr_chapter2};
    	my $chp3 = $mbr_rec->{mbr_chapter3};
	    	
    	my $c1D  = $shared->getFieldVal( "chapter", "chp_chapter_desc", "chp_code", $chp1 );
    	my $c2D  = $shared->getFieldVal( "chapter", "chp_chapter_desc", "chp_code", $chp2 );
    	my $c3D  = $shared->getFieldVal( "chapter", "chp_chapter_desc", "chp_code", $chp3 );
	    	
    	my $cLst = "$c1D";
    	if ( $c2D gt "" ) {
    		$cLst .= ", $c2D";
    	}
    	if ( $c3D gt "" ) {
    		$cLst .= ", $c3D";
    	}

        if ( $fndCnt > 0 ) {
        	$content .= "<tr><td valign=top align=left colspan=3><img src=\"$img_path/sep_line.jpg\" border=0 alt=\"\"></td></tr>\n";
        }
        
	    $content .= "<tr><td valign=top align=left class=form><span class=bullet>$dspN</span><br>\n";
	    $content .= "$addr<br>$city, $st $zip<br>$phn";
       	if ( $eml gt "" ) {
    		$dspN .= "<br><a href=\"mailto:$eml\"><span class=form><b>$eml</b></span></a>";
    	}
        $content .= "</td>\n";
	    $content .= "<td valign=top align=left class=form><span class=label>Local Association(s)</span><br>\n";
	    $content .= "$cLst</td>\n";
	    $content .= "<td valign=top align=left class=form><span class=label>Services</span><br>\n";
	    	
	    my $subSQL = "SELECT lku_description FROM member_service\n"
	               . "INNER JOIN lookup ON ( lku_category = 'SERVICE' AND mbs_service = lku_code )\n"
	               . "WHERE mbs_mbr_id = $id\n"
	               . "ORDER BY lku_description";
	    my @subrs  = $shared->getResultSet( $subSQL );
	    foreach my $sub ( @subrs ) {
	    	my $svc_desc = $sub->{lku_description};
	    	$content .= "$svc_desc<br>\n";
	    }
	    	
	    $content .= "</td></tr>\n";
	    	
	    $fndCnt++;
	}
	    		
    if ( $fndCnt == 0 ) {
	    $content  = "<tr><td valign=top align=left class=body colspan=3><b>No Certified Members found</b></td></tr>";
    }
    
    return $content;
}

###########################################################                   
# Get Chapter Leadership list
#----------------------------------------------------------
sub getChapLeaderList {
	
	my ( $self, $chp_code ) = @_;
	
	my $content = "<table border=0 cellpadding=4 cellspacing=0 width=100%>\n";
	
	my $SQL = "SELECT mbr_first, mbr_last, mbr_cert, mbr_phone, mbr_email, lku_description, lku_sort\n"
	        . "FROM member\n"
	        . "LEFT JOIN lookup ON ( lku_category = 'CHAP_LEADER' AND lku_code = mbr_chap_stat )\n"
	        . "WHERE mbr_chapter = '$chp_code'\n"
	        . "AND mbr_chap_stat <> ''\n"
	        . "AND mbr_paid_thru_date > curdate()\n"
	        . "ORDER BY lku_sort, mbr_last";
	
	my @rs   = $shared->getResultSet( $SQL );
	my $fndCnt = 0;
	my $eCnt   = 0;    
    foreach my $mbr_rec ( @rs ) {
    	my $lstN = $mbr_rec->{mbr_last};
    	my $dspN = "<b>" . $mbr_rec->{mbr_last};
    	if ( $mbr_rec->{mbr_first} gt "" ) {
    	    $dspN = "<b>" . $mbr_rec->{mbr_first} . " $lstN";
    	}
    	my $cert = $mbr_rec->{mbr_cert};
    	if ( $cert gt "" ) {
    		$dspN .= ", $cert";
    	}
    	$dspN   .= "</b>";
    	
    	my $phn  = $mbr_rec->{mbr_phone};
    	if ( $phn gt "" ) {
    		$dspN .= "<br>$phn";
    	}
    	
    	my $eml  = $mbr_rec->{mbr_email};
    	if ( $eml gt "" ) {
    		$dspN .= "<br><a href=\"mailto:$eml\"><b>$eml</b></a>";
    	}

        my $title = $mbr_rec->{lku_description};
        
        if ( $eCnt == 0 ) {
            $content .= "<tr>\n";
        }
        
        $content .= "<td valign=top align=left class=form><span class=sublabel><b>$title</b></span><br />$dspN<br /><br /></td>\n";

        if ( $eCnt == 1 ) {
            $content .= "</tr>\n";
            $eCnt     = 0;
        } else {
            $eCnt     = 1;
        }
	    	
	    $fndCnt++;
	}
	
	if ( $eCnt > 0 ) {
	    $content .= "<td valign=top align=left>&nbsp</td></tr>\n";
	}
	    		
    if ( $fndCnt == 0 ) {
	    $content  = "<tr><td valign=top align=left class=mbody colspan=3><b>No Leadership Defined.</b></td></tr>";
    }
    
    $content .= "</table>\n";
    
    return $content;
}



###########################################################                   
# Generate Slide show javascript and HTML.  Add javascript
# to cargo variable and return HTML result.
#----------------------------------------------------------
sub genSlideShow {
	
	my ( $self, $ssInput ) = @_;
	
	my $result = "";
	my $js     = "";
	my $iList  = "";
	my $fstImg = "";
	my $align  = "R";
	my $ssCat  = $ssInput;
	my $ssExt  = "";
	
	if ( $ssInput =~ /\:\:/ ) {
	    ( $ssCat, $ssExt ) = split( "::", $ssInput );
	}
	    
	
	if ( $ssCat =~ /\-/ ) {
	    my ( $pCat, $pAlgn ) = split( "-", $ssCat );
	    $ssCat = $pCat;
	    $align = $pAlgn;
	}
	
	my $ssdim  = $shared->getLookupExtra( "IMG_CATEGORY", $ssCat );
	my $width  = 300;
	my $height = 200;
	if ( $ssdim gt "" ) {
	    ( $width, $height ) = split( "X", $ssdim );
	}
	
	my $SQL    = "SELECT img_id, img_caption, img_description FROM image WHERE img_category = '$ssCat' order by img_description";
	my @rs     = $shared->getResultSet( $SQL );
	my $cnt    = 0;
	foreach my $rec ( @rs ) {
	    my $imgID = $rec->{img_id};
	    my $cap   = $rec->{img_caption};
	    
	    if ( $cnt > 0 ) {
	        $iList .= ",\n";
	    } else {
	        $fstImg = "<img src=\"$photo_url/photo_" . $imgID . ".jpg\" alt=\"$cap\" />";
	    }
	    
	    $iList   .= "'photo_" . $imgID . ".jpg': { caption: '" . $cap . "' }";
	    $cnt++;
	}
	
	if ( $cnt > 0 ) {
        $js = "<style type=\"text/css\">\n"
            . ".slideshow-images {\n"
            . "    height: $height" . "px\n"
            . "    width:  $width" . "px\n"
            . "}\n"
            . "</style>\n"
            . "<script type=\"text/javascript\" src=\"$js_path/mootools.js\"></script>\n"
	        . "<script type=\"text/javascript\" src=\"$js_path/slideshow.js\"></script>\n"
	        . "<script type=\"text/javascript\">\n"
	        . "//<![CDATA[\n"
	        . "window.addEvent('domready', function(){\n"
	        . "   var data = {\n"
	        . "$iList\n"
	        . "	  };\n"
	        . "   var myShow = new Slideshow( 'show', data, { captions: true, height: $height, hu: '$photo_url/', width: $width, delay: 6000});\n"
	        . "});\n"
	        . "//]]>\n"
	        . "</script>\n";
	        
	    $js_cargo  = $js;
	    
	    my $tStyle = "float:right;margin-left:10px;margin-botton:10px;margin-top:5px";
	    if ( $align eq "L" ) {
	        $tStyle = "float:left;margin-right:10px;margin-botton:10px;margin-top:5px";
	    }
	    
	    my $adjW = ( $width ) + 5;
	    
	    $result = "<table border=0 cellpadding=0 cellspacing=0 width=$adjW style=\"$tStyle\">\n"
                . "<tr><td>\n"
                . "<div class=\"blur\"><div class=\"shadow\"><div class=\"framework\">\n"
                . "<div id=\"show\" class=\"slideshow\" style=\"height:$height;width:$width;\">\n"
                . "    $fstImg\n"
                . "</div></div></div></div>\n"
                . "</td></tr>\n";
         if ( $ssExt gt "" ) {
             $result .= "<tr><td valign=top align=left class=footer>$ssExt</td></tr>\n";
         }
         
         $result .= "</table>\n";
    }

    return $result;
}


###########################################################                   
# Search for teacher
#----------------------------------------------------------
sub getFindTeacher {
	
	my $content   = "";
	
	my $srch_name = $shared->get_cgi( "srch_name" );
	my $srch_zip  = $shared->get_cgi( "srch_zip" );
	my $srch_svc  = $shared->get_cgi( "srch_svc" );
	my $srch_head = "Searched:&nbsp;";
	my $srch_par  = 0;
	my $srch_svD  = $shared->getLookupDesc( "SERVICE", $srch_svc );
	
	if ( ( $srch_zip gt "" ) || ( $srch_svc gt "" ) || ( $srch_name gt "" ) ) {
	
	    my $SQL = "SELECT DISTINCT mbr_id, mbr_org_name, mbr_first, mbr_last, mbr_phone, mbr_email,\n"
	            . "mbr_city, mbr_zip, mbr_chapter, mbr_chapter2\n"
	            . "FROM member\n"
    	        . "LEFT JOIN member_service ON mbr_id = mbs_mbr_id\n"
	            . "WHERE mbr_paid_thru_date > curdate()\n";
	    if ( $srch_name gt "" ) {
	        $SQL .= "AND mbr_last LIKE '$srch_name%'\n";
	        $srch_head .= "Last Name starts with <b>$srch_name</b>";
	        $srch_par++;
	    }
	    if ( $srch_zip gt "" ) {
		    $SQL .= "AND mbr_zip LIKE '$srch_zip%'\n";
		    if ( $srch_par > 0 ) {
    	        $srch_head .= ", Zip Code is <b>$srch_zip</b>";
    	    } else {
	            $srch_head .= "Zip Code is <b>$srch_zip</b>";
	        }
		    $srch_par++;
	    }
	    if ( $srch_svc gt "" ) {
		    $SQL .= "AND mbs_service = '$srch_svc'\n";
		    if ( $srch_par > 0 ) {
		        $srch_head .= ", Service is <b>$srch_svD</b>";
		    } else {
		        $srch_head .= "Service is <b>$srch_svD</b>";
		    }
		    $srch_par++;
	    }
	    $SQL .= "ORDER BY mbr_last, mbr_first";
	
	    my @rs   = $shared->getResultSet( $SQL );
	    my $row  = 0;
	    my $row_color;
	    my $fndCnt = 0;
	    
	    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	    $content .= "<tr><td valign=top align=left class=bullet><br>$srch_head<br><br></td></tr>\n";
	    $content .= "<tr><td valign=top align=left bgcolor=\"$color_bg2\">\n";
	    $content .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	    foreach my $mbr_rec ( @rs ) {
	    	my $id   = $mbr_rec->{mbr_id};
	    	my $club = $mbr_rec->{mbr_org_name};
	    	my $dspN = "<b>" . $mbr_rec->{mbr_first} . " " . $mbr_rec->{mbr_last} . "</b>";
	    	my $eml  = $mbr_rec->{mbr_email};
	    	if ( $eml gt "" ) {
	    		$dspN .= " <a href=\"mailto:$eml\"><img src=\"$img_path/mailbutn.gif\" border=0 alt=\"$eml\"></a>";
	    	}
	    	my $phn  = $mbr_rec->{mbr_phone};
	    	my $city = $mbr_rec->{mbr_city};
	    	my $zip  = $mbr_rec->{mbr_zip};
	    	my $chp1 = $mbr_rec->{mbr_chapter};
	    	my $chp2 = $mbr_rec->{mbr_chapter2};
	    	my $chp3 = $mbr_rec->{mbr_chapter3};
	    	
	    	
	    	my $c1D  = $shared->getFieldVal( "chapter", "chp_chapter_desc", "chp_code", $chp1 );
	    	my $c2D  = $shared->getFieldVal( "chapter", "chp_chapter_desc", "chp_code", $chp2 );
	    	my $c3D  = $shared->getFieldVal( "chapter", "chp_chapter_desc", "chp_code", $chp3 );
	    	
	    	my $cLst = "$c1D";
	    	if ( $c2D gt "" ) {
	    		$cLst .= "<br>$c2D";
	    	}
	    	if ( $c3D gt "" ) {
	    		$cLst .= "<br>$c3D";
	    	}
	    	
	    	
	    	if ( $row == 0 ) {
	    		$row_color = $color_bg1;
	    		$row = 1;
	    	} else {
	    		$row_color = $color_bg2;
	    		$row = 0;
	    	}
	    	
	    	$content .= "<tr bgcolor=\"$row_color\"><td valign=top align=left class=tabcell><span class=label><b>$club</b></span><br>$dspN<br>\n";
	    	$content .= "$city, UT $zip<br>$phn</td>\n";
	    	$content .= "<td valign=top align=left class=tabcell><b>Services</b><br>\n";
	    	
	    	my $subSQL = "SELECT lku_description, lku_sort FROM member_service\n"
	    	           . "INNER JOIN lookup ON ( lku_category = 'SERVICE' AND mbs_service = lku_code )\n"
	    	           . "WHERE mbs_mbr_id = $id\n"
	    	           . "ORDER BY lku_sort";
	    	my @subrs  = $shared->getResultSet( $subSQL );
	    	foreach my $sub ( @subrs ) {
	    		my $svc_desc = $sub->{lku_description};
	    		$content .= "$svc_desc<br>\n";
	    	}
	    	
	    	$content .= "</td></tr>\n";
	    	
	    	$fndCnt++;
	    }
	    $content .= "</table>\n";
	    $content .= "</td></tr>\n";
	    $content .= "</table>\n";
	    		
	    if ( $fndCnt == 0 ) {
    	    $content  = "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
	        $content .= "<tr><td valign=top align=left class=bullet><br>No Members found meeting search criteria.</td></tr>\n";
	        $content .= "</table>\n";
	    }
	
    }

    return $content;
	
}


###########################################################                   
# Generate Submenu Links for 
#----------------------------------------------------------
sub getLdrSubMenu {

    my ( $self ) = @_;
    
    my $result = "";
    
    $result .= "<a href=\"$cgi_url?action=view_page&page=leadership#executive\">Executive Board</a>\n";
    $result .= "&nbsp;\|&nbsp;<a href=\"$cgi_url?action=view_page&page=leadership#general\">General Board</a>\n";
    $result .= "&nbsp;\|&nbsp;<a href=\"$cgi_url?action=view_page&page=leadership#chapters\">Local Associations</a>\n";
    $result .= "&nbsp;\|&nbsp;<a href=\"$cgi_url?action=view_page&page=leadership#chairs\">Competitions/Evaluations Chairs</a>\n";
    $result .= "&nbsp;\|&nbsp;<a href=\"$cgi_url?action=view_page&page=leadership#pastpres\">Past Presidents</a>\n";
    
    return $result;
}


###########################################################                   
# Generate Submenu Links for 
#----------------------------------------------------------
sub getLdrContent {

    my ( $self ) = @_;
    
    my $result = &contentHead( "exec", "Executive Board" );
    $result   .= $self->getPhotoList( "EXEC" );
    $result   .= "<br /><br />\n";
    
    $result   .= &contentHead( "general", "Board of Directors" );
    $result   .= $self->getPhotoList( "BOD" );
    $result   .= "<br /><br />\n";
    
#    $result   .= &contentHead( "compcontact", "Competition Contacts" );
#    $result   .= $self->getRegList( "COMPCTC" );
#    $result   .= "<br /><br />\n";
    
    $result   .= &contentHead( "pastpres", "Past Presidents" );
    $result   .= $self->getPastPresList;
    
    return $result;
    
}

###########################################################                   
# Generate Content Subheading
#----------------------------------------------------------
sub contentHead {
    
    my ( $name, $label ) = @_;
    
    my $result = "";
    if ( $name gt "" ) {
        $result .= "<a id=\"$name\"></a>\n";
    }
    $result   .= "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n";
    $result   .= "<tr><td valign=top align=left class=colhead>$label</td></tr>\n";
    $result   .= "</table>\n";
    $result   .= "<br />\n";
    
    return $result;
}


###########################################################                   
# Generate Join Now Link
#----------------------------------------------------------
sub getJoinLink {
    
    my ( $self ) = @_;
    
    my $result = "";
    
    $result   .= "<center>\n";
    $result   .= "<table border=0 cellpadding=0 cellspacing=0 width=100>\n";
    $result   .= "<tr><td>\n";
    $result   .= "<div class=blur><div class=shadow><div class=framework>\n";
    $result   .= "<table border=0 cellpadding=0 cellspacing=0 width=96>\n";
    $result   .= "<tr><td valign=top align=center class=submenu><a href=\"$script_path/umta_reg.cgi?evt_id=5\">Join Now!</a>\n";
    $result   .= "</td></tr>\n";
    $result   .= "</table>\n";
    $result   .= "</div></div></div>\n";
    $result   .= "</td></tr>\n";
    $result   .= "</table>\n";
    $result   .= "</center>\n";
    
    return $result;
}

		
###########################################################                   
# Get Past Presidents List
#----------------------------------------------------------
sub getPastPresList {
	
	my ( $self ) = @_;
	
	my $content = "<table border=0 cellpadding=4 cellspacing=0 width=100%>\n";
	
	my $SQL = "SELECT mbr_first, mbr_last, mbr_cert, mbr_past_pres_years,\n"
	        . "mbr_photo, mbr_id\n"
	        . "FROM member\n"
	        . "WHERE mbr_past_pres_years <> ''\n"
	        . "ORDER BY mbr_past_pres_years";
	
	my @rs   = $shared->getResultSet( $SQL );
	my $fndCnt = 0;
	my $eCnt   = 0;
	my $lts    = time;
    foreach my $mbr_rec ( @rs ) {
    	my $dspN = $mbr_rec->{mbr_first} . " " . $mbr_rec->{mbr_last};
    	
    	my $cert = $mbr_rec->{mbr_cert};
    	if ( $cert gt "" ) {
    		$dspN .= ", $cert";
    	}

    	my $ppy = $mbr_rec->{mbr_past_pres_years};
    	if ( $ppy gt "" ) {
    		$dspN .= "<br>$ppy";
    	}

        if ( $eCnt == 0 ) {
            $content .= "<tr>\n";
        }
        
	    $content .= "<td valign=top align=left class=form width=25%><div style=\"float:left;padding-right:10px;padding-bottom:20px\">\n";
        if ( $mbr_rec->{mbr_photo} eq "N" ) {
            $content   .= "<img src=\"$img_path/member_filler.jpg\" border=\"1\" width=\"80\" height=\"100\">\n";
        } else {
            $content   .= "<img src=\"$photo_url/member_" . $mbr_rec->{mbr_id} . ".jpg?$lts\" border=\"1\" width=\"80\" height=\"100\">\n";
        }
	    $content .= "<br />$dspN\n";
	    $content .= "</div>\n";
	    $content .= "</td>\n";
	    
	    if ( $eCnt == 3 ) {
	        $content .= "</tr>\n";
	        $eCnt     = 0;
	    } else {
	        $eCnt++;
	    }
	    	
	    $fndCnt++;
	}
	
	if ( $eCnt > 0 ) {
	    my $clsp = ( 4 - $eCnt );
	    $content .= "<td valign=top align=left colspan=$clsp>&nbsp;</td></tr>\n";
	}
	    		
    if ( $fndCnt == 0 ) {
	    $content .= "<tr><td valign=top align=left class=mbody colspan=4><b>No Past Presidents Found.</b></td></tr>";
    }
    
    $content .= "</table>\n";
    
    return $content;
}

###########################################################                   
# Get Calendar Heading
#----------------------------------------------------------
sub getEventHead {
    
    my ( $self ) = @_;

    my $curYear     = $shared->getCurrentYear + 0;
    my $nxtYear     = ( $curYear + 1 );
    my $startDate   = $curYear . "-08-01";
    if ( ! $shared->isDatePast( $startDate ) ) {
        $curYear--;
        $nxtYear--;
    }
    
    my $result = "<h2>For $curYear - $nxtYear</h2>\n";
    
    return $result;
}


###########################################################                   
# Get Home Page Membership Stats
#----------------------------------------------------------
sub getHomeStats {
 
    my ( $self ) = @_;
    
    my $result   = "<br><div class=\"blur\"><div class=\"shadow\"><div class=\"framework\">\n"
                 . "<table border=0 cellpadding=2 cellspacing=0 width=100%>\n"
                 . "<tr><td bgcolor=\"$color_bg1\">\n"
                 . "<table border=0 cellpadding=3 cellspacing=0 width=100%>\n"
                 . "<tr bgcolor=\"$color_head1\"><td valign=top align=left class=colhead>Utah Federation Statistics</td></tr>\n"
                 . "</table>\n"
                 . "</td></tr>\n"
                 . "<tr><td class=mbody valign=top align=left bgcolor=\"$color_bg1\">\n";
                 
    my $sqlSC    = "SELECT COUNT(*) FROM chapter";
    my $sqlSM    = "SELECT COUNT(*) FROM member WHERE mbr_paid_thru_date > curdate()";
    my $sqlJM    = "SELECT ( SUM( mbr_chap_stud_cnt) + SUM( mbr_chap2_stud_cnt ) + SUM( mbr_chap3_stud_cnt ) ) AS total_stud_cnt FROM member WHERE mbr_paid_thru_date > curdate()";
    my $SCCnt    = $shared->getSQLCount( $sqlSC );
    my $SMCnt    = $shared->getSQLCount( $sqlSM );
    my $JMCnt    = $shared->getSQLCount( $sqlJM );
    my $TMCnt    = ( $SMCnt + $JMCnt );
    
    my $dspSC    = $shared->fmtNumMask( $SCCnt, "#,###" );
    my $dspSM    = $shared->fmtNumMask( $SMCnt, "#,###" );
    my $dspJM    = $shared->fmtNumMask( $JMCnt, "##,###" );
    my $dspTM    = $shared->fmtNumMask( $TMCnt, "##,###" );
                 
    my $data     = "<table border=0><tr><td valign=top align=left class=form>Senior Clubs:</td><td valign=top align=left class=form><b>$dspSC</b></td></tr>\n"
                 . "<tr><td valign=top align=left class=form>Senior Members:</td><td valign=top align=left class=form><b>$dspSM</b></td></tr>\n"
                 . "<tr><td valign=top align=left class=form>Junior Clubs:</td><td valign=top align=left class=form><b>$dspSM</b></td></tr>\n"
                 . "<tr><td valign=top align=left class=form>Junior Members:</td><td valign=top align=left class=form><b>$dspJM</b></td></tr>\n"
                 . "<tr><td valign=top align=left class=form>Total Members:</td><td valign=top align=left class=form><b>$dspTM</b></td></tr>\n"
                 . "<tr><td valign=top align=left class=form>Year Federated:</td><td valign=top align=left class=form><b>1920</b></td></tr>\n"
                 . "</table>\n";
    
    $result     .= $data . "</td></tr>\n"
                 . "</table>\n"
                 . "</div></div></div>\n";
                 
    return $result;
}

###########################################################                   
# Get Competition List
#----------------------------------------------------------
sub getCompList {

    my ( $self ) = @_;
    
    my $result = "";
    
    my $SQL    = "SELECT cmp_id, cmp_name FROM competition\n"
               . "WHERE ( ( cmp_exp_date > curdate() ) OR ( cmp_exp_date = '0000-00-00' ) )\n"
               . "ORDER BY cmp_order\n";
    my @rs     = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $id = $rec->{cmp_id};
        my $nm = uc( $rec->{cmp_name} );
        
        $result .= "<a href=\"$cgi_url?action=comp_dtl&cmp_id=$id\"><b>$nm</b></a><br><br>\n";
    }
    
    return $result;
}

###########################################################                   
# Get director list
#----------------------------------------------------------
sub getCompDetail {
	
	my ( $self, $cmp_id ) = @_;
	
	my $cmp_rec = $shared->get_rec( "competition", "cmp_id", $cmp_id );
	my $cName   = $cmp_rec->{cmp_name};
	my $ucName  = uc( $cName );
	my $natLbl  = $cmp_rec->{cmp_national_label};
	my $natLnk  = $cmp_rec->{cmp_national_link};
	my $cInfo   = $cmp_rec->{cmp_info};
	my $mID     = $cmp_rec->{cmp_chair_mbr_id};
	my $appLnk  = $cmp_rec->{cmp_app_link};
	my $ruleLnk = $cmp_rec->{cmp_rule_link};
	my $hosted  = $cmp_rec->{cmp_ufmc_hosted};
	my $lbl1    = $cmp_rec->{cmp_label1};
	my $lbl2    = $cmp_rec->{cmp_label2};
	my $lbl3    = $cmp_rec->{cmp_label3};
	my $lnk1    = $cmp_rec->{cmp_link1};
	my $lnk2    = $cmp_rec->{cmp_link2};
	my $lnk3    = $cmp_rec->{cmp_link3};
	my $content = "";

	$content   .= "<center><h2>$ucName</h2><br>\n";
	if ( ( $natLbl gt "" ) && ( $natLnk gt "" ) ) {
	    $content .= "<a href=\"$natLnk\" target=_blank><b>$natLbl</b></a><br>\n";
	}
	$content   .= "</center>\n";
	
	if ( $hosted eq "Y" ) {
	    my $mbr_rec = $shared->get_rec( "member", "mbr_id", $mID );
	    my $mName   = $mbr_rec->{mbr_first} . " " . $mbr_rec->{mbr_last};
	    my $addr    = $mbr_rec->{mbr_address};
	    my $city    = $mbr_rec->{mbr_city};
	    my $state   = $mbr_rec->{mbr_state};
	    my $zip     = $mbr_rec->{mbr_zip};
	    my $phn     = $mbr_rec->{mbr_phone};
	    my $eml     = $mbr_rec->{mbr_email};
	    my $photo   = $mbr_rec->{mbr_photo};
    	my $lts     = time;
	
	    my $aLink   = $appLnk;
	    if ( $appLnk =~ /^DOC\:(.*)/ ) {
	        $aLink  = $self->getDocLink( $1 );
	    } elsif ( $appLnk !~ /^http.*/ ) {
	        $aLink  = "http://" . $appLnk;
	    }
	
	    my $rLink   = $ruleLnk;
	    if ( $ruleLnk =~ /^DOC\:(.*)/ ) {
	        $rLink  = $self->getDocLink( $1 );
	    } elsif ( $ruleLnk !~ /^http.*/ ) {
	        $rLink  = "http://" . $ruleLnk;
	    }

    	$content   .= "<br>\n";
	    $content .= "<table border=0 cellpadding=4 cellspacing=0 width=100%>\n";
	    $content .= "<tr><td valign=top align=left class=label>Pertinent Information:</td>\n";
	    $content .= "<td valign=top align=left class=mbody>$cInfo<br><br></td></tr>\n";
	    $content .= "<tr><td valign=top align=left class=mbody colspan=2>\n";
	    $content .= "For any questions regarding the $cName, please contact:<br></td></tr>\n";
	    $content .= "<tr><td valign=top align=left colspan=2>\n";
	    $content .= "<table border=0 cellpadding=0 cellspacing=0 width=100%>\n";
	    $content .= "<tr><td valign=top align=left class=mbody>\n";
        if ( $photo eq "Y" ) {
            $content   .= "<img src=\"$photo_url/member_" . $mID . ".jpg?$lts\" border=\"0\" width=\"80\" height=\"100\" style=\"float:left;padding-right:10px\">\n";
        }
	    $content .= "<b>$mName</b><br>\n";
	
	    if ( $addr gt "" ) {
	        $content .= "$addr<br>$city, $state, $zip<br>\n";
	    }
	    if ( $phn gt "" ) {
	        $content .= "$phn<br>\n";
	    }
	    if ( $eml gt "" ) {
	        $content .= "<a href=\"mailto:$eml\"><b>$eml</b></a><br>\n";
	    }
	    
	    $content .= "</td>\n";
	    $content .= "<td valign=top align=right class=mbody><br><br>\n";
	
	    if ( $appLnk gt "" ) {
	        $content .= "<a href=\"$aLink\" target=_blank><b>[ Application Form ]</b></a><br>\n";
	    }
	    if ( $ruleLnk gt "" ) {
	        $content .= "<a href=\"$rLink\" target=_blank><b>[ Rules ]</b></a><br>\n";
	    }
	    if ( $lbl1 gt "" ) {
	        $content .= "<a href=\"$lnk1\" target=_blank><b>[ $lbl1 ]</b></a><br>\n";
	    }
	    if ( $lbl2 gt "" ) {
	        $content .= "<a href=\"$lnk2\" target=_blank><b>[ $lbl2 ]</b></a><br>\n";
	    }
	    if ( $lbl3 gt "" ) {
	        $content .= "<a href=\"$lnk3\" target=_blank><b>[ $lbl3 ]</b></a><br>\n";
	    }
	
	    $content .= "</td></tr>\n";
        $content .= "</table>\n";
        $content .= "</td></tr>\n";
        $content .= "</table>\n";
    } else {

        $content .= "<center><i>(CURRENTLY NOT PART OF THE UFMC COMPETITION REPERTOIRE)</i></center><br><br>\n";
        
	    $content .= "<table border=0 cellpadding=4 cellspacing=0 width=100%>\n";
	    $content .= "<tr><td valign=top align=left class=label>Pertinent Information:</td>\n";
	    $content .= "<td valign=top align=left class=mbody>$cInfo<br><br></td></tr>\n";
	    $content .= "</table>\n";
        
    }
    
    $content .= "<br><center><a href=\"$cgi_url?action=view_page&page=competition\"><b>[ Return to List ]</b></a></center>\n";
    
    return $content;
}

###########################################################                   
# Submit / Display Contact Data
#----------------------------------------------------------
sub getContactData {

    my ( $self, $zone3 ) = @_;
    
    my $mType  = $shared->get_cgi( "mbr_type" );
    my $name   = $shared->get_cgi( "c_name" );
    my $email  = $shared->get_cgi( "c_email" );
    my $phone  = $shared->get_cgi( "c_phone" );
    my $addr   = $shared->get_cgi( "c_addr" );
    my $instr  = $shared->get_cgi( "c_instr" );
    my $subj   = $shared->get_cgi( "c_subj" );
    my $mesg   = $shared->get_cgi( "c_msg" );
    my $ipAdd  = "$ENV{REMOTE_ADDR}";
    
    my $result = "";
    my $vMsg   = "";
    my $empty  = 1;
    if ( $name eq "" ) {
        $vMsg  = "Name is required.";
    } else {
        $empty = 0;
    }
    if ( $email =~ /([^\@]+)\@([^\.]+\..+)/ ) {
        $empty = 0;
    } else {
        if ( $vMsg gt "" ) {
            $vMsg .= "  Valid e-mail address is required.";
        } else {
            $vMsg .= "Valid e-mail address is required.";
        }
    }
    
    my $doForm  = 0;
    
    if ( ( $empty == 0 ) && ( $vMsg gt "" ) ) {
        $result .= "<span class=sublabel>$vMsg</span><br><br>\n";
        $doForm  = 1;
    }
    
    if ( $empty ) {
        $doForm  = 1;
    }
    
    if ( $doForm ) {
        
        $result .= "<br><center>\n";
        $result .= "<form method=post action=\"$cgi_url\">\n";
        $result .= "<input type=hidden name=\"action\" value=\"view_page\">\n";
        $result .= "<input type=hidden name=\"page\" value=\"contact\">\n";
        $result .= "<table border=0 cellpadding=0 cellspacing=0 width=500>\n";
        $result .= "<tr><td valign=top align=left>\n";
        $result .= "<div class=biggraybox>\n";
        $result .= "<table border=0 cellpadding=0 cellspacing=0 width=100%>\n";
        $result .= "<tr><td valign=top align=left class=bluehead colspan=2>\n";
        $result .= "Contact Form</td></tr>\n";
        $result .= "<tr><td valign=top align=left class=label>Member Type</td>\n";
        $result .= "<td valign=top align=left class=form><select name=mbr_type class=form>\n";
        
        $result .= $shared->pop_lookup_select( "CTC_MEMB_TYPE", $mType, "ORDER BY lku_sort" );
        
        $result .= "</select></td></tr>\n";
        $result .= "<tr><td valign=top align=left class=label>Name</td>\n";
        $result .= "<td valign=top align=left class=form><input name=c_name value=\"$name\" size=40 maxlength=60 class=form>&nbsp;<font color=\"red\">*</font></td></tr>\n";
        $result .= "<tr><td valign=top align=left class=label>E-mail</td>\n";
        $result .= "<td valign=top align=left class=form><input name=c_email value=\"$email\" size=40 maxlength=80 class=form>&nbsp;<font color=\"red\">*</font></td></tr>\n";
        $result .= "<tr><td valign=top align=left class=label>Address</td>\n";
        $result .= "<td valign=top align=left class=form><input name=c_addr value=\"$addr\" size=40 maxlength=80 class=form></td></tr>\n";
        $result .= "<tr><td valign=top align=left class=label>Phone</td>\n";
        $result .= "<td valign=top align=left class=form><input name=c_phone value=\"$phone\" size=20 maxlength=20 class=form></td></tr>\n";
        $result .= "<tr><td valign=top align=left class=label>Instrument</td>\n";
        $result .= "<td valign=top align=left class=form><input name=c_instr value=\"$instr\" size=20 maxlength=30 class=form></td></tr>\n";
        $result .= "<tr><td valign=top align=left class=label>Subject</td>\n";
        $result .= "<td valign=top align=left class=form><select name=c_subj class=form>\n";
        
        $result .= $shared->pop_lookup_select( "CTC_SUBJECT", $subj, "ORDER BY lku_sort" );
        
        $result .= "</select></td></tr>\n";
        $result .= "<tr><td valign=top align=left class=label>Message</td>\n";
        $result .= "<td valign=top align=left class=form><textarea name=c_msg rows=5 cols=40 class=form wrap>$mesg</textarea></td></tr>\n";
        $result .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
        $result .= "<td valign=top align=left class=form><input type=submit value=\"Submit Now\" class=form><br><font color=\"red\">* - Required Values</font></td></tr>\n";
        $result .= "</table>\n";
        $result .= "</div>\n";
        $result .= "</td></tr>\n";
        $result .= "</table>\n";
        $result .= "</form>\n";
        $result .= "</center>\n";
        
    } else {
        
        $self->notifyContact( $mType, $name, $email, $addr, $phone, $instr, $subj, $mesg );
        
        $name     =~ s/'/\\'/g;
        $addr     =~ s/'/\\'/g;
        $instr    =~ s/'/\\'/g;
        $mesg     =~ s/'/\\'/g;
        
        my $insert = "INSERT INTO contact_log ( ctl_date, ctl_ip_add, ctl_mbr_type, ctl_name, ctl_email, ctl_phone,\n"
                   . "ctl_addr, ctl_instr, ctl_subject, ctl_message ) VALUES ( now(), '$ipAdd', '$mType', '$name', '$email', '$phone',\n"
                   . "'$addr', '$instr', '$subj', '$mesg' )";
        
        $shared->doSQLUpdate( $insert );
        
        $result    = "<span class=sublabel>$zone3</span><br><br>";
        
    }
    
    return $result;

}


###########################################################                   
# Send notification to contact e-mail address
#----------------------------------------------------------
sub notifyContact {
	
	my ( $self, $mType, $cName, $cEml, $addr, $cPhn, $instr, $cSubj, $cMsg ) = @_;

    my $config        = new config;
	
	my $help_email    = $config->get_help_email;
	my $mailhost      = $config->get_mailhost;
	my $mailadmin     = $config->get_admin_email;
	my $mailfrom      = $config->get_mailfrom;

    my $member_type   = $shared->getLookupDesc( "CTC_MEMB_TYPE", $mType );
    my $subject       = $shared->getLookupDesc( "CTC_SUBJECT", $cSubj );
	
	my $email_body = "";
	$email_body   .= "The following contact request has been submitted on the UFMC\n";
	$email_body   .= "website.  See details below:\n";
	$email_body   .= "-------------------------------------------------------------------------\n";
	$email_body   .= "Membership Type: $member_type\n";
	$email_body   .= "           Name: $cName\n";
	$email_body   .= "         E-mail: $cEml\n";
	$email_body   .= "        Address: $addr\n";
	$email_body   .= "          Phone: $cPhn\n";
	$email_body   .= "     Instrument: $instr\n\n";
	$email_body   .= "MESSAGE\n";
	$email_body   .= "--------------------------------------------------------------------------\n";
	$email_body   .= "$cMsg\n\n";

	
    my $smtp = Net::SMTP->new( $mailhost );
    $smtp->mail( $help_email );
    $smtp->to( $help_email );
    $smtp->data();
    $smtp->datasend( "To: $help_email\n" );
    $smtp->datasend( "Reply-To: $mailadmin\n" );
    $smtp->datasend( "From: $mailfrom <$mailadmin>\n" );
    $smtp->datasend( "Subject: UFMC - $subject\n" );
    $smtp->datasend( "\n" );
    $smtp->datasend( $email_body );
    $smtp->dataend();
    $smtp->quit;
        

} 

###########################################################                   
# Display Board of Director Member Information
#----------------------------------------------------------
sub display_bodmem {

    my ( $self, $brd_id ) = @_;
    
    my $SQL = "SELECT mbr_first, mbr_last, mbr_address, mbr_city, mbr_state, mbr_zip, mbr_phone, mbr_email\n"
            . "FROM board\n"
            . "LEFT JOIN member ON brd_mbr_id = mbr_id\n"
            . "WHERE brd_id = $brd_id";
        
    my $result = "";
        
    my $rec = $shared->getResultRec( $SQL );
    if ( $rec->{mbr_last} gt "" ) {
        my $nm  = $rec->{mbr_first} . " " . $rec->{mbr_last};
        my $adr = $rec->{mbr_address};
        my $csz = $rec->{mbr_city} . ", " . $rec->{mbr_state} . "  " . $rec->{mbr_zip};
        my $eml = $rec->{mbr_email};
        my $phn = $rec->{mbr_phone};
        
        $result .= "$nm<br>";
        if ( $adr gt "" ) {
            $result .= "$adr<br>$csz<br>";
        }
        if ( $phn gt "" ) {
            $result .= "$phn<br>";
        }
        if ( $eml gt "" ) {
            $result .= "<a href=\"mailto:$eml\">$eml</a><br>";
        }
    } else {
        $result .= "<i>Unassigned</i><br>";
    }
    
    return $result;
    
}


###########################################################                   
# Generate and return site table
#----------------------------------------------------------
sub display_table {

    my ( $self, $tbl_code ) = @_;
    
    my $rec    = $shared->getResultRec( "SELECT * FROM table_def WHERE tbl_code = '$tbl_code'" );
    my $tbl_id = $rec->{tbl_id};
    
    my $result = $sitetables->genTable( $tbl_id );
    
    return $result;
    
}


1;
