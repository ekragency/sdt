#!/usr/bin/perl
########################################
# This package provides shared site specific
# routines and other functions.
#
# Created: 1/20/2014 - Eric Huber
###########################################################################
# Copyright 2014, P. Eric Huber
###########################################################################
use config;
use shared;

my $shared;
my $src_path;
my $color_head1;
my $color_head2;
my $color_bg1;
my $color_bg2;
my $color_bg3;
my $img_path;
my $script_path;
my $site_title;
my $java_msg = "";
my $page_dtls;

my $debug = 0;

package sdt;

# -------------------------------------------------------------------------------------
# The constructor   
# -------------------------------------------------------------------------------------
sub new {
    
    my ( $class, $sh_in ) = @_;
    
    my $self = {};
    
    my $config    = new config;
    $src_path     = $config->get_src_path;
    my $app_ver   = $config->get_app_ver;
    my (%colors)  = $config->get_colors;
    $color_head1  = $colors{ "HEAD1" };
    $color_head2  = $colors{ "HEAD2" };
    $color_bg1    = $colors{ "BG1" };
    $color_bg2    = $colors{ "BG2" };
    $color_bg3    = $colors{ "BG3" };
    $img_path     = $config->get_img_path;
    $script_path  = $config->get_script_path;
    $site_title   = $config->get_app_name;
    $debug        = $config->get_debug;
    $page_dtls->{template}    = "admin.html";
    $page_dtls->{color_head1} = $color_head1;
    $page_dtls->{color_head2} = $color_head2;
    $page_dtls->{color_bg1}   = $color_bg1;
    $page_dtls->{color_bg2}   = $color_bg2;
    $page_dtls->{color_bg3}   = $color_bg3;
    $page_dtls->{color_error} = $color_error;
    $page_dtls->{app_ver}     = $app_ver;
    $page_dtls->{css}         = "style.css";

    
    bless ( $self, $class );

    $shared  = $sh_in;
    
    return $self;
}
# -------------------------------------------------------------------------------------
	    
###########################################################                   
# Parse CSV File.
#----------------------------------------------------------
sub parseTXTFile {

    my ( $self, $fileIn ) = @_;
    
    my $retHash;
    $retHash->{return_code} = "SUCCESS";
    
    my $p_ctxcnt = 0;
    my $p_repcnt = 0;
    my $p_xfrcnt = 0;
    my $p_100cnt = 0;
    my $p_95cnt  = 0;
    my $p_85cnt  = 0;
    my $p_75cnt  = 0;
    my $p_50cnt  = 0;
    my $p_newcnt = 0;
    my $p_abase  = 0;
    my $p_alrn   = 0;
    my $totCnt   = 0;

	open ( FILE, $fileIn ) or $retHash->{return_code} = "ERROR";
	my @FILE = <FILE>;
	close( FILE );

    if ( $retHash->{return_code} eq "SUCCESS" ) {
        
        my $procLines = 0;
        foreach my $line ( @FILE ) {
 
            my @fld   = split( "\t", $line );
            my $asize = @fld;
    	
    	    my $control = $fld[0];

            if ( ( $control eq "Totals" ) || ( $control eq "Totales" ) ) {
                $procLines = 1;
            } elsif ( ( $control eq "File Details" ) || ( $control eq "Detalles del archivo" ) ) {
                $procLines = 0;
            }
            
 	        if ( $procLines == 1 ) {
    	    
    	        if ( $asize > 3 ) {
    	            my $cType    = $fld[1];
    	            my $wCnt     = $fld[3];
            
                    if ( ( $cType eq "Context Match" ) || ( $cType eq "Coincidencia de contexto" ) ) {
                        $p_ctxcnt += $wCnt;
                        $totCnt   += $wCnt;
                    } elsif ( ( $cType eq "Repetitions" ) || ( $cType eq "Repeticiones" ) ) {
                        $p_repcnt += $wCnt;
                        $totCnt   += $wCnt;
                    } elsif ( ( $cType eq "Cross-file Repetitions" ) || ( $cType eq "Repeticiones entre archivos" ) ) {
                        $p_xfrcnt += $wCnt;
                        $totCnt   += $wCnt;
                    } elsif ( $cType eq "100\%" ) {
                        $p_100cnt += $wCnt;
                        $totCnt   += $wCnt;
                    } elsif ( $cType eq "95\% - 99\%" ) {
                        $p_95cnt += $wCnt;
                        $totCnt  += $wCnt;
                    } elsif ( $cType eq "85\% - 94\%" ) {
                        $p_85cnt += $wCnt;
                        $totCnt  += $wCnt;
                    } elsif ( $cType eq "75\% - 84\%" ) {
                        $p_75cnt += $wCnt;
                        $totCnt  += $wCnt;
                    } elsif ( $cType eq "50\% - 74\%" ) {
                        $p_50cnt += $wCnt;
                        $totCnt  += $wCnt;
                    } elsif ( ( $cType =~ /^New.*/ ) || ( $cType =~ /^Nuevas.*/ ) ) {
                        $p_newcnt+= $wCnt;
                        $totCnt  += $wCnt;
                    } elsif ( ( $cType eq "AdaptiveMT Baseline" ) || ( $cType eq "Motor b�sico de AdaptiveMT" ) ) {
                        $p_abase += $wCnt;
                        $totCnt  += $wCnt;
                    } elsif ( ( $cType eq "AdaptiveMT with Learnings" ) || ( $cType eq "AdaptiveMT con aprendizaje" ) ) {
                        $p_alrn  += $wCnt;
                        $totCnt  += $wCnt;
                    }
                
   	            }
   	        }
        }
    }
    
   	if ( $totCnt > 0 ) {
   	    $retHash->{cnt_ctx}  = $p_ctxcnt;
 	    $retHash->{cnt_rep}  = $p_repcnt;
 	    $retHash->{cnt_xfr}  = $p_xfrcnt;
   	    $retHash->{cnt_100}  = $p_100cnt;
   	    $retHash->{cnt_95}   = $p_95cnt;
   	    $retHash->{cnt_85}   = $p_85cnt;
   	    $retHash->{cnt_75}   = $p_75cnt;
   	    $retHash->{cnt_50}   = $p_50cnt;
   	    $retHash->{cnt_none} = $p_newcnt;
   	    $retHash->{cnt_abas} = $p_abase;
   	    $retHash->{cnt_alrn} = $p_alrn;
   	    $retHash->{cnt_wtot} = $totCnt;
  	}
    
    return $retHash;
}	    


###########################################################                   
# Calc Amounts based on rates and counts passed
#----------------------------------------------------------
sub calcAnalysis {

    my ( $self, $hashIn, $aType, $rate, $hRate ) = @_;
    
    my $wph    = $shared->getConfigInt( "ADM_WPH" );
    my $wt_ctx = ( $shared->getConfigInt( "ADM_ANALCTXMATCHPCT" ) / 100 );
    my $wt_rep = ( $shared->getConfigInt( "ADM_ANALREPPCT" ) / 100 );
    my $wt_xfr = ( $shared->getConfigInt( "ADM_ANALXFILREPPCT" ) / 100 );
    my $wt_100 = ( $shared->getConfigInt( "ADM_ANAL100PCT" ) / 100 );
    my $wt_95  = ( $shared->getConfigInt( "ADM_ANAL95PCT" ) / 100 );
    my $wt_85  = ( $shared->getConfigInt( "ADM_ANAL85PCT" ) / 100 );
    my $wt_75  = ( $shared->getConfigInt( "ADM_ANAL75PCT" ) / 100 );
    my $wt_50  = ( $shared->getConfigInt( "ADM_ANAL50PCT" ) / 100 );
    my $wt_new = ( $shared->getConfigInt( "ADM_ANALNEWPCT" ) / 100 );
    my $wt_abs = ( $shared->getConfigInt( "ADM_ANALAMTBASEPCT" ) / 100 );
    my $wt_aln = ( $shared->getConfigInt( "ADM_ANALAMTLEARNPCT" ) / 100 );

    if ( $hashIn->{wht_ctx} gt "" ) {
        $wt_ctx = ( $hashIn->{wht_ctx} / 100 );
    }
    if ( $hashIn->{wht_rep} gt "" ) {
        $wt_rep = ( $hashIn->{wht_rep} / 100 );
    }
    if ( $hashIn->{wht_xfr} gt "" ) {
        $wt_xfr = ( $hashIn->{wht_xfr} / 100 );
    }
    if ( $hashIn->{wht_100} gt "" ) {
        $wt_100 = ( $hashIn->{wht_100} / 100 );
    }
    if ( $hashIn->{wht_95} gt "" ) {
        $wt_95  = ( $hashIn->{wht_95} / 100 );
    }
    if ( $hashIn->{wht_85} gt "" ) {
        $wt_85  = ( $hashIn->{wht_85} / 100 );
    }
    if ( $hashIn->{wht_75} gt "" ) {
        $wt_75  = ( $hashIn->{wht_75} / 100 );
    }
    if ( $hashIn->{wht_50} gt "" ) {
        $wt_50  = ( $hashIn->{wht_50} / 100 );
    }
    if ( $hashIn->{wht_none} gt "" ) {
        $wt_new = ( $hashIn->{wht_none} / 100 );
    }
    if ( $hashIn->{wht_abas} gt "" ) {
        $wt_abs = ( $hashIn->{wht_abas} / 100 );
    }
    if ( $hashIn->{wht_alrn} gt "" ) {
        $wt_aln = ( $hashIn->{wht_alrn} / 100 );
    }



    my $result = 0;
    
    if ( $aType eq "T" ) {
        
        my $amtctx  = $shared->round( $hashIn->{cnt_ctx} * $wt_ctx * $rate, 2 );
        my $amtrep  = $shared->round( $hashIn->{cnt_rep} * $wt_rep * $rate, 2 );
        my $amtxfr  = $shared->round( $hashIn->{cnt_xfr} * $wt_xfr * $rate, 2 );
        my $amt100  = $shared->round( $hashIn->{cnt_100} * $wt_100 * $rate, 2 );
        my $amt95   = $shared->round( $hashIn->{cnt_95} * $wt_95 * $rate, 2 );
        my $amt85   = $shared->round( $hashIn->{cnt_85} * $wt_85 * $rate, 2 );
        my $amt75   = $shared->round( $hashIn->{cnt_75} * $wt_75 * $rate, 2 );
        my $amt50   = $shared->round( $hashIn->{cnt_50} * $wt_50 * $rate, 2 );
        my $amtnew  = $shared->round( $hashIn->{cnt_none} * $wt_new * $rate, 2 );
        my $amtabs  = $shared->round( $hashIn->{cnt_abas} * $wt_abs * $rate, 2 );
        my $amtaln  = $shared->round( $hashIn->{cnt_alrn} * $wt_aln * $rate, 2 );
        my $amtTot  = ( $amtctx + $amtrep + $amtxfr + $amt100 + $amt95 + $amt85 + $amt75 + $amt50 + $amtnew + $amtabs + $amtaln );
        if ( $amtTot < $hRate ) {
            $amtTot = ( $hRate + 0 );
        }
        $result = $amtTot;
    } elsif ( $aType eq "E" ) {
        my $amtEdit = $shared->round( $hashIn->{cnt_wtot} * $rate, 2 );
        if ( $amtEdit < $hRate ) {
            $amtEdit = ( $hRate + 0 );
        }
        $result      = $amtEdit;
    } elsif ( $aType eq "P" ) {
        my $hours = 0;
        my $wTot  = $hashIn->{cnt_wtot};
        if ( ( $wTot / $wph ) == int( $wTot / $wph ) ) {
            $hours = int( $wTot / $wph );
        } else {
            $hours = int( ( $wTot / $wph ) + 1 );
        }        
        my $amtPF = $shared->round( $hours * $hRate, 2 );
        $result   = $amtPF;
    } elsif ( $aType eq "M" ) {
        my $amtMTPE = $shared->round( $hashIn->{cnt_wtot} * $rate, 2 );
        if ( $amtMTPE < $hRate ) {
            $amtMTPE = ( $hRate + 0 );
        }
        $result      = $amtMTPE;
    }
        

    return $result;
}

###########################################################                   
# Update the total quote amount from line items
#----------------------------------------------------------
sub updateQuote {

    my ( $self, $qte_id ) = @_;

    my $qAmt = 0;
    my $pAmt = 0;
    
    my $SQL  = "SELECT qtl_id, qtl_type, qtl_amount, qtl_rate FROM quote_line WHERE qtl_qte_id = $qte_id ORDER BY qtl_type";
    my @rs   = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $qlID = $rec->{qtl_id};
        my $type = $rec->{qtl_type};
        my $lAmt = ( $rec->{qtl_amount} + 0 );
        my $pct  = ( $rec->{qtl_rate} + 0 );
        
        if ( $type eq "E-DSC" ) {
            $qAmt -= $lAmt;
        } else {
            $qAmt += $lAmt;
        }
        
        if ( $type eq "F-PCT" ) {
            my $lineAmt = $qAmt * ( $pct / 100 );
            my $upLine  = "UPDATE quote_line SET qtl_amount = $lineAmt WHERE qtl_id = $qlID";
            $shared->doSQLUpdate( $upLine );
            $pAmt += $lineAmt;
        }
    }
    
    if ( $pAmt > 0 ) {
        $qAmt -= $pAmt;
    }
    
    my $update = "UPDATE quote SET qte_tot_amount = $qAmt WHERE qte_id = $qte_id";
    $shared->doSQLUpdate( $update );
    
}
   
###########################################################                   
# Get User List as selection
#----------------------------------------------------------
sub getUserSelect {
	
    my ( $self, $uID ) = @_;
    
    my $result = "<option value=\"0\"></option>";
    
    my $SQL = "SELECT usr_id, usr_name FROM user WHERE usr_active = 'Y' ORDER BY usr_name";
    my @rs  = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        my $id = $rec->{usr_id};
        my $nm = $rec->{usr_name};
        if ( $uID eq $id ) {
            $result .= "<option selected value=\"$id\">$nm</option>";
        } else {
            $result .= "<option value=\"$id\">$nm</option>";
        }
    }
    
    return $result;
} 
        

1;
