#!/usr/bin/perl
########################################
# This package provides shared database
# and CGI routines and other functions.
#
# Created: 06/30/2015 - Eric Huber
###########################################################################
# Copyright 2004, P. Eric Huber
###########################################################################
use Net::SMTP;

use config;
use shared;

my $shared;
my $src_path;
my $color_head1;
my $color_head2;
my $color_bg1;
my $color_bg2;
my $color_bg3;
my $img_path;
my $script_path;
my $site_title;
my $grace_logins;
my $lock_time;
my $lig_id = 0;
my $app_mode;
my $java_msg = "";
my $page_dtls;

my $debug = 0;

package ligauth;

# -------------------------------------------------------------------------------------
# The constructor   
# -------------------------------------------------------------------------------------
sub new {
    
    my ( $class, $sh_in ) = @_;
    
    my $self = {};
    
    my $config    = new config;
    $src_path     = $config->get_src_path;
    my $app_ver   = $config->get_app_ver;
    $img_path     = $config->get_img_path;
    $script_path  = $config->get_script_path;
    $site_title   = $config->get_lp_name;
    $debug        = $config->get_debug;
    $app_mode     = $config->get_app_mode;
    $page_dtls->{template}    = "lp.html";
    $page_dtls->{app_ver}     = $app_ver;
    $page_dtls->{css}         = "lp-style.css";
    if ( $app_mode eq "TEST" ) {
        $page_dtls->{template} = "lp-test.html";
    }
    
    bless ( $self, $class );

    $shared  = $sh_in;
    
    $lig_id = $self->authenticate;
    
    return $self;
}
# -------------------------------------------------------------------------------------

sub authenticate {     # Purpose: return either 0 or 1 given password

    my ($self)     = @_;
    my $ret_val    = 0;
    my $cgi_id     = $shared->get_cgi( "lpu_login" );
    my $cgi_pw     = $shared->get_cgi( "lpu_passwd" );
    my $action     = $shared->get_cgi( "action" );
    my $config     = new config;
    my $usrid_cky  = $config->get_lpid_cookie;
    my $usrpw_cky  = $config->get_lppw_cookie;
    my $loginnow   = 0;
    my $foundLogin = 0;
    
    if ( $action eq "logout" ) {
    	
    	$shared->store_cookie( $usrpw_cky, "delete\|-1d" );
    	
    } else {
    
        if ( $cgi_pw eq "" ) {
        	$cgi_id = $shared->get_cookie( $usrid_cky );
    	    $cgi_pw = $shared->get_cookie( $usrpw_cky ); 
        } else {
            $shared->store_cookie( $usrid_cky, $cgi_id );
    	    $shared->store_cookie( $usrpw_cky, $cgi_pw );
    	    $loginnow = 1;
        }
        
        my $SQL = "SELECT lig_id, lig_password\n"
                . "FROM linguist\n"
                . "WHERE lig_login = '$cgi_id'\n";
        my @rs  = $shared->getResultSet( $SQL );
        foreach my $usr_rec ( @rs ) {
        	$foundLogin = 1;
        	my $db_id = $usr_rec->{lig_id};
        	my $db_pw = $usr_rec->{lig_password};

            if ( ( $db_pw eq $cgi_pw ) && ( $cgi_pw gt "" ) ) {
                $ret_val = $db_id;
                if ( $loginnow ) {
                  	$shared->add_log( "LPACCESS", "Successful Lingust Portal Login for user - ($cgi_id).", 0 );
                    my $up1 = "UPDATE linguist SET lig_last_login = now() WHERE lig_id = $db_id";
                    $shared->doSQLUpdate( $up1 );
                }
            } else {
                if ( $loginnow ) {
                    $java_msg = "Login Failed!  Invalid Password Specified.";
                    $shared->add_log( "LPDNYPASS", "Invalid Linguist Portal Password Specified - ($cgi_id).", 0 );
                }
            }
            last;
        }
        
        if ( ( $foundLogin == 0 ) && ( $loginnow ) ) {
        	$java_msg = "Login Failed!  Invalid User Specified.";
        	$shared->add_log( "LPDNYUSER", "Invalid Linguist Portal User Specified - ($cgi_id).", 0 );
        }
        
    }
        
    return $ret_val;
}

###########################################################                   
# Set java_msg variable
#----------------------------------------------------------
sub setJavaMsg {
    
    my ( $self, $msg ) = @_;
    
    $java_msg = $msg;
    
}

###########################################################                   
# Return current usr_id
#----------------------------------------------------------
sub getCurrentUser {
	my ( $self ) = @_;
	return $lig_id;
}

###########################################################                   
# Return name of current user
#----------------------------------------------------------
sub getCurrentUserName {
	my ( $self ) = @_;
	my $usr_name = "";
	if ( $lig_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "linguist", "lig_id", $lig_id );
	    $usr_name   = $usr_rec->{lig_first_name} . " " . $usr_rec->{lig_last_name};
	}
	return $usr_name;
}

###########################################################                   
# Return name of current user
#----------------------------------------------------------
sub getCurrentUserLogin {
	my ( $self ) = @_;
	my $usr_login = "";
	if ( $lig_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "linguist", "lig_id", $lig_id );
	    $usr_login   = $usr_rec->{lig_login};
	}
	return $usr_login;
}

###########################################################                   
# Return name of current user
#----------------------------------------------------------
sub getCurrentUserPass {
	my ( $self ) = @_;
	my $usr_pass = "";
	if ( $lig_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "linguist", "lig_id", $lig_id );
	    $usr_pass   = $usr_rec->{lig_password};
	}
	return $usr_pass;
}

###########################################################                   
# Return currency of current user
#----------------------------------------------------------
sub getCurrentUserCurrency {
	my ( $self ) = @_;
	my $usr_curr = "";
	if ( $lig_id > 0 ) {
	    my $usr_rec = $shared->get_rec( "linguist", "lig_id", $lig_id );
	    $usr_curr   = $usr_rec->{lig_currency};
	}
	return $usr_curr;
}

###########################################################                   
# User Login
#----------------------------------------------------------
sub login {

    my ( $self, $usr_url, $noMsg ) = @_;
    
   	my $pg_content = "";
    $pg_content   .= "<form name=logonform method=post action=\"$usr_url\">\n";
    $pg_content   .= "<input type=hidden name=action value=\"login\">\n";
    $pg_content   .= "<table border=0 cellpadding=5 cellspacing=0 width=200>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>User ID:</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input name=lpu_login value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>Password:</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input type=password name=lpu_passwd value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td>&nbsp;</td><td valign=top align=left class=form><input type=submit value=\" Login Now \" class=form></td></tr>\n";
    $pg_content   .= "</table>\n";
    $pg_content   .= "</form>\n";
    $pg_content   .= "<a href=\"$usr_url?action=reset_pass_form\" class=form>Forgot Password?</a><br><br>\n";
    $pg_content   .= "<script language=\"JavaScript\">\n";
    $pg_content   .= "<!--\n";
    $pg_content   .= "document.logonform.lpu_login.focus();\n";
    $pg_content   .= "//-->\n";
    $pg_content   .= "</script>\n";
    
        
    $page_dtls->{page_head}    = "System Access";
    $page_dtls->{html_title}   = "$site_title - Access";
    $page_dtls->{nav_buttons}  = "&nbsp;";
    $page_dtls->{page_body}    = $pg_content;
    
    if ( $noMsg eq "Y" ) {
    	$java_msg = "";
    }
        
    $shared->page_gen( $page_dtls, $java_msg );
    
}
    	


###########################################################                   
# Logout Page
#----------------------------------------------------------
sub logout {

    my ( $self, $usr_url ) = @_;

    my $pg_content;    
    $pg_content  = "<center>\n";
    $pg_content .= "<form method=post action=\"$usr_url\">\n";
    $pg_content .= "<table border=0 cellpadding=5 cellspacing=0 width=\"90%\">\n";
    $pg_content .= "<tr><td valign=top align=left class=form>\n";
    $pg_content .= "You are now logged out of $site_title</td></tr>\n";
    $pg_content .= "<tr><td valign=top align=center class=form>\n";
    $pg_content .= "<input type=submit value=\" Login Again \" class=form></td></tr>\n";
    $pg_content .= "</table>\n";
    $pg_content .= "</form>\n";
    $pg_content .= "</center>\n";
    
    $page_dtls->{page_head}    = "System Access";
    $page_dtls->{html_title}   = "$site_title - Access";
    $page_dtls->{page_body}    = $pg_content;
        
    $shared->page_gen( $page_dtls );
    
}


###########################################################                   
# Change Password Form
#----------------------------------------------------------
sub chg_pass_form {

    my ( $self, $usr_url ) = @_;
    
   	my $pg_content = "";
    $pg_content   .= "<form method=post action=\"$usr_url\">\n";
    $pg_content   .= "<input type=hidden name=action value=\"chg_pass\">\n";
    $pg_content   .= "<table border=0 cellpadding=5 cellspacing=0 width=300>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>New Password:</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input type=password name=new_pass value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>Confirm (Retype):</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input type=password name=new_conf value=\"\" size=15 class=form></td></tr>\n";
    $pg_content   .= "<tr><td>&nbsp;</td><td valign=top align=left class=form><input type=submit value=\" Change Now \" class=form>&nbsp;\n";
    $pg_content   .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$usr_url'\" class=form></td></tr>\n";
    $pg_content   .= "</table>\n";
    $pg_content   .= "</form>\n";
        
    $page_dtls->{page_head}    = "Change Password";
    $page_dtls->{html_title}   = "$site_title - Change Password";
    $page_dtls->{page_body}    = $pg_content;
        
    $shared->page_gen( $page_dtls, $java_msg );
    
}

###########################################################                   
# Change Password Form
#----------------------------------------------------------
sub chg_pass {

    my ( $self, $usr_url ) = @_;
    
    my $result = "";
    
    my $new_pass   = $shared->get_cgi( "new_pass" );
    my $new_conf   = $shared->get_cgi( "new_conf" );
    my $config     = new config;
    my $usrpw_cky  = $config->get_lppw_cookie;
    
    if ( $new_pass gt "" ) {
    	if ( $new_pass eq $new_conf ) {
    		
    		my $update = "UPDATE linguist SET lig_password = '$new_pass' WHERE lig_id = $lig_id";
    		$shared->doSQLUpdate( $update );
    		
    	    $shared->store_cookie( $usrpw_cky, $new_pass );
    	    
    	    $result = "Password Changed";
    	    
    	    my $uLogin = $self->getCurrentUserLogin;
           	$shared->add_log( "LPPWCHG", "Linguist Portal User password updated - ($uLogin).", 0 );

    		
        } else {
        	$result = "New password and confirmation do not match.  Please retype your new password exactly the same way in the confirmation field.";
        	$self->chg_pass_form( $usr_url );
        }
    } else {
    	$result = "Password cannot be blank.";
    	$self->chg_pass_form( $usr_url );
    }
    
    return $result;
}

###########################################################                   
# Change Password Form
#----------------------------------------------------------
sub reset_pass_form {

    my ( $self, $usr_url ) = @_;
    
   	my $pg_content = "";
    $pg_content   .= "<form method=post action=\"$usr_url\">\n";
    $pg_content   .= "<input type=hidden name=action value=\"reset_pass\">\n";
    $pg_content   .= "<table border=0 cellpadding=5 cellspacing=0 width=500>\n";
    $pg_content   .= "<tr><td align=left valign=top class=label>E-mail Address</td>\n";
    $pg_content   .= "<td align=left valign=top class=form><input name=eml_input value=\"\" size=40 class=form></td></tr>\n";
    $pg_content   .= "<tr><td>&nbsp;</td><td valign=top align=left class=form><input type=submit value=\" Reset Now \" class=form>&nbsp;\n";
    $pg_content   .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$usr_url'\" class=form></td></tr>\n";
    $pg_content   .= "</table>\n";
    $pg_content   .= "</form>\n";
        
    $page_dtls->{page_head}    = "Forgot Password - Reset";
    $page_dtls->{html_title}   = "$site_title - Reset Password";
    $page_dtls->{page_body}    = $pg_content;
        
    $shared->page_gen( $page_dtls, $java_msg );
    
}

###########################################################                   
# Change Password Form
#----------------------------------------------------------
sub reset_pass {

    my ( $self, $usr_url ) = @_;
    
    my $result = "";
    
    my $eml_input  = $shared->get_cgi( "eml_input" );

    my $SQL = "SELECT * FROM linguist WHERE lig_email = '$eml_input'";
    my $rec = $shared->getResultRec( $SQL );
    if ( defined( $rec ) ) {
        my $l_id  = $rec->{lig_id};
        my $l_uid = $rec->{lig_login};
        if ( $l_uid gt "" ) {
            my $newPass = &gen_pass_strong;
            $shared->doSQLUpdate( "UPDATE linguist SET lig_password = '$newPass' WHERE lig_id = $l_id" );
            $self->notify_pass_reset( $eml_input, $l_uid, $newPass );
            $result = "Password reset and notification sent";
        } else {
            $result = "Access to Linguist Portal has not been setup.  Contact Administrator.";
        }
    } else {
        $result = "Access to Linguist Portal has not been setup.  Contact Administrator. (Invalid E-mail)";
    }
    
    return $result;
}

###########################################################                   
# Generate Strong Password
#----------------------------------------------------------
sub gen_pass_strong {
    
    my @part1 = ( "Ejs1", "wDo4p", "xQ3L", "n8S", "vT9L", "hSH0", "Zr5f", "meCp7", "bTYk", "dkW6" );
    my @part2 = ( "\@", "\$", "*", ";", "^", "!", "(", ")", "-", "+" );
    my @part3 = ( "W7Da", "bSt5", "Mr8w", "Q2Sf", "Et5w", "L7jq", "N2N5", "Ir6c", "enP3", "N9ox" );
 
    my $r1    = int( rand( 10 ) );
    my $r2    = int( rand( 10 ) );
    my $r3    = int( rand( 10 ) );
   
    my $newPass  = $part1[ $r1 ] . $part2[ $r2 ] . $part3[ $r3 ];
   
    return $newPass;
}

###########################################################                   
# Send notification linguist for password reset
#----------------------------------------------------------
sub notify_pass_reset {
	
	my ( $self, $n_email, $n_user, $n_pass ) = @_;

    my $config        = new config;
	
	my $help_email    = $config->get_help_email;
	my $mailhost      = $config->get_mailhost;
	my $mailadmin     = $config->get_help_email;
	my $mailfrom      = "SDT Admin";

	my $email_body = "";
	$email_body   .= "You have requested a password reset on the Linguist Portal for SameDay\n";
	$email_body   .= "Translations.  Your user ID and new password appear below.  We recommend\n";
	$email_body   .= "changing your password to something more familiar once you login.\n\n";
	$email_body   .= "  User ID: $n_user\n";
	$email_body   .= " Password: $n_pass\n\n";

	
    my $smtp = Net::SMTP->new( $mailhost );
    $smtp->mail( $mailadmin );
    $smtp->to( $n_email );
    $smtp->data();
    $smtp->datasend( "To: $n_email\n" );
    $smtp->datasend( "Reply-To: $mailadmin\n" );
    $smtp->datasend( "From: $mailfrom <$mailadmin>\n" );
    $smtp->datasend( "Subject: SDT Linguist Portal - Password Reset\n" );
    $smtp->datasend( "\n" );
    $smtp->datasend( $email_body );
    $smtp->dataend();
    $smtp->quit;
        
} 

###########################################################                   
# Get Login Status Box
#----------------------------------------------------------
sub getLoginStatus {
	
	my ( $self ) = @_;
	
	my $result = "";
	my $uname  = $self->getCurrentUserName;
	if ( $uname gt "" ) {
	    $result = "Logged in as: <b>$uname</b>";
	}
	return $result;
}


1;
