#!/usr/bin/perl
########################################
# This package provides shared database
# and CGI routines and other functions.
#
# Created: 03/10/2004 - Eric Huber
########################################

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;

my $shared;
my $auth;
my $src_path;
my $color_head1;
my $color_head2;
my $color_bg1;
my $color_bg2;
my $color_bg3;
my $img_path;
my $js_path;
my $css_path;
my $rte_path;
my $site_title;

my $debug = 0;
my $page_dtls;

package admin;

# -------------------------------------------------------------------------------------
# The constructor   
# -------------------------------------------------------------------------------------
sub new {
    
    my ( $class, $sh_in, $auth_in ) = @_;
    
    my $self = {};
    
    my $config    = new config;
    $src_path     = $config->get_src_path;
    my $app_ver   = $config->get_app_ver;
    my (%colors)  = $config->get_colors;
    $color_head1  = $colors{ "HEAD1" };
    $color_head2  = $colors{ "HEAD2" };
    $color_bg1    = $colors{ "BG1" };
    $color_bg2    = $colors{ "BG2" };
    $color_bg3    = $colors{ "BG3" };
    $img_path     = $config->get_img_path;
    $js_path      = $config->get_js_path;
    $css_path     = $config->get_css_path;
    $site_title   = $config->get_app_name;
    $debug        = $config->get_debug;
    
    bless ( $self, $class );

    $shared                       = $sh_in;
    $auth                         = $auth_in;
    $page_dtls->{template}        = "admin.html";
    $page_dtls->{css}             = "style.css";
    $page_dtls->{admin_nav_links} = $auth->getUserOptions;
    $page_dtls->{login_stat}      = $auth->getLoginStatus;
    $page_dtls->{app_ver}         = $app_ver;

    return $self;
}
# -------------------------------------------------------------------------------------



###########################################################                   
# View transactions for account
#----------------------------------------------------------
sub browse {

    my ( $self, $mnt_code, $admin_url ) = @_;
    
    my $result = 0;
    my $row    = 1;
    my $row_color;
    my $SQL;
    my $heading;
    my $key_field;

    if ( $auth->validate_access( $mnt_code ) ) {
    	
    	my $hd_rec = $shared->get_rec( "sys_head", "syh_code", $mnt_code );
    	if ( $hd_rec->{syh_id} gt "" ) {
    		
    		$SQL       = $hd_rec->{syh_browse_sql};
    		$heading   = $hd_rec->{syh_browse_heading};
            $key_field = $hd_rec->{syh_key_field};
            $doAddDel  = $hd_rec->{syh_do_add_del};
            $result    = 1;

            my $pg_body = "";
            $pg_body .= "<table border=0 cellpadding=0 cellspacing=0 width=\"630\">\n";
            $pg_body .= "<tr><td>\n";
            $pg_body .= "<div class=blur><div class=shadow><div class=framework>\n";
            $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=\"630\">\n";
            $pg_body .= "<tr bgcolor=$color_bg1><td valign=top align=left>\n";
            $pg_body .= "<table border=0 cellpadding=0 cellspacing=0 width=\"626\">\n";
            $pg_body .= "<tr bgcolor=$color_head1>\n";
        
            my $qyDef = "SELECT sym_order, sym_field, sym_label FROM sys_maint\n"
                      . "WHERE sym_code = '$mnt_code'\n"
                      . "AND sym_browse = 'Y'\n"
                      . "ORDER BY sym_order";
                  
            my @rsDef = $shared->getResultSet( $qyDef );
        
            foreach my $sym_rec ( @rsDef ) {
            	my $col_head = $sym_rec->{sym_label};
        	    $pg_body .= "<td valign=top align=left class=colhead>$col_head</td>\n";
            	$fld_cnt++;
            }
        
            $pg_body .= "<td valign=top width=110 class=rightcolhead>\n";
            if ( $doAddDel eq "Y" ) {
                $pg_body .= "<a href=\"$admin_url?action=add_form&mnt_code=$mnt_code\">";
                $pg_body .= "<img src=\"$img_path/add.gif\" border=0 alt=\"Add Record\"></a></td>\n";
            } else {
            	$pg_body .= "&nbsp;</td>\n";
            }
        
            $pg_body .= "</tr>\n";
        
            my @rs = $shared->getResultSet( $SQL );
            my $rsCnt = @rs;
            my $dbCnt = 0;
            foreach my $db_rec ( @rs ) {
        	
        	    $dbCnt++;
        	    
        	    if ( $row == 1 ) {
        	    	$row_color = $color_bg2;
        		    $row = 0;
        	    } else {
        		    $row_color = $color_bg1;
        		    $row = 1;
        	    }
        	
        	    $pg_body .= "<tr bgcolor=\"$row_color\">";
        	
        	    my $key_val = $db_rec->{$key_field};
        	    foreach my $sym_rec ( @rsDef ) {
        		    my $fld     = $sym_rec->{sym_field};
        		    my $fld_val = $db_rec->{$fld};
        	        $pg_body .= "<td valign=top align=left class=colbody>$fld_val";
        	        if ( $fld =~ /_order$/ ) {
        	        	if ( $dbCnt > 1 ) {
        	        	    $pg_body .= "<br><a href=\"$admin_url?action=order_dn&mnt_code=$mnt_code&$key_field=$key_val&order_fld=$fld&cur_order=$fld_val\">\n";
        	        	    $pg_body .= "<img src=\"$img_path/up_arrow.gif\" border=0 alt=\"Move Up\"></a>";
        	        	}
        	        	if ( $dbCnt < $rsCnt ) {
        	        	    $pg_body .= "<br><a href=\"$admin_url?action=order_up&mnt_code=$mnt_code&$key_field=$key_val&order_fld=$fld&cur_order=$fld_val\">\n";
        	        	    $pg_body .= "<img src=\"$img_path/dn_arrow.gif\" border=0 alt=\"Move Down\"></a>";
        	        	}
        	        }
        	        $pg_body .= "</td>";
        	    }
        	
        	    $pg_body .= "<td valign=top align=right class=colbody>";
        	    $pg_body .= "<a href=\"$admin_url?action=edit_form&mnt_code=$mnt_code&$key_field=$key_val\">";
        	    $pg_body .= "<img src=\"$img_path/icon_edit.gif\" border=0 alt=\"Edit Record\" title=\"Edit Record\"></a>";
        	    if ( $doAddDel eq "Y" ) {
        	        $pg_body .= "&nbsp;<a href=\"$admin_url?action=drop&mnt_code=$mnt_code&$key_field=$key_val\" ";
        	        $pg_body .= "onClick=\"return confirm('Deleting this record will permanently remove it.  Continue?')\">";
        	        $pg_body .= "<img src=\"$img_path/icon_drop.gif\" border=0 alt=\"Delete Record\" title=\"Delete Record\"></a>\n";
        	    }
        	    $pg_body .= "</td></tr>\n";
            }
        
            $pg_body .= "</table>\n";
            $pg_body .= "</td></tr>\n";
            $pg_body .= "</table>\n";
            $pg_body .= "</div></div></div>\n";
            $pg_body .= "</td></tr>\n";
            $pg_body .= "</table>\n";
            
            $page_dtls->{page_head}    = $heading;
            $page_dtls->{html_title}   = "$site_title - $heading";
            $page_dtls->{page_body}    = $pg_body;
        
            $shared->page_gen( $page_dtls, "" );

        }
    
    } else {
    	$shared->do_err( "You are not authenticated.  Admin option unavailable.", "admin.html" );
    }
    
    return $result;
        
}

###########################################################                   
# Form to add record
#----------------------------------------------------------
sub add_form {

    my ( $self, $mnt_code, $admin_url ) = @_;
    my $result = 0;
    my $heading;
    
    if ( $auth->validate_access( $mnt_code ) ) {

    	my $hd_rec = $shared->get_rec( "sys_head", "syh_code", $mnt_code );
    	if ( $hd_rec->{syh_id} gt "" ) {
    		
    		$heading   = $hd_rec->{syh_add_heading};
    		$result    = 1;

            my $pg_body = "";
            $pg_body .= "<form method=post action=\"$admin_url\">\n";
            $pg_body .= "<input type=\"hidden\" name=\"action\" value=\"add\">\n";
            $pg_body .= "<input type=\"hidden\" name=\"mnt_code\" value=\"$mnt_code\">\n";
            $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=\"530\">\n";

            my $qyDef = "SELECT sym_order, sym_field, sym_label, sym_size, sym_type\n"
                      . "FROM sys_maint\n"
                      . "WHERE sym_code = '$mnt_code'\n"
                      . "AND sym_add = 'Y'\n"
                      . "ORDER BY sym_order";
                  
            my @rsDef = $shared->getResultSet( $qyDef );
        
            my $fld_cnt = 0;
            foreach my $sym_rec ( @rsDef ) {
            	my $fld       = $sym_rec->{sym_field};
            	my $fld_label = $sym_rec->{sym_label};
            	my $fld_type  = $sym_rec->{sym_type};
            	my $fld_size  = $sym_rec->{sym_size};
            	my $fld_list  = $sym_rec->{sym_select_list};
            	
        	    $pg_body .= "<tr><td valign=top align=left class=label>$fld_label</td>\n";
      		    $pg_body .= "<td valign=top align=left class=form>";
      		
        	    if ( ( $fld_type eq "TEXT" ) || ( $fld_type eq "NUMBER" ) ) {
        		    $pg_body .= "<input name=\"$fld\" value=\"\" size=$fld_size class=form>\n";
        	    } elsif ( $fld_type eq "TEXTAREA" ) {
        		    $pg_body .= "<textarea name=\"$fld\" cols=$fld_size rows=5 wrap class=form></textarea>\n";
        	    } elsif ( $fld_type eq "CHECKBOX" ) {
        	    	$pg_body .= "<input type=\"checkbox\" name=\"$fld\" value=\"Y\" class=form> Yes\n";
        	    } elsif ( $fld_type eq "RTE" ) {
        	        $pg_body .= "<script src=\"$rte_path/js/richtext.js\" type=\"text/javascript\" language=\"javascript\"></script>\n"
                              . "<script src=\"$rte_path/js/config.js\" type=\"text/javascript\" language=\"javascript\"></script>\n"
                              . "<script>\n"
                              . "rteName=\"$fld\";\n"
                              . "initRTE( \"\", \"$css_path/rte.css\" );\n"
                              . "</script>\n";

        	    } elsif ( $fld_type eq "SELECTBOX" ) {
        	    	if ( $fld_list gt "" ) {
        	    		$pg_body .= "<select name=\"$fld\" class=form>\n";
        	    		my @list = split( ",", $fld_list );
        	    		foreach my $elem ( @list ) {
        	    			my @set = split( ":", $elem );
        	    			my $sLn = @set;
        	    			if ( $sLn == 1 ) {
        	    			    $pg_body .= "<option value=\"" . $set[0] . "\">" . $set[0] . "</option>\n";
        	    			} else {
        	    				$pg_body .= "<option value=\"" . $set[0] . "\">" . $set[1] . "</option>\n";
        	    			}
        	    		}
        	    		$pg_body .= "</select>\n";
        	    	}
        	    }
        	    			
        	    	
        	
        	    $pg_body .= "</td></tr>\n";
            }
        	
            $pg_body .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
            $pg_body .= "<td valign=top align=left class=form><input type=submit value=\"Add Now\" class=form>&nbsp;\n";
            $pg_body .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$admin_url?action=browse&mnt_code=$mnt_code'\" class=form></td></tr>\n";
            $pg_body .= "</table>\n";
            $pg_body .= "</form>\n";
        
            $page_dtls->{page_head}    = $heading;
            $page_dtls->{html_title}   = "$site_title - $heading";
            $page_dtls->{page_body}    = $pg_body;
        
            $shared->page_gen( $page_dtls, "" );
        }
               
    } else {
    	$shared->do_err( "You are not authenticated.  Admin option unavailable.", "admin.html" );
    }
    
    return $result;
}


###########################################################                   
# Add transaction to DB
#----------------------------------------------------------
sub add_rec {

    my ( $self, $mnt_code ) = @_;
    
    my $table;
    my $result = 0;
    
    if ( $auth->validate_access( $mnt_code ) ) {

    	my $hd_rec = $shared->get_rec( "sys_head", "syh_code", $mnt_code );
    	if ( $hd_rec->{syh_id} gt "" ) {
    		
    		$table      = $hd_rec->{syh_table};
    		my $ord_fld = $hd_rec->{syh_ord_field};
    		my $key_fld = $hd_rec->{syh_key_field};
    		
            my $SQL  = "INSERT INTO $table ( ";
            my $VALS = "";

            my $qyDef = "SELECT sym_order, sym_field, sym_type\n"
                      . "FROM sys_maint\n"
                      . "WHERE sym_code = '$mnt_code'\n"
                      . "AND sym_add = 'Y'\n"
                      . "ORDER BY sym_order";
                  
            my @rsDef = $shared->getResultSet( $qyDef );        
        
            my $fld_cnt = 0;
            foreach my $sym_rec ( @rsDef ) {
        	    my $fld      = $sym_rec->{sym_field};
        	    my $fld_type = $sym_rec->{sym_type};
        	
        	    my $input_value = $shared->get_cgi( $fld );
        	    $input_value =~ s/'/\\'/g;
        	    
        	    if ( $fld_type eq "CHECKBOX" ) {
        	    	if ( $input_value eq "" ) {
        	    		$input_value = 'N';
        	    	}
        	    }
        	    
        	    if ( $fld_cnt == 0 ) {
        		    $SQL  .= "$fld";
        		    if ( $fld_type eq "NUMBER" ) {
        		        $VALS .= "$input_value";
        		    } else {
        			    $VALS .= "'$input_value'";
        		    }
        	    } else {
        		    $SQL .= ", $fld";
        		    if ( $fld_type eq "NUMBER" ) {
        			    $VALS .= ", $input_value";
        		    } else {
        			    $VALS .= ", '$input_value'";
        		    }
        	    }
        	    $fld_cnt++;
            }
        
            $SQL .= " ) VALUES ( " . $VALS . " )";
        
            $shared->doSQLUpdate( $SQL );
            
            if ( $ord_fld gt "" ) {
            	my $new_ord = $shared->getSQLLastInsertID;
            	$shared->doSQLUpdate( "UPDATE $table SET $ord_fld = $new_ord WHERE $key_fld = $new_ord" );
            }
        
            $result = 1;
        }
        
    }
    
    return $result;
}
    
###########################################################                   
# Form to add record
#----------------------------------------------------------
sub edit_form {

    my ( $self, $mnt_code, $admin_url ) = @_;
    
    my $result = 0;
    my $SQL;
    my $key_field;
    my $heading;
    my $date_cnt = 0;
    my $rte_cnt  = 0;
    
    if ( $auth->validate_access( $mnt_code ) ) {

        my $hd_rec = $shared->get_rec( "sys_head", "syh_code", $mnt_code );
    	if ( $hd_rec->{syh_id} gt "" ) {
    		
    		$SQL       = $hd_rec->{syh_edit_sql};
    		$heading   = $hd_rec->{syh_edit_heading};
            $key_field = $hd_rec->{syh_key_field};

            my $key_val = $shared->get_cgi( $key_field );
            $SQL =~ s/<K>/$key_val/g;
            
            my @rs = $shared->getResultSet( $SQL );
            foreach my $this_rec ( @rs ) {
        
                my $pg_body = "";
                $pg_body .= "<form name=\"admin_form\" method=post action=\"$admin_url\">\n";
                $pg_body .= "<input type=\"hidden\" name=\"action\" value=\"edit\">\n";
                $pg_body .= "<input type=\"hidden\" name=\"mnt_code\" value=\"$mnt_code\">\n";
                $pg_body .= "<input type=\"hidden\" name=\"$key_field\" value=\"$key_val\">\n";
                $pg_body .= "<table border=0 cellpadding=2 cellspacing=0 width=\"800\">\n";
            
                my $qyDef = "SELECT *\n"
                          . "FROM sys_maint\n"
                          . "WHERE sym_code = '$mnt_code'\n"
                          . "ORDER BY sym_order";
                  
                my @rsDef = $shared->getResultSet( $qyDef );
        
                my $fld_cnt = 0;
                my $edt_cnt = 0;
                foreach my $sym_rec ( @rsDef ) {
                	my $fld       = $sym_rec->{sym_field};
                	my $fld_label = $sym_rec->{sym_label};
        	        my $fld_type  = $sym_rec->{sym_type};
        	        my $fld_size  = $sym_rec->{sym_size};
        	        my $fld_stat  = $sym_rec->{sym_status};
        	        my $fld_list  = $sym_rec->{sym_select_list};
        	        my $lu_opt    = $sym_rec->{sym_lookup_optional};
        	        my $fld_instr = $sym_rec->{sym_instruct};
        	        my $skip_fld  = $sym_rec->{sym_skip_field};
        	    
        	        my $skp_val   = "";
        	        
        	        if ( $skip_fld gt "" ) {
        	            $skp_val  = $this_rec->{$skip_fld};
        	        }
        	        
        	        my $fld_val = $this_rec->{$fld};
        	        
        	        if ( $skp_val ne "Y" ) {
        	    
        	            $pg_body .= "<tr><td valign=top align=left class=label>$fld_label</td>\n";
      		            $pg_body .= "<td valign=top align=left class=colbody>";
      		
      		            if ( $fld_stat eq "VIEWONLY" ) {
      		    	        $pg_body .= "<b>$fld_val</b>\n";
      		            } else {
        	                if ( ( $fld_type eq "TEXT" ) || ( $fld_type eq "NUMBER" ) ) {
        	            	    my $htm_qt = "&quot;";
        	            	    $fld_val =~ s/"/$htm_qt/g;
        		                $pg_body .= "<input name=\"$fld\" value=\"$fld_val\" size=$fld_size class=form>\n";
        	                } elsif ( $fld_type eq "TEXTAREA" ) {
        		                $pg_body .= "<textarea name=\"$fld\" cols=$fld_size rows=8 wrap class=form>$fld_val</textarea>\n";
                	        } elsif ( $fld_type eq "RTE" ) {
                	            $fld_val =~ s/[\n\r]/ /g;
                	            $pg_body .= "<script>\n"
                                          . "rteName=\"$fld\";\n"
                                          . "initRTE( \"$fld_val\", \"$css_path/rte.css\" );\n"
                                          . "</script>\n";
                                $rte_cnt++;
        		            
        	                } elsif ( $fld_type eq "CHECKBOX" ) {
        	            	    if ( $fld_val eq "Y" ) {
        	            		    $pg_body .= "<input type=\"checkbox\" name=\"$fld\" value=\"Y\" checked class=form>\n";
        	            	    } else {
        	            		    $pg_body .= "<input type=\"checkbox\" name=\"$fld\" value=\"Y\" class=form>\n";
        	            	    }
        	                } elsif ( $fld_type eq "SELECTBOX" ) {
                	    	    if ( $fld_list gt "" ) {
                	    		    $pg_body .= "<select name=\"$fld\" class=form>\n";
        	            		    my @list = split( ",", $fld_list );
        	    		            foreach my $elem ( @list ) {
        	    			            my @set = split( ":", $elem );
        	    		        	    my $sLn = @set;
        	    		        	    if ( $set[0] eq $fld_val ) {
        	    			                if ( $sLn == 1 ) {
        	    			                    $pg_body .= "<option value=\"" . $set[0] . "\" selected>" . $set[0] . "</option>\n";
        	    			                } else {
        	    				                $pg_body .= "<option value=\"" . $set[0] . "\" selected>" . $set[1] . "</option>\n";
        	    			                }
                                        } else {
        	    			                if ( $sLn == 1 ) {
        	    			                    $pg_body .= "<option value=\"" . $set[0] . "\">" . $set[0] . "</option>\n";
        	    			                } else {
        	    				                $pg_body .= "<option value=\"" . $set[0] . "\">" . $set[1] . "</option>\n";
        	    			                }
                                        }
        	    		            }
        	    		            $pg_body .= "</select>\n";
        	    	            }
        	                } elsif ( $fld_type eq "LOOKUP" ) {
                	    	    if ( $fld_list gt "" ) {
                	    		    $pg_body .= "<select name=\"$fld\" class=form>\n";
                	    		    if ( $lu_opt eq "Y" ) {
                	    			    $pg_body .= "<option value=\"\"></option>\n";
                	    		    }
                	    		    $pg_body .= $shared->pop_lookup_select( $fld_list, $fld_val );
        	    		            $pg_body .= "</select>\n";
        	    	            }
            	            } elsif ( $fld_type eq "SELECTSQL" ) {
            	                if ( $fld_list gt "" ) {
                    	            $pg_body .= "<select name=\"$fld\" class=form>\n";
                    	            if ( $lu_opt eq "Y" ) {
                    	    	        $pg_body .= "<option value=\"0\"></option>\n";
                	                }
                	                my @rsSelect = $shared->getResultSet( $fld_list );
                	                foreach my $nameVal ( @rsSelect ) {
                	    	            my $code = $nameVal->{CODE};
                	    	            my $desc = $nameVal->{DESC};
                	    	            if ( $fld_val eq "$code" ) {
                	    	    	        $pg_body .= "<option value=\"$code\" selected>$desc</option>\n";
                	    	            } else {
                	    	                $pg_body .= "<option value=\"$code\">$desc</option>\n";
                	    	            }
                	                }
        	    	                $pg_body .= "</select>\n";
        	                    } 
        	            
        	                } elsif ( $fld_type eq "DATE" ) {
        	        	
                	            $date_cnt++;
                 	            $pg_body .= "<INPUT size=\"$fld_size\" type='text' value=\"$fld_val\" name=\"$fld\" id=\"$fld\" maxlength=10 class=form>&nbsp;\n";
                                $pg_body .= "<A NAME=\"date_choose_$date_cnt\" ID=\"date_choose_$date_cnt\" width='18px' height='18px' align='top'><IMG width='18px' height='18px' align='top' SRC=\"$img_path/calendar.gif\" title='Choose date' ></A>\n";
	                            $pg_body .= "<script type=\"text/javascript\">\n";
                                $pg_body .= "Calendar.setup({\n";
	                            $pg_body .= "inputField     : \"$fld\",\n";
	                            $pg_body .= "button         : \"date_choose_$date_cnt\",\n";
	                            $pg_body .= "weekNumbers    : false,\n";
	                            $pg_body .= "ifFormat       : '%Y-%m-%d',\n";
	                            $pg_body .= "align          : 'Br',\n";
	                            $pg_body .= "singleClick    : true });\n";
                                $pg_body .= "</script>\n";
                            
                            }

	       	                if ( $fld_instr gt "" ) {
    	   	                    $pg_body .= "<br><span class=notice>$fld_instr</span>";
       		                }
       		            
       		                $edt_cnt++;
        	            }
        	    
        	            $pg_body .= "</td></tr>\n";
                    }
                }
        	
                $pg_body .= "<tr><td valign=top align=left class=label>&nbsp;</td>\n";
                $pg_body .= "<td valign=top align=left class=colbody>\n";
                if ( $edt_cnt > 0 ) {
                    $pg_body .= "<input type=submit value=\"Update Now\" class=form>&nbsp;\n";
                }
                $pg_body .= "<input type=button value=\"Cancel\" onClick=\"parent.location.href='$admin_url?action=browse&mnt_code=$mnt_code'\" class=form></td></tr>\n";
                $pg_body .= "</table>\n";
                $pg_body .= "</form>\n";
        
                my $js    = "";
                if ( $date_cnt > 0 ) {    
                    $js   .= "<link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"$css_path/calendar.css\"/>\n";
                    $js   .= "<script type=\"text/javascript\" src=\"$js_path/calendar.js\"></script>\n";
                    $js   .= "<script type=\"text/javascript\" src=\"$js_path/calendar-language.js\"></script>\n";
                    $js   .= "<script type=\"text/javascript\" src=\"$js_path/calendar-setup.js\"></script>\n";
                }
                if ( $rte_cnt > 0 ) {
                    $js   .= "<script src=\"$rte_path/js/richtext.js\" type=\"text/javascript\" language=\"javascript\"></script>\n";
                    $js   .= "<script src=\"$rte_path/js/config.js\" type=\"text/javascript\" language=\"javascript\"></script>\n";
                }
                
                if ( $js gt "" ) {
                    $page_dtls->{javascript} = $js;
                }
        
                $page_dtls->{page_head}    = $heading;
                $page_dtls->{html_title}   = "$site_title - $heading";
                $page_dtls->{page_body}    = $pg_body;
        
                $shared->page_gen( $page_dtls );
            
                last;
            }
        }
               
    } else {
    	$shared->do_err( "You are not authenticated.  Admin option unavailable.", "admin.html" );
    }
    return $result;

}

###########################################################                   
# Edit record in DB
#----------------------------------------------------------
sub edit_rec {

    my ( $self, $mnt_code, $table, $key_field ) = @_;

    my $result = 0;
    my $table;
    my $key_field;
    my $curQry;
    
    if ( $auth->validate_access( $mnt_code ) ) {

    	my $hd_rec = $shared->get_rec( "sys_head", "syh_code", $mnt_code );
    	if ( $hd_rec->{syh_id} gt "" ) {
    		
    		$table      = $hd_rec->{syh_table};
            $key_field  = $hd_rec->{syh_key_field};
            $curQry     = $hd_rec->{syh_edit_sql};

            my $key_val = $shared->get_cgi( $key_field );
            $curQry    =~ s/<K>/$key_val/g;
            
            my $curRec  = $shared->getResultRec( $curQry );
        
            my $SQL     = "UPDATE $table SET ";
            my $WHERE   = "WHERE $key_field = $key_val";

            my $qyDef = "SELECT sym_order, sym_field, sym_type, sym_skip_field\n"
                      . "FROM sys_maint\n"
                      . "WHERE sym_code = '$mnt_code'\n"
                      . "AND sym_status = 'EDIT'\n"
                      . "ORDER BY sym_order";
                  
            my @rsDef = $shared->getResultSet( $qyDef );
        
            my $fld_cnt = 0;
            foreach my $sym_rec ( @rsDef ) {
        	    my $fld      = $sym_rec->{sym_field};
        	    my $fld_type = $sym_rec->{sym_type};
        	    my $skip_fld = $sym_rec->{sym_skip_field};
        	    
        	    my $skp_val  = "";
        	    if ( $skip_fld gt "" ) {
        	        $skp_val = $curRec->{$skip_fld};
        	    }
        	
        	    if ( $skp_val ne "Y" ) {
        	        
            	    my $input_value = $shared->get_cgi( $fld );
        	        $input_value =~ s/'/\\'/g;
        	    
        	        if ( $fld_type eq "CHECKBOX" ) {
        	    	    if ( $input_value eq "" ) {
        	    		    $input_value = "N";
        	    	    }
        	        }
        	    
        	        if ( $fld_cnt == 0 ) {
        		        if ( $fld_type eq "NUMBER" ) {
        		            $SQL .= "$fld = $input_value";
        		        } else {
        			        $SQL .= "$fld = '$input_value'";
        		        }
        	        } else {
        		        if ( $fld_type eq "NUMBER" ) {
        			        $SQL .= ", $fld = $input_value";
        		        } else {
        			        $SQL .= ", $fld = '$input_value'";
        		        }
        	        }
        	    }
        	    
        	    $fld_cnt++;
            }
        
            $SQL .= " " . $WHERE;
        
            $shared->doSQLUpdate( $SQL );
        
            $result = 1;
        }
    }
    
    return $result;
}

###########################################################                   
# Drop record in DB
#----------------------------------------------------------
sub drop_rec {

    my ( $self, $mnt_code ) = @_;

    my $key_field;
    my $table;
    
    my $result = 0;
    
    if ( $auth->validate_access( $mnt_code ) ) {

    	my $hd_rec = $shared->get_rec( "sys_head", "syh_code", $mnt_code );
    	if ( $hd_rec->{syh_id} gt "" ) {
    		
    		$table     = $hd_rec->{syh_table};
            $key_field = $hd_rec->{syh_key_field};

            my $key_val = $shared->get_cgi( $key_field );
        
            my $SQL     = "DELETE FROM $table WHERE $key_field = $key_val";

            $shared->doSQLUpdate( $SQL );
        
            $result = 1;
        }
        
    }
    
    return $result;
}



1;
