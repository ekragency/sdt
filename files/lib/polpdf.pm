#!/usr/bin/perl
########################################
# This package provides PDF reports for 
# the Metafraze Tools site
#
# Created: 08/01/2013 - Eric Huber
###########################################################################
# Copyright 2013, P. Eric Huber
###########################################################################
use PDF::API2;
use Date::Calc qw( :all );
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;
use ucode;

my $shared;
my $ucode;
my $src_path;
my $color_head1;
my $color_head2;
my $color_bg1;
my $color_bg2;
my $color_bg3;
my $color_error;
my $img_path;
my $site_title;
my $pdf_path;
my $pdf_url;
my $log_path;
my ( $now_year, $now_month, $now_day ) = Today();

my $java_msg = "";

my $debug = 1;

my $f1;
my $f2;
my $f3;
my $ptReg = 9;
my $ptSml = 8;
my $ptLrg = 20;
my @tab   = ( 50, 140, 230, 340, 410, 490, 570 );
my @bTab  = ( 50, 154, 278, 377, 471, 570 );
my $tCP   = 310;
my $y;

my $ySpc  = 14;


package polpdf;

# -------------------------------------------------------------------------------------
# The constructor   
# -------------------------------------------------------------------------------------
sub new {
    
    my ( $class, $sh_in ) = @_;
    
    my $self = {};
    
    my $config    = new config;
    $src_path     = $config->get_src_path;
    my (%colors)  = $config->get_colors;
    $color_head1  = $colors{ "HEAD1" };
    $color_head2  = $colors{ "HEAD2" };
    $color_bg1    = $colors{ "BG1" };
    $color_bg2    = $colors{ "BG2" };
    $color_bg3    = $colors{ "BG3" };
    $color_error  = $colors{ "ERROR" };
    $img_path     = $config->get_img_path;
    $site_title   = $config->get_app_name;
    $pdf_path     = $config->get_pdf_path;
    $pdf_url      = $config->get_pdf_url;
    $log_path     = $config->get_log_path;
    
    bless ( $self, $class );

    $shared  = $sh_in;
    $ucode   = new ucode;
    
    return $self;
}
# -------------------------------------------------------------------------------------

###########################################################                   
# Generate the PO in PDF format
#----------------------------------------------------------
sub genPO {
	
	my ( $self, $pid ) = @_;

    if ( $debug ) {
        open( DEBUG, ">$log_path/polpdf.log" );
        print DEBUG "=========================\n";
        print DEBUG "Begin of genPO subroutine\n";
    }
    	     
	my $rec    = $shared->get_rec( "po_log", "pol_id", $pid );
	my $aUSR   = $rec->{pol_add_usr_id};
	my $pono   = $rec->{pol_pono};
	my $pType  = $rec->{pol_type};
	my $pjno   = $rec->{pol_proj_no};
    my $iDt    = $shared->getDateFmt( $rec->{pol_add_date} );
	my $lID    = $rec->{pol_ling_id};
	my $cID    = $rec->{pol_cln_id};
	my $src    = $rec->{pol_src_lng_id};
	my $tgt    = $rec->{pol_tgt_lng_id};
	my $instV  = $rec->{pol_notes};
	my $notEd  = $rec->{pol_not_edit_text};
	my $cur    = $rec->{pol_currency};
	my $due    = $rec->{pol_due_time};
	my $doTR   = $rec->{pol_do_trans};
	my $doED   = $rec->{pol_do_edit};
	my $doPF   = $rec->{pol_do_proof};
	my $doMT   = $rec->{pol_do_mtpe};
	my $doDsc  = $rec->{pol_do_disc};
	my $tRate  = $rec->{pol_trans_rate};
	my $eRate  = $rec->{pol_edit_rate};
	my $pRate  = $rec->{pol_hourly_rate};
	my $mRate  = $rec->{pol_mtpe_rate};
	my $dRate  = $rec->{pol_disc_amt};
	my $wCnt   = $rec->{pol_total_wcnt};
	my $tAmt   = $rec->{pol_trans_amt};
	my $eAmt   = $rec->{pol_edit_amt};
	my $pAmt   = $rec->{pol_proof_amt};
	my $mAmt   = $rec->{pol_mtpe_amt};
	my $dAmt   = $rec->{pol_disc_amt};
	my $amtT   = $rec->{pol_total_amt};
	my $doMin  = $rec->{pol_min_rate};
	my $doDMin = $rec->{pol_dscmin_rate};
	my $mdRate = $rec->{pol_dscmin_rate_value};
	my $spcLbl = "Hourly Rate";
	if ( $doDMin eq "Y" ) {
	    $spcLbl = "Hourly Rate Discounted $mdRate \%";
	}
	my $subTot = 0;
	
	my $dspDsc = "";
	if ( $doDsc eq "Y" ) {
	    $dspDsc = "-" . $shared->fmtNumber( $dAmt );
	}
 	
	my $lrec   = $shared->get_rec( "linguist", "lig_id", $lID );
	my $lName  = $ucode->encodeChar( $shared->trim( $lrec->{lig_first_name} ) . " " . $shared->trim( $lrec->{lig_last_name} ) );
	my $lPhn   = $lrec->{lig_phone};
	my $lFax   = $lrec->{lig_fax};
	my $lEml   = $lrec->{lig_email};
	
	my $crec   = $shared->get_rec( "client", "cln_id", $cID );
	my $cName  = $crec->{cln_name};
	
	my $srcL   = $shared->getFieldVal( "language", "lng_desc", "lng_id", $src );
	my $tgtL   = $shared->getFieldVal( "language", "lng_desc", "lng_id", $tgt );
	my $tLCd   = $shared->getFieldVal( "language", "lng_code", "lng_id", $tgt );
	my $sLCd   = $shared->getFieldVal( "language", "lng_code", "lng_id", $src );
	
	my $bTo    = $shared->getConfigString( "ADM_POBILLTO" );
	my $comp   = $shared->getConfigString( "ADM_POCOMP" );
	my $add1   = $shared->getConfigString( "ADM_POADDR1" );
	my $add2   = $shared->getConfigString( "ADM_POADDR2" );
	my $phn    = $shared->getConfigString( "ADM_POPHONE" );
	my $fax    = $shared->getConfigString( "ADM_POFAX" );
	my $howP   = $shared->getConfigText( "ADM_POHOWPD" );
	my $terms  = $shared->getConfigText( "ADM_POTERMS" );
	my $instF  = $shared->getConfigText( "ADM_POINSTEXT" );
	my $sdtURL = $shared->getConfigString( "ADM_POURL" );
	my $mtfURL = $shared->getConfigString( "ADM_MFURL" );
    if ($mtfURL eq ""){
        $mtfURL = "www.metafraze.com";
    }

	my $urec   = $shared->get_rec( "user", "usr_id", $aUSR );
	my $uName  = $urec->{usr_name};
	my $uEml   = $urec->{usr_email};
	
	my $instTxt = $instV . "\n\n" . $instF;
	if ( $notEd eq "Y" ) {
	    my $neText = $shared->getConfigText( "ADM_PONOTEDIT" );
	    $instTxt  .= "\n\n" . $neText;
	}

	
	my $fullpdf    = "$pdf_path/$pono" . "_" . $tLCd . ".pdf";
	my $pdfURL     = "$pdf_url/$pono" . "_" . $tLCd . ".pdf";
	
    my $pdf  = PDF::API2->new;
    $f1      = $pdf->ttfont('/usr/share/fonts/dejavu/DejaVuSans.ttf');
    $f2      = $pdf->corefont( 'Verdana-Bold' );
    $f3      = $pdf->corefont( 'Helvetica-Oblique' );

    my $page = $pdf->page;
    $page->mediabox( 612, 792 );
    my $gfx        = $page->gfx;
    my $text       = $page->text;
    my $text_head  = $page->text;
    $text_head->font( $f2, $ptLrg );
    $text_head->fillcolor( "#000" );

    my $text_label = $page->text;
    $text_label->font( $f2, $ptReg );
    $text_label->fillcolor( "#000" );
    
    my $text_value = $page->text;
    $text_value->font( $f1, $ptReg );
    $text_value->fillcolor( "#000" );

    my $text_pgph  = $page->text;
    $text_pgph->font( $f1, $ptReg );
    $text_pgph->fillcolor( "#000" );

    my $text_sml   = $page->text;
    $text_sml->font( $f1, $ptSml );
    $text_sml->fillcolor( "#000" );
    
    my $text_smb   = $page->text;
    $text_smb->font( $f2, $ptSml );
    $text_smb->fillcolor( "#000" );

        
    my $logo = $pdf->image_png( "$src_path/logo.png" );
    $gfx->image( $logo, $tab[0], 700 );
    
    $text_head->translate( $tab[1], 725 );
    $text_head->text( "Purchase Order" );

    $y = 685;
    
    &doBoxText( $gfx, $text_label, $tab[0], $tab[1], $y, 1, "P.O. Number:", "R" );
    &doBoxText( $gfx, $text_value, $tab[1], $tab[2], $y, 1, $pono, "L" );
    &doBoxText( $gfx, $text_label, $tab[2], $tab[3], $y, 1, "Date Issued:", "R" );
    &doBoxText( $gfx, $text_value, $tab[3], $tab[4], $y, 1, $iDt, "L" );
    &doBoxText( $gfx, $text_label, $tab[4], $tab[5], $y, 1, "Requested by:", "R" );
    &doBoxText( $gfx, $text_value, $tab[5], $tab[6], $y, 1, $uName, "L" );
    
    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_value, $tab[0], $tab[2], $y, 1, "Project Number:", "R" );
    &doBoxText( $gfx, $text_value, $tab[2], $tab[6], $y, 1, "$pjno ($cName)", "L" );
    
    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_label, $tab[0], $tab[1], $y, 1, "To:", "R" );
    &doBoxText( $gfx, $text_value, $tab[1], $tab[3], $y, 1, $lName, "L" );
    &doBoxText( $gfx, $text_label, $tab[3], $tab[4], $y, 1, "Bill to:", "R" );
    &doBoxText( $gfx, $text_value, $tab[4], $tab[6], $y, 1, $bTo, "L" );
    
    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_label, $tab[0], $tab[1], $y, 1, "Company:", "R" );
    &doBoxText( $gfx, $text_value, $tab[1], $tab[3], $y, 1, "", "L" );
    &doBoxText( $gfx, $text_label, $tab[3], $tab[4], $y, 1, "Company:", "R" );
    &doBoxText( $gfx, $text_value, $tab[4], $tab[6], $y, 1, $comp, "L" );
    
    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_label, $tab[0], $tab[1], $y, 1, "Address:", "R" );
    &doBoxText( $gfx, $text_value, $tab[1], $tab[3], $y, 1, "", "L" );
    &doBoxText( $gfx, $text_label, $tab[3], $tab[4], $y, 1, "Address:", "R" );
    &doBoxText( $gfx, $text_value, $tab[4], $tab[6], $y, 1, $add1, "L" );
    
    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_label, $tab[0], $tab[1], $y, 1, "", "R" );
    &doBoxText( $gfx, $text_value, $tab[1], $tab[3], $y, 1, "", "L" );
    &doBoxText( $gfx, $text_label, $tab[3], $tab[4], $y, 1, "", "R" );
    &doBoxText( $gfx, $text_value, $tab[4], $tab[6], $y, 1, $add2, "L" );

    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_label, $tab[0], $tab[1], $y, 1, "Phone:", "R" );
    &doBoxText( $gfx, $text_value, $tab[1], $tab[3], $y, 1, $lPhn, "L" );
    &doBoxText( $gfx, $text_label, $tab[3], $tab[4], $y, 1, "Phone:", "R" );
    &doBoxText( $gfx, $text_value, $tab[4], $tab[6], $y, 1, $phn, "L" );

    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_label, $tab[0], $tab[1], $y, 1, "Fax:", "R" );
    &doBoxText( $gfx, $text_value, $tab[1], $tab[3], $y, 1, $lFax, "L" );
    &doBoxText( $gfx, $text_label, $tab[3], $tab[4], $y, 1, "Fax:", "R" );
    &doBoxText( $gfx, $text_value, $tab[4], $tab[6], $y, 1, $fax, "L" );

    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_label, $tab[0], $tab[1], $y, 1, "E-mail:", "R" );
    &doBoxText( $gfx, $text_value, $tab[1], $tab[3], $y, 1, $lEml, "L" );
    &doBoxText( $gfx, $text_label, $tab[3], $tab[4], $y, 1, "E-mail:", "R" );
    &doBoxText( $gfx, $text_value, $tab[4], $tab[6], $y, 1, $uEml, "L" );
    
    $y -= $ySpc;
    
    my $tmpSpc = 10;
    my $factor = &doTextWrap( $text_pgph, $tab[1], $tab[6], $y, $instTxt, $tmpSpc );
    my $rows   = &convertSpc( $factor, $tmpSpc );
    &doBoxText( $gfx, $text_label, $tab[0], $tab[1], $y, $rows, "Instructions:", "R" );
    &doBoxText( $gfx, $text_value, $tab[1], $tab[6], $y, $rows, "", "L" );
    
    $y -= ( $ySpc * $rows );
    
    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_label, $tab[0], $tab[1], $y, 2, "Source lang:", "R" );
    &doBoxText( $gfx, $text_value, $tab[1], $tab[3], $y, 2, $srcL, "L" );
    $text_value->translate( $tab[1] + 5, $y - $ySpc );
    $text_value->text( $sLCd );
    
    &doBoxText( $gfx, $text_label, $tab[3], $tab[4], $y, 2, "Target lang:", "R" );
    &doBoxText( $gfx, $text_value, $tab[4], $tab[6], $y, 2, $tgtL, "L" );
    $text_value->translate( $tab[4] + 5, $y - $ySpc );
    $text_value->text( $tLCd );
    
    
    $y -= ( $ySpc * 3 );
    
    my $spcBox = "N";
    if ( ( $doMin eq "Y" ) || ( $doDMin eq "Y" ) ) {
        $spcBox = "Y";
    }
    
    &doBoxText( $gfx, $text_label, $bTab[0], $bTab[1], $y, 1, "Service/Material", "L" );
    &doBoxText( $gfx, $text_label, $bTab[1], $bTab[2], $y, 1, "Ship/Due Date", "L" );
    if ( $spcBox eq "N" ) {
        &doBoxText( $gfx, $text_label, $bTab[2], $bTab[3], $y, 1, "Total Words/Qty", "R" );
        &doBoxText( $gfx, $text_label, $bTab[3], $bTab[4], $y, 1, "Rate", "R" );
    } else {
        &doBoxText( $gfx, $text_label, $bTab[2], $bTab[4], $y, 1, "Special Rate" );
    }
    &doBoxText( $gfx, $text_label, $bTab[4], $bTab[5], $y, 1, "Amount $cur", "R" );
    
    $y -= $ySpc;
    
    if ( $doTR eq "Y" ) {
        &doBoxText( $gfx, $text_value, $bTab[0], $bTab[1], $y, 1, "Translation", "L" );
        &doBoxText( $gfx, $text_value, $bTab[1], $bTab[2], $y, 1, $due, "L" );
        if ( $spcBox eq "N" ) {
            &doBoxText( $gfx, $text_value, $bTab[2], $bTab[3], $y, 1, "$wCnt words", "R" );
            &doBoxText( $gfx, $text_value, $bTab[3], $bTab[4], $y, 1, "$cur $tRate/word", "R" );
        } else {
            &doBoxText( $gfx, $text_value, $bTab[2], $bTab[4], $y, 1, "$spcLbl", "L" );
        }
        &doBoxText( $gfx, $text_value, $bTab[4], $bTab[5], $y, 1, sprintf("%.2f", $tAmt), "R" );
        $subTot += $tAmt;
        $y -= $ySpc;
    }
    
    if ( $doED eq "Y" ) {
        &doBoxText( $gfx, $text_value, $bTab[0], $bTab[1], $y, 1, "Review", "L" );
        &doBoxText( $gfx, $text_value, $bTab[1], $bTab[2], $y, 1, $due, "L" );
        if ( $spcBox eq "N" ) {
            &doBoxText( $gfx, $text_value, $bTab[2], $bTab[3], $y, 1, "$wCnt words", "R" );
            &doBoxText( $gfx, $text_value, $bTab[3], $bTab[4], $y, 1, "$cur $eRate/word", "R" );
        } else {
            &doBoxText( $gfx, $text_value, $bTab[2], $bTab[4], $y, 1, "$spcLbl", "L" );
        }
        &doBoxText( $gfx, $text_value, $bTab[4], $bTab[5], $y, 1, sprintf("%.2f", $eAmt), "R" );
        $subTot += $eAmt;
        $y -= $ySpc;
    }
    
    if ( $doPF eq "Y" ) {
        &doBoxText( $gfx, $text_value, $bTab[0], $bTab[1], $y, 1, "Proof-Reading", "L" );
        &doBoxText( $gfx, $text_value, $bTab[1], $bTab[2], $y, 1, $due, "L" );
        if ( $spcBox eq "N" ) {
            &doBoxText( $gfx, $text_value, $bTab[2], $bTab[3], $y, 1, "$wCnt words", "R" );
            &doBoxText( $gfx, $text_value, $bTab[3], $bTab[4], $y, 1, "$cur $eRate/hr", "R" );
        } else {
            &doBoxText( $gfx, $text_value, $bTab[2], $bTab[4], $y, 1, "$spcLbl", "L" );
        }
        &doBoxText( $gfx, $text_value, $bTab[4], $bTab[5], $y, 1, sprintf("%.2f", $pAmt), "R" );
        $subTot += $pAmt;
        $y -= $ySpc;
    }

    if ( $doMT eq "Y" ) {
        &doBoxText( $gfx, $text_value, $bTab[0], $bTab[1], $y, 1, "MTPE", "L" );
        &doBoxText( $gfx, $text_value, $bTab[1], $bTab[2], $y, 1, $due, "L" );
        if ( $spcBox eq "N" ) {
            &doBoxText( $gfx, $text_value, $bTab[2], $bTab[3], $y, 1, "$wCnt words", "R" );
            &doBoxText( $gfx, $text_value, $bTab[3], $bTab[4], $y, 1, "$cur $mRate/word", "R" );
        } else {
            &doBoxText( $gfx, $text_value, $bTab[2], $bTab[4], $y, 1, "$spcLbl", "L" );
        }
        &doBoxText( $gfx, $text_value, $bTab[4], $bTab[5], $y, 1, sprintf("%.2f", $mAmt), "R" );
        $subTot += $mAmt;
        $y -= $ySpc;
    }

    
    my $feeSQL = "SELECT * FROM po_fee WHERE pof_pol_id = $pid ORDER BY pof_service";
    my @feers  = $shared->getResultSet( $feeSQL );
    foreach my $fee_rec ( @feers ) {
        my $fSvc = $fee_rec->{pof_service};
        my $fAmt = $fee_rec->{pof_amt};

        &doBoxText( $gfx, $text_value, $bTab[0], $bTab[4], $y, 1, $fSvc, "L" );
        &doBoxText( $gfx, $text_value, $bTab[4], $bTab[5], $y, 1, sprintf("%.2f", $fAmt), "R" );
        $subTot += $fAmt;
        $y -= $ySpc;
    }
        
    my $dspSubTot = $shared->fmtNumber( $subTot );
    
    &doBoxText( $gfx, $text_label, $bTab[0], $bTab[1], $y, 5, "How to get paid:", "R" );
    &doBoxText( $gfx, $text_value, $bTab[1], $bTab[3], $y, 5, "", "L" );
    &doBoxText( $gfx, $text_label, $bTab[3], $bTab[4], $y, 1, "Subtotal", "R" );
    &doBoxText( $gfx, $text_value, $bTab[4], $bTab[5], $y, 1, sprintf("%.2f", $subTot), "R" );

    my $fact2 = &doTextWrap( $text_sml, $bTab[1], $bTab[3], $y, $howP, 9 );
    
    $y -= $ySpc;
        
    &doBoxText( $gfx, $text_label, $bTab[3], $bTab[4], $y, 1, "Discount", "R" );
    &doBoxText( $gfx, $text_value, $bTab[4], $bTab[5], $y, 1, $dspDsc, "R" );
        
    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_label, $bTab[3], $bTab[4], $y, 1, "Tax", "R" );
    &doBoxText( $gfx, $text_value, $bTab[4], $bTab[5], $y, 1, "", "R" );
    
    $y -= $ySpc;
        
    &doBoxText( $gfx, $text_label, $bTab[3], $bTab[4], $y, 1, "Shipping", "R" );
    &doBoxText( $gfx, $text_value, $bTab[4], $bTab[5], $y, 1, "", "R" );
    
    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_label, $bTab[3], $bTab[4], $y, 1, "Total", "R" );
    &doBoxText( $gfx, $text_value, $bTab[4], $bTab[5], $y, 1, sprintf("%.2f", $amtT), "R" );
    
    $y -= $ySpc;
    
    &doBoxText( $gfx, $text_value, $bTab[0], $bTab[5], $y, 2, "", "L" );

    $text_smb->translate( $bTab[0] + 5, $y );
    $text_smb->text( "TERMS:" );
    
    my $fact3 = &doTextWrap( $text_sml, $bTab[0] + 40, $bTab[5], $y, $terms, 9 );
    
    $y -= ( $ySpc * 4 );

    my $mid   = $bTab[0] + int( ( $bTab[5] - $bTab[0] ) / 2 );    
    #my $twPOS = $mid - 55;
    my $twPOS = $mid - 40;
    #my $fbPOS = $mid - 25;
    my $fbPOS = $mid - 10;
    #my $liPOS = $mid + 5;
    my $liPOS = $mid + 20;
    #my $pnPOS = $mid + 35;
    
    my $imgTW = $pdf->image_png( "$src_path/new_twit.png" );
    $gfx->image( $imgTW, $twPOS, $y );
    my $imgFB = $pdf->image_png( "$src_path/new_fb.png" );
    $gfx->image( $imgFB, $fbPOS, $y );
    my $imgLI = $pdf->image_png( "$src_path/new_lkin.png" );
    $gfx->image( $imgLI, $liPOS, $y );
    #my $imgPN = $pdf->image_png( "$src_path/new_pint.png" );
    #$gfx->image( $imgPN, $pnPOS, $y );

    my $fbURL = qq(https://www.facebook.com/Metafraze-105765925059905);
    my $twURL = qq(https://www.linkedin.com/company/metafraze/);
    my $liURL = qq(https://twitter.com/metafraze_);
    #my $pnURL = qq(https://www.pinterest.com/SDTranslations/);
    
    my $twLnk = $page->annotation(); 
    $twLnk->url( $twURL, -rect =>[ $twPOS, $y, $twPOS + 20, $y + 20 ], -border =>[1,1,1] );
    my $fbLnk = $page->annotation(); 
    $fbLnk->url( $fbURL, -rect =>[ $fbPOS, $y, $fbPOS + 20, $y + 20 ], -border =>[1,1,1] );
    my $liLnk = $page->annotation(); 
    $liLnk->url( $liURL, -rect =>[ $liPOS, $y, $liPOS + 20, $y + 20 ], -border =>[1,1,1] );
    #my $pnLnk = $page->annotation();
    #$pnLnk->url( $pnURL, -rect =>[ $pnPOS, $y, $pnPOS + 20, $y + 20 ], -border =>[1,1,1] );
    
    $y = 28;
    my $ftLine1 = "$add1 - $add2";
    my $ftLine2 = "Tel $phn - Fax $fax - $sdtURL | $mtfURL";
    $text_sml->translate( $mid, $y );
    $text_sml->text_center( $ftLine1 );
    $y -= 9;
    $text_sml->translate( $mid, $y );
    $text_sml->text_center( $ftLine2 );
    
    if ( ( $doTR eq "Y" ) && ( $doMin eq "N" ) && ( $pType eq "DFLT" ) ) {

        my $pCtxCnt = $rec->{pol_ctx_wcnt};
        my $pRepCnt = $rec->{pol_rep_wcnt};
        my $pXFRCnt = $rec->{pol_xfr_wcnt};
        my $p100Cnt = $rec->{pol_100_wcnt};
        my $p95Cnt  = $rec->{pol_95_wcnt};
        my $p85Cnt  = $rec->{pol_85_wcnt};
        my $p75Cnt  = $rec->{pol_75_wcnt};
        my $p50Cnt  = $rec->{pol_50_wcnt};
        my $pNoCnt  = $rec->{pol_no_wcnt};
        my $pABase  = $rec->{pol_abase_wcnt};
        my $pALrn   = $rec->{pol_alrn_wcnt};
        
        my $wt_ctx = ( $shared->getConfigInt( "ADM_ANALCTXMATCHPCT" ) / 100 );
        my $wt_rep = ( $shared->getConfigInt( "ADM_ANALREPPCT" ) / 100 );
        my $wt_xfr = ( $shared->getConfigInt( "ADM_ANALXFILREPPCT" ) / 100 );
        my $wt_100 = ( $shared->getConfigInt( "ADM_ANAL100PCT" ) / 100 );
        my $wt_95  = ( $shared->getConfigInt( "ADM_ANAL95PCT" ) / 100 );
        my $wt_85  = ( $shared->getConfigInt( "ADM_ANAL85PCT" ) / 100 );
        my $wt_75  = ( $shared->getConfigInt( "ADM_ANAL75PCT" ) / 100 );
        my $wt_50  = ( $shared->getConfigInt( "ADM_ANAL50PCT" ) / 100 );
        my $wt_new = ( $shared->getConfigInt( "ADM_ANALNEWPCT" ) / 100 );
        my $wt_abs = ( $shared->getConfigInt( "ADM_ANALAMTBASEPCT" ) / 100 );
        my $wt_aln = ( $shared->getConfigInt( "ADM_ANALAMTLEARNPCT" ) / 100 );

        if ( $rec->{pol_ctx_weight} gt "" ) {
            $wt_ctx = ( $rec->{pol_ctx_weight} / 100 );
        }
        if ( $rec->{pol_rep_weight} gt "" ) {
            $wt_rep = ( $rec->{pol_rep_weight} / 100 );
        }
        if ( $rec->{pol_xfr_weight} gt "" ) {
            $wt_xfr = ( $rec->{pol_xfr_weight} / 100 );
        }
        if ( $rec->{pol_100_weight} gt "" ) {
            $wt_100 = ( $rec->{pol_100_weight} / 100 );
        }
        if ( $rec->{pol_95_weight} gt "" ) {
            $wt_95  = ( $rec->{pol_95_weight} / 100 );
        }
        if ( $rec->{pol_85_weight} gt "" ) {
            $wt_85  = ( $rec->{pol_85_weight} / 100 );
        }
        if ( $rec->{pol_75_weight} gt "" ) {
            $wt_75  = ( $rec->{pol_75_weight} / 100 );
        }
        if ( $rec->{pol_50_weight} gt "" ) {
            $wt_50  = ( $rec->{pol_50_weight} / 100 );
        }
        if ( $rec->{pol_none_weight} gt "" ) {
            $wt_new = ( $rec->{pol_none_weight} / 100 );
        }
        if ( $rec->{pol_abase_weight} gt "" ) {
            $wt_abs = ( $rec->{pol_abase_weight} / 100 );
        }
        if ( $rec->{pol_alrn_weight} gt "" ) {
            $wt_aln = ( $rec->{pol_alrn_weight} / 100 );
        }
        
        my $amtctx  = $shared->round( $pCtxCnt * $wt_ctx * $tRate, 2 );
        my $amtrep  = $shared->round( $pRepCnt * $wt_rep * $tRate, 2 );
        my $amtxfr  = $shared->round( $pXFRCnt * $wt_xfr * $tRate, 2 );
        my $amt100  = $shared->round( $p100Cnt * $wt_100 * $tRate, 2 );
        my $amt95   = $shared->round( $p95Cnt * $wt_95 * $tRate, 2 );
        my $amt85   = $shared->round( $p85Cnt * $wt_85 * $tRate, 2 );
        my $amt75   = $shared->round( $p75Cnt * $wt_75 * $tRate, 2 );
        my $amt50   = $shared->round( $p50Cnt * $wt_50 * $tRate, 2 );
        my $amtnew  = $shared->round( $pNoCnt * $wt_new * $tRate, 2 );
        my $amtabs  = $shared->round( $pABase * $wt_abs * $tRate, 2 );
        my $amtaln  = $shared->round( $pALrn * $wt_aln * $tRate, 2 );
        my $amtTot  = ( $amtctx + $amtrep + $amtxfr + $amt100 + $amt95 + $amt85 + $amt75 + $amt50 + $amtnew + $amtabs + $amtaln );
       

        my $page1 = $pdf->page;
        $page1->mediabox( 612, 792 );
        my $gfx1        = $page1->gfx;
        my $text1       = $page1->text;
        my $text_head1  = $page1->text;
        $text_head1->font( $f2, $ptLrg );
        $text_head1->fillcolor( "#000" );

        my $text_label1 = $page1->text;
        $text_label1->font( $f2, $ptReg );
        $text_label1->fillcolor( "#000" );
    
        my $text_value1 = $page1->text;
        $text_value1->font( $f1, $ptReg );
        $text_value1->fillcolor( "#000" );

        my $text_sml1   = $page1->text;
        $text_sml1->font( $f1, $ptSml );
        $text_sml1->fillcolor( "#000" );

        $gfx1->image( $logo, $tab[0], 700 );
    
        $text_head1->translate( $tab[1], 725 );
        $text_head1->text( "Purchase Order (Continued)" );

        $y = 685;

        &doBoxText( $gfx1, $text_label1, $tab[0], $tab[1], $y, 1, "P.O. Number:", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[2], $y, 1, $pono, "L" );
        &doBoxText( $gfx1, $text_label1, $tab[2], $tab[3], $y, 1, "Date Issued:", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, $iDt, "L" );
        &doBoxText( $gfx1, $text_label1, $tab[4], $tab[5], $y, 1, "Requested by:", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $uName, "L" );
    
        $y -= ( $ySpc * 2 );
        
        $text_label1->translate( $tab[0] + 5, $y );
        $text_label1->text( "Translation Breakdown" );
        $text_label1->translate( $tab[5] - 5, $y );
        $text_label1->text_right( "Word Price:" );
        $text_value1->translate( $tab[6] - 5, $y );
        $text_value1->text_right( "$tRate ($cur)" );
        
        $y -= $ySpc;

        my $bdRows = 13;
        if ( $amtTot < $pRate ) {
            $bdRows++;
            if ( $doDMin eq "Y" ) {
                $bdRows++;
            }
        }
        
        &doBoxText( $gfx1, $text_label1, $tab[0], $tab[1], $y, $bdRows, "Analysis:", "R" );
        &doBoxText( $gfx1, $text_label1, $tab[1], $tab[3], $y, 1, "Match Types", "L" );
        &doBoxText( $gfx1, $text_label1, $tab[3], $tab[4], $y, 1, "Weight", "R" );
        &doBoxText( $gfx1, $text_label1, $tab[4], $tab[5], $y, 1, "Words", "R" );
        &doBoxText( $gfx1, $text_label1, $tab[5], $tab[6], $y, 1, "Amount", "R" );
         
        $y -= $ySpc;

        my $dspCtxAmt = $shared->fmtNumber( $amtctx );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "Context Match", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_ctx * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $pCtxCnt, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dspCtxAmt, "R" );

        $y -= $ySpc;

        my $dspRepAmt = $shared->fmtNumber( $amtrep );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "Repetitions", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_rep * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $pRepCnt, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dspRepAmt, "R" );

        $y -= $ySpc;

        my $dspXFRAmt = $shared->fmtNumber( $amtxfr );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "Cross-file Repetitions", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_xfr * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $pXFRCnt, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dspXFRAmt, "R" );

        $y -= $ySpc;

        my $dsp100Amt = $shared->fmtNumber( $amt100 );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "100\%", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_100 * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $p100Cnt, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dsp100Amt, "R" );

        $y -= $ySpc;

        my $dsp95Amt = $shared->fmtNumber( $amt95 );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "95\% - 99\%", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_95 * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $p95Cnt, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dsp95Amt, "R" );

        $y -= $ySpc;

        my $dsp85Amt = $shared->fmtNumber( $amt85 );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "85\% - 94\%", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_85 * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $p85Cnt, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dsp85Amt, "R" );

        $y -= $ySpc;

        my $dsp75Amt = $shared->fmtNumber( $amt75 );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "75\% - 84\%", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_75 * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $p75Cnt, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dsp75Amt, "R" );

        $y -= $ySpc;

        my $dsp50Amt = $shared->fmtNumber( $amt50 );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "50\% - 74\%", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_50 * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $p50Cnt, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dsp50Amt, "R" );

        $y -= $ySpc;

        my $dspNewAmt = $shared->fmtNumber( $amtnew );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "No Match", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_new * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $pNoCnt, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dspNewAmt, "R" );

        $y -= $ySpc;

        my $dspABSAmt = $shared->fmtNumber( $amtabs );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "AdaptiveMT Baseline", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_abs * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $pABase, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dspABSAmt, "R" );

        $y -= $ySpc;

        my $dspALNAmt = $shared->fmtNumber( $amtaln );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[3], $y, 1, "AdaptiveMT with Learnings", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[3], $tab[4], $y, 1, ( $wt_aln * 100 ) . "\%", "R" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $pALrn, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dspALNAmt, "R" );
        
        $y -= $ySpc;

        my $dspTAmt = $shared->fmtNumber( $amtTot );
        &doBoxText( $gfx1, $text_value1, $tab[1], $tab[4], $y, 1, "TRANSLATION TOTAL", "L" );
        &doBoxText( $gfx1, $text_value1, $tab[4], $tab[5], $y, 1, $wCnt, "R" );
        &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dspTAmt, "R" );

        if ( $amtTot < $pRate ) {

            $y -= $ySpc;

            my $dspPAmt = $shared->fmtNumber( $pRate );
            &doBoxText( $gfx1, $text_value1, $tab[1], $tab[5], $y, 1, "STANDARD MINIMUM RATE", "L" );
            &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dspPAmt, "R" );
            
            if ( $doDMin eq "Y" ) {
                
                $y -= $ySpc;
                
                my $dpAmt = ( $pRate * ( $mdRate / 100 ) );
                my $dspDPAmt = $shared->fmtNumber( $dpAmt );
                &doBoxText( $gfx1, $text_value1, $tab[1], $tab[5], $y, 1, "APPLIED DISCOUNTED RATE", "L" );
                &doBoxText( $gfx1, $text_value1, $tab[5], $tab[6], $y, 1, $dspDPAmt, "R" );
            }
                
        }

        $y = 28;
        #my $ftLine1 = "$add1 - $add2";
        #my $ftLine2 = "Tel $phn - Fax $fax - $sdtURL | $mtfURL";
        $text_sml1->translate( $mid, $y );
        $text_sml1->text_center( $ftLine1 );
        $y -= 9;
        $text_sml1->translate( $mid, $y );
        $text_sml1->text_center( $ftLine2 );
        
    }
    
    $pdf->saveas( "$fullpdf" );
    
    $pdf->end();
    
    system( "chmod 644 $fullpdf" );

    if ( $debug ) {
        close( DEBUG );
    }
    
    return $pdfURL;
}

###########################################################                   
# Create Box with right aligned label
#----------------------------------------------------------
sub doBoxText {

    my ( $gfx, $text, $x1, $x2, $y, $rows, $label, $align ) = @_;
    
    $gfx->move( $x1, $y - ( ( $ySpc * $rows ) + 4 - $ySpc ) );
    $gfx->line( $x2, $y - ( ( $ySpc * $rows ) + 4 - $ySpc ) );
    $gfx->line( $x2, $y + ( $ySpc - 4 ) );
    $gfx->line( $x1, $y + ( $ySpc - 4 ) );
    $gfx->close;
    $gfx->stroke;

    if ( $label gt "" ) {
        if ( $align eq "R" ) {
            $text->translate( $x2 - 5, $y );
            $text->text_right( $label );
        } else {
            $text->translate( $x1 + 5, $y );
            $text->text( $label );
        }
    }
} 

###########################################################                   
# Create paragraph of wrapped text
#----------------------------------------------------------
sub doTextWrap {

    my ( $text, $x1, $x2, $y1, $info, $yS ) = @_;

    my $w1 = ( $x2 - 5 ) - ( $x1 + 5 );

	my $result = 0;
	my $line   = "";
	my $pLine  = "";
	my $pWord  = "";

    
    if ( $yS eq "" ) {
        $yS = $ySpc;
    }
    
    my $y  = $y1;

	$info =~ s/[ \t]+/ /g;
	$info =~ s/\r\n|\r|\n/ <br> /g;
	$info =~ s/\(/\[/g;
	$info =~ s/\)/\]/g;

	my @words  = split( " ", $info );
	foreach my $word ( @words ) {
	    if ( ( $text->advancewidth( $line ) > $w1 ) || ( $word eq "<br>" ) ) {
	        $text->translate( $x1 + 5, $y );
	        if ( $word eq "<br>" ) {
	            $text->text( $line );
	            $word = "";
	            $line = "";
	        } else {
	            $text->text( $pLine );
	            $line = $pWord;
	        }
	        $y -= $yS;
	        $result++;
	    }
	    $pLine = $line;
	    if ( $line eq "" ) {
	    	$line .= $word;
	    } else {
	    	$line .= " " . $word;
	    }
	    $pWord = $word;
	}
	
	if ( $line gt "" ) {
	    $text->translate( $x1 + 5, $y );
	    if ( $text->advancewidth( $line ) > $w1 ) {
	        $text->text( $pLine );
	        $y -= $yS;
	        $text->translate( $x1 + 5, $y );
	        $text->text( $pWord );
	        $result += 2;
	    } else {
	        $text->translate( $x1 + 5, $y );
	        $text->text( $line );
		    $result++;
		}
	}
	
	return $result;

}

###########################################################                   
# Convert Factor to regular Spacing
#----------------------------------------------------------
sub convertSpc {

    my ( $factor, $spacing ) = @_;
    
    my $used   = ( $spacing * $factor );
    my $result = ( int( $used / $ySpc ) + 2 );
    
    return $result;

}

1;
