#!/usr/bin/perl
########################################
# This package provides PDF reports for 
# the SDT Quote system
#
# Created: 02/01/2014 - Eric Huber
###########################################################################
# Copyright 2014, P. Eric Huber
###########################################################################
use PDF::API2;
use Date::Calc qw( :all );
use strict;

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;

my $shared;
my $ucode;
my $src_path;
my $color_head1;
my $color_head2;
my $color_bg1;
my $color_bg2;
my $color_bg3;
my $color_error;
my $img_path;
my $site_title;
my $pdf_path;
my $pdf_url;
my ( $now_year, $now_month, $now_day ) = Today();

my $java_msg = "";

my $debug = 0;

my $f1;
my $f2;
my $f3;
my $ptReg = 9;
my $ptSml = 8;
my $ptMed = 14;
my $ptLrg = 20;
my @tab   = ( 50, 170, 230, 350, 450, 490, 570 );
my @bTab  = ( 50, 350, 410, 470, 570 );
my $bFill = "#BBB";
my $tCP   = 310;
my $y;


my $ySpc  = 11;
my $bSpc  = 14;
my $tSpc  = 10;


package qtepdf;

# -------------------------------------------------------------------------------------
# The constructor   
# -------------------------------------------------------------------------------------
sub new {
    
    my ( $class, $sh_in ) = @_;
    
    my $self = {};
    
    my $config    = new config;
    $src_path     = $config->get_src_path;
    my (%colors)  = $config->get_colors;
    $color_head1  = $colors{ "HEAD1" };
    $color_head2  = $colors{ "HEAD2" };
    $color_bg1    = $colors{ "BG1" };
    $color_bg2    = $colors{ "BG2" };
    $color_bg3    = $colors{ "BG3" };
    $color_error  = $colors{ "ERROR" };
    $img_path     = $config->get_img_path;
    $site_title   = $config->get_app_name;
    $pdf_path     = $config->get_pdf_path;
    $pdf_url      = $config->get_pdf_url;
    $debug        = $config->get_debug;
    
    bless ( $self, $class );

    $shared  = $sh_in;
    
    return $self;
}
# -------------------------------------------------------------------------------------

###########################################################                   
# Generate the Quote in PDF format
#----------------------------------------------------------
sub genQuote {
	
	my ( $self, $qte_id ) = @_;
	     
	my $rec    = $shared->get_rec( "quote", "qte_id", $qte_id );
    my $cln_id = $rec->{qte_cln_id};
    my $clc_id = $rec->{qte_clc_id};
    my $pjno   = $rec->{qte_prj_no};
    my $qtDt   = $shared->getDateFmt( $rec->{qte_add_date} );
    my $qBrand = $rec->{qte_brand};
    my $qtUsr  = $rec->{qte_add_usr_id};
    my $deliv  = $rec->{qte_delivery};
    my $qAmt   = $rec->{qte_tot_amount};
    my $cmnt   = $rec->{qte_comment};
    
    my $clnRec = $shared->get_rec( "client", "cln_id", $cln_id );
    my $clnOth = $clnRec->{cln_other};
    my $cName  = $clnRec->{cln_name};
    my $cCode  = $clnRec->{cln_code};
    
    my $ccName = "";
    my $ccAdd  = "";
    my $ccAdd2 = "";
    my $ccPhn  = "";
    
    if ( $clnOth eq "N" ) {
        my $clcRec = $shared->get_rec( "client_contact", "clc_id", $clc_id );
        $ccName    = $clcRec->{clc_contact};
        $ccAdd     = $clcRec->{clc_address};
        my $ccCity = $clcRec->{clc_city};
        my $ccSt   = $clcRec->{clc_state};
        my $ccZip  = $clcRec->{clc_zip};
        $ccPhn     = $clcRec->{clc_phone};
    
        $ccAdd2    = $ccCity . ", " . $ccSt . " " . $ccZip;
        if ( $ccSt eq "" ) {
            $ccAdd2 = $ccCity . " " . $ccZip;
        }
    } else {
        $cName   = $rec->{qte_oth_name};
        $ccName  = $rec->{qte_oth_contact};
        $ccAdd   = $rec->{qte_oth_addr_1};
        $ccAdd2  = $rec->{qte_oth_addr_2};
        $ccPhn   = $rec->{qte_oth_phone};
    }
        
	my $comp   = $shared->getConfigString( "ADM_POCOMP" );
	my $add1   = $shared->getConfigString( "ADM_POADDR1" );
	my $add2   = $shared->getConfigString( "ADM_POADDR2" );
	my $phn    = $shared->getConfigString( "ADM_POPHONE" );
	my $sdtEml = $shared->getConfigString( "ADM_QTEML" );
	my $mfPhn  = $shared->getConfigString( "ADM_POPHONEMF" );
	my $mfEml  = $shared->getConfigString( "ADM_QTEMLMF" );
    my $wph    = $shared->getConfigInt( "ADM_WPH" );
    my $qrd    = $shared->getConfigText( "ADM_QTTRRATE" );
    my $yStop  = $shared->getConfigInt( "ADM_PDFYSTOP" );
	
	my $urec   = $shared->get_rec( "user", "usr_id", $qtUsr );
	my $uName  = $urec->{usr_name};
	
	my $fullpdf    = "$pdf_path/" . $pjno . ".pdf";
	my $pdfURL     = "$pdf_url/" . $pjno . ".pdf";
	
    my $pdf  = PDF::API2->new;
    $f1      = $pdf->corefont( 'Verdana' );
    $f2      = $pdf->corefont( 'Verdana-Bold' );
    $f3      = $pdf->corefont( 'Helvetica-Oblique' );

    my ( $page, $gfx, $info, $h1, $bold, $foot, $label ); 
    $page    = $pdf->page;
    $page->mediabox( 612, 792 );
    $gfx     = $page->gfx;
    $info    = &setFontProps( $page, "INFO" );
    $h1      = &setFontProps( $page, "H1" ); 
    $label   = &setFontProps( $page, "LABEL" );
    $bold    = &setFontProps( $page, "BOLD" );
    $foot    = &setFontProps( $page, "FOOT" );
    
    my $logo;
    my $dspPhn;
    my $dspEml;
    if ( $qBrand eq "SDT" ) {
        $logo   = $pdf->image_png( "$src_path/logo.png" );
        $dspPhn = $phn;
        $dspEml = $sdtEml;
    } elsif ( $qBrand eq "MF" ) {
        $logo   = $pdf->image_png( "$src_path/logo_mf.png" );
        $dspPhn = $mfPhn;
        $dspEml = $mfEml;
    }
    $gfx->image( $logo, $tab[0], 700 );
    
    &rightText( $h1, $tab[6], 725, "QUOTE" );

    $y = 685;

    &leftText( $label, $tab[0], $y, $add1 );
    &rightText( $info, $tab[6], $y, "DATE: " . $qtDt );
    
    $y -= $ySpc;
    
    &leftText( $label, $tab[0], $y, $add2 );
    
    $y -= $ySpc;

    &leftText( $label, $tab[0], $y, $dspPhn );

    $y -= $ySpc;
        
    &leftText( $label, $tab[0], $y, $dspEml );
    
    $y -= ( $ySpc * 2 );
    
    &leftText( $label, $tab[0] + 5, $y, "TO" );
    &leftText( $info, $tab[0] + 25, $y, $cName );
    
    $y -= $ySpc;
    
    &leftText( $info, $tab[0] + 25, $y, $ccAdd );
    
    $y -= $ySpc;
    
    &leftText( $info, $tab[0] + 25, $y, $ccAdd2 );
    
    $y -= $ySpc;
    
    &leftText( $info, $tab[0] + 25, $y, $ccPhn );
    
    $y -= $ySpc;
    
    if ( $clnOth eq "N" ) {
        &leftText( $info, $tab[0] + 25, $y, "Customer ID: " . $cCode );
    }

    $y -= ( $ySpc * 2 );
    
    &doBox4S1( $gfx, $bold, $y, "SALES PERSON", "CUSTOMER CONTACT", "PROJECT NUMBER", "DELIVERY DATE", 1 );
    
    $y -= $bSpc;
    
    &doBox4S1( $gfx, $info, $y, $uName, $ccName, $pjno, $deliv, 0 );
    
    $y -= ( $ySpc * 2 );
    
    if ( $cmnt gt "" ) {
        &doBoxText( $gfx, $bold, $tab[0], $tab[6], $y, 1, "COMMENTS", "L", $bFill );
        $y -= $bSpc;
        my $factor = &doTextWrap( $info, $tab[0], $tab[6], $y, $cmnt, $tSpc );
        my $rows   = &convertSpc( $factor, $tSpc );
        &doBoxText( $gfx, $info, $tab[0], $tab[6], $y, $rows, "", "L" );
        $y -= ( $bSpc * ( $rows + 1 ) );
    }
     
    &doBox4S2( $gfx, $bold, $y, "DESCRIPTION", "TOT QTY", "RATE", "LINE TOTAL (USD)", 1 );
    $y -= $bSpc;
    
    my $rw     = 0;
    my $rwFill = "";
    
    my $curPg  = 1;    
    my $SQL    = "SELECT qtl_type, qtl_desc, qtl_qty, qtl_rate, qtl_amount FROM quote_line\n"
               . "WHERE qtl_qte_id = $qte_id\n"
               . "ORDER BY qtl_type";
    my @rs     = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        
        my $iType = $rec->{qtl_type};
        my $iDesc = $rec->{qtl_desc};
        my $iQty  = $rec->{qtl_qty};
        my $iRate = $rec->{qtl_rate};
        my $iAmt  = sprintf("%.2f", $rec->{qtl_amount});
        
        if ( $y < $yStop ) {
            if ( $curPg > 0 ) {
                &leftText( $foot, $tab[0], 25, $pjno . " - Page: $curPg" );
            }

            $page    = $pdf->page;
            $page->mediabox( 612, 792 );
            $gfx     = $page->gfx;
            $info    = &setFontProps( $page, "INFO" );
            $h1      = &setFontProps( $page, "H1" ); 
            $label   = &setFontProps( $page, "LABEL" );
            $bold    = &setFontProps( $page, "BOLD" );
            $foot    = &setFontProps( $page, "FOOT" );
            $gfx->image( $logo, $tab[0], 700 );
    
            &rightText( $h1, $tab[6], 725, "QUOTE" );
            &rightText( $label, $tab[6], 710, "(continued)" );

            $y = 675;
            &doBox4S2( $gfx, $bold, $y, "DESCRIPTION", "TOT QTY", "RATE", "LINE TOTAL (USD)", 1 );
            $y -= $bSpc;

            $rw = 0;
            $curPg++;
        }

        if ( $rw == 1 ) {
            $rwFill = "#EEE";
            $rw = 0;
        } else {
            $rwFill = "";
            $rw = 1;
        }
        
        my $dspQty = $iQty;
        my $dspRt  = $iRate;
        my $dspAmt = $iAmt;
        if ( ( $iType eq "D-OTH" ) || ( $iType eq "E-DSC" ) || ( $iType eq "F-PCT" ) ) {
            $dspQty = "";
            $dspRt  = "";
            if ( $iType eq "E-DSC" ) {
                $dspAmt = "-" . $iAmt;
            }
            if ( $iType eq "F-PCT" ) {
                $dspRt  = $shared->fmtNumber( $iRate, 1 ) . "\%";
                $dspAmt = "-" . $iAmt;
            }
        } elsif ( $iType eq "A-TR" ) {
            $dspRt  = $iRate . "*";
        } elsif ( $iType eq "C-PF" ) {
            my $hours = 0;
            if ( ( $iQty / $wph ) == int( $iQty / $wph ) ) {
                $hours = int( $iQty / $wph );
            } else {
                $hours = int( ( $iQty / $wph ) + 1 );
            }        
            $dspQty = $hours . " (Hrs)";
            $dspRt  = $shared->fmtNumber( $iRate );
        }
        
        my @desc   = &doTextWrapArray( $info, $bTab[0], $bTab[1], $iDesc );
        my $rows   = @desc;
        &doBoxTextLines( $gfx, $info, $bTab[0], $bTab[1], $y, $rows, \@desc, "L", $rwFill, $bFill );
        &doBoxText( $gfx, $info, $bTab[1], $bTab[2], $y, $rows, $dspQty, "R", $rwFill, $bFill );
        &doBoxText( $gfx, $info, $bTab[2], $bTab[3], $y, $rows, $dspRt, "R", $rwFill, $bFill );
        &doBoxText( $gfx, $info, $bTab[3], $bTab[4], $y, $rows, $dspAmt, "R", $rwFill, $bFill );
#       &leftText( $info, $bTab[1] + 5, $y, "$y" );
        $y -= ( $bSpc * $rows );
    }    
        
    &doBoxText( $gfx, $info, $bTab[2], $bTab[3], $y, 1, "SUBTOTAL", "R", "", $bFill );
    &doBoxText( $gfx, $info, $bTab[3], $bTab[4], $y, 1, $shared->fmtNumber( $qAmt ), "R", "", $bFill );
    $y -= $bSpc;
    my $fact2 = &doTextWrap( $foot, $bTab[0], $bTab[2], $y, "* $qrd", 9 );
    &doBoxText( $gfx, $info, $bTab[2], $bTab[3], $y, 1, "SALES TAX", "R", "", $bFill );
    &doBoxText( $gfx, $info, $bTab[3], $bTab[4], $y, 1, "", "R", "", $bFill );
    $y -= $bSpc;
    &doBoxText( $gfx, $bold, $bTab[2], $bTab[3], $y, 1, "TOTAL", "R", "", $bFill );
    &doBoxText( $gfx, $bold, $bTab[3], $bTab[4], $y, 1, $shared->fmtNumber( $qAmt ), "R", "", $bFill );
    $y -= ( $bSpc * 2 );
    
    if ( $fact2 > 3 ) {
        my $lCnt = convertSpc( ( $fact2 - 3 ), 9 );
        $y -= ( $ySpc * $lCnt);
    }
    
    &leftText( $foot, $tab[0], $y, "Quotation prepared by: $uName" );
    $y -= ( $bSpc * 2 );
    &leftText( $foot, $tab[0], $y, "To accept this quotation, sign here and return: ____________________________________________________________" );
    
    
    my $mid   = $tab[0] + int( ( $tab[6] - $tab[0] ) / 2 );    

    $y = 28;
    my $ftLine = "Thank you for your business!";
    $info->translate( $mid, $y );
    $info->text_center( $ftLine );

    if ( $curPg > 1 ) {
        &leftText( $foot, $tab[0], 25, $pjno . " - Page: $curPg" );
    }
    
    $pdf->saveas( "$fullpdf" );
    
    $pdf->end();
    
    system( "chmod 644 $fullpdf" );

    return $pdfURL;
}

###########################################################                   
# Create Box with right aligned label
#----------------------------------------------------------
sub doBoxText {

    my ( $gfx, $text, $x1, $x2, $y, $rows, $label, $align, $fColor, $sColor ) = @_;

    if ( $fColor gt "" ) {
        $gfx->fillcolor( $fColor );
    }
    if ( $sColor gt "" ) {
        $gfx->strokecolor( $sColor );
    }
    $gfx->move( $x1, $y - ( ( $bSpc * $rows ) + 4 - $bSpc ) );
    $gfx->line( $x2, $y - ( ( $bSpc * $rows ) + 4 - $bSpc ) );
    $gfx->line( $x2, $y + ( $bSpc - 4 ) );
    $gfx->line( $x1, $y + ( $bSpc - 4 ) );
    $gfx->close;
    if ( $fColor gt "" ) {
        $gfx->fillstroke;
    } else {
        $gfx->stroke;
    }

    if ( $label gt "" ) {
        if ( $align eq "R" ) {
            $text->translate( $x2 - 5, $y );
            $text->text_right( $label );
        } else {
            $text->translate( $x1 + 5, $y );
            $text->text( $label );
        }
    }
} 

###########################################################                   
# Create Box with right aligned label
#----------------------------------------------------------
sub doBoxTextLines {

    my ( $gfx, $text, $x1, $x2, $y1, $rows, $lines, $align, $fColor, $sColor, $inSpc ) = @_;

    if ( $inSpc eq "" ) {
        $inSpc = $ySpc;
    }
    
    my $y = $y1;
    
    if ( $fColor gt "" ) {
        $gfx->fillcolor( $fColor );
    }
    if ( $sColor gt "" ) {
        $gfx->strokecolor( $sColor );
    }
    $gfx->move( $x1, $y - ( ( $bSpc * $rows ) + 4 - $bSpc ) );
    $gfx->line( $x2, $y - ( ( $bSpc * $rows ) + 4 - $bSpc ) );
    $gfx->line( $x2, $y + ( $bSpc - 4 ) );
    $gfx->line( $x1, $y + ( $bSpc - 4 ) );
    $gfx->close;
    if ( $fColor gt "" ) {
        $gfx->fillstroke;
    } else {
        $gfx->stroke;
    }

    foreach my $line ( @$lines ) {
        if ( $align eq "R" ) {
            $text->translate( $x2 - 5, $y );
            $text->text_right( $line );
        } else {
            $text->translate( $x1 + 5, $y );
            $text->text( $line );
        }
        $y -= $inSpc;
    }
}

###########################################################                   
# Create paragraph of wrapped text
#----------------------------------------------------------
sub doTextWrap {

    my ( $text, $x1, $x2, $y1, $info, $yS ) = @_;

    my $w1 = ( $x2 - 5 ) - ( $x1 + 5 );

	my $result = 0;
	my $line   = "";
	my $pLine  = "";
	my $pWord  = "";

    
    if ( $yS eq "" ) {
        $yS = $ySpc;
    }
    
    my $y  = $y1;

	$info =~ s/[ \t]+/ /g;
	$info =~ s/\r\n|\r|\n/ <br> /g;
	$info =~ s/\(/\[/g;
	$info =~ s/\)/\]/g;

	my @words  = split( " ", $info );
	foreach my $word ( @words ) {
	    if ( ( $text->advancewidth( $line ) > $w1 ) || ( $word eq "<br>" ) ) {
	        $text->translate( $x1 + 5, $y );
	        if ( $word eq "<br>" ) {
	            $text->text( $line );
	            $word = "";
	            $line = "";
	        } else {
	            $text->text( $pLine );
	            $line = $pWord;
	        }
	        $y -= $yS;
	        $result++;
	    }
	    $pLine = $line;
	    if ( $line eq "" ) {
	    	$line .= $word;
	    } else {
	    	$line .= " " . $word;
	    }
	    $pWord = $word;
	}
	
	if ( $line gt "" ) {
	    $text->translate( $x1 + 5, $y );
	    if ( $text->advancewidth( $line ) > $w1 ) {
	        $text->text( $pLine );
	        $y -= $yS;
	        $text->translate( $x1 + 5, $y );
	        $text->text( $pWord );
	        $result += 2;
	    } else {
	        $text->translate( $x1 + 5, $y );
	        $text->text( $line );
		    $result++;
		}
	}
	
	return $result;

}

###########################################################                   
# Create paragraph of wrapped text
#----------------------------------------------------------
sub doTextWrapArray {

    my ( $text, $x1, $x2, $data ) = @_;

    my $w1 = ( $x2 - 5 ) - ( $x1 + 5 );

	my @result;
	my $line   = "";
	my $pLine  = "";
	my $pWord  = "";

    
	$data =~ s/[ \t]+/ /g;
	$data =~ s/\r\n|\r|\n/ <br> /g;
	$data =~ s/\(/\[/g;
	$data =~ s/\)/\]/g;

	my @words  = split( " ", $data );
	foreach my $word ( @words ) {
	    if ( ( $text->advancewidth( $line ) > $w1 ) || ( $word eq "<br>" ) ) {
	        if ( $word eq "<br>" ) {
	            push( @result, $line );
	            $word = "";
	            $line = "";
	        } else {
	            push( @result, $pLine );
	            $line = $pWord;
	        }
	    }
	    $pLine = $line;
	    if ( $line eq "" ) {
	    	$line .= $word;
	    } else {
	    	$line .= " " . $word;
	    }
	    $pWord = $word;
	}
	
	if ( $line gt "" ) {
	    if ( $text->advancewidth( $line ) > $w1 ) {
	        push( @result, $pLine );
	        push( @result, $pWord );
	    } else {
	        push( @result, $line );
		}
	}
	
	return @result;

}

###########################################################                   
# Convert Factor to regular Spacing
#----------------------------------------------------------
sub convertSpc {

    my ( $factor, $spacing ) = @_;
    
    my $used   = ( $spacing * $factor );
    my $result = ( int( $used / $ySpc ) + 1 );
    
    return $result;

}

###########################################################                   
# Create 4 column box - style 1
#----------------------------------------------------------
sub doBox4S1 {

    my ( $gfx, $txtObj, $row, $c1, $c2, $c3, $c4, $shade ) = @_;
    
    if ( $shade ) {
                
        $gfx->fillcolor( $bFill );
        $gfx->strokecolor( $bFill );
        $gfx->move( $tab[0], $row - 4 );
        $gfx->line( $tab[0], $row + ( $bSpc - 2 ) );
        $gfx->line( $tab[6], $row + ( $bSpc - 2 ) );
        $gfx->line( $tab[6], $row - 4 );
        $gfx->close;
        $gfx->fillstroke;
        
    } else {
        
        $gfx->strokecolor( $bFill );
        $gfx->move( $tab[0], $row - 4 );
        $gfx->line( $tab[0], $row + ( $bSpc - 2 ) );
        $gfx->line( $tab[6], $row + ( $bSpc - 2 ) );
        $gfx->line( $tab[6], $row - 4 );
        $gfx->close;
        $gfx->stroke;
        
        $gfx->move( $tab[1], $row - 4 );
        $gfx->line( $tab[1], $row + ( $bSpc - 2 ) );
        $gfx->line( $tab[4], $row + ( $bSpc - 2 ) );
        $gfx->line( $tab[4], $row - 4 );
        $gfx->close;
        $gfx->stroke;
        
        $gfx->move( $tab[3], $row - 4 );
        $gfx->line( $tab[3], $row + ( $bSpc - 2 ) );
        $gfx->close;
        $gfx->stroke;
        
    }

    &leftText( $txtObj, $tab[0] + 5, $row, $c1 );
    &leftText( $txtObj, $tab[1] + 5, $row, $c2 );
    &leftText( $txtObj, $tab[3] + 5, $row, $c3 );
    &leftText( $txtObj, $tab[4] + 5, $row, $c4 );
    
}

###########################################################                   
# Create 4 column box - style 2
#----------------------------------------------------------
sub doBox4S2 {

    my ( $gfx, $txtObj, $row, $c1, $c2, $c3, $c4 ) = @_;
    
    $gfx->fillcolor( $bFill );
    $gfx->strokecolor( $bFill );
    $gfx->move( $tab[0], $row - 4 );
    $gfx->line( $tab[0], $row + ( $bSpc - 2 ) );
    $gfx->line( $tab[6], $row + ( $bSpc - 2 ) );
    $gfx->line( $tab[6], $row - 4 );
    $gfx->close;
    $gfx->fillstroke;

    &leftText( $txtObj, $bTab[0] + 5, $row, $c1 );
    &rightText( $txtObj, $bTab[2] - 5, $row, $c2 );
    &rightText( $txtObj, $bTab[3] - 5, $row, $c3 );
    &rightText( $txtObj, $bTab[4] - 5, $row, $c4 );
    
}      

###########################################################                   
# Print left aligned text
#----------------------------------------------------------
sub leftText {

    my ( $txtObj, $x, $y, $val ) = @_;
    
    $txtObj->translate( $x, $y );
    $txtObj->text( $val );

}

###########################################################                   
# Print right aligned text
#----------------------------------------------------------
sub rightText {

    my ( $txtObj, $x, $y, $val ) = @_;
    
    $txtObj->translate( $x, $y );
    $txtObj->text_right( $val );

}

###########################################################                   
# Setup Font Properties
#----------------------------------------------------------
sub setFontProps {

    my ( $page, $type ) = @_;
    
    my $result = $page->text;
    if ( $type eq "INFO" ) {
        $result->font( $f1, $ptReg );
        $result->fillcolor( "#000" );
    } elsif ( $type eq "H1" ) {
        $result->font( $f2, $ptLrg );
        $result->fillcolor( "#AAA" );
    } elsif ( $type eq "H2" ) {
        $result->font( $f2, $ptMed );
        $result->fillcolor( "#000" );
    } elsif ( $type eq "H3" ) {
        $result->font( $f2, $ptMed );
        $result->fillcolor( "#000" );
    } elsif ( $type eq "FOOT" ) {
        $result->font( $f1, $ptSml );
        $result->fillcolor( "#000" );
    } elsif ( $type eq "LABEL" ) {
        $result->font( $f2, $ptReg );
        $result->fillcolor( "#AAA" );
    } elsif ( $type eq "BOLD" ) {
        $result->font( $f2, $ptReg );
        $result->fillcolor( "#000" );
    }
    
    return $result;
}

1;
