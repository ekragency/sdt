#!/usr/bin/perl
########################################
# This package provides user 
# user authentication and password
# request subroutines.
#
# Created: 10/13/2004 - Eric Huber
###########################################################################
# Copyright 2004, P. Eric Huber
###########################################################################

use DBI;
use CGI;
use Number::Format;
use Date::Calc qw( :all );
use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;

my $dbh;
my %my_cookies;
my $cgi_url;
my $done_cgi_head;
my $cgiObject;
my $debug=0;
my $color_head1;
my $color_head2;
my $color_head3;
my $color_bg1;
my $color_bg2;
my $color_bg3;
my $color_bg4;
my $color_error;
my $script_path;
my $img_path;
my $base_url;
my $js_path;
my $css_path;
my $src_path;
my $nf = new Number::Format();
my ( $now_year, $now_month, $now_day ) = Today();

my @months = ( "JAN", "FEB", "MAR", "APR", "MAY", "JUN",
               "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" );
            
my $moddate = localtime( time );               

my %MonthDesc;
$MonthDesc{ "01" } = "January";
$MonthDesc{ "02" } = "February";
$MonthDesc{ "03" } = "March";
$MonthDesc{ "04" } = "April";
$MonthDesc{ "05" } = "May";
$MonthDesc{ "06" } = "June";
$MonthDesc{ "07" } = "July";
$MonthDesc{ "08" } = "August";
$MonthDesc{ "09" } = "September";
$MonthDesc{ "10" } = "October";
$MonthDesc{ "11" } = "November";
$MonthDesc{ "12" } = "December";

my @MonthDays = ( "01".."31" );

my $startYear = 1900;


package shared;

# -------------------------------------------------------------------------------------
# The constructor   
# -------------------------------------------------------------------------------------
sub new {
    
    my $self = {};
    my $config = new config;

    $script_path      = $config->get_script_path;
    $cgi_url          = "$script_path/site.cgi";
    my (%colors)      = $config->get_colors;
    $color_head1      = $colors{ "HEAD1" };
    $color_head2      = $colors{ "HEAD2" };
    $color_head3      = $colors{ "HEAD3" };
    $color_bg1        = $colors{ "BG1" };
    $color_bg2        = $colors{ "BG2" };
    $color_bg3        = $colors{ "BG3" };
    $color_bg4        = $colors{ "BG4" };
    $color_error      = $colors{ "ERROR" };
    $img_path         = $config->get_img_path;
    $base_url         = $config->get_base_url;
    $js_path          = $config->get_js_path;
    $css_path         = $config->get_css_path;
    $src_path         = $config->get_src_path;
    $debug            = $config->get_debug;
    
    $dbh       = DBI->connect( $config->get_dbconnect, $config->get_dbuser, $config->get_dbpass ) ||
                  die "Could not connect to Database: $DBI::errstr";

    
    bless $self;              

    $cgiObject     = new CGI;
    $done_cgi_head = 0;
    
    return $self;
}
# -------------------------------------------------------------------------------------

############################################################
# Error Handler
#-----------------------------------------------------------
sub do_err {

    my ( $self, $pg_body, $template ) = @_;    
    my ( $page_dtls );
    
    if ( $template gt "" ) {
    	$page_dtls->{template}  = $template;
    } else {
        $page_dtls->{template}  = "admin.html";
    }
    $page_dtls->{html_title}    = "Error Report";
    $page_dtls->{submenu_links} = "<span class=\"subtitle\">ERROR REPORT</span>";
    $page_dtls->{page_body}     = $pg_body;
    
    $self->page_gen( $page_dtls, "" );
}

############################################################
# Success Handler
#-----------------------------------------------------------
sub do_ok {

    my ( $self, $pg_body, $template ) = @_;    
    my ( $page_dtls );
    
    if ( $template gt "" ) {
    	$page_dtls->{template}  = $template;
    } else {
        $page_dtls->{template}  = "admin.html";
    }
    $page_dtls->{html_title}    = "Success";
    $page_dtls->{submenu_links} = "<span class=\"subtitle\">SUCCESS!</span>";
    $page_dtls->{page_body}     = $pg_body;
    
    $self->page_gen( $page_dtls, "" );
}



###########################################################                   
# Generate the page
#----------------------------------------------------------
sub page_gen {
	
    my ( $self, $page_dtls, $java_msg ) = @_;
    
    my $config       = new config;
    
    my $src_path     = $config->get_src_path;
    my $script_path  = $config->get_script_path;
    my $template     = "$src_path/admin.html";
    my $base_href    = $config->get_base_url;
    my $css_path     = $config->get_css_path;
    my $js_path      = $config->get_js_path;
    my $site_title   = $config->get_app_name;
    my $app_name     = $config->get_app_name;
    my $app_ver      = $config->get_app_ver;
    my $copyrt       = "Copyright &copy; $now_year - Same Day Translations.  All Rights Reserved.";
    my $parent_page  = $page_dtls->{page_code};
    my $doFade       = 0;
    
    $page_dtls->{copyright}       = $copyrt;
    $page_dtls->{nav_links}       = $self->getPageLinks( $parent_page );
    
    $page_dtls->{page_url}        = "$cgi_url?action=view_page&page=";
    
   
    if ( $page_dtls->{template} gt "" ) {
        $template = $src_path . "/" . $page_dtls->{template};
    }
    
    if ( $page_dtls->{doFade} eq "Y" ) {
        $doFade = 1;
    }
          
    if ( ! $done_cgi_head ) {
        &cgi_header;
    }
   
    my $css_url      = "$css_path/default.css";
    if ( $page_dtls->{css} gt "" ) {
        $css_url     = "$css_path/" . $page_dtls->{css};
    }
    
    $page_dtls->{html_title}   = $site_title unless ( $page_dtls->{html_title} );
    $page_dtls->{app_name}     = $app_name;
    $page_dtls->{img_path}     = $img_path;
    $page_dtls->{script_path}  = $script_path;
    $page_dtls->{color_head1}  = $color_head1;
    $page_dtls->{color_head2}  = $color_head2;
    $page_dtls->{color_head3}  = $color_head3;
    $page_dtls->{color_bg1}    = $color_bg1;
    $page_dtls->{color_bg2}    = $color_bg2;
    $page_dtls->{color_bg3}    = $color_bg3;
    $page_dtls->{color_bg4}    = $color_bg4;
    $page_dtls->{java_fade}    = "&nbsp;";

    open (FILE, "<$template") or die "Couldn't open $template: $!";
    
    while (<FILE>) {
        if ( $_ =~ /^<!--<BASE HREF.+/ ) {
            # print "<BASE HREF=\"$base_href/\">\n";
            print "<link HREF=\"$css_url\" REL=\"styleSheet\" TYPE=\"text/css\">\n";
            # print "<script type=\"text/javascript\" src=\"$js_path/oodomimagerollover.js\">\n";
            # print "/***********************************************\n";
            # print "* DOM Image Rollover II- By Adam Smith (http://www.codevendor.com)\n";
            # print "* Script featured on and available at Dynamic Drive (http://www.dynamicdrive.com)\n";
            # print "* Keep this notice intact for usage please\n";
            # print "***********************************************/\n";
            # print "</script>\n";
            
            if ( $page_dtls->{javascript} gt "" ) {
                print $page_dtls->{javascript} . "\n";
            }
            
            if ( $doFade ) {
                print "<script src=\"$js_path/jquery.min.js\"></script>\n";
                print "<script>\n";
                print "\$( function () {\n";
                print "\$('.faderdiv').fadeIn('slow', function () {\n";
                print "\$(this).delay(3000).fadeOut('slow');\n";
                print "});\n";
                print "});\n";    
                print "</script>\n";
            }

            
            if ( $page_dtls->{open_file} gt "" ) {
                print "<META HTTP-EQUIV=\"Refresh\" Content=\"0; URL=" . $page_dtls->{open_file} . "\">\n";
            }
            if ( $page_dtls->{download} gt "" ) {
                print "<META HTTP-EQUIV=\"Refresh\" Content=\"0; URL=$script_path/download.cgi?file=" . $page_dtls->{download} . "\">\n";
            }
            
            
        } elsif ( $_ =~ /^onLoad=""/ ) {
            if ( ( $java_msg gt "" ) && ( $doFade ) ) {
                $page_dtls->{java_fade} = "<div class=\"faderdiv\">$java_msg</div>";
                if ( $page_dtls->{onload} gt "" ) {
                    print "onLoad=\"" . $page_dtls->{onload} . "\"\n";
                }
            } elsif ( $java_msg gt "" ) {
                print "onLoad=\"alert( '$java_msg' )\"\n";
            } elsif ( $page_dtls->{onload} gt "" ) {
                print "onLoad=\"" . $page_dtls->{onload} . "\"\n";
            } else {
                print $_;
            }
        } else {
            s/\{\{(.*?)\}\}/$page_dtls->{$1}/g;
            print $_;
        }
    }
    
    close FILE;
    
    exit;

}


###########################################################                   
# Get Page Navigation Links
#----------------------------------------------------------
sub getPageLinks {

	my ( $self, $cur_page ) = @_;
	my $result   = "";
	
	my $SQL = "SELECT pag_id, pag_code, pag_nav_order, pag_sub_label,\n"
	        . "pag_title, pag_external_url, pag_target_blank\n"
	        . "FROM pages\n"
	        . "WHERE pag_nav_order > 0\n"
	        . "ORDER BY pag_nav_order";
	        
	my @rs  = $self->getResultSet( $SQL );
	foreach my $pag_rec ( @rs ) {
	    my $pag_id    = $pag_rec->{pag_id};
	    my $pag_code  = $pag_rec->{pag_code};
	    my $pag_label = $pag_rec->{pag_sub_label};
	    my $pag_title = $pag_rec->{pag_title};
	    my $pag_url   = $pag_rec->{pag_external_url};
	    my $tgt_blank = $pag_rec->{pag_target_blank};
	    
	    $result .= "<li>";
	    
	    if ( $pag_url gt "" ) {
	        if ( $pag_url =~ /^http:\/\// ) {
    	        $result .= "<a href=\"" . $pag_url . "\"";
    	    } else {
    	        $result .= "<a href=\"$script_path/" . $pag_url . "\"";
    	    }
    	        
    	    if ( $tgt_blank eq "Y" ) {
    	        $result .= " target=_blank";
    	    }
    	    $result .= ">";
        } else { 
            $result .= "<a href=\"" . $cgi_url . "?action=view_page&page=" . $pag_code . "\">";
        }
        
        $result .= $pag_label;
        $result .= "</a></li>\n";
        
	}
	return $result;

}

###########################################################                   
# Check to see if page is parent or child of page specified
#----------------------------------------------------------
sub isSPMember {
	
	my ( $self, $cur_code, $par_code ) = @_;
	
	my $result = 0;
	
	if ( $par_code eq $cur_code ) {
		my $SQL = "SELECT count(*) FROM sub_pages WHERE sbp_pag_code = '$cur_code'";
		my $cnt = $self->getSQLCount( $SQL );
		if ( $cnt > 0 ) {
		    $result = 1;
		}
	} else {
		my $SQL = "SELECT count(*) FROM sub_pages WHERE sbp_pag_code = '$par_code' AND sbp_sub_pag_code = '$cur_code'";
		my $cnt = $self->getSQLCount( $SQL );
		if ( $cnt > 0 ) {
			$result = 1;
		}
	}
	
	return $result;
}


###########################################################                   
# Get subheading for sub-page to include in content
#----------------------------------------------------------
sub getSubHeading {
	
	my ( $self, $cur_page ) = @_;
	
	my $result = "";
	
    my $spCnt = $self->getSQLCount( "SELECT COUNT(*) FROM sub_pages WHERE sbp_pag_code = '$cur_page'" );
    my $isSP  = $self->getSQLCount( "SELECT COUNT(*) FROM sub_pages WHERE sbp_sub_pag_code = '$cur_page'" );
    
    if ( ( $spCnt > 0 ) || ( $isSP > 0 ) ) {
        $result = "<h2>" . $self->getFieldVal( "pages", "pag_heading", "pag_code", $cur_page ) . "</h2>";
    }
	
	return $result;
}

###########################################################                   
# Get submenu links or header label
#----------------------------------------------------------
sub getSubMenu {

  	my ( $self, $cur_page ) = @_;
	my $result   = "";

    	
    my $spCnt = $self->getSQLCount( "SELECT COUNT(*) FROM sub_pages WHERE sbp_pag_code = '$cur_page'" );
    my $isSP  = $self->getSQLCount( "SELECT COUNT(*) FROM sub_pages WHERE sbp_sub_pag_code = '$cur_page'" );
    
    if ( $spCnt > 0 ) {
        $result = $self->getSubPages( $cur_page );
    } elsif ( $isSP ) {
        my $parent_pg = $self->getFieldVal( "sub_pages", "sbp_pag_code", "sbp_sub_pag_code", $cur_page );
        $result = $self->getSubPages( $parent_pg );
    } else {
        my $sm_label  = $self->getFieldVal( "pages", "pag_heading", "pag_code", $cur_page );
        $result = "<span class=\"subtitle\">$sm_label</span>";
    }
    
    return $result;

}



###########################################################                   
# Check to see if page is parent or child of page specified
#----------------------------------------------------------
sub getSubPages {
	
	my ( $self, $par_code ) = @_;
	
    my $par_rec = $self->getResultRec( "SELECT * FROM pages WHERE pag_code = '$par_code'" );
    my $par_lbl = $par_rec->{pag_sub_label};

	my $result = "<a href=\"" . $cgi_url . "?action=view_page&page=" . $par_code . "\"><b>$par_lbl</b></a>\n";
    
    my $SQL    = "SELECT pag_sub_label, pag_external_url, pag_target_blank,\n"
               . "sbp_sub_pag_code, sbp_order, sbp_a_name\n"
               . "FROM sub_pages\n"
               . "INNER JOIN pages ON sbp_sub_pag_code = pag_code\n"
               . "WHERE sbp_pag_code = '$par_code'\n"
               . "ORDER BY sbp_order";
    my @rs     = $self->getResultSet( $SQL );
    my $cnt    = 0;
    my $lln    = 0;
    foreach my $sub ( @rs ) {
    	my $label   = $sub->{pag_sub_label};
    	my $ext_url = $sub->{pag_external_url};
    	my $tgtB    = $sub->{pag_target_blank};
    	my $code    = $sub->{sbp_sub_pag_code};
    	my $name    = $sub->{sbp_a_name};
    	
    	$lln       += length( $label );

   	    if ( $lln > 100 ) {
   	        $result .= "<br />";
   	        $lln     = 0;
   	    } else {
   	        $result .= "&nbsp;\|&nbsp;";
   	    }
    	
	    if ( $ext_url gt "" ) {
	        if ( $ext_url =~ /^http:\/\// ) {
    	        $result .= "<a href=\"" . $ext_url . "\"";
    	    } else {
    	        $result .= "<a href=\"$script_path/" . $ext_url . "\"";
    	    }
    	    if ( $tgtB eq "Y" ) {
    	    	$result .= " target=_blank";
    	    }
    	    $result .= "><b>$label</b></a>\n";
	    } else {
  	        $result .= "<a href=\"" . $cgi_url . "?action=view_page&page=" . $code;
  	        if ( $name gt "" ) {
  	            $result .= "\#$name";
  	        }
  	        $result .= "\"><b>$label</b></a>\n";
	    }
	    $cnt++;
	}
	
	return $result;
}


############################################################
# Get Page Properties and return as hash
#-----------------------------------------------------------
sub getPageProps {
    
    my ( $self, $pag_code ) = @_;
    
    my $page_dtls;

	my $pag_rec   = $self->get_rec( "pages", "pag_code", $pag_code );
	my $pag_bannr = $pag_rec->{pag_banner};
	my $pag_title = $pag_rec->{pag_title};
		
    $page_dtls->{banner_title}  = $pag_bannr;
    $page_dtls->{page_code}     = $pag_code;
    $page_dtls->{submenu_links} = $self->getSubPages( $pag_code );
    
    return $page_dtls;
    
}


###########################################################                   
# Get a single count value from a "select count(*)" query
#----------------------------------------------------------
sub getSQLCount {
    my ( $self, $query ) = @_;
    
    my $retval = 0;
    my $cleanq = $query;
    if ( $query =~ /(.+)[\n\t ]*order by.+/i ) {
        $cleanq = $1;
    }

    if ( $debug ) {
    	open( DEBUG, ">>$src_path/debug.log" );
    	print DEBUG "###OPENING DEBUG FILE - getSQLCount Subroutine###\n";
    	print DEBUG "SQL:\n$cleanq\n";
    }

    my $sth = $dbh->prepare( $cleanq );

    my $count = $sth->execute();
    
    if ( $count > 0 ) {
        ( $retval ) = $sth->fetchrow_array();
    }
    
    if ( $debug ) {
    	print DEBUG "\nCount Result: $retval\n\n";
    	close( DEBUG );
    }
    
    $sth->finish();
    
    return $retval;
}

###########################################################                   
# Get a single record from the database.  Return a single
# field value.
#----------------------------------------------------------
sub getFieldVal {
	my ( $self, $table, $retFld, $srchFld, $srchVal ) = @_;
	
	my $ret_val = "";

    if ( $srchVal gt "" ) {
        my $query = "SELECT $retFld FROM $table WHERE $srchFld = ?";
	
	    my $sth = $dbh->prepare( $query );
    
        my $count = $sth->execute( $srchVal );
    
        if ( $count == 1 ) {
    	    ( $ret_val ) = $sth->fetchrow_array();
        }
    
        $sth->finish();
    }
    
    return $ret_val;
}


###########################################################                   
# Clone a database record - all fields except primary key.
# Return new primary key value
#----------------------------------------------------------
sub clone_rec {
    my ( $self, $table, $prim_key_name, $src_rec_key ) = @_;
    
    my $ret_val = 0;
    
    my $sth = $dbh->prepare( "SELECT * FROM $table " .
                             "WHERE $prim_key_name = ?" );
    
    my $count = $sth->execute( $src_rec_key );
    
    if ( $count == 1 ) {
        my $hash = $sth->fetchrow_hashref();
        
        my $insert1 = "INSERT INTO $table ( ";
        my $insert2 = " ) VALUES ( ";
        my @vals;
        
        my $fcnt = 0;
        foreach my $fld ( keys %$hash ) {
            if ( $fld ne $prim_key_name ) {
                my $val = $hash->{$fld};
                if ( $fcnt > 0 ) {
                    $insert1 .= ", $fld";
                    $insert2 .= ", ?";
                } else {
                    $insert1 .= $fld;
                    $insert2 .= "?";
                }
                push( @vals, $val );
                $fcnt++;
            }
        }
        my $insert = $insert1 . $insert2 . " )";
        my $stin   = $dbh->prepare( $insert );
        $stin->execute( @vals );
        $stin->finish();
        
        $ret_val   = $self->getSQLLastInsertID;
    }
    
    $sth->finish();
    
    return $ret_val;
}

###########################################################                   
# Get a single record from the database.  Return a hash
# of values.
#----------------------------------------------------------
sub get_rec {
    my ( $self, $table, $prim_key_name, $prim_key_val ) = @_;
    
    my $ret_hash;
    
    my $sth = $dbh->prepare( "SELECT * FROM $table " .
                             "WHERE $prim_key_name = ?" );
    
    my $count = $sth->execute( $prim_key_val );
    
    if ( $count == 1 ) {
        $ret_hash = $sth->fetchrow_hashref();
    }
    
    $sth->finish();
    
    return $ret_hash;
}

###########################################################                   
# Get records from the database.  Return an array of hash
# records.
#----------------------------------------------------------
sub getResultSet {
    my ( $self, $query ) = @_;
    
    my @ret_array;
    
    my $sth = $dbh->prepare( $query );

    my $count = $sth->execute()  || die "getResultSet Query Error: $!\n$query";
    
    if ( $count > 0 ) {
        while( my $hash = $sth->fetchrow_hashref() ) {
            push( @ret_array, $hash );
        }
    }
    
    $sth->finish();
    
    return @ret_array;
}

###########################################################                   
# Get a record from the database from an SQL statement.  
# Return a hash.
#----------------------------------------------------------
sub getResultRec {
    my ( $self, $query ) = @_;
    
    my $hash;
    
    my $sth = $dbh->prepare( $query );

    my $count = $sth->execute();
    
    if ( $count > 0 ) {
        $hash = $sth->fetchrow_hashref();
    }
    
    $sth->finish();
    
    return $hash;
}
	
###########################################################                   
# Return the value for the action parameter from the CGI
# object.
#----------------------------------------------------------
sub get_cgi {
    
    my ( $self, $param ) = @_;
    my $ret_val = $cgiObject->param( "$param" );
    
    return $ret_val;
    
}

###########################################################                   
# Return the CGI object
#----------------------------------------------------------
sub get_cgiObject {
	
	return $cgiObject;
	
}


###########################################################                   
# Get the last insert ID
#----------------------------------------------------------
sub getSQLLastInsertID {
	
	my ( $self ) = @_;
	my $ret_val = 0;
	
	my $query = "SELECT LAST_INSERT_ID()";
	my $sth   = $dbh->prepare( $query );
	my $rows  = $sth->execute();
	if ( $rows > 0 ) {
		( $ret_val ) = $sth->fetchrow_array();
	}
	
	return $ret_val;
}

###########################################################                   
# Set the cookie value in the hash
#----------------------------------------------------------
sub store_cookie {
    my ( $self, $name, $value ) = @_;
    
    $my_cookies{ $name } = "$value";
    
}

###########################################################                   
# Request a cookie value
#----------------------------------------------------------
sub get_cookie {
	my ( $self, $name ) = @_;
	
	my $result = $cgiObject->cookie( $name );
	
	return $result;
}

###########################################################                   
# Generate the CGI header using the defined cgiObject
#----------------------------------------------------------
sub cgi_header {
    
    my ( $obj_cookie, $cookie_name, $cookie_str );
    my ( $cookie_val, $cookie_exp );
    my @COOKIES;
    
    my $config      = new config;
    my $cookie_path = $config->get_cookie_path;
    
    foreach $cookie_name ( keys ( %my_cookies ) ) {
        $cookie_str = $my_cookies{$cookie_name};
        if ( $cookie_str =~ /([^\|]+)\|([^\|]*)\|(.*)/ ) {
            $cookie_val = $1;
            $cookie_exp = $2;
            $cookie_path = $3;
        } elsif ( $cookie_str =~ /([^\|]+)\|(.*)/ ) {
            $cookie_val = $1;
            $cookie_exp = $2;
        } else {  
            $cookie_val = $cookie_str;
            $cookie_exp = "";
        }
        
        if (( $cookie_path ne "" ) && ( $cookie_exp ne "" ) && ( $cookie_val ne "" )) {
            $obj_cookie = $cgiObject->cookie( -name=>$cookie_name, -value=>$cookie_val, -expires=>$cookie_exp, -path=>$cookie_path );
        } elsif (( $cookie_exp ne "" ) && ( $cookie_val ne "" )) {
            $obj_cookie = $cgiObject->cookie( -name=>$cookie_name, -value=>$cookie_val, -expires=>$cookie_exp );
        } elsif (( $cookie_path ne "" ) && ( $cookie_val ne "" )) {
            $obj_cookie = $cgiObject->cookie( -name=>$cookie_name, -value=>$cookie_val, -path=>$cookie_path );
        } else {
            $obj_cookie = $cgiObject->cookie( -name=>$cookie_name, -value=>$cookie_val );
        }
        
        push( @COOKIES, $obj_cookie );
    }
    
    if ( @COOKIES ) {
        print $cgiObject->header( -cookie=>[ @COOKIES ] );
    }
    else {
        print $cgiObject->header();
    }
    
    $done_cgi_head = 1;
}

###########################################################                   
# Get JSCAL2 header links for calendar popup
#----------------------------------------------------------
sub getJSCalHead {

    my ( $self ) = @_;
    
    my $result = "";
    
    $result .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"$css_path/jscal2.css\" />\n";
    $result .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"$css_path/border-radius.css\" />\n";
    $result .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"$css_path/steel.css\" />\n";
    $result .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"$css_path/reduce-spacing.css\" />\n";
    $result .= "<script type=\"text/javascript\" src=\"$js_path/jscal2.js\"></script>\n";
    $result .= "<script type=\"text/javascript\" src=\"$js_path/jscal2-en.js\"></script>\n";
    
    return $result;
    
}

###########################################################                   
# Get JSCAL2 input field with popup button
#----------------------------------------------------------
sub getJSCalInput {

    my ( $self, $fldName, $fldVal, $trigName ) = @_;
    
    my $result = "";
    
    $result .= "<input id=\"$fldName\" name=\"$fldName\" value=\"$fldVal\" size=13 class=form />\n";
    $result .= "<input type=button id=\"$trigName\" value=\"...\" class=form />\n";
    $result .= "<script>\n";
    $result .= "Calendar.setup({\n";
    $result .= "  inputField : \"$fldName\",\n";
    $result .= "  trigger    : \"$trigName\",\n";
    $result .= "  fdow       : 0,\n";
    $result .= "  dateFormat : \"%m/%d/%Y\",\n";
    $result .= "  onSelect   : function() { this.hide() }\n";
    $result .= "  });\n";
    $result .= "</script>\n";
    
    return $result;
}

###########################################################                   
# Convert to SQL Date
#----------------------------------------------------------
sub getSQLDate {

    my ( $self, $date_in ) = @_;
    
    my $result        = $date_in;
    if ( $date_in =~ /\d+\/\d+\/\d+/ ) {
        my @dparts    = split( "/", $date_in );
        $result       = $dparts[2] . "-" . $dparts[0] . "-" . $dparts[1];
    }
    
    if ( ( $result eq "" ) || ( $result eq "N/A" ) ) {
        $result = "0000-00-00";
    }
    
    return $result;
}

###########################################################                   
# Return an SQL date string for today.
#----------------------------------------------------------
sub getSQLToday {
    
    my ( $self ) = @_;
    
    my ( $sec, $min, $hour, $tday, $tmn, $tyr, undef, undef, undef ) = localtime();
    
    my ( $day, $month, $year, $retval );
    
    $day   = sprintf( "%02d", $tday );
    $month = sprintf( "%02d", ( $tmn + 1 ) );
    $year  = sprintf( "%04d", ( $tyr + 1900 ) );
    
    $retval = "$year-$month-$day";
    
    return $retval;
}

###########################################################                   
# Return an SQL datetime string for NOW.
#----------------------------------------------------------
sub getSQLNow {
    
    my ( $self ) = @_;
    
    my ( $tsec, $tmin, $thour, $tday, $tmn, $tyr, undef, undef, undef ) = localtime();
    
    my ( $day, $month, $year, $hour, $min, $sec, $retval );
    
    $day   = sprintf( "%02d", $tday );
    $month = sprintf( "%02d", ( $tmn + 1 ) );
    $year  = sprintf( "%04d", ( $tyr + 1900 ) );
    $hour  = sprintf( "%02d", $thour );
    $min   = sprintf( "%02d", $tmin );
    $sec   = sprintf( "%02d", $tsec );
    
    $retval = "$year-$month-$day $hour:$min:$sec";
    
    return $retval;
}


###########################################################                   
# Return Datestamp value of current system clock.
#----------------------------------------------------------
sub getDateStamp {

    my ( $self, $doTime ) = @_;
    
    if ( $doTime eq "" ) {
        $doTime = "N";
    }
    
    my $date   = $self->getSQLToday;
    my $ts     = substr( $self->getTimeStamp, 0, 4 );
    $date     =~ s/\-//g;
    my $result = $date . $ts;
    if ( $doTime eq "N" ) {
        $result = $date;
    }
    
    return $result;
    
}

###########################################################                   
# Get Time Stamp
#----------------------------------------------------------
sub getTimeStamp {
    
    my ( $self ) = @_;
    
    my ( $tsec, $tmin, $thour, $tday, $tmn, $tyr, undef, undef, undef ) = localtime();
    
    my ( $hour, $min, $retval );
    
    $hour  = sprintf( "%02d", $thour );
    $min   = sprintf( "%02d", $tmin );
    $sec   = sprintf( "%02d", $tsec );
    
    $retval = $hour . $min . $sec;
    
    return $retval;
}

###########################################################                   
# Check the date format
#----------------------------------------------------------
sub isValidDate {
    
    my ( $self, $sqlDate, $required ) = @_;
    
    
    my $retval = 0;
    
    if ( $sqlDate gt "" ) {
    	if ( $sqlDate =~ /(\d{4})\-(\d+)\-(\d+)/ ) {
            my $year  = $1;
            my $month = $2;
            my $day   = $3;
            if ( ( $month < 13 ) && ( $day < 32 ) ) {
             	$retval = 1;
            }
        }
    } else {
    	if ( $required == 0 ) {
    		$retval = 1;
        }
    }
    return $retval;
}

        
###########################################################                   
# Get Date Description
#----------------------------------------------------------
sub getDateDesc {
    
    my ( $self, $sqlDate, $incYear ) = @_;
    
    my $retval = "";
    if ( $sqlDate gt "" ) {
        my $year  = substr( $sqlDate, 0, 4 );
        my $month = substr( $sqlDate, 5, 2 );
        my $day   = substr( $sqlDate, 8, 2 );
    
        if ( substr( $day, 0, 1 ) eq "0" ) {
            $day = substr( $day, 1, 1 );
        }
    
        if ( $incYear ) {
            $retval = $MonthDesc{ $month } . " $day, $year";
        } else {
            $retval = $MonthDesc{ $month } . " $day";
        }
    }
    
    return $retval;
}


###########################################################                   
# Get Date Description
#----------------------------------------------------------
sub getDateFmt {
    
    my ( $self, $sqlDate ) = @_;
    
    my $retval = "N/A";
    if ( length( $sqlDate ) > 10 ) {
        if ( $sqlDte ne "0000-00-00 00:00:00" ) {
            my ( $dPart, $tPart ) = split( " ", $sqlDate );
            my ( $year, $month, $day ) = split( "-", $dPart );
            $retval = "$month/$day/$year $tPart";
        }
    } else {
        if ( ( $sqlDate gt "" ) && ( $sqlDate ne "0000-00-00" ) ) {
            my ( $year, $month, $day ) = split( "-", $sqlDate );
    
            $retval = "$month/$day/$year";
        }
    }
    return $retval;
}

###########################################################                   
# Get Date Inputs
#----------------------------------------------------------
sub getDateInput {
    
    my ( $self, $sqlDate, $inp_code, $emptyOK ) = @_;
    
    my $year  = "";
    my $month = "";
    my $day   = "";
    if ( $sqlDate gt "" ) {
        $year  = substr( $sqlDate, 0, 4 );
        $month = substr( $sqlDate, 5, 2 );
        $day   = substr( $sqlDate, 8, 2 );
    }
    
    my $retval = "<span class=form>Month: </span><select name=\"" . $inp_code . "_month\" class=form>\n";
    my $monNo;
    if ( $emptyOK == 1 ) {
        $retval .= "<option value=\"\"></option>\n";
    }
    foreach $monNo ( sort keys %MonthDesc ) {
        if ( $monNo eq $month ) {
            $retval .= "<option selected value=\"$monNo\">" . $MonthDesc{ $monNo } . "</option>\n";
        } else {
            $retval .= "<option value=\"$monNo\">" . $MonthDesc{ $monNo } . "</option>\n";
        }
    }
    $retval .= "</select>\n";
    
    $retval .= "<span class=form>Day: </span><select name=\"" . $inp_code . "_day\" class=form>\n";
    my $dayNo;
    if ( $emptyOK == 1 ) {
        $retval .= "<option value=\"\"></option>\n";
    }
    foreach $dayNo ( @MonthDays ) {
        if ( $dayNo eq $day ) {
            $retval .= "<option selected value=\"" . $dayNo . "\">" . $dayNo . "</option>\n";
        } else {
            $retval .= "<option value=\"" . $dayNo . "\">" . $dayNo . "</option>\n";
        }
    }
    $retval .= "</select>\n";
    
    $retval .= "<span class=form>Year: </span><select name=\"" . $inp_code . "_year\" class=form>\n";
    my $yearNo;
    if ( $emptyOK == 1 ) {
        $retval .= "<option value=\"\"></option>\n";
    }
    my ( undef, undef, undef, undef, undef, $current_year, undef, undef, undef ) = localtime(time);
    for ( $yearNo = $startYear; $yearNo <= $current_year + 1900; $yearNo++ ) {
        if ( $yearNo eq $year ) {
            $retval .= "<option selected value=\"$yearNo\">$yearNo</option>\n";
        } else {
            $retval .= "<option value=\"$yearNo\">$yearNo</option>\n";
        }
    }
    $retval .= "</select>\n";
    
    return $retval;
}

###########################################################                   
# Return Current Year
#----------------------------------------------------------
sub getCurrentYear {
	
	return $now_year;
	
}

###########################################################                   
# Return Current Month
#----------------------------------------------------------
sub getCurrentMonth {
	
	return $now_month;
	
}

###########################################################                   
# Return Current Year
#----------------------------------------------------------
sub getCurrentDay {
	
	return $now_day;
	
}

###########################################################                   
# Return date (first day of current month)
#----------------------------------------------------------
sub getDateFirst {
	
	return "$now_year-$now_month-01";
	
}

###########################################################                   
# Return date (first day of previous month)
#----------------------------------------------------------
sub getDatePrevFirst {
	
	my $rptMon = ( $now_month - 1 );
	my $rptYr  = $now_year;
	my $rptDay = "01";
	
	if ( $now_month == 1 ) {
	    $rptYr  = ( $now_year - 1 );
	    $rptMon = 12;
	}
	
	my $result = $rptYr . "-"
	           . sprintf( "%02s", $rptMon ) . "-"
	           . $rptDay;
	           
	return $result;
	
}

###########################################################                   
# Return date (last day of previous month)
#----------------------------------------------------------
sub getDatePrevLast {
	
	use Date::Calc qw( :all );
	
	my ( $rptYr, $rptMn, $rptDy ) = Add_Delta_Days( $now_year, $now_month, 1, -1 );

    my $result = $rptYr . "-"
               . sprintf( "%02s", $rptMn ) . "-"
               . sprintf( "%02s", $rptDy );
   	           
	return $result;
	
}

###########################################################                   
# Return MonthsDesc array
#----------------------------------------------------------
sub getMonthDesc {
	
	return (%MonthDesc);
	
}

###########################################################                   
# Return the valid SQL formatted date from string passed
# If date is not valid, then return an empty string.
#----------------------------------------------------------
sub getDateValid {
	
	my ( $self, $raw ) = @_;

  	use Date::Calc qw( :all );
	
	my $result = "";
	
	if ( $raw gt "" ) {
	
	    if ( $raw =~ /^\d{4}\-\d+\-\d+$/ ) {
		    $result = $raw;
	    } else {
    	    if ( my ( $year, $month, $day ) = Decode_Date_US( $raw ) ) {
    		    $result = "$year-$month-$day";
    	    }
        }
    
        if ( $result gt "" ) {
            my ( $yr, $mn, $dy ) = split( "-", $result );
            if ( !check_date( $yr, $mn, $dy ) ) {
        	    $result = "";
            }
        }
    }
    
    return $result;
} 
		

###########################################################                   
# Update record in database
#----------------------------------------------------------
sub doSQLUpdate {
    
    my ( $self, $SQL ) = @_;
    
    $dbh->do( $SQL ) || 
    $self->do_err( "Unable to update record.  $DBI::errstr<BR>\n<BR>\nSQL:<BR>\n$SQL" );
    
}

############################################################
# Clean the update hash.  Remove special characters
#-----------------------------------------------------------
sub cleanHash {
    
    my ( $self, $hash ) = @_;
    
    foreach my $fld ( keys %$hash ) {
        $hash->{$fld} =~ s/\\//g;
    }
    
}


###########################################################
# Return a select option list for branches defined.
#----------------------------------------------------------
sub pop_lookup_select {
    
    my ( $self, $category, $cur_val, $ord_by ) = @_;
    my $retval;
    
    if ( $ord_by eq "" ) {
    	$ord_by = "ORDER BY lku_description";
    }
    
    my @rs         = $self->getResultSet( "SELECT * FROM lookup WHERE lku_category = '$category' AND lku_active = 'Y' $ord_by" );
    
    foreach my $lku_rec ( @rs ) {
        my $code = $lku_rec->{lku_code};
        my $desc = $lku_rec->{lku_description};
        if ( $code eq "" ) {
            $code = $desc;
        }
        if ( $cur_val eq $code ) {
            $retval .= "<option selected value=\"$code\">$desc</option>\n";
        } else {
            $retval .= "<option value=\"$code\">$desc</option>\n";
        }
    }
    
    return $retval;
}

###########################################################
# Return the description of the lookup code passed.
#----------------------------------------------------------
sub getLookupDesc {
    
    my ( $self, $category, $code ) = @_;
    my $retval;
    
    my $lku_rec = $self->getResultRec( "SELECT * FROM lookup WHERE lku_category = '$category' AND lku_code = '$code'" );
    $retval = $lku_rec->{lku_description};
    
    return $retval;
}

###########################################################
# Return the extra field of the lookup code passed.
#----------------------------------------------------------
sub getLookupExtra {
    
    my ( $self, $category, $code ) = @_;
    my $retval;
    
    my $lku_rec = $self->getResultRec( "SELECT * FROM lookup WHERE lku_category = '$category' AND lku_code = '$code'" );
    $retval = $lku_rec->{lku_extra};
    
    return $retval;
}

###########################################################
# Return an array of values (code::desc) from lookup
#----------------------------------------------------------
sub getLookupArray {
    
    my ( $self, $category ) = @_;
    my @retval;
    
    my $SQL = "SELECT * FROM lookup WHERE lku_category = '$category' ORDER BY lku_description";
    my @rs  = $self->getResultSet( $SQL );
    foreach my $lu_rec ( @rs ) {
    	my $code = $lu_rec->{lku_code};
    	my $desc = $lu_rec->{lku_description};
    	push( @retval, "$code</:/>$desc" );
    }
    
    return @retval;
}

###########################################################
# Return an hash of values from lookup
#----------------------------------------------------------
sub getLookupHash {
    
    my ( $self, $category ) = @_;
    
    my $ret_hash;
    
    my $SQL = "SELECT * FROM lookup WHERE lku_category = '$category' ORDER BY lku_description";
    my @rs  = $self->getResultSet( $SQL );
    foreach my $lu_rec ( @rs ) {
    	my $code = $lu_rec->{lku_code};
    	my $desc = $lu_rec->{lku_description};
    	$ret_hash->{$code} = $desc;
    }
    
    return $ret_hash;
}

###########################################################                   
# Trim a string
#----------------------------------------------------------
sub trim {

    my ( $self, $result ) = @_;
        
    $result =~ s/^[ \t]+//;
    $result =~ s/[ \t]+$//;
    $result =~ s/[ \t]+/ /g;
    
    return $result;
}

###########################################################                   
# Round a number
#----------------------------------------------------------
sub round {

    my ( $self, $number, $dec ) = @_;
        
    my $result = $number;
    
    if ( $dec == 1 ) {
    	my $tmp = int( $number * 100 ) + 5;
        $result = int( $tmp / 10 ) / 10;
    	
    } elsif ( $dec == 2 ) {
    	my $tmp = int( $number * 1000 ) + 5;
    	$result = int( $tmp / 10 ) / 100;
    	
    } elsif ( $dec == 3 ) {
    	my $tmp = int( $number * 10000 ) + 5;
    	$result = int( $tmp / 10 ) / 1000;
    	
    } else {
    	my $tmp = int( $number * 10 ) + 5;
    	$result = int( $tmp / 10 );
    }
    
    return $result;
}

###########################################################                   
# Format a number for currency display
#----------------------------------------------------------
sub fmtCurrency {

    my ( $self, $input, $symb ) = @_;
    
    if ( $symb eq "" ) {
        $symb  = "USD";
    }
        
    my $temp   = $self->validNumber( $input );
    my $temp2  = $nf->format_picture( $temp, "##,###,###.##" );
    
    my $result = "\$ " . $temp2;
    if ( $symb eq "EUR" ) {
        $result = "&#8364; " . $temp2;
    } elsif ( $symb eq "GBP" ) {
        $result = "&#163; " . $temp2;
    }
    
    return $result;
}

###########################################################                   
# Format a number
#----------------------------------------------------------
sub fmtNumber {

    my ( $self, $input, $doOne ) = @_;
    my $result;
    
    my $temp   = $self->validNumber( $input );
    if ( $doOne ) {
        $result = $self->trim( $nf->format_picture( $temp, "#,###,###.#" ) );
    } else {
        $result = $self->trim( $nf->format_picture( $temp, "#,###,###.##" ) );
    }
    
    return $result;
}


###########################################################                   
# Format a number
#----------------------------------------------------------
sub fmtNumMask {

    my ( $self, $input, $mask ) = @_;
    my $result;
    
    my $temp   = $self->validNumber( $input );
    $result    = $self->trim( $nf->format_picture( $temp, $mask ) );
    
    return $result;
}


###########################################################
# Clean Number Entry
###########################################################
sub validNumber {
	
	my ( $self, $input ) = @_;
	
	$input =~ s/[\,\$]//g;
	my $result = $input + 0;
	
	return $result;
}

###########################################################
# Upload File
###########################################################
sub uploadFile {
	
	my ( $self, $input_name, $file_name, $filtExt ) = @_;
    my $retval = 0;
    
    my $input_file = $cgiObject->param( $input_name );
    if ( $input_file gt "" ) {
        my $file_ext = $filtExt;
        if ( $filtExt gt "" ) {
            if ( $input_file =~ /.*[\.](.*)$/ ) {
                $file_ext = $1;
            }
        }
        if ( lc( $file_ext ) eq lc( $filtExt ) ) {
            my $handle = $cgiObject->upload( $input_name );
            open( OUT, ">$file_name" );
            while ( <$handle> ) {
                print OUT;
            }
            close( OUT );
            system( "/bin/chmod 644 $file_name" );
        } else {
            $retval = 2;
        }
    } else {
        $retval = 1;
    }
                
    return $retval;
}

###########################################################
# Is Date in Past
###########################################################
sub isDatePast {
	
	my ( $self, $datein ) = @_;
	
	my $result = 0;
	
	my $today  = $self->getSQLToday;
	my $days   = $self->getDateDiff( $today, $datein );
	if ( $days < 0 ) {
		$result = 1;
	}
	
	return $result;
}

###########################################################
# Get difference in Days between two SQL formatted dates
###########################################################
sub getDateDiff {
	
	my ( $self, $date1, $date2 ) = @_;
	
	use Date::Calc qw( :all );
	
	my $result = 0;
	
	if ( ( $date1 gt "" ) && ( $date1 ne "0000-00-00" ) &&
	     ( $date2 gt "" ) && ( $date2 ne "0000-00-00" ) ) {
	    my ( $yr1, $mon1, $day1 ) = split( "-", $date1 );
	    my ( $yr2, $mon2, $day2 ) = split( "-", $date2 );
	    $result = Delta_Days( $yr1, $mon1, $day1, $yr2, $mon2, $day2 );
	}
	
	return $result;
}    

###########################################################
# Get difference in Months between two SQL formatted dates
###########################################################
sub getMonthDiff {
	
	my ( $self, $date1, $date2 ) = @_;
	
	use Date::Calc qw( :all );
	
	my $result = 0;
	
	if ( ( $date1 gt "" ) && ( $date1 ne "0000-00-00" ) &&
	     ( $date2 gt "" ) && ( $date2 ne "0000-00-00" ) ) {
	    my ( $yr1, $mon1, $day1 ) = split( "-", $date1 );
	    my ( $yr2, $mon2, $day2 ) = split( "-", $date2 );
	    
	    my ( $dY, $dM, $dD ) = Delta_YMD( $yr1, $mon1, $day1, $yr2, $mon2, $day2 );
	    
	    $result += ( $dY * 12 );
	    $result += $dM;
	    if ( $dD < 0 ) {
	    	$result -= 1;
	    }
	}
	
	return $result;
}

###########################################################                   
# Display PDF file.
#----------------------------------------------------------
sub disp_pdf {
	
	my ( $self, $pdfurl ) = @_;
	
    if ( ! $done_cgi_head ) {
        &cgi_header;
    }

    print qq|<HTML>\n|;
    print qq|<HEAD>\n|;
    print qq|<META HTTP-EQUIV="Refresh" Content="0; URL=$pdfurl">\n|;
    print qq|</HEAD>\n|;
    print qq|<BODY>\n|;
    print qq|</BODY>\n|;
    print qq|</HTML>\n|;
    
}

###########################################################                   
# Stream PDF file to browser.
#----------------------------------------------------------
sub streamPDF {
	
	my ( $self, $pdf ) = @_;
	
    open( PDF, $pdf ) or die "Could not open PDF [$!]";
    binmode PDF;
    my $output = do { local $/; <PDF> };
    close ( PDF );

    print "Content-Type: application/pdf\n";
    print "Content-Length: " . length( $output ) . "\n\n";
    print $output;
    
}


###########################################################                   
# Add record to audit_log table
#----------------------------------------------------------
sub add_log {
	
	my ( $self, $type, $text, $usr_id ) = @_;
	
    my $log_ref  = "$ENV{REMOTE_ADDR}";
	$text =~ s/'/\\'/g;
	
	my $insert = "INSERT INTO audit_log ( log_time, log_type, log_usr_id, log_ip_add, log_text )\n"
	           . "VALUES ( now(), '$type', $usr_id, '$log_ref', '$text' )";
	$self->doSQLUpdate( $insert );
	
}

###########################################################                   
# Get Config Int Value
#----------------------------------------------------------
sub getConfigInt {

    my ( $self, $code ) = @_;
    
    my $result = "";
    
    my $SQL = "SELECT * FROM sys_config WHERE cfg_code = '$code'";
    my $rec = $self->getResultRec( $SQL );
    if ( defined( $rec ) ) {
    	$result = $rec->{cfg_int_value};
    }
    
    return $result;
}

###########################################################                   
# Get Config String Value
#----------------------------------------------------------
sub getConfigString {

    my ( $self, $code ) = @_;
    
    my $result = "";
    
    my $SQL = "SELECT * FROM sys_config WHERE cfg_code = '$code'";
    my $rec = $self->getResultRec( $SQL );
    if ( defined( $rec ) ) {
    	$result = $rec->{cfg_string_value};
    }
    
    return $result;
}

###########################################################                   
# Get Config Date Value
#----------------------------------------------------------
sub getConfigDate {

    my ( $self, $code ) = @_;
    
    my $result = "";
    
    my $SQL = "SELECT * FROM sys_config WHERE cfg_code = '$code'";
    my $rec = $self->getResultRec( $SQL );
    if ( defined( $rec ) ) {
    	$result = $rec->{cfg_date_value};
    }
    
    return $result;
}

###########################################################                   
# Get Config Text Value
#----------------------------------------------------------
sub getConfigText {

    my ( $self, $code ) = @_;
    
    my $result = "";
    
    my $SQL = "SELECT * FROM sys_config WHERE cfg_code = '$code'";
    my $rec = $self->getResultRec( $SQL );
    if ( defined( $rec ) ) {
    	$result = $rec->{cfg_text_value};
    }
    
    return $result;
}

###########################################################                   
# Set new config value
#----------------------------------------------------------
sub setConfig {

    my ( $self, $code, $value ) = @_;
    
    my $SQL = "SELECT * FROM sys_config WHERE cfg_code = '$code'";
    my $rec = $self->getResultRec( $SQL );
    if ( defined( $rec ) ) {
    	my $type   = $rec->{cfg_type};
    	my $update = "UPDATE sys_config SET ";
    	if ( $type eq "I" ) {
    	    $update .= "cfg_int_value = $value ";
    	} elsif ( $type eq "S" ) {
    	    $value =~ s/'/\\'/g;
    	    $update .= "cfg_string_value = '$value' ";
    	} elsif ( $type eq "D" ) {
    	    $value =~ s/'/\\'/g;
    	    $update .= "cfg_date_value = '$value' ";
    	} elsif ( $type eq "T" ) {
    	    $value =~ s/'/\\'/g;
    	    $update .= "cfg_text_value = '$value' ";
    	}
    	$update .= "WHERE cfg_code = '$code'";
    	$self->doSQLUpdate( $update );
    }
}

###########################################################                   
# Get the user name for ID passed.
#----------------------------------------------------------
sub getUserName {

    my ( $self, $uid ) = @_;
    
    my $result = $self->getFieldVal( "user", "usr_name", "usr_id", $uid );
    
    return $result;
}
	           
###########################################################                   
# Get Javascript and CSS header information for tabber.js
#----------------------------------------------------------
sub getTabberHead {

    my ($content);  
    
    my $conf;
    $conf->{css_path}  = $css_path;
    $conf->{js_path}   = $js_path;
    
    open ( FILE, "<$src_path/tabber.dat" ) or die "Couldn't open $src_path/$form_name: $!";
    while ( <FILE> ) {
        s/\{\{(.*?)\}\}/$conf->{$1}/g;
        $content .= $_;
    }
    close FILE;
    return $content;
}


1;
