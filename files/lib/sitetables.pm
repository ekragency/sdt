#!/usr/bin/perl
########################################
# This package provides shared UMTA functions.
#
# Created: 10/14/2004 - Eric Huber
###########################################################################
# Copyright 2004, P. Eric Huber
###########################################################################

use lib ( "/mnt/d/projects/sdt/files/lib" );
use config;
use shared;

my $shared;
my $src_path;
my $color_head1;
my $color_head2;
my $color_bg1;
my $color_bg2;
my $color_bg3;
my $color_error;
my $img_path;
my $site_title;
my $java_msg = "";

my $debug = 0;

package sitetables;

# -------------------------------------------------------------------------------------
# The constructor   
# -------------------------------------------------------------------------------------
sub new {
    
    my ( $class, $sh_in ) = @_;
    
    my $self = {};
    
    my $config    = new config;
    $src_path     = $config->get_src_path;
    my (%colors)  = $config->get_colors;
    $color_head1  = $colors{ "HEAD1" };
    $color_head2  = $colors{ "HEAD2" };
    $color_bg1    = $colors{ "BG1" };
    $color_bg2    = $colors{ "BG2" };
    $color_bg3    = $colors{ "BG3" };
    $color_error  = $colors{ "ERROR" };
    $img_path     = $config->get_img_path;
    $site_title   = $config->get_app_name;
    $debug        = $config->get_debug;
    
    bless ( $self, $class );

    $shared  = $sh_in;
    
    return $self;
}
# -------------------------------------------------------------------------------------

###########################################################                   
# Verify that event ID is valid.
#----------------------------------------------------------
sub getColumnProps {
	my ( $self, $tbl_id ) = @_;
	my @retVal;
	
	my $tbl_rec = $shared->get_rec( "table_def", "tbl_id", $tbl_id );
	my $hd1     = $tbl_rec->{tbl_col_head1};
	my $hd2     = $tbl_rec->{tbl_col_head2};
	my $hd3     = $tbl_rec->{tbl_col_head3};
	my $hd4     = $tbl_rec->{tbl_col_head4};
	my $hd5     = $tbl_rec->{tbl_col_head5};
	my $hd6     = $tbl_rec->{tbl_col_head6};
	my $al1     = $tbl_rec->{tbl_col_align1};
	my $al2     = $tbl_rec->{tbl_col_align2};
	my $al3     = $tbl_rec->{tbl_col_align3};
	my $al4     = $tbl_rec->{tbl_col_align4};
	my $al5     = $tbl_rec->{tbl_col_align5};
	my $al6     = $tbl_rec->{tbl_col_align6};
	
	if ( $hd1 gt "" ) {
	    push( @retVal, "tbd_col1\t$hd1\t$al1" );
	}
	if ( $hd2 gt "" ) {
	    push( @retVal, "tbd_col2\t$hd2\t$al2" );
	}
	if ( $hd3 gt "" ) {
	    push( @retVal, "tbd_col3\t$hd3\t$al3" );
	}
	if ( $hd4 gt "" ) {
	    push( @retVal, "tbd_col4\t$hd4\t$al4" );
	}
	if ( $hd5 gt "" ) {
	    push( @retVal, "tbd_col5\t$hd5\t$al5" );
	}
	if ( $hd6 gt "" ) {
	    push( @retVal, "tbd_col6\t$hd6\t$al6" );
	}
	
	return @retVal;
}

###########################################################                   
# Generate a table for a tbl_id passed.
#----------------------------------------------------------
sub genTable {

    my ( $self, $tbl_id ) = @_;
    
    my $tbl_rec = $shared->get_rec( "table_def", "tbl_id", $tbl_id );
    my $tStyle  = $tbl_rec->{tbl_style};

    my $result  = "";
    if ( $tStyle eq "LINKS" ) {
        $result = $self->doLinksTable( $tbl_rec );
    } elsif ( $tStyle eq "WKSHOPS" ) {
        $result = $self->doWkShopsTable( $tbl_rec );
    } elsif ( $tStyle eq "S1" ) {
        $result = $self->doS1Table( $tbl_rec );
    }
     
    return $result;
    
}

##########################################################                   
# Build Links Table
#----------------------------------------------------------
sub doLinksTable {

    my ( $self, $tbl_rec ) = @_;

    my $tbl_id = $tbl_rec->{tbl_id};
    
    my $content = "<table border=0 cellpadding=3 cellspacing=0>\n";
    
    my $SQL     = "SELECT * FROM table_data WHERE tbd_tbl_id = $tbl_id\n"
                . "ORDER BY tbd_sort, tbd_col1";
    my @rs      = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        
        my $lnk_img = $rec->{tbd_col1};
        my $lnk_url = $rec->{tbd_col2};
        my $lnk_lbl = $rec->{tbd_col3};
        my $lnk_cap = $rec->{tbd_col4};
        
        $content   .= "<tr><td valign=top align=left>"
                    . "<a href=\"$lnk_url\" target=_blank><img src=\"$img_path/links/$lnk_img\" border=1 alt=\"\" title=\"\" width=120 style=\"margin-bottom:5px\"></a></td>\n"
                    . "<td valign-top align=left><img src=\"$img_path/spacer.gif\" border=0 width=20></td>\n"
                    . "<td valign=top align=left><a href=\"$lnk_url\" target=_blank><b>$lnk_lbl</b></a><br>$lnk_cap</td></tr>\n";
    }
    
    $content .= "</table>\n";
    
    return $content;
    
}

##########################################################                   
# Build Workshops Table
#----------------------------------------------------------
sub doWkShopsTable {

    my ( $self, $tbl_rec ) = @_;

    my $tbl_id = $tbl_rec->{tbl_id};
    
    my $content = "<table border=0 cellpadding=3 cellspacing=0>\n";
    
    my $SQL     = "SELECT * FROM table_data WHERE tbd_tbl_id = $tbl_id\n"
                . "ORDER BY tbd_sort, tbd_col1";
    my @rs      = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        
        my $iName   = $rec->{tbd_col1};
        my $iPoints = $rec->{tbd_col2};
        my $iDesc   = $rec->{tbd_col3};
        
        $content   .= "<tr><td valign=top align=left class=mbody colspan=2><b>$iName</td></tr>\n"
                    . "<tr><td valign=top align=left class=label>$iPoints Points</td>\n"
                    . "<td valign-top align=left class=form>$iDesc</td></tr>\n";
    }
    
    $content .= "</table>\n";
    
    return $content;
    
}

##########################################################                   
# Build Generic Style 1 Table
#----------------------------------------------------------
sub doS1Table {

    my ( $self, $tbl_rec ) = @_;

    my $tbl_id = $tbl_rec->{tbl_id};
    my $tAlign = $tbl_rec->{tbl_align};
    my $tWidth = $tbl_rec->{tbl_width};
    
    my $style  = "float:left;margin-right:10px;margin-bottom:10px;margin-top:5px";
    if ( $tAlign eq "R" ) {
        $style = "float:right;margin-left:10px;margin-bottom:10px;margin-top:5px";
    } elsif ( $tAlign eq "C" ) {
        $style = "float:center;margin-left:10px;margin-right:10px;margin-bottom:10px;margin-top:5px";
    }
    
    my $widthParam = "";
    if ( $tWidth > 0 ) {
        $widthParam = "width=$tWidth";
    }

    my $result   = "<table border=0 cellpadding=0 cellspacing=0 $widthParam style=\"$style\">\n";
    $result     .= "<tr><td>\n";
    $result     .= "<table border=0 cellpadding=0 cellspacing=0>\n";
    $result     .= "<tr><td>\n";
    $result     .= "<div class=\"blur\"><div class=\"shadow\"><div class=\"framework\">\n";
    $result     .= "<table border=0 cellpadding=2 cellspacing=0 $widthParam>\n";
    $result     .= "<tr><td bgcolor=\"$color_bg1\">\n";
    $result     .= "<table border=0 cellpadding=3 cellspacing=0 $widthParam>\n";
    $result     .= "<tr bgcolor=\"$color_head\" class=colhead>\n";
    
    my @colProp  = $self->getColumnProps( $tbl_id );
    foreach my $col ( @colProp ) {
        my ( $fld, $head, $align ) = split( "\t", $col );
        
        if ( $align eq "L" ) {
            $result .= "<td valign=top align=left>$head</td>\n";
        } elsif ( $align eq "R" ) {
            $result .= "<td valign=top align=right>$head</td>\n";
        } else {
            $result .= "<td valign=top align=center>$head</td>\n";
        }
    }
    $result .= "</tr>\n";

    my $row     = 0;
    my $row_color;
    
    my $SQL     = "SELECT * FROM table_data WHERE tbd_tbl_id = $tbl_id\n"
                . "ORDER BY tbd_sort, tbd_col1";
    my @rs      = $shared->getResultSet( $SQL );
    foreach my $rec ( @rs ) {
        
        if ( $row == 0 ) {
            $row_color = $color_bg1;
            $row = 1;
        } else {
            $row_color = $color_bg2;
            $row = 0;
        }
        
        $result .= "<tr bgcolor=\"$row_color\" class=mbody>\n";
        
        foreach my $col ( @colProp ) {
            my ( $fld, $head, $align ) = split( "\t", $col );
            
            my $tVal = $rec->{$fld};
            $tVal   =~ s/\"/&quot;/g;
            
            if ( $align eq "L" ) {
                $result .= "<td valign=top align=left>$tVal</td>";
            } elsif ( $align eq "R" ) {
                $result .= "<td valign=top align=right>$tVal</td>";
            } else {
                $result .= "<td valign=top align=center>$tVal</td>";
            }
        }
        
        $result .= "</tr>\n";
    }
       
    $result .= "</table>\n";
    $result .= "</td></tr>\n";
    $result .= "</table>\n";
    $result .= "</div></div></div>\n";
    $result .= "</td></tr>\n";
    $result .= "</table>\n";
    $result .= "</td></tr>\n";
    $result .= "</table>\n";
    
    return $result;
}


1;
