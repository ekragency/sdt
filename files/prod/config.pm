#!/usr/bin/perl
########################################
# This holds all the site-wide config
# parameters.
#
# Created: 04/14/2010 - Eric Huber
###########################################################################
# Copyright 2010, Moonlight Design, LLC
###########################################################################

package config;

# -------------------------------------------------------------------------------------
# The constructor
# -------------------------------------------------------------------------------------
sub new
{
    return bless {};
}
# -------------------------------------------------------------------------------------

# Paths and other settings (required to update)
my ($base_url)          = "http://translate.local";
my ($admin_email)       = "admin\@utahaws.net";
my ($mailfrom)          = "UtahAWS Admin";
my ($help_email)        = "sdt\@utahaws.net";
my ($paypal_email)      = "moonlight\@dbdev.biz";
my ($mailhost)          = "localhost";
my ($db_name)           = "sdt_db";
my ($db_host)           = "";
my ($db_user)           = "sdtUser";
my ($db_pass)           = "l\@nG123";


# Other global variables (optional update)
my ($base_path)         = "/mnt/d/projects/sdt/files";
my ($root_path)         = "/mnt/d/projects/sdt/htdocs";
my ($temp_path)         = "$base_path/temp";
my ($img_path)          = "$base_url/images";
my ($script_path)       = "http://translate.local/cgi-bin";
my ($js_path)           = "$base_url/javascript";
my ($forms_url)         = "$base_url/forms";
my ($src_path)          = "$base_path/src";
my ($forms_path)        = "$root_path/forms";
my ($css_path)          = "$base_url/css";
my ($cookie_path)       = "/cgi-bin";
my ($pdf_url)           = "$base_url/pdf";
my ($pdf_path)          = "$root_path/pdf";
my ($photo_path)        = "$root_path/photo";
my ($photo_url)         = "$base_url/photo";
my ($log_path)          = "$base_path/log";
my ($imp_path)          = "$base_path/import";
my ($out_path)          = "$base_path/output";
my ($max_photo_size)    = 75;
my ($max_photo_dim)     = 400;


my ($grace_logins)      = 3;
my ($lock_time)         = 3600;                      # Time in seconds that user will be locked out
my %colors;
$colors{ "HEAD1" }      = "#004581";
$colors{ "HEAD2" }      = "#4C85B0";
$colors{ "HEAD3" }      = "#000000";
$colors{ "BG1"   }      = "#E5E5E5";
$colors{ "BG2"   }      = "#D4D4D4";
$colors{ "BG3"   }      = "#FEE6A3";
$colors{ "BG4"   }      = "#C1C1C1";
$colors{ "ERROR" }      = "#660000";


# Other variables (don't update)
my ($usrid_cookie)      = "sdtusrid";
my ($usrpw_cookie)      = "sdtusrpw";
my ($tmid_cookie)       = "tmuid";
my ($tmpw_cookie)       = "tmupw";
my ($lpid_cookie)       = "lpuid";
my ($lppw_cookie)       = "lpupw";
my ($app_name)          = "Metafraze Tools";
my ($tm_name)           = "Terminology Manager";
my ($lp_name)           = "Linguist Portal";
my ($app_ver)           = "1.122";
my ($app_mode)          = "PROD";
my ($debug)             = 0;
my ($dbconnect)         = "DBI:mysql:dbname=$db_name";
if ( $db_host gt "" ) {
	$dbconnect          = "DBI:mysql:dbname=$db_name:host=$db_host";
}



sub get_admin_email {
	return $admin_email;
}

sub get_mailfrom {
	return $mailfrom;
}

sub get_help_email {
	return $help_email;
}

sub get_paypal_email {
	return $paypal_email;
}

sub get_base_path {
    return $base_path;
}

sub get_root_path {
    return $root_path;
}

sub get_temp_path {
    return $temp_path;
}

sub get_base_url {
    return $base_url;
}

sub get_img_path {
    return $img_path;
}

    
sub get_script_path {
    return $script_path;
}

sub get_js_path {
	return $js_path;
}

sub get_forms_url {
	return $forms_url;
}

sub get_src_path {
    return $src_path;
}

sub get_forms_path {
	return $forms_path;
}

sub get_mailhost {
    return $mailhost;
}

sub get_css_path {
    return $css_path;
}

sub get_cookie_path {
    return $cookie_path;
}

sub get_pdf_url {
	return $pdf_url;
}

sub get_pdf_path {
	return $pdf_path;
}

sub get_photo_path {
    return $photo_path;
}

sub get_photo_url {
    return $photo_url;
}

sub get_log_path {
    return $log_path;
}

sub get_imp_path {
    return $imp_path;
}

sub get_out_path {
    return $out_path;
}

sub get_max_photo_size {
	return $max_photo_size;
}

sub get_max_photo_dim {
	return $max_photo_dim;
}

sub get_usrid_cookie {
	return $usrid_cookie;
}

sub get_usrpw_cookie {
	return $usrpw_cookie;
}

sub get_tmid_cookie {
	return $tmid_cookie;
}

sub get_tmpw_cookie {
	return $tmpw_cookie;
}

sub get_lpid_cookie {
	return $lpid_cookie;
}

sub get_lppw_cookie {
	return $lppw_cookie;
}

sub get_app_name {
    return $app_name;
}

sub get_tm_name {
    return $tm_name;
}

sub get_lp_name {
    return $lp_name;
}

sub get_app_ver {
    return $app_ver;
}

sub get_app_mode {
    return $app_mode;
}

sub get_grace_logins {
	return $grace_logins;
}

sub get_lock_time {
	return $lock_time;
}

sub get_dbconnect {
    return $dbconnect;
}
 
sub get_dbuser {
    return $db_user;
}
 
sub get_dbpass {
    return $db_pass;
}
 
sub get_colors {
    return (%colors);
}

sub get_debug {
	return $debug;
}


1;
