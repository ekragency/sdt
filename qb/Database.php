<?php

namespace QuickBooks;

use Dotenv\Dotenv;

class Database{

    private $connection = null;

    public static function createConnection(){
        $dv = Dotenv::createImmutable(__DIR__."/../");
        $dv->load();

        $host = getenv("DB_HOST");
        $port = getenv("DB_PORT");
        $dbName = getenv("DB_DATABASE");
        $user = getenv("DB_USERNAME");
        $pass = getenv("DB_PASSWORD");

        $pdo = new \PDO("mysql:host=$host;port=$port;dbname=$dbName", $user, $pass);

        return $pdo;
    }

    public function __construct()
    {
        $this->connection = self::createConnection();
    }

    public function getConnection(){
        return $this->connection;
    }

    public function conn(){
        return $this->connection;
    }
}
