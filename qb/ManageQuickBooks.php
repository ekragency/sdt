<?php
//https://developer.intuit.com/app/developer/qbo/docs/develop/sdks-and-samples-collections/php#tutorial-objective


namespace QuickBooks;

use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Bill;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Vendor;

class ManageQuickBooks
{
    private $db = null;
    private $configs = null;
    private $keys = null;
    private $dataService = null;
    
    public function __construct(){
        $this->db = new Database();
        $this->configs = (Object) QBConfigs::getConfigs();
        $this->keys = (Object) QBConfigs::getKeys();
        $this->dataService = DataService::Configure(array(
            'auth_mode' => 'oauth2',
            'ClientID' => $this->configs->client_id,
            'ClientSecret' => $this->configs->client_secret,
            'RedirectURI' => $this->configs->oauth_redirect_uri,
            'scope' => "com.intuit.quickbooks.accounting",
            'baseUrl' => $this->configs->base_url,
            'accessTokenKey' => $this->keys->access_key,
            //'refreshTokenKey' => $keys->refresh_key,
            'QBORealmID' => $this->keys->realm_id,
        ));
    }
    
    public static function submitInvoices($maxAmount = 100000) {
        $inst = new self();
        $inst->createInvoices();
    }

    public static function submitBills($maxAmount = 100000) {
        $inst = new self();
        $inst->createBills($maxAmount);
    }

    public function listCategoryItems(){
        $items = $this->dataService
            ->Query("SELECT * FROM Item");

        foreach($items as $item){
            Output::info("$item->Id - $item->Name");
        }
    }

    public function findInvoices(){
        $projQuery = $this->db->conn()
            ->prepare("SELECT `prj_id`,`prj_cln_id` FROM `project` WHERE `prj_invoiced` LIKE 'N' AND `prj_delivery_date` != '0000-00-00' GROUP BY `prj_cln_id`;");

        $clientStat = $this->db->conn()
            ->prepare("SELECT * FROM `client` WHERE `cln_id` = :client_id;");

        $clientProjects = $this->db->conn()
            ->prepare("SELECT count(*) as num_projects FROM `project` WHERE `prj_invoiced` LIKE 'N' AND `prj_delivery_date` != '0000-00-00' AND `prj_cln_id` = :client_id;");

        $projQuery->execute();

        $resultObject = (Object) [
            "totalCount" => 0,
            "clients" => []
        ];

        while($project = $projQuery->fetchObject()){
            $clientStat->execute([":client_id" => $project->prj_cln_id]);
            $client = $clientStat->fetchObject();

            $clientProjects->execute([":client_id" => $project->prj_cln_id]);

            $clientData = (Object) [
                "id" => $client->cln_id,
                "name" => $client->cln_name,
                "projects" => 0
            ];
            $cProj = $clientProjects->fetchObject();
            $clientData->projects = $cProj->num_projects;

            $resultObject->totalCount += $clientData->projects;
            $resultObject->clients[] = $clientData;
        }

        return $resultObject;
    }

    public function processAllInvoices($maxAmount = null){
        $invoiceData = $this->findInvoices();
        $count = 0;
        foreach($invoiceData->clients as $client){
            $this->processInvoiceForClient($client->id);
            $count++;
            if(!empty($maxAmount) && $count >= $maxAmount){
                Output::warn("Reached maximum processing amount. Exiting.");
                return;
            }
        }
    }

    public function processInvoiceForClient($clientId){
        $clientStat = $this->db->conn()
            ->prepare("SELECT * FROM `client` WHERE `cln_id` = :client_id;");

        $clientProjects = $this->db->conn()
            ->prepare("SELECT * FROM `project` WHERE `prj_invoiced` LIKE 'N' AND `prj_delivery_date` != '0000-00-00' AND `prj_cln_id` = :client_id;");

        $clientContactStat = $this->db->conn()
            ->prepare("SELECT * FROM `client_contact` WHERE `clc_id` = :contact_id;");

        $updateProjectStat = $this->db->conn()
            ->prepare("UPDATE `project` SET `prj_invoiced` = 'Y', `prj_delivered_to_client` = :delivery_note WHERE `prj_id` = :project_id;");

		$clientContact = null;

        $clientStat->execute([":client_id" => $clientId]);
        $client = $clientStat->fetchObject();

        $clientProjects->execute([":client_id" => $clientId]);

        $invoiceLines = [];
        $totalAmount = 0;
        while($project = $clientProjects->fetchObject()){
            $clientContactStat->execute([":contact_id" => $project->prj_clc_id]);
            $clientContact = $clientContactStat->fetchObject();
            $amount = $project->prj_rev_usd_amt > 0 ? $project->prj_rev_usd_amt:$project->prj_rev_eur_amt;
            $totalAmount += $amount;
            $invoiceLines[] = [
                "Description" => "Client Project \"$project->prj_client_proj_no\" | Client Contact - $clientContact->clc_contact | Project #$project->prj_no",
                "Amount" => $amount,
                "DetailType" => "SalesItemLineDetail",
                "SalesItemLineDetail" => [
                    "ItemRef" => [
                        "value" => 1,//109, //109 for live
                        "name" => $project->prj_no
                    ]
                ]
            ];
        }

        if(empty($invoiceLines)){
            Output::error("No ready projects found for $client->cln_name. Skipping this invoice.");
            return;
        }

        Output::info("Creating invoice for $client->cln_name");
        $qbCustomer = null;
        try{
            //https://c50.sandbox.qbo.intuit.com/app/customers
            //https://intuit.github.io/QuickBooks-V3-PHP-SDK/quickstart.html#get-by-entity-id
            $customers = $this->dataService->Query("SELECT * FROM Customer WHERE FullyQualifiedName LIKE '$client->cln_name'", 1, 10);

            if(empty($customers) || count($customers) == 0){
                $customers = $this->dataService->Query("SELECT * FROM Customer WHERE DisplayName LIKE '$client->cln_name'", 1, 10);
            }

            if(empty($customers) || count($customers) == 0){
                $customers = $this->dataService->Query("SELECT * FROM Customer WHERE CompanyName LIKE '$client->cln_name'", 1, 10);
            }

            if(empty($customers) || count($customers) == 0){
                Output::warn("Customer $client->cln_name not found in QuickBooks. Creating...");
                $clientContactStat->execute([":client_id" => $clientId]);

                $clientContact = null;
                $allContacts = [];

                while($clientContactTest = $clientContactStat->fetchObject()){
                    $allContacts[] = $clientContactTest;
                    if(!empty($clientContactTest->clc_address)){
                        $clientContact = $clientContactTest;
                    }
                }

                $nameParts = (Object) FullNameParser::parse($clientContact->clc_contact);

                $customerToCreate = Customer::create([
                    "FullyQualifiedName" => $client->cln_name,
                    "PrimaryEmailAddr" => [
                        "Address" => $clientContact->clc_email
                    ],
                    "DisplayName" => $client->cln_name,
                    "Suffix" => "",
                    "Title" => "",
                    "MiddleName" => "",
                    "Notes" => "",
                    "FamilyName" => $nameParts->lname,
                    "PrimaryPhone" => [
                        "FreeFormNumber" => $clientContact->clc_phone
                    ],
                    "CompanyName" => $client->cln_name,
                    "BillAddr" => [
                        "CountrySubDivisionCode" => $clientContact->clc_state,
                        "City" => $clientContact->clc_city,
                        "PostalCode" => $clientContact->clc_zip,
                        "Line1" => $clientContact->clc_address,
                        "Country" => $clientContact->clc_country
                    ],
                    "GivenName" => $nameParts->fname
                ]);

                $qbCustomer = $this->dataService->Add($customerToCreate);

            }
            else{
                Output::info("Customer located in QuickBooks.");
                $qbCustomer = $customers[0];
            }

        }catch (\Exception $e){
            Output::error("Failed to create or find client.");
            $error = $this->dataService->getLastError();
            Output::error($error->getIntuitErrorMessage());
            Output::error($error->getIntuitErrorDetail());
            Output::error($e->getMessage());
        }


        Output::info("Submitting invoice for $client->cln_name : $".number_format($totalAmount, 2));
        $invoiceToCreate = Invoice::create([
            "AutoDocNumber" => true,
            "Line" => $invoiceLines,
            "CustomerRef" => [
                "value" => $qbCustomer->Id,
                "name" => $client->cln_name
            ]
        ]);

        $invoice = $this->dataService->Add($invoiceToCreate);
        if(!empty($invoice->DocNumber)){
            $clientProjects->execute([":client_id" => $clientId]);
            while($project = $clientProjects->fetchObject()){
                $updateProjectStat->execute([
                    ":project_id" => $project->prj_id,
                    ":delivery_note" => "Invoice #".$invoice->DocNumber
                ]);
            }
            Output::info("Invoice #$invoice->DocNumber created.");
        } else {
            Output::error("An error occurred creating the invoice. ");
            $error = $this->dataService->getLastError();
            Output::error($error->getIntuitErrorMessage());
            Output::error($error->getIntuitErrorDetail());
            Output::varDump($invoice);
        }
        Output::info("--------------------------------");

    }

    public function findAllBills(){
    	$invoiceStat = $this->db->conn()
			->prepare("SELECT * FROM `invoice` WHERE `inv_status` = 'PEND' GROUP BY `inv_ling_id`");

    	$linguistStat = $this->db->conn()
			->prepare("SELECT * FROM `linguist` WHERE `lig_id` = :linguist_id");

		$invoiceCount = $this->db->conn()
			->prepare("SELECT count(*) as num_invoices FROM `invoice` WHERE `inv_status` = 'PEND' AND `inv_ling_id` = :linguist_id");

    	$invoiceStat->execute();

		$resultObject = (Object) [
			"totalCount" => 0,
			"linguists" => []
		];

		while($invoice = $invoiceStat->fetchObject()){
			$linguistStat->execute([":linguist_id" => $invoice->inv_ling_id]);
			$linguist = $linguistStat->fetchObject();
			$invoiceCount->execute([":linguist_id" => $invoice->inv_ling_id]);
			$counter = $invoiceCount->fetchObject();
			$count = $counter->num_invoices;

			$resultObject->totalCount += $count;
			$resultObject->linguists[] = (Object) [
				"id" => $linguist->lig_id,
				"name" => "$linguist->lig_first_name $linguist->lig_last_name",
				"bills" => $count
			];

		}

		return $resultObject;
	}

	public function processAllBills($maxAmount = null){
    	$billData = $this->findAllBills();
		$count = 0;
		foreach($billData->linguists as $linguist){
			$this->processBillsForVendor($linguist->id);
			$count++;
			if(!empty($maxAmount) && $count >= $maxAmount){
				Output::warn("Reached maximum processing amount. Exiting.");
				return;
			}
		}
	}

    public function processBillsForVendor($lid){
		$invoiceStat = $this->db->conn()
			->prepare("SELECT * FROM `invoice` WHERE `inv_status` = 'PEND' AND `inv_ling_id` = :linguist_id");

		$linguistStat = $this->db->conn()
			->prepare("SELECT * FROM `linguist` WHERE `lig_id` = :linguist_id");

		$updateInvoiceStat = $this->db->conn()
			->prepare("UPDATE `invoice` SET `inv_status` = 'PROC' WHERE `inv_id` = :invoice_id");

		$logStat = $this->db->conn()
			->prepare("SELECT * FROM `po_log` WHERE `pol_inv_id` = :invoice_id");

		$clientStat = $this->db->conn()
			->prepare("SELECT * FROM `client` WHERE `cln_id` = :client_id;");

		$linguistStat->execute([":linguist_id" => $lid]);

		$linguist = $linguistStat->fetchObject();

		Output::info("Creating bill from $linguist->lig_first_name $linguist->lig_last_name");

		//get or create vendor

		$vendors = $this->dataService
			->Query("SELECT * FROM vendor WHERE GivenName LIKE '$linguist->lig_first_name' AND FamilyName LIKE '$linguist->lig_last_name'");

		if(empty($vendors) || count($vendors) == 0){
			$vendors = $this->dataService
				->Query("SELECT * FROM vendor WHERE DisplayName LIKE '$linguist->lig_first_name $linguist->lig_last_name'");
		}

		if(empty($vendors) || count($vendors) == 0){
			$vendors = $this->dataService
				->Query("SELECT * FROM vendor WHERE CompanyName LIKE '$linguist->lig_first_name $linguist->lig_last_name'");
		}

		$qbVendor = null;
		$txnDate = null;
		if(empty($vendors) || count($vendors) == 0){
			Output::warn("Vendor $linguist->lig_first_name $linguist->lig_last_name not found in QuickBooks. Creating...");

			$vendorToCreate = Vendor::create([
				"BillAddr" => [
					"Line1"=> "$linguist->lig_first_name $linguist->lig_last_name",
					"Line2"=> $linguist->lig_address,
					"Line3"=> "",
					"City"=> $linguist->lig_city,
					"Country"=> $linguist->lig_country,
					"CountrySubDivisionCode"=> $linguist->lig_state,
					"PostalCode"=> $linguist->lig_zip
				],
				"CurrencyRef" => [
					"value" => $linguist->lig_currency
				],
				"TaxIdentifier"=> "",
				"AcctNum"=> "",
				"Title"=> "",
				"GivenName"=> $linguist->lig_first_name,
				"FamilyName"=> $linguist->lig_last_name,
				"Suffix"=> "",
				"CompanyName"=> "$linguist->lig_first_name $linguist->lig_last_name",
				"DisplayName"=> "$linguist->lig_first_name $linguist->lig_last_name",
				"PrintOnCheckName"=> "$linguist->lig_first_name $linguist->lig_last_name",
				"PrimaryPhone"=> [
					"FreeFormNumber"=> $linguist->lig_phone
				],
				"Mobile"=> [
					"FreeFormNumber"=> ""
				],
				"PrimaryEmailAddr"=> [
					"Address"=> $linguist->lig_email
				],
				"WebAddr"=> [
					"URI"=> ""
				]
			]);

			$qbVendor = $this->dataService->Add($vendorToCreate);

		} else {
			Output::info("Vendor located in QuickBooks.");
			$qbVendor = $vendors[0];
		}
		if(empty($qbVendor)){
			Output::error("An error occurred creating/getting the vendor.");
			$error = $this->dataService->getLastError();
			Output::error($error->getIntuitErrorMessage());
			Output::error($error->getIntuitErrorDetail());
		} else {

			//create bill lines
			$billLines = [];
			$invoiceStat->setFetchMode(\PDO::FETCH_OBJ);
			$invoiceStat->execute([":linguist_id" => $lid]);
			$invoices = $invoiceStat->fetchAll();
			$lineId = 0;

			foreach ($invoices as $invoice) {
				$logStat->execute([":invoice_id" => $invoice->inv_id]);
				$poLog = $logStat->fetchObject();
				$clientStat->execute([":client_id" => $poLog->pol_cln_id]);
				$client = $clientStat->fetchObject();

				$billLines[] = [
					"DetailType" => "AccountBasedExpenseLineDetail",
					"Id" => $lineId, //0 creates new
					"Description" =>
						"Invoice Number $invoice->inv_no | PO $poLog->pol_pono | Project $poLog->pol_proj_no | Client $client->cln_name",
					"Amount" => $invoice->inv_amount,
					"AccountBasedExpenseLineDetail" => [
						"AccountRef" => [
							"value" => "7"//"105" //105 for live
						]
					]
				];

				if(empty($txnDate) || (strtotime($txnDate) < strtotime($invoice->inv_date))) {
					$txnDate = $invoice->inv_date;
				}
				$lineId++;
			}

			$billToCreate = Bill::create(
				[
					"Line"=> $billLines,
					"VendorRef"=> [
						"value"=> $qbVendor->Id
					],
					"TxnDate" => $txnDate
				]);

			$bill = $this->dataService->Add($billToCreate);

			if(!empty($bill->MetaData)){
				foreach($invoices as $invoice){
					$updateInvoiceStat->execute([
						":invoice_id" => $invoice->inv_id
					]);
				}

				Output::info("Bill created.");
			}else {
				Output::error("An error occurred creating the bill. ");
				$error = $this->dataService->getLastError();
				Output::error($error->getIntuitErrorMessage());
				Output::error($error->getIntuitErrorDetail());
				Output::varDump($bill);
			}
		}

	}
}
