<?php
namespace QuickBooks;
use Dotenv\Dotenv;
//https://github.com/IntuitDeveloper/OAuth2_PHP/tree/master/OAuth_2

class QBConfigs
{
    private static $accessKey;
    private static $refreshKey;

    public static function getConfigs(){
        $dv = Dotenv::createImmutable(__DIR__."/../");
        $dv->load();

        return array(
            'authorizationRequestUrl' => 'https://appcenter.intuit.com/connect/oauth2', //Example https://appcenter.intuit.com/connect/oauth2',
            'tokenEndPointUrl' => 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer', //Example https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer',
            'base_url' =>getenv("QB_URL"),
            'client_id' => getenv("QB_CLIENT"), //Example 'Q0wDe6WVZMzyu1SnNPAdaAgeOAWNidnVRHWYEUyvXVbmZDRUfQ',
            'client_secret' => getenv("QB_SECRET"), //Example 'R9IttrvneexLcUZbj3bqpmtsu5uD9p7UxNMorpGd',
            'oauth_scope' => 'com.intuit.quickbooks.accounting', //Example 'com.intuit.quickbooks.accounting',
            'openID_scope' => 'openid profile email', //Example 'openid profile email',
            'oauth_redirect_uri' => getenv("APP_URL").'/qoauth/index.php', //Example https://d1eec721.ngrok.io/OAuth_2/OAuth2PHPExample.php',
            'openID_redirect_uri' => getenv("APP_URL").'/qoauth/OAuthOpenIDExample.php',//Example 'https://d1eec721.ngrok.io/OAuth_2/OAuthOpenIDExample.php',
            'mainPage' => getenv("APP_URL").'/qoauth/index.php', //Example https://d1eec721.ngrok.io/OAuth_2/index.php',
            'refreshTokenPage' => getenv("APP_URL").'/qoauth/RefreshToken.php', //Example https://d1eec721.ngrok.io/OAuth_2/RefreshToken.php'
        );
    }

    public static function getKeys(){
        $dv = Dotenv::createImmutable(__DIR__."/../");
        $dv->load();

        if(empty(self::$accessKey) || empty(self::$refreshKey)){
            throw new \Exception("Needed keys unavailable.");
        }

        return [
            'access_key' => self::$accessKey,
            'refresh_key' => self::$refreshKey,
            'realm_id' => getenv("QB_REALM")
        ];
    }

    public static function setKeys($access, $refresh){
        self::$accessKey = $access;
        self::$refreshKey = $refresh;
    }
}

