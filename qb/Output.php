<?php

namespace QuickBooks;

class Output{

    public static function info($str){
        if(defined('STDIN')){
            echo "\e[0;32m$str\e[0m\n";
        }
        else{
            echo "<div><span class=\"info\">$str</span></div>";
        }
        self::logLine("<div><span class=\"info\">$str</span></div>");
    }

    public static function error($str){
        if(defined('STDIN')){
            echo "\e[0;31m$str\e[0m\n";
        }
        else{
            echo "<div><span class=\"error\">$str</span></div>";
        }
        self::logLine("<div><span class=\"error\">$str</span></div>");
    }

    public static function warn($str){
        if(defined('STDIN')){
            echo "\e[1;33;40m$str\e[0m\n";
        }
        else{
            echo "<div><span class=\"warning\">$str</span></div>";
        }
        self::logLine("<div><span class=\"warning\">$str</span></div>");
    }

    public static function varDump($data){
		if(defined('STDIN')){
			var_dump($data);
		} else {
			echo "<pre>";
			var_dump($data);
			echo "</pre>";
		}
	}

	public static function logLine($str){
    	file_put_contents( "log.html",$str."\r\n", FILE_APPEND);
	}

}
