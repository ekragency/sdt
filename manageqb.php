<?php
//https://developer.intuit.com/app/developer/qbo/docs/develop/sdks-and-samples-collections/php#tutorial-objective
require __DIR__ . "/vendor/autoload.php";
use QuickBooks\Output;

Output::info("This is no longer meant to run from the command line.");